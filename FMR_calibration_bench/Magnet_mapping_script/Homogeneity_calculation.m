function [field_homogeneity_map]=Homogeneity_calculation(field_map,X_ref,Y_ref,Z_ref,X_index,Y_index,Z_index,field_index)
%calculation of the homogeneity w.r.t. the magnetic field at
%X_ref,Y_ref,Z_ref position

X_ref_index=find(field_map(:,X_index)==X_ref);   % find the indexes with X_ref value into the Xs of the field map
Y_ref_index=find(field_map(:,Y_index)==Y_ref);   % find the indexes with Y_ref value into the Ys of the field map
Z_ref_index=find(field_map(:,Z_index)==Z_ref);   % find the indexes with Z_ref value into the Zs of the field map
field_reference_index=X_ref_index(ismember(X_ref_index,Y_ref_index)&ismember(X_ref_index,Z_ref_index)); % find the index that is common to the 3 previous indexes
field_reference=field_map(field_reference_index,field_index);    %find the field value corsponsdiing to the defined reference position
field_homogeneity=(field_map(:,field_index)-field_reference)./field_reference;
field_homogeneity_map=[field_map(:,X_index) field_map(:,Y_index) field_map(:,Z_index) field_homogeneity];
end