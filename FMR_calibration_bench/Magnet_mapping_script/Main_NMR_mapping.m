%%% analysis mapping dipole PXMDBCAWWP-OR000501 (PS bumper dipole)
%%%%ATTENTION Y value from the bottom pole
clear all, close all,
%% parameter list to be filled
X_index=1;    %colomn position of the X position values
Y_index=2;    %colomn position of the Y position values
Z_index=3;    %colomn position of the Z position values
field_index=4; %colomn position of the magnetic field values
X_inter_step=1;         % X Step in mm for the interpolation caluclation
Y_inter_step=1;         % Y Step in mm for the interpolation caluclation
interp_method='spline'; %Interpolation  method
X_ref=0;               % X position used to calculate the field homogeneity 
Y_ref=14;               % Y position (from the bottom pole used to calculate the field homogeneity 
Z_ref=0               % Z position used to calculate the field homogeneity 
Z_sel_slide=0          % Z position of the 2D analysis (a slide in XY at Z_sel_slide) 
filename=input('insert the Excel results file name e.g."mapping 20042015.xlsx":\n','s')  % complete name of the excel file
field_level=input('insert the field level of the mapping e.g."500G":\n','s')   %Field level of the mapping to be analysed
%%% END parameter list to be filled
%% extract data form the Excel file
[field_map,current,date_meas]=NMR_data_read(filename,field_level);
%%% END extract data form the Excel file
%% plot the mapping field values
plot3(field_map(:,X_index),field_map(:,Y_index),field_map(:,field_index),'.')


[X_meas,Y_meas,Y_meas_relative,map_xy]=mapping_into_matrix(field_map,Z_sel_slide,X_index,Y_index,Z_index,field_index);

figure1=figure;
axes1 = axes('Parent',figure1);
view(axes1,[140 40]);
grid(axes1,'on');
hold(axes1,'all');
surf(X_meas,Y_meas,map_xy*10000)
xlabel('X [mm]')
ylabel('Y [mm]')
zlabel('Field [Gauss]')
colorbar;
map_title=strcat('Field map at ',field_level,'at z=',num2str(Z_sel_slide),' mm');
title(map_title)
saveas(gcf, map_title, 'png')
saveas(gcf, map_title, 'fig')
%%%END plot the mapping field values

%% Homogeneity 
%%% the reference position (X=0 mm; Y=-27.5 mm; Z= 0mm ) of the NMR is taken as the origin of the
%%% relative calculation
[field_homogeneity_map]=Homogeneity_calculation(field_map,X_ref,Y_ref,Z_ref,X_index,Y_index,Z_index,field_index);

plot3(field_map(:,X_index),field_map(:,Y_index),field_homogeneity_map(:,4),'.')
xlabel('X [mm]')
ylabel('Y [mm]')
zlabel('field homogeneity [p.u.]')
[X_meas,Y_meas,Y_meas_relative,homog_map_xy]=mapping_into_matrix(field_homogeneity_map,Z_sel_slide,X_index,Y_index,Z_index,field_index);

figure2=figure;
axes1 = axes('Parent',figure2);
view(axes1,[140 40]);
grid(axes1,'on');
hold(axes1,'all');
surf(X_meas,Y_meas,homog_map_xy)
xlabel('X [mm]')
ylabel('Y [mm]')
zlabel('field homogeneity [p.u.]')
colorbar;
homog_title=strcat('Field homogeneity at ',field_level,'at z=',num2str(Z_sel_slide),' mm');
title(homog_title)
saveas(gcf, homog_title, 'png')
saveas(gcf, homog_title, 'fig')
%%% interpolation of the data to have 1 mm mesh
[X_meas_inter_grid,Y_meas_inter_grid,homog_map_xy_inter]=field_map_interpolation(homog_map_xy,X_meas,Y_meas,X_inter_step,Y_inter_step,interp_method);

figure3=figure;
axes1 = axes('Parent',figure3);
view(axes1,[140 40]);
grid(axes1,'on');
hold(axes1,'all');
surf(X_meas_inter_grid,Y_meas_inter_grid,homog_map_xy_inter)
xlabel('X [mm]')
ylabel('Y [mm]')
zlabel('field homogeneity [p.u.]')
colorbar;
homog_title_inter=strcat('Field homogeneity interpolation at ',field_level,'at z=',num2str(Z_sel_slide),' mm');
title(homog_title_inter)
saveas(gcf, homog_title_inter, 'png')
saveas(gcf, homog_title_inter, 'fig')
%% Excel convertion
excel_filename=filename;
excel_sheet_name=strcat('field_level_',field_level);
XlRange='A1';
date_analysis={'Date and Time',datestr(now)};
xlswrite(excel_filename,date_analysis,excel_sheet_name,XlRange)

XlRange='A2';
excel_data_title={'X [mm]','Y [mm]','Yrelative [mm]','Z [mm]','Field [T]'};
xlswrite(excel_filename,excel_data_title,excel_sheet_name,XlRange)

XlRange='A3';
excel_data=Z_sel_slide;
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)

XlRange='C3';
excel_data=X_meas';
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)

XlRange='A4';
excel_data=Y_meas;
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)

XlRange='B4';
excel_data=Y_meas_relative;
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)

XlRange='C4';
excel_data=map_xy;
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)
