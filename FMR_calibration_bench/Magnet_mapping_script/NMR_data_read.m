function [field_map,current,measurement_date]=NMR_data_read(filename,field_level)
%%% excel data extraction for NMR mapping
%% extract data form the Excel file
[num_bin,txt_bin,measurement_date]=xlsread(filename,'sheet1','B1'); % take the data as raw
[num_bin,txt_bin,field_map_500G]=xlsread(filename,'sheet1','A3:D102');
current_500G=xlsread(filename,'sheet1','D1');

[num_bin,txt_bin,field_map_700G(:,4)]=xlsread(filename,'sheet1','F3:F102');
field_map_700G(:,1:3)=field_map_500G(:,1:3);
current_700G=xlsread(filename,'sheet1','F1');

[num_bin,txt_bin,field_map_1200G(:,4)]=xlsread(filename,'sheet1','H3:H102');
field_map_1200G(:,1:3)=field_map_500G(:,1:3);
current_1200G=xlsread(filename,'sheet1','H1');
clear num_bin, clear txt_bin  % remove the intermediate useless data
%%% END extract data form the Excel file
%% format the data into matlab matrix readable
field_map_text=strcmp(field_map_500G,'not locked');  %TRUE when the text "not locked" is in the cell
[ind_row,ind_col]=find(field_map_text);      % find the corresponding intex of the TRUE value above   
field_map_500G(ind_row,ind_col)={nan};     %replace the "not locked" corresponding index by "NaN"
field_map_500G=cell2mat(field_map_500G);   % convert the cell to a matrix format for performing operation on it

field_map_text=strcmp(field_map_700G,'not locked');
[ind_row,ind_col]=find(field_map_text);
field_map_700G(ind_row,ind_col)={nan};
field_map_700G=cell2mat(field_map_700G);

field_map_text=strcmp(field_map_1200G,'not locked');
[ind_row,ind_col]=find(field_map_text);
field_map_1200G(ind_row,ind_col)={nan};
field_map_1200G=cell2mat(field_map_1200G);
%%% END format the data into matlab matrix readable
if strcmp(field_level,'500G');
    field_map=field_map_500G;
    current=current_500G;
elseif strcmp(field_level,'700G');
    field_map=field_map_700G;
    current=current_700G;
elseif strcmp(field_level,'1200G');
    field_map=field_map_1200G;
    current=current_1200G;
end


end