function [X_meas_inter_grid,Y_meas_inter_grid,homog_map_xy_inter]=field_map_interpolation(homog_map_xy,X_meas,Y_meas,X_inter_step,Y_inter_step,interp_method);
%perform the interpolation following the step of interpolation in
%millimeter and the interpolation method
[X_meas_grid,Y_meas_grid]=meshgrid(X_meas,Y_meas);
[X_meas_inter_grid,Y_meas_inter_grid]=meshgrid([X_meas(1):X_inter_step:X_meas(end)],[Y_meas(1):Y_inter_step:Y_meas(end)]);
homog_map_xy_inter=interp2(X_meas_grid,Y_meas_grid,homog_map_xy,X_meas_inter_grid,Y_meas_inter_grid,interp_method);

end