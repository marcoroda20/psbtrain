function [X_meas,Y_meas,Y_meas_relative,map_xy]=mapping_into_matrix(field_map,Z_sel_slide,X_index,Y_index,Z_index,field_index);
%%% convert the array of results into matrix results for the surface plot
X_meas=unique(field_map(:,X_index));  % search the X values used for the entire mapping
Y_meas=unique(field_map(:,Y_index));  % search the Y values used for the entire mapping
Y_meas_relative=Y_meas-(83/2);    % Give the Y value w.r.t. the center of the magnet apperture (83 mm) 
Z_meas=unique(field_map(:,Z_index));  % search the Z values used for the entire mapping
nb_Xval=length(X_meas);                         % number of X values in the mapping
nb_Yval=length(Y_meas);                         % number of Z values in the mappin
field_map_2D=field_map((field_map(:,Z_index)==Z_sel_slide),:);   %select all data row w.r.t. the Z slide choose for the 2D plot
map_xy=zeros(nb_Yval,nb_Xval);                  % create the matrix used to store the entire mapping 
map_xy(:,:)=NaN;  % replace the zeros per NaN to avoid misreading with the real magnetic field values
for i=1:nb_Yval     % loop usedd to "reashape" the list of measurement values to a matrix B(y,x)
    Y_findval=find(field_map_2D(:,Y_index)==Y_meas(i));   % find the index of the same Y value in the Y colonm of the measurements 
    temposorting=sortrows(field_map_2D(Y_findval,:),X_index);  % sort the X values in assending from the previous line
    index_X_sorting=ismember(X_meas,temposorting(:,X_index));       %find the X index from the sorting to place the X value at the correct position
    map_xy(i,index_X_sorting)=temposorting(:,field_index);   % after the finding and sorting of the Y X values it select and place the B values at the correct position in the matrix
end

end