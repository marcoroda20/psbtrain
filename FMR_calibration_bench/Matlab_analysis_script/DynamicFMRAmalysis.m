clear all;close all;
%% Analysis of the dynamic measures of the FMR
path='FMR21_09_11_ 53';
path='FMR22_09_11_  8';
path='FMR23_09_11_ 15';
path='FMR29_09_11_ 20';
listing = dir(path);
current_Gain=60;
index=1;
for n=1:length(listing)
    if ((listing(n).isdir==0)&&(strcmp(lower(listing(n).name),'config.txt')~=1))
%         channel{index} = importdata([path '\' listing(n).name]);
        tic
        channel{index}=dynread_2([path '\' listing(n).name],2*5*1*10);
        toc
        index=index+1;
        
    end
end
freqs = read_fres(strcat(path,'\config.txt'));
ncycle=10;

%% Sort the measures and prepare to analyse
NMR_locking=reshape(channel{1}(:,1),2,size(channel{1}(:,1),1)/2)';
NMR_locking(:,1)=NMR_locking(:,1);
dt=channel{1}(1,2);
FMR_dyn_cycle=reshape(channel{1}(:,3:end)',size(channel{1}(:,3:end),2)*2,size(channel{1}(:,3:end),1)/2);
Current_dyn_cycle=reshape(channel{2}(:,3:end)',size(channel{2}(:,3:end),2)*2,size(channel{2}(:,3:end),1)/2);
Coil_dyn_cycle=reshape(channel{3}(:,3:end)',size(channel{3}(:,3:end),2)*2,size(channel{3}(:,3:end),1)/2);

fe=1/dt;
time=(0:dt:dt*size(Current_dyn_cycle,1)-dt);

%% Analysis

% integration of the coil voltage to have the field
FieldCoil=-cumtrapz(time,Coil_dyn_cycle);
% find the mean value of the 3 differente plate
[index] = findStable(FieldCoil,dt);
% take out the drift of the integral (the field)
FieldCoilCorr=-cumtrapz(time,Coil_dyn_cycle-repmat(mean(Coil_dyn_cycle(index(:,1):index(:,2),:))...
    ,size(Coil_dyn_cycle,1),1));
% calcul the mean
[meanplatefield] = calcFind(FieldCoilCorr,index);
% figure;
% plot(time,FieldCoilCorr);
% hold on;
% plot(time(index),FieldCoilCorr(index),'o');
% plot(repmat(time(round([mean(index(1,1:2)) mean(index(1,3:4))])),10,1),...
%     meanplatefield,'*')
% calibration with the NMR values
equivalentSurf=FieldCoilCorr(240001,:)./(NMR_locking(:,2)-NMR_locking(:,1))';

% find the FMR-time stamp for each signals
% TO DO
for i=1:size(FieldCoilCorr,2)
    field_Tesla(:,i) = (FieldCoilCorr(:,i)/equivalentSurf(i))+NMR_locking(i,1);
end
% figure,plot (time,field_Tesla),hold on,plot(time(1),NMR_locking(:,1),'*r'),plot(time(240000),NMR_locking(:,2),'*b')
FMR_dyn_cycle=FMR_dyn_cycle(1:240000,:) ;
threshold=0.1;      % 10% of the range of the FMR signal
ncoefdct=(0.05*size(FMR_dyn_cycle,1));% number off coefficient use for the idct
%threshold
FMR_thres=threshold*(max(FMR_dyn_cycle)-min(FMR_dyn_cycle))+min(FMR_dyn_cycle);
FMR_under_thres=zeros(size(FMR_dyn_cycle));
for i=1:size(FMR_dyn_cycle,2)
    FMR_under_thres(:,i)=(FMR_dyn_cycle(:,i)<FMR_thres(i));
end


dFMR_sign = diff(FMR_dyn_cycle);
dct_dFMR_sign=dct(dFMR_sign);
idct_dFMR_sign=idct([dct_dFMR_sign(1:ncoefdct,:) ; zeros(size(dct_dFMR_sign,1)-ncoefdct,size(dct_dFMR_sign,2))]);
sign_dFMR_sign = sign(idct_dFMR_sign);
%zero crossing
zero_crossing=sign_dFMR_sign-[sign_dFMR_sign(1,:);sign_dFMR_sign(1:end-1,:)];
FMR_index=zeros(1,size(zero_crossing,2));
for i=1:size(FMR_dyn_cycle,2)
     FMR_index(i)=(find((zero_crossing(:,i).*FMR_under_thres(1:end-1,i))~=0));
end

% figure(4);
% 
% plot(time(1:240000),FMR_dyn_cycle);
% hold on;
% figure(5);plot(time,field_Tesla);hold on;
for n=1:size(FMR_dyn_cycle,2)
%     figure(4),plot(time(FMR_index(n)),FMR_dyn_cycle(FMR_index(n),n),'.r')
    Field_FMR_meas(n) = field_Tesla(FMR_index(n),n);
%     figure(5),plot(time(FMR_index(n)),Field_FMR_meas(n),'.r')
end
Current_dyn_cycle= current_Gain*Current_dyn_cycle;
% calculate the field ramp rate
index_top = index(:,3)-130000;
index_bott = index(:,2)+20000;
figure(10),hold on,
for i=1:length(index_top)
    Ramp_rate(i) = (field_Tesla(index_top(i),i)-field_Tesla(index_bott(i),i))./(time(index_top(i))-time(index_bott(i)));
    current_rate(i) = (Current_dyn_cycle(index_top(i),i)-Current_dyn_cycle(index_bott(i),i))./(time(index_top(i))-time(index_bott(i)));
    figure(10),plot(Current_dyn_cycle(index_bott(i):index_top(i),i))
 end



% calculate the difference between FMR and field coil
FMRfield=repmat((0.05:(0.15-0.05)/8:0.15),ncycle,1);
diffFMRcoil=mean(FieldCoilCorr(index(:,1):index(:,2)))*equivalentSurf(:)-FMRfield(:,1);


%% Plot for every rampe rate

disp('Done...')

