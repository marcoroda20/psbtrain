function [Field_FMR_meas_up,FMR_max_value_up,Ramp_rate_up,Field_FMR_meas_down,...
    FMR_max_value_down,Ramp_rate_down,file_position,equivalentSurf,snr,...
    Coil_dyn_cycle,NMR_locking_up,NMR_locking_down,field_Tesla,...
    Current_dyn_cycle,FMR_dyn_cycle,fe,FMR_index_up]=...
    DynamicFMRAnalysis_function_2(fid,nfreques,n_ramp_rate,file_position_start,...
    ncycle,current_Gain,negative_current_bottom,shifttime,sampleperchannel,DAQ_channel,Offset_bot_NMR_FMR)

% nramprate=5;

for n=1:length(fid)
        tic
        [channel{n},file_position(n)]=dynread_3(fid(n),2*n_ramp_rate*nfreques*ncycle,file_position_start(n),sampleperchannel);
        toc
end

%% Sort the measures and prepare to analyse
%use DAQ_channel.current data for finding the NMR data and dt data
% the DAQ_channel structure below is used with +1 because the index of the cell must
% start at 1 
NMR_locking_up=reshape(channel{DAQ_channel.current+1}(:,1),2,size(channel{DAQ_channel.current+1}(:,1),1)/2)';
if (negative_current_bottom==1)
    NMR_locking_up(:,1)=-NMR_locking_up(:,1);
end
NMR_locking_down=reshape([channel{DAQ_channel.current+1}(2:end,1);channel{DAQ_channel.current+1}(1,1)],2,size(channel{DAQ_channel.current+1}(:,1),1)/2)';
dt=channel{DAQ_channel.current+1}(1,2);
FMR_dyn_cycle=reshape(channel{DAQ_channel.FMR+1}(:,3:end)',size(channel{DAQ_channel.FMR+1}(:,3:end),2)*2,size(channel{DAQ_channel.FMR+1}(:,3:end),1)/2);      %channel 1
Current_dyn_cycle=reshape(channel{DAQ_channel.current+1}(:,3:end)',size(channel{DAQ_channel.current+1}(:,3:end),2)*2,size(channel{DAQ_channel.current+1}(:,3:end),1)/2);   %channel 2
Coil_dyn_cycle=reshape(channel{DAQ_channel.coil+1}(:,3:end)',size(channel{DAQ_channel.coil+1}(:,3:end),2)*2,size(channel{DAQ_channel.coil+1}(:,3:end),1)/2);      %channel 3

fe=1/dt;
time=(0:dt:dt*size(Current_dyn_cycle,1)-dt);

%% Analysis

% integration of the coil voltage to have the field
FieldCoil=cumtrapz(time,Coil_dyn_cycle);       %- sign in front of cumtrapz removed by AB the 25/03/2015
% find the mean value of the 3 differente plateau for the NMR and Current level calculation 
[index] = findStable(FieldCoil,dt,shifttime);
% take out the drift of the integral (the field)
drift=(FieldCoil(end-1,:)-FieldCoil(2,:))/(time(end-1)-time(2));  %corresponds to the slope coefficient, the index 2 and end-1 are choosen to avoid the problem of the first and last values (sometime spikes) 
for i=1:size(FieldCoil,2)
    FieldCoilCorr(:,i)=FieldCoil(:,i)-drift(i).*time'; %correct the drift on the flux
end 
% FieldCoilCorr=cumtrapz(time,Coil_dyn_cycle-...
%     repmat(mean(Coil_dyn_cycle(index(:,1):index(:,2),:)),...
%     size(Coil_dyn_cycle,1),1));  %- sign in front of cumtrapz removed by AB the 25/03/2015
% calcul the mean
% figure;
% plot(time,FieldCoilCorr);
% hold on;
% plot(time(index),FieldCoilCorr(index),'o');
% check the sign of the current at the start of the cycle to correct the
% sigh of the bottom NMR marker
Current_flatbottom_sign=sign(mean(Current_dyn_cycle(index(:,1):index(:,2),:))); %check the sign of the flatbottom current over the index average used for flux drift correction
equivalentSurf=mean(FieldCoilCorr(index(:,3):index(:,4),:))./(NMR_locking_up(:,2)-(Current_flatbottom_sign'.*NMR_locking_up(:,1)))';  % index(:,3):index(:,4) gives the flatop indexes 
 
% find the FMR-time stamp for each signals
for i=1:size(FieldCoilCorr,2)
    field_Tesla(:,i) = (FieldCoilCorr(:,i)/equivalentSurf(i))+((Current_flatbottom_sign(i).*NMR_locking_up(i,1))+Offset_bot_NMR_FMR);   %added 22/04/2014 "Offset_bot_NMR_fit" corresponds to the offset due to the magnet homogeneity difference at the NMR location w.r.t. the FMR location 
end

%% ramp UP
Current_dyn_cycle= current_Gain*Current_dyn_cycle;
FMR_dyn_cycle_up=FMR_dyn_cycle(1:floor(size(FieldCoilCorr,1)/2),:) ;
threshold=0.15;      % 15% of the range of the FMR signal
ncoefdct=(0.05*size(FMR_dyn_cycle_up,1));% number off coefficient use for the idct
%threshold
FMR_thres_up=threshold*(max(FMR_dyn_cycle_up)-min(FMR_dyn_cycle_up))+min(FMR_dyn_cycle_up);
FMR_under_thres_up=zeros(size(FMR_dyn_cycle_up));
for i=1:size(FMR_dyn_cycle_up,2)
    FMR_under_thres_up(:,i)=(FMR_dyn_cycle_up(:,i)<FMR_thres_up(i));
end

dFMR_sign_up = diff(FMR_dyn_cycle_up);
dct_dFMR_sign_up=dct(dFMR_sign_up);
idct_dFMR_sign_up=idct([dct_dFMR_sign_up(1:ncoefdct,:) ; zeros(size(dct_dFMR_sign_up,1)-ncoefdct,size(dct_dFMR_sign_up,2))]);
sign_dFMR_sign_up = sign(idct_dFMR_sign_up);
%zero crossing
zero_crossing_up=sign_dFMR_sign_up-[sign_dFMR_sign_up(1,:);sign_dFMR_sign_up(1:end-1,:)];
FMR_index_up=zeros(1,size(zero_crossing_up,2));
j=1;
for i=1:size(FMR_dyn_cycle_up,2)
    FMR_index_up(i)=find((zero_crossing_up(:,i).*FMR_under_thres_up(1:end-1,i))~=0);
end

for n=1:size(FMR_index_up,2)
    Field_FMR_meas_up(n) = field_Tesla(FMR_index_up(n),n);
    FMR_max_value_up(n) = FMR_dyn_cycle_up(FMR_index_up(n),n);
end
figure
color_plot=hsv(size(field_Tesla,2));
for n=1:size(field_Tesla,2)
    plot(time,field_Tesla(:,n),'color',color_plot(n,:))
    if n==1
        hold on
    end
    plot(time(FMR_index_up(n)),field_Tesla(FMR_index_up(n),n),'*','color',color_plot(n,:))
end

% calcul the ramp rate UP
nwin=200;   %used for filtering otherwise the derivative of  "Current_dyn_cycle" is not correct  => discontinuity 
b=ones(1,nwin)/nwin; %used for filtering otherwise the derivative of  "Current_dyn_cycle" is not correct  => discontinuity 
Current_dyn_cycle_up=filtfilt(b,1,Current_dyn_cycle(1:floor(size(Current_dyn_cycle,1)/2),:));  %no delay filter
for n=1:size(FMR_index_up,2)   % take the ramp rate value AT the FMR marker point
    Current_didt_up(length(Current_dyn_cycle_up),n)=0;     %add a zero value to fit with the size of the time and current array
    Current_didt_up(1:(end-1),n)=diff(Current_dyn_cycle_up(:,n))./dt;
    Ramp_rate_up(n) = Current_didt_up(FMR_index_up(n),n);    
end

% % % nwin=200;
% % % b=ones(1,nwin)/nwin;
% % % % Coil_dyn_cycle_up=Coil_dyn_cycle(1:floor(size(FieldCoilCorr,1)/2),:);
% % % Coil_dyn_cycle_up=filtfilt(b,1,Coil_dyn_cycle(1:floor(size(FieldCoilCorr,1)/2),:));
% % % t=0.01; % [ms]
% % % timedec=round(t*fe);
% % % for i=1:size(Coil_dyn_cycle_up,2); % calculate the field ramp rate
% % %     index_top_up(i) = find (Coil_dyn_cycle_up(:,i)>+0.05,1,'first');
% % %     index_bott_up(i) = find (Coil_dyn_cycle_up(index_top_up(i):end,i)<+0.04,1,'first');
% % %     index_top_up(i) = index_top_up(i)+timedec;
% % %     index_bott_up(i) = index_top_up(i)+index_bott_up(i)-2*timedec;
% % % end
% % % for i=1:length(index_top_up)
% % %     P_up = polyfit(time(index_top_up(i):index_bott_up(i)),...
% % %         field_Tesla(index_top_up(i):index_bott_up(i),i)',1);
% % %     Ramp_rate_up(i)=P_up(1);
% % %     current_rate_up(i) = (Current_dyn_cycle(index_top_up(i),i)-...
% % %         Current_dyn_cycle(index_bott_up(i),i))./...
% % %         (time(index_top_up(i))-time(index_bott_up(i)));
% % % end

%% rampe DOWN
FMR_dyn_cycle_down=FMR_dyn_cycle(floor(size(FieldCoilCorr,1)/2)+1:end,:) ;
threshold=0.15;      % 10% of the range of the FMR signal
ncoefdct=(0.05*size(FMR_dyn_cycle_down,1));% number off coefficient use for the idct
%threshold
FMR_thres_down=threshold*(max(FMR_dyn_cycle_down)-min(FMR_dyn_cycle_down))+min(FMR_dyn_cycle_down);
FMR_under_thres_down=zeros(size(FMR_dyn_cycle_down));
for i=1:size(FMR_dyn_cycle_down,2)
    FMR_under_thres_down(:,i)=(FMR_dyn_cycle_down(:,i)<FMR_thres_down(i));
end

dFMR_sign_down = diff(FMR_dyn_cycle_down);
dct_dFMR_sign_down=dct(dFMR_sign_down);
idct_dFMR_sign_down=idct([dct_dFMR_sign_down(1:ncoefdct,:) ; zeros(size(dct_dFMR_sign_down,1)-ncoefdct,size(dct_dFMR_sign_down,2))]);
sign_dFMR_sign_down = sign(idct_dFMR_sign_down);
%zero crossing
zero_crossing_down=sign_dFMR_sign_down-[sign_dFMR_sign_down(1,:);sign_dFMR_sign_down(1:end-1,:)];
FMR_index_down=zeros(1,size(zero_crossing_down,2));
j=1;
for i=1:size(FMR_dyn_cycle_down,2)
    FMR_index_down(i)=find((zero_crossing_down(:,i).*FMR_under_thres_down(1:end-1,i))~=0,1,'first');
end

offset_index_down=floor(size(FieldCoilCorr,1)/2);
Field_FMR_meas_down=zeros(size(FMR_index_down,2),1);
FMR_max_value_down=zeros(size(FMR_index_down,2),1);
for n=1:size(FMR_index_down,2)
    Field_FMR_meas_down(n) = field_Tesla(FMR_index_down(n)+offset_index_down,n);
    FMR_max_value_down(n) = FMR_dyn_cycle_down(FMR_index_down(n),n);
end

% to take out the last measure with error
for n=1:length(Field_FMR_meas_down)
    if (abs(mean(Field_FMR_meas_down)-Field_FMR_meas_down(n))>...
            0.05*abs(mean(Field_FMR_meas_down)))
        if (n~=1)
            Field_FMR_meas_down(n)=Field_FMR_meas_down(n-1);
        else
            Field_FMR_meas_down(n)=Field_FMR_meas_down(end);
        end
    end    
end    
% calcul the ramp rate DOWN
nwin=200;
b=ones(1,nwin)/nwin;
Coil_dyn_cycle_down=filtfilt(b,1,Coil_dyn_cycle(floor(size(FieldCoilCorr,1)/2)+1:end,:));
t=0.01; % [ms]
timedec=round(t*fe);
for i=1:size(Coil_dyn_cycle_down,2); % calculate the field ramp rate
    index_top_down(i) = find (Coil_dyn_cycle_down(:,i)<-0.05,1,'first');
    index_bott_down(i) = find (Coil_dyn_cycle_down(index_top_down(i):end,i)>-0.04,1,'first');
    index_top_down(i) = offset_index_down+index_top_down(i)+timedec;
    index_bott_down(i) = index_top_down(i)+index_bott_down(i)-2*timedec;
end
for i=1:length(index_top_down)
    P_down = polyfit(time(index_top_down(i):index_bott_down(i)),...
        field_Tesla(index_top_down(i):index_bott_down(i),i)',1);
    Ramp_rate_down(i)=P_down(1);
    current_rate_down(i) = (Current_dyn_cycle(index_top_down(i),i)-...
        Current_dyn_cycle(index_bott_down(i),i))./...
        (time(index_top_down(i))-time(index_bott_down(i)));
end

% Calcul of SNR
[snr] = snrsignalFMR(FMR_dyn_cycle,dt);

disp('Done...')

