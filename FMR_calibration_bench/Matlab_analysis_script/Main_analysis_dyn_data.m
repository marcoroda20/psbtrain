%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Created by Giancarlo Golluccio in 2010
% Modified by Anthony Beaumont March 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%clear all;close all;
example_signal_plot=0;
%% Input parameters
FMR2B=28.13888912591537;    % GHz/T
FMRoffset=16.88870361250e-3;% GHz
ramp_rates = 1;%5;             % # of ramp rates to analyze
current_Gain=60;            % Gain of the DCCT used to read the current
magnet_TF=1050.308187;      % Magnet central transfer function in A/T
negative_current_bottom=0;	% =1  change the sign to the NMR value if the current flat bottom is negative
DAQ_channel.FMR=0;          % =2 PXI DAQ =0 USB DAQ channel number of the FMR on the NI-DAQ  =0 if on ai0
DAQ_channel.current=1;      % =0 PXI DAQ =1 USB DAQ channel number of the current DCCT on the NI-DAQ
DAQ_channel.coil=2;         % =4 PXI DAQ =2 USB DAQchannel number of the pick-up coil on the NI-DAQ
shifttime=0.035              % 0.1 for findstable()0.03
abs_field_offset_NMR_500G=-0.883E-4; %offset given by the NMR mapping with the field difference between the NMR and FMR sensor at 500 G
abs_field_offset_NMR_700G=-1.19E-4; %offset given by the NMR mapping with the field difference between the NMR and FMR sensor at 700 G
abs_field_offset_NMR_1200G=-2.05E-4; %offset given by the NMR mapping with the field difference between the NMR and FMR sensor at 1200 G
abs_field_offset_NMR_475G=0.047536756000000;
abs_field_offset_NMR=[abs_field_offset_NMR_500G abs_field_offset_NMR_700G abs_field_offset_NMR_1200G];
Offset_bot_NMR_fit_para=polyfit([500e-4 700e-4 1200e-4],abs_field_offset_NMR,1);
Offset_bot_NMR_fit=polyval(Offset_bot_NMR_fit_para,abs_field_offset_NMR_475G);
% read the measures from 0.05T to 0.22T
Series_Number_FMR= input('insert the serial number of the FMR sensor :\n','s');
path1= input('insert measurement directory name (ex: FMR30_03_15_ 14_2freq):\n','s');
ncycle= input('insert the number of cycles per set point:\n');
calibration_analysis= input('Do you want to perform the calibration analysis? (yes=1;no=0):\n');
power_anaysis= input('Do you want to perform the Power influence analysis? (yes=1;no=0):\n');
if power_anaysis==1
    struct_pos= input('Give the position in the structure data for the Power influence analysis ? (1...n):\n');
    power_anaysis_end= input(strcat('Does the position {',num2str(struct_pos),'} is the last measurement? (yes=1;no=0):\n'));
end
% % % Serial_Number_FMR='5930'
% % % path1 ='FMR30_03_15_ 14_2freq'
% % % ncycle=2;                  % # of cycles for each ramp rate

% nfreqs1 = 2;                % # of frequencies value to analyze

%end Input parameters
%% read the DAQ configuration parameters
[fid1,file_position_start1]=get_fid(path1);
[DAQ_config_para]=config_para_read(path1);

%%% END read configuration parameters
%% function to find the channel and analyse the data to give the FMR marker w.r.t. the ramp rate
nfreqs1=DAQ_config_para.nptfreq; % # of frequencies value to analyze
freqs1 = read_fres(strcat(path1,'\config.txt'));

[Field_FMR_meas_up1(:,1),FMR_max_value_up1(:,1),Ramp_rate_up1(:,1),...
    Field_FMR_meas_down1(:,1),FMR_max_value_down1(:,1),Ramp_rate_down1(:,1),...
    file_position1,equivalentSurf1(:,1),snr1(:,1),Coil_dyn_cycle1,...
    NMR_locking_up1,NMR_locking_down1,field_Tesla1,Current_dyn_cycle1,...
    FMR_dyn_cycle1,sample_rate,FMR_index_up1]= DynamicFMRAnalysis_function_2(fid1,nfreqs1,...
    ramp_rates,file_position_start1,ncycle,current_Gain,...
    negative_current_bottom,shifttime,DAQ_config_para.sampleperchannel,DAQ_channel,Offset_bot_NMR_fit);

%%% end function
%% reconstruct the FMR frequence set in the RF generator
if DAQ_config_para.nptfreq<=1    % check if only 1 freqence set
    field_range_FMR=DAQ_config_para.Fieldmin;  % the frequence set corrsponds to the miminum field 
else % if more than 1 freqence 
    field_range_FMR= linspace(DAQ_config_para.Fieldmin,DAQ_config_para.Fieldmax,DAQ_config_para.nptfreq);% create the array of fields from min field to max field with N-steps 
end
Freq_FMR_set= DAQ_config_para.TFHzT.*field_range_FMR;   % find the Frequency correspondance with the set fields values 
round_dig_freq = 1;                                     %used to cut the freqency data to the RF generator digit correspondance in total 11 digits
Freq_FMR_set = round(Freq_FMR_set.*(10.^round_dig_freq))./(10.^round_dig_freq); % oblige to * then / by 10^n to round the frequency number because round() returns only integer!

%%% end reconstruct the FMR frequence set in the RF generator
%% Reshape of the data + Calculation of the average and stdev of the FMR marker and ramp rate 
Field_FMR_meas_up1=reshape(Field_FMR_meas_up1,ncycle,[]);
Ramp_rate_up1=reshape(Ramp_rate_up1,ncycle,[]);
Field_FMR_meas_up1_average=mean(Field_FMR_meas_up1);
Field_FMR_meas_up1_stdev=std(Field_FMR_meas_up1);
Ramp_rate_up1_average=mean(Ramp_rate_up1);
Ramp_rate_up1_stdev=std(Ramp_rate_up1);
Field_ramp_rate_up_average=Ramp_rate_up1_average./magnet_TF;
Field_ramp_rate_up_stdev=Ramp_rate_up1_stdev./magnet_TF;
Field_ramp_rate_up_total_av=mean(Field_ramp_rate_up_average);
Field_ramp_rate_up_total_std=sqrt(sum(Field_ramp_rate_up_stdev.^2));
%end Calculatin of the average and stdev of the FMR marker and ramp rate 
%% recontruct the time
dt=1./DAQ_config_para.DAQrate;
time=(0:dt:dt*size(FMR_dyn_cycle1,1)-dt)';
%%% end recontruct the time
%% Power influence on the FMR signal 
if power_anaysis==1;
power_inf_meas.time{struct_pos}=time;
power_inf_meas.reffield{struct_pos}=DAQ_config_para.Fieldmin;
power_inf_meas.power{struct_pos}=DAQ_config_para.PowerdBm;
power_inf_meas.reffreq{struct_pos}=Freq_FMR_set;
power_inf_meas.fieldmeas{struct_pos}=Field_FMR_meas_up1;
power_inf_meas.FMRsignal{struct_pos}=FMR_dyn_cycle1;
power_inf_meas.fieldmeas_av{struct_pos}=Field_FMR_meas_up1_average;
power_inf_meas.fieldmeas_std{struct_pos}=Field_FMR_meas_up1_stdev;
power_inf_meas.FMRname{struct_pos}=Series_Number_FMR;
end
%%% END Power influence on the FMR signal
%% Plots
if calibration_analysis==1;
%%%%%%%%%%% all measurements marker data %%%%%%%%%%%
figure
plot(Field_FMR_meas_up1*10000,'o-')    % x10000 to have it in Gauss
legend(strcat('Frequency= ',num2str(Freq_FMR_set'),' Hz'));
xlabel('Measurement number')
ylabel('Field [Gauss]')
title(strcat('comparison FMR marker serial number: ',Series_Number_FMR))
grid
saveas(gcf, strcat('FMR marker serial number ',Series_Number_FMR), 'png')
saveas(gcf, strcat('FMR marker serial number ',Series_Number_FMR), 'fig')

%%%%%%%%%% dynamic load line  %%%%%%%%%%%%%%%
figure
FieldG_FMR_curveFit_para=polyfit(Freq_FMR_set*1e-9,Field_FMR_meas_up1_average.*10000,1);
FieldG_FMR_curveFit=polyval(FieldG_FMR_curveFit_para,Freq_FMR_set*1e-9);
errorbar(Freq_FMR_set*1e-9,Field_FMR_meas_up1_average*10000,Field_FMR_meas_up1_stdev*10000,'.-')    % x10000 to have it in Gauss
hold on
plot(Freq_FMR_set*1e-9,FieldG_FMR_curveFit,'k')
text(Freq_FMR_set(1)*1e-9,FieldG_FMR_curveFit(1),strcat(' \leftarrow',' B_m_a_r_k_e_r[Gauss]=',...
    num2str(FieldG_FMR_curveFit_para(1)),'*F[GHz]+',num2str(FieldG_FMR_curveFit_para(2))))
legend('Measurements','linear bestfit');
xlabel('Frequency [GHz]')
ylabel('Field [Gauss]')
title(strcat('FMR Dynamic transfer function serial number: ',Series_Number_FMR,' at field ramp rate=',num2str(Field_ramp_rate_up_total_av,2),' T/s'))
grid
saveas(gcf, strcat('FMR Dynamic transfer function serial number',Series_Number_FMR), 'png')
saveas(gcf, strcat('FMR Dynamic transfer function serial number',Series_Number_FMR), 'fig')
%%%%%%%%%% dynamic transfer function (DTF) %%%%%%%%%%%%%%%
DTF=(Freq_FMR_set*1e-9)./Field_FMR_meas_up1_average;   % calcul the GHz/T ratio for all field levels
DTF_average=mean(DTF);                          % calcul the GHz/T ratio average of all the levels over the measurement range
DTF_max=max(DTF);                               % calcul the max GHz/T ratio over the measurement range
DTF_min=min(DTF);                               % calcul the max GHz/T ratio over the measurement range
DTF_error=(DTF_max-DTF_min)/2;    % calcul the maximum error GHz/T ratio over the measurement range "/2" correspond to the +/- error on the average
disp(strcat('DTF over measurement range(',num2str(Field_FMR_meas_up1_average(1),'%6.2f'),...
' to ',num2str(Field_FMR_meas_up1_average(end),'%6.3f'),' Tesla): ',num2str(DTF_average,'%6.3f'),'+/-',num2str(DTF_error,'%6.3f'),' GHz/T'))
%%%%%%%%%% Example of a FMR signal with the marker%%%%%%%%
if example_signal_plot==1
    figure
    [AX,H1,H2]=plotyy(time,field_Tesla1(:,101)*10000,time,FMR_dyn_cycle1(:,101));
    set(H1,'LineStyle','-','color','r');
    set(AX(1),'Ycolor','r')
    set(H2,'LineStyle','-','color','b');
    set(AX(2),'Ycolor','b');
    set(AX(2),'Ylim',min(get(AX(2),'Ylim')) * [1 -1]);
    set(AX,'Xlim',[time(1) time(end)]);
    set(AX(1),'YTickMode','auto')
    set(AX(2),'YTickMode','auto')
    set(AX,'XTickMode','auto')
    hold(AX(1))
    [val,FMR_index_up]=min(abs(field_Tesla1(1:(end/2),101)-Field_FMR_meas_up1(1,11)));
    plot(AX(1),time(FMR_index_up(1)),Field_FMR_meas_up1(1,11)*10000,'ko');
    grid;
    xlabel('Time [s]');
    ylabel('field [Gauss]');
    set(get(AX(2),'Ylabel'),'String','FMR signal [V]');
    title_ex=strcat('Signal at FMR frequency ',num2str(freqs1(11)*1e-9,'%6.2f'),'GHz');
    title(title_ex)
    saveas(gcf, strrep(title_ex, '.', '_'), 'png')
    saveas(gcf, strrep(title_ex, '.', '_'), 'fig')
end
end
%%% end Plots
%% reshape of the cell into matrix for excel postanalysis %%%%%%%%%%%%%%%
if calibration_analysis==1;
% sheet with data
excel_filename='FMR_all_sensors_results.xlsx';
excel_sheet_name=strcat('Dynamic_TF_',Series_Number_FMR);
excel_data_title={'Set frequency [Hz]','FMR marker value [T]','FMR marker value standard deviation [T]'};
excel_data=[Freq_FMR_set' Field_FMR_meas_up1_average' Field_FMR_meas_up1_stdev'];
excel_FMR_series={'FMR series number',Series_Number_FMR};
XlRange='A1';
xlswrite(excel_filename,excel_FMR_series,excel_sheet_name,XlRange)
date_analysis={'Date and Time',datestr(now)};
XlRange='A2';
xlswrite(excel_filename,date_analysis,excel_sheet_name,XlRange)
XlRange='A3';
xlswrite(excel_filename,excel_data_title,excel_sheet_name,XlRange)
XlRange='A4';
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)
%sheet with parameters used during the measurement + Matlab info 
excel_sheet_name=strcat('input_parameters_',Series_Number_FMR);
excel_FMR_series={'FMR series number',Series_Number_FMR};
XlRange='A1';
xlswrite(excel_filename,excel_FMR_series,excel_sheet_name,XlRange)
XlRange='A2';
xlswrite(excel_filename,date_analysis,excel_sheet_name,XlRange)
XlRange='A3';
excel_data={'matlab path and script'};     % put {} for a string otherwise it writes each caracter in a separated Excel cell
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)
XlRange='B3';
excel_data={strcat(mfilename('fullpath'),'.m')};% put {} for a string otherwise it writes each caracter in a separated Excel cell
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)
XlRange='A4';
excel_data={'raw data folder name'};           % put {} for a string otherwise it writes each caracter in a separated Excel cell
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)
XlRange='B4';
excel_data={path1};                            % put {} for a string otherwise it writes each caracter in a separated Excel cell
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)
XlRange='A5';
excel_data={'number of cycle per measurement'};           % put {} for a string otherwise it writes each caracter in a separated Excel cell
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)
XlRange='B5';
excel_data=ncycle;                            
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)
XlRange='A6';
excel_data=fieldnames(DAQ_config_para);
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)
XlRange='B6';
excel_data=struct2cell(DAQ_config_para);
xlswrite(excel_filename,excel_data,excel_sheet_name,XlRange)
end
%%% END reshape of the cell into matrix for excel postanalysis %%%%%%%%%%%%%
%% Matlab workspace backup
if (power_anaysis==1)&&(power_anaysis_end==0);
%do nothing until the user indicate that it is the last Power value
%measured
clearvars -except power_inf_meas Series_Number_FMR
else
    date_of_the_analysis=datestr(now,'dd_mm_yyyy');
    save(strcat('Workspace_FMR_ref',Series_Number_FMR,'_analysis_date',date_of_the_analysis,'.mat'))
    disp(strcat('Analysis, Excel file creation and backup done the ',date_of_the_analysis))
end
%%% end Matlab workspace backup
%% old analysis
% % % % n_freques = nfreqs1;
% % % % for i = 2:n_freques
% % % %     if (i==8)
% % % %        i 
% % % %     end
% % % %     file_position_start1=file_position1;
% % % %     [Field_FMR_meas_up1(:,i),FMR_max_value_up1(:,i),Ramp_rate_up1(:,i),...
% % % %         Field_FMR_meas_down1(:,i),FMR_max_value_down1(:,i),Ramp_rate_down1(:,i),...
% % % %         file_position1,equivalentSurf1(:,i),snr1(:,i)] = DynamicFMRAnalysis_function(fid1,...
% % % %         1,ramp_rates,file_position_start1,ncycle,current_Gain,...
% % % %         negative_current_bottom,shifttime);
% % % % end

% read the measures from 0.06T 
% % % % nfreqs2 = 1;                % # of frequencies value to analyze
% % % % shifttime=0.1;              % for findstable()
% % % % path2 ='FMR11_11_11_ 23';
% % % % [fid2,file_position_start2]=get_fid(path2);
% % % % freqs2 = read_fres(strcat(path2,'\config.txt'));
% % % % [Field_FMR_meas_up2(:,1),FMR_max_value_up2(:,1),Ramp_rate_up2(:,1),...
% % % %     Field_FMR_meas_down2(:,1),FMR_max_value_down2(:,1),Ramp_rate_down2(:,1),...
% % % %     file_position2,equivalentSurf2(:,1),snr2(:,1),Coil_dyn_cycle2,...
% % % %     NMR_locking_up2,NMR_locking_down2,field_Tesla2,Current_dyn_cycle2,...
% % % %     FMR_dyn_cycle2,sample_rate2]= DynamicFMRAnalysis_function(fid2,...
% % % %     1,ramp_rates,file_position_start2,ncycle,current_Gain,...
% % % %     negative_current_bottom,shifttime);
% % read the measures from high f
% nfreqs3 = 4;                % # of frequencies value to analyze
% shifttime=0.05;             % for findstable()
% path3 ='FMR15_11_11_ 43';
% [fid3,file_position_start3]=get_fid(path3);
% freqs3 = read_fres(strcat(path3,'\config.txt'));
% [Field_FMR_meas_up3(:,1),FMR_max_value_up3(:,1),Ramp_rate_up3(:,1),...
%     Field_FMR_meas_down3(:,1),FMR_max_value_down3(:,1),Ramp_rate_down3(:,1),...
%     file_position3,equivalentSurf3(:,1),snr3(:,1),Coil_dyn_cycle3,...
%     NMR_locking_up3,NMR_locking_down3,field_Tesla3,Current_dyn_cycle3,...
%     FMR_dyn_cycle3,sample_rate3]= DynamicFMRAnalysis_function(fid3,...
%     1,ramp_rates,file_position_start3,ncycle,current_Gain,...
%     negative_current_bottom,shifttime);
% n_freques = nfreqs3;
% for i = 2:n_freques
%     file_position_start3=file_position3;
%     [Field_FMR_meas_up3(:,i),FMR_max_value_up3(:,i),Ramp_rate_up3(:,i),...
%         Field_FMR_meas_down3(:,i),FMR_max_value_down3(:,i),Ramp_rate_down3(:,i),...
%         file_position3,equivalentSurf3(:,i),snr3(:,i)] = DynamicFMRAnalysis_function(fid3,...
%         1,ramp_rates,file_position_start3,ncycle,current_Gain,...
%         negative_current_bottom,shifttime);
% end
% 
% % construct the matrix of both measures
% freqs = [freqs1(1) freqs2 freqs1(2:end) freqs3];
% Field_FMR_meas_up =...
%     [Field_FMR_meas_up1(:,1) Field_FMR_meas_up2 Field_FMR_meas_up1(:,2:end) Field_FMR_meas_up3];
% FMR_max_value_up =...
%     [FMR_max_value_up1(:,1) FMR_max_value_up2 FMR_max_value_up1(:,2:end) FMR_max_value_up3];
% Ramp_rate_up =...
%     [Ramp_rate_up1(:,1) Ramp_rate_up2 Ramp_rate_up1(:,2:end) Ramp_rate_up3];
% Field_FMR_meas_down =...
%     [Field_FMR_meas_down1(:,1) Field_FMR_meas_down2 Field_FMR_meas_down1(:,2:end) Field_FMR_meas_down3];
% FMR_max_value_down =...
%     [FMR_max_value_down1(:,1) FMR_max_value_down2 FMR_max_value_down1(:,2:end) FMR_max_value_down3];
% Ramp_rate_down =...
%     [Ramp_rate_down1(:,1) Ramp_rate_down2 Ramp_rate_down1(:,2:end) Ramp_rate_down3];
% equivalentSurf =...
%     [equivalentSurf1(:,1) equivalentSurf2 equivalentSurf1(:,2:end) equivalentSurf3];

% construct the matrix of both measures
% % % % freqs = [freqs1(1) freqs2 freqs1(2:end-1)];
% % % % Field_FMR_meas_up =...
% % % %     [Field_FMR_meas_up1(:,1) Field_FMR_meas_up2 Field_FMR_meas_up1(:,2:end)];
% % % % FMR_max_value_up =...
% % % %     [FMR_max_value_up1(:,1) FMR_max_value_up2 FMR_max_value_up1(:,2:end)];
% % % % Ramp_rate_up =...
% % % %     [Ramp_rate_up1(:,1) Ramp_rate_up2 Ramp_rate_up1(:,2:end)];
% % % % Field_FMR_meas_down =...
% % % %     [Field_FMR_meas_down1(:,1) Field_FMR_meas_down2 Field_FMR_meas_down1(:,2:end)];
% % % % FMR_max_value_down =...
% % % %     [FMR_max_value_down1(:,1) FMR_max_value_down2 FMR_max_value_down1(:,2:end)];
% % % % Ramp_rate_down =...
% % % %     [Ramp_rate_down1(:,1) Ramp_rate_down2 Ramp_rate_down1(:,2:end)];
% % % % equivalentSurf =...
% % % %     [equivalentSurf1(:,1) equivalentSurf2 equivalentSurf1(:,2:end)];
% % % % snr =...
% % % %     [snr1(:,1) snr2 snr1(:,2:end)];
% % % % 
% % % % Area_Eq_01 = reshape(equivalentSurf1,size(equivalentSurf1,1)*size(equivalentSurf1,2),1);
% % % % Field_FMR_up= reshape(Field_FMR_meas_up,size(Field_FMR_meas_up,1)*size(Field_FMR_meas_up,2),1);
% % % % Value_FMR_up= reshape(FMR_max_value_up,size(FMR_max_value_up,1)*size(FMR_max_value_up,2),1);
% % % % RampRate_up= reshape(Ramp_rate_up,size(Ramp_rate_up,1)*size(Ramp_rate_up,2),1);
% % % % 
% % % % % Plot SNR in function of field
% % % % figure('name','SNR');
% % % % subplot(2,1,1)
% % % % plot(freqs1(1:end-1)/1e9,mean(2*snr1));
% % % % grid on;
% % % % xlabel('f [GHz]');
% % % % ylabel('SNR {dB}');
% % % % subplot(2,1,2)
% % % % plot(freqs1(1:end-1)/(FMR2B*1e9),10.^(mean(snr1)./10));
% % % % grid on;
% % % % xlabel('B [T]');
% % % % ylabel('SNR');
% % % % 
% % % % % Plot to see the value of the minimum of the FMR signal
% % % % figure('name','FMR signal detection'),hold on
% % % % for n=1:10
% % % %      [min_ind(n),val_(n)] = min(FMR_dyn_cycle1(:,n));
% % % %      plot(FMR_dyn_cycle1(:,n));
% % % %      hold on;
% % % %      plot(val_(n),min_ind(n),'r*')    
% % % % end
% % % % 
% % % % % calcul of standard deviation and mean of FMR field measure, ramp rate
% % % % % and equivalent surface of coil
% % % % n=1;
% % % % for i=1:10:size(Field_FMR_meas_up,1)
% % % %     std_field_up(n,:) = std(Field_FMR_meas_up(i:i+9,:));
% % % %     mean_field_up(n,:) = mean(Field_FMR_meas_up(i:i+9,:));
% % % %     std_Ramp_rate_up(n,:) = std(Ramp_rate_up(i:i+9,:));
% % % %     mean_Ramp_rate_up(n,:) = mean(Ramp_rate_up(i:i+9,:));
% % % %     std_eq_surf (n,:) = std(equivalentSurf(i:i+9,:));
% % % %     mean_eq_surf (n,:) = mean(equivalentSurf(i:i+9,:));
% % % %     n=n+1;
% % % % end
% % % % 
% % % % n=1;
% % % % for i=1:10:size(Field_FMR_meas_down,1)
% % % %     std_field_down(n,:) = std(Field_FMR_meas_down(i:i+9,:));
% % % %     mean_field_down(n,:) = mean(Field_FMR_meas_down(i:i+9,:));
% % % %     std_Ramp_rate_down(n,:) = std(Ramp_rate_down(i:i+9,:));
% % % %     mean_Ramp_rate_down(n,:) = mean(Ramp_rate_down(i:i+9,:));
% % % %     n=n+1;
% % % % end
% % % % 
% % % % % Plot to see the relative dispersion of FMR-detection
% % % % for n=1:size(Field_FMR_meas_up,2)
% % % %     figure('name',['Relative dispersion in function of ramp rate ' num2str( mean(Field_FMR_meas_up(:,n)),2) ' [T]']);
% % % %     plot(mean_Ramp_rate_up(:,n),...
% % % %         3*std_field_up(:,n)*1e6,'*');
% % % %     hold on;
% % % %     grid on;
% % % %     ylabel('3*sigma [uT]');
% % % %     xlabel('rampe-rate [T/s]');
% % % %     title([num2str( mean(Field_FMR_meas_up(:,n)),2) ' [T]']);
% % % % end
% % % % 
% % % % figure('name','Relative dispersion in function of ramp rate');
% % % % plot(mean_Ramp_rate_up(:,2:end),...
% % % %     3*std_field_up(:,2:end)*1e6,'*');
% % % % hold on;
% % % % grid on;
% % % % ylabel('3*sigma [uT]');
% % % % xlabel('rampe-rate [T/s]');
% % % % clear h;
% % % % for n=2:size(Field_FMR_meas_up,2)
% % % %     h{n-1}=[num2str(mean(Field_FMR_meas_up(:,n)),2) ' T'];
% % % % end
% % % % legend(h,'Location','EastOutside');
% % % % 
% % % % 
% % % % [data]=eddycurrentcalculation();
% % % % figure('name','B in function of ramp rate');
% % % % errorbar(data.Ramp_rate*data.Tf_magnet/10000,((data.B_av)*1e6)-94800,2*data.B_r*1e6,2*data.B_r*1e6,'b*');
% % % % hold on;
% % % % grid on;
% % % % errorbar(mean_Ramp_rate_up(:,1),(mean_field_up(:,1)-mean_field_up(1,1))*1e6,2*std_field_up(:,1)*1e6,2*std_field_up(:,1)*1e6,'r*');
% % % % plot(data.Ramp_rate*data.Tf_magnet/10000,mean(data.tau)*(data.Ramp_rate*data.Tf_magnet/10000)*1e6-88.86,'k');
% % % % % errorbar(mean_Ramp_rate_up(:,2),(mean_field_up(:,2)-mean_field_up(1,2))*1e6,2*std_field_up(:,2)*1e6,2*std_field_up(:,2)*1e6,'r*');
% % % % ylabel('B [uT]')
% % % % xlabel('ramp rate [T/s]')
% % % % legend('0.09 T, with alu','0.06 T without alu','eddy current calculated','Location','EastOutside')
% % % % 
% % % % figure('name','Error of FMR measure in function of ramp-rate at 0.06 T');
% % % % i=2;
% % % % plot(Ramp_rate_up(:,i),1e6*(Field_FMR_meas_up(:,i)-mean(Field_FMR_meas_up(1:10,i))),'.');
% % % % hold on;
% % % % ylabel('B-mean(B) [uT]');
% % % % xlabel('rampe-rate [T/s]');
% % % % grid on;
% % % % title([num2str(mean(Field_FMR_meas_up(:,i)),2) ' T']);
% % % % 
% % % % % % Plot to see FMR detection in funtion of ramp-rate
% % % % % figure('name','Error of FMR measure in function of ramp-rate');
% % % % % ax(1)=subplot(2,1,1);
% % % % % for i=1:4
% % % % %     plot(Ramp_rate_up(:,i),(Field_FMR_meas_up(:,i)-mean(Field_FMR_meas_up(1:10,i))),'.');
% % % % %     hold on;
% % % % %     ylabel('UP B-mean(B) [T]');
% % % % %     xlabel('rampe-rate UP [T/s]');
% % % % % end
% % % % % grid on;
% % % % % ax(2)=subplot(2,1,2);
% % % % % for i=1:4
% % % % %     plot(Ramp_rate_down(:,i),(Field_FMR_meas_down(:,i)-mean(Field_FMR_meas_down(1:10,i))),'.');
% % % % %     hold on;
% % % % %     ylabel('DOWN B-mean(B) [T]');
% % % % %     xlabel('rampe-rate UP [T/s]');
% % % % % end
% % % % % grid on;
% % % % 
% % % % n_freques=length(freqs);
% % % % figure;
% % % % ax(1)=subplot(2,1,1);
% % % % errorbar(freqs(1:n_freques)/1e9,(1/(FMR2B*1e9)*freqs(1:n_freques)-FMRoffset/FMR2B)-mean_field_up(2,:),std_field_up(2,:),std_field_up(2,:),'.')
% % % % hold on;grid on;
% % % % errorbar(freqs(1:n_freques)/1e9,(1/(FMR2B*1e9)*freqs(1:n_freques)-FMRoffset/FMR2B)-mean_field_up(1,:),std_field_up(1,:),std_field_up(1,:),'.r')
% % % % errorbar(freqs(1:n_freques)/1e9,(1/(FMR2B*1e9)*freqs(1:n_freques)-FMRoffset/FMR2B)-mean_field_up(3,:),std_field_up(3,:),std_field_up(3,:),'.k')
% % % % errorbar(freqs(1:n_freques)/1e9,(1/(FMR2B*1e9)*freqs(1:n_freques)-FMRoffset/FMR2B)-mean_field_up(4,:),std_field_up(4,:),std_field_up(4,:),'.m')
% % % % errorbar(freqs(1:n_freques)/1e9,(1/(FMR2B*1e9)*freqs(1:n_freques)-FMRoffset/FMR2B)-mean_field_up(5,:),std_field_up(5,:),std_field_up(5,:),'.g')
% % % % ylabel('UP error B [T]');
% % % % xlabel('Frequ. [GHz]');
% % % % ax(2)=subplot(2,1,2);
% % % % errorbar(freqs(1:n_freques)/1e9,(1/(FMR2B*1e9)*freqs(1:n_freques)-FMRoffset/FMR2B)-mean_field_down(2,:),std_field_down(2,:),std_field_down(2,:),'.')
% % % % hold on;grid on;
% % % % errorbar(freqs(1:n_freques)/1e9,(1/(FMR2B*1e9)*freqs(1:n_freques)-FMRoffset/FMR2B)-mean_field_down(1,:),std_field_down(1,:),std_field_down(1,:),'.r')
% % % % errorbar(freqs(1:n_freques)/1e9,(1/(FMR2B*1e9)*freqs(1:n_freques)-FMRoffset/FMR2B)-mean_field_down(3,:),std_field_down(3,:),std_field_down(3,:),'.k')
% % % % errorbar(freqs(1:n_freques)/1e9,(1/(FMR2B*1e9)*freqs(1:n_freques)-FMRoffset/FMR2B)-mean_field_down(4,:),std_field_down(4,:),std_field_down(4,:),'.m')
% % % % errorbar(freqs(1:n_freques)/1e9,(1/(FMR2B*1e9)*freqs(1:n_freques)-FMRoffset/FMR2B)-mean_field_down(5,:),std_field_down(5,:),std_field_down(5,:),'.g')
% % % % ylabel('DOWN error B [T]');
% % % % xlabel('Frequ. [GHz]');
% % % % 
% % % % figure('name','Std=f(mean) for each ramp rate');
% % % % title('Std=f(mean) for each ramp rate');
% % % % ax(1)=subplot(2,1,1);
% % % % plot(mean(mean_field_up),std_field_up);
% % % % grid on;
% % % % ylabel('UP std B [T]');
% % % % % Legend
% % % % nmeas=10;
% % % % hleg={};
% % % % for n=1:round(length(Ramp_rate_up)/nmeas)
% % % %    hleg{n}=['Ramp rate ' num2str(round(mean(Ramp_rate_up((n-1)*nmeas+1:n*nmeas))*10)/10,2) ' [T/s]']; 
% % % % end
% % % % legend(hleg,'Location','EastOutside')
% % % % ax(2)=subplot(2,1,2);
% % % % plot(mean(mean_field_down),std_field_down);
% % % % xlabel('mean B [T]');
% % % % ylabel('DOWN std B [T]');
% % % % grid on;
% % % % % Legend
% % % % legend(hleg,'Location','EastOutside')
% % % % 
% % % % % Print Value
% % % % disp('')
% % % % disp(['Equivalent surface : ' num2str(mean(Area_Eq_01)) ])
% % % % disp(['(std : ' num2str(std(Area_Eq_01)) ')'])
% % % % disp('')
% % % %
fclose all;