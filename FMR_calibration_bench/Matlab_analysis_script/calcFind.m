function [meanfield] = calcFind(signalfield,index)

if (nargin<3)
    shifttime=0.04;
end

meanfield=zeros(size(signalfield,2),size(index,2)/2);
for n=1:size(signalfield,2)
    meanfield(n,1)=mean(signalfield(index(n,1):index(n,2),n));
    meanfield(n,2)=mean(signalfield(index(n,3):index(n,4),n));
end

