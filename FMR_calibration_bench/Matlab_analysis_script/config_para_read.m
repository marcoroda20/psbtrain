% extract the parameter value from the config.txt file
function [config_para]=config_para_read(path)

listing = dir(path);
index=1;
j=1;
for n=1:length(listing)
    if ((listing(n).isdir==0)&&(strcmp(listing(n).name,'config.txt')==1))   %find the config.txt file
        find_config = fopen(strcat(path,'/',listing(n).name));   %open config.txt file
        fseek(find_config,0,'eof');                              % position the index at the end of the file 
        EOF = ftell(find_config);                                % write the end file endex value
        fseek(find_config,0,'bof');                              % position the index at the begining of the file
        k=ftell(find_config);                                    % write in k the the index at the begining of the file => 0
        while(k<EOF)                                             % perform loop until the last index in the file  
            data_config = textscan(find_config,'%s %s','HeaderLines',4,'Delimiter',':');   % extract form the file the parameter names and its related values  the scan start after the 4 lines of Headlines then the separation between names andvalues are given by the ":" symbole 
            for i=1:length(data_config{1})                       % remove in the parameter name incompatibility for the Matlab Variable Name convention
                field_struct(i,1)=strrep(data_config{1}(i),' ','');  %remove space by empty
                field_struct(i,1)=strrep(field_struct(i,1),'#','');  %remove # by empty
                field_struct(i,1)=strrep(field_struct(i,1),'-','');   %remove - by empty
                field_struct(i,1)=strrep(field_struct(i,1),'[','');  %remove [ by empty
                field_struct(i,1)=strrep(field_struct(i,1),']','');   %remove ] by empty
                field_struct(i,1)=strrep(field_struct(i,1),'/','');   %remove / by empty  	
            end
            for i=1:length(data_config{1,2})                           % convert the char format value of the parameters given by the Matlab function textscan() by number format
                data_config{1,2}{i,1}=str2num(data_config{1,2}{i,1});
            end
            config_para=cell2struct(data_config{1,2},field_struct(:,1));  % convert the cells of parameter by a structure Configuration parameter.Parameter Name= parameter value
            k = ftell(find_config);
        end

       
        index=index+1;
        j=j+1;
        fclose(find_config);
    end
end