function [out,file_position]=dynread_3(fid,nline,file_position_start,sampleperchannel)

%load('stringfile.mat','str')
fseek(fid,file_position_start,'bof');
out=[];
h=waitbar(0,strcat('loading DAQ Channel datafiles...',num2str(0),'%'));  %create a loading bar
for n=1:nline
    dum=textscan(fid, '%f',(sampleperchannel+2),'delimiter', ',');  %240002
    if (size(dum{1},1)<(sampleperchannel+2))        %240002
        out=[out;out(n-2,:)];
    else
        out=[out;dum{1}'];
    end
    load_progress=(n./nline).*100;
    waitbar(load_progress./100,h,strcat('loading DAQ Channel datafile...',num2str(load_progress,'%6.0f'),'%'))  %update the loading bar
end
file_position=ftell(fid);
close(h);


% line=[];out=[];
% for n=1:20
%     tline = fgetl(fid);
%     m=1;count=0;
%     while (strcmp(tline(m),char(10))~=1)&&(strcmp(tline(m),char(13))~=1)
%         word=[];
%         while (strcmp(tline(m),char(44))~=1)&&...
%                 (strcmp(tline(m),char(10))~=1)&&...
%                 (strcmp(tline(m),char(13))~=1)
%             word=[word tline(m)];
%             m=m+1;
%         end
%         m=m+1;
%         line=[line str2num(word)];
%         count=count+1;
%         count
%     end
%     out=[out;line];
% end
% 
% fclose(fid);

