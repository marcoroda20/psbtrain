function [index] = findStable(signalfield,dt,shifttime)

if (nargin<3)
    shifttime=0.04;
end

sizeindex=4;   %used to create a matrix index (Ncycle,4) "4" corresponds to low plateau start index,low plateau end index, high plateau start index , hight plateau end index

index=zeros(size(signalfield,2),sizeindex);
meanfield=zeros(size(signalfield,2),size(index,2)/2);
signalfieldparse=diff(signalfield);
for n=1:size(signalfield,2)
    %find the start and stop index for first plate
    index(n,1)=1;
    index(n,2)=find(signalfieldparse(:,n)>max(signalfieldparse(:,n))/2,1,'first')-round(shifttime/dt);
    meanfield(n,1)=mean(signalfield(index(n,1):index(n,2),n));
    %find the start and stop index for second plate
    index(n,3)=size(signalfield,1)/2+1;
    index(n,4)=find(signalfieldparse(:,n)<min(signalfieldparse(:,n))/2,1,'first')-round(shifttime/dt);    
    meanfield(n,2)=mean(signalfield(index(n,3):index(n,4),n));
end

