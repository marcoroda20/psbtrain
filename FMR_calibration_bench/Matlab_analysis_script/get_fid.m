function [fid,file_position_start]=get_fid(path)
listing = dir(path);
index=1;
j=1;
for n=1:length(listing)
    if ((listing(n).isdir==0)&&(strcmp(lower(listing(n).name),'config.txt')~=1))
%         channel{index} = importdata([path '\' listing(n).name]);
        fid(j)=fopen([path '\' listing(n).name]);
        file_position_start(j) = ftell(fid(j));
        index=index+1;
        j=j+1;
        
    end
end