function freqs = read_fres(filename)

fid=fopen(filename);

fseek(fid,0,1);
EOF = ftell(fid);
fseek(fid,0,-1);
k=ftell(fid);
t_line=fgetl(fid);
while(k<EOF)
    if(strfind(t_line,'Field min : ')~=0)
        start_field = sscanf (t_line,'Field min : %f');
    elseif (strfind(t_line,'Field max : ')~=0)
        end_field = sscanf (t_line,'Field max : %f');
    elseif(strfind(t_line,'n pt freq')~=0)
        nptn = sscanf (t_line,'n pt freq :       %f');
    elseif(strfind(t_line,'TF [Hz/T]')~=0)
        freq_GHz = sscanf (t_line,'TF [Hz/T] : %f');
    end
    t_line=fgetl(fid);
    k = ftell(fid);
end
fclose (fid);
if nptn==1
    freqs=start_field*freq_GHz;
else
    freqs=linspace(start_field,end_field,nptn)*freq_GHz;
end

     