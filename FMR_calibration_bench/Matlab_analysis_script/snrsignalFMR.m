function [snr]=snrsignalFMR(dyn_cycle,dt)

FMR_dyn = dyn_cycle(1:floor(size(dyn_cycle,1)/2),:);

wint=0.01; % time window to search stable signal[s]

winpt=1000*round((wint/dt)/1000);
snr=zeros(1,size(FMR_dyn,2));
for n=1:size(FMR_dyn,2)
    block=reshape(FMR_dyn(:,n),winpt,...
        round(length(FMR_dyn(:,n))/winpt));
    V =var(block,1);
    snoise=[];
    for m=1:size(block,2)
        if V(m)~=max(V)
            snoise=[snoise;block(:,m)];
        end
    end
    noise=-1*mean(snoise);
    npt=size(FMR_dyn,1);
    signal=max(FMR_dyn(round(0.1*npt):round(0.9*npt),n))-...
        min(FMR_dyn(round(0.1*npt):round(0.9*npt),n));
    snr(n)=20*log10(signal/noise);
end


