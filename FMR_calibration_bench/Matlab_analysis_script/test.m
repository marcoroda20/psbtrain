
Tf_magnet=9.50529;
Ramp_rate=(0.5:0.5:2.5); % T/s
W=0.03;
L = 0.036;
t = 0.001; %Alu case
%t = 0.0001; % alu sticker
rho = 2.82e-8; %aluminium
% Bdot = Ramp_rate*Tf_magnet/10000;
Bdot = Ramp_rate;
mu = 1.2567e-6; %alu case
Ae = W*L
% Re =rho*2*((L+W)/(t*W))     %alu sticker
Re =rho*(2*(L+W)/(t*W))	 %alu case
Ie =Bdot*W*L/Re;
Be = Ie*mu/(L+W);
Phi_e = Be*Ae;
Le = Phi_e./Ie
tau = Le./Re


plot(Ramp_rate,Be,'r')


% Ae = W*L/2
% Re =rho*(2*(L+W)/(t*W)) %alu case
% Be = Ie*mu*2/(L+W);

