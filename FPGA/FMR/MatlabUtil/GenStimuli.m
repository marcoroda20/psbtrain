
pathname = 'C:\PSBTrain\FPGA\FMR\spec_peak_detector\testbench';
filename1 = 'adc1.sti';
filename2 = 'adc2.sti';

f = 1e3;
fe = 10e6;
time = 5e-3;
nbit = 16;
percentutil = 0.8;
phi=pi/4;
rangemax = 2^(nbit-1)-1;
rangemin = -1*2^(nbit-1);

t=(0:1/fe:time);
adc1_signal = floor(percentutil*rangemax*sin(2*pi*f*t));
adc2_signal = floor(percentutil*rangemax*sin(2*pi*f*t+phi));

struct.mode = 'fixed'; 
struct.roundmode = 'floor'; 
struct.overflowmode = 'saturate'; 
struct.format = [16 0]; 
q = quantizer(struct);
% adc1_signalbin = num2bin(q,adc1_signal);
% adc2_signalbin = num2bin(q,adc2_signal);

fid1 = fopen([pathname '\' filename1],'w');
fid2 = fopen([pathname '\' filename2],'w');
% fprintf(fid1,'ADC1\n');
% fprintf(fid2,'ADC2\n');
for n=1:length(adc1_signal)
%     fprintf(fid1,[adc1_signalbin(n,:) '\n']);    
    fprintf(fid1,'%d\n',adc1_signal(n));    
end
for n=1:length(adc2_signal)
%     fprintf(fid2,[adc2_signalbin(n,:) '\n']);    
    fprintf(fid2,'%d\n',adc2_signal(n));    
end

dadc1_signal = diff(adc1_signal);
dadc2_signal = diff(adc2_signal);
figure('name','2 canaux');
plot(t,adc1_signal,t,adc2_signal)
grid on
figure('name','Canal 1');
plot(t,adc1_signal,t(2:end),100*dadc1_signal)
grid on

fclose(fid1);
fclose(fid2);
