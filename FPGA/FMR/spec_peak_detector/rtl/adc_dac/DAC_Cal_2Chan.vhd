----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC/MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    10:47:44 02/14/2013 
-- Design Name: 
-- Module Name:    DAC_Cal_2Chan - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.peak_detector_pkg.all;

entity DAC_Cal_2Chan is
    port ( 
		clk_i : in std_logic;
		reset_i : in std_logic;
		
		load_1_i				: in std_logic;
		DAC_prog_data_1_i	: in std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
		PADC_data_1_i 		: in std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
		Diff_ADC_1_i		: in std_logic_vector(NB_BIT_DIFF_DATA_BUS-1 downto 0);
		Threshold_1_i		: in std_logic_vector(NB_BIT_THRESHOLD-1 downto 0);
		DAC_sel_1_i			: in std_logic_vector(NB_BIT_DAC_SEL-1 downto 0);
		load_1_o				: out std_logic;
		DAC_data_1_o		: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);

		load_2_i				: in std_logic;
		DAC_prog_data_2_i	: in std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
		PADC_data_2_i 		: in std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
		Diff_ADC_2_i		: in std_logic_vector(NB_BIT_DIFF_DATA_BUS-1 downto 0);
		Threshold_2_i		: in std_logic_vector(NB_BIT_THRESHOLD-1 downto 0);
		DAC_sel_2_i			: in std_logic_vector(NB_BIT_DAC_SEL-1 downto 0);
		load_2_o				: out std_logic;
		DAC_data_2_o		: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0)
	);		  
end DAC_Cal_2Chan;

architecture Behavioral of DAC_Cal_2Chan is

--Signal
signal Threshold_1_s		: std_logic_vector(NB_BIT_THRESHOLD-1 downto 0);
signal Threshold_2_s		: std_logic_vector(NB_BIT_THRESHOLD-1 downto 0);

signal DAC_prog_data_1_s	: std_logic_vector (NB_BIT_ADC_DATA_BUS-1 downto 0);
signal PADC_DATA_1_s			: std_logic_vector (NB_BIT_ADC_DATA_BUS-1 downto 0);
signal DIFF_ADC_1_s			: std_logic_vector (NB_BIT_DIFF_DATA_BUS-1 downto 0);

signal dac_data_1_s			: std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);

signal DAC_prog_data_2_s	: std_logic_vector (NB_BIT_ADC_DATA_BUS-1 downto 0);
signal PADC_DATA_2_s			: std_logic_vector (NB_BIT_ADC_DATA_BUS-1 downto 0);
signal DIFF_ADC_2_s			: std_logic_vector (NB_BIT_DIFF_DATA_BUS-1 downto 0);

signal dac_data_2_s			: std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);

begin

	--Threshold strait binary for the DAC
	Threshold_1_s <= not Threshold_1_i(Threshold_1_i'length-1) & 
							Threshold_1_i(Threshold_1_i'length-2 downto 0);
	Threshold_2_s <= not Threshold_2_i(Threshold_2_i'length-1) & 
							Threshold_2_i(Threshold_2_i'length-2 downto 0);

	--Input latches
	process (clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i = '1') then
				DAC_prog_data_1_s	<= (others=>'0');
				PADC_DATA_1_s		<= (others=>'0');
				DIFF_ADC_1_s		<= (others=>'0');
				DAC_prog_data_2_s	<= (others=>'0');
				PADC_DATA_2_s		<= (others=>'0');
				DIFF_ADC_2_s		<= (others=>'0');
			else
				if (load_1_i='1') then
					DAC_prog_data_1_s	<= DAC_prog_data_1_i;
					PADC_DATA_1_s		<= PADC_data_1_i;
					DIFF_ADC_1_s		<= Diff_ADC_1_i;
				end if;
				if (load_2_i='1') then
					DAC_prog_data_2_s	<= DAC_prog_data_2_i;
					PADC_DATA_2_s		<= PADC_data_2_i;
					DIFF_ADC_2_s		<= Diff_ADC_2_i;
				end if;
			end if;
		end if;
	end process;

	process (clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i = '1') then
				dac_data_1_s <= (others=>'0');
				dac_data_2_s <= (others=>'0');
			else
				--Selection for DAC1
				if (DAC_sel_1_i(2) = '0') then
					case DAC_sel_1_i(1 downto 0) is
						when "00" =>
							dac_data_1_s	<= Diff_ADC_1_s;
						when "01" =>
							dac_data_1_s	<= PADC_data_1_s;
						when "10" =>
							dac_data_1_s	<= DAC_prog_data_1_s;
						when "11" =>
							dac_data_1_s	<= Threshold_1_s;
						when others =>
							dac_data_1_s	<= Diff_ADC_1_s;
					end case;
				else
					case DAC_sel_1_i(1 downto 0) is
						when "00" =>
							dac_data_1_s	<= Diff_ADC_2_s;
						when "01" =>
							dac_data_1_s	<= PADC_data_2_s;
						when "10" =>
							dac_data_1_s	<= DAC_prog_data_2_s;
						when "11" =>
							dac_data_1_s	<= Threshold_2_s;
						when others =>
							dac_data_1_s	<= Diff_ADC_2_s;
					end case;
				end if;
				--Selection for DAC2
				if (DAC_sel_2_i(2) = '0') then
					case DAC_sel_2_i(1 downto 0) is
						when "00" =>
							dac_data_2_s	<= Diff_ADC_1_s;
						when "01" =>
							dac_data_2_s	<= PADC_data_1_s;
						when "10" =>
							dac_data_2_s	<= DAC_prog_data_1_s;
						when "11" =>
							dac_data_2_s	<= Threshold_1_s;
						when others =>
							dac_data_2_s	<= Diff_ADC_1_s;
					end case;
				else
					case DAC_sel_2_i(1 downto 0) is
						when "00" =>
							dac_data_2_s	<= Diff_ADC_2_s;
						when "01" =>
							dac_data_2_s	<= PADC_data_2_s;
						when "10" =>
							dac_data_2_s	<= DAC_prog_data_2_s;
						when "11" =>
							dac_data_2_s	<= Threshold_2_s;
						when others =>
							dac_data_2_s	<= Diff_ADC_2_s;
					end case;
				end if;
			end if;
		end if;
	end process;
	
	--Output latches
	process (clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i = '1') then
				DAC_data_1_o	<= (others=>'0');
				load_1_o		<= '0';
				DAC_data_2_o	<= (others=>'0');
				load_2_o		<= '0';
			else
				DAC_data_1_o	<= dac_data_1_s;
				load_1_o			<= load_1_i;
				DAC_data_2_o	<= dac_data_2_s;
				load_2_o			<= load_2_i;
			end if;
		end if;
	end process;
	
end Behavioral;

