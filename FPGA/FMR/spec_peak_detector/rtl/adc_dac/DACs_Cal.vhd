----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC/MM
-- Engineer: 		David Giloteaux
-- 
-- Create Date:    14:18:28 07/06/2011 
-- Design Name: 
-- Module Name:    DACs_Cal - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

use work.CfgPeakDetector.all;

entity DACs_Cal is
    port ( 
		clk_i : in std_logic;
		reset_i : in std_logic;
		
		load_i				: in std_logic;
		DAC_prog_data_i	: in std_logic_vector (NB_BIT_ADC_DATA_BUS-1 downto 0);
		PADC_data_i 		: in std_logic_vector (NB_BIT_ADC_DATA_BUS-1 downto 0);
		Diff_ADC_i			: in std_logic_vector (NB_BIT_DIFF_DATA_BUS-1 downto 0);
		DAC_sel_i			: in std_logic_vector(NB_BIT_DAC_SEL-1 downto 0);
		load_o				: out std_logic;
		DAC_data_o			: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0)
	);		  
end DACs_Cal;

architecture Behavioral of DACs_Cal is

signal DAC_prog_data_s	: std_logic_vector (NB_BIT_ADC_DATA_BUS-1 downto 0);
signal PADC_DATA_s		: std_logic_vector (NB_BIT_ADC_DATA_BUS-1 downto 0);
signal DIFF_ADC_s			: std_logic_vector (NB_BIT_DIFF_DATA_BUS-1 downto 0);

signal dac_data_s			: std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);

begin

	--Input latch
	process (clk_i,reset_i,DAC_prog_data_i,PADC_data_i,
			Diff_ADC_i)
	begin
		if (reset_i = '1') then
			DAC_prog_data_s	<= (others=>'0');
			PADC_DATA_s			<= (others=>'0');
			DIFF_ADC_s			<= (others=>'0');
		elsif (clk_i'event and clk_i = '1') then
			if (load_i='1') then
				DAC_prog_data_s	<= DAC_prog_data_i;
				PADC_DATA_s			<= PADC_data_i;
				DIFF_ADC_s			<= Diff_ADC_i;
			end if;
		end if;
	end process;

	process (clk_i,reset_i,DAC_prog_data_s,PADC_data_s,
			Diff_ADC_s,DAC_sel_i)
	begin
		if (reset_i = '1') then
			dac_data_s <= (others=>'0');
		elsif (clk_i'event and clk_i = '1') then
			if DAC_sel_i(2)='0' then 
				case DAC_sel_i(1 downto 0) is
					when "11" =>
						dac_data_s	<= DAC_prog_data_s;
					when "01" =>
						dac_data_s	<= x"FFFF";
					when "10" =>
						dac_data_s	<= x"0000";
					when others =>
						dac_data_s	<= PADC_data_s;
				end case;
			else
				case DAC_sel_i(1 downto 0) is
					when "11" =>
						dac_data_s	<= DAC_prog_data_s;
					when "01" =>
						dac_data_s	<= x"FFFF";
					when "10" =>
						dac_data_s	<= x"0000";
					when others =>
						dac_data_s	<= Diff_ADC_s;
				end case;
			end if;
		end if;
	end process;
	
	--Output latch
	process (clk_i,reset_i,dac_data_s)
	begin
		if (reset_i = '1') then
			DAC_data_o	<= (others=>'0');
			load_o		<= '0';
		elsif (clk_i'event and clk_i = '1') then
			DAC_data_o	<= dac_data_s;
			load_o		<= load_i;
		end if;
	end process;
	
end Behavioral;

