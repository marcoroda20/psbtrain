----------------------------------------------------------------------------------
-- Company: 		CERN/TE-MS-MM
-- Engineer: 		Daniel Oberson
-- 
-- Description: Package for ADC and DAC for PeakDetector
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

library work;
use work.peak_detector_pkg.all;

package adc_dac_pkg is

	component AD7626sm
		Port ( 
		clk_500_i 	: in std_logic;
		clk_250_i 	: in std_logic;
		reset_i		: in std_logic;

		delay_clk_i : std_logic_vector(3 downto 0);

		DCO_1_i		: in  std_logic;
		D_1_i			: in  std_logic;

		DCO_2_i		: in  std_logic;
		D_2_i			: in  std_logic;

		adc_clk_o	: out  std_logic;
		adc_cnv_o	: out  std_logic;

		DAC_data_1_o		: out  std_logic_vector(15 downto 0);
		ADC_data_1_o		: out  std_logic_vector(15 downto 0);
		data_rdy_1_o		: out  std_logic;

		DAC_data_2_o		: out  std_logic_vector(15 downto 0);
		ADC_data_2_o		: out  std_logic_vector(15 downto 0);
		data_rdy_2_o		: out  std_logic
		);		  
	end component;

	component DAC_Cal_2Chan 
		 port ( 
			clk_i : in std_logic;
			reset_i : in std_logic;
			
			load_1_i				: in std_logic;
			DAC_prog_data_1_i	: in std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
			PADC_data_1_i 		: in std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
			Diff_ADC_1_i		: in std_logic_vector(NB_BIT_DIFF_DATA_BUS-1 downto 0);
			Threshold_1_i		: in std_logic_vector(NB_BIT_THRESHOLD-1 downto 0);
			DAC_sel_1_i			: in std_logic_vector(NB_BIT_DAC_SEL-1 downto 0);
			load_1_o				: out std_logic;
			DAC_data_1_o		: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);

			load_2_i				: in std_logic;
			DAC_prog_data_2_i	: in std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
			PADC_data_2_i 		: in std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
			Diff_ADC_2_i		: in std_logic_vector(NB_BIT_DIFF_DATA_BUS-1 downto 0);
			Threshold_2_i		: in std_logic_vector(NB_BIT_THRESHOLD-1 downto 0);
			DAC_sel_2_i			: in std_logic_vector(NB_BIT_DAC_SEL-1 downto 0);
			load_2_o				: out std_logic;
			DAC_data_2_o		: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0)
		);		  
	end component;

	component DAC_mux_output 
		port(
			clk_50MHz_i			: in std_logic;
			reset_i		: in std_logic;
			
			DAC1_data_bus_i	: in std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
			DAC1_load_i			: in std_logic;
			DAC2_data_bus_i	: in std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
			DAC2_load_i			: in std_logic;
			
			DAC_data_bus_o		: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
			load1_o				: out std_logic;
			load2_o				: out std_logic
		);
	end component;

end adc_dac_pkg;

