----------------------------------------------------------------------------------
-- Company:      CERN TE/MSC/MM
-- Engineer:     Daniel Oberson
-- 
-- Create Date:    10:16:57 11/15/2013 
-- Design Name: 
-- Module Name:    acqu_ddr_ctrl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

library work;
use work.genram_pkg.all;
use work.utils_pkg.all;

entity acqu_ddr_ctrl is
	generic(
		g_sim : integer := 0;
		g_WORD_LENGTH : integer := 64;
		g_DATA_LENGTH : integer := 64
	);
	port(
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		--Control
		samples_wr_en_i : in std_logic;
		ddr_test_en_i   : in std_logic;
		ddr_range_i     : in std_logic_vector(24 downto 0);	
		last_addr_o     : out std_logic_vector(24 downto 0);	
		
		--Data
		sync_data_i       : in std_logic_vector(g_DATA_LENGTH-1 downto 0);
		sync_data_valid_i : in std_logic;
		C0_i              : in std_logic;

		-- DDR wishbone interface
		wb_ddr_clk_i   : in  std_logic;
		wb_ddr_adr_o   : out std_logic_vector(31 downto 0);
		wb_ddr_dat_o   : out std_logic_vector(63 downto 0);
		wb_ddr_sel_o   : out std_logic_vector(7 downto 0);
		wb_ddr_stb_o   : out std_logic;
		wb_ddr_we_o    : out std_logic;
		wb_ddr_cyc_o   : out std_logic;
		wb_ddr_ack_i   : in  std_logic;
		wb_ddr_stall_i : in  std_logic;
		
		--To test
		ram_addr_o : out std_logic_vector(13 downto 0);
		ram_data_o : out std_logic_vector(31 downto 0);
		ram_wr_o   : out std_logic;

		mem_range_o		: out std_logic;
		acqu_end_o     : out std_logic_vector(1 downto 0)
	);
end acqu_ddr_ctrl;

architecture Behavioral of acqu_ddr_ctrl is

	--Constant
	constant c_DDR_MEMORY_RANGE_SIM : integer := 16;--28;--64;--32;
	constant c_MAX_FIFO_DATA_COUNT  : integer := g_DATA_LENGTH/g_WORD_LENGTH;--log2_ceil(sync_data_i'length/g_WORD_LENGTH);

	--Type declaration
	type wr_fifo_states_t is (
		IDLE,
		LATCH_IN,
		WRITE_WORD,
		WAIT_END_WRITE
	);
	type word_array_t is array (log2_ceil(g_DATA_LENGTH/g_WORD_LENGTH) downto 0) of 
				std_logic_vector(g_WORD_LENGTH-1 downto 0);

	--Signal
	signal state_s : wr_fifo_states_t;
	signal fifo_data_cnt_s  : unsigned(log2_ceil(g_DATA_LENGTH/g_WORD_LENGTH) downto 0);

	signal data_in_s   : std_logic_vector(sync_data_i'length-1 downto 0);
	signal data_word_s : std_logic_vector(g_WORD_LENGTH-1 downto 0);
	signal word_s      : word_array_t;
	signal data_rdy_s  : std_logic;
	
	signal switch_mem_range_s : std_logic;
	signal acqu_end_s         : std_logic;
	signal acqu_end_p         : std_logic;
	signal acqu_end_out_s     : std_logic;
	signal C0_s               : std_logic;
	signal C0_p_s             : std_logic;
	--signal C0_clear_s         : std_logic;
	
	signal sync_data_valid_s : std_logic;

	signal ram_addr_cnt_s : unsigned(24 downto 0);

	signal wb_ddr_stall_t : std_logic;
	
	signal rst_n_s : std_logic;
	
	signal switch_test_val_s : std_logic;

	-- Wishbone to DDR flowcontrol FIFO
	signal wb_ddr_fifo_din     : std_logic_vector(63 downto 0);
	signal wb_ddr_fifo_dout    : std_logic_vector(63 downto 0);
	signal wb_ddr_fifo_empty   : std_logic;
	signal wb_ddr_fifo_full    : std_logic;
	signal wb_ddr_fifo_wr      : std_logic;
	signal wb_ddr_fifo_wr_q    : std_logic;
	signal wb_ddr_fifo_rd      : std_logic;
	signal wb_ddr_fifo_valid   : std_logic;
	signal wb_ddr_fifo_valid_1 : std_logic;
	signal wb_ddr_fifo_dreq    : std_logic;
	signal wb_ddr_fifo_wr_en   : std_logic;
	
	signal wb_ddr_stb_s : std_logic := '0';
	signal incr_s       : std_logic := '0';

	signal wb_ddr_ack_s : std_logic := '0';
	signal wb_ddr_ack_end_s : std_logic := '0';

	--Test
	signal wb_ddr_fifo_rd_r : std_logic;
	signal wb_ddr_cyc_s     : std_logic;
	
	signal ram_wr_s   : std_logic := '0';
	signal ram_wr_d   : std_logic := '0';
	signal one_shot_s : std_logic := '0';
	
begin

	--State machine to write by word to the fifo
	process(clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				state_s <= IDLE;
				fifo_data_cnt_s <= (others=>'0');
				data_in_s <= (others=>'0');
				data_word_s <= (others=>'0');
				data_rdy_s <= '0';
			else
				data_rdy_s <= '0';
				case state_s is
					when IDLE =>
						if (sync_data_valid_s='1' and samples_wr_en_i='1') then
							state_s <= LATCH_IN;
						end if;
						fifo_data_cnt_s <= (others=>'0');
					when LATCH_IN =>
						state_s <= WRITE_WORD;
						data_in_s <= sync_data_i;
					when WRITE_WORD =>
						state_s <= WAIT_END_WRITE;
						fifo_data_cnt_s <= fifo_data_cnt_s + 1;
						data_word_s <= data_in_s((to_integer(c_MAX_FIFO_DATA_COUNT-fifo_data_cnt_s-1)+1)*g_WORD_LENGTH-1
										downto to_integer(c_MAX_FIFO_DATA_COUNT-fifo_data_cnt_s-1)*g_WORD_LENGTH);										
						data_rdy_s <= '1';				
					when WAIT_END_WRITE =>
						if (wb_ddr_fifo_wr_q='1' and wb_ddr_fifo_wr='0') then
							if (fifo_data_cnt_s<unsigned(to_signed(c_MAX_FIFO_DATA_COUNT,fifo_data_cnt_s'length))) then
								state_s <= WRITE_WORD;
							else
								state_s <= IDLE;
							end if;
						end if;
				end case;
			end if;		
		end if;
	end process;

	rst_n_s <= not reset_i;
  ------------------------------------------------------------------------------
  -- Flow control FIFO for data to DDR
  ------------------------------------------------------------------------------
  cmp_wb_ddr_fifo : generic_sync_fifo
    generic map (
      g_data_width             => 64,
      g_size                   => 64,
      g_show_ahead             => false,
      g_with_empty             => true,
      g_with_full              => true,
      g_with_almost_empty      => false,
      g_with_almost_full       => false,
      g_with_count             => false,
      g_almost_empty_threshold => 0,
      g_almost_full_threshold  => 0
      )
    port map(
      rst_n_i        => rst_n_s,
      clk_i          => clk_i,
      d_i            => wb_ddr_fifo_din,
      we_i           => wb_ddr_fifo_wr,
      q_o            => wb_ddr_fifo_dout,
      rd_i           => wb_ddr_fifo_rd,
      empty_o        => wb_ddr_fifo_empty,
      full_o         => wb_ddr_fifo_full,
      almost_empty_o => open,
      almost_full_o  => open,
      count_o        => open
      );

  -- One clock cycle delay for the FIFO's VALID signal. Since the General Cores
  -- package does not offer the possibility to use the FWFT feature of the FIFOs,
  -- we simulate the valid flag here according to Figure 4-7 in ref. [1].
	--Data input pulse
  p_wb_ddr_fifo_valid : process (clk_i) 
  begin
    if rising_edge(clk_i) then
		 if reset_i = '1' then
			wb_ddr_fifo_valid   <= '0';
			wb_ddr_fifo_valid_1 <= '0';
			wb_ddr_fifo_rd_r    <= '0';
		 else
			if (wb_ddr_fifo_rd='1' or wb_ddr_fifo_rd_r='1') then 
				wb_ddr_fifo_valid <= '1';                     
			else                                        
				wb_ddr_fifo_valid <= '0';                  
			end if;                                      
			wb_ddr_fifo_valid_1 <= wb_ddr_fifo_valid;
			if (wb_ddr_fifo_empty = '1') then
			  wb_ddr_fifo_valid <= '0';
			end if;
			if (wb_ddr_fifo_rd='1' and wb_ddr_fifo_rd_r='0' and wb_ddr_ack_i='0') then --to test
				wb_ddr_fifo_rd_r <= '1';                            
			elsif (wb_ddr_ack_i='1') then                         
				wb_ddr_fifo_rd_r <= '0';                    
			end if;                                        
		end if;
    end if;
  end process;

	--Data input pulse
	cmp_sync_data_valid : RisingDetect 
		port map(
			clk_i		=> wb_ddr_clk_i,
			reset_i	=> reset_i,
			
			s_i	=> sync_data_valid_i,
			s_o	=> sync_data_valid_s
		);

--	--Data input pulse
--	cmp_ack_end : RisingDetect 
--		port map(
--			clk_i		=> wb_ddr_clk_i,
--			reset_i	=> reset_i,
--			
--			s_i	=> wb_ddr_ack_s,
--			s_o	=> wb_ddr_ack_end_s
--		);
--  wb_ddr_ack_s <= not wb_ddr_ack_i;
  
  p_wb_ddr_fifo_input : process (clk_i, reset_i)
  begin
    if rising_edge(clk_i) then
		 if reset_i = '1' then
			wb_ddr_fifo_din   <= (others => '0');
			wb_ddr_fifo_wr_en <= '0';
			wb_ddr_fifo_wr_q  <= '0';
		 else
--			wb_ddr_fifo_din   <= sync_data_i; 
			wb_ddr_fifo_din   <= data_word_s; 
--			wb_ddr_fifo_wr_en <= samples_wr_en_i and sync_data_valid_s;
			wb_ddr_fifo_wr_en <= samples_wr_en_i and data_rdy_s;
			wb_ddr_fifo_wr_q  <= wb_ddr_fifo_wr;
		 end if;
    end if;
  end process p_wb_ddr_fifo_input;
  wb_ddr_fifo_wr <= wb_ddr_fifo_wr_en and not(wb_ddr_fifo_full);

  wb_ddr_fifo_rd   <= wb_ddr_fifo_dreq and not(wb_ddr_fifo_empty) and 
								not(wb_ddr_stall_t) and not(wb_ddr_fifo_rd_r) and not(wb_ddr_cyc_s);
  wb_ddr_fifo_dreq <= '1';

  ------------------------------------------------------------------------------
  -- RAM address counter (32-bit word address)
  ------------------------------------------------------------------------------
  p_ram_addr_cnt : process (wb_ddr_clk_i, reset_i)
  begin
    if rising_edge(wb_ddr_clk_i) then
		 if reset_i = '1' then
			ram_addr_cnt_s <= (others => '0');
			switch_mem_range_s <= '0';
			acqu_end_s <= '0';
			switch_test_val_s <= '0';
			C0_p_s <='0';
			--C0_clear_s <= '0';
			last_addr_o <= (others => '0');
		 else			
			if (C0_s='1') then
				C0_p_s <='1';
			elsif ((wb_ddr_fifo_rd = '1') and (C0_p_s = '1')) then
				C0_p_s <='0';
			end if;
			if (switch_mem_range_s='0') then
--				--TO TEST
--				if (ram_addr_cnt_s < (unsigned(ddr_range_i)/2)) then
--					C0_p_s <='0';
--				else
--					C0_p_s <='1';
--				end if;
--				--END TEST
				if (wb_ddr_fifo_rd = '1') then
					if (g_sim=0) then
						if (  (ram_addr_cnt_s >= unsigned(to_signed(0,ram_addr_cnt_s'length))) and 
								(ram_addr_cnt_s < (unsigned(ddr_range_i)-1)) and 
								(C0_p_s='0') )  then
							ram_addr_cnt_s <= ram_addr_cnt_s + 1;
							acqu_end_s <= '0';
							--C0_clear_s <='0';
						else
							ram_addr_cnt_s <= unsigned(ddr_range_i);
							switch_mem_range_s <= '1';
							acqu_end_s <= '1';
							--C0_clear_s <='1';
							--C0_p_s <='0';
							last_addr_o <= std_logic_vector(ram_addr_cnt_s);
						end if;
					else
						if (  (ram_addr_cnt_s >= unsigned(to_signed(0,ram_addr_cnt_s'length))) and 
								(ram_addr_cnt_s < unsigned(to_signed(c_DDR_MEMORY_RANGE_SIM-1,ram_addr_cnt_s'length))) and
								(C0_p_s='0') )  then
							ram_addr_cnt_s <= ram_addr_cnt_s + 1;
							acqu_end_s <= '0';
							--C0_clear_s <='0';
						else
							ram_addr_cnt_s <= unsigned(to_signed(c_DDR_MEMORY_RANGE_SIM,ram_addr_cnt_s'length));
							switch_mem_range_s <= '1';
							acqu_end_s <= '1';
							--C0_clear_s <='1';
							--C0_p_s <='0';
							last_addr_o <= std_logic_vector(ram_addr_cnt_s);
						end if;
					end if;
				end if;
			else
--				--TO TEST
--				if (ram_addr_cnt_s < (2*unsigned(ddr_range_i)-unsigned(ddr_range_i)/2)) then
--					C0_p_s <='0';
--				else
--					C0_p_s <='1';
--				end if;
--				--END TEST
				if (wb_ddr_fifo_rd = '1') then
					if (g_sim=0) then
						if (  (ram_addr_cnt_s >= unsigned(ddr_range_i)) and
								(ram_addr_cnt_s < (2*unsigned(ddr_range_i)-1)) and
								(C0_p_s='0') )  then
							ram_addr_cnt_s <= ram_addr_cnt_s + 1;
							acqu_end_s <= '0';
							--C0_clear_s <='0';
						else
							ram_addr_cnt_s <= (others => '0');
							switch_mem_range_s <= '0';
							switch_test_val_s <= not switch_test_val_s;
							acqu_end_s <= '1';
							--C0_clear_s <='1';
							--C0_p_s <='0';
							last_addr_o <= std_logic_vector(ram_addr_cnt_s-unsigned(ddr_range_i));
						end if;
					else
						if (  (ram_addr_cnt_s >= unsigned(to_signed(c_DDR_MEMORY_RANGE_SIM,ram_addr_cnt_s'length))) and
								(ram_addr_cnt_s < unsigned(to_signed(2*c_DDR_MEMORY_RANGE_SIM-1,ram_addr_cnt_s'length))) and 
								(C0_p_s='0') ) then
							ram_addr_cnt_s <= ram_addr_cnt_s + 1;
							acqu_end_s <= '0';
							--C0_clear_s <='0';
						else
							ram_addr_cnt_s <= (others => '0');
							switch_mem_range_s <= '0';
							switch_test_val_s <= not switch_test_val_s;
							acqu_end_s <= '1';
							--C0_clear_s <='1';
							--C0_p_s <='0';
							last_addr_o <= std_logic_vector(ram_addr_cnt_s-unsigned(ddr_range_i));
						end if;
					end if;
				end if;
			end if;		
		 end if;
    end if;
  end process p_ram_addr_cnt;
  mem_range_o <= switch_mem_range_s;
  
  ------------------------------------------------------------------------------
  -- Wishbone master (to DDR)
  ------------------------------------------------------------------------------
  p_wb_master : process (wb_ddr_clk_i, reset_i)
  begin
    if rising_edge(wb_ddr_clk_i) then
		 if reset_i = '1' then
			wb_ddr_cyc_s   <= '0'; -- to test
			wb_ddr_cyc_o   <= '0';
			wb_ddr_we_o    <= '0';
			wb_ddr_stb_o   <= '0';
			wb_ddr_adr_o   <= (others => '0');
			wb_ddr_dat_o   <= (others => '0');
			wb_ddr_stall_t <= '0';
			wb_ddr_stb_s   <= '0';
			ram_addr_o     <= (others => '0');
			ram_data_o     <= (others => '0');
			ram_wr_s       <= '0';--to test
			ram_wr_d       <= '0';--to test
		 else
			if (wb_ddr_fifo_rd = '1') then
			   wb_ddr_stb_o <= '1';
				wb_ddr_stb_s <= '1';
			   wb_ddr_adr_o <= "0000000" & std_logic_vector(ram_addr_cnt_s);
				ram_addr_o   <= std_logic_vector(ram_addr_cnt_s(ram_addr_o'length-1 downto 0));--to test
			   --To check 
				if ddr_test_en_i = '1' then
					if switch_test_val_s='0' then
						wb_ddr_dat_o <= x"00000000" & "0000000" & std_logic_vector(ram_addr_cnt_s);
						ram_data_o   <= "0000000" & std_logic_vector(ram_addr_cnt_s);--to test
					else
						wb_ddr_dat_o <= x"000000000" & "00" & std_logic_vector(unsigned(ddr_range_i & '0')-ram_addr_cnt_s);
						ram_data_o   <= "000000" & std_logic_vector(unsigned(ddr_range_i & '0')-ram_addr_cnt_s);--to test
					end if;
				else
					wb_ddr_dat_o <= wb_ddr_fifo_dout;
					ram_data_o   <= wb_ddr_fifo_dout(31 downto 0);
				end if;
			else
				wb_ddr_stb_o <= '0';
				wb_ddr_stb_s <= '0';
			end if;

			--To test
			ram_wr_d <= ram_wr_s;--to test
			ram_wr_o <= ram_wr_d;--to test
			
			if (wb_ddr_fifo_rd = '1' and ram_wr_s='0') then
				ram_wr_s <= '1';
			else
				ram_wr_s <= '0';
			end if;

			if ((wb_ddr_fifo_rd = '1' or wb_ddr_fifo_rd_r = '1') and not(wb_ddr_ack_i='1')) then 
				wb_ddr_cyc_s <= '1';                                                        
			else                                                                            
				wb_ddr_cyc_s <= '0';                                                     
			end if;                                                                  
			
			if (wb_ddr_fifo_rd = '1') then 
			  wb_ddr_cyc_o <= '1';
			  wb_ddr_we_o  <= '1';
			else
			  wb_ddr_cyc_o <= '0';
			  wb_ddr_we_o  <= '0';
			end if;

			wb_ddr_stall_t <= wb_ddr_stall_i;

		 end if;
    end if;
  end process p_wb_master;
  
  wb_ddr_sel_o <= X"FF";

	--End acqu pulse
	cmp_acqu_end : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> acqu_end_s,
			s_o	=> acqu_end_p
		);

	--C0 input pulse
	cmp_C0 : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> C0_i,
			s_o	=> C0_s
		);

	cmp_monostable_acq_end : monostable
	generic map(
		g_n_clk => 10
	)
	port map(
		clk_i		=> wb_ddr_clk_i,
		reset_i	=> reset_i,
		
		s_i => acqu_end_p,
		s_o => acqu_end_out_s
	);
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				acqu_end_o <= (others=>'0');
			else
				if (acqu_end_out_s='1') then
					if (switch_mem_range_s='1') then
						acqu_end_o <= "01";
					else
						acqu_end_o <= "10";
					end if;
				else
					acqu_end_o <= (others=>'0');
				end if;
			end if;
		end if;
	end process;
	
end Behavioral;

