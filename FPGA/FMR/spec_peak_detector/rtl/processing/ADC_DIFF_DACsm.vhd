----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC/MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    11:20:34 10/04/2012 
-- Design Name: 
-- Module Name:    ADC_DIFF_DACsm - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.peak_detector_pkg.all;
use work.adc_dac_pkg.all;
use work.utils_pkg.all;
use work.processing_pkg.all;

entity ADC_DIFF_DACsm is
	port(
		clk_20MHz_i		: in std_logic;
		clk_50MHz_i		: in std_logic;
		clk_500MHz_i	: in std_logic;
		clk_250MHz_i	: in std_logic;
		
		DCM_lock_i		: in std_logic;

		reset_i			: in std_logic;

		delay_clk_i : std_logic_vector(3 downto 0);

		filter_on_i  : in std_logic;
      diff_cfg_1_i : in std_logic;
      diff_cfg_2_i : in std_logic;

		ADC_DCO_1_i		: in std_logic;
		ADC_D_1_i		: in std_logic;
		
		ADC_data_1_o		: out std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
		DIFF_data_1_o		: out std_logic_vector(NB_BIT_DIFF_DATA_BUS-1 downto 0);
		ADC_data_rdy_1_o	: out std_logic;
		
		ADC_DCO_2_i		: in std_logic;
		ADC_D_2_i		: in std_logic;
		
		ADC_data_2_o		: out std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
		DIFF_data_2_o		: out std_logic_vector(NB_BIT_DIFF_DATA_BUS-1 downto 0);
		ADC_data_rdy_2_o	: out std_logic;
				
		ADC_CNV_o		: out std_logic;
		ADC_CLK_o		: out std_logic;
		
		threshold_1_i	: in std_logic_vector(NB_BIT_THRESHOLD-1 downto 0);
		threshold_2_i	: in std_logic_vector(NB_BIT_THRESHOLD-1 downto 0);

		DAC_sel_1_i		: in std_logic_vector(NB_BIT_DAC_SEL-1 downto 0);
		DAC_sel_2_i		: in std_logic_vector(NB_BIT_DAC_SEL-1 downto 0);
		
		DAC_prog_data_1_i	: in  std_logic_vector (NB_BIT_ADC_DATA_BUS-1 downto 0);
		DAC_data_bus_1_o	: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
		DAC_load_1_o		: out std_logic;
		
		DAC_prog_data_2_i	: in  std_logic_vector (NB_BIT_ADC_DATA_BUS-1 downto 0);
		DAC_data_bus_2_o	: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
		DAC_load_2_o		: out std_logic;
		
		peak_pulse_1_o	: out std_logic;
		peak_pulse_2_o	: out std_logic
	);
end ADC_DIFF_DACsm;

architecture Behavioral of ADC_DIFF_DACsm is

	--Signal
	signal clk_intern_250MHz_s	: std_logic;

	signal DIFF_data_1_s		: std_logic_vector(NB_BIT_DIFF_DATA_BUS-1 downto 0);
	signal ADC_data_rdy_1_s	: std_logic;
	signal DAC_data_bus_1_s	: std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
	signal ADC_data_bus_1_s	: std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
	signal DAC_data_1_s		: std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
	signal DAC_load_1_s 		: std_logic;
	
	signal DIFF_data_2_s		: std_logic_vector(NB_BIT_DIFF_DATA_BUS-1 downto 0);
	signal ADC_data_rdy_2_s	: std_logic;
	signal DAC_data_bus_2_s	: std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
	signal ADC_data_bus_2_s	: std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
	signal DAC_data_2_s		: std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
	signal DAC_load_2_s 		: std_logic;

begin

	cmp_ADC_cmd : AD7626sm 
		port map ( 
		clk_500_i 	=> clk_500MHz_i,
		clk_250_i 	=> clk_250MHz_i,
		reset_i		=> reset_i,

		delay_clk_i => delay_clk_i,
--		cfg_dco_edge_1_i => cfg_dco_edge_1_i,
--		cfg_dco_edge_2_i => cfg_dco_edge_2_i,

		DCO_1_i		=> ADC_DCO_1_i,
		D_1_i			=> ADC_D_1_i,

		DCO_2_i		=> ADC_DCO_2_i,
		D_2_i			=> ADC_D_2_i,

		adc_clk_o	=> ADC_CLK_o,
		adc_cnv_o	=> ADC_CNV_o,

		DAC_data_1_o=> DAC_data_bus_1_s,
		ADC_data_1_o=> ADC_data_bus_1_s,
		data_rdy_1_o=> ADC_data_rdy_1_s,

		DAC_data_2_o=> DAC_data_bus_2_s,
		ADC_data_2_o=> ADC_data_bus_2_s,
		data_rdy_2_o=> ADC_data_rdy_2_s
	);		

	--Output ADC 1 data value for register
	ADC_data_1_o <= ADC_data_bus_1_s;
	
	cmp_monostable_drdy1 : monostable
	generic map(
		g_n_clk => 4
	)
	port map(
		clk_i		=> clk_250MHz_i,
		reset_i	=> reset_i,
		
		s_i => ADC_data_rdy_1_s,
		s_o => ADC_data_rdy_1_o
	);
	
	cmp_DIFF_1 : Differentiator
	port map( 
		clk_50MHz_i	=> clk_50MHz_i,
      reset_i		=> reset_i,
		diff_cfg_i  => diff_cfg_1_i,
		filter_on_i => filter_on_i,
		ADC_DATA		=> DAC_data_bus_1_s,
		THRESH		=> threshold_1_i,
		CLK_DATA		=> clk_250MHz_i,
		ADC_REG		=> ADC_data_rdy_1_s,
		LOAD_DAC		=> DAC_load_1_s,
		PEAK_PULSE	=> peak_pulse_1_o,
		DAC_data_o	=> DAC_data_1_s,
		DIFF_ADC		=> DIFF_data_1_s
	);		  
	
	--Output ADC 2 data value for register
	ADC_data_2_o <= ADC_data_bus_2_s;

	cmp_monostable_drdy2 : monostable
	generic map(
		g_n_clk => 4
	)
	port map(
		clk_i		=> clk_250MHz_i,
		reset_i	=> reset_i,
		
		s_i => ADC_data_rdy_2_s,
		s_o => ADC_data_rdy_2_o
	);

	cmp_DIFF_2 : Differentiator
	port map( 
		clk_50MHz_i	=> clk_50MHz_i,
      reset_i		=> reset_i,
		diff_cfg_i  => diff_cfg_2_i,
		filter_on_i => filter_on_i,
		ADC_DATA		=> DAC_data_bus_2_s,
		THRESH		=> threshold_2_i,
		CLK_DATA		=> clk_250MHz_i,
		ADC_REG		=> ADC_data_rdy_2_s,
		LOAD_DAC		=> DAC_load_2_s,
		PEAK_PULSE	=> peak_pulse_2_o,
		DAC_data_o	=> DAC_data_2_s,
		DIFF_ADC		=> DIFF_data_2_s
	);		  
	
	DIFF_data_1_o <= DIFF_data_1_s;
	DIFF_data_2_o <= DIFF_data_2_s;
	
	cmp_DAC_cmd_2Chan : DAC_Cal_2Chan
	port map( 
		clk_i 	=> clk_50MHz_i,
		reset_i 	=> reset_i,

		load_1_i				=> DAC_load_1_s,
		DAC_prog_data_1_i	=> DAC_prog_data_1_i,
		PADC_data_1_i 		=> DAC_data_1_s,
		Diff_ADC_1_i		=> DIFF_data_1_s,
		Threshold_1_i		=> threshold_1_i,
		DAC_sel_1_i			=> DAC_sel_1_i,
		load_1_o				=> DAC_load_1_o,
		DAC_data_1_o		=> DAC_data_bus_1_o,

		load_2_i				=> DAC_load_2_s,
		DAC_prog_data_2_i	=> DAC_prog_data_2_i,
		PADC_data_2_i 		=> DAC_data_2_s,
		Diff_ADC_2_i		=> DIFF_data_2_s,
		Threshold_2_i		=> threshold_2_i,
		DAC_sel_2_i			=> DAC_sel_2_i,
		load_2_o				=> DAC_load_2_o,
		DAC_data_2_o		=> DAC_data_bus_2_o
	);		  

end Behavioral;

