----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC/MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    09:10:02 09/29/2011 
-- Design Name: 
-- Module Name:    XLXI_Filter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.NUMERIC_STD.all;

library UNISIM;
use UNISIM.VComponents.all;

entity XLXI_Filter is
	Port(
		clk_i					: in std_logic; --250MHz
		clk_div2_i			: in std_logic; --125MHz
		reset_i				: in std_logic;
		DATA_ADC_i 			: in std_logic_vector(15 downto 0);
		DATA_rdy_i 			: in std_logic;

		DATA_filtered_o 	: out std_logic_vector(15 downto 0);
		DATA_rdy_o 			: out std_logic
	);
end XLXI_Filter;

architecture Biquad of XLXI_Filter is

	--TYPE DECLARATION
	type filter_cell_t is 
		(WAIT_STATE,--wait state
		 LOAD_I,		--load the input signal
		 CALC_A2_B2,--calcul of the k-2 stage
		 CALC_A1_B1,--calcul of the k-1 stage
		 LOAD_A,		--load the sum of A1 and A2 stages
		 CALC_A0,	--calcul of the A0 stage
		 CALC_B0,	--calcul of the B0 stage
		 LOAD_O,		--push the output out and into the two register
		 DRDY			--signal data_ready_out to '1'
		 );	

	--CONSTANT DECLARATION
	--Filter coefficients
	--fc=200/10000*fe  
	constant K  : std_logic_vector(17 downto 0) := "001110110000000000";--K*256
	constant A1 : std_logic_vector(17 downto 0) := "011101001010011011";
	constant A2 : std_logic_vector(17 downto 0) := "110010100110101111";
	constant B0 : std_logic_vector(17 downto 0) := "001111111111111111";
	constant B1 : std_logic_vector(17 downto 0) := "011111111111111101";
	constant B2 : std_logic_vector(17 downto 0) := "001111111111111111";

	constant OPMODE_POST_ADD	: std_logic_vector(7 downto 0) := "00001101";
	constant OPMODE_MULT 		: std_logic_vector(7 downto 0) := "00000001";

	--SIGNAL DECLARATION
	signal	data_in_s : std_logic_vector(17 downto 0) := (others=>'0');
	signal 	A0_s,A1_s,A2_s : std_logic_vector(47 downto 0) := (others=>'0');
	signal 	B0_s,B1_s,B2_s : std_logic_vector(47 downto 0) := (others=>'0');
	signal	val_gin_s : std_logic_vector(35 downto 0) := (others=>'0');
	signal	val_z0_s,val_z1_s,val_z2_s : std_logic_vector(17 downto 0) := (others=>'0');
	
	signal 	CE_z1_s,CE_z2_s,CE_out_s,
				CE_in_K_s,CE_in_A_s,CE_out_K_s,
				CE_in_A1_s,CE_in_A2_s,
				CE_in_B0_s,CE_in_B1_s,CE_in_B2_s : std_logic := '0';

	signal start_cnt_s : integer := 0;
	signal seq_start_s : std_logic := '0';
	signal data_in_adc_s : std_logic_vector(15 downto 0) := (others=>'0');

	signal ready_cnt_s : integer := 0;
	signal ready_out_s : std_logic := '0';

	signal state_s	: filter_cell_t := WAIT_STATE;

begin

	--Register Input
	process (clk_i,reset_i,DATA_ADC_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				data_in_adc_s <= (others=>'0');
			elsif (DATA_rdy_i='1') then
				data_in_adc_s <= DATA_ADC_i;
			end if;
		end if;						
	end process;
	--Generator signal seq_start_s
	process (clk_div2_i,reset_i,DATA_rdy_i)
	begin
		if rising_edge(clk_div2_i) then
			if (reset_i='1') then
				start_cnt_s <= 0;
			elsif (DATA_rdy_i='1' or (start_cnt_s>0 and start_cnt_s<3)) then--6
				start_cnt_s <= start_cnt_s+1;
			else
				start_cnt_s <= 0;
			end if;
		end if;						
	end process;
	process (clk_div2_i,reset_i,seq_start_s)
	begin
		if rising_edge(clk_div2_i) then
			if (reset_i='1') then
				seq_start_s <= '0';
			elsif ((start_cnt_s>0) and (start_cnt_s<=3)) then--6
				seq_start_s <= '1';
			else
				seq_start_s <= '0';
			end if;
		end if;						
	end process;

	--Connect the A0_s to val_z0_s
	val_z0_s <= A0_s(35) & A0_s(32 downto 16);
	--Register Z-1
	process (clk_div2_i,reset_i,val_z0_s,CE_z1_s)
	begin
		if rising_edge(clk_div2_i) then
			if (reset_i='1') then
				val_z1_s <= (others=>'0');
			else
				if (CE_z1_s='1') then
					val_z1_s <= val_z0_s;
				end if;
			end if;				
		end if;				
	end process;

	--Register Z-2
	process (clk_div2_i,reset_i,val_z1_s,CE_z2_s)
	begin
		if rising_edge(clk_div2_i) then
			if (reset_i='1') then
				val_z2_s <= (others=>'0');
			else
				if (CE_z2_s='1') then
					val_z2_s <= val_z1_s;
				end if;
			end if;				
		end if;				
	end process;
	
	--Register Output
	process (clk_div2_i,reset_i,B0_s,CE_out_s)
	begin
		if rising_edge(clk_div2_i) then
			if (reset_i='1') then
				DATA_filtered_o <= (others=>'0');
			else
				if (CE_out_s='1') then
					DATA_filtered_o <= B0_s(32 downto 17);
				end if;
			end if;				
		end if;				
	end process;
	
	--Adapt the entry from 16bits to 18bits
	data_in_s<=data_in_adc_s(data_in_adc_s'length-1) & data_in_adc_s(data_in_adc_s'length-1) & data_in_adc_s;

	--Multiplier for K-factor
   DSP48A1_K : DSP48A1
   generic map (
      A0REG => 0,              -- First stage A input pipeline register (0/1)
      A1REG => 1,              -- Second stage A input pipeline register (0/1)
      B0REG => 0,              -- First stage B input pipeline register (0/1)
      B1REG => 1,              -- Second stage B input pipeline register (0/1)
      CARRYINREG => 0,         -- CARRYIN input pipeline register (0/1)
      CARRYINSEL => "OPMODE5", -- Specify carry-in source, "CARRYIN" or "OPMODE5" 
      CARRYOUTREG => 0,        -- CARRYOUT output pipeline register (0/1)
      CREG => 1,               -- C input pipeline register (0/1)
      DREG => 0,               -- D pre-adder input pipeline register (0/1)
      MREG => 0,               -- M pipeline register (0/1)
      OPMODEREG => 0,          -- Enable=1/disable=0 OPMODE input pipeline registers
      PREG => 1,               -- P output pipeline register (0/1)
      RSTTYPE => "SYNC"        -- Specify reset type, "SYNC" or "ASYNC" 
   )
   port map (
      P => A0_s,						-- 48-bit output: data output
      PCIN => (others=>'0'),		-- 48-bit input: P cascade input (if used, connect to PCOUT of another DSP48A1)
      CLK => clk_div2_i,				-- 1-bit input: clock input
		OPMODE => OPMODE_POST_ADD,	-- 8-bit input: operation mode input
      A => K,							-- 18-bit input: A data input
      B => data_in_s,				-- 18-bit input: B data input (connected to fabric or BCOUT of adjacent DSP48A1)
      C => A1_s,						-- 48-bit input: C data input
      CARRYIN => '0', 				-- 1-bit input: carry input signal (if used, connect to CARRYOUT pin of another DSP48A1)

      D => (others=>'0'),			-- 18-bit input: B pre-adder data input
      CEA => CE_in_K_s,				-- 1-bit input: active high clock enable input for A registers
      CEB => CE_in_K_s,				-- 1-bit input: active high clock enable input for B registers
      CEC => CE_in_A_s,				-- 1-bit input: active high clock enable input for C registers
      CECARRYIN => '0',				-- 1-bit input: active high clock enable input for CARRYIN registers
      CED => '0',						-- 1-bit input: active high clock enable input for D registers
      CEM => '0',						-- 1-bit input: active high clock enable input for multiplier registers
      CEOPMODE => '0',				-- 1-bit input: active high clock enable input for OPMODE registers
      CEP => CE_out_K_s,			-- 1-bit input: active high clock enable input for P registers
      RSTA => reset_i,           -- 1-bit input: reset input for A pipeline registers
      RSTB => reset_i,           -- 1-bit input: reset input for B pipeline registers
      RSTC => reset_i,           -- 1-bit input: reset input for C pipeline registers
      RSTCARRYIN => reset_i,		-- 1-bit input: reset input for CARRYIN pipeline registers
      RSTD => reset_i,           -- 1-bit input: reset input for D pipeline registers
      RSTM => reset_i,           -- 1-bit input: reset input for M pipeline registers
      RSTOPMODE => reset_i,   	-- 1-bit input: reset input for OPMODE pipeline registers
      RSTP => reset_i            -- 1-bit input: reset input for P pipeline registers
   );

	--Multiplier for a1-coefficient
   DSP48A1_A1 : DSP48A1
   generic map (
      A0REG => 0,              -- First stage A input pipeline register (0/1)
      A1REG => 1,              -- Second stage A input pipeline register (0/1)
      B0REG => 0,              -- First stage B input pipeline register (0/1)
      B1REG => 1,              -- Second stage B input pipeline register (0/1)
      CARRYINREG => 0,         -- CARRYIN input pipeline register (0/1)
      CARRYINSEL => "OPMODE5", -- Specify carry-in source, "CARRYIN" or "OPMODE5" 
      CARRYOUTREG => 0,        -- CARRYOUT output pipeline register (0/1)
      CREG => 1,               -- C input pipeline register (0/1)
      DREG => 0,               -- D pre-adder input pipeline register (0/1)
      MREG => 0,               -- M pipeline register (0/1)
      OPMODEREG => 0,          -- Enable=1/disable=0 OPMODE input pipeline registers
      PREG => 0,               -- P output pipeline register (0/1)
      RSTTYPE => "SYNC"        -- Specify reset type, "SYNC" or "ASYNC" 
   )
   port map (
      P => A1_s,						-- 48-bit output: data output
      CLK => clk_div2_i,				-- 1-bit input: clock input
      OPMODE => OPMODE_POST_ADD,	-- 8-bit input: operation mode input
      A => A1,							-- 18-bit input: A data input
      B => val_z1_s,					-- 18-bit input: B data input (connected to fabric or BCOUT of adjacent DSP48A1)
      C => A2_s,						-- 48-bit input: C data input
      D => (others=>'0'),			-- 18-bit input: B pre-adder data input
      CEA => CE_in_A1_s,			-- 1-bit input: active high clock enable input for A registers
      CEB => CE_in_A1_s,			-- 1-bit input: active high clock enable input for B registers
      CEC => CE_in_A1_s,			-- 1-bit input: active high clock enable input for C registers
      CECARRYIN => '0',				-- 1-bit input: active high clock enable input for CARRYIN registers
      CED => '0',						-- 1-bit input: active high clock enable input for D registers
      CEM => '0',						-- 1-bit input: active high clock enable input for multiplier registers
      CEOPMODE => '0',				-- 1-bit input: active high clock enable input for OPMODE registers
      CEP => '0',						-- 1-bit input: active high clock enable input for P registers
      RSTA => reset_i,				-- 1-bit input: reset input for A pipeline registers
      RSTB => reset_i,				-- 1-bit input: reset input for B pipeline registers
      RSTC => reset_i,				-- 1-bit input: reset input for C pipeline registers
      RSTCARRYIN => reset_i,		-- 1-bit input: reset input for CARRYIN pipeline registers
      RSTD => reset_i,				-- 1-bit input: reset input for D pipeline registers
      RSTM => reset_i,				-- 1-bit input: reset input for M pipeline registers
      RSTOPMODE => reset_i,		-- 1-bit input: reset input for OPMODE pipeline registers
      RSTP => reset_i				-- 1-bit input: reset input for P pipeline registers
   );

	--Multiplier for a1-coefficient
   DSP48A1_A2 : DSP48A1
   generic map (
      A0REG => 0,              -- First stage A input pipeline register (0/1)
      A1REG => 1,              -- Second stage A input pipeline register (0/1)
      B0REG => 0,              -- First stage B input pipeline register (0/1)
      B1REG => 1,              -- Second stage B input pipeline register (0/1)
      CARRYINREG => 0,         -- CARRYIN input pipeline register (0/1)
      CARRYINSEL => "OPMODE5", -- Specify carry-in source, "CARRYIN" or "OPMODE5" 
      CARRYOUTREG => 0,        -- CARRYOUT output pipeline register (0/1)
      CREG => 0,               -- C input pipeline register (0/1)
      DREG => 0,               -- D pre-adder input pipeline register (0/1)
      MREG => 0,               -- M pipeline register (0/1)
      OPMODEREG => 0,          -- Enable=1/disable=0 OPMODE input pipeline registers
      PREG => 0,               -- P output pipeline register (0/1)
      RSTTYPE => "SYNC"        -- Specify reset type, "SYNC" or "ASYNC" 
   )
   port map (
      P => A2_s,						-- 48-bit output: data output
      CLK => clk_div2_i,				-- 1-bit input: clock input
      OPMODE => OPMODE_MULT,		-- 8-bit input: operation mode input
      A => A2,							-- 18-bit input: A data input
      B => val_z2_s,					-- 18-bit input: B data input (connected to fabric or BCOUT of adjacent DSP48A1)
      C => (others=>'0'),			-- 48-bit input: C data input
      D => (others=>'0'),			-- 18-bit input: B pre-adder data input
      CEA => CE_in_A2_s,			-- 1-bit input: active high clock enable input for A registers
      CEB => CE_in_A2_s,			-- 1-bit input: active high clock enable input for B registers
      CEC => '0',						-- 1-bit input: active high clock enable input for C registers
      CECARRYIN => '0',				-- 1-bit input: active high clock enable input for CARRYIN registers
      CED => '0',						-- 1-bit input: active high clock enable input for D registers
      CEM => '0',						-- 1-bit input: active high clock enable input for multiplier registers
      CEOPMODE => '0',				-- 1-bit input: active high clock enable input for OPMODE registers
      CEP => '0',						-- 1-bit input: active high clock enable input for P registers
      RSTA => reset_i,				-- 1-bit input: reset input for A pipeline registers
      RSTB => reset_i,				-- 1-bit input: reset input for B pipeline registers
      RSTC => reset_i,				-- 1-bit input: reset input for C pipeline registers
      RSTCARRYIN => reset_i,		-- 1-bit input: reset input for CARRYIN pipeline registers
      RSTD => reset_i,				-- 1-bit input: reset input for D pipeline registers
      RSTM => reset_i,				-- 1-bit input: reset input for M pipeline registers
      RSTOPMODE => reset_i,		-- 1-bit input: reset input for OPMODE pipeline registers
      RSTP => reset_i				-- 1-bit input: reset input for P pipeline registers
   );
	
	--Multiplier for a1-coefficient
   DSP48A1_B0 : DSP48A1
   generic map (
      A0REG => 0,              -- First stage A input pipeline register (0/1)
      A1REG => 1,              -- Second stage A input pipeline register (0/1)
      B0REG => 0,              -- First stage B input pipeline register (0/1)
      B1REG => 1,              -- Second stage B input pipeline register (0/1)
      CARRYINREG => 0,         -- CARRYIN input pipeline register (0/1)
      CARRYINSEL => "OPMODE5", -- Specify carry-in source, "CARRYIN" or "OPMODE5" 
      CARRYOUTREG => 0,        -- CARRYOUT output pipeline register (0/1)
      CREG => 1,               -- C input pipeline register (0/1)
      DREG => 0,               -- D pre-adder input pipeline register (0/1)
      MREG => 0,               -- M pipeline register (0/1)
      OPMODEREG => 0,          -- Enable=1/disable=0 OPMODE input pipeline registers
      PREG => 0,               -- P output pipeline register (0/1)
      RSTTYPE => "SYNC"        -- Specify reset type, "SYNC" or "ASYNC" 
   )
   port map (
      P => B0_s,						-- 48-bit output: data output
      CLK => clk_div2_i,				-- 1-bit input: clock input
      OPMODE => OPMODE_POST_ADD,	-- 8-bit input: operation mode input
      A => B0,							-- 18-bit input: A data input
      B => val_z0_s,					-- 18-bit input: B data input (connected to fabric or BCOUT of adjacent DSP48A1)
      C => B1_s,						-- 48-bit input: C data input
      D => (others=>'0'),			-- 18-bit input: B pre-adder data input
      CEA => CE_in_B0_s,			-- 1-bit input: active high clock enable input for A registers
      CEB => CE_in_B0_s,			-- 1-bit input: active high clock enable input for B registers
      CEC => CE_in_B0_s,			-- 1-bit input: active high clock enable input for C registers
      CECARRYIN => '0',				-- 1-bit input: active high clock enable input for CARRYIN registers
      CED => '0',						-- 1-bit input: active high clock enable input for D registers
      CEM => '0',						-- 1-bit input: active high clock enable input for multiplier registers
      CEOPMODE => '0',				-- 1-bit input: active high clock enable input for OPMODE registers
      CEP => '0',						-- 1-bit input: active high clock enable input for P registers
      RSTA => reset_i,				-- 1-bit input: reset input for A pipeline registers
      RSTB => reset_i,				-- 1-bit input: reset input for B pipeline registers
      RSTC => reset_i,				-- 1-bit input: reset input for C pipeline registers
      RSTCARRYIN => reset_i,		-- 1-bit input: reset input for CARRYIN pipeline registers
      RSTD => reset_i,				-- 1-bit input: reset input for D pipeline registers
      RSTM => reset_i,				-- 1-bit input: reset input for M pipeline registers
      RSTOPMODE => reset_i,		-- 1-bit input: reset input for OPMODE pipeline registers
      RSTP => reset_i				-- 1-bit input: reset input for P pipeline registers
   );

	--Multiplier for a1-coefficient
   DSP48A1_B1 : DSP48A1
   generic map (
      A0REG => 0,              -- First stage A input pipeline register (0/1)
      A1REG => 1,              -- Second stage A input pipeline register (0/1)
      B0REG => 0,              -- First stage B input pipeline register (0/1)
      B1REG => 1,              -- Second stage B input pipeline register (0/1)
      CARRYINREG => 0,         -- CARRYIN input pipeline register (0/1)
      CARRYINSEL => "OPMODE5", -- Specify carry-in source, "CARRYIN" or "OPMODE5" 
      CARRYOUTREG => 0,        -- CARRYOUT output pipeline register (0/1)
      CREG => 1,               -- C input pipeline register (0/1)
      DREG => 0,               -- D pre-adder input pipeline register (0/1)
      MREG => 0,               -- M pipeline register (0/1)
      OPMODEREG => 0,          -- Enable=1/disable=0 OPMODE input pipeline registers
      PREG => 0,               -- P output pipeline register (0/1)
      RSTTYPE => "SYNC"        -- Specify reset type, "SYNC" or "ASYNC" 
   )
   port map (
      P => B1_s,						-- 48-bit output: data output
      CLK => clk_div2_i,				-- 1-bit input: clock input
      OPMODE => OPMODE_POST_ADD,	-- 8-bit input: operation mode input
      A => B1,							-- 18-bit input: A data input
      B => val_z1_s,					-- 18-bit input: B data input (connected to fabric or BCOUT of adjacent DSP48A1)
      C => B2_s,					-- 48-bit input: C data input
      D => (others=>'0'),			-- 18-bit input: B pre-adder data input
      CEA => CE_in_B1_s,			-- 1-bit input: active high clock enable input for A registers
      CEB => CE_in_B1_s,			-- 1-bit input: active high clock enable input for B registers
      CEC => CE_in_B1_s,			-- 1-bit input: active high clock enable input for C registers
      CECARRYIN => '0',				-- 1-bit input: active high clock enable input for CARRYIN registers
      CED => '0',						-- 1-bit input: active high clock enable input for D registers
      CEM => '0',						-- 1-bit input: active high clock enable input for multiplier registers
      CEOPMODE => '0',				-- 1-bit input: active high clock enable input for OPMODE registers
      CEP => '0',						-- 1-bit input: active high clock enable input for P registers
      RSTA => reset_i,				-- 1-bit input: reset input for A pipeline registers
      RSTB => reset_i,				-- 1-bit input: reset input for B pipeline registers
      RSTC => reset_i,				-- 1-bit input: reset input for C pipeline registers
      RSTCARRYIN => reset_i,		-- 1-bit input: reset input for CARRYIN pipeline registers
      RSTD => reset_i,				-- 1-bit input: reset input for D pipeline registers
      RSTM => reset_i,				-- 1-bit input: reset input for M pipeline registers
      RSTOPMODE => reset_i,		-- 1-bit input: reset input for OPMODE pipeline registers
      RSTP => reset_i				-- 1-bit input: reset input for P pipeline registers
   );

	--Multiplier for a1-coefficient
   DSP48A1_B2 : DSP48A1
   generic map (
      A0REG => 0,              -- First stage A input pipeline register (0/1)
      A1REG => 1,              -- Second stage A input pipeline register (0/1)
      B0REG => 0,              -- First stage B input pipeline register (0/1)
      B1REG => 1,              -- Second stage B input pipeline register (0/1)
      CARRYINREG => 0,         -- CARRYIN input pipeline register (0/1)
      CARRYINSEL => "OPMODE5", -- Specify carry-in source, "CARRYIN" or "OPMODE5" 
      CARRYOUTREG => 0,        -- CARRYOUT output pipeline register (0/1)
      CREG => 0,               -- C input pipeline register (0/1)
      DREG => 0,               -- D pre-adder input pipeline register (0/1)
      MREG => 0,               -- M pipeline register (0/1)
      OPMODEREG => 0,          -- Enable=1/disable=0 OPMODE input pipeline registers
      PREG => 0,               -- P output pipeline register (0/1)
      RSTTYPE => "SYNC"        -- Specify reset type, "SYNC" or "ASYNC" 
   )
   port map (
      P => B2_s,						-- 48-bit output: data output
      CLK => clk_div2_i,				-- 1-bit input: clock input
      OPMODE => OPMODE_MULT,		-- 8-bit input: operation mode input
      A => B2,							-- 18-bit input: A data input
      B => val_z2_s,					-- 18-bit input: B data input (connected to fabric or BCOUT of adjacent DSP48A1)
      C => (others=>'0'),			-- 48-bit input: C data input
      D => (others=>'0'),			-- 18-bit input: B pre-adder data input
      CEA => CE_in_B2_s,			-- 1-bit input: active high clock enable input for A registers
      CEB => CE_in_B2_s,			-- 1-bit input: active high clock enable input for B registers
      CEC => '0',						-- 1-bit input: active high clock enable input for C registers
      CECARRYIN => '0',				-- 1-bit input: active high clock enable input for CARRYIN registers
      CED => '0',						-- 1-bit input: active high clock enable input for D registers
      CEM => '0',						-- 1-bit input: active high clock enable input for multiplier registers
      CEOPMODE => '0',				-- 1-bit input: active high clock enable input for OPMODE registers
      CEP => '0',						-- 1-bit input: active high clock enable input for P registers
      RSTA => reset_i,				-- 1-bit input: reset input for A pipeline registers
      RSTB => reset_i,				-- 1-bit input: reset input for B pipeline registers
      RSTC => reset_i,				-- 1-bit input: reset input for C pipeline registers
      RSTCARRYIN => reset_i,		-- 1-bit input: reset input for CARRYIN pipeline registers
      RSTD => reset_i,				-- 1-bit input: reset input for D pipeline registers
      RSTM => reset_i,				-- 1-bit input: reset input for M pipeline registers
      RSTOPMODE => reset_i,		-- 1-bit input: reset input for OPMODE pipeline registers
      RSTP => reset_i				-- 1-bit input: reset input for P pipeline registers
   );

	--State Machine
	process (clk_div2_i,reset_i)
	begin
		if rising_edge(clk_div2_i) then
			if (reset_i='1') then
				state_s <= WAIT_STATE;
				CE_z1_s		<='0';
				CE_z2_s		<='0';
				CE_out_s		<='0';
				CE_in_K_s	<='0';
				CE_in_A_s	<='0';
				CE_out_K_s	<='0';
				CE_in_A1_s	<='0';
				CE_in_A2_s	<='0';
				CE_in_B0_s	<='0';
				CE_in_B1_s	<='0';
				CE_in_B2_s	<='0';
				ready_out_s	<='0';
			else
				CE_z1_s		<='0';
				CE_z2_s		<='0';
				CE_out_s		<='0';
				CE_in_K_s	<='0';
				CE_in_A_s	<='0';
				CE_out_K_s	<='0';
				CE_in_A1_s	<='0';
				CE_in_A2_s	<='0';
				CE_in_B0_s	<='0';
				CE_in_B1_s	<='0';
				CE_in_B2_s	<='0';
				ready_out_s	<='0';
				case state_s is
						when WAIT_STATE =>
							if (seq_start_s='1') then
								state_s <= LOAD_I;
							end if;
						when LOAD_I =>
							state_s <= CALC_A2_B2;
							CE_in_K_s	<='1';
						when CALC_A2_B2 =>
							state_s <= CALC_A1_B1;
							CE_in_A2_s	<='1';
							CE_in_B2_s	<='1';
						when CALC_A1_B1 =>
							state_s <= LOAD_A;
							CE_in_A1_s	<='1';
							CE_in_B1_s	<='1';
						when LOAD_A =>
							state_s <= CALC_A0;
							CE_in_A_s	<='1';
						when CALC_A0 =>
							state_s <= CALC_B0;
							CE_out_K_s	<='1';
						when CALC_B0 =>
							state_s <= LOAD_O;
							CE_in_B0_s	<='1';
						when LOAD_O =>
							state_s <= DRDY;
							CE_z1_s		<='1';
							CE_z2_s		<='1';
							CE_out_s		<='1';
						when DRDY =>
							state_s <= WAIT_STATE;
							ready_out_s	<='1';
				end case;
			end if;					
		end if;					
	end process;

	--Generator signal DATA_rdy_o
	process (clk_div2_i,reset_i,ready_out_s,ready_cnt_s)
	begin
		if rising_edge(clk_div2_i) then
			if (reset_i='1') then
				ready_cnt_s <= 0;
			elsif (ready_out_s='1' or (ready_cnt_s>0 and ready_cnt_s<10)) then
				ready_cnt_s <= ready_cnt_s+1;
			else
				ready_cnt_s <= 0;
			end if;
		end if;						
	end process;
	
	process (clk_div2_i,reset_i,ready_cnt_s)
	begin
		if rising_edge(clk_div2_i) then
			if (reset_i='1') then
				DATA_rdy_o <= '0';
			elsif ((ready_cnt_s>0) and (ready_cnt_s<=3)) then
				DATA_rdy_o <= '0';
			elsif ((ready_cnt_s>3) and (ready_cnt_s<8)) then
				DATA_rdy_o <= '1';
			else
				DATA_rdy_o <= '0';
			end if;
		end if;						
	end process;
	
end Biquad;

