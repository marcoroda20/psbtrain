----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:08:38 10/08/2012 
-- Design Name: 
-- Module Name:    Differentiatorsm - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Differentiatorsm is
    port ( 
		clk_250MHz_i	: in std_logic;
		reset_i			: in std_logic;
		
		ADC_data_i		: in  std_logic_vector(15 downto 0);
		Threhold_i		: in  std_logic_vector(15 downto 0);
		ADC_data_rdy_i	: in  std_logic;
		
		Diff_data_o		: out  std_logic_vector(15 downto 0);
		Peak_pulse_o	: out  std_logic;
		data_rdy_o		: out  std_logic
	);			  
end Differentiatorsm;

architecture Behavioral of Differentiatorsm is

--Type declaration
type diff_states_t is (
	IDLE,
	START_CMD,
	DIFF,
	COMPARATOR
	);

	--Signal declaration
	signal data_in_s		: std_logic_vector(15 downto 0);
	signal start_cmd_s	: std_logic;

	signal diff_s	: std_logic_vector(15 downto 0);

begin

--Load input
process (clk_250MHz_i,reset_i)
begin
	if (reset_i='1') then
		data_in_s <= (others=>'0');
		start_cmd_s <= '0';
   elsif (clk_250MHz_i'event and clk_250MHz_i = '1') then 
		if (ADC_data_rdy_i = '1') then
			data_in_s <= ADC_data_i;
			start_cmd_s <= ADC_data_rdy_i;
		end if;
   end if;
end process;

--Output calculation
process (Present_State_s)
begin
	case Present_State_s is
		when IDLE =>
			data_in_s <= ADC_data_i;
			diff_s <= ;
		when DIFF =>
			diff_s <= ;
		when COMPARATOR =>
			diff_s <= data_in_s-;
	end case;
end process;

--Next State
process (Present_State_s,start_cmd_s) 
begin
	case Present_State_s is
		when IDLE =>
			if (start_cmd_s = '1') then
				Future_State_s <= START_CMD;
			else
				Future_State_s <= START_CMD;
			end if;
		when START_CMD =>
			Future_State_s <= DIFF;
		when DIFF =>
			Future_State_s <= COMPARATOR;
		when COMPARATOR =>
			Future_State_s <= IDLE;
	end case;
end process;

--Update State Machine
process (clk_250MHz_i,reset_i,Future_State_s) 
begin
	if (reset_i='1') then
		Present_State_s <= IDLE;
   elsif (clk_250MHz_i'event and clk_250MHz_i = '1') then  
		Present_State_s <= Future_State_s;
   end if;
end process;

end Behavioral;

