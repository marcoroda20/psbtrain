----------------------------------------------------------------------------------
-- Company:        CERN TE/MSC/MM
-- Engineer:       Daniel Oberson
-- 
-- Create Date:    14:34:07 01/01/2014 
-- Design Name: 
-- Module Name:    c0_time - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

library work;
use work.utils_pkg.all;

entity c0_time is
	port(
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		c0_pulse_i   : in std_logic;
		trig_pulse_i : in std_logic;
		
		c0_time_o : out std_logic_vector(31 downto 0) 
	);
end c0_time;

architecture Behavioral of c0_time is

	--Signal
	signal counter_s    : std_logic_vector(31 downto 0);
	signal c0_pulse_s   : std_logic;
	signal c0_pulse_n_s : std_logic;
	signal time_meas_s  : std_logic;
	
begin

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				counter_s   <= (others=>'0');
				time_meas_s <= '0';
				c0_time_o   <= (others=>'0');
			else
				if (c0_pulse_s='1') then
					time_meas_s <= '1';
					counter_s   <= std_logic_vector(to_unsigned(1,counter_s'length));--1 because of the Rising_edge detection
				elsif (trig_pulse_i='1') then -- active low for output trig
					if (time_meas_s='1') then
						counter_s <= std_logic_vector(unsigned(counter_s) + to_unsigned(1,counter_s'length));
					else
						-- Output
						c0_time_o <= counter_s;
					end if;
				else
					time_meas_s <= '0';
				end if;
			end if;
		end if;
	end process;
		
	c0_pulse_n_s <= not c0_pulse_i;
	
	cmp_C0_RisingDetect : RisingDetect
		port map(
			clk_i   => clk_i,
			reset_i => reset_i,
			
			s_i => c0_pulse_n_s,--not c0_pulse_i, -- active low C0 pulse
			s_o => c0_pulse_s
		);
		
end Behavioral;

