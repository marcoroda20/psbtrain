----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC/MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    10:29:08 06/15/2012 
-- Design Name: 
-- Module Name:    spec_top_fmc_adc_16b_10Ms - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versionsp: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.peak_detector_pkg.all;
use work.gn4124_core_pkg.all;
use work.ddr3_ctrl_pkg.all;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.sdb_meta_pkg.all;
use work.utils_pkg.all;

entity spec_top_fmc_adc_16b_10Ms is
  generic (
    -- Simulation mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the testbench.
    -- Its purpose is to reduce some internal counters/timeouts to speed up simulations.
    g_simulation : integer := 0
    );
	port(
      -- Local oscillator
      clk_20MHz_i  : in std_logic;      -- 20MHz VCXO clock
		reset_n_hw_i : in std_logic;
		
      -- Carrier font panel LEDs
      led_red_o   : out std_logic;
      led_green_o : out std_logic;

      -- Auxiliary pins
      aux_leds_o    : out std_logic_vector(3 downto 0);
      aux_buttons_i : in  std_logic_vector(1 downto 0);

      -- PCB version
      pcb_ver_i : in std_logic_vector(3 downto 0);

		-- I2C bus connected to the EEPROM on the DIO mezzanine. This EEPROM is used
		-- for storing WR Core's configuration parameters.
		fmc_scl_b : inout std_logic;
		fmc_sda_b : inout std_logic;

      -- FMC slot management
      fmc0_prsnt_m2c_n_i : in std_logic;  -- Mezzanine present (active low)
		
      -- FMC 1-wire interface (DS18B20 thermometer + unique ID)
      fmc_one_wire_b : inout std_logic;    -- 1-Wire interface to DS18B20

      -- Carrier 1-wire interface (DS18B20 thermometer + unique ID)
      carrier_one_wire_b : inout std_logic;-- 1-Wire interface to DS18B20

      -- GN4124 interface
      L_CLKp       : in    std_logic;                      -- Local bus clock (frequency set in GN4124 config registers)
      L_CLKn       : in    std_logic;                      -- Local bus clock (frequency set in GN4124 config registers)
      L_RST_N      : in    std_logic;                      -- Reset from GN4124 (RSTOUT18_N)
      P2L_RDY      : out   std_logic;                      -- Rx Buffer Full Flag
      P2L_CLKn     : in    std_logic;                      -- Receiver Source Synchronous Clock-
      P2L_CLKp     : in    std_logic;                      -- Receiver Source Synchronous Clock+
      P2L_DATA     : in    std_logic_vector(15 downto 0);  -- Parallel receive data
      P2L_DFRAME   : in    std_logic;                      -- Receive Frame
      P2L_VALID    : in    std_logic;                      -- Receive Data Valid
      P_WR_REQ     : in    std_logic_vector(1 downto 0);   -- PCIe Write Request
      P_WR_RDY     : out   std_logic_vector(1 downto 0);   -- PCIe Write Ready
      RX_ERROR     : out   std_logic;                      -- Receive Error
      L2P_DATA     : out   std_logic_vector(15 downto 0);  -- Parallel transmit data
      L2P_DFRAME   : out   std_logic;                      -- Transmit Data Frame
      L2P_VALID    : out   std_logic;                      -- Transmit Data Valid
      L2P_CLKn     : out   std_logic;                      -- Transmitter Source Synchronous Clock-
      L2P_CLKp     : out   std_logic;                      -- Transmitter Source Synchronous Clock+
      L2P_EDB      : out   std_logic;                      -- Packet termination and discard
      L2P_RDY      : in    std_logic;                      -- Tx Buffer Full Flag
      L_WR_RDY     : in    std_logic_vector(1 downto 0);   -- Local-to-PCIe Write
      P_RD_D_RDY   : in    std_logic_vector(1 downto 0);   -- PCIe-to-Local Read Response Data Ready
      TX_ERROR     : in    std_logic;                      -- Transmit Error
      VC_RDY       : in    std_logic_vector(1 downto 0);   -- Channel ready
      GPIO         : inout std_logic_vector(1 downto 0);   -- GPIO[0] -> GN4124 GPIO8
                                                           -- GPIO[1] -> GN4124 GPIO9
      -- DDR3 interface
      DDR3_CAS_N   : out   std_logic;
      DDR3_CK_N    : out   std_logic;
      DDR3_CK_P    : out   std_logic;
      DDR3_CKE     : out   std_logic;
      DDR3_LDM     : out   std_logic;
      DDR3_LDQS_N  : inout std_logic;
      DDR3_LDQS_P  : inout std_logic;
      DDR3_ODT     : out   std_logic;
      DDR3_RAS_N   : out   std_logic;
      DDR3_RESET_N : out   std_logic;
      DDR3_UDM     : out   std_logic;
      DDR3_UDQS_N  : inout std_logic;
      DDR3_UDQS_P  : inout std_logic;
      DDR3_WE_N    : out   std_logic;
      DDR3_DQ      : inout std_logic_vector(15 downto 0);
      DDR3_A       : out   std_logic_vector(13 downto 0);
      DDR3_BA      : out   std_logic_vector(2 downto 0);
      DDR3_ZIO     : inout std_logic;
      DDR3_RZQ     : inout std_logic;

		--FMC Led
		C0_LED_o		: out std_logic;
		
      -- FMC slot	
		--FMC slot DAC 
		DAC1_CLK_o	: out std_logic; 
		DAC2_CLK_o	: out std_logic; 
		DAC_DATA_o	: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);

		--FMC slot DIO
		SYS_DIO_io	: inout std_logic_vector(NB_SYS_IO-1 downto 0);
		DIR_DIO_o	: out std_logic_vector(NB_SYS_IO-1 downto 0);
	
		--FMC slot Differential IO
--		DI0_DIFF_P_i	: in std_logic;
--		DI0_DIFF_N_i	: in std_logic;
--		DI1_DIFF_P_i	: in std_logic;
--		DI1_DIFF_N_i	: in std_logic;
--		DI2_DIFF_P_i	: in std_logic;
--		DI2_DIFF_N_i	: in std_logic;

		DO0_DIFF_P_o	: out std_logic;
		DO0_DIFF_N_o	: out std_logic;
		DO1_DIFF_P_o	: out std_logic;
		DO1_DIFF_N_o	: out std_logic;
		DO2_DIFF_P_o	: out std_logic;
		DO2_DIFF_N_o	: out std_logic;

		--FMC slot ADC 
		ADC_CLK_P_i		: in std_logic;
		ADC_CLK_N_i		: in std_logic;

		ADC1_D_P_i		: in std_logic;
		ADC1_D_N_i		: in std_logic;

		ADC1_DCO_P_i	: in std_logic;
		ADC1_DCO_N_i	: in std_logic;
		
		ADC1_CLK_P_o	: out std_logic;
		ADC1_CLK_N_o	: out std_logic;

		ADC2_D_P_i		: in std_logic;
		ADC2_D_N_i		: in std_logic;
		
		ADC2_DCO_P_i	: in std_logic;
		ADC2_DCO_N_i	: in std_logic;
		
		ADC2_CLK_P_o	: out std_logic;
		ADC2_CLK_N_o	: out std_logic;

		ADC_CNV_P_o		: out std_logic;
		ADC_CNV_N_o		: out std_logic;
		
		--FMC slot TRIG
		TRIG2_MISS_o	: out std_logic;
		TRIG2_OK_o		: out std_logic;
		
		TRIG1_MISS_o	: out std_logic;
		TRIG1_OK_o		: out std_logic
	);
end spec_top_fmc_adc_16b_10Ms;

architecture Behavioral of spec_top_fmc_adc_16b_10Ms is

  ------------------------------------------------------------------------------
  -- SDB crossbar constants declaration
  --
  -- WARNING: All address in sdb and crossbar are BYTE addresses!
  ------------------------------------------------------------------------------

  -- Number of master port(s) on the wishbone crossbar
  constant c_NUM_WB_MASTERS : integer := 7;

  -- Number of slave port(s) on the wishbone crossbar
  constant c_NUM_WB_SLAVES : integer := 1;

  -- Wishbone master(s)
  constant c_MASTER_GENNUM : integer := 0;

  -- Wishbone slave(s)
  constant c_SLAVE_DMA       : integer := 0;  -- DMA controller in the Gennum core
  constant c_SLAVE_ONEWIRE   : integer := 1;  -- Carrier onewire interface
  constant c_SLAVE_SPEC_CSR  : integer := 2;  -- SPEC control and status registers
  constant c_SLAVE_INT       : integer := 3;  -- Interrupt controller
  constant c_SLAVE_FMC       : integer := 4;  -- FMC Peak Detector mezzanine

  -- Devices sdb description
  constant c_DMA_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"000000000000003F",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000601",
        version   => x"00000001",
        date      => x"20131107",
        name      => "WB-DMA.Control     ")));

  constant c_ONEWIRE_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"0000000000000007",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000602",
        version   => x"00000001",
        date      => x"20131107",
        name      => "WB-Onewire.Control ")));

  constant c_SPEC_CSR_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"000000000000001F",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000603",
        version   => x"00000001",
        date      => x"20131107",
        name      => "WB-SPEC-CSR        ")));

  constant c_INT_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"000000000000000F",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000604",
        version   => x"00000001",
        date      => x"20121116",
        name      => "WB-Int.Control     ")));

  -- f_xwb_bridge_manual_sdb(size, sdb_addr)
  constant c_FMC_SDB_BRIDGE : t_sdb_bridge := f_xwb_bridge_manual_sdb(x"00001fff", x"00004000");

  -- sdb header address
  constant c_SDB_ADDRESS : t_wishbone_address := x"00000000";

  -- Wishbone crossbar layout
  constant c_INTERCONNECT_LAYOUT : t_sdb_record_array(7 downto 0) :=
    (
      0 => f_sdb_embed_device(c_DMA_SDB_DEVICE, x"00001000"),
      1 => f_sdb_embed_device(c_ONEWIRE_SDB_DEVICE, x"00001100"),
      2 => f_sdb_embed_device(c_SPEC_CSR_SDB_DEVICE, x"00001200"),
      3 => f_sdb_embed_device(c_INT_SDB_DEVICE, x"00002000"),
      4 => f_sdb_embed_bridge(c_FMC_SDB_BRIDGE, x"00004000"),
      5 => f_sdb_embed_repo_url(c_SDB_REPO_URL),
      6 => f_sdb_embed_synthesis(c_SDB_SYNTHESIS),
      7 => f_sdb_embed_integration(c_SDB_INTEGRATION)
      );

  ------------------------------------------------------------------------------
  -- Other constants declaration
  ------------------------------------------------------------------------------

	--Simulation
	constant c_G_SIMULATON : integer := g_simulation;

	constant c_CARRIER_TYPE   : std_logic_vector(15 downto 0) := X"0001";
	constant c_BITSTREAM_TYPE : std_logic_vector(31 downto 0) := X"00000001";
	constant c_BITSTREAM_DATE : std_logic_vector(31 downto 0) := X"deadface";  -- UTC time

	constant c_BAR0_APERTURE    : integer := 18;  -- nb of bits for 32-bit word address (= byte aperture - 2)
	constant c_CSR_WB_SLAVES_NB : integer := 12;

	--Start reset timer
	constant START_RESET_TIMER_SIM : integer := 20;
	constant START_RESET_TIMER     : integer := 125000000; -- 1 s with a 125MHz clock
	--Start reset timer
	constant START_INIT_TIMER_SIM : integer := 2*START_RESET_TIMER_SIM;
	constant START_INIT_TIMER     : integer := 2*START_RESET_TIMER; -- 2 s with a 125MHz clock

	--Component	
	component irq_controller
	  port (
		 -- Clock, reset
		 clk_i   : in std_logic;
		 rst_n_i : in std_logic;

		 -- Interrupt sources input, must be 1 clk_i tick long
		 irq_src_p_i : in std_logic_vector(31 downto 0);

		 -- IRQ pulse output
		 irq_p_o : out std_logic;

		 -- Wishbone interface
		 wb_adr_i : in  std_logic_vector(1 downto 0);
		 wb_dat_i : in  std_logic_vector(31 downto 0);
		 wb_dat_o : out std_logic_vector(31 downto 0);
		 wb_cyc_i : in  std_logic;
		 wb_sel_i : in  std_logic_vector(3 downto 0);
		 wb_stb_i : in  std_logic;
		 wb_we_i  : in  std_logic;
		 wb_ack_o : out std_logic
		 );
	end component;

	--Signal
	-- Clock
	signal sys_clk_fb_s         : std_logic;
	signal sys_clk_20_s         : std_logic; 
	signal sys_clk_20_buf_s     : std_logic; 
	signal sys_clk_50_s         : std_logic; 
	signal sys_clk_50_buf_s     : std_logic; 
	signal sys_clk_125_s        : std_logic; 
	signal sys_clk_125_buf_s    : std_logic; 
	signal sys_clk_250_s        : std_logic; 
	signal sys_clk_250_buf_s    : std_logic; 
	signal sys_clk_500_s        : std_logic;
	signal sys_clk_500_buf_s    : std_logic;
	signal ddr_clk_s            : std_logic; 
	signal ddr_clk_buf_s        : std_logic; 
	signal sys_clk_pll_locked_s : std_logic;
	
	signal sys_clk_in_cryst_s   : std_logic; 

	--Reset
	signal reset_s         : std_logic := '0'; 
	signal reset_n_s       : std_logic := '1'; 
	signal but_pci_rst_n_s : std_logic;
	signal sw_rst_fmc0_n_s : std_logic;

	signal start_timer_reset_s   : std_logic := '0';
	signal start_timer_counter_s : integer := 0;	
	
	signal init_done_s          : std_logic := '0';
	signal dma_rdy_s            : std_logic := '0';
	signal start_init_counter_s : integer := 0;	
	
	-- LCLK from GN4124 used as system clock
	signal l_clk : std_logic;

	-- Dedicated clock for GTP transceiver
	signal gtp_clk_s : std_logic;

	-- GN4124
	signal gn4124_status_s  : std_logic_vector(31 downto 0);
	signal p2l_pll_locked_s	: std_logic;
	
	-- DDR3
	signal ddr3_status_s     : std_logic_vector(31 downto 0);
	signal ddr3_calib_done_s : std_logic;

	-- Wishbone buse(s) from crossbar master port(s)
	signal cnx_master_out : t_wishbone_master_out_array(c_NUM_WB_MASTERS-1 downto 0);
	signal cnx_master_in  : t_wishbone_master_in_array(c_NUM_WB_MASTERS-1 downto 0);

	-- Wishbone buse(s) to crossbar slave port(s)
	signal cnx_slave_out : t_wishbone_slave_out_array(c_NUM_WB_SLAVES-1 downto 0);
	signal cnx_slave_in  : t_wishbone_slave_in_array(c_NUM_WB_SLAVES-1 downto 0);

	-- Wishbone address from GN4124 core (32-bit word address)
	signal gn_wb_adr : std_logic_vector(31 downto 0);

	-- Wishbone address from to DMA controller (32-bit word address)
	signal dma_ctrl_wb_adr : std_logic_vector(31 downto 0);

	-- GN4124 core DMA port to DDR wishbone bus
	signal wb_dma_adr   : std_logic_vector(31 downto 0);
	signal wb_dma_dat_i : std_logic_vector(31 downto 0);
	signal wb_dma_dat_o : std_logic_vector(31 downto 0);
	signal wb_dma_sel   : std_logic_vector(3 downto 0);
	signal wb_dma_cyc   : std_logic;
	signal wb_dma_stb   : std_logic;
	signal wb_dma_we    : std_logic;
	signal wb_dma_ack   : std_logic;
	signal wb_dma_stall : std_logic;
	signal wb_dma_err   : std_logic;
	signal wb_dma_rty   : std_logic;
	signal wb_dma_int   : std_logic;

	-- FMC BTrain core to DDR wishbone bus
	signal wb_ddr_adr   : std_logic_vector(31 downto 0);
	signal wb_ddr_dat_o : std_logic_vector(63 downto 0);
	signal wb_ddr_sel   : std_logic_vector(7 downto 0);
	signal wb_ddr_cyc   : std_logic;
	signal wb_ddr_stb   : std_logic;
	signal wb_ddr_we    : std_logic;
	signal wb_ddr_ack   : std_logic;
	signal wb_ddr_stall : std_logic;

	-- Interrupts stuff
	signal dma_irq_s           : std_logic_vector(1 downto 0);
	signal irq_sources_s       : std_logic_vector(31 downto 0);
	signal irq_to_gn4124_s     : std_logic;
--	signal irq_sources_2_led   : std_logic_vector(31 downto 0);
	signal ddr_wr_fifo_empty   : std_logic;
	signal ddr_wr_fifo_empty_d : std_logic;
	signal ddr_wr_fifo_empty_p : std_logic;
	signal acq_end_irq_p       : std_logic_vector(1 downto 0);

	signal mem_range_s      : std_logic;
	signal acqu_end_s       : std_logic_vector(1 downto 0);
	signal acq_end          : std_logic_vector(1 downto 0);
	signal last_acq_end     : std_logic_vector(1 downto 0);
	signal acqu_end_force_s : std_logic;

	signal wb_dma_dat_i_test : std_logic_vector(31 downto 0);
	signal wb_dma_ack_test   : std_logic;
	signal wb_dma_stall_test : std_logic;

	signal not_aux_button_1_s	: std_logic;

	-- Carrier 1-wire
	signal carrier_owr_en : std_logic_vector(0 downto 0);
	signal carrier_owr_i  : std_logic_vector(0 downto 0);

	--Peak-detector
	signal ADC1_D_s   : std_logic; 
	signal ADC1_DCO_s : std_logic; 
	signal ADC2_D_s   : std_logic; 
	signal ADC2_DCO_s : std_logic; 
	
	signal ADC_CNV_s  : std_logic; 
	signal ADC_CLK_s  : std_logic; 
	signal ADC1_CLK_s : std_logic; 
	signal ADC2_CLK_s : std_logic; 

	signal Delay1_s : std_logic_vector(5 downto 0); 
	signal Delay2_s : std_logic_vector(5 downto 0); 

	signal ADC1_CLK_delay_s : std_logic;
	signal ADC2_CLK_delay_s : std_logic;

	signal C0_LED_s     : std_logic; 
	signal WIN1_s       : std_logic; 
	signal WIN2_s       : std_logic; 
	signal PPD1_s       : std_logic; 
	signal PPD2_s       : std_logic; 
	signal TRIG1_OK_s   : std_logic; 
	signal TRIG2_OK_s   : std_logic; 
	signal TRIG1_MISS_s : std_logic; 
	signal TRIG2_MISS_s : std_logic; 
	
	signal C0_PULSE_in_s : std_logic;

	signal reset_hw_s    : std_logic;
	signal reset_sw_s    : std_logic;
	signal en_hw_reset_s : std_logic;	

	signal Eff_Sim_bit_s : std_logic;

	signal SYS_DIO_in_s  : std_logic_vector(NB_SYS_IO-1 downto 0);
	signal SYS_DIO_out_s : std_logic_vector(NB_SYS_IO-1 downto 0);

	signal reg_dio_dir_s     : std_logic_vector(NB_SYS_IO-1 downto 0);
	signal reg_dio_cfg_s     : std_logic_vector(NB_SYS_IO-1 downto 0);
	signal not_reg_dio_dir_s : std_logic_vector(NB_SYS_IO-1 downto 0);

	signal ram_data_wr_s    : std_logic_vector(31 downto 0);
	signal ram_data_rd_s    : std_logic_vector(31 downto 0);
	signal ram_addr_s       : std_logic_vector(13 downto 0);
	signal ram_addr_mux_s    : std_logic_vector(13 downto 0);
	signal ram_wr_s         : std_logic;
	signal ram_addr_count_s : unsigned(6 downto 0);

	attribute keep : string;
	
	attribute keep of ram_data_wr_s : signal is "TRUE";
	attribute keep of ram_data_rd_s : signal is "TRUE";
	attribute keep of ram_addr_s    : signal is "TRUE";
	attribute keep of ram_wr_s      : signal is "TRUE";
	attribute keep of ram_addr_mux_s : signal is "TRUE";
	
	attribute keep of irq_sources_s : signal is "TRUE";
		
	attribute keep of ram_addr_count_s : signal is "TRUE";
	
	attribute keep of acqu_end_force_s : signal is "TRUE";
	
	attribute keep of wb_ddr_adr   : signal is "TRUE";
	attribute keep of wb_ddr_dat_o : signal is "TRUE";
	attribute keep of wb_ddr_sel   : signal is "TRUE";
	attribute keep of wb_ddr_cyc   : signal is "TRUE";
	attribute keep of wb_ddr_stb   : signal is "TRUE";
	attribute keep of wb_ddr_we    : signal is "TRUE";
	attribute keep of wb_ddr_ack   : signal is "TRUE";
	attribute keep of wb_ddr_stall : signal is "TRUE";
		
	attribute keep of wb_dma_adr   : signal is "TRUE";
	attribute keep of wb_dma_dat_i : signal is "TRUE";
	attribute keep of wb_dma_dat_o : signal is "TRUE";
	attribute keep of wb_dma_sel   : signal is "TRUE";
	attribute keep of wb_dma_cyc   : signal is "TRUE";
	attribute keep of wb_dma_stb   : signal is "TRUE";
	attribute keep of wb_dma_we    : signal is "TRUE";
	attribute keep of wb_dma_ack   : signal is "TRUE";
	attribute keep of wb_dma_stall : signal is "TRUE";
	
	attribute keep of wb_dma_dat_i_test : signal is "TRUE";
	attribute keep of wb_dma_ack_test : signal is "TRUE";
	attribute keep of wb_dma_stall_test : signal is "TRUE";

	attribute keep of cnx_master_out : signal is "TRUE";
	attribute keep of cnx_master_in  : signal is "TRUE";
	attribute keep of cnx_slave_in   : signal is "TRUE";
	attribute keep of cnx_slave_out  : signal is "TRUE";	
	 
begin

  ------------------------------------------------------------------------------
  -- Clocks distribution from 80MHz oscillator
  --  20.000 MHz system clock
  --  50.000 MHz system clock
  -- 125.000 MHz system clock
  -- 250.000 MHz system clock
  -- 333.333 MHz DDR3 clock
  -- 500.000 MHz system clock
  ------------------------------------------------------------------------------
	
   IBUFDS_ADC_CLK_s : IBUFGDS
   generic map (
      DIFF_TERM => TRUE, 		-- Differential Termination 
      IBUF_LOW_PWR => TRUE,	-- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD => "LVDS_25")
   port map (
      O => ADC_CLK_s,	-- Clock buffer output
      I => ADC_CLK_P_i,	-- Diff_p clock buffer input
      IB => ADC_CLK_N_i	-- Diff_n clock buffer input
   );

  cmp_sys_clk_buf : IBUFG
    port map (
      I => clk_20MHz_i,
      O => sys_clk_in_cryst_s);

  cmp_sys_clk_cryst_pll : PLL_BASE
    generic map (
      BANDWIDTH          => "OPTIMIZED",
      CLK_FEEDBACK       => "CLKFBOUT",
      COMPENSATION       => "INTERNAL",
      DIVCLK_DIVIDE      => 4,
      CLKFBOUT_MULT      => 50,
      CLKFBOUT_PHASE     => 0.000,
      CLKOUT0_DIVIDE     => 8,
      CLKOUT0_PHASE      => 0.000,
      CLKOUT0_DUTY_CYCLE => 0.500,
      CLKOUT1_DIVIDE     => 4,
      CLKOUT1_PHASE      => 0.000,
      CLKOUT1_DUTY_CYCLE => 0.500,
      CLKOUT2_DIVIDE     => 3,
      CLKOUT2_PHASE      => 0.000,
      CLKOUT2_DUTY_CYCLE => 0.500,
      CLKOUT3_DIVIDE     => 2,
      CLKOUT3_PHASE      => 0.000,
      CLKOUT3_DUTY_CYCLE => 0.500,
      CLKOUT4_DIVIDE     => 50,
      CLKOUT4_PHASE      => 0.000, 
      CLKOUT4_DUTY_CYCLE => 0.500,
      CLKOUT5_DIVIDE     => 20,
      CLKOUT5_PHASE      => 0.000, 
      CLKOUT5_DUTY_CYCLE => 0.500,
      CLKIN_PERIOD       => 12.5,
      REF_JITTER         => 0.016)
    port map (
      CLKFBOUT => sys_clk_fb_s,
      CLKOUT0  => sys_clk_125_buf_s,
      CLKOUT1  => sys_clk_250_buf_s,
      CLKOUT2  => ddr_clk_buf_s,
      CLKOUT3  => sys_clk_500_buf_s,
      CLKOUT4  => sys_clk_20_buf_s,
      CLKOUT5  => sys_clk_50_buf_s,
      LOCKED   => sys_clk_pll_locked_s,
      RST      => '0',
      CLKFBIN  => sys_clk_fb_s,
      CLKIN    => ADC_CLK_s);

  cmp_clk_20_buf : BUFG
    port map (
      O => sys_clk_20_s,
      I => sys_clk_20_buf_s);

  cmp_clk_50_buf : BUFG
    port map (
      O => sys_clk_50_s,
      I => sys_clk_50_buf_s);

  cmp_clk_125_buf : BUFG
    port map (
      O => sys_clk_125_s,
      I => sys_clk_125_buf_s);

  cmp_clk_250_buf : BUFG
    port map (
      O => sys_clk_250_s,
      I => sys_clk_250_buf_s);

  cmp_clk_500_buf : BUFG
    port map (
      O => sys_clk_500_s,
      I => sys_clk_500_buf_s);

  cmp_ddr_clk_buf : BUFG
    port map (
      O => ddr_clk_s,
      I => ddr_clk_buf_s);
			
 	------------------------------------------------------------------------------
	-- ADC input - output
	------------------------------------------------------------------------------
  IBUFDS_ADC1_D_s : IBUFDS
   generic map (
      DIFF_TERM => TRUE, 		-- Differential Termination 
      IBUF_LOW_PWR => FALSE,	-- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD => "LVDS_25")
   port map (
      O => ADC1_D_s,			-- Buffer output
      I => ADC1_D_P_i,		-- Diff_p buffer input (connect directly to top-level port)
      IB => ADC1_D_N_i		-- Diff_n buffer input (connect directly to top-level port)
   );	
	
   IBUFGDS_ADC1_DCO_s : IBUFGDS
   generic map (
      DIFF_TERM => TRUE, 		-- Differential Termination 
      IBUF_LOW_PWR => FALSE,	-- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD => "LVDS_25")
   port map (
      O => ADC1_DCO_s,		-- Buffer output
      I => ADC1_DCO_P_i,	-- Diff_p buffer input (connect directly to top-level port)
      IB => ADC1_DCO_N_i	-- Diff_n buffer input (connect directly to top-level port)
   );
	
   IBUFDS_ADC2_D_s : IBUFDS
   generic map (
      DIFF_TERM => TRUE, 		-- Differential Termination 
      IBUF_LOW_PWR => FALSE,	-- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD => "LVDS_25")
   port map (
      O => ADC2_D_s,			-- Buffer output
      I => ADC2_D_P_i,		-- Diff_p buffer input (connect directly to top-level port)
      IB => ADC2_D_N_i		-- Diff_n buffer input (connect directly to top-level port)
   );

   IBUFGDS_ADC2_DCO_s : IBUFGDS
   generic map (
      DIFF_TERM => TRUE, 		-- Differential Termination 
      IBUF_LOW_PWR => FALSE,	-- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD => "LVDS_25")
   port map (
      O => ADC2_DCO_s,		-- Buffer output
      I => ADC2_DCO_P_i,	-- Diff_p buffer input (connect directly to top-level port)
      IB => ADC2_DCO_N_i	-- Diff_n buffer input (connect directly to top-level port)
   );

	--Differential Output
   OBUFDS_ADC1_CLK_s : OBUFDS
   generic map (
      IOSTANDARD => "LVDS_25")
   port map (
      O => ADC1_CLK_P_o,	-- Diff_p output (connect directly to top-level port)
      OB => ADC1_CLK_N_o,	-- Diff_n output (connect directly to top-level port)
--      I => ADC1_CLK_delay_s-- Buffer input 
      I => ADC1_CLK_s-- Buffer input 
   );
--	cmp_delay_adc1_clk : DelayLine 
--	generic map(
--		 REG_DELAY_LONG => 6
--	)
--	port map( 
--		line_i 		=> ADC1_CLK_s,
--		reg_delay_i	=> Delay1_s,
--		line_o 		=> ADC1_CLK_delay_s
--	);
	
   OBUFDS_ADC2_CLK_s : OBUFDS
   generic map (
      IOSTANDARD => "LVDS_25")
   port map (
      O => ADC2_CLK_P_o,	-- Diff_p output (connect directly to top-level port)
      OB => ADC2_CLK_N_o,	-- Diff_n output (connect directly to top-level port)
      I => ADC2_CLK_s-- Buffer input 
--      I => ADC2_CLK_delay_s-- Buffer input 
   );
	
--	cmp_delay_adc2_clk : DelayLine 
--	generic map(
--		 REG_DELAY_LONG => 6
--	)
--	port map( 
--		line_i 		=> ADC2_CLK_s,
--		reg_delay_i	=> Delay2_s,
--		line_o 		=> ADC2_CLK_delay_s
--	);
   OBUFDS_ADC_CNV_s : OBUFDS
   generic map (
      IOSTANDARD => "LVDS_25")
   port map (
      O => ADC_CNV_P_o,		-- Diff_p output (connect directly to top-level port)
      OB => ADC_CNV_N_o,	-- Diff_n output (connect directly to top-level port)
      I => ADC_CNV_s			-- Buffer input 
   );
				
  ------------------------------------------------------------------------------
  -- Local clock from gennum LCLK
  ------------------------------------------------------------------------------
  cmp_l_clk_buf : IBUFDS
    generic map (
      DIFF_TERM    => false,            -- Differential Termination
      IBUF_LOW_PWR => true,             -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "DEFAULT")
    port map (
      O  => l_clk,                      -- Buffer output
      I  => L_CLKp,                     -- Diff_p buffer input (connect directly to top-level port)
      IB => L_CLKn                      -- Diff_n buffer input (connect directly to top-level port)
      );

	------------------------------------------------------------------------------
	-- Power On reset, reset button, PCI reset
	------------------------------------------------------------------------------
	U_Reset_Gen : spec_reset_gen
		port map (
			clk_sys_i        => sys_clk_125_s,
			rst_pcie_n_a_i   => L_RST_N,
			rst_button_n_a_i => aux_buttons_i(0),
			rst_n_o          => but_pci_rst_n_s
		);

	process (sys_clk_125_s)
	begin
		if rising_edge(sys_clk_125_s) then
			-- Start reset system
			if (start_timer_counter_s>=START_RESET_TIMER and c_G_SIMULATON=0) then
				-- Hard/Software general system reset
				if (Eff_Sim_bit_s=STATUS_SIMUL and 
						((reset_n_hw_i='0' and en_hw_reset_s='1')or(reset_sw_s='1'))) then
					reset_hw_s <= '1';
					start_timer_reset_s   <= '0';
					start_timer_counter_s <= 0;
				else
					reset_hw_s <= '0';
					start_timer_reset_s   <= '1';
					start_timer_counter_s <= start_timer_counter_s;
				end if;
			elsif (start_timer_counter_s>=START_RESET_TIMER_SIM and c_G_SIMULATON=1) then 
				-- Hard/Software general system reset
				if (Eff_Sim_bit_s=STATUS_SIMUL and 
						((reset_n_hw_i='0' and en_hw_reset_s='1')or(reset_sw_s='1'))) then
					reset_hw_s <= '1';
					start_timer_reset_s   <= '0';
					start_timer_counter_s <= 0;
				else
					reset_hw_s <= '0';
					start_timer_reset_s   <= '1';
					start_timer_counter_s <= start_timer_counter_s;
				end if;
			else
				start_timer_counter_s <= start_timer_counter_s+1;
			end if;
			reset_n_s <= but_pci_rst_n_s and sys_clk_pll_locked_s and start_timer_reset_s;
			reset_s <= not(but_pci_rst_n_s and sys_clk_pll_locked_s and start_timer_reset_s);

			-- Start init system
			if (((start_init_counter_s>=START_INIT_TIMER and c_G_SIMULATON=0)or
					(start_init_counter_s>=START_INIT_TIMER_SIM and c_G_SIMULATON=1)) and dma_rdy_s='1') then
				init_done_s <= '1';
				start_init_counter_s <= start_init_counter_s;
			else
				init_done_s <= '0';
				start_init_counter_s <= start_init_counter_s+1;
			end if;
		end if;
	end process;

	------------------------------------------------------------------------------
	-- GN4124 interface
	------------------------------------------------------------------------------
	cmp_gn4124_core : gn4124_core
	port map(
		rst_n_a_i       => L_RST_N,
		status_o        => gn4124_status_s,
		-- P2L Direction Source Sync DDR related signals
		p2l_clk_p_i     => P2L_CLKp,
		p2l_clk_n_i     => P2L_CLKn,
		p2l_data_i      => P2L_DATA,
		p2l_dframe_i    => P2L_DFRAME,
		p2l_valid_i     => P2L_VALID,
		-- P2L Control
		p2l_rdy_o       => P2L_RDY,
		p_wr_req_i      => P_WR_REQ,
		p_wr_rdy_o      => P_WR_RDY,
		rx_error_o      => RX_ERROR,
		-- L2P Direction Source Sync DDR related signals
		l2p_clk_p_o     => L2P_CLKp,
		l2p_clk_n_o     => L2P_CLKn,
		l2p_data_o      => L2P_DATA,
		l2p_dframe_o    => L2P_DFRAME,
		l2p_valid_o     => L2P_VALID,
		l2p_edb_o       => L2P_EDB,
		-- L2P Control
		l2p_rdy_i       => L2P_RDY,
		l_wr_rdy_i      => L_WR_RDY,
		p_rd_d_rdy_i    => P_RD_D_RDY,
		tx_error_i      => TX_ERROR,
		vc_rdy_i        => VC_RDY,

		-- Interrupt interface
		dma_irq_o       => dma_irq_s,
		irq_p_i         => irq_to_gn4124_s,
		irq_p_o         => GPIO(0),
		-- DMA registers wishbone interface (slave classic)
		dma_reg_clk_i   => sys_clk_125_s,
		dma_reg_adr_i   => dma_ctrl_wb_adr,
		dma_reg_dat_i   => cnx_master_out(c_SLAVE_DMA).dat,
		dma_reg_sel_i   => cnx_master_out(c_SLAVE_DMA).sel,
		dma_reg_stb_i   => cnx_master_out(c_SLAVE_DMA).stb,
		dma_reg_we_i    => cnx_master_out(c_SLAVE_DMA).we,
		dma_reg_cyc_i   => cnx_master_out(c_SLAVE_DMA).cyc,
		dma_reg_dat_o   => cnx_master_in(c_SLAVE_DMA).dat,
		dma_reg_ack_o   => cnx_master_in(c_SLAVE_DMA).ack,
		dma_reg_stall_o => cnx_master_in(c_SLAVE_DMA).stall,
		-- CSR wishbone interface (master pipelined)
		csr_clk_i       => sys_clk_125_s,
		csr_adr_o       => gn_wb_adr,
		csr_dat_o       => cnx_slave_in(c_MASTER_GENNUM).dat,
		csr_sel_o       => cnx_slave_in(c_MASTER_GENNUM).sel,
		csr_stb_o       => cnx_slave_in(c_MASTER_GENNUM).stb,
		csr_we_o        => cnx_slave_in(c_MASTER_GENNUM).we,
		csr_cyc_o       => cnx_slave_in(c_MASTER_GENNUM).cyc,
		csr_dat_i       => cnx_slave_out(c_MASTER_GENNUM).dat,
		csr_ack_i       => cnx_slave_out(c_MASTER_GENNUM).ack,
		csr_stall_i     => cnx_slave_out(c_MASTER_GENNUM).stall,
      csr_err_i       => cnx_slave_out(c_MASTER_GENNUM).err,
      csr_rty_i       => cnx_slave_out(c_MASTER_GENNUM).rty,
      csr_int_i       => cnx_slave_out(c_MASTER_GENNUM).int,
		-- DMA wishbone interface (pipelined)
		dma_clk_i       => sys_clk_125_s,
		dma_adr_o       => wb_dma_adr,
		dma_dat_o       => wb_dma_dat_o,
		dma_sel_o       => wb_dma_sel,
		dma_stb_o       => wb_dma_stb,
		dma_we_o        => wb_dma_we,
		dma_cyc_o       => wb_dma_cyc,
		dma_dat_i       => wb_dma_dat_i,
		dma_ack_i       => wb_dma_ack,
		dma_stall_i     => wb_dma_stall,
      dma_err_i       => cnx_master_in(c_SLAVE_DMA).err,
      dma_rty_i       => cnx_master_in(c_SLAVE_DMA).rty,
      dma_int_i       => cnx_master_in(c_SLAVE_DMA).int
	);

	p2l_pll_locked_s <= gn4124_status_s(0);
	
	-- Convert 32-bit word address into byte address for crossbar
	cnx_slave_in(c_MASTER_GENNUM).adr <= gn_wb_adr(29 downto 0) & "00";

	-- Convert 32-bit byte address into word address for DMA controller
	dma_ctrl_wb_adr <= "00" & cnx_master_out(c_SLAVE_DMA).adr(31 downto 2);

	-- Unused wishbone signals
	cnx_master_in(c_SLAVE_DMA).err <= '0';
	cnx_master_in(c_SLAVE_DMA).rty <= '0';
	cnx_master_in(c_SLAVE_DMA).int <= '0';

	------------------------------------------------------------------------------
	-- CSR wishbone crossbar
	------------------------------------------------------------------------------

	cmp_sdb_crossbar : xwb_sdb_crossbar
	 generic map (
		g_num_masters => c_NUM_WB_SLAVES,
		g_num_slaves  => c_NUM_WB_MASTERS,
		g_registered  => true,
		g_wraparound  => true,
		g_layout      => c_INTERCONNECT_LAYOUT,
		g_sdb_addr    => c_SDB_ADDRESS)

	 port map (
		clk_sys_i => sys_clk_125_s,
		rst_n_i   => reset_n_s,
		slave_i   => cnx_slave_in,
		slave_o   => cnx_slave_out,
		master_i  => cnx_master_in,
		master_o  => cnx_master_out);

	------------------------------------------------------------------------------
	-- Carrier 1-wire master
	--    DS18B20 (thermometer + unique ID)
	------------------------------------------------------------------------------
	cmp_carrier_onewire : xwb_onewire_master
	 generic map(
		g_interface_mode      => CLASSIC,
		g_address_granularity => BYTE,
		g_num_ports           => 1,
		g_ow_btp_normal       => "5.0",
		g_ow_btp_overdrive    => "1.0"
		)
	 port map(
		clk_sys_i => sys_clk_125_s,
		rst_n_i   => reset_n_s,

		slave_i => cnx_master_out(c_SLAVE_ONEWIRE),
		slave_o => cnx_master_in(c_SLAVE_ONEWIRE),
		desc_o  => open,

		owr_pwren_o => open,
		owr_en_o    => carrier_owr_en,
		owr_i       => carrier_owr_i
		);

	carrier_one_wire_b <= '0' when carrier_owr_en(0) = '1' else 'Z';
	carrier_owr_i(0)   <= carrier_one_wire_b;

	------------------------------------------------------------------------------
	-- Carrier CSR
	--    Carrier type and PCB version
	--    Bitstream (firmware) type and date
	--    Release tag
	--    VCXO DAC control (CLR_N)
	------------------------------------------------------------------------------
	cmp_carrier_csr : carrier_csr
	 port map(
		rst_n_i                          => reset_n_s,
		clk_sys_i                        => sys_clk_125_s,
      wb_adr_i                         => cnx_master_out(c_SLAVE_SPEC_CSR).adr(3 downto 2),  -- cnx_master_out.adr is byte address
      wb_dat_i                         => cnx_master_out(c_SLAVE_SPEC_CSR).dat,
      wb_dat_o                         => cnx_master_in(c_SLAVE_SPEC_CSR).dat,
      wb_cyc_i                         => cnx_master_out(c_SLAVE_SPEC_CSR).cyc,
      wb_sel_i                         => cnx_master_out(c_SLAVE_SPEC_CSR).sel,
      wb_stb_i                         => cnx_master_out(c_SLAVE_SPEC_CSR).stb,
      wb_we_i                          => cnx_master_out(c_SLAVE_SPEC_CSR).we,
      wb_ack_o                         => cnx_master_in(c_SLAVE_SPEC_CSR).ack,
      wb_stall_o                       => open,
		carrier_csr_carrier_pcb_rev_i    => pcb_ver_i,
		carrier_csr_carrier_reserved_o   => open,
		carrier_csr_carrier_type_i       => c_CARRIER_TYPE,
		carrier_csr_stat_fmc_pres_i      => fmc0_prsnt_m2c_n_i,
		carrier_csr_stat_p2l_pll_lck_i   => p2l_pll_locked_s,
		carrier_csr_stat_sys_pll_lck_i   => sys_clk_pll_locked_s,
		carrier_csr_stat_ddr3_cal_done_i => ddr3_calib_done_s,
		carrier_csr_stat_reserved_o      => open,
		carrier_csr_ctrl_led_o           => aux_leds_o,
		carrier_csr_ctrl_reserved_o      => open,
      carrier_csr_rst_fmc0_n_o         => sw_rst_fmc0_n_s,
      carrier_csr_rst_reserved_o       => open
	);

	-- Unused wishbone signals
	cnx_master_in(c_SLAVE_SPEC_CSR).err   <= '0';
	cnx_master_in(c_SLAVE_SPEC_CSR).rty   <= '0';
	cnx_master_in(c_SLAVE_SPEC_CSR).stall <= '0';
	cnx_master_in(c_SLAVE_SPEC_CSR).int   <= '0';

	------------------------------------------------------------------------------
	-- Interrupt controller
	------------------------------------------------------------------------------
	cmp_irq_controller : irq_controller
	 port map(
		clk_i   => sys_clk_125_s,
		rst_n_i => reset_n_s,

		irq_src_p_i => irq_sources_s,

		irq_p_o => irq_to_gn4124_s,

      wb_adr_i => cnx_master_out(c_SLAVE_INT).adr(3 downto 2),  -- cnx_master_out.adr is byte address
      wb_dat_i => cnx_master_out(c_SLAVE_INT).dat,
      wb_dat_o => cnx_master_in(c_SLAVE_INT).dat,
      wb_cyc_i => cnx_master_out(c_SLAVE_INT).cyc,
      wb_sel_i => cnx_master_out(c_SLAVE_INT).sel,
      wb_stb_i => cnx_master_out(c_SLAVE_INT).stb,
      wb_we_i  => cnx_master_out(c_SLAVE_INT).we,
      wb_ack_o => cnx_master_in(c_SLAVE_INT).ack
	);

	-- Unused wishbone signals
	cnx_master_in(c_SLAVE_INT).err   <= '0';
	cnx_master_in(c_SLAVE_INT).rty   <= '0';
	cnx_master_in(c_SLAVE_INT).stall <= '0';
	cnx_master_in(c_SLAVE_INT).int   <= '0';

	-- IRQ sources
	--   0    -> End of DMA transfer
	--   1    -> DMA transfer error
	--   2    -> End of acquisition block 0
	--   3    -> End of acquisition block 1
	--   4-31 -> Unused
	irq_sources_s(1 downto 0)  <= dma_irq_s;
	irq_sources_s(3 downto 2)  <= acq_end_irq_p;
	irq_sources_s(31 downto 4) <= (others => '0');

	-- Detects end of adc core writing to ddr
	p_ddr_wr_fifo_empty : process (sys_clk_125_s)
	begin
	 if rising_edge(sys_clk_125_s) then
		ddr_wr_fifo_empty_d <= ddr_wr_fifo_empty;
	 end if;
	end process p_ddr_wr_fifo_empty;

	ddr_wr_fifo_empty_p <= ddr_wr_fifo_empty and not(ddr_wr_fifo_empty_d);

	-- End of acquisition interrupt generation
	p_acq_end : process (sys_clk_125_s)
	begin
	 if rising_edge(sys_clk_125_s) then
		if reset_s = '1' then
		  acq_end <= (others=>'0');
		elsif acqu_end_s /= "00" then
		  acq_end <= acqu_end_s;
		elsif acqu_end_force_s = '1' and last_acq_end = "01" then --to test
		  acq_end <= "10";
		  last_acq_end <= "10";
		elsif acqu_end_force_s = '1' and last_acq_end = "10" then --to test
		  acq_end <= "01";
		  last_acq_end <= "01";
		else                               --to test
		  acq_end <= (others=>'0');
		end if;
	 end if;
	end process p_acq_end;

	acq_end_irq_p <= acq_end;--to test

	-- IRQ leds
--	gen_irq_led : for I in 0 to 3 generate
--	 cmp_irq_led : gc_extend_pulse
--		generic map (
--		  g_width => 5000000)
--		port map (
--		  clk_i      => sys_clk_125_s,
--		  rst_n_i    => reset_s,
--		  pulse_i    => irq_sources_s(I),
--		  extended_o => led_s(I));
--	end generate gen_irq_led;	

	------------------------------------------------------------------------------
	-- DMA wishbone bus slaves
	--  -> DDR3 controller
	------------------------------------------------------------------------------
	cmp_ddr_ctrl : ddr3_ctrl
	 generic map(
		g_BANK_PORT_SELECT   => "SPEC_BANK3_64B_32B",
		g_MEMCLK_PERIOD      => 3000,
		g_SIMULATION         => "FALSE",--g_SIMULATION,
		g_CALIB_SOFT_IP      => "TRUE",--g_CALIB_SOFT_IP,
		g_P0_MASK_SIZE       => 8,
--		g_P0_DATA_PORT_SIZE  => 65,
		g_P0_DATA_PORT_SIZE  => 64,
		g_P0_BYTE_ADDR_WIDTH => 30,
		g_P1_MASK_SIZE       => 4,
		g_P1_DATA_PORT_SIZE  => 32,
		g_P1_BYTE_ADDR_WIDTH => 30)
	 port map (
		clk_i   => ddr_clk_s,
		rst_n_i => reset_n_s,

		status_o => ddr3_status_s,

		ddr3_dq_b     => DDR3_DQ,
		ddr3_a_o      => DDR3_A,
		ddr3_ba_o     => DDR3_BA,
		ddr3_ras_n_o  => DDR3_RAS_N,
		ddr3_cas_n_o  => DDR3_CAS_N,
		ddr3_we_n_o   => DDR3_WE_N,
		ddr3_odt_o    => DDR3_ODT,
		ddr3_rst_n_o  => DDR3_RESET_N,
		ddr3_cke_o    => DDR3_CKE,
		ddr3_dm_o     => DDR3_LDM,
		ddr3_udm_o    => DDR3_UDM,
		ddr3_dqs_p_b  => DDR3_LDQS_P,
		ddr3_dqs_n_b  => DDR3_LDQS_N,
		ddr3_udqs_p_b => DDR3_UDQS_P,
		ddr3_udqs_n_b => DDR3_UDQS_N,
		ddr3_clk_p_o  => DDR3_CK_P,
		ddr3_clk_n_o  => DDR3_CK_N,
		ddr3_rzq_b    => DDR3_RZQ,
		ddr3_zio_b    => DDR3_ZIO,

		wb0_clk_i   => sys_clk_125_s,
		wb0_sel_i   => wb_ddr_sel,
		wb0_cyc_i   => wb_ddr_cyc,
		wb0_stb_i   => wb_ddr_stb,
		wb0_we_i    => wb_ddr_we,
		wb0_addr_i  => wb_ddr_adr,
		wb0_data_i  => wb_ddr_dat_o,
		wb0_data_o  => open,
		wb0_ack_o   => wb_ddr_ack,
		wb0_stall_o => wb_ddr_stall,

		p0_cmd_empty_o   => open,
		p0_cmd_full_o    => open,
		p0_rd_full_o     => open,
		p0_rd_empty_o    => open,
		p0_rd_count_o    => open,
		p0_rd_overflow_o => open,
		p0_rd_error_o    => open,
		p0_wr_full_o     => open,
		p0_wr_empty_o    => ddr_wr_fifo_empty,
		p0_wr_count_o    => open,
		p0_wr_underrun_o => open,
		p0_wr_error_o    => open,

		wb1_clk_i   => sys_clk_125_s,
		wb1_sel_i   => wb_dma_sel,--"0000",
		wb1_cyc_i   => wb_dma_cyc,--'0',
		wb1_stb_i   => wb_dma_stb,--'0',
		wb1_we_i    => wb_dma_we,--'0',
		wb1_addr_i  => wb_dma_adr,--x"00000000",
		wb1_data_i  => wb_dma_dat_o,--x"00000000",
		wb1_data_o  => wb_dma_dat_i,--wb_dma_dat_i,--wb_dma_dat_i_test,
		wb1_ack_o   => wb_dma_ack,--wb_dma_ack,--wb_dma_ack_test,--open,
		wb1_stall_o => wb_dma_stall,--open,

		p1_cmd_empty_o   => open,
		p1_cmd_full_o    => open,
		p1_rd_full_o     => open,
		p1_rd_empty_o    => open,
		p1_rd_count_o    => open,
		p1_rd_overflow_o => open,
		p1_rd_error_o    => open,
		p1_wr_full_o     => open,
		p1_wr_empty_o    => open,
		p1_wr_count_o    => open,
		p1_wr_underrun_o => open,
		p1_wr_error_o    => open

		);
	ddr3_calib_done_s <= ddr3_status_s(0);

	-- unused Wishbone signals
	wb_dma_err <= '0';
	wb_dma_rty <= '0';
	wb_dma_int <= '0';

	process (sys_clk_20_s,reset_s)
	begin 
		if rising_edge(sys_clk_20_s) then
			if (reset_s='1') then
				dma_rdy_s <= '0';
			else
				if (wb_dma_stall='0') then
					dma_rdy_s <= '1';
				end if;
			end if;
		end if;
	end process;

	--Peak Detector
   cmp_peakdetector : peak_detect
	generic map(g_simulation => g_simulation)
	port map(
		clk_sys_i   => sys_clk_125_s,
		clk_20MHz_i	=> sys_clk_20_s, 
		clk_50MHz_i	=> sys_clk_50_s, 
		clk_250MHz_i=> sys_clk_250_s, 
		clk_500MHz_i=> sys_clk_500_s, 

		reset_i		  => reset_s, 
		reset_hw_i    => reset_hw_s,
		reset_sw_o    => reset_sw_s,
		en_hw_reset_o => en_hw_reset_s,
		init_done_i   => init_done_s,

		DCM_lock_i	=> reset_n_s,

		ADC2_D 		=> ADC2_D_s, 
		ADC2_DCO 	=> ADC2_DCO_s, 
		ADC1_D 		=> ADC1_D_s, 
		ADC1_DCO 	=> ADC1_DCO_s, 
		PPD1 			=> PPD1_s, 
		PPD2 			=> PPD2_s, 
		

		Eff_Sim_bit_i => Eff_Sim_bit_s,

      wb_csr_adr_i   => cnx_master_out(c_SLAVE_FMC).adr,
      wb_csr_dat_i   => cnx_master_out(c_SLAVE_FMC).dat,
      wb_csr_dat_o   => cnx_master_in(c_SLAVE_FMC).dat,
      wb_csr_cyc_i   => cnx_master_out(c_SLAVE_FMC).cyc,
      wb_csr_sel_i   => cnx_master_out(c_SLAVE_FMC).sel,
      wb_csr_stb_i   => cnx_master_out(c_SLAVE_FMC).stb,
      wb_csr_we_i    => cnx_master_out(c_SLAVE_FMC).we,
      wb_csr_ack_o   => cnx_master_in(c_SLAVE_FMC).ack,
      wb_csr_stall_o => cnx_master_in(c_SLAVE_FMC).stall,

		delay1_o => Delay1_s,
		delay2_o => Delay2_s,

		C0_time_1_o => open,
		C0_time_2_o => open,

		reg_dio_dir_o   => reg_dio_dir_s,
		reg_dio_cfg_o   => reg_dio_cfg_s,
		reg_dio_input_i => SYS_DIO_in_s,
		
		ADC_CNV	 	=> ADC_CNV_s, 
		ADC1_CLK 	=> ADC1_CLK_s, 
		ADC2_CLK 	=> ADC2_CLK_s, 

		C0_LED 		=> C0_LED_s, 

		-- DDR wishbone interface
      wb_ddr_clk_i   => sys_clk_125_s,
      wb_ddr_adr_o   => wb_ddr_adr,
      wb_ddr_dat_o   => wb_ddr_dat_o,
      wb_ddr_sel_o   => wb_ddr_sel,
      wb_ddr_stb_o   => wb_ddr_stb,
      wb_ddr_we_o    => wb_ddr_we,
      wb_ddr_cyc_o   => wb_ddr_cyc,
      wb_ddr_ack_i   => wb_ddr_ack,
      wb_ddr_stall_i => wb_ddr_stall,
		
		--To test
		ram_addr_o => ram_addr_s,
		ram_data_o => ram_data_wr_s,
		ram_wr_o   => ram_wr_s,

		mem_range_o		=> mem_range_s,
		acqu_end_o     => acqu_end_s,
					
		TRIG1_OK 	=> TRIG1_OK_s, 
		TRIG2_OK 	=> TRIG2_OK_s, 
		TRIG1_MISS 	=> TRIG1_MISS_s, 
		TRIG2_MISS 	=> TRIG2_MISS_s, 
		WIN1 			=> WIN1_s,
		WIN2 			=> WIN2_s,

		C0_PULSE 	=> C0_PULSE_in_s,
		DAC1_CLK 	=> DAC1_CLK_o, 
		DAC2_CLK 	=> DAC2_CLK_o, 
		DAC_DATA 	=> DAC_DATA_o
   );

	------------------------------------------------------------------------------
	-- Logical Input - Output configuration
	------------------------------------------------------------------------------

	dio_input : process (sys_clk_125_s)
	begin
		if rising_edge(sys_clk_125_s) then
			if (g_simulation = 0) then
				if (reg_dio_cfg_s(4)='0') then
					C0_PULSE_in_s <= not SYS_DIO_in_s(4);
				else
					C0_PULSE_in_s <= SYS_DIO_in_s(4);
				end if;
				if (reg_dio_cfg_s(5)='0') then
					Eff_Sim_bit_s <= SYS_DIO_in_s(5);  --'1' => effective, '0' => simulated (Default)
				else
					Eff_Sim_bit_s <= not SYS_DIO_in_s(5);  --'0' => effective, '1' => simulated
				end if;
			else
				C0_PULSE_in_s <= '1';
				Eff_Sim_bit_s <= '0';
			end if;
		end if;
	end process;

	dio_output : process (reg_dio_cfg_s,PPD2_s,PPD1_s,WIN2_s,WIN1_s,TRIG2_OK_s,TRIG1_OK_s,TRIG2_MISS_s,TRIG1_MISS_s)
	begin
		if (reg_dio_cfg_s(0)='0') then
			SYS_DIO_out_s(0) <= PPD2_s; 		      --Channel 2 -> TRIG Low
		else
			SYS_DIO_out_s(0) <= not(PPD2_s); 		--Channel 2 -> TRIG Low
		end if;
		if (reg_dio_cfg_s(1)='0') then
			SYS_DIO_out_s(1) <= PPD1_s; 		      --Channel 1 -> TRIG High
		else
			SYS_DIO_out_s(1) <= not(PPD1_s); 		--Channel 1 -> TRIG High
		end if;
		if (reg_dio_cfg_s(2)='0') then
			SYS_DIO_out_s(2) <= WIN2_s;  		      --Channel 2 -> Window Low
		else
			SYS_DIO_out_s(2) <= not(WIN2_s);  		--Channel 2 -> Window Low
		end if;
		if (reg_dio_cfg_s(3)='0') then
			SYS_DIO_out_s(3) <= WIN1_s;  		      --Channel 1 -> Window High
		else
			SYS_DIO_out_s(3) <= not(WIN1_s);  		--Channel 1 -> Window High
		end if;
		-- 4 to 5 used as input
		if (reg_dio_cfg_s(6)='0') then
			SYS_DIO_out_s(6) <= TRIG2_OK_s;	      --RSVD1 bit (used as output to check)
			DO2_DIFF_N_o	  <= TRIG2_OK_s;
			TRIG2_OK_o		  <= TRIG2_OK_s;
		else
			SYS_DIO_out_s(6) <= not(TRIG2_OK_s);	--RSVD1 bit (used as output to check)
			DO2_DIFF_N_o	  <= not(TRIG2_OK_s);
			TRIG2_OK_o		  <= not(TRIG2_OK_s);
		end if;
		if (reg_dio_cfg_s(7)='0') then
			SYS_DIO_out_s(7) <= TRIG1_OK_s;	      --RSVD2 bit (used as output to check)
			DO1_DIFF_N_o	  <= TRIG1_OK_s;
			TRIG1_OK_o       <= TRIG1_OK_s;
		else
			SYS_DIO_out_s(7) <= not(TRIG1_OK_s);	--RSVD2 bit (used as output to check)
			DO1_DIFF_N_o	  <= not(TRIG1_OK_s);
			TRIG1_OK_o       <= not(TRIG1_OK_s);
		end if;
		if (reg_dio_cfg_s(8)='0') then
			SYS_DIO_out_s(8) <= TRIG2_MISS_s;     --RSVD3 bit (used as output to check)
			DO2_DIFF_P_o	  <= TRIG2_MISS_s;
			TRIG2_MISS_o	  <= TRIG2_MISS_s;
		else
			SYS_DIO_out_s(8) <= not(TRIG2_MISS_s);--RSVD3 bit (used as output to check)
			DO2_DIFF_P_o	  <= not(TRIG2_MISS_s);
			TRIG2_MISS_o	  <= not(TRIG2_MISS_s);
		end if;
		if (reg_dio_cfg_s(9)='0') then
			SYS_DIO_out_s(9) <= TRIG1_MISS_s;     --RSVD4 bit (used as output to check)
			DO1_DIFF_P_o	  <= TRIG1_MISS_s;
			TRIG1_MISS_o     <= TRIG1_MISS_s;
		else
			SYS_DIO_out_s(9) <= not(TRIG1_MISS_s);--RSVD4 bit (used as output to check)
			DO1_DIFF_P_o	  <= not(TRIG1_MISS_s);
			TRIG1_MISS_o     <= not(TRIG1_MISS_s);
		end if;
	end process;
--	--Input
--	C0_PULSE_in_s <= SYS_DIO_in_s(4) when reg_dio_cfg_s(i)='0' else reg_dio_cfg_s(i) ;
--	Eff_Sim_bit_s <= SYS_DIO_in_s(5) when (g_simulation = 0) else '1';
--	--Output
--	SYS_DIO_out_s(0) <= PPD2_s; 		--Channel 2 -> TRIG Low
--	SYS_DIO_out_s(1) <= PPD1_s; 		--Channel 1 -> TRIG High
--	SYS_DIO_out_s(2) <= WIN2_s;  		--Channel 2 -> Window Low
--	SYS_DIO_out_s(3) <= WIN1_s;  		--Channel 1 -> Window High
--	
--	SYS_DIO_out_s(6) <= TRIG2_OK_s;	--RSVD1 bit (used as output to check)
--	SYS_DIO_out_s(7) <= TRIG1_OK_s;	--RSVD2 bit (used as output to check)
--	SYS_DIO_out_s(8) <= TRIG2_MISS_s;--RSVD3 bit (used as output to check)
--	SYS_DIO_out_s(9) <= TRIG1_MISS_s;--RSVD4 bit (used as output to check)

	--Direction of IO register
	DIR_DIO_o <= reg_dio_dir_s;

	--Output
	DO0_DIFF_P_o	<= '0';
	DO0_DIFF_N_o	<= '0';
		
	SYS_DIO_gen : for i in 0 to NB_SYS_IO-1 generate
		not_reg_dio_dir_s(i) <= not reg_dio_dir_s(i);
		IOBUF_inst : IOBUF
			generic map (
				DRIVE => 12,
				IOSTANDARD => "DEFAULT",
				SLEW => "SLOW"
			)
			port map (
				O	=> SYS_DIO_in_s(i),	   -- Extern to FPGA
				IO	=> SYS_DIO_io(i),		   -- Buffer inout port (connect directly to top-level port)
				I	=> SYS_DIO_out_s(i),	   -- FPGA to extern
				T	=> not_reg_dio_dir_s(i) --not RegDio_s(i)	-- 3-state enable input, high=extern to FPGA, low=FPGA to extern
			);
	end generate;

	--Data input pulse
	cmp_acqu_end_force : RisingDetect 
		port map(
			clk_i		=> sys_clk_125_s,
			reset_i	=> reset_s,
			
			s_i	=> not_aux_button_1_s,
			s_o	=> acqu_end_force_s
		);
	not_aux_button_1_s <= not aux_buttons_i(1);
	
	--Output LED
	led_red_o   <= not (fmc0_prsnt_m2c_n_i);
	led_green_o <= fmc0_prsnt_m2c_n_i;
	C0_LED_o    <= C0_LED_s;

end Behavioral;

