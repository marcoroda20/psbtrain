----------------------------------------------------------------------------------
-- Company: 		CERN
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    18:20:28 11/01/2012 
-- Design Name: 
-- Module Name:    DelayLine - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;

entity DelayLine is
	generic(
		 REG_DELAY_LONG : integer := 5  -- default: 5 bits
	);
	Port ( 
		line_i 		: in std_logic;
		reg_delay_i	: in std_logic_vector(REG_DELAY_LONG-1 downto 0);
		line_o 		: out std_logic
	);
end DelayLine;

architecture Behavioral of DelayLine is

--Signal declaration and attribute keep
attribute keep : string;
signal late_s : std_logic_vector((2**(REG_DELAY_LONG+1))-3 downto 0);
attribute keep of late_s : signal is "TRUE";

begin

	--Generate inverter
	late_s(0) <= not line_i;
	inverter_gen : for i in 1 to (2**(REG_DELAY_LONG+1))-3 generate
		late_s(i) <= not late_s(i-1); 
	end generate;
	
	--MUX
	process (reg_delay_i,late_s,line_i)
	begin
		if (reg_delay_i = std_logic_vector(conv_unsigned(0,REG_DELAY_LONG))) then
			line_o <= line_i;
		end if;
		for i in 1 to (2**REG_DELAY_LONG)-1 loop
			if (reg_delay_i = std_logic_vector(conv_unsigned(i,REG_DELAY_LONG))) then
				line_o <= late_s(2*i-1);
			end if;
		end loop;
	end process;

end Behavioral;

