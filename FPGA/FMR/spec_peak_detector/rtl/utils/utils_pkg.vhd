----------------------------------------------------------------------------------
-- Company: 		CERN/TE-MS-MM
-- Engineer: 		Daniel Oberson
-- 
-- Description: Package with some utils core
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package utils_pkg is

	function log2_ceil(N : natural) return positive;

	component spec_reset_gen 
	  port (
		 clk_sys_i : in std_logic;

		 rst_pcie_n_a_i   : in std_logic;
		 rst_button_n_a_i : in std_logic;

		 rst_n_o : out std_logic
		 );
	end component;

	component RisingDetect 
		port (
			clk_i			: in std_logic;
			reset_i		: in std_logic;
			
			s_i	: in std_logic;
			s_o	: out std_logic
		);
	end component;

	component monostable 
		generic(
			g_n_clk : integer := 4
		);
		port (
			clk_i			: in std_logic;
			reset_i		: in std_logic;
			
			s_i	: in std_logic;
			s_o	: out std_logic
		);
	end component;

	component DelayLine 
		generic(
			 REG_DELAY_LONG : integer := 5  -- default: 5 bits
		);
		Port ( 
			line_i 		: in std_logic;
			reg_delay_i	: in std_logic_vector(REG_DELAY_LONG-1 downto 0);
			line_o 		: out std_logic
		);
	end component;

	component SynchroGen 
		port(
			clk_i		: in std_logic; 
			reset_i	: in std_logic;

			reg_sample_period_i : in std_logic_vector(25 downto 0);
			send_tick_o       : out std_logic
		);
	end component;

end utils_pkg;

package body utils_pkg is

  -----------------------------------------------------------------------------
  -- Returns log of 2 of a natural number
  -----------------------------------------------------------------------------
  function log2_ceil(N : natural) return positive is
  begin
    if N <= 2 then
      return 1;
    elsif N mod 2 = 0 then
      return 1 + log2_ceil(N/2);
    else
      return 1 + log2_ceil((N+1)/2);
    end if;
  end;

end utils_pkg;
