target = "xilinx"
action = "synthesis"

syn_device = "xc6slx45t"
syn_grade = "-3"
syn_package = "fgg484"
syn_top = "spec_top_fmc_adc_16b_10Ms"
syn_project = "spec_top_fmc_adc_16b_10Ms.xise"

files = ["../spec_top_fmc_peak_detect.ucf"]

modules = { "local" : "../rtl" }
