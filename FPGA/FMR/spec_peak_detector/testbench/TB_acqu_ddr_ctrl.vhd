--------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer:		Daniel Oberson
--
-- Create Date:   14:22:24 13/12/2013
-- Design Name:   
-- Module Name:   /psbtrain/FPGA/FMR/spec_peak_detector/testbench/TB_acqu_ddr_ctrl.vhd
-- Project Name:  spec_peak_detector
-- Target Device:  
-- Tool versions:  
-- Description:   Testbench for control of DDR
-- Additional Comments: To run the testbench with Modelsim type the command:
--                      do run_ddr_ctrl.do
--
--------------------------------------------------------------------------------
LIBRARY ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.std_logic_unsigned.all; 

use std.textio.all;

library work;
use work.peak_detector_pkg.all;
use work.adc_dac_pkg.all;
use work.processing_pkg.all;
use work.dma_pkg.all;
use work.utils_pkg.all;

  ENTITY TB_acqu_ddr_ctrl IS
  END TB_acqu_ddr_ctrl;

  ARCHITECTURE behavior OF TB_acqu_ddr_ctrl IS 

	-- Component Declaration
	signal clk_125_sti : std_logic;
	signal clk_20_sti  : std_logic;
	signal clk_50_sti  : std_logic;
	signal clk_250_sti : std_logic;
	signal clk_500_sti : std_logic;

	signal reset_s   : std_logic;
	signal reset_n_s : std_logic;
	
	--Control
	signal samples_wr_en_sti   : std_logic;
	signal ddr_test_en_sti     : std_logic;		
		
	--Data
	signal sync_data_sti       : std_logic_vector(31 downto 0);
	signal sync_data_valid_sti : std_logic;
	
	signal sync_data_s       : std_logic_vector(31 downto 0);
	signal sync_data_valid_s : std_logic;

	-- DDR wishbone interface
	signal wb_ddr_clk_sti   : std_logic;
	signal wb_ddr_adr_o     : std_logic_vector(31 downto 0);
	signal wb_ddr_dat_o     : std_logic_vector(31 downto 0);
	signal wb_ddr_sel_o     : std_logic_vector(7 downto 0);
	signal wb_ddr_stb_o     : std_logic;
	signal wb_ddr_we_o      : std_logic;
	signal wb_ddr_cyc_o     : std_logic;
	signal wb_ddr_ack_sti   : std_logic;
	signal wb_ddr_stall_sti : std_logic;
		
	signal mem_range_o		: std_logic;
	signal acqu_end_obs     : std_logic_vector(1 downto 0);

	signal ddr_sample_period_s : std_logic_vector(25 downto 0);
	signal ddr_tick_s          : std_logic;
	
	-- Clock period definitions
   constant clk_125_period : time := 8 ns;
   constant clk_20_period : time := 50 ns;
   constant clk_50_period : time := 20 ns;
   constant clk_250_period : time := 4 ns;
   constant clk_500_period : time := 2 ns;

	attribute keep : string;
	attribute keep of acqu_end_obs : signal is "TRUE";

  BEGIN

  -- Component Instantiation
	cmp_acqu_ddr_ctrl : acqu_ddr_ctrl
		generic map(
			g_sim => 0,
			g_DDR_MEMORY_RANGE => 1000
		)
		port map(
			clk_i   => clk_125_sti,
			reset_i => reset_s,
			
			--Control
			samples_wr_en_i   => samples_wr_en_sti,
			ddr_test_en_i     => ddr_test_en_sti,
			
			--Data
			sync_data_i       => sync_data_s,--=> sync_data_sti,
			sync_data_valid_i => sync_data_valid_s,--=> sync_data_valid_sti,

			-- DDR wishbone interface
			wb_ddr_clk_i   => clk_125_sti,
			wb_ddr_adr_o   => wb_ddr_adr_o,
			wb_ddr_dat_o   => wb_ddr_dat_o,
			wb_ddr_sel_o   => wb_ddr_sel_o,
			wb_ddr_stb_o   => wb_ddr_stb_o,
			wb_ddr_we_o    => wb_ddr_we_o,
			wb_ddr_cyc_o   => wb_ddr_cyc_o,
			wb_ddr_ack_i   => wb_ddr_ack_sti,
			wb_ddr_stall_i => wb_ddr_stall_sti,
			
			mem_range_o		=> mem_range_o,
			acqu_end_o     => acqu_end_obs
		);

	cmp_decimator : decimator
	generic map(
      g_DATA_LENGTH  => 32,
      g_DECIM_FACTOR => 25
	)
	port map(
		clk_i		=> clk_125_sti,
		reset_i	=> reset_s,

		data_i	=> sync_data_sti,
		d_rdy_i	=> sync_data_valid_sti,

		data_o	=> sync_data_s,
		d_rdy_o	=> sync_data_valid_s
	);

	--Write to DDR		
	cmp_SynchroGen : SynchroGen
		port map(
			clk_i		=> clk_125_sti,
			reset_i	=> reset_s,

			reg_sample_period_i => ddr_sample_period_s,
			send_tick_o         => ddr_tick_s
		);
	ddr_sample_period_s <= "00"&x"0001F4";--250kS/s 8ns unit

	cmp_monostable : monostable
		generic map(
			g_n_clk => 4 
		)
		port map(
			clk_i		=> clk_125_sti,
			reset_i	=> reset_s,
			
			s_i => ddr_tick_s,
			s_o => sync_data_valid_sti
		);

	
   -- Clock process definitions
   clk_125_process :process
   begin
		clk_125_sti <= '0';
		wait for clk_125_period/2;
		clk_125_sti <= '1';
		wait for clk_125_period/2;
   end process;

   clk_20_process :process
   begin
		clk_20_sti <= '0';
		wait for clk_20_period/2;
		clk_20_sti <= '1';
		wait for clk_20_period/2;
   end process;

   clk_50_process :process
   begin
		clk_50_sti <= '0';
		wait for clk_50_period/2;
		clk_50_sti <= '1';
		wait for clk_50_period/2;
   end process;

   clk_250_process :process
   begin
		clk_250_sti <= '0';
		wait for clk_250_period/2;
		clk_250_sti <= '1';
		wait for clk_250_period/2;
   end process;

   clk_500_process :process
   begin
		clk_500_sti <= '0';
		wait for clk_500_period/2;
		clk_500_sti <= '1';
		wait for clk_500_period/2;
   end process;

	reset_n_s <= reset_s;
	
	--  Test Bench Statements
	tb : PROCESS
	BEGIN

		--Control
		samples_wr_en_sti  <= '1';
		ddr_test_en_sti    <= '1';
			
		--Data
		sync_data_sti <= x"12345678";

		-- DDR wishbone interface
		wb_ddr_clk_sti   <= clk_125_sti;
		wb_ddr_ack_sti   <= '1';
		wb_ddr_stall_sti <= '0';

		reset_s <= '1';
		wait for 100 ns; -- wait until global set/reset completes
		reset_s <= '0';

		-- Add user defined stimulus here
		while (acqu_end_obs="00") loop	
			wait for clk_125_period/2;	
		end loop;	
		
--		reset_s <= '1';
--		wait for 100 ns; -- wait until global set/reset completes
--		reset_s <= '0';
		
		wait; -- will wait forever
		
	END PROCESS tb;
  --  End Test Bench 

  END;
