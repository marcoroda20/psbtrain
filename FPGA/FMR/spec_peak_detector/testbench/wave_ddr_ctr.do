onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_acqu_ddr_ctrl/reset_s
add wave -noupdate -divider Input
add wave -noupdate /tb_acqu_ddr_ctrl/sync_data_valid_sti
add wave -noupdate /tb_acqu_ddr_ctrl/samples_wr_en_sti
add wave -noupdate /tb_acqu_ddr_ctrl/ddr_test_en_sti
add wave -noupdate -divider Inside
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/rst_n_s
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/sync_data_valid_s
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/sync_data_i
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/switch_mem_range_s
add wave -noupdate -radix unsigned /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/ram_addr_cnt_s
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/g_sim
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/acqu_end_s
add wave -noupdate /tb_acqu_ddr_ctrl/clk_125_sti
add wave -noupdate /tb_acqu_ddr_ctrl/acqu_end_obs
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/switch_test_val_s
add wave -noupdate -divider WB
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/wb_ddr_fifo_din
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/wb_ddr_fifo_dout
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/wb_ddr_fifo_empty
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/wb_ddr_fifo_full
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/wb_ddr_fifo_wr
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/wb_ddr_fifo_rd
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/wb_ddr_fifo_valid
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/wb_ddr_fifo_dreq
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/wb_ddr_fifo_wr_en
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/wb_ddr_adr_o
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/wb_ddr_dat_o
add wave -noupdate /tb_acqu_ddr_ctrl/cmp_acqu_ddr_ctrl/wb_ddr_sel_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {20521060711 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 451
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ps} {840 us}
