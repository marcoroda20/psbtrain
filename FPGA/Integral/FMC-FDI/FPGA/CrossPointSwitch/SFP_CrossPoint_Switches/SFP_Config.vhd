----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:42:15 07/03/2013 
-- Design Name: 
-- Module Name:    SFP_Config - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
-- library UNISIM;
-- use UNISIM.VComponents.all;

entity SFP_Config is

    Port ( lvcmos_wr_sel : in  STD_LOGIC;
	        reset : in STD_LOGIC;
	        clk_20mhz : in STD_LOGIC;
           lvcmos_sel0 : out  STD_LOGIC;
           lvcmos_sel1 : out  STD_LOGIC;
           o_sfp_ok : out  STD_LOGIC;
           o_sfp_fail : out  STD_LOGIC;
           s_sfp_ok : out  STD_LOGIC;
           s_sfp_fail : out  STD_LOGIC;
           cl_sfp_ok : out  STD_LOGIC;
           cl_sfp_fail : out  STD_LOGIC;
           ol_sfp_ok : out  STD_LOGIC;
           ol_sfp_fail : out  STD_LOGIC;
           o_s_led : out  STD_LOGIC;
           cl_sfp_los : in  STD_LOGIC;
           cl_sfp_rate_sel : out  STD_LOGIC;
           cl_sfp_tx_disable : out  STD_LOGIC;
           cl_sfp_tx_fault : in  STD_LOGIC;
           cl_sfp_mod_def0 : in  STD_LOGIC;
           cl_sfp_mod_def1 : out  STD_LOGIC;
           cl_sfp_mod_def2 : inout  STD_LOGIC;
           ol_sfp_mod_def0 : in  STD_LOGIC;
           ol_sfp_mod_def1 : out  STD_LOGIC;
           ol_sfp_mod_def2 : inout  STD_LOGIC;
           ol_sfp_los : in  STD_LOGIC;
           ol_sfp_rate_sel : out  STD_LOGIC;
           ol_sfp_tx_disable : out  STD_LOGIC;
           ol_sfp_tx_fault : in  STD_LOGIC;
           o_sfp_tx_fault : in  STD_LOGIC;
           o_sfp_tx_disable : out  STD_LOGIC;
           o_sfp_rate_sel : out  STD_LOGIC;
           o_sfp_los : in  STD_LOGIC;
           o_sfp_mod_def2 : inout  STD_LOGIC;
           o_sfp_mod_def1 : out  STD_LOGIC;
           o_sfp_mod_def0 : in  STD_LOGIC;
           s_sfp_mod_def2 : inout  STD_LOGIC;
           s_sfp_mod_def1 : out  STD_LOGIC;
           s_sfp_mod_def0 : in  STD_LOGIC;
           s_sfp_tx_fault : in  STD_LOGIC;
           s_sfp_tx_disable : out  STD_LOGIC;
           s_sfp_rate_sel : out  STD_LOGIC;
           s_sfp_los : in  STD_LOGIC );
			  
end SFP_Config;

architecture Behavioral of SFP_Config is

signal en_tff1, div1, clk_80khz : std_logic := '0';

-- This subtype will constrain counter width. 
-- Tell to the synthesizer how many bits wide the generated counter should be.
subtype divtype250 is natural range 0 to 249; -- 0 to n-1 = 0 to 250-1
signal cnt250: divtype250;

signal lvcmossel0 : std_logic :='0'; 
signal lvcmossel1 : std_logic :='1';
signal o_fault, s_fault, ol_fault, cl_fault : std_logic :='0';
--signal ow_nrd, sw_nrd, olw_nrd, clw_nrd : std_logic :='0';

begin

--------------------------------------------- � 250 clock divider ---------------------------------------------------			 

process(clk_20mhz, reset, cnt250)
begin
   if ( reset = '1' ) then
           cnt250 <= 0;
   elsif rising_edge(clk_20mhz) then -- n - 1 = 250 - 1 = 249
       if (cnt250 = 249) then        -- Counter count to 249 on the rising edge of clk_20mhz
		           cnt250 <= 0;          
		 else 
		           cnt250 <= cnt250 + 1;
       end if;
   end if;
end process;

-- Enable the Toggle Flip-Flop Div2

en_tff1 <= '1' when ( ( cnt250 = 0 ) or ( cnt250 = 125 ) ) else '0'; -- 0 or n/2 = 0 or 250 /2

process(clk_20mhz, reset, en_tff1, div1)
  begin
    if ( RESET = '1' ) then 
	        div1 <= '0';
    elsif rising_edge(clk_20mhz) then
       if (en_tff1 = '1') then 
		       div1 <= not(div1);
	    end if;
	 end if;
end process;

clk_80khz <= div1;

----------------------------------------------------------------------------------------------------------------------

process (clk_20mhz)
begin
   if clk_20mhz'event and clk_20mhz = '1' then
	   if lvcmos_wr_sel = '1' then -- spare selected
		   lvcmossel0 <= '1';
			lvcmossel1 <= '0';
			o_s_led <= '1';
		else
		   lvcmossel0 <= '0'; -- operational selected
			lvcmossel1 <= '1';
			o_s_led <= '0';
		end if;
	end if;
end process;

lvcmos_sel0 <= lvcmossel0;
lvcmos_sel1 <= lvcmossel1;

-- sfp_tx_fault : High = laser fault
-- sfp_mod_def0 : Low : SFP module is present
-- sfp_los : High = received optical power is below the worst case receiver sensitivity

o_fault <= o_sfp_tx_fault or o_sfp_los or o_sfp_mod_def0;
o_sfp_ok <= not o_fault;
o_sfp_fail <= o_fault;
o_sfp_rate_sel <= '1'; -- receiver full bandwidth
o_sfp_tx_disable <= '1'; -- transmitter OFF
o_sfp_mod_def1 <= '0';--clk_80khz; -- I2C serial interface clock

--ow_nrd <= '0';
o_sfp_mod_def2 <= 'Z';-- when ow_nrd = '0' else 'Z'; -- I2C serial interface bidirectional data
--o_sfp_mod_def2_in <= o_sfp_mod_def2; 

s_fault <= s_sfp_tx_fault or s_sfp_los or s_sfp_mod_def0;
s_sfp_ok <= not s_fault;
s_sfp_fail <= s_fault;
s_sfp_rate_sel <= '1'; -- receiver full bandwidth
s_sfp_tx_disable <= '1'; -- transmitter OFF
s_sfp_mod_def1 <= '0';--clk_80khz; -- I2C serial interface clock

--sw_nrd <= '0';
s_sfp_mod_def2 <= 'Z';-- when sw_nrd = '0' else 'Z'; -- I2C serial interface bidirectional data
--s_sfp_mod_def2_in <= s_sfp_mod_def2;


ol_fault <= ol_sfp_tx_fault or ol_sfp_los or ol_sfp_mod_def0;
ol_sfp_ok <= not ol_fault;
ol_sfp_fail <= ol_fault;
ol_sfp_rate_sel <= '1'; -- receiver full bandwidth
ol_sfp_tx_disable <= '1'; -- transmitter OFF
ol_sfp_mod_def1 <= '0';--clk_80khz; -- I2C serial interface clock

--olw_nrd <= '0';
ol_sfp_mod_def2 <= 'Z'; -- when olw_nrd = '0' else 'Z'; -- I2C serial interface bidirectional data
--ol_sfp_mod_def2_in <= ol_sfp_mod_def2;


cl_fault <= cl_sfp_tx_fault or cl_sfp_los or cl_sfp_mod_def0;
cl_sfp_ok <= not cl_fault;
cl_sfp_fail <= cl_fault;
cl_sfp_rate_sel <= '1'; -- receiver full bandwidth
cl_sfp_tx_disable <= '1'; -- transmitter OFF
cl_sfp_mod_def1 <= '0';--clk_80khz; -- I2C serial interface clock

--clw_nrd <= '0';
cl_sfp_mod_def2 <= 'Z';-- when clw_nrd = '0' else 'Z'; -- I2C serial interface bidirectional data
--cl_sfp_mod_def2_in <= cl_sfp_mod_def2;

end Behavioral;

