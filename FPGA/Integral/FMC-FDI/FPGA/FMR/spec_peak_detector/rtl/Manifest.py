files = [
  #ADC DAC
  "../rtl/adc_dac/adc_dac_pkg.vhd",
  "../rtl/adc_dac/AD7626sm.vhd",
  "../rtl/adc_dac/DAC_Cal_2Chan.vhd",
  "../rtl/adc_dac/DAC_mux_output.vhd",
  #processing    
  "../rtl/processing/processing_pkg.vhd",
  "../rtl/processing/ADC_DIFF_DACsm.vhd",
  "../rtl/processing/Biquad_s16b_c18b.vhd",
  "../rtl/processing/Differentiator.vhd",
  "../rtl/processing/PPD_Win.vhd",
  "../rtl/processing/c0_time.vhd",
#  "../rtl/processing/RC_Filter.vhd",
#  "../rtl/processing/RC_Filter_TBW.vhd"
  #sdb
  "../rtl/sdb/sdb_meta_pkg.vhd",
  #dma
  "../rtl/dma/dma_pkg.vhd",
  "../rtl/dma/acqu_ddr_ctrl.vhd",
  "../rtl/dma/decimator.vhd",
  #wbgen
  "../rtl/wbgen/peak_detect_reg.vhd",
  "../rtl/wbgen/carrier_csr.vhd",
  "../rtl/wbgen/irq_controller_regs.vhd",  
  #utils
  "../rtl/utils/utils_pkg.vhd",
  "../rtl/utils/spec_reset_gen.vhd",
  "../rtl/utils/DelayLine.vhd",
  "../rtl/utils/RisingDetect.vhd",
  "../rtl/utils/SynchroGen.vhd",
  "../rtl/utils/monostable.vhd",
  "../rtl/peak_detector_pkg.vhd",
  "../rtl/spec_top_fmc_adc_16b_10Ms.vhd",
#  "../rtl/PeakDetectorTop.vhd",
  "../rtl/peak_detect.vhd",
  "../rtl/irq_controller.vhd",

  "../rtl/testreg.vhd",
  "../rtl/dma/BlockRAM128x32.vhd",
  "../rtl/dma/BlockRAM2Kx32.vhd",
  "../rtl/dma/BlockRAM16Kx32.vhd"
]
         
modules = {
  "git" : [ "git://ohwr.org/hdl-core-lib/general-cores.git::proposed_master",
            "git://ohwr.org/hdl-core-lib/ddr3-sp6-core.git::spec_bank3_64b_32b",
            "git://ohwr.org/hdl-core-lib/gn4124-core.git::master" ]
}

fetchto="../ip_cores"

