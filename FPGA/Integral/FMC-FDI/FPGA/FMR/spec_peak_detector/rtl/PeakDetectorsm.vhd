----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC/MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    16:37:32 10/04/2012 
-- Design Name: 
-- Module Name:    PeakDetectorsm - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

library work;
use work.peak_detector_pkg.all;
use work.adc_dac_pkg.all;
use work.processing_pkg.all;
use work.dma_pkg.all;
use work.utils_pkg.all;

entity PeakDetectorsm is
	generic(g_simulation : integer := 0);
	port ( 
		ADC1_D       : in std_logic; 
		ADC1_DCO     : in std_logic; 
		ADC2_D       : in std_logic; 
		ADC2_DCO     : in std_logic;
		clk_sys_i    : in std_logic;
		clk_20MHz_i  : in std_logic; 
		clk_50MHz_i  : in std_logic; 
		clk_250MHz_i : in std_logic; 
		clk_500MHz_i : in std_logic; 
		C0_PULSE     : in std_logic; 
		Eff_Sim_bit_i: in std_logic;
		DAC1_sel     : in std_logic_vector (2 downto 0); 
		DAC2_sel     : in std_logic_vector (2 downto 0); 
		DCM_lock_i   : in std_logic; 
		reset_i      : in std_logic; 
		ADC_CNV      : out std_logic; 
		ADC1_CLK     : out std_logic; 
		ADC2_CLK     : out std_logic; 
		C0_LED       : out std_logic; 
		DAC_DATA     : out std_logic_vector (15 downto 0); 
		DAC1_CLK     : out std_logic; 
		DAC2_CLK     : out std_logic; 
		PPD1         : out std_logic; 
		PPD2         : out std_logic; 
		--Register
		ADC_data_1_o 		: out std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
		ADC_data_rdy_1_o 	: out std_logic;
		ADC_data_2_o 		: out std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
		ADC_data_rdy_2_o 	: out std_logic;
		DAC_data_1_o 		: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
		DAC_data_2_o 		: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
		DAC_prog_1_i 		: in std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
		DAC_prog_2_i 		: in std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
		Threshold1   		: in std_logic_vector (NB_BIT_THRESHOLD-1 downto 0); 
		Threshold2   		: in std_logic_vector (NB_BIT_THRESHOLD-1 downto 0); 
		TrigWidth_1_i		: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		TrigWidth_2_i		: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		WinDelay_1_i		: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		WinDelay_2_i		: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		WinWidth_1_i		: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		WinWidth_2_i		: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		EnWin_1_i			: in std_logic;
		EnWin_2_i			: in std_logic;
		EnTrigSim_1_i		: in std_logic;
		EnTrigSim_2_i		: in std_logic;
		TrigSimDelay_1_i	: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		TrigSimDelay_2_i	: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		
		TrigForcedDelay_1_i	: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		TrigForcedDelay_2_i	: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);

		C0_time_1_o : out std_logic_vector(31 downto 0);
		C0_time_2_o : out std_logic_vector(31 downto 0);

		-- DDR wishbone interface
		wb_ddr_clk_i   : in  std_logic;
		wb_ddr_adr_o   : out std_logic_vector(31 downto 0);
		wb_ddr_dat_o   : out std_logic_vector(63 downto 0);
		wb_ddr_sel_o   : out std_logic_vector(7 downto 0);
		wb_ddr_stb_o   : out std_logic;
		wb_ddr_we_o    : out std_logic;
		wb_ddr_cyc_o   : out std_logic;
		wb_ddr_ack_i   : in  std_logic;
		wb_ddr_stall_i : in  std_logic;
		
		samples_wr_en_i: in  std_logic;
		ddr_test_en_i  : in  std_logic;
--		dma_running_i  : in  std_logic;
		
		--To test
		ram_addr_o : out std_logic_vector(13 downto 0);
		ram_data_o : out std_logic_vector(31 downto 0);
		ram_wr_o   : out std_logic;

		mem_range_o		: out std_logic;
		acqu_end_o     : out std_logic_vector(1 downto 0);

		--Trig
		no_trig_1_o	: out std_logic;
		no_trig_2_o	: out std_logic;
		
		TRIG1_MISS   : out   std_logic; 
		TRIG1_OK     : out   std_logic; 
		TRIG2_MISS   : out   std_logic; 
		TRIG2_OK     : out   std_logic; 
		WIN1         : out   std_logic; 
		WIN2         : out   std_logic
	);
end PeakDetectorsm;

architecture BEHAVIORAL of PeakDetectorsm is
   
	constant c_DDR_COUNTER_SIM : Integer := 12; -- 0.096us when clk_sys_i = 125MHz

	--Signal Declaration
   signal ADC_CLK_s			: std_logic;
   signal ADC_1_DATA_BUS	: std_logic_vector (15 downto 0);
   signal DIFF_data_1_s    : std_logic_vector (15 downto 0);
   signal ADC_1_data_rdy_s	: std_logic;
   signal ADC_2_DATA_BUS	: std_logic_vector (15 downto 0);
   signal DIFF_data_2_s    : std_logic_vector (15 downto 0);
   signal ADC_2_data_rdy_s	: std_logic;
   signal DAC_1_DATA_BUS	: std_logic_vector (15 downto 0);
   signal DAC_2_DATA_BUS	: std_logic_vector (15 downto 0);
   signal DAC_1_LOAD			: std_logic;
   signal DAC_2_LOAD			: std_logic;
   signal Pulse1				: std_logic;
   signal Pulse2				: std_logic;
   signal PulseOutput1		: std_logic;
   signal PulseOutput2		: std_logic;
	
	signal TRIG1_MISS_s     : std_logic;
	signal TRIG2_MISS_s     : std_logic;
	
	--DDR
	signal data_rdy_decim_s : std_logic;
	
	signal ddr_sample_period_s : std_logic_vector(25 downto 0);
	signal ddr_tick_s          : std_logic;
	signal ddr_tick_long_s     : std_logic;

	signal counter_sim_ddr_s   : integer;
	signal data_sim_ddr_s      : unsigned(63 downto 0);
   signal data_before_decim_s : std_logic_vector(63 downto 0);
	signal data_after_decim_s  : std_logic_vector(63 downto 0);
   signal data_ddr_rdy_s      : std_logic;
	
	attribute keep : string;
	attribute keep of C0_PULSE: signal is "TRUE";

	attribute keep of Pulse1: signal is "TRUE";
	attribute keep of PulseOutput1: signal is "TRUE";
	attribute keep of TRIG1_OK: signal is "TRUE";
	attribute keep of TRIG1_MISS: signal is "TRUE";
	attribute keep of WIN1: signal is "TRUE";

	attribute keep of Pulse2: signal is "TRUE";
	attribute keep of PulseOutput2: signal is "TRUE";
	attribute keep of TRIG2_OK: signal is "TRUE";
	attribute keep of TRIG2_MISS: signal is "TRUE";
	attribute keep of WIN2: signal is "TRUE";

begin

	cmp_ADC_DIFF_DAC : ADC_DIFF_DACsm
		port map(
			clk_20MHz_i		=> clk_20MHz_i,
			clk_50MHz_i		=> clk_50MHz_i,
			clk_500MHz_i	=> clk_500MHz_i,
			clk_250MHz_i	=> clk_250MHz_i,
			
			DCM_lock_i		=> DCM_lock_i,

			reset_i			=> reset_i,

			ADC_DCO_1_i		=> ADC1_DCO,
			ADC_D_1_i		=> ADC1_D,
			
			ADC_data_1_o		=> ADC_1_DATA_BUS,
			DIFF_data_1_o		=> DIFF_data_1_s,
			ADC_data_rdy_1_o	=> ADC_1_data_rdy_s,
			
			ADC_DCO_2_i		=> ADC2_DCO,
			ADC_D_2_i		=> ADC2_D,
			
			ADC_data_2_o		=> ADC_2_DATA_BUS,
			DIFF_data_2_o		=> DIFF_data_2_s,
			ADC_data_rdy_2_o	=> ADC_2_data_rdy_s,
					
			ADC_CNV_o		=> ADC_CNV,
			ADC_CLK_o		=> ADC_CLK_s,
			
			threshold_1_i	=> Threshold1,
			threshold_2_i	=> Threshold2,
			
			DAC_sel_1_i		=> DAC1_sel,
			DAC_sel_2_i		=> DAC2_sel,
			
			DAC_prog_data_1_i	=> DAC_prog_1_i,
			DAC_data_bus_1_o	=> DAC_1_DATA_BUS,
			DAC_load_1_o		=> DAC_1_LOAD,
			
			DAC_prog_data_2_i	=> DAC_prog_2_i,
			DAC_data_bus_2_o	=> DAC_2_DATA_BUS,
			DAC_load_2_o		=> DAC_2_LOAD,
			
			peak_pulse_1_o	=> Pulse1,
			peak_pulse_2_o	=> Pulse2
		);
			
	ADC1_CLK <= ADC_CLK_s;
	ADC2_CLK <= ADC_CLK_s;

   PPD1 <= PulseOutput1;
   PPD2 <= PulseOutput2;

	--Output ADC data for the register				 
	ADC_data_1_o <= ADC_1_DATA_BUS;	
	ADC_data_rdy_1_o <= ADC_1_data_rdy_s;	
	ADC_data_2_o <= ADC_2_DATA_BUS;
	ADC_data_rdy_2_o <= ADC_2_data_rdy_s;

	--Output DAC data for the register				 
	DAC_data_1_o <= DAC_1_DATA_BUS;		 
	DAC_data_2_o <= DAC_2_DATA_BUS;

	--C0 and Windows generation
	C0_LED <= C0_PULSE;--Active low pulse

	cmp_ppd_win_1 : PPD_Win
		 port map( 
			clk_50MHz_i => clk_50MHz_i,
			reset_i 		=> reset_i,
			
			pulse_i 		=> Pulse1,
			pulse_o 		=> PulseOutput1,
			C0_pulse_i	=> C0_PULSE,
			--Register
			eff_sim_bit_i		=> Eff_Sim_bit_i,
			pulse_width_i		=> TrigWidth_1_i,
			win_delay_i			=> WinDelay_1_i,
			win_width_i			=> WinWidth_1_i,
			en_win_i				=> EnWin_1_i,
			en_trig_sim_i		=> EnTrigSim_1_i,
			trig_sim_delay_i	=> TrigSimDelay_1_i,
			trig_forc_delay_i	=> TrigForcedDelay_1_i,
						
			no_trig_o	=> no_trig_1_o,
			pulse_ok_o	=> TRIG1_OK,
			pulse_fail_o=> TRIG1_MISS_s,
			window_o		=> WIN1			
		);

	-- Time after C0 of trig 1 (unit : 1/clk_20MHz_i=>50ns)
	cmp_c0_time_1 : c0_time
		port map(
			clk_i   => clk_20MHz_i,
			reset_i => reset_i,
			
			c0_pulse_i   => C0_PULSE,
			trig_pulse_i => PulseOutput1,
--			trig_miss_i  => TRIG2_MISS_s,
			
			c0_time_o => C0_time_1_o
		);
		
	cmp_ppd_win_2 : PPD_Win
		 port map( 
			clk_50MHz_i => clk_50MHz_i,
			reset_i 		=> reset_i,
			
			pulse_i 		=> Pulse2,
			pulse_o 		=> PulseOutput2,
			C0_pulse_i	=> C0_PULSE,
			--Register
			eff_sim_bit_i		=> Eff_Sim_bit_i,
			pulse_width_i		=> TrigWidth_2_i,
			win_delay_i			=> WinDelay_2_i,
			win_width_i			=> WinWidth_2_i,
			en_win_i				=> EnWin_2_i,
			en_trig_sim_i		=> EnTrigSim_2_i,
			trig_sim_delay_i	=> TrigSimDelay_2_i,
			trig_forc_delay_i	=> TrigForcedDelay_2_i,
			
			no_trig_o	=> no_trig_2_o,
			pulse_ok_o	=> TRIG2_OK,
			pulse_fail_o=> TRIG2_MISS_s,
			window_o		=> WIN2
		);

	-- Time after C0 of trig 2 (unit : 1/clk_20MHz_i=>50ns)
	cmp_c0_time_2 : c0_time
		port map(
			clk_i   => clk_20MHz_i,
			reset_i => reset_i,
			
			c0_pulse_i   => C0_PULSE,
			trig_pulse_i => PulseOutput2,
--			trig_miss_i  => TRIG2_MISS_s,
			
			c0_time_o => C0_time_2_o
		);

   cmp_dacmux : DAC_mux_output
      port map (
			clk_50MHz_i		=>clk_50MHz_i,
			reset_i			=>reset_i,
			
			DAC1_data_bus_i=>DAC_1_DATA_BUS,
			DAC1_load_i		=>DAC_1_LOAD,
			DAC2_data_bus_i=>DAC_2_DATA_BUS,
			DAC2_load_i		=>DAC_2_LOAD,
			DAC_data_bus_o	=>DAC_DATA,
			load1_o			=>DAC1_CLK,
			load2_o			=>DAC2_CLK
		);

--	-- Process to choose diagnostic values
--	process (clk_sys_i)
--	begin
--		if rising_edge(clk_sys_i) then
--			if (reset_i='1') then
--				diag_data_s <= (others=>'0');
--			else
--				case diag_reg_i is
--					when IDLE =>
--						diag_data_s <= DIFF_data_1_s;
--					when others =>
--						diag_data_s <= DIFF_data_2_s;
--				end case;
--			end if;
--		end if;
--	end process

	-- Process to simulate acquisition in Modelsim
	process (clk_sys_i,reset_i)
	begin
		if rising_edge(clk_sys_i) then
			if (reset_i='1') then
				data_before_decim_s <= (others=>'0');
				data_ddr_rdy_s	     <= '0';
				counter_sim_ddr_s   <= 0;
				data_sim_ddr_s      <= (others=>'0');
			else
				if (g_simulation = 0) then
					data_before_decim_s	<= DIFF_data_2_s & DIFF_data_1_s & ADC_2_DATA_BUS & ADC_1_DATA_BUS;
					data_ddr_rdy_s	<= ADC_1_data_rdy_s;
					counter_sim_ddr_s <= 0;
				else
					data_before_decim_s <= std_logic_vector(data_sim_ddr_s);
					if (counter_sim_ddr_s < (c_DDR_COUNTER_SIM-1)) then
						counter_sim_ddr_s <= counter_sim_ddr_s+1;
						data_ddr_rdy_s	<= '0';
					else
						counter_sim_ddr_s <= 0;
						data_ddr_rdy_s	<= '1';
						data_sim_ddr_s <= data_sim_ddr_s + 1;
					end if;
				end if;
			end if;
		end if;
	end process;

	--Write to DDR		
	cmp_decimator : decimator 
		generic map(
			g_DATA_LENGTH  => 64,
			g_DECIM_FACTOR => 200--400--40--to test --200
		)
		port map(
			clk_i		=> clk_sys_i,
			reset_i	=> reset_i,
			
--			disable_decim_i => wb_ddr_stall_i,

			data_i	=> data_before_decim_s,
			d_rdy_i	=> data_ddr_rdy_s,

			data_o	=> data_after_decim_s,
			d_rdy_o	=> data_rdy_decim_s
		);
	
	cmp_acqu_ddr_ctrl : acqu_ddr_ctrl 
		generic map(
			g_sim => g_simulation,
			g_DDR_MEMORY_RANGE => 360000 --2048--360000--16--8192--360000--16--50000 --360000
		)
		port map(
			clk_i   => clk_sys_i,
			reset_i => reset_i,
			
			--Control
			samples_wr_en_i   => samples_wr_en_i,
			ddr_test_en_i     => ddr_test_en_i,
--			dma_running_i     => dma_running_i,
			
			--Data
--			diag_data_i       => diag_data_s,
			sync_data_i       => data_after_decim_s,
			sync_data_valid_i => data_rdy_decim_s,--ddr_tick_long_s,--ddr_tick_s,--ADC_1_data_rdy_s,
			C0_i					=> C0_PULSE,

			-- DDR wishbone interface
			wb_ddr_clk_i   => wb_ddr_clk_i,
			wb_ddr_adr_o   => wb_ddr_adr_o,
			wb_ddr_dat_o   => wb_ddr_dat_o,
			wb_ddr_sel_o   => wb_ddr_sel_o,
			wb_ddr_stb_o   => wb_ddr_stb_o,
			wb_ddr_we_o    => wb_ddr_we_o,
			wb_ddr_cyc_o   => wb_ddr_cyc_o,
			wb_ddr_ack_i   => wb_ddr_ack_i,
			wb_ddr_stall_i => wb_ddr_stall_i,

			--To test
			ram_addr_o => ram_addr_o,
			ram_data_o => ram_data_o,
			ram_wr_o   => ram_wr_o,

			mem_range_o => mem_range_o,
			acqu_end_o  => acqu_end_o
		);
   					 
end BEHAVIORAL;


