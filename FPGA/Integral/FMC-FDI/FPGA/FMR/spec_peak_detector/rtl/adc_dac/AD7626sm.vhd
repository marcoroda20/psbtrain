----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC/MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    09:05:55 10/03/2012 
-- Design Name: 
-- Module Name:    AD7626sm - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;


entity AD7626sm is
	Port ( 
	clk_500_i 	: in std_logic;
	clk_250_i 	: in std_logic;
	reset_i		: in std_logic;

	delay_clk_i : std_logic_vector(3 downto 0);

	DCO_1_i		: in  std_logic;
	D_1_i			: in  std_logic;

	DCO_2_i		: in  std_logic;
	D_2_i			: in  std_logic;

	adc_clk_o	: out  std_logic;
	adc_cnv_o	: out  std_logic;

	DAC_data_1_o		: out  std_logic_vector(15 downto 0);
	ADC_data_1_o		: out  std_logic_vector(15 downto 0);
	data_rdy_1_o		: out  std_logic;

	DAC_data_2_o		: out  std_logic_vector(15 downto 0);
	ADC_data_2_o		: out  std_logic_vector(15 downto 0);
	data_rdy_2_o		: out  std_logic
	);		  
end AD7626sm;

architecture Behavioral of AD7626sm is

--Constant declaration
constant MAX_CNT_GENERAL: integer := 24;
constant START_SEQUENCE	: integer := 0;
constant NB_ADC_CLK		: integer := 16;
constant T_CNVH			: integer := 6; --units = 1/clk_used
constant T_WAIT_LATCH	: integer := 14; --units = 1/clk_used

	
--Type declaration
type adc_states_t is (
	IDLE,
	CNV_CMD,
	WAIT_LATCH,
	LATCH_ADC_VALUE,
	WAIT_ONE_STATE,
	LATCH_ADC_VALUE_OUT
	);

--Signal Declaration
signal state_s : adc_states_t;

signal counter_general_s 			: std_logic_vector(4 downto 0);
signal counter_general_latched_s : std_logic_vector(4 downto 0);

signal adc_cnv_s 				: std_logic;
signal adc_cnv_1_s			: std_logic;
signal adc_cnv_2_s			: std_logic;
signal adc_cnv_3_s			: std_logic;
signal start_adc_clk_s 		: std_logic;
signal latch_cmd_adc_data_s: std_logic;
signal load_data_out_1_s	: std_logic;
signal load_data_out_2_s	: std_logic;

signal load_2_1_s				: std_logic;

signal start_adc_cnv_s					: std_logic;
signal start_adc_clk_latched_s		: std_logic;
signal latch_cmd_adc_data_latched_s	: std_logic;

signal latch_cmd_adc2_data_latched_s: std_logic;

signal counter_adc_clk_s 	: std_logic_vector(4 downto 0);
signal counter_adc_cnv_s 	: std_logic_vector(4 downto 0);

signal serial2paral_1_s	: std_logic_vector(15 downto 0);
signal serial2paral_2_s	: std_logic_vector(15 downto 0);
signal first_adc1_data_s: std_logic;
signal first_adc2_data_s: std_logic;
signal latch_ADC_1_s	: std_logic_vector(15 downto 0);
signal latch_ADC_2_s	: std_logic_vector(15 downto 0);
signal ADC_data_1_s	: std_logic_vector(15 downto 0);
signal ADC_data_2_s	: std_logic_vector(15 downto 0);

signal adc_clk_delay_s : std_logic_vector(15 downto 0);

begin

--Counter 0-24 to synchronize State Machine, unit 4 ns
process (clk_250_i,reset_i,counter_general_s)
begin
   if rising_edge(clk_250_i) then 
		if (reset_i='1') then
			counter_general_s <= (others=>'0');
			counter_general_latched_s <= (others=>'0');
		else
			if (counter_general_s < 
					conv_std_logic_vector(MAX_CNT_GENERAL,counter_general_s'length)) then
				counter_general_s <= counter_general_s+1;
			else
				counter_general_s <= (others=>'0');
			end if;
			counter_general_latched_s <= counter_general_s;
		end if;
   end if;
end process;

--State Machine
process (clk_250_i,reset_i) 
begin
   if rising_edge(clk_250_i) then  
		if (reset_i='1') then
			state_s <= IDLE;
			adc_cnv_s				<= '0';
			start_adc_clk_s		<= '0';
			latch_cmd_adc_data_s	<= '0';
			load_data_out_1_s		<= '0';		
			load_data_out_2_s		<= '0';		
		else
			case state_s is
				when IDLE =>
					if (counter_general_latched_s = 
							conv_std_logic_vector(START_SEQUENCE,counter_general_latched_s'length)) then
						state_s <= CNV_CMD;
					end if;
					adc_cnv_s				<= '0';
					start_adc_clk_s		<= '0';
					latch_cmd_adc_data_s	<= '0';
					load_data_out_1_s		<= '1';		
					load_data_out_2_s		<= '1';		
				when CNV_CMD =>
					if (counter_general_latched_s = 
							conv_std_logic_vector(T_CNVH+1,counter_general_latched_s'length)) then
						state_s <= WAIT_LATCH;
					end if;
					adc_cnv_s				<= '1';
					start_adc_clk_s		<= '1';
					latch_cmd_adc_data_s	<= '0';
					load_data_out_1_s		<= '0';		
					load_data_out_2_s		<= '0';		
				when WAIT_LATCH =>
					if (counter_general_latched_s = 
							conv_std_logic_vector(T_WAIT_LATCH+T_CNVH+1,counter_general_latched_s'length)) then
						state_s <= LATCH_ADC_VALUE;
					end if;
					adc_cnv_s				<= '0';
					start_adc_clk_s		<= '0';
					latch_cmd_adc_data_s	<= '0';
					load_data_out_1_s		<= '0';		
					load_data_out_2_s		<= '0';		
				when LATCH_ADC_VALUE =>
					state_s <= WAIT_ONE_STATE;
					adc_cnv_s				<= '0';
					start_adc_clk_s		<= '0';
					latch_cmd_adc_data_s	<= '0';
					load_data_out_1_s		<= '0';		
					load_data_out_2_s		<= '0';		
				when WAIT_ONE_STATE =>
					state_s <= LATCH_ADC_VALUE_OUT;
					adc_cnv_s				<= '0';
					start_adc_clk_s		<= '0';
					latch_cmd_adc_data_s	<= '1';
					load_data_out_1_s		<= '0';		
					load_data_out_2_s		<= '0';		
				when LATCH_ADC_VALUE_OUT =>
					state_s <= IDLE;
					adc_cnv_s				<= '0';
					start_adc_clk_s		<= '0';
					latch_cmd_adc_data_s	<= '0';
					load_data_out_1_s		<= '1';		
					load_data_out_2_s		<= '1';		
			end case;
		end if;
	end if;
end process;

--Resynchronize output of state machine
process(clk_250_i,reset_i,adc_cnv_s,latch_cmd_adc_data_s,load_2_1_s,
		latch_cmd_adc_data_latched_s,load_data_out_1_s,load_data_out_2_s)
begin
	if rising_edge(clk_250_i) then
		if (reset_i='1') then
			adc_cnv_1_s							<= '0';
			adc_cnv_o							<= '0';
			latch_cmd_adc_data_latched_s	<= '0';
			latch_cmd_adc2_data_latched_s <= '0';
			data_rdy_1_o						<= '0';
			load_2_1_s							<= '0';
			data_rdy_2_o						<= '0';
		else
			adc_cnv_1_s							<= adc_cnv_s;
			adc_cnv_o							<= adc_cnv_1_s;
			latch_cmd_adc_data_latched_s	<= latch_cmd_adc_data_s;
			--one clock shift to compensate difference of pcb line
			latch_cmd_adc2_data_latched_s <= latch_cmd_adc_data_latched_s;
			data_rdy_1_o						<= load_data_out_1_s;
			load_2_1_s							<= load_data_out_2_s;
			data_rdy_2_o						<= load_2_1_s;
		end if;
	end if;
end process;

--ADC_CLK generator (counter unit 2ns)
process (clk_500_i,reset_i,counter_adc_clk_s,start_adc_clk_s)
begin
   if rising_edge(clk_500_i) then 
		if (reset_i='1') then
			counter_adc_clk_s <= (others=>'0');
		else
			if (start_adc_clk_s ='1' or counter_adc_clk_s/=
					conv_std_logic_vector(0,counter_adc_clk_s'length)) then
				counter_adc_clk_s <= counter_adc_clk_s+1;
			else
				counter_adc_clk_s <= (others=>'0');
			end if;
		end if;
   end if;
end process;

--CLK to send to ADC with latency to compensate resynchronisation
process (clk_500_i,reset_i,counter_adc_clk_s(0))
begin 
   if rising_edge(clk_500_i) then 
		if (reset_i='1') then
			adc_clk_o       <= '0';
			adc_clk_delay_s <= (others=>'0');
		else
			adc_clk_delay_s(0) <= counter_adc_clk_s(0); 
			for i in 1 to (adc_clk_delay_s'length)-1 loop
				adc_clk_delay_s(i) <= adc_clk_delay_s(i-1);
			end loop;
			if (delay_clk_i="0000") then
				adc_clk_o <= counter_adc_clk_s(0); 
			else
				adc_clk_o <= adc_clk_delay_s(conv_integer(delay_clk_i));
			end if;
		end if;
	end if;
end process;

--Read the ADC data through DCO and D signals
--Latch the data
process (clk_250_i,reset_i,latch_cmd_adc_data_latched_s,first_adc1_data_s,
			latch_cmd_adc2_data_latched_s,first_adc2_data_s)
begin
	if rising_edge(clk_250_i) then
		if (reset_i='1') then
			latch_ADC_1_s		<= (others=>'0');
			latch_ADC_2_s		<= (others=>'0');
			first_adc1_data_s	<= '0';
			first_adc2_data_s	<= '0';
		else
			if (latch_cmd_adc_data_latched_s='1') then 
				if (first_adc1_data_s='1') then
					latch_ADC_1_s <= serial2paral_1_s;
				end if;
				first_adc1_data_s <= '1';
			end if;
			if (latch_cmd_adc2_data_latched_s='1') then
				if (first_adc2_data_s='1') then 
					latch_ADC_2_s <= serial2paral_2_s;
				end if;
				first_adc2_data_s <= '1';
			end if;
		end if;
	end if;
end process;

--Output
process (clk_250_i,reset_i,latch_ADC_1_s,latch_ADC_2_s)
begin
	if rising_edge(clk_250_i) then
		if (reset_i='1') then
			ADC_data_1_s	<= (others=>'0');
			ADC_data_2_s	<= (others=>'0');
		else
			ADC_data_1_s <=  latch_ADC_1_s;
			ADC_data_2_s <=  latch_ADC_2_s;
		end if;
	end if;
end process;
DAC_data_1_o <=  (not ADC_data_1_s(15) ) & ADC_data_1_s(14 downto 0); --Straight binary format
DAC_data_2_o <=  (not ADC_data_2_s(15) ) & ADC_data_2_s(14 downto 0); --Straight binary format
ADC_data_1_o <=  ADC_data_1_s;
ADC_data_2_o <=  ADC_data_2_s;

--Deserialiser
process (reset_i,DCO_1_i,D_1_i,serial2paral_1_s)
begin
	if rising_edge(DCO_1_i) then
		if (reset_i='1') then
			serial2paral_1_s <= (others=>'0');
		else
			serial2paral_1_s <= serial2paral_1_s(14 downto 0) & D_1_i;   			
		end if;
	end if;
end process;

process (reset_i,DCO_2_i,D_2_i,serial2paral_2_s)
begin
	if rising_edge(DCO_2_i) then
		if (reset_i='1') then
			serial2paral_2_s <= (others=>'0');
		else
			serial2paral_2_s <= serial2paral_2_s(14 downto 0) & D_2_i;   
		end if;
	end if;
end process;

end Behavioral;

