----------------------------------------------------------------------------------
-- Company:			CERN TE/MSC/MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    08:34:17 06/15/2012 
-- Design Name: 
-- Module Name:    DAC_mux_output - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.peak_detector_pkg.all;

entity DAC_mux_output is
	port(
		clk_50MHz_i			: in std_logic;
		reset_i		: in std_logic;
		
		DAC1_data_bus_i	: in std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
		DAC1_load_i			: in std_logic;
		DAC2_data_bus_i	: in std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
		DAC2_load_i			: in std_logic;
		
		DAC_data_bus_o		: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
		load1_o				: out std_logic;
		load2_o				: out std_logic
	);
end DAC_mux_output;

architecture Behavioral of DAC_mux_output is

--Type declaration
type dac_mux_states_t is (
	IDLE,
	LOAD_DATA,
	DAC_CLK1,
	WAIT_STATE,
	DAC_CLK2
	);

	--Signal
	signal state_s	: dac_mux_states_t := IDLE;

	signal DAC1_data_bus_s		: std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
	signal DAC2_data_bus_s		: std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);

begin

--State machine
process (clk_50MHz_i) 
begin
	if rising_edge(clk_50MHz_i) then
		if (reset_i='1') then
			state_s <= IDLE;
			load1_o <= '0';
			load2_o <= '0';
		else
			load1_o <= '0';
			load2_o <= '0';
			DAC_data_bus_o <= DAC1_data_bus_s;
			case state_s is
				when IDLE =>
					if (DAC1_load_i='1' or DAC2_load_i='1') then
						state_s <= LOAD_DATA;	
						DAC1_data_bus_s <= DAC1_data_bus_i;
						DAC2_data_bus_s <= DAC2_data_bus_i;
					end if;
					DAC_data_bus_o <= DAC2_data_bus_s;
				when LOAD_DATA =>
					state_s <= DAC_CLK1;
				when DAC_CLK1 =>
					state_s <= WAIT_STATE;
					load1_o <= '1';
				when WAIT_STATE =>
					state_s <= DAC_CLK2;
					DAC_data_bus_o <= DAC2_data_bus_s;
				when DAC_CLK2 =>
					state_s <= IDLE;
					load2_o <= '1';
					DAC_data_bus_o <= DAC2_data_bus_s;
				when others =>
					state_s <= IDLE;
			end case;
		end if;
	end if;
end process;

end Behavioral;

