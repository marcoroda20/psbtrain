--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package dma_pkg is

	component acqu_ddr_ctrl 
		generic(
			g_sim : integer := 0;
			g_WORD_LENGTH : integer := 64;
			g_DATA_LENGTH : integer := 64
		);
		port(
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			--Control
			samples_wr_en_i : in std_logic;
			ddr_test_en_i   : in std_logic;
			ddr_range_i     : in std_logic_vector(24 downto 0);	
			last_addr_o     : out std_logic_vector(24 downto 0);	
			
			--Data
			sync_data_i       : in std_logic_vector(g_DATA_LENGTH-1 downto 0);
			sync_data_valid_i : in std_logic;
			C0_i              : in std_logic;

			-- DDR wishbone interface
			wb_ddr_clk_i   : in  std_logic;
			wb_ddr_adr_o   : out std_logic_vector(31 downto 0);
			wb_ddr_dat_o   : out std_logic_vector(63 downto 0);
			wb_ddr_sel_o   : out std_logic_vector(7 downto 0);
			wb_ddr_stb_o   : out std_logic;
			wb_ddr_we_o    : out std_logic;
			wb_ddr_cyc_o   : out std_logic;
			wb_ddr_ack_i   : in  std_logic;
			wb_ddr_stall_i : in  std_logic;
			
			--To test
			ram_addr_o : out std_logic_vector(13 downto 0);
			ram_data_o : out std_logic_vector(31 downto 0);
			ram_wr_o   : out std_logic;

			mem_range_o		: out std_logic;
			acqu_end_o     : out std_logic_vector(1 downto 0)
		);
	end component;

	component decimator 
		generic(
			g_DATA_LENGTH  : integer := 16
--			g_KEEP_SIZE  : integer := 8
		);
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;

			decim_factor_i : in std_logic_vector(31 downto 0);
			
			data_i	: in std_logic_vector(g_DATA_LENGTH-1 downto 0);
			d_rdy_i	: in std_logic;

			data_o	: out std_logic_vector(g_DATA_LENGTH-1 downto 0);
			d_rdy_o	: out std_logic
		);
	end component;
	
end dma_pkg;

