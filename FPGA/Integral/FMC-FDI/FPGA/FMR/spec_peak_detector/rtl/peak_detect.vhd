----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC/MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    16:37:32 10/04/2012 
-- Design Name: 
-- Module Name:    peak_detect - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

library work;
use work.peak_detector_pkg.all;
use work.sdb_meta_pkg.all;
use work.adc_dac_pkg.all;
use work.processing_pkg.all;
use work.dma_pkg.all;
use work.wishbone_pkg.all;
use work.utils_pkg.all;

entity peak_detect is
	generic(g_simulation : integer := 0);
	port ( 
		clk_sys_i    : in std_logic;
		clk_20MHz_i  : in std_logic; 
		clk_50MHz_i  : in std_logic; 
		clk_250MHz_i : in std_logic; 
		clk_500MHz_i : in std_logic; 
		reset_i      : in std_logic; 
		init_done_i  : in std_logic;

		C0_PULSE     : in std_logic; 
		
		reset_hw_i    : in std_logic;
		reset_sw_o    : out std_logic;
		en_hw_reset_o : out std_logic;	
		
		Eff_Sim_bit_i : in std_logic;

		-- CSR wishbone interface
		wb_csr_adr_i   : in  std_logic_vector(31 downto 0);
		wb_csr_dat_i   : in  std_logic_vector(31 downto 0);
		wb_csr_dat_o   : out std_logic_vector(31 downto 0);
		wb_csr_cyc_i   : in  std_logic;
		wb_csr_sel_i   : in  std_logic_vector(3 downto 0);
		wb_csr_stb_i   : in  std_logic;
		wb_csr_we_i    : in  std_logic;
		wb_csr_ack_o   : out std_logic;
		wb_csr_stall_o : out std_logic;

		DCM_lock_i   : in std_logic; 

		ADC1_D       : in std_logic; 
		ADC1_DCO     : in std_logic; 
		ADC2_D       : in std_logic; 
		ADC2_DCO     : in std_logic;

		ADC_CNV      : out std_logic; 
		ADC1_CLK     : out std_logic; 
		ADC2_CLK     : out std_logic; 
		C0_LED       : out std_logic; 
		DAC_DATA     : out std_logic_vector (15 downto 0); 
		DAC1_CLK     : out std_logic; 
		DAC2_CLK     : out std_logic; 
		PPD1         : out std_logic; 
		PPD2         : out std_logic; 
		
		delay1_o : out std_logic_vector(5 downto 0);
		delay2_o : out std_logic_vector(5 downto 0);

		C0_time_1_o : out std_logic_vector(31 downto 0);
		C0_time_2_o : out std_logic_vector(31 downto 0);

		reg_dio_dir_o   : out std_logic_vector(9 downto 0);
		reg_dio_cfg_o   : out std_logic_vector(9 downto 0);
		reg_dio_input_i : in std_logic_vector(9 downto 0);

		-- DDR wishbone interface
		wb_ddr_clk_i   : in  std_logic;
		wb_ddr_adr_o   : out std_logic_vector(31 downto 0);
		wb_ddr_dat_o   : out std_logic_vector(63 downto 0);
		wb_ddr_sel_o   : out std_logic_vector(7 downto 0);
		wb_ddr_stb_o   : out std_logic;
		wb_ddr_we_o    : out std_logic;
		wb_ddr_cyc_o   : out std_logic;
		wb_ddr_ack_i   : in  std_logic;
		wb_ddr_stall_i : in  std_logic;
				
		--To test
		ram_addr_o : out std_logic_vector(13 downto 0);
		ram_data_o : out std_logic_vector(31 downto 0);
		ram_wr_o   : out std_logic;

		mem_range_o		: out std_logic;
		acqu_end_o     : out std_logic_vector(1 downto 0);

		--Trig LED
		TRIG1_MISS   : out   std_logic; 
		TRIG1_OK     : out   std_logic; 
		TRIG2_MISS   : out   std_logic; 
		TRIG2_OK     : out   std_logic; 
		WIN1         : out   std_logic; 
		WIN2         : out   std_logic
	);
end peak_detect;

architecture BEHAVIORAL of peak_detect is

  ------------------------------------------------------------------------------
  -- SDB crossbar constants declaration
  --
  -- WARNING: All address in sdb and crossbar are BYTE addresses!
  ------------------------------------------------------------------------------

  -- Number of master port(s) on the wishbone crossbar
  constant c_NUM_WB_MASTERS : integer := 3;

  -- Number of slave port(s) on the wishbone crossbar
  constant c_NUM_WB_SLAVES : integer := 1;

  -- Wishbone master(s)
  constant c_WB_MASTER : integer := 0;

  -- Wishbone slave(s)
  constant c_SLAVE_FMC_PEAK_DETECTOR : integer := 0;  -- FMC Peak Detector mezzanine
  constant c_SLAVE_FMC_I2C           : integer := 1;  -- FMC I2C mezzanine
  constant c_SLAVE_FMC_ONEWIRE       : integer := 2;  -- FMC onewire interface

  --FMC
  constant c_FMC_PEAK_DETECTOR_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",              -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"0000000000000fff",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000605",
        version   => x"00000001",
        date      => x"20131107",
        name      => "WB-peak-detect-core")));

  constant c_FMC_ONEWIRE_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",              -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"0000000000000007",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000606",
        version   => x"00000001",
        date      => x"20131107",
        name      => "WB-Onewire.Control ")));

  constant c_FMC_I2C_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",              -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"000000000000001F",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000607",
        version   => x"00000001",
        date      => x"20131107",
        name      => "WB-I2C.Control     ")));
		  
  -- sdb header address
  constant c_SDB_ADDRESS : t_wishbone_address := x"00000000";

  -- Wishbone crossbar layout
  constant c_INTERCONNECT_LAYOUT : t_sdb_record_array(2 downto 0) :=
    (
      0 => f_sdb_embed_device(c_FMC_PEAK_DETECTOR_SDB_DEVICE, x"00001000"),
      1 => f_sdb_embed_device(c_FMC_I2C_SDB_DEVICE, x"00002000"),
      2 => f_sdb_embed_device(c_FMC_ONEWIRE_SDB_DEVICE, x"00002100")
      );
   
	constant c_DDR_COUNTER_SIM : Integer := 12; -- 0.096us when clk_sys_i = 125MHz
	constant c_DATA_LENGTH : Integer := 64; -- nb bits of data to save to DDR

	------------------------------------------------------------------------------
	-- Signals declaration
	------------------------------------------------------------------------------

	--Signal Declaration
	signal reset_n_s : std_logic;
	
	signal n_C0_PULSE : std_logic;
	
   signal ADC_CLK_s			: std_logic;
   signal DIFF_data_1_s    : std_logic_vector (15 downto 0);
   signal ADC_1_data_rdy_s	: std_logic;
   signal DIFF_data_2_s    : std_logic_vector (15 downto 0);
   signal ADC_2_data_rdy_s	: std_logic;
   signal DAC_1_DATA_BUS	: std_logic_vector (15 downto 0);
   signal DAC_2_DATA_BUS	: std_logic_vector (15 downto 0);
   signal DAC_1_LOAD			: std_logic;
   signal DAC_2_LOAD			: std_logic;
   signal Pulse1				: std_logic;
   signal Pulse2				: std_logic;
   signal PulseOutput1		: std_logic;
   signal PulseOutput2		: std_logic;
	
	signal TRIG1_MISS_s     : std_logic;
	signal TRIG2_MISS_s     : std_logic;
	
	--DDR
	signal data_rdy_decim_s : std_logic;
	
	signal ddr_sample_period_s : std_logic_vector(25 downto 0);
	signal ddr_tick_s          : std_logic;
	signal ddr_tick_long_s     : std_logic;

	signal counter_sim_ddr_s   : integer;
	signal data_sim_ddr_s      : unsigned(63 downto 0);
   signal data_before_decim_s : std_logic_vector(c_DATA_LENGTH-1 downto 0);
	signal data_after_decim_s  : std_logic_vector(c_DATA_LENGTH-1 downto 0);
   signal data_ddr_rdy_s      : std_logic;

	--PeakDetector Register
	signal StNoTrig1_s           : std_logic;
	signal StFNmr1_s             : std_logic;
	signal reg_st_reset_sw_s     : std_logic;
	signal reg_st_samples_wr_en_s: std_logic;
	signal StNotrig2_s           : std_logic;
	signal StFNmr2_s             : std_logic;
	
	signal ADC_data_1_s        : std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
	signal ADC_data_2_s        : std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
	signal DAC_prog_1_s        : std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
	signal DAC_prog_2_s        : std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
	signal DAC1_sel_s          : std_logic_vector(NB_BIT_DAC_SEL-1 downto 0);
	signal DAC2_sel_s          : std_logic_vector(NB_BIT_DAC_SEL-1 downto 0);
	signal Threshold1_s        : std_logic_vector(NB_BIT_THRESHOLD-1 downto 0); 
	signal Threshold2_s        : std_logic_vector(NB_BIT_THRESHOLD-1 downto 0); 

	signal TrigWidth_1_s       : std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal TrigWidth_2_s       : std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal WinDelay_1_s        : std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal WinDelay_2_s        : std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal WinWidth_1_s        : std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal WinWidth_2_s        : std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal EnWin_1_s           : std_logic;
	signal EnWin_2_s           : std_logic;
	signal EnTrigSim_1_s       : std_logic;
	signal EnTrigSim_2_s       : std_logic;
	signal TrigForcedDelay_1_s : std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal TrigForcedDelay_2_s : std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal TrigSimDelay_1_s    : std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal TrigSimDelay_2_s    : std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal C0_time_1_s         : std_logic_vector(31 downto 0);
	signal C0_time_2_s         : std_logic_vector(31 downto 0);
	signal reg_decim_factor_s  : std_logic_vector(31 downto 0);

	signal filter_on_s : std_logic;
	signal delay_s : std_logic_vector(3 downto 0);
	
	signal reg_cfg_diff_cfg_1_s : std_logic;
	signal reg_cfg_diff_cfg_2_s : std_logic;

	signal reg_ddr_range_s : std_logic_vector(24 downto 0);
	signal reg_last_addr_s : std_logic_vector(24 downto 0);

--	signal RegDio_s            : std_logic_vector(NB_SYS_IO-1 downto 0);
	signal notRegDio_s         : std_logic_vector(NB_SYS_IO-1 downto 0);
	signal RegCtype_s          : std_logic_vector(NB_BIT_CTYPE_BUS-1 downto 0) := x"deadface";
	
	signal samples_wr_en_s : std_logic;
	signal ddr_test_en_s   : std_logic;

	signal window_1_s : std_logic;
	signal window_2_s : std_logic;

	-- Wishbone buse(s) from crossbar master port(s)
	signal cnx_master_out : t_wishbone_master_out_array(c_NUM_WB_MASTERS-1 downto 0);
	signal cnx_master_in  : t_wishbone_master_in_array(c_NUM_WB_MASTERS-1 downto 0);

	-- Wishbone buse(s) to crossbar slave port(s)
	signal cnx_slave_out : t_wishbone_slave_out_array(c_NUM_WB_SLAVES-1 downto 0);
	signal cnx_slave_in  : t_wishbone_slave_in_array(c_NUM_WB_SLAVES-1 downto 0);

	attribute keep : string;
	attribute keep of C0_PULSE: signal is "TRUE";

	attribute keep of Pulse1: signal is "TRUE";
	attribute keep of PulseOutput1: signal is "TRUE";
	attribute keep of TRIG1_OK: signal is "TRUE";
	attribute keep of TRIG1_MISS: signal is "TRUE";
	attribute keep of WIN1: signal is "TRUE";

	attribute keep of Pulse2: signal is "TRUE";
	attribute keep of PulseOutput2: signal is "TRUE";
	attribute keep of TRIG2_OK: signal is "TRUE";
	attribute keep of TRIG2_MISS: signal is "TRUE";
	attribute keep of WIN2: signal is "TRUE";

begin

	n_C0_PULSE <= not C0_PULSE;--Active low pulse
  ------------------------------------------------------------------------------
  -- CSR wishbone crossbar
  ------------------------------------------------------------------------------
  cmp_sdb_crossbar : xwb_sdb_crossbar
    generic map (
      g_num_masters => c_NUM_WB_SLAVES,
      g_num_slaves  => c_NUM_WB_MASTERS,
      g_registered  => true,
      g_wraparound  => true,
      g_layout      => c_INTERCONNECT_LAYOUT,
      g_sdb_addr    => c_SDB_ADDRESS)
    port map (
      clk_sys_i => clk_sys_i,
      rst_n_i   => reset_n_s,
      slave_i   => cnx_slave_in,
      slave_o   => cnx_slave_out,
      master_i  => cnx_master_in,
      master_o  => cnx_master_out);

	--Reset
	reset_n_s <= not(reset_i);
	
	-- Connect crossbar slave port to entity port
	cnx_slave_in(c_WB_MASTER).adr <= wb_csr_adr_i;
	cnx_slave_in(c_WB_MASTER).dat <= wb_csr_dat_i;
	cnx_slave_in(c_WB_MASTER).sel <= wb_csr_sel_i;
	cnx_slave_in(c_WB_MASTER).stb <= wb_csr_stb_i;
	cnx_slave_in(c_WB_MASTER).we  <= wb_csr_we_i;
	cnx_slave_in(c_WB_MASTER).cyc <= wb_csr_cyc_i;

	wb_csr_dat_o   <= cnx_slave_out(c_WB_MASTER).dat;
	wb_csr_ack_o   <= cnx_slave_out(c_WB_MASTER).ack;
	wb_csr_stall_o <= cnx_slave_out(c_WB_MASTER).stall;

	------------------------------------------------------------------------------
	-- Peak Detector Register 
	--    Register to configure peak detecting :
	--    	Status Register, ADC data, DAC register,
	--    	Threshold, Configuration, Delays and widths   
	--    	Direction of digital I/O  
	------------------------------------------------------------------------------
	cmp_peak_detect_reg : peak_detect_reg 
	port map(
		rst_n_i              => reset_n_s,
		clk_sys_i            => clk_sys_i,
		
		wb_adr_i             => cnx_master_out(c_SLAVE_FMC_PEAK_DETECTOR).adr(6 downto 2),
		wb_dat_i             => cnx_master_out(c_SLAVE_FMC_PEAK_DETECTOR).dat,
		wb_dat_o             => cnx_master_in(c_SLAVE_FMC_PEAK_DETECTOR).dat,
		wb_cyc_i             => cnx_master_out(c_SLAVE_FMC_PEAK_DETECTOR).cyc,
		wb_sel_i             => cnx_master_out(c_SLAVE_FMC_PEAK_DETECTOR).sel,
		wb_stb_i             => cnx_master_out(c_SLAVE_FMC_PEAK_DETECTOR).stb,
		wb_we_i              => cnx_master_out(c_SLAVE_FMC_PEAK_DETECTOR).we,
		wb_ack_o             => cnx_master_in(c_SLAVE_FMC_PEAK_DETECTOR).ack,
		wb_stall_o           => open,
		-- Port for BIT field: 'No Trigger 1' in reg: 'Status Register'
		reg_st_notrig1_i     => StNoTrig1_s,
		-- Port for BIT field: 'FMR or NMR 1 ' in reg: 'Status Register'
		reg_st_fnmr1_o       => StFNmr1_s,
		-- Port for BIT field: 'Simulation or effective bit' in reg: 'Status Register'
		reg_st_simeff_i      => Eff_Sim_bit_i,
		-- Port for BIT field: 'Enable test DDR' in reg: 'Status Register'
		reg_st_ddr_tst_o     => ddr_test_en_s,
		-- Port for BIT field: 'Enable HW reset' in reg: 'Status Register'
		reg_st_en_hw_reset_o => en_hw_reset_o,
		-- Port for BIT field: 'Reset HW input' in reg: 'Status Register'
		reg_st_reset_hw_i    => reset_hw_i,
		-- Port for BIT field: 'Reset SW input' in reg: 'Status Register'
		reg_st_reset_sw_o     => reg_st_reset_sw_s,
		-- Port for BIT field: 'DDR Samples enable' in reg: 'Status Register'
		reg_st_samples_wr_en_o=> reg_st_samples_wr_en_s,
		-- Port for BIT field: 'Initialization done bit' in reg: 'Status Register'
		reg_st_init_done_i   => init_done_i,
		-- Port for BIT field: 'No Trigger 2' in reg: 'Status Register'
		reg_st_notrig2_i     => StNotrig2_s,
		-- Port for BIT field: 'FMR or NMR 2 ' in reg: 'Status Register'
		reg_st_fnmr2_o       => StFNmr2_s,
		-- Port for std_logic_vector field: 'DIO' in reg: 'Direction of digital I/O'
		reg_dio_dir_o        => reg_dio_dir_o,
		-- Port for std_logic_vector field: 'Configuration' in reg: 'Digital I/O'
		reg_dio_cfg_o        => reg_dio_cfg_o,
		-- Port for std_logic_vector field: 'Input' in reg: 'Digital I/O'
		reg_dio_input_i      => reg_dio_input_i,
		-- Port for std_logic_vector field: 'ADC1' in reg: 'ADC data'
		reg_adc_data_1_i      => ADC_data_1_s,
		-- Port for std_logic_vector field: 'ADC2' in reg: 'ADC data'
		reg_adc_data_2_i      => ADC_data_2_s,
		-- Port for std_logic_vector field: 'DATA1' in reg: 'DAC register'
		reg_dac_data_1_i      => DAC_1_DATA_BUS,
		-- Port for std_logic_vector field: 'DATA2' in reg: 'DAC register'
		reg_dac_data_2_i      => DAC_2_DATA_BUS,
		-- Port for std_logic_vector field: 'DATA1_bus_prog' in reg: 'DAC bus program register'
		reg_prg_bus_data_1_o	=> DAC_prog_1_s,
		-- Port for std_logic_vector field: 'DATA2_bus_prog' in reg: 'DAC bus program register'
		reg_prg_bus_data_2_o	=> DAC_prog_2_s,
		-- Port for std_logic_vector field: 'THR1' in reg: 'Threshold'
		reg_thr_chan1_o      => Threshold1_s,
		-- Port for std_logic_vector field: 'THR2' in reg: 'Threshold'
		reg_thr_chan2_o      => Threshold2_s,
		-- Port for BIT field: 'ENWIN1' in reg: 'Configuration'
		reg_cfg_enwin1_o     => EnWin_1_s,
		-- Port for BIT field: 'ENTRIGWIN1' in reg: 'Configuration'
		reg_cfg_entrigwin1_o => EnTrigSim_1_s,
		-- Port for BIT field: 'Filter On/Off' in reg: 'Configuration'
		reg_cfg_filter_on_o  => filter_on_s,
		-- Port for std_logic_vector field: 'DACSEL1' in reg: 'Configuration'
		reg_cfg_dacsel1_o    => DAC1_sel_s,
		-- Port for std_logic_vector field: 'Delay Clk' in reg: 'Configuration'
		reg_cfg_delay_clk_o  => delay_s,
		-- Port for BIT field: 'Differential or signal' in reg: 'Configuration'
		reg_cfg_diff_cfg_1_o => reg_cfg_diff_cfg_1_s,
		-- Port for BIT field: 'ENWIN2' in reg: 'Configuration'
		reg_cfg_enwin2_o     => EnWin_2_s,
		-- Port for BIT field: 'ENTRIGWIN2' in reg: 'Configuration'
		reg_cfg_entrigwin2_o => EnTrigSim_2_s,
		-- Port for std_logic_vector field: 'DACSEL2' in reg: 'Configuration'
		reg_cfg_dacsel2_o    => DAC2_sel_s,
		-- Port for BIT field: 'Differential or signal' in reg: 'Configuration'
		reg_cfg_diff_cfg_2_o => reg_cfg_diff_cfg_2_s,
		-- Port for std_logic_vector field: 'WINDELAY1' in reg: 'Windows delay 1'
		reg_windelay1_o      => WinDelay_1_s,
		-- Port for std_logic_vector field: 'WINDELAY2' in reg: 'Windows delay 2'
		reg_windelay2_o      => WinDelay_2_s,
		-- Port for std_logic_vector field: 'WINWIDTH1' in reg: 'Windows width 1'
		reg_winwidth1_o      => WinWidth_1_s,
		-- Port for std_logic_vector field: 'WINWIDTH2' in reg: 'Windows width 2'
		reg_winwidth2_o      => WinWidth_2_s,
		-- Port for std_logic_vector field: 'TRIGWIDTH1' in reg: 'Trigger width 1'
		reg_trigwidth1_o     => TrigWidth_1_s,
		-- Port for std_logic_vector field: 'TRIGWIDTH2' in reg: 'Trigger width 2'
		reg_trigwidth2_o     => TrigWidth_2_s,
		-- Port for std_logic_vector field: 'TRIGDELFORCED1' in reg: 'Trigger delay forced 1'
		reg_trigdelforced1_o	=> TrigForcedDelay_1_s,
		-- Port for std_logic_vector field: 'TRIGDELFORCED2' in reg: 'Trigger delay forced 2'
		reg_trigdelforced2_o	=> TrigForcedDelay_2_s,
		-- Port for std_logic_vector field: 'TRIGDELSIM1' in reg: 'Trigger delay simulated 1'
		reg_trigdelsim1_o    => TrigSimDelay_1_s,
		-- Port for std_logic_vector field: 'TRIGDELSIM2' in reg: 'Trigger delay simulated 2'
		reg_trigdelsim2_o    => TrigSimDelay_2_s,
		-- Port for std_logic_vector field: 'C0+time channel F' in reg: 'C0+time channel F'
		reg_c0_time_f_i      => C0_time_1_s,
		-- Port for std_logic_vector field: 'C0+time channel D' in reg: 'C0+time channel D'
		reg_c0_time_d_i      => C0_time_2_s,
		-- Port for std_logic_vector field: 'CTYPE' in reg: 'Cycle type'
		reg_ctype_i          => RegCtype_s,
		-- Port for std_logic_vector field: 'decim_factor' in reg: 'Decimator'
		reg_decim_factor_o   => reg_decim_factor_s,
		-- Port for std_logic_vector field: 'DDR range' in reg: 'Memory Range'
		reg_ddr_range_o      => reg_ddr_range_s,
		-- Port for std_logic_vector field: 'Last sample' in reg: 'Last sample'
		reg_last_addr_i      => reg_last_addr_s
	);

	reset_sw_o    <= reg_st_reset_sw_s;

	cmp_ADC_DIFF_DAC : ADC_DIFF_DACsm
		port map(
			clk_20MHz_i		=> clk_20MHz_i,
			clk_50MHz_i		=> clk_50MHz_i,
			clk_500MHz_i	=> clk_500MHz_i,
			clk_250MHz_i	=> clk_250MHz_i,
			
			DCM_lock_i		=> DCM_lock_i,

			reset_i			=> reset_i,
			
			delay_clk_i    => delay_s,

			filter_on_i    => filter_on_s,
			diff_cfg_1_i   => reg_cfg_diff_cfg_1_s,
			diff_cfg_2_i   => reg_cfg_diff_cfg_2_s,

			ADC_DCO_1_i		=> ADC1_DCO,
			ADC_D_1_i		=> ADC1_D,
			
			ADC_data_1_o		=> ADC_data_1_s,
			DIFF_data_1_o		=> DIFF_data_1_s,
			ADC_data_rdy_1_o	=> ADC_1_data_rdy_s,
			
			ADC_DCO_2_i		=> ADC2_DCO,
			ADC_D_2_i		=> ADC2_D,
			
			ADC_data_2_o		=> ADC_data_2_s,
			DIFF_data_2_o		=> DIFF_data_2_s,
			ADC_data_rdy_2_o	=> ADC_2_data_rdy_s,
					
			ADC_CNV_o		=> ADC_CNV,
			ADC_CLK_o		=> ADC_CLK_s,
			
			threshold_1_i	=> Threshold1_s,
			threshold_2_i	=> Threshold2_s,
			
			DAC_sel_1_i		=> DAC1_sel_s,
			DAC_sel_2_i		=> DAC2_sel_s,
			
			DAC_prog_data_1_i	=> DAC_prog_1_s,
			DAC_data_bus_1_o	=> DAC_1_DATA_BUS,
			DAC_load_1_o		=> DAC_1_LOAD,
			
			DAC_prog_data_2_i	=> DAC_prog_2_s,
			DAC_data_bus_2_o	=> DAC_2_DATA_BUS,
			DAC_load_2_o		=> DAC_2_LOAD,
			
			peak_pulse_1_o	=> Pulse1,
			peak_pulse_2_o	=> Pulse2
		);
			
	ADC1_CLK <= ADC_CLK_s;
	ADC2_CLK <= ADC_CLK_s;

   PPD1 <= PulseOutput1;
   PPD2 <= PulseOutput2;

	--C0 and Windows generation
	C0_LED <= n_C0_PULSE;--Active low pulse

	cmp_ppd_win_1 : PPD_Win
		 port map( 
			clk_50MHz_i => clk_50MHz_i,
			reset_i 		=> reset_i,
			
			pulse_i 		=> Pulse1,
			pulse_o 		=> PulseOutput1,
			C0_pulse_i	=> n_C0_PULSE,
			--Register
			eff_sim_bit_i		=> Eff_Sim_bit_i,
			pulse_width_i		=> TrigWidth_1_s,
			win_delay_i			=> WinDelay_1_s,
			win_width_i			=> WinWidth_1_s,
			en_win_i				=> EnWin_1_s,
			en_trig_sim_i		=> EnTrigSim_1_s,
			trig_sim_delay_i	=> TrigSimDelay_1_s,
			trig_forc_delay_i	=> TrigForcedDelay_1_s,
						
			no_trig_o	=> StNotrig1_s,
			pulse_ok_o	=> TRIG1_OK,
			pulse_fail_o=> TRIG1_MISS,
			window_o		=> window_1_s			
		);
	WIN1 <= window_1_s;
	
	-- Time after C0 of trig 1 (unit : 1/clk_20MHz_i=>50ns)
	cmp_c0_time_1 : c0_time
		port map(
			clk_i   => clk_20MHz_i,
			reset_i => reset_i,
			
			c0_pulse_i   => n_C0_PULSE,
			trig_pulse_i => PulseOutput1,
			
			c0_time_o => C0_time_1_s
		);
	C0_time_1_o <= C0_time_1_s;
	
	cmp_ppd_win_2 : PPD_Win
		 port map( 
			clk_50MHz_i => clk_50MHz_i,
			reset_i 		=> reset_i,
			
			pulse_i 		=> Pulse2,
			pulse_o 		=> PulseOutput2,
			C0_pulse_i	=> n_C0_PULSE,
			--Register
			eff_sim_bit_i		=> Eff_Sim_bit_i,
			pulse_width_i		=> TrigWidth_2_s,
			win_delay_i			=> WinDelay_2_s,
			win_width_i			=> WinWidth_2_s,
			en_win_i				=> EnWin_2_s,
			en_trig_sim_i		=> EnTrigSim_2_s,
			trig_sim_delay_i	=> TrigSimDelay_2_s,
			trig_forc_delay_i	=> TrigForcedDelay_2_s,
			
			no_trig_o	=> StNotrig2_s,
			pulse_ok_o	=> TRIG2_OK,
			pulse_fail_o=> TRIG2_MISS,
			window_o		=> window_2_s
		);
	WIN2 <= window_2_s;

	-- Time after C0 of trig 2 (unit : 1/clk_20MHz_i=>50ns)
	cmp_c0_time_2 : c0_time
		port map(
			clk_i   => clk_20MHz_i,
			reset_i => reset_i,
			
			c0_pulse_i   => n_C0_PULSE,
			trig_pulse_i => PulseOutput2,
			
			c0_time_o => C0_time_2_s
		);
	C0_time_2_o <= C0_time_2_s;

   cmp_dacmux : DAC_mux_output
      port map (
			clk_50MHz_i		=>clk_50MHz_i,
			reset_i			=>reset_i,
			
			DAC1_data_bus_i=>DAC_1_DATA_BUS,
			DAC1_load_i		=>DAC_1_LOAD,
			DAC2_data_bus_i=>DAC_2_DATA_BUS,
			DAC2_load_i		=>DAC_2_LOAD,
			DAC_data_bus_o	=>DAC_DATA,
			load1_o			=>DAC1_CLK,
			load2_o			=>DAC2_CLK
		);

	-- Process to construct sample to DDR and simulate acquisition in Modelsim
	process (clk_sys_i,reset_i)
	begin
		if rising_edge(clk_sys_i) then
			if (reset_i='1') then
				data_before_decim_s <= (others=>'0');
				data_ddr_rdy_s	     <= '0';
				counter_sim_ddr_s   <= 0;
				data_sim_ddr_s      <= (others=>'0');
			else
				if (g_simulation = 0) then
					data_before_decim_s	<= "000000" & window_2_s & PulseOutput2 & "000000" & window_1_s & PulseOutput1 &
							"0000000" & Eff_Sim_bit_i & "0000000" & C0_PULSE &
							ADC_data_2_s & ADC_data_1_s;			
					data_ddr_rdy_s	<= ADC_1_data_rdy_s;
					counter_sim_ddr_s <= 0;
				else
					data_before_decim_s <= std_logic_vector(data_sim_ddr_s);
					if (counter_sim_ddr_s < (c_DDR_COUNTER_SIM-1)) then
						counter_sim_ddr_s <= counter_sim_ddr_s+1;
						data_ddr_rdy_s	<= '0';
					else
						counter_sim_ddr_s <= 0;
						data_ddr_rdy_s	<= '1';
						data_sim_ddr_s <= data_sim_ddr_s + 1;
					end if;
				end if;
			end if;
		end if;
	end process;

	--Write to DDR		
	cmp_decimator : decimator 
		generic map(
			g_DATA_LENGTH  => c_DATA_LENGTH
		)
		port map(
			clk_i		=> clk_sys_i,
			reset_i	=> reset_i,
			
			decim_factor_i => reg_decim_factor_s,

			data_i	=> data_before_decim_s,
			d_rdy_i	=> data_ddr_rdy_s,

			data_o	=> data_after_decim_s,
			d_rdy_o	=> data_rdy_decim_s
		);
	
	cmp_acqu_ddr_ctrl : acqu_ddr_ctrl 
		generic map(
			g_sim => g_simulation,
			g_WORD_LENGTH => 64,
			g_DATA_LENGTH => c_DATA_LENGTH
		)
		port map(
			clk_i   => clk_sys_i,
			reset_i => reset_i,
			
			--Control
			samples_wr_en_i   => samples_wr_en_s,
			ddr_test_en_i     => ddr_test_en_s,
			ddr_range_i       => reg_ddr_range_s,
			last_addr_o       => reg_last_addr_s,
			
			--Data
			sync_data_i       => data_after_decim_s,
			sync_data_valid_i => data_rdy_decim_s,
			C0_i					=> C0_PULSE,

			-- DDR wishbone interface
			wb_ddr_clk_i   => wb_ddr_clk_i,
			wb_ddr_adr_o   => wb_ddr_adr_o,
			wb_ddr_dat_o   => wb_ddr_dat_o,
			wb_ddr_sel_o   => wb_ddr_sel_o,
			wb_ddr_stb_o   => wb_ddr_stb_o,
			wb_ddr_we_o    => wb_ddr_we_o,
			wb_ddr_cyc_o   => wb_ddr_cyc_o,
			wb_ddr_ack_i   => wb_ddr_ack_i,
			wb_ddr_stall_i => wb_ddr_stall_i,

			--To test
			ram_addr_o => ram_addr_o,
			ram_data_o => ram_data_o,
			ram_wr_o   => ram_wr_o,

			mem_range_o => mem_range_o,
			acqu_end_o  => acqu_end_o
		);
	samples_wr_en_s <= reg_st_samples_wr_en_s and init_done_i when (g_simulation = 0) else init_done_i;
   					 
end BEHAVIORAL;


