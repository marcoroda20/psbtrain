--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

package peak_detector_pkg is

-- Constant
	constant NB_BIT_ADC_DATA_BUS	: integer := 16;
	constant NB_BIT_DIFF_DATA_BUS	: integer := NB_BIT_ADC_DATA_BUS;
	constant NB_BIT_THRESHOLD		: integer := NB_BIT_ADC_DATA_BUS;
	constant NB_BIT_COUNTER			: integer := 32;
	
	constant NB_BIT_DAC_SEL			: integer := 3;
	constant NB_BIT_DAC_DATA_BUS	: integer := 16;

	constant NB_BIT_UTC_BUS			: integer := 32;
	constant NB_BIT_CTYPE_BUS		: integer := 32;

	constant NB_SYS_IO	: integer := 10;		
	
	-- Logic level for effective/simul bit
	constant STATUS_EFFECTIVE : std_logic := '1';
	constant STATUS_SIMUL     : std_logic := '0';

	--Component
	component peak_detect 
		generic(g_simulation : integer := 0);
		port ( 
			clk_sys_i    : in std_logic;
			clk_20MHz_i  : in std_logic; 
			clk_50MHz_i  : in std_logic; 
			clk_250MHz_i : in std_logic; 
			clk_500MHz_i : in std_logic; 
			reset_i      : in std_logic; 
			init_done_i  : in std_logic;

			C0_PULSE     : in std_logic; 
			
			reset_hw_i    : in std_logic;
			reset_sw_o    : out std_logic;
			en_hw_reset_o : out std_logic;	
			
			Eff_Sim_bit_i : in std_logic;

			-- CSR wishbone interface
			wb_csr_adr_i   : in  std_logic_vector(31 downto 0);
			wb_csr_dat_i   : in  std_logic_vector(31 downto 0);
			wb_csr_dat_o   : out std_logic_vector(31 downto 0);
			wb_csr_cyc_i   : in  std_logic;
			wb_csr_sel_i   : in  std_logic_vector(3 downto 0);
			wb_csr_stb_i   : in  std_logic;
			wb_csr_we_i    : in  std_logic;
			wb_csr_ack_o   : out std_logic;
			wb_csr_stall_o : out std_logic;

			DCM_lock_i   : in std_logic; 

			ADC1_D       : in std_logic; 
			ADC1_DCO     : in std_logic; 
			ADC2_D       : in std_logic; 
			ADC2_DCO     : in std_logic;

			ADC_CNV      : out std_logic; 
			ADC1_CLK     : out std_logic; 
			ADC2_CLK     : out std_logic; 
			C0_LED       : out std_logic; 
			DAC_DATA     : out std_logic_vector (15 downto 0); 
			DAC1_CLK     : out std_logic; 
			DAC2_CLK     : out std_logic; 
			PPD1         : out std_logic; 
			PPD2         : out std_logic; 
			
			delay1_o : out std_logic_vector(5 downto 0);
			delay2_o : out std_logic_vector(5 downto 0);

			C0_time_1_o : out std_logic_vector(31 downto 0);
			C0_time_2_o : out std_logic_vector(31 downto 0);

			reg_dio_dir_o   : out std_logic_vector(9 downto 0);
			reg_dio_cfg_o   : out std_logic_vector(9 downto 0);
			reg_dio_input_i : in std_logic_vector(9 downto 0);

			-- DDR wishbone interface
			wb_ddr_clk_i   : in  std_logic;
			wb_ddr_adr_o   : out std_logic_vector(31 downto 0);
			wb_ddr_dat_o   : out std_logic_vector(63 downto 0);
			wb_ddr_sel_o   : out std_logic_vector(7 downto 0);
			wb_ddr_stb_o   : out std_logic;
			wb_ddr_we_o    : out std_logic;
			wb_ddr_cyc_o   : out std_logic;
			wb_ddr_ack_i   : in  std_logic;
			wb_ddr_stall_i : in  std_logic;
			
--			samples_wr_en_i: in  std_logic;
--			ddr_test_en_i  : in  std_logic;
			
			--To test
			ram_addr_o : out std_logic_vector(13 downto 0);
			ram_data_o : out std_logic_vector(31 downto 0);
			ram_wr_o   : out std_logic;

			mem_range_o		: out std_logic;
			acqu_end_o     : out std_logic_vector(1 downto 0);

			--Trig			
			TRIG1_MISS   : out   std_logic; 
			TRIG1_OK     : out   std_logic; 
			TRIG2_MISS   : out   std_logic; 
			TRIG2_OK     : out   std_logic; 
			WIN1         : out   std_logic; 
			WIN2         : out   std_logic
		);
	end component;

	component peak_detect_reg 
		port (
			rst_n_i                                  : in     std_logic;
			clk_sys_i                                : in     std_logic;
			wb_adr_i                                 : in     std_logic_vector(4 downto 0);
			wb_dat_i                                 : in     std_logic_vector(31 downto 0);
			wb_dat_o                                 : out    std_logic_vector(31 downto 0);
			wb_cyc_i                                 : in     std_logic;
			wb_sel_i                                 : in     std_logic_vector(3 downto 0);
			wb_stb_i                                 : in     std_logic;
			wb_we_i                                  : in     std_logic;
			wb_ack_o                                 : out    std_logic;
			wb_stall_o                               : out    std_logic;
			-- Port for BIT field: 'No Trigger high' in reg: 'Status Register'
			reg_st_notrig1_i                         : in     std_logic;
			-- Port for BIT field: 'FMR or NMR high' in reg: 'Status Register'
			reg_st_fnmr1_o                           : out    std_logic;
			-- Port for BIT field: 'Simulation or effective bit' in reg: 'Status Register'
			reg_st_simeff_i                          : in     std_logic;
			-- Port for BIT field: 'Enable test DDR' in reg: 'Status Register'
			reg_st_ddr_tst_o                         : out    std_logic;
			-- Port for BIT field: 'Enable HW reset' in reg: 'Status Register'
			reg_st_en_hw_reset_o                     : out    std_logic;
			-- Port for BIT field: 'Reset HW input' in reg: 'Status Register'
			reg_st_reset_hw_i                        : in     std_logic;
			-- Port for BIT field: 'Reset SW input' in reg: 'Status Register'
			reg_st_reset_sw_o                        : out    std_logic;
			-- Port for BIT field: 'DDR Samples enable' in reg: 'Status Register'
			reg_st_samples_wr_en_o                   : out    std_logic;
			-- Port for BIT field: 'Initialization done bit' in reg: 'Status Register'
			reg_st_init_done_i                       : in     std_logic;
			-- Port for BIT field: 'No Trigger low' in reg: 'Status Register'
			reg_st_notrig2_i                         : in     std_logic;
			-- Port for BIT field: 'FMR or NMR low' in reg: 'Status Register'
			reg_st_fnmr2_o                           : out    std_logic;
			-- Port for std_logic_vector field: 'DIO' in reg: 'Direction of digital I/O'
			reg_dio_dir_o                            : out    std_logic_vector(9 downto 0);
			-- Port for std_logic_vector field: 'Configuration' in reg: 'Digital I/O'
			reg_dio_cfg_o                            : out    std_logic_vector(9 downto 0);
			-- Port for std_logic_vector field: 'Input' in reg: 'Digital I/O'
			reg_dio_input_i                          : in     std_logic_vector(9 downto 0);
			-- Port for std_logic_vector field: 'ADC1' in reg: 'ADC data'
			reg_adc_data_1_i                         : in     std_logic_vector(15 downto 0);
			-- Port for std_logic_vector field: 'ADC2' in reg: 'ADC data'
			reg_adc_data_2_i                         : in     std_logic_vector(15 downto 0);
			-- Port for std_logic_vector field: 'DATA1' in reg: 'DAC register'
			reg_dac_data_1_i                         : in     std_logic_vector(15 downto 0);
			-- Port for std_logic_vector field: 'DATA2' in reg: 'DAC register'
			reg_dac_data_2_i                         : in     std_logic_vector(15 downto 0);
			-- Port for std_logic_vector field: 'DATA1_bus_prog' in reg: 'DAC bus program register'
			reg_prg_bus_data_1_o                     : out    std_logic_vector(15 downto 0);
			-- Port for std_logic_vector field: 'DATA2_bus_prog' in reg: 'DAC bus program register'
			reg_prg_bus_data_2_o                     : out    std_logic_vector(15 downto 0);
			-- Port for std_logic_vector field: 'THR1' in reg: 'Threshold'
			reg_thr_chan1_o                          : out    std_logic_vector(15 downto 0);
			-- Port for std_logic_vector field: 'THR2' in reg: 'Threshold'
			reg_thr_chan2_o                          : out    std_logic_vector(15 downto 0);
			-- Port for BIT field: 'ENWIN1' in reg: 'Configuration'
			reg_cfg_enwin1_o                         : out    std_logic;
			-- Port for BIT field: 'ENTRIGWIN1' in reg: 'Configuration'
			reg_cfg_entrigwin1_o                     : out    std_logic;
			-- Port for BIT field: 'Filter On/Off' in reg: 'Configuration'
			reg_cfg_filter_on_o                      : out    std_logic;
			-- Port for std_logic_vector field: 'DACSEL1' in reg: 'Configuration'
			reg_cfg_dacsel1_o                        : out    std_logic_vector(2 downto 0);
			-- Port for std_logic_vector field: 'Delay Clk' in reg: 'Configuration'
			reg_cfg_delay_clk_o                      : out    std_logic_vector(3 downto 0);
			-- Port for BIT field: 'Differential or signal' in reg: 'Configuration'
			reg_cfg_diff_cfg_1_o                     : out    std_logic;
			-- Port for BIT field: 'ENWIN2' in reg: 'Configuration'
			reg_cfg_enwin2_o                         : out    std_logic;
			-- Port for BIT field: 'ENTRIGWIN2' in reg: 'Configuration'
			reg_cfg_entrigwin2_o                     : out    std_logic;
			-- Port for std_logic_vector field: 'DACSEL2' in reg: 'Configuration'
			reg_cfg_dacsel2_o                        : out    std_logic_vector(2 downto 0);
			-- Port for BIT field: 'Differential or signal' in reg: 'Configuration'
			reg_cfg_diff_cfg_2_o                     : out    std_logic;
			-- Port for std_logic_vector field: 'WINDELAY1' in reg: 'Windows delay high'
			reg_windelay1_o                          : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'WINDELAY2' in reg: 'Windows delay low'
			reg_windelay2_o                          : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'WINWIDTH1' in reg: 'Windows width 1'
			reg_winwidth1_o                          : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'WINWIDTH2' in reg: 'Windows width low'
			reg_winwidth2_o                          : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'TRIGWIDTH1' in reg: 'Trigger width high'
			reg_trigwidth1_o                         : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'TRIGWIDTH2' in reg: 'Trigger width low'
			reg_trigwidth2_o                         : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'TRIGDELFORCED1' in reg: 'Trigger delay forced high'
			reg_trigdelforced1_o                     : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'TRIGDELFORCED2' in reg: 'Trigger delay forced low'
			reg_trigdelforced2_o                     : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'TRIGDELSIM1' in reg: 'Trigger delay simulated high'
			reg_trigdelsim1_o                        : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'TRIGDELSIM2' in reg: 'Trigger delay simulated low'
			reg_trigdelsim2_o                        : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'C0+time channel F' in reg: 'C0+time channel F'
			reg_c0_time_f_i                          : in     std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'C0+time channel D' in reg: 'C0+time channel D'
			reg_c0_time_d_i                          : in     std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'CTYPE' in reg: 'Cycle type'
			reg_ctype_i                              : in     std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'decim_factor' in reg: 'Decimator'
			reg_decim_factor_o                       : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'DDR range' in reg: 'Memory Range'
			reg_ddr_range_o                          : out    std_logic_vector(24 downto 0);
			-- Port for std_logic_vector field: 'Last sample' in reg: 'Last sample'
			reg_last_addr_i                          : in     std_logic_vector(24 downto 0)
		);
	end component;

	component carrier_csr 
		port (
			rst_n_i                                  : in     std_logic;
			clk_sys_i                                : in     std_logic;
			wb_adr_i                                 : in     std_logic_vector(1 downto 0);
			wb_dat_i                                 : in     std_logic_vector(31 downto 0);
			wb_dat_o                                 : out    std_logic_vector(31 downto 0);
			wb_cyc_i                                 : in     std_logic;
			wb_sel_i                                 : in     std_logic_vector(3 downto 0);
			wb_stb_i                                 : in     std_logic;
			wb_we_i                                  : in     std_logic;
			wb_ack_o                                 : out    std_logic;
			wb_stall_o                               : out    std_logic;
			-- Port for std_logic_vector field: 'PCB revision' in reg: 'Carrier type and PCB version'
			carrier_csr_carrier_pcb_rev_i            : in     std_logic_vector(3 downto 0);
			-- Port for std_logic_vector field: 'Reserved register' in reg: 'Carrier type and PCB version'
			carrier_csr_carrier_reserved_o           : out    std_logic_vector(11 downto 0);
			-- Port for std_logic_vector field: 'Carrier type' in reg: 'Carrier type and PCB version'
			carrier_csr_carrier_type_i               : in     std_logic_vector(15 downto 0);
			-- Port for BIT field: 'FMC presence' in reg: 'Status'
			carrier_csr_stat_fmc_pres_i              : in     std_logic;
			-- Port for BIT field: 'GN4142 core P2L PLL status' in reg: 'Status'
			carrier_csr_stat_p2l_pll_lck_i           : in     std_logic;
			-- Port for BIT field: 'System clock PLL status' in reg: 'Status'
			carrier_csr_stat_sys_pll_lck_i           : in     std_logic;
			-- Port for BIT field: 'DDR3 calibration status' in reg: 'Status'
			carrier_csr_stat_ddr3_cal_done_i         : in     std_logic;
			-- Port for std_logic_vector field: 'Reserved' in reg: 'Status'
			carrier_csr_stat_reserved_o              : out    std_logic_vector(27 downto 0);
			-- Port for std_logic_vector field: 'LED' in reg: 'Control'
			carrier_csr_ctrl_led_o                   : out    std_logic_vector(3 downto 0);
			-- Port for std_logic_vector field: 'Reserved' in reg: 'Control'
			carrier_csr_ctrl_reserved_o              : out    std_logic_vector(27 downto 0);
			-- Port for BIT field: 'State of the reset line' in reg: 'Reset Register'
			carrier_csr_rst_fmc0_n_o                 : out    std_logic;
			-- Port for std_logic_vector field: 'Reserved' in reg: 'Reset Register'
			carrier_csr_rst_reserved_o               : out    std_logic_vector(30 downto 0)
		);
	end component;
	
end peak_detector_pkg;


