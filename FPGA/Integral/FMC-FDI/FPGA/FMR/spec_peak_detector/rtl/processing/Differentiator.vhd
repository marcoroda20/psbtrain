----------------------------------------------------------------------------------
-- Company: CERN TE/MSC-MM
-- Engineer: David Giloteaux
-- 
-- Create Date:    17:46:21 02/08/2011 
-- Design Name: 
-- Module Name:    Differentiator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 16-Bit Digital differentiator based on the central difference formula
--              (Yi+1 - Yi-1)/2h
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
-- Daniel Oberson => Add filter and pulse width
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_SIGNED.all;

use work.peak_detector_pkg.all;

entity Differentiator is
    Port ( 
		clk_50MHz_i	: in std_logic;
		reset_i		: in std_logic;
		diff_cfg_i  : in std_logic;
		filter_on_i : in std_logic;
		ADC_DATA		: in  STD_LOGIC_VECTOR (15 downto 0);
		THRESH      : in  STD_LOGIC_VECTOR (NB_BIT_THRESHOLD-1 downto 0);
		CLK_DATA    : in  STD_LOGIC;
		ADC_REG     : in  STD_LOGIC;
		LOAD_DAC, PEAK_PULSE : out STD_LOGIC;
		DAC_data_o  : out std_logic_vector(15 downto 0);
		DIFF_ADC    : out  STD_LOGIC_VECTOR (15 downto 0)
	);			  
end Differentiator;

architecture Behavioral of Differentiator is

component XLXI_Filter is
	Port(
		clk_i					: in std_logic; --250MHz
		clk_div2_i			: in std_logic; --125MHz
		reset_i				: in std_logic;
		DATA_ADC_i 			: in std_logic_vector(15 downto 0);
		DATA_rdy_i 			: in std_logic;
		
		DATA_filtered_o 	: out std_logic_vector(15 downto 0);
		DATA_rdy_o 			: out std_logic
	);
end component XLXI_Filter;

constant WIDTH_PULSE	: integer := 16;

signal ADC_2s_DATA, Yi_Reg, Yi_minus1_Reg, Yi_plus1_Reg, Sub_Yip1_Yim1, diff_or_signal_s : std_logic_vector(15 downto 0) := x"0000"; 
signal Yi_minus2_Reg, Yi_plus2_Reg, Gain_Diff_Reg : std_logic_vector(15 downto 0) := x"0000";
signal Sign, Sign1, Sign_Trans : std_logic :='0';
signal EN, EN1, EN2, EN3, J1, K1, QJK1, EN_ADC_REG : std_logic :='0';
signal M1, M2, M3, M4, M5, M6, Shift_M4 : std_logic :='0'; 
signal CLK_DATA_DIV2, AR1, AR2, AR3, RST_REG, ADC_DRDY : std_logic :='0';
signal ADC_Reg_Mask, Shift1_ADC_Reg, CE_DIFF : std_logic :='0';
signal PW_Cnt : std_logic_vector(15 downto 0) := x"0000";
signal Start_Cnt, TCPWCnt, TC_PW_Cnt : std_logic :='0';
signal Start_Pw, EndPw, End_Pw : std_logic :='0';
signal LDPW_Cnt : std_logic_vector(3 downto 0) := x"0";
signal Enable_Sign_Trans, ENAR1, ENAR2, ENAR_Fall : std_logic :='0';
signal OneTrans, One_Trans, Only_One_Trans : std_logic :='0';

signal Yi_minus3_Reg, Yi_plus3_Reg, Sub_Yip2_Yim2 : std_logic_vector(15 downto 0) := x"0000";
signal Subx4_Yip2_Yim2, Subx5_Yip1_Yim1, Num : std_logic_vector(19 downto 0) := x"00000";

signal Vi, Filt, k : std_logic_vector(23 downto 0) := x"000000";
signal Signal_Filtered : std_logic_vector(15 downto 0) := x"0000";
signal M, Filt_Out : std_logic_vector(47 downto 0) := x"000000000000";
signal CE_DIFF_F1, CE_DIFF_F2, CE_DIFF_FILT : std_logic :='0';

signal cnt_width_p_s : integer;

signal LOAD_DAC_s : std_logic;
signal ADC_DATA_1_s : std_logic_vector(15 downto 0);

begin

ADC_2s_DATA <= ( not ADC_DATA(15) ) & ADC_DATA(14 downto 0); --ADC Data in 2's complement binary code

process (CLK_DATA,CLK_DATA_DIV2) --CLK_CNV = 20MHZ or T=50ns
begin
   if CLK_DATA'event and CLK_DATA = '1' then  
        CLK_DATA_DIV2 <= not CLK_DATA_DIV2;
   end if;
end process;

process (CLK_DATA_DIV2,reset_i,ADC_REG,AR1,AR2) 
begin
   if (reset_i='1') then
	   AR1 <= '0';
      AR2 <= '0';
      AR3 <= '0';
   elsif CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '0' then
	   AR1 <= ADC_REG;
      AR2 <= AR1;
      AR3 <= AR2;
	end if;
end process;

ADC_DRDY <= AR2 and ( not AR3 );

--16-Bit threshold comparator
process(CLK_DATA_DIV2,reset_i,ADC_DRDY,ADC_2s_DATA,THRESH)
begin
   if (reset_i='1') then
		EN <= '0';
   elsif (CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '1') then
     if ADC_DRDY = '1' then   
			if ( ADC_2s_DATA < THRESH ) then 
				EN <= '1';
			else 
				EN <= '0';
			end if;
		end if;
   end if; 
end process;

--The EN signal must be at a same state for at least 3 samples to validate the state changing

process(CLK_DATA_DIV2,reset_i,EN,EN1,EN2) -- 3-stage shift register
begin
   if (reset_i='1') then
         EN1 <= '0';
         EN2 <= '0';
         EN3 <= '0';
   elsif (CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '1') then
	   if ADC_DRDY = '1' then
         EN1 <= EN;
         EN2 <= EN1;
         EN3 <= EN2;
		end if;
   end if;
end process;
 
J1 <= EN1 and EN2 and EN3;
K1 <= (( not EN1 ) and ( not EN2 ) and ( not EN3 )) when diff_cfg_i='0' else not Sign ;

process(CLK_DATA_DIV2,reset_i,QJK1,J1,K1) -- JK Flip Flop
begin
   if (reset_i='1') then
		QJK1 <= '0';
	elsif (CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '1') then
      if (J1 = '0' and K1 ='0') then
          QJK1 <= QJK1; -- no change
		elsif (J1 = '1' and K1 ='0') then
		    QJK1 <= '1';
		elsif (J1 = '0' and K1 ='1') then
		    QJK1 <= '0';
		elsif (J1 = '1' and K1 ='1') then
		    QJK1 <= not QJK1; -- toggle
		end if;
	end if;
end process;		

EN_ADC_REG <= QJK1;

--Mask the first five ADC_REG signal
process (CLK_DATA_DIV2,reset_i,ADC_DRDY,M1,M2,M3,M4,M5) 
begin
   if (reset_i='1') then
	      M1 <= '0';
			M2 <= '0';
			M3 <= '0';
			M4 <= '0';
			M5 <= '0';
			M6 <= '0';
   elsif CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '1' then
	   if ADC_DRDY = '1' then
	      M1 <= ADC_DRDY;
			M2 <= M1;
			M3 <= M2;
			M4 <= M3;
			M5 <= M4;
			M6 <= M5;
		end if;
	end if;
end process;

process (CLK_DATA_DIV2,reset_i,M6) 
begin
   if (reset_i='1') then
	   Shift_M4 <= '0';
   elsif CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '1' then
	   Shift_M4 <= M6; --M4 before
	end if;
end process;

ADC_REG_Mask <= ADC_DRDY and Shift_M4;--Mask the first ADC_REG signal

--Shift one clock period to obtain the loading signal of the differentiator register. 
process (CLK_DATA_DIV2,reset_i,ADC_REG_Mask,Shift1_ADC_REG,CE_DIFF,CE_DIFF_F1,CE_DIFF_F2) 
begin
   if (reset_i='1') then
      Shift1_ADC_REG <= '0';
		CE_DIFF <= '0';
		CE_DIFF_F1 <= '0';
		CE_DIFF_F2 <= '0';
		CE_DIFF_FILT <= '0'; 
   elsif CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '0' then
      Shift1_ADC_REG <= ADC_REG_Mask;-- CE_DIFF = differentiator register loading signal
		CE_DIFF <= Shift1_ADC_REG;
		CE_DIFF_F1 <= CE_DIFF;
		CE_DIFF_F2 <= CE_DIFF_F1;
		CE_DIFF_FILT <= CE_DIFF_F2; 
   end if;
end process;

RST_REG <= '0';  

--7-Stage 16-Bit shift register
process (CLK_DATA_DIV2,reset_i,ADC_2s_DATA,Yi_plus3_Reg,
	Yi_plus2_Reg,Yi_plus1_Reg,Yi_Reg,Yi_minus1_Reg,Yi_minus2_Reg)
begin
    if (reset_i='1') then
          Yi_plus3_Reg <= (others => '0'); 		 
			 Yi_plus2_Reg <= (others => '0'); 		 
			 Yi_plus1_Reg <= (others => '0'); 		 
			 Yi_Reg <= (others => '0'); 		 
			 Yi_minus1_Reg <= (others => '0'); 		 
			 Yi_minus2_Reg <= (others => '0'); 		 
			 Yi_minus3_Reg <= (others => '0'); 		 
   elsif CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '1' then
		 if RST_REG = '1' then
          Yi_plus3_Reg <= x"0000"; 		 
			 Yi_plus2_Reg <= x"0000";
			 Yi_plus1_Reg <= x"0000";
			 Yi_Reg <= x"0000";
			 Yi_minus1_Reg <= x"0000";
			 Yi_minus2_Reg <= x"0000";
			 Yi_minus3_Reg <= x"0000";
	    elsif ADC_DRDY = '1' then
		    Yi_plus3_Reg <= ADC_2s_DATA;
			 Yi_plus2_Reg <= Yi_plus3_Reg;
	       Yi_plus1_Reg <= Yi_plus2_Reg;
			 Yi_Reg <= Yi_plus1_Reg;
			 Yi_minus1_Reg <= Yi_Reg;
			 Yi_minus2_Reg <= Yi_minus1_Reg;
          Yi_minus3_Reg <= Yi_minus2_Reg; 			 
		 end if;
	 end if;
end process;

Sub_Yip1_Yim1 <= Yi_plus1_Reg - Yi_minus1_Reg; --Signed substractor
Sub_Yip2_Yim2 <= Yi_plus2_Reg - Yi_minus2_Reg;
Subx4_Yip2_Yim2 <= Sub_Yip2_Yim2(15) & Sub_Yip2_Yim2(15) & Sub_Yip2_Yim2 & "00";
Subx5_Yip1_Yim1 <= ( Sub_Yip1_Yim1(15) & Sub_Yip1_Yim1(15) & Sub_Yip1_Yim1 & "00" ) + ( Sub_Yip1_Yim1(15) & Sub_Yip1_Yim1(15) & Sub_Yip1_Yim1(15) & Sub_Yip1_Yim1(15) & Sub_Yip1_Yim1 );
Num <= Subx5_Yip1_Yim1 + Subx4_Yip2_Yim2 + ( Yi_plus3_Reg(15) & Yi_plus3_Reg(15) & Yi_plus3_Reg(15) & Yi_plus3_Reg(15) & Yi_plus3_Reg ) - ( Yi_minus3_Reg(15) & Yi_minus3_Reg(15) & Yi_minus3_Reg(15) & Yi_minus3_Reg(15) & Yi_minus3_Reg );
Gain_Diff_Reg <= Num(19) & Num(19 downto 5); -- Divide by 32

-- Diff_Reg_Update = Loading signal of the Sub_Div2_Reg when ADC samples are below of the threshold value.
process (CLK_DATA_DIV2,reset_i,RST_REG,Gain_Diff_Reg,ADC_2s_DATA)
begin
   if (reset_i='1') then
		diff_or_signal_s <= (others => '0');
   elsif CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '1' then
       if RST_REG = '1' then 
			 diff_or_signal_s <= x"0000";
	    elsif CE_DIFF = '1' then
	       if (diff_cfg_i='0') then
				diff_or_signal_s <= Gain_Diff_Reg; --Divide by 2 Gain_Diff_Reg(16 downto 1)
	       else
			   diff_or_signal_s <= ADC_2s_DATA; 
			 end if;
		 end if;
	 end if;
end process;

process (CLK_DATA_DIV2,reset_i,CE_DIFF,EndPw,CE_DIFF)
begin
   if (reset_i='1') then
		Start_Pw <= '0';
   elsif CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '1' then
	   if EndPw = '1' then
		   Start_Pw <= '0';
	   elsif CE_DIFF = '1' then
		   Start_Pw <= CE_DIFF;
      end if;
   end if;
end process;

-- Load_Dac pulse width counter

process (CLK_DATA_DIV2,reset_i,End_PW,LDPW_Cnt) 
begin
   if (reset_i='1') then
		LDPW_Cnt <= (others => '0');
   elsif CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '0' then
      if End_PW = '1' then
		   LDPW_Cnt <= (others => '0');
		elsif Start_Pw = '1' then
		   LDPW_Cnt <= LDPW_Cnt + 1;
		end if;
	end if;
end process;

EndPw <= '1' when LDPW_Cnt = 6 else '0';-- 6 clock periods of 8ns --> pulse width = 48ns

process (CLK_DATA_DIV2,reset_i,EndPw) 
begin
   if (reset_i='1') then
		End_Pw <= '0';
   elsif CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '1' then
      End_Pw <= EndPw; --Shift EndPw of one-half clock period
	end if;             --Resets pulse width counter (line 221)
end process;

--------------------------------------------------------------------------------------------------------
--Zero crossing detector
process (CLK_DATA_DIV2,reset_i,Sign)
begin
	if (reset_i='1') then
		Sign1 <= '0';
   elsif CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '1' then
		Sign1 <= Sign;
	end if;
end process;

Sign_Trans <= ( not Sign ) and Sign1; --Negative to positive sign transition

Enable_Sign_Trans <= Sign_Trans and EN_ADC_REG;

process (CLK_DATA_DIV2,reset_i,Enable_Sign_Trans,ENAR_Fall,OneTrans)
begin
	if (reset_i='1') then
		OneTrans <= '0';
		One_Trans <= '0';
   elsif CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '0' then
	    if ENAR_Fall = '1' then
		    OneTrans <= '0';
	    elsif Enable_Sign_Trans = '1' then
          OneTrans <= Enable_Sign_Trans; 
		 end if;
       One_Trans <= OneTrans;
	 end if;
end process;

Only_One_Trans <= ( not One_Trans ) and Enable_Sign_Trans;

process (CLK_DATA_DIV2,reset_i,EN_ADC_REG,ENAR1) 
begin
   if (reset_i='1') then
		ENAR1 <= '0';
		ENAR2 <= '0';
   elsif CLK_DATA_DIV2'event and CLK_DATA_DIV2 = '1' then
	   ENAR1 <= EN_ADC_REG;
		ENAR2 <= ENAR1;
	end if;
end process;

--process (CLK_DATA_DIV2,reset_i,ENAR1,ENAR2) 
--begin
--	if (reset_i='1') then
--		ENAR_Fall <= '0';
--	elsif (CLK_DATA_DIV2'event and CLK_DATA_DIV2='1') then
--		if ((( not ENAR1 ) and ENAR2)='1') then
--			ENAR_Fall <= '1';
--		else
--			ENAR_Fall <= '0';
--		end if;
--	end if;
--end process;
ENAR_Fall <= ( not ENAR1 ) and ENAR2;
	 
--to have a wide pulse width
process (CLK_DATA_DIV2,reset_i,Only_One_Trans,cnt_width_p_s)
begin
	if (reset_i='1') then
		PEAK_PULSE		<='0';
		cnt_width_p_s	<= 0;
	elsif (CLK_DATA_DIV2='1' and CLK_DATA_DIV2'event) then
		if (Only_One_Trans='1' or (cnt_width_p_s/=0 and cnt_width_p_s <= WIDTH_PULSE) ) then
			PEAK_PULSE		<='1';
			cnt_width_p_s	<= cnt_width_p_s+1;
		else
			PEAK_PULSE		<='0';
			cnt_width_p_s	<= 0;
		end if;
	end if;
end process;

Biquad:XLXI_Filter
	Port map(
		clk_i					=>CLK_DATA,
		clk_div2_i			=>CLK_DATA_DIV2,
		reset_i				=>reset_i,
		DATA_ADC_i 			=>diff_or_signal_s,
		DATA_rdy_i 			=>CE_DIFF_FILT,
		
		DATA_filtered_o 	=>Signal_Filtered,
		DATA_rdy_o 			=>LOAD_DAC_s
	);
----POUR BYPASSER LE FILTRE

Sign <= Signal_Filtered(15) when filter_on_i='1' else diff_or_signal_s(15);

--REsynchronise PADC et DIFF
process (clk_50MHz_i,reset_i,Signal_Filtered,LOAD_DAC_s,ADC_DATA,ADC_DATA_1_s)
begin
	if (reset_i='1') then
		DIFF_ADC		<= (others=>'0');
		LOAD_DAC		<= '0';
		ADC_DATA_1_s<= (others=>'0');
		DAC_data_o	<= (others=>'0');
	elsif (clk_50MHz_i'event and clk_50MHz_i='1') then
		if (filter_on_i='1') then --filter on
			DIFF_ADC		<= not Signal_Filtered(15) & Signal_Filtered(14 downto 0); --Straight binary output
			LOAD_DAC		<= LOAD_DAC_s;
		else                      --filter off
			DIFF_ADC		<= not diff_or_signal_s(15) & diff_or_signal_s(14 downto 0); --Straight binary output
			LOAD_DAC		<= CE_DIFF_FILT;
		end if;
		ADC_DATA_1_s<= ADC_DATA;
		DAC_data_o	<= ADC_DATA_1_s;
	end if;
end process;

end Behavioral;

