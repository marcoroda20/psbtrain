----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC-MM 
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    09:03:23 23/10/2012
-- Design Name: 
-- Module Name:    PPD_Win - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


use work.peak_detector_pkg.all;

entity PPD_Win is
    port ( 
		clk_50MHz_i : in std_logic;
		reset_i 		: in std_logic;
		pulse_i 		: in  STD_LOGIC;
		pulse_o 		: out std_logic;
		C0_pulse_i	: in  STD_LOGIC;
		--Register
		eff_sim_bit_i		: in  std_logic;
		pulse_width_i		: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		win_delay_i			: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		win_width_i			: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		en_win_i				: in std_logic;
		en_trig_sim_i		: in std_logic;
		trig_sim_delay_i	: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		trig_forc_delay_i	: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		
		no_trig_o	: out std_logic;
		pulse_ok_o	: out std_logic;
		pulse_fail_o: out std_logic;
		window_o		: out std_logic
	);
end PPD_Win;

architecture Behavioral of PPD_Win is

--Constant
constant WINWIDTH_DEFAULT		: std_logic_vector(NB_BIT_COUNTER-1 downto 0) := x"000f4240";
constant PULSEWIDTH_DEFAULT	: std_logic_vector(NB_BIT_COUNTER-1 downto 0) := x"00000032";

--Type declaration
type ppd_win_states_t is (
	IDLE,
	DELAY,
	WINDOW,
	PULSE,
	WAIT_FORCED,
	PULSE_SIMUL_FORCED,
	NO_PULSE,
	WAIT_END
	);

--Signal Declaration
signal state_s	: ppd_win_states_t;

signal C0_pulse_1_s : std_logic;	
signal C0_pulse_2_s : std_logic;	
signal C0_pulse_3_s : std_logic;	
signal C0_0_s : std_logic;
signal C0_1_s : std_logic;

signal cnt_delay_s : unsigned(NB_BIT_COUNTER-1 downto 0);
signal cnt_window_s : unsigned(NB_BIT_COUNTER-1 downto 0);
signal cnt_pulse_s : unsigned(NB_BIT_COUNTER-1 downto 0);
signal cnt_forced_s : unsigned(NB_BIT_COUNTER-1 downto 0);
--signal cnt_simul_s : unsigned(NB_BIT_COUNTER-1 downto 0);

signal cnt_delay_latched_s : unsigned(NB_BIT_COUNTER-1 downto 0);
signal cnt_pulse_latched_s : unsigned(NB_BIT_COUNTER-1 downto 0);
signal cnt_simul_latched_s : unsigned(NB_BIT_COUNTER-1 downto 0);

signal pulse_width_s : unsigned(NB_BIT_COUNTER-1 downto 0);
signal win_delay_s : unsigned(NB_BIT_COUNTER-1 downto 0);
signal win_width_s : unsigned(NB_BIT_COUNTER-1 downto 0);
--signal trig_sim_delay_s : unsigned(NB_BIT_COUNTER-1 downto 0);
signal trig_forc_delay_s : unsigned(NB_BIT_COUNTER-1 downto 0);

signal pulse_in_1_s	: std_logic;	
signal pulse_s			: std_logic;	
signal pulse_ok_s		: std_logic;		
signal pulse_fail_s	: std_logic;		
signal window_s		: std_logic;		
signal window_start_s: std_logic;		
signal pulse_1_s		: std_logic;	
signal pulse_ok_1_s 	: std_logic;	
signal pulse_fail_1_s: std_logic;	
signal window_1_s 	: std_logic;	

signal pulse_in_s	: std_logic;

attribute keep : string;
attribute keep of C0_pulse_i: signal is "TRUE";
attribute keep of pulse_i: signal is "TRUE";

attribute keep of state_s: signal is "TRUE";

attribute keep of pulse_s: signal is "TRUE";
attribute keep of pulse_ok_s: signal is "TRUE";
attribute keep of pulse_fail_s: signal is "TRUE";
attribute keep of window_s: signal is "TRUE";
attribute keep of window_start_s: signal is "TRUE";

attribute keep of cnt_delay_s: signal is "TRUE";
attribute keep of cnt_window_s: signal is "TRUE";
attribute keep of cnt_pulse_s: signal is "TRUE";
attribute keep of cnt_forced_s: signal is "TRUE";

attribute keep of win_delay_i: signal is "TRUE";
attribute keep of pulse_width_i: signal is "TRUE";
attribute keep of win_width_i: signal is "TRUE";
attribute keep of trig_sim_delay_i: signal is "TRUE";
attribute keep of trig_forc_delay_i: signal is "TRUE";
attribute keep of en_win_i: signal is "TRUE";
attribute keep of en_trig_sim_i: signal is "TRUE";
attribute keep of eff_sim_bit_i: signal is "TRUE";

attribute keep of no_trig_o: signal is "TRUE";
attribute keep of pulse_o: signal is "TRUE";

begin

--Input 
process (clk_50MHz_i,reset_i)
begin
	if rising_edge(clk_50MHz_i) then
		if (reset_i='1') then
			C0_pulse_1_s		<= '0';
			C0_pulse_2_s		<= '0';
			C0_pulse_3_s		<= '0';
			pulse_width_s		<= (others=>'0');
			win_delay_s			<= (others=>'0');
			win_width_s			<= (others=>'0');
			trig_forc_delay_s	<= (others=>'0');
			pulse_in_1_s		<= '0';
			pulse_in_s			<= '0';
		else
			--C0 pulse treatment
			C0_pulse_1_s <= C0_pulse_i;
			C0_pulse_2_s <= C0_pulse_1_s;
			C0_pulse_3_s <= C0_pulse_2_s;
			if (C0_pulse_1_s='1' and C0_pulse_2_s='1' and C0_pulse_3_s='1') then
				C0_1_s <='1';
			else
				C0_1_s <='0';
			end if;
			if (C0_pulse_1_s='0' and C0_pulse_2_s='0' and C0_pulse_3_s='0') then
				C0_0_s <='1';
			else
				C0_0_s <='0';
			end if;
			--Pulse width with default value
			if (pulse_width_i=x"00000000") then
				pulse_width_s		<= unsigned(PULSEWIDTH_DEFAULT);
			else
				pulse_width_s		<= unsigned(pulse_width_i);
			end if;
			--Delay for window
			win_delay_s			<= unsigned(win_delay_i);
			--Window width
			if (win_width_i=x"00000000") then
				win_width_s			<= unsigned(WINWIDTH_DEFAULT);
			else
				win_width_s			<= unsigned(win_width_i);
			end if;
			--Delay for different configuration of pulse
			trig_forc_delay_s	<= unsigned(trig_forc_delay_i);
			--Rise detection of pulse_i
			pulse_in_1_s		<= pulse_i;
			if (pulse_i='1' and pulse_in_1_s='0') then
				pulse_in_s	<= '1';
			else
				pulse_in_s	<= '0';
			end if;
		end if;
	end if;
end process;

--Resynchronize output of state machine
process(clk_50MHz_i,reset_i)
begin
	if rising_edge(clk_50MHz_i) then
		if (reset_i='1') then
			pulse_o		<= '1';
			pulse_ok_o 	<= '0';
			pulse_fail_o<= '0';
			window_o 	<= '0';
		else
			pulse_ok_o 	<= pulse_ok_s;
			pulse_fail_o<= pulse_fail_s;
			if (state_s = PULSE or 
				 state_s = PULSE_SIMUL_FORCED) then
				if (pulse_ok_s='1') then
					no_trig_o <= '0';
				else
					no_trig_o <= '1';
				end if;
			end if;
			pulse_o		<= not pulse_s;
			window_o 	<= window_s;
		end if;
	end if;
end process;

-- State machine
process (clk_50MHz_i,reset_i) 
begin
   if rising_edge(clk_50MHz_i) then  
		if (reset_i='1') then
			state_s <= IDLE;
			pulse_s			<= '0';
			pulse_ok_s		<= '0';
			pulse_fail_s	<= '0';
			window_start_s	<= '0';
		else
			case state_s is
				when IDLE =>
					if (en_win_i='1') then
						if (C0_pulse_i='0' and C0_0_s='1') then
							state_s <= DELAY;
						end if;
					else
						if (pulse_in_s = '1') then
							state_s <= PULSE;
						end if;
					end if;
					pulse_s			<= '0';
					pulse_ok_s		<= '0';
					pulse_fail_s	<= '0';
					window_start_s	<= '0';
				when DELAY =>
					if (cnt_delay_s >= win_delay_s) then
						state_s <= WINDOW;
					end if;
					pulse_s			<= '0';
					pulse_ok_s		<= '0';
					pulse_fail_s	<= '0';
					window_start_s	<= '0';
				when WINDOW =>
					if (pulse_in_s = '1') then
						state_s <= PULSE;
					elsif (cnt_window_s >= win_width_s) then
						if (en_trig_sim_i='1') then
							state_s <= WAIT_FORCED;
						else
							state_s <= NO_PULSE;
						end if;
					end if;
					pulse_s			<= '0';
					pulse_ok_s		<= '0';
					pulse_fail_s	<= '0';
					window_start_s	<= '1';
				when PULSE =>
					if (cnt_pulse_s >= pulse_width_s) then
						if (en_win_i='1') then
							state_s <= WAIT_END;
						else
							state_s <= IDLE;
						end if;
					end if;
					pulse_s			<= '1';
					pulse_ok_s		<= '1';
					pulse_fail_s	<= '0';
					window_start_s	<= '0';
				when WAIT_FORCED =>
					if (cnt_forced_s >= trig_forc_delay_s) then
						state_s <= PULSE_SIMUL_FORCED;
					end if;
					pulse_s			<= '0';
					pulse_ok_s		<= '0';
					pulse_fail_s	<= '1';
					window_start_s	<= '0';
				when PULSE_SIMUL_FORCED =>
					if (cnt_pulse_s >= pulse_width_s) then
						state_s <= WAIT_END;
					end if;
					pulse_s			<= '1';
					pulse_ok_s		<= '0';
					pulse_fail_s	<= '1';
					window_start_s	<= '0';
				when NO_PULSE =>
					state_s <= WAIT_END;
					pulse_s			<= '0';
					pulse_ok_s		<= '0';
					pulse_fail_s	<= '1';
					window_start_s	<= '0';
				when WAIT_END =>
					if (C0_pulse_i='1' and C0_1_s='1') then
						state_s <= IDLE;
					end if;
					pulse_s			<= '0';
					pulse_ok_s		<= '0';
					pulse_fail_s	<= '0';
					window_start_s	<= '0';
			end case;
		end if;
	end if;
end process;

--Timing
process (clk_50MHz_i,reset_i)
begin
	if rising_edge(clk_50MHz_i) then
		if (reset_i='1') then
			cnt_delay_s		<= (others=>'0');
			cnt_pulse_s		<= (others=>'0');
			cnt_forced_s	<= (others=>'0');
		else
			if (state_s = DELAY) then
				cnt_delay_s		<= cnt_delay_s+1;
			else
				cnt_delay_s		<= (others=>'0');
			end if;
			if (state_s = PULSE or state_s = PULSE_SIMUL_FORCED) then
				cnt_pulse_s		<= cnt_pulse_s+1;
			else
				cnt_pulse_s		<= (others=>'0');
			end if;
			if (state_s = WAIT_FORCED) then
				cnt_forced_s		<= cnt_forced_s+1;
			else
				cnt_forced_s		<= (others=>'0');
			end if;
		end if;
	end if;
end process;

--Window generation
process (clk_50MHz_i,reset_i)
begin
	if rising_edge(clk_50MHz_i) then
		if (reset_i='1') then
			cnt_window_s <= (others=>'0');
			window_s		<= '0';
		else
			if ((window_start_s='1' and cnt_window_s=0) or
				  (cnt_window_s/=0 and 
					cnt_window_s <= win_width_s)) then
				cnt_window_s	<= cnt_window_s+1;
				window_s			<= '1';
			else
				cnt_window_s 	<= (others=>'0');
				window_s			<= '0';
			end if;
		end if;
	end if;
end process;

end Behavioral;

