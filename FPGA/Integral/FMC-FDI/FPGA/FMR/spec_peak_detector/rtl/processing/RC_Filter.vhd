----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:02:27 09/29/2011 
-- Design Name: 
-- Module Name:    RC_Filter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RC_Filter is
    Port ( Vo : out  STD_LOGIC_VECTOR(15 downto 0);
           CLK : in  STD_LOGIC);
end RC_Filter;

architecture Behavioral of RC_Filter is

signal Vi, Filt, k, Sub : std_logic_vector(15 downto 0) := x"0000";
signal M, Filt_Out : std_logic_vector(31 downto 0) := x"00000000";
signal CE_DIFF_FILT, RST_REG : std_logic :='0';

begin

-- 1st order low pass digital filter parameters :

-- y[n] = y[n-1] + k( x[n-1]-y[n-1] ) 
-- k = dt/RC
-- y[n] = Vo'
-- y[n-1] = Vo
-- x[n-1] = Vi

k <= x"1000";

Vi <= x"7FFF";

RST_REG <= '0';

CE_DIFF_FILT <= '1';

Sub <= Vi - Filt;

M <= k * Sub;

process (CLK) 
begin
   if CLK'event and CLK = '1' then
      if RST_REG = '1' then 
         Filt_Out <= (others => '0');
      elsif CE_DIFF_FILT = '1' then
         Filt_Out <= Filt_Out + M;
      end if;
   end if;
end process;

Filt <= Filt_Out(31 downto 16);

Vo <= Filt;

end Behavioral;


