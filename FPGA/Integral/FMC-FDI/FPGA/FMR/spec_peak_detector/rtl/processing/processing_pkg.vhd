----------------------------------------------------------------------------------
-- Company: 		CERN/TE-MS-MM
-- Engineer: 		Daniel Oberson
-- 
-- Description: Package with some utils core
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

library work;
use work.peak_detector_pkg.all;

package processing_pkg is

	component Differentiator 
		port( 
			clk_50MHz_i	: in std_logic;
			reset_i		: in std_logic;
		   diff_cfg_i  : in std_logic;
			filter_on_i : in std_logic;
			ADC_DATA		: in  STD_LOGIC_VECTOR (15 downto 0);
			THRESH : in  STD_LOGIC_VECTOR (NB_BIT_THRESHOLD-1 downto 0);
			CLK_DATA : in  STD_LOGIC;
			ADC_REG : in  STD_LOGIC;
			LOAD_DAC, PEAK_PULSE : out STD_LOGIC;
			DAC_data_o : out std_logic_vector(15 downto 0);
			DIFF_ADC : out  STD_LOGIC_VECTOR (15 downto 0)
		);			  
	end component;

	component ADC_DIFF_DACsm 
		port(
			clk_20MHz_i		: in std_logic;
			clk_50MHz_i		: in std_logic;
			clk_500MHz_i	: in std_logic;
			clk_250MHz_i	: in std_logic;
			
			DCM_lock_i		: in std_logic;

			reset_i			: in std_logic;

			delay_clk_i : std_logic_vector(3 downto 0);

			filter_on_i  : in std_logic;
			diff_cfg_1_i : in std_logic;
			diff_cfg_2_i : in std_logic;

			ADC_DCO_1_i		: in std_logic;
			ADC_D_1_i		: in std_logic;
			
			ADC_data_1_o		: out std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
			DIFF_data_1_o		: out std_logic_vector(NB_BIT_DIFF_DATA_BUS-1 downto 0);
			ADC_data_rdy_1_o	: out std_logic;
			
			ADC_DCO_2_i		: in std_logic;
			ADC_D_2_i		: in std_logic;
			
			ADC_data_2_o		: out std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
			DIFF_data_2_o		: out std_logic_vector(NB_BIT_DIFF_DATA_BUS-1 downto 0);
			ADC_data_rdy_2_o	: out std_logic;
					
			ADC_CNV_o		: out std_logic;
			ADC_CLK_o		: out std_logic;
			
			threshold_1_i	: in std_logic_vector(NB_BIT_THRESHOLD-1 downto 0);
			threshold_2_i	: in std_logic_vector(NB_BIT_THRESHOLD-1 downto 0);

			DAC_sel_1_i		: in std_logic_vector(NB_BIT_DAC_SEL-1 downto 0);
			DAC_sel_2_i		: in std_logic_vector(NB_BIT_DAC_SEL-1 downto 0);
			
			DAC_prog_data_1_i	: in  std_logic_vector (NB_BIT_ADC_DATA_BUS-1 downto 0);
			DAC_data_bus_1_o	: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
			DAC_load_1_o		: out std_logic;
			
			DAC_prog_data_2_i	: in  std_logic_vector (NB_BIT_ADC_DATA_BUS-1 downto 0);
			DAC_data_bus_2_o	: out std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
			DAC_load_2_o		: out std_logic;
			
			peak_pulse_1_o	: out std_logic;
			peak_pulse_2_o	: out std_logic
		);
	end component;

	component c0_time 
		port(
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			c0_pulse_i   : in std_logic;
			trig_pulse_i : in std_logic;
--			trig_miss_i  : in std_logic;
			
			c0_time_o : out std_logic_vector(31 downto 0) 
		);
	end component;

	component PPD_Win 
		 port ( 
			clk_50MHz_i : in std_logic;
			reset_i 		: in std_logic;
			pulse_i 		: in  STD_LOGIC;
			pulse_o 		: out std_logic;
			C0_pulse_i	: in  STD_LOGIC;
			--Register
			eff_sim_bit_i		: in  std_logic;
			pulse_width_i		: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
			win_delay_i			: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
			win_width_i			: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
			en_win_i				: in std_logic;
			en_trig_sim_i		: in std_logic;
			trig_sim_delay_i	: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
			trig_forc_delay_i	: in std_logic_vector(NB_BIT_COUNTER-1 downto 0);
			
			no_trig_o	: out std_logic;
			pulse_ok_o	: out std_logic;
			pulse_fail_o: out std_logic;
			window_o		: out std_logic
		);
	end component;

	component XLXI_Filter 
		Port(
			clk_i					: in std_logic; --250MHz
			clk_div2_i			: in std_logic; --125MHz
			reset_i				: in std_logic;
			DATA_ADC_i 			: in std_logic_vector(15 downto 0);
			DATA_rdy_i 			: in std_logic;

			DATA_filtered_o 	: out std_logic_vector(15 downto 0);
			DATA_rdy_o 			: out std_logic
		);
	end component;

end processing_pkg;

