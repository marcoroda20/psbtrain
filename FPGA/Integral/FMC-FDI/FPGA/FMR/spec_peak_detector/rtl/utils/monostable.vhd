----------------------------------------------------------------------------------
-- Company:       CERN TE/MSC/MM
-- Engineer:      Daniel Oberson
-- 
-- Create Date:    15:55:47 12/13/2013 
-- Design Name: 
-- Module Name:    monostable - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;
use IEEE.math_real."ceil";
use IEEE.math_real."log2";

entity monostable is
	generic(
		g_n_clk : integer := 4
	);
	port (
		clk_i			: in std_logic;
		reset_i		: in std_logic;
		
		s_i	: in std_logic;
		s_o	: out std_logic
	);
end monostable;

architecture Behavioral of monostable is

--	signal count_s : integer;
	signal count_s : unsigned(integer(ceil(log2(real(g_n_clk))))-1 downto 0);

begin

	process (clk_i)
	begin 
		if rising_edge(clk_i) then
			if (reset_i='1') then
				s_o <='0';
--				count_s <= 0;
				count_s <= (others=>'0');
			else
--				if ((s_i='1') or ( (count_s>0) and (count_s<g_n_clk))) then
				if ((s_i='1') or ( (count_s>unsigned(to_signed(0,count_s'length))) and
				                   (count_s<unsigned(to_signed(g_n_clk,count_s'length))))) then
					s_o     <='1';
					count_s <= count_s + 1;
				else
					s_o     <='0';
--					count_s <= 0;
					count_s <= (others=>'0');
				end if;
			end if;
		end if;
	end process;

end Behavioral;

