---------------------------------------------------------------------------------------
-- Title          : Wishbone slave core for Peak Detector register
---------------------------------------------------------------------------------------
-- File           : peak_detect_reg.vhd
-- Author         : auto-generated by wbgen2 from peak_detect_reg.wb
-- Created        : Mon Jul 14 16:54:18 2014
-- Standard       : VHDL'87
---------------------------------------------------------------------------------------
-- THIS FILE WAS GENERATED BY wbgen2 FROM SOURCE FILE peak_detect_reg.wb
-- DO NOT HAND-EDIT UNLESS IT'S ABSOLUTELY NECESSARY!
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity peak_detect_reg is
  port (
    rst_n_i                                  : in     std_logic;
    clk_sys_i                                : in     std_logic;
    wb_adr_i                                 : in     std_logic_vector(4 downto 0);
    wb_dat_i                                 : in     std_logic_vector(31 downto 0);
    wb_dat_o                                 : out    std_logic_vector(31 downto 0);
    wb_cyc_i                                 : in     std_logic;
    wb_sel_i                                 : in     std_logic_vector(3 downto 0);
    wb_stb_i                                 : in     std_logic;
    wb_we_i                                  : in     std_logic;
    wb_ack_o                                 : out    std_logic;
    wb_stall_o                               : out    std_logic;
-- Port for BIT field: 'No Trigger high' in reg: 'Status Register'
    reg_st_notrig1_i                         : in     std_logic;
-- Port for BIT field: 'FMR or NMR high' in reg: 'Status Register'
    reg_st_fnmr1_o                           : out    std_logic;
-- Port for BIT field: 'Simulation or effective bit' in reg: 'Status Register'
    reg_st_simeff_i                          : in     std_logic;
-- Port for BIT field: 'Enable test DDR' in reg: 'Status Register'
    reg_st_ddr_tst_o                         : out    std_logic;
-- Port for BIT field: 'Enable HW reset' in reg: 'Status Register'
    reg_st_en_hw_reset_o                     : out    std_logic;
-- Port for BIT field: 'Reset HW input' in reg: 'Status Register'
    reg_st_reset_hw_i                        : in     std_logic;
-- Port for BIT field: 'Reset SW input' in reg: 'Status Register'
    reg_st_reset_sw_o                        : out    std_logic;
-- Port for BIT field: 'DDR Samples enable' in reg: 'Status Register'
    reg_st_samples_wr_en_o                   : out    std_logic;
-- Port for BIT field: 'Initialization done bit' in reg: 'Status Register'
    reg_st_init_done_i                       : in     std_logic;
-- Port for BIT field: 'No Trigger low' in reg: 'Status Register'
    reg_st_notrig2_i                         : in     std_logic;
-- Port for BIT field: 'FMR or NMR low' in reg: 'Status Register'
    reg_st_fnmr2_o                           : out    std_logic;
-- Port for std_logic_vector field: 'Direction' in reg: 'Digital I/O'
    reg_dio_dir_o                            : out    std_logic_vector(9 downto 0);
-- Port for std_logic_vector field: 'Configuration' in reg: 'Digital I/O'
    reg_dio_cfg_o                            : out    std_logic_vector(9 downto 0);
-- Port for std_logic_vector field: 'Input' in reg: 'Digital I/O'
    reg_dio_input_i                          : in     std_logic_vector(9 downto 0);
-- Port for std_logic_vector field: 'decim_factor' in reg: 'Decimator'
    reg_decim_factor_o                       : out    std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'DDR range' in reg: 'Memory Range'
    reg_ddr_range_o                          : out    std_logic_vector(24 downto 0);
-- Port for std_logic_vector field: 'Last sample' in reg: 'Last sample'
    reg_last_addr_i                          : in     std_logic_vector(24 downto 0);
-- Port for std_logic_vector field: 'ADC1' in reg: 'ADC data'
    reg_adc_data_1_i                         : in     std_logic_vector(15 downto 0);
-- Port for std_logic_vector field: 'ADC2' in reg: 'ADC data'
    reg_adc_data_2_i                         : in     std_logic_vector(15 downto 0);
-- Port for std_logic_vector field: 'DATA1' in reg: 'DAC register'
    reg_dac_data_1_i                         : in     std_logic_vector(15 downto 0);
-- Port for std_logic_vector field: 'DATA2' in reg: 'DAC register'
    reg_dac_data_2_i                         : in     std_logic_vector(15 downto 0);
-- Port for std_logic_vector field: 'DATA1_bus_prog' in reg: 'DAC bus program register 1'
    reg_prg_bus_data_1_o                     : out    std_logic_vector(15 downto 0);
-- Port for std_logic_vector field: 'DATA2_bus_prog' in reg: 'DAC bus program register 2'
    reg_prg_bus_data_2_o                     : out    std_logic_vector(15 downto 0);
-- Port for std_logic_vector field: 'THR1' in reg: 'Threshold 1'
    reg_thr_chan1_o                          : out    std_logic_vector(15 downto 0);
-- Port for std_logic_vector field: 'THR2' in reg: 'Threshold 2'
    reg_thr_chan2_o                          : out    std_logic_vector(15 downto 0);
-- Port for BIT field: 'ENWIN1' in reg: 'Configuration'
    reg_cfg_enwin1_o                         : out    std_logic;
-- Port for BIT field: 'ENTRIGWIN1' in reg: 'Configuration'
    reg_cfg_entrigwin1_o                     : out    std_logic;
-- Port for BIT field: 'Filter On/Off' in reg: 'Configuration'
    reg_cfg_filter_on_o                      : out    std_logic;
-- Port for std_logic_vector field: 'DACSEL1' in reg: 'Configuration'
    reg_cfg_dacsel1_o                        : out    std_logic_vector(2 downto 0);
-- Port for std_logic_vector field: 'Delay Clk' in reg: 'Configuration'
    reg_cfg_delay_clk_o                      : out    std_logic_vector(3 downto 0);
-- Port for BIT field: 'Differential or signal' in reg: 'Configuration'
    reg_cfg_diff_cfg_1_o                     : out    std_logic;
-- Port for BIT field: 'ENWIN2' in reg: 'Configuration'
    reg_cfg_enwin2_o                         : out    std_logic;
-- Port for BIT field: 'ENTRIGWIN2' in reg: 'Configuration'
    reg_cfg_entrigwin2_o                     : out    std_logic;
-- Port for std_logic_vector field: 'DACSEL2' in reg: 'Configuration'
    reg_cfg_dacsel2_o                        : out    std_logic_vector(2 downto 0);
-- Port for BIT field: 'Differential or signal' in reg: 'Configuration'
    reg_cfg_diff_cfg_2_o                     : out    std_logic;
-- Port for std_logic_vector field: 'WINDELAY1' in reg: 'Windows delay high'
    reg_windelay1_o                          : out    std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'WINDELAY2' in reg: 'Windows delay low'
    reg_windelay2_o                          : out    std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'WINWIDTH1' in reg: 'Windows width 1'
    reg_winwidth1_o                          : out    std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'WINWIDTH2' in reg: 'Windows width low'
    reg_winwidth2_o                          : out    std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'TRIGWIDTH1' in reg: 'Trigger width high'
    reg_trigwidth1_o                         : out    std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'TRIGWIDTH2' in reg: 'Trigger width low'
    reg_trigwidth2_o                         : out    std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'TRIGDELFORCED1' in reg: 'Trigger delay forced high'
    reg_trigdelforced1_o                     : out    std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'TRIGDELFORCED2' in reg: 'Trigger delay forced low'
    reg_trigdelforced2_o                     : out    std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'TRIGDELSIM1' in reg: 'Trigger delay simulated high'
    reg_trigdelsim1_o                        : out    std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'TRIGDELSIM2' in reg: 'Trigger delay simulated low'
    reg_trigdelsim2_o                        : out    std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'C0+time channel F' in reg: 'C0+time channel F'
    reg_c0_time_f_i                          : in     std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'C0+time channel D' in reg: 'C0+time channel D'
    reg_c0_time_d_i                          : in     std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'CTYPE' in reg: 'Cycle type'
    reg_ctype_i                              : in     std_logic_vector(31 downto 0)
  );
end peak_detect_reg;

architecture syn of peak_detect_reg is

signal reg_st_fnmr1_int                         : std_logic      ;
signal reg_st_ddr_tst_int                       : std_logic      ;
signal reg_st_en_hw_reset_int                   : std_logic      ;
signal reg_st_reset_sw_int                      : std_logic      ;
signal reg_st_samples_wr_en_int                 : std_logic      ;
signal reg_st_fnmr2_int                         : std_logic      ;
signal reg_dio_dir_int                          : std_logic_vector(9 downto 0);
signal reg_dio_cfg_int                          : std_logic_vector(9 downto 0);
signal reg_decim_factor_int                     : std_logic_vector(31 downto 0);
signal reg_ddr_range_int                        : std_logic_vector(24 downto 0);
signal reg_prg_bus_data_1_int                   : std_logic_vector(15 downto 0);
signal reg_prg_bus_data_2_int                   : std_logic_vector(15 downto 0);
signal reg_thr_chan1_int                        : std_logic_vector(15 downto 0);
signal reg_thr_chan2_int                        : std_logic_vector(15 downto 0);
signal reg_cfg_enwin1_int                       : std_logic      ;
signal reg_cfg_entrigwin1_int                   : std_logic      ;
signal reg_cfg_filter_on_int                    : std_logic      ;
signal reg_cfg_dacsel1_int                      : std_logic_vector(2 downto 0);
signal reg_cfg_delay_clk_int                    : std_logic_vector(3 downto 0);
signal reg_cfg_diff_cfg_1_int                   : std_logic      ;
signal reg_cfg_enwin2_int                       : std_logic      ;
signal reg_cfg_entrigwin2_int                   : std_logic      ;
signal reg_cfg_dacsel2_int                      : std_logic_vector(2 downto 0);
signal reg_cfg_diff_cfg_2_int                   : std_logic      ;
signal reg_windelay1_int                        : std_logic_vector(31 downto 0);
signal reg_windelay2_int                        : std_logic_vector(31 downto 0);
signal reg_winwidth1_int                        : std_logic_vector(31 downto 0);
signal reg_winwidth2_int                        : std_logic_vector(31 downto 0);
signal reg_trigwidth1_int                       : std_logic_vector(31 downto 0);
signal reg_trigwidth2_int                       : std_logic_vector(31 downto 0);
signal reg_trigdelforced1_int                   : std_logic_vector(31 downto 0);
signal reg_trigdelforced2_int                   : std_logic_vector(31 downto 0);
signal reg_trigdelsim1_int                      : std_logic_vector(31 downto 0);
signal reg_trigdelsim2_int                      : std_logic_vector(31 downto 0);
signal ack_sreg                                 : std_logic_vector(9 downto 0);
signal rddata_reg                               : std_logic_vector(31 downto 0);
signal wrdata_reg                               : std_logic_vector(31 downto 0);
signal bwsel_reg                                : std_logic_vector(3 downto 0);
signal rwaddr_reg                               : std_logic_vector(4 downto 0);
signal ack_in_progress                          : std_logic      ;
signal wr_int                                   : std_logic      ;
signal rd_int                                   : std_logic      ;
signal allones                                  : std_logic_vector(31 downto 0);
signal allzeros                                 : std_logic_vector(31 downto 0);

begin
-- Some internal signals assignments. For (foreseen) compatibility with other bus standards.
  wrdata_reg <= wb_dat_i;
  bwsel_reg <= wb_sel_i;
  rd_int <= wb_cyc_i and (wb_stb_i and (not wb_we_i));
  wr_int <= wb_cyc_i and (wb_stb_i and wb_we_i);
  allones <= (others => '1');
  allzeros <= (others => '0');
-- 
-- Main register bank access process.
  process (clk_sys_i, rst_n_i)
  begin
    if (rst_n_i = '0') then 
      ack_sreg <= "0000000000";
      ack_in_progress <= '0';
      rddata_reg <= "00000000000000000000000000000000";
      reg_st_fnmr1_int <= '0';
      reg_st_en_hw_reset_int <= '0';
      reg_st_ddr_tst_int <= '0';                --DDR test off
      reg_st_reset_sw_int <= '0';
      reg_st_samples_wr_en_int <= '1';          --DDR write enabled 
      reg_st_fnmr2_int <= '0';
      reg_dio_dir_int <= "1111001111";          --x"3CF"
      reg_dio_cfg_int <= "0000000000";          --x"000" all configured as active low
      reg_prg_bus_data_1_int <= "0000000000000000";
      reg_prg_bus_data_2_int <= "0000000000000000";
      reg_thr_chan1_int <= x"e000";             -- -2.5V
      reg_thr_chan2_int <= x"e000";             -- -2.5V 
      reg_cfg_enwin1_int <= '1';
      reg_cfg_entrigwin1_int <= '1';            --'1'=> there is automatic trigger at the end of the window (for simulated Btrain)
      reg_cfg_filter_on_int <= '1';             --Filter on by default
      reg_cfg_dacsel1_int <= "101";
      reg_cfg_delay_clk_int <= "0101";          --10ns of delay on ADC_CLK to compensate CNV resynchronisation
      reg_cfg_diff_cfg_1_int <= '0';            --'0'=>differential
      reg_cfg_enwin2_int <= '1';
      reg_cfg_entrigwin2_int <= '1';            --'1'=> there is automatic trigger at the end of the window (for simulated Btrain)
      reg_cfg_dacsel2_int <= "100";
      reg_cfg_diff_cfg_2_int <= '0';            --'0'=>differential
      reg_windelay1_int <= x"003D08F7";         --80ms
      reg_windelay2_int <= x"003D08F7";         --85ms
      reg_winwidth1_int <= x"000f423f";         --20ms
      reg_winwidth2_int <= x"000f423f";         --20ms
      reg_trigwidth1_int <= x"00001387";        --100us
      reg_trigwidth2_int <= x"00001387";        --100us
      reg_trigdelforced1_int <= x"0000c350";    --1ms
      reg_trigdelforced2_int <= x"0000c350";    --1ms
      reg_trigdelsim1_int <= x"0044aa20";       --90ms
      reg_trigdelsim2_int <= x"0044aa20";       --90ms
      reg_decim_factor_int <= x"000000c8";      -- 200
      reg_ddr_range_int <= '0' & x"06ddd0";--x"057e40";     -- 360000
    elsif rising_edge(clk_sys_i) then
-- advance the ACK generator shift register
      ack_sreg(8 downto 0) <= ack_sreg(9 downto 1);
      ack_sreg(9) <= '0';
      if (ack_in_progress = '1') then
        if (ack_sreg(0) = '1') then
          ack_in_progress <= '0';
        else
        end if;
      else
        if ((wb_cyc_i = '1') and (wb_stb_i = '1')) then
          case rwaddr_reg(4 downto 0) is
          when "00000" => 
            if (wb_we_i = '1') then
              reg_st_fnmr1_int <= wrdata_reg(1);
              reg_st_ddr_tst_int <= wrdata_reg(3);
              reg_st_en_hw_reset_int <= wrdata_reg(4);
              reg_st_reset_sw_int <= wrdata_reg(6);
              reg_st_samples_wr_en_int <= wrdata_reg(7);
              reg_st_fnmr2_int <= wrdata_reg(17);
            end if;
            rddata_reg(0) <= reg_st_notrig1_i;
            rddata_reg(1) <= reg_st_fnmr1_int;
            rddata_reg(2) <= reg_st_simeff_i;
            rddata_reg(3) <= reg_st_ddr_tst_int;
            rddata_reg(4) <= reg_st_en_hw_reset_int;
            rddata_reg(5) <= reg_st_reset_hw_i;
            rddata_reg(6) <= reg_st_reset_sw_int;
            rddata_reg(7) <= reg_st_samples_wr_en_int;
            rddata_reg(8) <= reg_st_init_done_i;
            rddata_reg(16) <= reg_st_notrig2_i;
            rddata_reg(17) <= reg_st_fnmr2_int;
            rddata_reg(9) <= 'X';
            rddata_reg(10) <= 'X';
            rddata_reg(11) <= 'X';
            rddata_reg(12) <= 'X';
            rddata_reg(13) <= 'X';
            rddata_reg(14) <= 'X';
            rddata_reg(15) <= 'X';
            rddata_reg(18) <= 'X';
            rddata_reg(19) <= 'X';
            rddata_reg(20) <= 'X';
            rddata_reg(21) <= 'X';
            rddata_reg(22) <= 'X';
            rddata_reg(23) <= 'X';
            rddata_reg(24) <= 'X';
            rddata_reg(25) <= 'X';
            rddata_reg(26) <= 'X';
            rddata_reg(27) <= 'X';
            rddata_reg(28) <= 'X';
            rddata_reg(29) <= 'X';
            rddata_reg(30) <= 'X';
            rddata_reg(31) <= 'X';
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "00001" => 
            if (wb_we_i = '1') then
              reg_dio_dir_int <= wrdata_reg(9 downto 0);
              reg_dio_cfg_int <= wrdata_reg(19 downto 10);
            end if;
            rddata_reg(9 downto 0) <= reg_dio_dir_int;
            rddata_reg(19 downto 10) <= reg_dio_cfg_int;
            rddata_reg(29 downto 20) <= reg_dio_input_i;
            rddata_reg(30) <= 'X';
            rddata_reg(31) <= 'X';
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "00010" => 
            if (wb_we_i = '1') then
              reg_decim_factor_int <= wrdata_reg(31 downto 0);
            end if;
            rddata_reg(31 downto 0) <= reg_decim_factor_int;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "00011" => 
            if (wb_we_i = '1') then
              reg_ddr_range_int <= wrdata_reg(24 downto 0);
            end if;
            rddata_reg(24 downto 0) <= reg_ddr_range_int;
            rddata_reg(25) <= 'X';
            rddata_reg(26) <= 'X';
            rddata_reg(27) <= 'X';
            rddata_reg(28) <= 'X';
            rddata_reg(29) <= 'X';
            rddata_reg(30) <= 'X';
            rddata_reg(31) <= 'X';
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "00100" => 
            if (wb_we_i = '1') then
            end if;
            rddata_reg(24 downto 0) <= reg_last_addr_i;
            rddata_reg(25) <= 'X';
            rddata_reg(26) <= 'X';
            rddata_reg(27) <= 'X';
            rddata_reg(28) <= 'X';
            rddata_reg(29) <= 'X';
            rddata_reg(30) <= 'X';
            rddata_reg(31) <= 'X';
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "00101" => 
            if (wb_we_i = '1') then
            end if;
            rddata_reg(15 downto 0) <= reg_adc_data_1_i;
            rddata_reg(31 downto 16) <= reg_adc_data_2_i;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "00110" => 
            if (wb_we_i = '1') then
            end if;
            rddata_reg(15 downto 0) <= reg_dac_data_1_i;
            rddata_reg(31 downto 16) <= reg_dac_data_2_i;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "00111" => 
            if (wb_we_i = '1') then
              reg_prg_bus_data_1_int <= wrdata_reg(15 downto 0);
            end if;
            rddata_reg(15 downto 0) <= reg_prg_bus_data_1_int;
            rddata_reg(16) <= 'X';
            rddata_reg(17) <= 'X';
            rddata_reg(18) <= 'X';
            rddata_reg(19) <= 'X';
            rddata_reg(20) <= 'X';
            rddata_reg(21) <= 'X';
            rddata_reg(22) <= 'X';
            rddata_reg(23) <= 'X';
            rddata_reg(24) <= 'X';
            rddata_reg(25) <= 'X';
            rddata_reg(26) <= 'X';
            rddata_reg(27) <= 'X';
            rddata_reg(28) <= 'X';
            rddata_reg(29) <= 'X';
            rddata_reg(30) <= 'X';
            rddata_reg(31) <= 'X';
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "01000" => 
            if (wb_we_i = '1') then
              reg_prg_bus_data_2_int <= wrdata_reg(15 downto 0);
            end if;
            rddata_reg(15 downto 0) <= reg_prg_bus_data_2_int;
            rddata_reg(16) <= 'X';
            rddata_reg(17) <= 'X';
            rddata_reg(18) <= 'X';
            rddata_reg(19) <= 'X';
            rddata_reg(20) <= 'X';
            rddata_reg(21) <= 'X';
            rddata_reg(22) <= 'X';
            rddata_reg(23) <= 'X';
            rddata_reg(24) <= 'X';
            rddata_reg(25) <= 'X';
            rddata_reg(26) <= 'X';
            rddata_reg(27) <= 'X';
            rddata_reg(28) <= 'X';
            rddata_reg(29) <= 'X';
            rddata_reg(30) <= 'X';
            rddata_reg(31) <= 'X';
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "01001" => 
            if (wb_we_i = '1') then
              reg_thr_chan1_int <= wrdata_reg(15 downto 0);
            end if;
            rddata_reg(15 downto 0) <= reg_thr_chan1_int;
            rddata_reg(16) <= 'X';
            rddata_reg(17) <= 'X';
            rddata_reg(18) <= 'X';
            rddata_reg(19) <= 'X';
            rddata_reg(20) <= 'X';
            rddata_reg(21) <= 'X';
            rddata_reg(22) <= 'X';
            rddata_reg(23) <= 'X';
            rddata_reg(24) <= 'X';
            rddata_reg(25) <= 'X';
            rddata_reg(26) <= 'X';
            rddata_reg(27) <= 'X';
            rddata_reg(28) <= 'X';
            rddata_reg(29) <= 'X';
            rddata_reg(30) <= 'X';
            rddata_reg(31) <= 'X';
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "01010" => 
            if (wb_we_i = '1') then
              reg_thr_chan2_int <= wrdata_reg(15 downto 0);
            end if;
            rddata_reg(15 downto 0) <= reg_thr_chan2_int;
            rddata_reg(16) <= 'X';
            rddata_reg(17) <= 'X';
            rddata_reg(18) <= 'X';
            rddata_reg(19) <= 'X';
            rddata_reg(20) <= 'X';
            rddata_reg(21) <= 'X';
            rddata_reg(22) <= 'X';
            rddata_reg(23) <= 'X';
            rddata_reg(24) <= 'X';
            rddata_reg(25) <= 'X';
            rddata_reg(26) <= 'X';
            rddata_reg(27) <= 'X';
            rddata_reg(28) <= 'X';
            rddata_reg(29) <= 'X';
            rddata_reg(30) <= 'X';
            rddata_reg(31) <= 'X';
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "01011" => 
            if (wb_we_i = '1') then
              reg_cfg_enwin1_int <= wrdata_reg(0);
              reg_cfg_entrigwin1_int <= wrdata_reg(1);
              reg_cfg_filter_on_int <= wrdata_reg(2);
              reg_cfg_dacsel1_int <= wrdata_reg(6 downto 4);
              reg_cfg_delay_clk_int <= wrdata_reg(11 downto 8);
              reg_cfg_diff_cfg_1_int <= wrdata_reg(12);
              reg_cfg_enwin2_int <= wrdata_reg(16);
              reg_cfg_entrigwin2_int <= wrdata_reg(17);
              reg_cfg_dacsel2_int <= wrdata_reg(22 downto 20);
              reg_cfg_diff_cfg_2_int <= wrdata_reg(28);
            end if;
            rddata_reg(0) <= reg_cfg_enwin1_int;
            rddata_reg(1) <= reg_cfg_entrigwin1_int;
            rddata_reg(2) <= reg_cfg_filter_on_int;
            rddata_reg(6 downto 4) <= reg_cfg_dacsel1_int;
            rddata_reg(11 downto 8) <= reg_cfg_delay_clk_int;
            rddata_reg(12) <= reg_cfg_diff_cfg_1_int;
            rddata_reg(16) <= reg_cfg_enwin2_int;
            rddata_reg(17) <= reg_cfg_entrigwin2_int;
            rddata_reg(22 downto 20) <= reg_cfg_dacsel2_int;
            rddata_reg(28) <= reg_cfg_diff_cfg_2_int;
            rddata_reg(3) <= 'X';
            rddata_reg(7) <= 'X';
            rddata_reg(13) <= 'X';
            rddata_reg(14) <= 'X';
            rddata_reg(15) <= 'X';
            rddata_reg(18) <= 'X';
            rddata_reg(19) <= 'X';
            rddata_reg(23) <= 'X';
            rddata_reg(24) <= 'X';
            rddata_reg(25) <= 'X';
            rddata_reg(26) <= 'X';
            rddata_reg(27) <= 'X';
            rddata_reg(29) <= 'X';
            rddata_reg(30) <= 'X';
            rddata_reg(31) <= 'X';
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "01100" => 
            if (wb_we_i = '1') then
              reg_windelay1_int <= wrdata_reg(31 downto 0);
            end if;
            rddata_reg(31 downto 0) <= reg_windelay1_int;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "01101" => 
            if (wb_we_i = '1') then
              reg_windelay2_int <= wrdata_reg(31 downto 0);
            end if;
            rddata_reg(31 downto 0) <= reg_windelay2_int;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "01110" => 
            if (wb_we_i = '1') then
              reg_winwidth1_int <= wrdata_reg(31 downto 0);
            end if;
            rddata_reg(31 downto 0) <= reg_winwidth1_int;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "01111" => 
            if (wb_we_i = '1') then
              reg_winwidth2_int <= wrdata_reg(31 downto 0);
            end if;
            rddata_reg(31 downto 0) <= reg_winwidth2_int;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "10000" => 
            if (wb_we_i = '1') then
              reg_trigwidth1_int <= wrdata_reg(31 downto 0);
            end if;
            rddata_reg(31 downto 0) <= reg_trigwidth1_int;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "10001" => 
            if (wb_we_i = '1') then
              reg_trigwidth2_int <= wrdata_reg(31 downto 0);
            end if;
            rddata_reg(31 downto 0) <= reg_trigwidth2_int;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "10010" => 
            if (wb_we_i = '1') then
              reg_trigdelforced1_int <= wrdata_reg(31 downto 0);
            end if;
            rddata_reg(31 downto 0) <= reg_trigdelforced1_int;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "10011" => 
            if (wb_we_i = '1') then
              reg_trigdelforced2_int <= wrdata_reg(31 downto 0);
            end if;
            rddata_reg(31 downto 0) <= reg_trigdelforced2_int;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "10100" => 
            if (wb_we_i = '1') then
              reg_trigdelsim1_int <= wrdata_reg(31 downto 0);
            end if;
            rddata_reg(31 downto 0) <= reg_trigdelsim1_int;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "10101" => 
            if (wb_we_i = '1') then
              reg_trigdelsim2_int <= wrdata_reg(31 downto 0);
            end if;
            rddata_reg(31 downto 0) <= reg_trigdelsim2_int;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "10110" => 
            if (wb_we_i = '1') then
            end if;
            rddata_reg(31 downto 0) <= reg_c0_time_f_i;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "10111" => 
            if (wb_we_i = '1') then
            end if;
            rddata_reg(31 downto 0) <= reg_c0_time_d_i;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when "11000" => 
            if (wb_we_i = '1') then
            end if;
            rddata_reg(31 downto 0) <= reg_ctype_i;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when others =>
-- prevent the slave from hanging the bus on invalid address
            ack_in_progress <= '1';
            ack_sreg(0) <= '1';
          end case;
        end if;
      end if;
    end if;
  end process;
  
  
-- Drive the data output bus
  wb_dat_o <= rddata_reg;
-- No Trigger high
-- FMR or NMR high
  reg_st_fnmr1_o <= reg_st_fnmr1_int;
-- Simulation or effective bit
-- Enable test DDR
  reg_st_ddr_tst_o <= reg_st_ddr_tst_int;
-- Enable HW reset
  reg_st_en_hw_reset_o <= reg_st_en_hw_reset_int;
-- Reset HW input
-- Reset SW input
  reg_st_reset_sw_o <= reg_st_reset_sw_int;
-- DDR Samples enable
  reg_st_samples_wr_en_o <= reg_st_samples_wr_en_int;
-- Initialization done bit
-- No Trigger low
-- FMR or NMR low
  reg_st_fnmr2_o <= reg_st_fnmr2_int;
-- Direction
  reg_dio_dir_o <= reg_dio_dir_int;
-- Configuration
  reg_dio_cfg_o <= reg_dio_cfg_int;
-- Input
-- decim_factor
  reg_decim_factor_o <= reg_decim_factor_int;
-- DDR range
  reg_ddr_range_o <= reg_ddr_range_int;
-- Last sample
-- ADC1
-- ADC2
-- DATA1
-- DATA2
-- DATA1_bus_prog
  reg_prg_bus_data_1_o <= reg_prg_bus_data_1_int;
-- DATA2_bus_prog
  reg_prg_bus_data_2_o <= reg_prg_bus_data_2_int;
-- THR1
  reg_thr_chan1_o <= reg_thr_chan1_int;
-- THR2
  reg_thr_chan2_o <= reg_thr_chan2_int;
-- ENWIN1
  reg_cfg_enwin1_o <= reg_cfg_enwin1_int;
-- ENTRIGWIN1
  reg_cfg_entrigwin1_o <= reg_cfg_entrigwin1_int;
-- Filter On/Off
  reg_cfg_filter_on_o <= reg_cfg_filter_on_int;
-- DACSEL1
  reg_cfg_dacsel1_o <= reg_cfg_dacsel1_int;
-- Delay Clk
  reg_cfg_delay_clk_o <= reg_cfg_delay_clk_int;
-- Differential or signal
  reg_cfg_diff_cfg_1_o <= reg_cfg_diff_cfg_1_int;
-- ENWIN2
  reg_cfg_enwin2_o <= reg_cfg_enwin2_int;
-- ENTRIGWIN2
  reg_cfg_entrigwin2_o <= reg_cfg_entrigwin2_int;
-- DACSEL2
  reg_cfg_dacsel2_o <= reg_cfg_dacsel2_int;
-- Differential or signal
  reg_cfg_diff_cfg_2_o <= reg_cfg_diff_cfg_2_int;
-- WINDELAY1
  reg_windelay1_o <= reg_windelay1_int;
-- WINDELAY2
  reg_windelay2_o <= reg_windelay2_int;
-- WINWIDTH1
  reg_winwidth1_o <= reg_winwidth1_int;
-- WINWIDTH2
  reg_winwidth2_o <= reg_winwidth2_int;
-- TRIGWIDTH1
  reg_trigwidth1_o <= reg_trigwidth1_int;
-- TRIGWIDTH2
  reg_trigwidth2_o <= reg_trigwidth2_int;
-- TRIGDELFORCED1
  reg_trigdelforced1_o <= reg_trigdelforced1_int;
-- TRIGDELFORCED2
  reg_trigdelforced2_o <= reg_trigdelforced2_int;
-- TRIGDELSIM1
  reg_trigdelsim1_o <= reg_trigdelsim1_int;
-- TRIGDELSIM2
  reg_trigdelsim2_o <= reg_trigdelsim2_int;
-- C0+time channel F
-- C0+time channel D
-- CTYPE
  rwaddr_reg <= wb_adr_i;
  wb_stall_o <= (not ack_sreg(0)) and (wb_stb_i and wb_cyc_i);
-- ACK signal generation. Just pass the LSB of ACK counter.
  wb_ack_o <= ack_sreg(0);
end syn;
