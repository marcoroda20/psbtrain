--------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer:		Daniel Oberson
--
-- Create Date:   14:22:24 13/12/2013
-- Design Name:   
-- Module Name:   /psbtrain/FPGA/FMR/spec_peak_detector/testbench/TB_peakdetector.vhd
-- Project Name:  spec_peak_detector
-- Target Device:  
-- Tool versions:  
-- Description:   Testbench for PeakDetector
-- Additional Comments: To run the testbench with Modelsim type the command:
--                      do run_peakdetector.do
--
--------------------------------------------------------------------------------
LIBRARY ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
--use ieee.std_logic_unsigned.all; 

use std.textio.all;

library work;
use work.peak_detector_pkg.all;
use work.adc_dac_pkg.all;
use work.processing_pkg.all;
use work.dma_pkg.all;
use work.utils_pkg.all;

  ENTITY TB_peakdetector IS
  END TB_peakdetector;

  ARCHITECTURE behavior OF TB_peakdetector IS 

	-- Component Declaration
	signal clk_125_sti : std_logic;
	signal clk_20_sti  : std_logic;
	signal clk_50_sti  : std_logic;
	signal clk_250_sti : std_logic;
	signal clk_500_sti : std_logic;

	signal reset_s   : std_logic;
	signal reset_n_s : std_logic;
          
	signal init_done_sti : std_logic := '1';

	signal ADC2_D   : std_logic;
	signal ADC2_DCO : std_logic;
	signal ADC1_D   : std_logic;
	signal ADC1_DCO : std_logic;
	signal PPD1     : std_logic;
	signal PPD2     : std_logic;
		
	signal Eff_Sim_bit_i : std_logic;

		--Register
	signal ADC_data_1_o     : std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
	signal ADC_data_rdy_1_o : std_logic;
	signal ADC_data_2_o     : std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
	signal ADC_data_rdy_2_o : std_logic;
	signal DAC_data_1_o     : std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
	signal DAC_data_2_o     : std_logic_vector(NB_BIT_DAC_DATA_BUS-1 downto 0);
		
	signal DAC_prog_1_i 		: std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
	signal DAC_prog_2_i 		: std_logic_vector(NB_BIT_ADC_DATA_BUS-1 downto 0);
	signal Threshold1   		: std_logic_vector (NB_BIT_THRESHOLD-1 downto 0); 
	signal Threshold2   		: std_logic_vector (NB_BIT_THRESHOLD-1 downto 0); 
	signal TrigWidth_1_i		: std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal TrigWidth_2_i		: std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal WinDelay_1_i		: std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal WinDelay_2_i		: std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal WinWidth_1_i		: std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal WinWidth_2_i		: std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal EnWin_1_i			: std_logic;
	signal EnWin_2_i			: std_logic;
	signal EnTrigSim_1_i		: std_logic;
	signal EnTrigSim_2_i		: std_logic;
	signal TrigSimDelay_1_i	: std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal TrigSimDelay_2_i	: std_logic_vector(NB_BIT_COUNTER-1 downto 0);
		
	signal TrigForcedDelay_1_i	: std_logic_vector(NB_BIT_COUNTER-1 downto 0);
	signal TrigForcedDelay_2_i	: std_logic_vector(NB_BIT_COUNTER-1 downto 0);

	signal ADC_CNV	 	: std_logic;
	signal ADC1_CLK 	: std_logic;
	signal ADC2_CLK 	: std_logic;
	signal DAC1_sel 	: std_logic_vector (2 downto 0); 
	signal DAC2_sel 	: std_logic_vector (2 downto 0); 
	signal C0_LED 		: std_logic;

		-- DDR wishbone interface
	signal wb_ddr_clk_i   : std_logic;
	signal wb_ddr_adr_o   : std_logic_vector(31 downto 0);
	signal wb_ddr_dat_o   : std_logic_vector(63 downto 0);
	signal wb_ddr_sel_o   : std_logic_vector(7 downto 0);
	signal wb_ddr_stb_o   : std_logic;
	signal wb_ddr_we_o    : std_logic;
	signal wb_ddr_cyc_o   : std_logic;
	signal wb_ddr_ack_i   : std_logic;
	signal wb_ddr_stall_i : std_logic;

	signal ddr_test_en_i  : std_logic;

	signal mem_range_o  : std_logic;
	signal acqu_end_o   : std_logic;
			
	signal no_trig_1_o  : std_logic;
	signal no_trig_2_o  : std_logic;
		
	signal TRIG1_OK    : std_logic;
	signal TRIG2_OK    : std_logic;
	signal TRIG1_MISS  : std_logic;
	signal TRIG2_MISS  : std_logic;
	signal WIN1        : std_logic;
	signal WIN2        : std_logic;

	signal C0_PULSE 	: std_logic;
	signal DAC1_CLK 	: std_logic;
	signal DAC2_CLK 	: std_logic;
	signal DAC_DATA 	: std_logic_vector (15 downto 0); 

  -- Clock period definitions
   constant clk_125_period : time := 8 ns;
   constant clk_20_period : time := 50 ns;
   constant clk_50_period : time := 20 ns;
   constant clk_250_period : time := 4 ns;
   constant clk_500_period : time := 2 ns;

  BEGIN

  -- Component Instantiation
	cmp_peak_detect : peak_detect 
	generic map(g_simulation => 0)
	port map(
		clk_sys_i   => clk_125_sti,
		clk_20MHz_i	=> clk_20_sti, 
		clk_50MHz_i	=> clk_50_sti, 
		clk_250MHz_i=> clk_250_sti, 
		clk_500MHz_i=> clk_500_sti, 
		reset_i		=> reset_s, 
		init_done_i => init_done_sti, 

		reset_hw_i    => '0',
		reset_sw_o    => open,
		en_hw_reset_o => open,
		
		Eff_Sim_bit_i => Eff_Sim_bit_i,

		-- CSR wishbone interface
		wb_csr_adr_i   => x"00000000",
		wb_csr_dat_i   => x"00000000",
		wb_csr_dat_o   => open,
		wb_csr_cyc_i   => '0',
		wb_csr_sel_i   => "0000",
		wb_csr_stb_i   => '0',
		wb_csr_we_i    => '0',
		wb_csr_ack_o   => open,
		wb_csr_stall_o => open,

		ADC2_D 		=> ADC2_D, 
		ADC2_DCO 	=> ADC2_DCO, 
		ADC1_D 		=> ADC1_D, 
		ADC1_DCO 	=> ADC1_DCO, 
		PPD1 			=> PPD1, 
		PPD2 			=> PPD2, 

		ADC_CNV	 	=> ADC_CNV, 
		ADC1_CLK 	=> ADC1_CLK, 
		ADC2_CLK 	=> ADC2_CLK, 
		--DAC1_sel 	=> DAC1_sel, 
		--DAC2_sel 	=> DAC2_sel, 
		C0_LED 		=> C0_LED, 

		C0_PULSE 	=> C0_PULSE,
		DAC1_CLK 	=> DAC1_CLK, 
		DAC2_CLK 	=> DAC2_CLK, 
		DAC_DATA 	=> DAC_DATA,
		
		delay1_o => open,
		delay2_o => open,

		C0_time_1_o => open,
		C0_time_2_o => open,

		reg_dio_dir_o  => open,
		reg_dio_cfg_o  => open,
		reg_dio_input_i=> "0000000000",

		-- DDR wishbone interface
      wb_ddr_clk_i   => clk_125_sti,
      wb_ddr_adr_o   => wb_ddr_adr_o,
      wb_ddr_dat_o   => wb_ddr_dat_o,
      wb_ddr_sel_o   => wb_ddr_sel_o,
      wb_ddr_stb_o   => wb_ddr_stb_o,
      wb_ddr_we_o    => wb_ddr_we_o,
      wb_ddr_cyc_o   => wb_ddr_cyc_o,
      wb_ddr_ack_i   => '1',--wb_ddr_ack_i,
      wb_ddr_stall_i => '0',--wb_ddr_stall_i,
				
		--To test
		ram_addr_o => open,
		ram_data_o => open,
		ram_wr_o   => open,

		mem_range_o	=> open,
		acqu_end_o  => open,

		--Trig LED
		TRIG1_OK 	=> TRIG1_OK, 
		TRIG2_OK 	=> TRIG2_OK, 
		TRIG1_MISS 	=> TRIG1_MISS, 
		TRIG2_MISS 	=> TRIG2_MISS, 
		WIN1 			=> WIN1,
		WIN2 			=> WIN2,

		DCM_lock_i	=> reset_n_s		

   );
	
   -- Clock process definitions
   clk_125_process :process
   begin
		clk_125_sti <= '0';
		wait for clk_125_period/2;
		clk_125_sti <= '1';
		wait for clk_125_period/2;
   end process;

   clk_20_process :process
   begin
		clk_20_sti <= '0';
		wait for clk_20_period/2;
		clk_20_sti <= '1';
		wait for clk_20_period/2;
   end process;

   clk_50_process :process
   begin
		clk_50_sti <= '0';
		wait for clk_50_period/2;
		clk_50_sti <= '1';
		wait for clk_50_period/2;
   end process;

   clk_250_process :process
   begin
		clk_250_sti <= '0';
		wait for clk_250_period/2;
		clk_250_sti <= '1';
		wait for clk_250_period/2;
   end process;

   clk_500_process :process
   begin
		clk_500_sti <= '0';
		wait for clk_500_period/2;
		clk_500_sti <= '1';
		wait for clk_500_period/2;
   end process;

	reset_n_s <= reset_s;

	--Simulation of ADC
	adc : process
		--To read values in file
		file adc_1			: text open READ_MODE is "adc1.sti"; -- fichier des coefficients pour les stimuli
		file adc_2			: text open READ_MODE is "adc2.sti"; -- fichier des coefficients pour les stimuli
		variable adc_1_line	: line;	
		variable adc_2_line	: line;	
		variable adc_1_v 		: integer;
		variable adc_2_v 		: integer;
		variable adc_1_slv : std_logic_vector(15 downto 0);
		variable adc_2_slv : std_logic_vector(15 downto 0);
		variable length_v		: integer;
		variable count_v 	   : integer := 0;
	begin
		while (ADC1_CLK='0') loop	
			wait for clk_250_period/2;	
		end loop;							
		while (ADC1_CLK='1') loop	
			wait for clk_250_period/2;	
		end loop;							
		ADC1_D <= '1';
		ADC2_D <= '1';
		wait for clk_250_period/2;	
		readline(adc_1,adc_1_line);--third line : 'Signal'
		read(adc_1_line,adc_1_v);
		readline(adc_2,adc_2_line);--third line : 'Signal'
		read(adc_2_line,adc_2_v);
		count_v:=0;
		while (count_v<16) loop
			adc_1_slv := std_logic_vector(to_signed(adc_1_v,adc_1_slv'length));
			adc_2_slv := std_logic_vector(to_signed(adc_2_v,adc_2_slv'length));
			ADC1_D <= adc_1_slv(15-count_v);
			ADC2_D <= adc_2_slv(15-count_v);
			ADC1_DCO <= '1';
			ADC2_DCO <= '1';
			wait for clk_250_period/2;
			ADC1_DCO <= '0';
			ADC2_DCO <= '0';
			wait for clk_250_period/2;
			count_v:=count_v+1;
		end loop;
		ADC1_D <= '0';
		ADC2_D <= '0';
	end process;

	--  Test Bench Statements
	tb : PROCESS
	BEGIN

		--Init 
		Eff_Sim_bit_i <= '1';--effective
		ddr_test_en_i <= '1';--counter as data written in DDR
		
		DAC1_sel          <= "101";
		DAC2_sel          <= "100";
		
		DAC_prog_1_i 		<= (others=>'0');
		DAC_prog_2_i 		<= (others=>'0');
		Threshold1   		<= (others=>'0');
		Threshold2   		<= (others=>'0');
		TrigWidth_1_i		<= x"00000032";    --1us
		TrigWidth_2_i		<= x"00000032";    --1us
		WinDelay_1_i		<= x"003d0900";    --80ms
		WinDelay_2_i		<= x"003d0900";    --80ms
		WinWidth_1_i		<= x"000f4240";    --20ms
		WinWidth_2_i		<= x"000f4240";    --20ms
		EnWin_1_i			<= '1';
		EnWin_2_i			<= '1';
		EnTrigSim_1_i		<= '1';
		EnTrigSim_2_i		<= '1';
		TrigSimDelay_1_i	<= x"0044aa20";    --90ms
		TrigSimDelay_2_i	<= x"0044aa20";    --90ms
	
		TrigForcedDelay_1_i <= x"0000c350";  --1ms
		TrigForcedDelay_2_i <= x"0000c350";  --1ms

		reset_s <= '1';
		wait for 100 ns; -- wait until global set/reset completes
		reset_s <= '0';

		-- Add user defined stimulus here

		wait; -- will wait forever
	END PROCESS tb;
  --  End Test Bench 

  END;
