//
// Peakdetector new BTrain 
//
//
// Objectives:
// - test the Peakdetector for new BTrain
//

`timescale 1ps/1ps 

module main;

   // Parameters
   
   // Reference clock period.
   reg clk_20m = 0;
   reg clk_80m = 0;
   reg [1:0] button = 'b11; 
   reg pulse_in = 0;
   reg reset_hw = 1;
   reg L_RST_N = 'b1;

   // Generate the 20 MHz VCXO clock
   always #(50ns / 2) clk_20m <= ~clk_20m; 
   // Generate the 80 MHz
   always #(12.5ns / 2) clk_80m <= ~clk_80m; 
  
   wire [4:0] dio_out_b;
      
   spec_top_fmc_adc_16b_10Ms
     #(
       .g_simulation (1)
    ) peak_detect_sim (
           
      // Local oscillator
      .clk_20MHz_i(clk_20m),

      .ADC_CLK_P_i(clk_80m),
      .ADC_CLK_N_i(~clk_80m),
      .reset_n_hw_i(reset_hw),
		
      // Carrier font panel LEDs
      //led_red_sfp_o   : out std_logic;
      //led_green_sfp_o : out std_logic;

      .L_RST_N(L_RST_N),
      .aux_buttons_i( button )
      //     .dio_p_i( {3'b0, pulse_in, 1'b0} ),
      //     .dio_n_i( {3'b1, ~pulse_in, 1'b1} )
  
  );  

  initial begin
      // wait until both SPECs see the Ethernet link. Otherwise the packet we're going 
      // to send might end up in void...
      //wait(link_up_a == 1'b1 && link_up_b == 1'b1);

     forever begin // send a pulse every 30 us;
        //#30us;
        #500us;
        button = 'b11; 
        #1us;
        button = 'b01; 
        #1us;
        button = 'b11;
        #10us;
        button = 'b01; 
        #1us;
        button = 'b11;
        #400us;
     end end
   
endmodule // main

