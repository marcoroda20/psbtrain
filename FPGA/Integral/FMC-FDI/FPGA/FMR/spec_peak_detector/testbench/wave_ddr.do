onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /main/peak_detect_sim/g_simulation
add wave -noupdate /main/peak_detect_sim/but_pci_rst_n_s
add wave -noupdate /main/peak_detect_sim/sys_clk_pll_locked_s
add wave -noupdate /main/peak_detect_sim/start_timer_reset_s
add wave -noupdate /main/peak_detect_sim/reset_n_hw_i
add wave -noupdate -format Literal /main/peak_detect_sim/reset_n_s
add wave -noupdate -format Literal /main/peak_detect_sim/reset_s
add wave -noupdate /main/peak_detect_sim/aux_buttons_i
add wave -noupdate /main/peak_detect_sim/init_done_s
add wave -noupdate -divider {RAM test}
add wave -noupdate -radix decimal /main/peak_detect_sim/ram_addr_s
add wave -noupdate /main/peak_detect_sim/ram_wr_s
add wave -noupdate /main/peak_detect_sim/ram_addr_s
add wave -noupdate -radix decimal /main/peak_detect_sim/ram_data_rd_s
add wave -noupdate -radix decimal /main/peak_detect_sim/ram_data_wr_s
add wave -noupdate /main/peak_detect_sim/ram_addr_count_s
add wave -noupdate /main/peak_detect_sim/ram_addr_mux_s
add wave -noupdate -divider DDR
add wave -noupdate -divider {WB DDR}
add wave -noupdate /main/peak_detect_sim/wb_ddr_cyc
add wave -noupdate -radix hexadecimal /main/peak_detect_sim/wb_ddr_sel
add wave -noupdate /main/peak_detect_sim/wb_ddr_stall
add wave -noupdate /main/peak_detect_sim/wb_ddr_stb
add wave -noupdate /main/peak_detect_sim/wb_ddr_ack
add wave -noupdate -radix decimal /main/peak_detect_sim/wb_ddr_dat_o
add wave -noupdate /main/peak_detect_sim/wb_ddr_we
add wave -noupdate -radix decimal /main/peak_detect_sim/wb_ddr_adr
add wave -noupdate /main/peak_detect_sim/sys_clk_125_buf_s
add wave -noupdate -divider {WB DMA}
add wave -noupdate /main/peak_detect_sim/wb_dma_ack
add wave -noupdate -radix hexadecimal /main/peak_detect_sim/wb_dma_adr
add wave -noupdate /main/peak_detect_sim/wb_dma_cyc
add wave -noupdate -radix hexadecimal /main/peak_detect_sim/wb_dma_dat_i
add wave -noupdate -radix hexadecimal /main/peak_detect_sim/wb_dma_dat_o
add wave -noupdate -radix hexadecimal /main/peak_detect_sim/wb_dma_sel
add wave -noupdate /main/peak_detect_sim/wb_dma_stall
add wave -noupdate /main/peak_detect_sim/wb_dma_stb
add wave -noupdate /main/peak_detect_sim/wb_dma_we
add wave -noupdate -divider Acqu_Ctrl
add wave -noupdate /main/peak_detect_sim/irq_sources_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/acqu_end_o
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/acqu_end_out_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/acqu_end_p
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/acqu_end_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/c_DDR_MEMORY_RANGE_SIM
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/clk_i
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/ddr_range_i
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/ddr_test_en_i
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/g_sim
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/incr_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/last_addr_o
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/mem_range_o
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/one_shot_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/ram_addr_cnt_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/ram_addr_o
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/ram_data_o
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/ram_wr_d
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/ram_wr_o
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/ram_wr_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/reset_i
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/rst_n_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/samples_wr_en_i
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/switch_mem_range_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/switch_test_val_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/sync_data_i
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/sync_data_valid_i
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/sync_data_valid_s
add wave -noupdate -divider {WORD FIFO Write}
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/sync_data_i
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/sync_data_valid_i
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/sync_data_valid_s
add wave -noupdate -height 16 /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/state_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/fifo_data_cnt_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_wr
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_wr_q
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/data_in_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/data_rdy_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/data_word_s
add wave -noupdate -divider FIFO
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_din
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_dout
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_dreq
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_empty
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_full
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_rd
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_rd_r
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_valid
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_valid_1
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_wr
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_fifo_wr_en
add wave -noupdate -divider DDR_crtl
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_ack_end_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_ack_i
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_ack_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_adr_o
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_clk_i
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_cyc_o
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_cyc_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_dat_o
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_sel_o
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_stall_i
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_stall_t
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_stb_o
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_stb_s
add wave -noupdate /main/peak_detect_sim/cmp_peakdetector/cmp_acqu_ddr_ctrl/wb_ddr_we_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {419913750 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 589
configure wave -valuecolwidth 69
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits fs
update
WaveRestoreZoom {0 ps} {802765642 ps}
