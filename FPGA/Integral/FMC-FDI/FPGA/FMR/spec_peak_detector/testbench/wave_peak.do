onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_peakdetector/clk_20_sti
add wave -noupdate /tb_peakdetector/clk_50_sti
add wave -noupdate /tb_peakdetector/clk_250_sti
add wave -noupdate /tb_peakdetector/clk_500_sti
add wave -noupdate /tb_peakdetector/reset_s
add wave -noupdate /tb_peakdetector/reset_n_s
add wave -noupdate -divider {DDR synchro}
add wave -noupdate -divider ADC
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/DIFF_data_2_o
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/DIFF_data_1_o
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/DAC_load_2_o
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/DAC_load_1_o
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/DAC_data_bus_2_o
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/DAC_data_bus_1_o
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/ADC_data_rdy_2_o
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/ADC_data_rdy_1_o
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/ADC_data_2_o
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/ADC_data_1_o
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/ADC_CNV_o
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/ADC_CLK_o
add wave -noupdate /tb_peakdetector/ADC1_DCO
add wave -noupdate /tb_peakdetector/ADC2_DCO
add wave -noupdate -divider {ADC inside}
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/cmp_ADC_cmd/serial2paral_2_s
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/cmp_ADC_cmd/serial2paral_1_s
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/ADC_D_2_i
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ADC_DIFF_DAC/ADC_D_1_i
add wave -noupdate -divider {END ACQU}
add wave -noupdate -divider {WB ddr}
add wave -noupdate /tb_peakdetector/clk_125_sti
add wave -noupdate -divider Pulse
add wave -noupdate /tb_peakdetector/WIN2
add wave -noupdate /tb_peakdetector/WIN1
add wave -noupdate /tb_peakdetector/TRIG2_OK
add wave -noupdate /tb_peakdetector/TRIG2_MISS
add wave -noupdate /tb_peakdetector/TRIG1_OK
add wave -noupdate /tb_peakdetector/TRIG1_MISS
add wave -noupdate /tb_peakdetector/PPD2
add wave -noupdate /tb_peakdetector/PPD1
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ppd_win_1/state_s
add wave -noupdate /tb_peakdetector/cmp_peak_detect/cmp_ppd_win_2/state_s
add wave -noupdate -divider Decimator
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {399239 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 535
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ns} {2996304 ns}
