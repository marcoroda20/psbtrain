clc,close all;
% Generate Coefficients and References files
Bk          = 0;
Bsmooth     = 2147483647;%90;
corr_fact   = 0.2;
Vk          = 0.00;
Vo          = 0;
deltat      = 8;
low_marker  = 0;
high_marker = 0;

f=50;
t=(0:0.001:0.200);
signal = round(100*sin(2*pi*f*t));
signal = 2^14*ones(size(t));

pathname = '../spec_btrain/testbench';

[Bkp1]=writefile(Bk,Bsmooth,corr_fact,Vk,Vo,deltat,low_marker,high_marker,signal,pathname);
