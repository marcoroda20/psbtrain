% Generate FD_sum example

pathname=['C:\PSBTrain\FPGA\Integral\spec_btrain\testbench'];

N_VALUE=5;

ADC = [3.75*ones(1,N_VALUE) -3.75*ones(1,N_VALUE+1) 0.01*ones(1,100*N_VALUE)];

[signal18bits]=writefileADC(ADC,pathname);
% [signal18bits]=writefileADC(ADC);