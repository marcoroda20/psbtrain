clc,close all;
% Generate Coefficients and References files
corr_fact   = 1;
Vo          = 5;

f=50;
t=(0:0.001:0.200);
signal = round(100*sin(2*pi*f*t));

pathname = '../spec_btrain/testbench';

[Bdot]=writefile_dot(corr_fact,Vo,signal,pathname);
