clc,close all;
% Generate Coefficients and References files
% Max, Nb_val_mean, Nb_val_signal
max_val = 200;
nb_val_mean = 5;

f=100;
t=(0:0.001:0.100);
signal = round(300*sin(2*pi*f*t));
pathname = '../spec_btrain/testbench';

writefile_overr(max_val,nb_val_mean,signal,pathname);
