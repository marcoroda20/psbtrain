%
% Generation of file for simulation of Marco's integral
%


function [Bk]=writefile3_8(Bk,Vk,Bsmooth,signal,pathname)

if nargin<5
    pathname='';
end

if (strcmp(pathname,'')==1)
    fid_sti = fopen('coeff3_8.dat', 'w');
    fid_ref = fopen('ref3_8.dat', 'w');
else
    fid_sti = fopen([pathname '\coeff3_8.dat'], 'w');
    fid_ref = fopen([pathname '\ref3_8.dat'], 'w');
end

%% Write coefficient file
% coefficient
[count] = fprintf(fid_sti,'Bk,Vk \n');
[count] = fprintf(fid_sti,'%ld %ld \n', [Bk Vk ]);
% signal
[count] = fprintf(fid_sti,'Signal : \n');
[count] = fprintf(fid_sti,'%ld \n',length(signal));
for n=1:length(signal)
    [count] = fprintf(fid_sti,'%ld\n',signal(n));
end

%% Write reference file : 
% Bk=Bkm3+3/8*(Vkm3+3*Vkm2+3*Vkm1+Vk);
% after initialization
Bkm3=0;Bkm2=0;Bkm1=0;
Vkm3=0;Vkm2=0;Vkm1=0;
Bk=Bkm3+floor(3/8*(Vkm3+3*Vkm2+3*Vkm1+Vk));
[count] = fprintf(fid_ref,'After initialization : \n');
[count] = fprintf(fid_ref,'%ld \n',Bk);
% integral of the signal
[count] = fprintf(fid_ref,'Signal : \n');
Bk=zeros(floor(length(signal)/4),1);
Bk_3_8=[];

for n=1:length(signal)
    Bk(n)=Bkm3+floor(3/8*(Vkm3+3*Vkm2+3*Vkm1+signal(n)));
    if (mod(n,4)==1)
        Bk_3_8=[Bk_3_8 Bk(n)];
    end
    [count] = fprintf(fid_ref,'%ld \n',Bk(n));
    Bkm3=Bkm2;
    Bkm2=Bkm1;
    Bkm1=Bk(n);
    Vkm3=Vkm2;
    Vkm2=Vkm1;
    Vkm1=signal(n);
end
t=(1:1:length(signal));
t_3_8=(1:4:length(signal));
int_signal = cumsum(signal);

figure;
plot(t,signal,'b')
hold on
plot(t,int_signal,'g')
error=int_signal(1:4:end)-Bk_3_8;
plot(t_3_8,error,'r')
plot(t_3_8,Bk_3_8,'k')
grid on;

fclose(fid_sti);
fclose(fid_ref);
