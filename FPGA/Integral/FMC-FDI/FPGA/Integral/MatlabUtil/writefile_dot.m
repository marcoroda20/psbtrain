%
% Generation of file for simulation of Bdot
%
% TestBench : 


function [Bdot]=writefile_dot(corr_fact,Vo,signal,pathname);

if nargin<4
    pathname=[''];
end

if (strcmp(pathname,'')==1)
    fid_sti = fopen('coeff_dot.dat', 'w');
    fid_ref = fopen('ref_dot.dat', 'w');
else
    fid_sti = fopen([pathname '\coeff_dot.dat'], 'w');
    fid_ref = fopen([pathname '\ref_dot.dat'], 'w');
end

%% Write coefficient file
% coefficient
[count] = fprintf(fid_sti,'corr_fact Vo \n');
fact=floor(corr_fact*2^31);
if fact>=2^31
    corr_reg = fact-2^32;
else
    corr_reg = fact;
end
[count] = fprintf(fid_sti,'%ld %ld \n', [corr_reg Vo]);
% signal %ld
[count] = fprintf(fid_sti,'Signal : \n');
[count] = fprintf(fid_sti,'%d \n',length(signal));
for n=1:length(signal)
    [count] = fprintf(fid_sti,'%d \n',signal(n));
end

%% Write reference file : 
% Bdot
[count] = fprintf(fid_ref,'Bdot : \n');
Bdot=zeros(length(signal),1);
for n=1:length(signal)
    Bdot(n)=floor((fact/2^31)*(signal(n)-Vo));
    [count] = fprintf(fid_ref,'%d \n',Bdot(n));
end

figure;
plot(signal,'b');
hold on;
plot(Bdot,'r');
grid on;
fclose(fid_sti);
fclose(fid_ref);
