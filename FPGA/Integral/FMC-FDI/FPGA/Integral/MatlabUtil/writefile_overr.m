%
% Generation of file for simulation of OverRange
%
% TestBench : 


function writefile_overr(max_val,nb_val_mean,signal,pathname);

if nargin<4
    pathname=[''];
end

if (strcmp(pathname,'')==1)
    fid_sti = fopen('ADC_val.dat', 'w');
else
    fid_sti = fopen([pathname '\ADC_val.dat'], 'w');
end

%% Write file
[count] = fprintf(fid_sti,'Max Nb_val_mean Nb_val_signal \n');
[count] = fprintf(fid_sti,'%ld %ld %ld \n', [floor(max_val) floor(nb_val_mean) length(signal)]);
% signal 
[count] = fprintf(fid_sti,'Signal : \n');
for n=1:length(signal)
    if signal(n)>=2^17-1
        [count] = fprintf(fid_sti,'%d \n',2^17-1);
    elseif signal(n)<=-2^17 
        [count] = fprintf(fid_sti,'%d \n',-2^17);
    else
        [count] = fprintf(fid_sti,'%d \n',signal(n));
    end
end

figure;
plot(signal,'b');
hold on;grid on;
plot(max_val*ones(1,length(signal)),'r');
plot(-1*max_val*ones(1,length(signal)),'r');


fclose(fid_sti);
