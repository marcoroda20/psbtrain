modules = {
  "git" : [ "git://ohwr.org/hdl-core-lib/general-cores.git::proposed_master",
            "git://ohwr.org/hdl-core-lib/ddr3-sp6-core.git::spec_bank3_64b_32b",
            "git://ohwr.org/hdl-core-lib/gn4124-core.git::master"

 ]}

files = [	
   #Packages
   "fmc_adc2M18b2ch_pkg.vhd",
   "calib/calib_pkg.vhd",
   "integral/integral_pkg.vhd",
   "adc_dac/adc_dac_pkg.vhd",
   "streamers16bit/streamers_pkg.vhd",
   "wr/wr_btrain_pkg.vhd",
   "lcd/lcd_pkg.vhd",
   "dma/dma_pkg.vhd",
   "utils/utils_pkg.vhd",
   "sdb/sdb_meta_pkg.vhd",

   "spec_top_fmc_adc2M18b2ch.vhd",
  	
   "irq_controller.vhd",

   "fmc_adc2M18b2ch.vhd",
   "testreg.vhd",
   "timestamp_adder.vhd",
 	
   #Calib
   "calib/calib.vhd",
   "calib/calib_cmd_dac.vhd",
   "calib/calibration.vhd",
   "calib/ctrl_calib.vhd",
   "calib/gain_offset_calc.vhd",
   "calib/offset_calc.vhd",
   "calib/switch_ctrl.vhd",
   "calib/mean_measurement.vhd",
   "calib/fine_offset_calculation.vhd",
   #Integral
   "integral/BCalcul3_8.vhd",
   "integral/BCalculRect.vhd",
   "integral/BTrain_integral.vhd",
   "integral/B_FD_Sum.vhd",
   "integral/mux_integral.vhd",
   "integral/Bdot.vhd",
   "integral/B_err_mult.vhd",
   #ADC (AD7986) and DAC (AD5791)
   "adc_dac/AD5791.vhd",
   "adc_dac/AD7986.vhd",
   #"adc_dac/AD5292_SPI.vhd",
   "adc_dac/AD5292.vhd",
   "adc_dac/acquisition_core.vhd",
   "adc_dac/ADOverRange.vhd",
   "adc_dac/BDOT_DAC.vhd",
   "adc_dac/switch_dac.vhd",
   "adc_dac/Acquisition_AD7986_corr.vhd",
   "adc_dac/correction.vhd",
   "adc_dac/mux_dac_val.vhd",
   
#White Rabbit
   #"streamers/ht-corelib-spartan6.v",
   #"streamers/ht-corelib-spartan6.vhd",
   #"streamers/dropping_buffer.vhd",
   #"streamers/gc_escape_detector.vhd",
   #"streamers/gc_escape_inserter.vhd",
   #"streamers/rx_streamer.vhd",
   #"streamers/tx_streamer.vhd",
   #"streamers/streamers_pkg.vhd",
   #"streamers/xrx_streamer.vhd",
   #"streamers/xtx_streamer.vhd",

   #"streamers16bit/dropping_buffer.vhd",
   #"streamers16bit/gc_escape_detector.vhd",
   #"streamers16bit/gc_escape_inserter.vhd",
   #"streamers16bit/xrx_streamer.vhd",
   #"streamers16bit/xtx_streamer.vhd",

   #WR Btrain
   #"wr/txCtrl.vhd",
   #"wr/rxCtrl.vhd",
   #"wr/SynchroGen.vhd",
   #"wr/WRBTrain.vhd",
   #LCD interface

   "lcd/LCD4x40_Interface.vhd",
   #Wishbone register
   "wbgen/fmc_adc2M18b2ch_reg.vhd",
   "wbgen/carrier_csr.vhd",
   "wbgen/irq_controller_regs.vhd",
   
   #DMA
   "dma/acqu_ddr_ctrl.vhd",
   "dma/decimator.vhd",
   #Utils
   "utils/syncData2clocks.vhd",
   "utils/delayLine.vhd",
   "utils/BCD_counter.vhd",
   "utils/bin2bcd.vhd",
   "utils/clock_divider.vhd",
   "utils/RisingDetect.vhd",
   "utils/monostable.vhd",
   "utils/maintain.vhd",
   "utils/wb_addr_decoder.vhd",
   "utils/CLK_DIV_EVEN.vhd",
   "utils/Mult36x36_signed.vhd",
   "utils/Mult32x32_signed.vhd",
   "utils/Mult32x32_2chan.vhd",
   "utils/divider.vhd",
   "utils/spec_reset_gen.vhd",
   "utils/mean_pow2.vhd",
   "utils/losses_counter.vhd",
   "utils/C0_time.vhd"
]

fetchto="../ip_cores" 


