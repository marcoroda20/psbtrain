----------------------------------------------------------------------------------
-- Company:        CERN TE/MSC/MM
-- Engineer:       Daniel Oberson
-- 
-- Create Date:    13:52:15 07/17/2013 
-- Design Name: 
-- Module Name:    acquisition_core - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.adc_dac_pkg.all;
use work.integral_pkg.all;

entity acquisition_core is
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		--ADC control bus
		clk_adc_i	: in std_logic;
		clk_cnv_i	: in std_logic;

		--to test
		adc_sync_i  : in std_logic_vector(2 downto 0);

		adc_sdo_i	: in std_logic;
		adc_sdi_o	: out std_logic;
		adc_sck_o	: out std_logic;
		adc_bsck_i  : in std_logic;
		adc_cnv_o	: out std_logic;

		--Correction coefficient
		gain_i	: in std_logic_vector(31 downto 0);
		offset_i : in std_logic_vector(31 downto 0);
		
		--Register to choose the type of integral calculation
		reg_val_cfg_sel_integral_i : in std_logic_vector(1 downto 0);
		reg_int_c0_used_i          : in std_logic;
		reg_int_range_sel_i        : in std_logic_vector(3 downto 0);
		soft_fixed_i               : in std_logic;
		soft_val_i                 : in std_logic_vector(31 downto 0);
		
		--Register
		Bsmooth_i	: in std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0);
		corr_fact_i	: in std_logic_vector(NB_BITS_A_GAIN_VALUE-1 downto 0);
		Vo_i			: in std_logic_vector(NB_BITS_OFFSET_VALUE-1 downto 0);

		--Input information signals
		low_marker_i  : in std_logic;
		high_marker_i : in std_logic;

		--Marker values
		low_marker_val_i  : in std_logic_vector(31 downto 0);
		high_marker_val_i : in std_logic_vector(31 downto 0);

		C0_i		      : in std_logic;
		calib_status_i : in std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);
		
		--Output
		B_error_low_o  : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		B_error_high_o : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);

		adc_data_rdy_o	: out std_logic;
		adc_data_o		: out std_logic_vector(17 downto 0);

		adc_corr_rdy_o	: out std_logic;
		adc_corr_o		: out std_logic_vector(17 downto 0);
		
		B_data_rdy_o	: out std_logic;
		BKp1_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		
		Bdot_data_rdy_o: out std_logic;
		Bdot_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0)
	);
end acquisition_core;

architecture Behavioral of acquisition_core is
	
	--Signal
	signal adc_data_rdy_s: std_logic;
	signal adc_data_s		: std_logic_vector(17 downto 0);

	signal adc_corr_rdy_s     : std_logic;
	signal adc_corr_rdy_buf_s : std_logic;
	signal adc_corr_s         : std_logic_vector(17 downto 0);
	signal adc_corr_buf_s     : std_logic_vector(17 downto 0);
	
	signal BKp1_s			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal B_data_rdy_s	: std_logic;

	signal Bdot_data_rdy_s	: std_logic;
	signal Bdot_s				: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	
begin

	--ADC and correction 
	cmp_Acquisition_AD7986_corr : Acquisition_AD7986_corr
		port map(
			clk_i			=> clk_i,
			clk_adc_i	=> clk_adc_i,
			reset_i		=> reset_i,
			clk_cnv_i	=> clk_cnv_i,

			-- to compensate resynchronization of CNV by ADC clock outside FPGA (see schematic)
			adc_sync_i  => adc_sync_i,

			adc_sdo_i	=> adc_sdo_i,
			adc_sdi_o	=> adc_sdi_o,
			adc_sck_o	=> adc_sck_o,
			adc_bsck_i  => adc_bsck_i,
			adc_cnv_o	=> adc_cnv_o,
			
			gain_i	=> gain_i,
			offset_i => offset_i,

			adc_data_rdy_o	=> adc_data_rdy_s,
			adc_data_o		=> adc_data_s,

			adc_corr_rdy_o	=> adc_corr_rdy_buf_s,
			adc_corr_o		=> adc_corr_buf_s
		);

	--Latch after correction to avoid to see the calibration
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				adc_corr_rdy_s	<= '0';
				adc_corr_s		<= (others=>'0');
			else
				if ((calib_status_i=IDLE_st)or
				     (calib_status_i=NEXT_ZCYCLE_START_st)or
					  (calib_status_i=DPOT_st)) then
					adc_corr_rdy_s	<= adc_corr_rdy_buf_s;
					adc_corr_s		<= adc_corr_buf_s;	
				end if;
			end if;
		end if;
	end process;

	adc_data_rdy_o	<= adc_data_rdy_s;
	adc_data_o		<= adc_data_s;

	adc_corr_rdy_o	<= adc_corr_rdy_buf_s;
	adc_corr_o		<= adc_corr_buf_s;	


	--BTrain integral calculation
	cmp_BTrain_integral : BTrain_integral
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
		
			adc_data_i		=> adc_corr_s,
			adc_data_rdy_i	=> adc_corr_rdy_s,

			init_Bk_i		=> '0',

			--Register	
			reg_val_cfg_sel_integral_i => reg_val_cfg_sel_integral_i,
			int_c0_used_i          => reg_int_c0_used_i,
			int_range_sel_i        => reg_int_range_sel_i,
			soft_fixed_i           => soft_fixed_i,
			soft_val_i             => soft_val_i,
			
			Bsmooth_i		=> Bsmooth_i,
			corr_fact_i		=> corr_fact_i,
			Vo_i				=> Vo_i,

			--Input information signals
			low_marker_i  => low_marker_i,
			high_marker_i => high_marker_i,

			--Marker values
			low_marker_val_i  => low_marker_val_i,
			high_marker_val_i => high_marker_val_i,

			C0_i				=> C0_i,
			
			--Output
			B_error_low_o  => B_error_low_o,
			B_error_high_o => B_error_high_o,
			BKp1_o			=> BKp1_s,
			B_data_rdy_o	=> B_data_rdy_s
		);
	
	B_data_rdy_o <= B_data_rdy_s;
	BKp1_o       <= BKp1_s;
		
	--Bdot calculation
	cmp_Bdot : Bdot
		--Calcul :
		-- Bdot = corr_fact_i*(Vk_i-Vo_i);
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			--Control input
			start_i	=> adc_corr_rdy_s,
			Vk_i		=> adc_corr_s,

			--Data
			corr_fact_i	=> corr_fact_i,
			Vo_i			=> Vo_i,
						
			--Output
			data_rdy_o	=> Bdot_data_rdy_s,
			Bdot_o		=> Bdot_s
		);

	Bdot_data_rdy_o <= Bdot_data_rdy_s;
	Bdot_o          <= Bdot_s;
	
end Behavioral;

