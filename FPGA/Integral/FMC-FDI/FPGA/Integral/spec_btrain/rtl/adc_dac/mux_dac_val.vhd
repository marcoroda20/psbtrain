----------------------------------------------------------------------------------
-- Company:        CERN TE/MSC/MM
-- Engineer:       Daniel Oberson 
-- 
-- Create Date:    17:44:08 07/16/2013 
-- Design Name: 
-- Module Name:    mux_dac_val - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.fmc_adc2M18b2ch_pkg.all;

entity mux_dac_val is
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		--Bk
		BKp1_F_i       : in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		B_data_rdy_F_i : in std_logic;

		BKp1_D_i       : in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		B_data_rdy_D_i : in std_logic;

		--B
		B_i            : in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		B_data_rdy_i   : in std_logic;

		--Bdot
		Bdot_F_i          : in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		Bdot_data_rdy_F_i : in std_logic;

		Bdot_D_i				: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		Bdot_data_rdy_D_i	: in std_logic;

		--Bdot
		Bdot_i            : in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		Bdot_data_rdy_i   : in std_logic;
		
		--ADC value
		adc_data_F_i		: in std_logic_vector(17 downto 0);
		adc_data_rdy_F_i	: in std_logic;

		adc_data_D_i		: in std_logic_vector(17 downto 0);
		adc_data_rdy_D_i	: in std_logic;

		adc_data_F_corr_i		: in std_logic_vector(17 downto 0);
		adc_data_rdy_F_corr_i: in std_logic;

		adc_data_D_corr_i		: in std_logic_vector(17 downto 0);
		adc_data_rdy_D_corr_i: in std_logic;

		--White Rabbit value
		wr_B_val_i     : in std_logic_vector(31 downto 0);
		wr_Bdot_val_i  : in std_logic_vector(31 downto 0);
		wr_I_val_i     : in std_logic_vector(31 downto 0);
		wr_drdy_i      : in std_logic;

		--register
		reg_val_sel_i			: in std_logic_vector(3 downto 0);
		reg_val_sel_range_i	: in std_logic_vector(3 downto 0);
		
		--Output
		val_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		data_rdy_o	: out std_logic
	);
end mux_dac_val;

architecture Behavioral of mux_dac_val is

	--Signal
	signal val_s		: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal data_rdy_s	: std_logic;
	
begin

	--Mux for DAC value
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				val_s			<= (others=>'0');
				data_rdy_s	<= '0';
			else
				case reg_val_sel_i is
					-- F
					when "0000" =>
						val_s			<= BKp1_F_i;
						data_rdy_s	<= B_data_rdy_F_i;
					when "0001" =>
						val_s			<= Bdot_F_i(17 downto 0) & "00000000000000";
						data_rdy_s	<= Bdot_data_rdy_F_i;
					when "0010" =>
						val_s			<= adc_data_F_i & "00000000000000";
						data_rdy_s	<= adc_data_rdy_F_i;
					when "0011" =>
						val_s			<= adc_data_F_corr_i & "00000000000000";
						data_rdy_s	<= adc_data_rdy_F_corr_i;
					-- D
					when "0100" =>
						val_s			<= BKp1_D_i;
						data_rdy_s	<= B_data_rdy_D_i;
					when "0101" =>
						val_s			<= Bdot_D_i(17 downto 0) & "00000000000000";
						data_rdy_s	<= Bdot_data_rdy_D_i;
					when "0110" =>
						val_s			<= adc_data_D_i & "00000000000000";
						data_rdy_s	<= adc_data_rdy_D_i;
					when "0111" =>
						val_s			<= adc_data_D_corr_i & "00000000000000";
						data_rdy_s	<= adc_data_rdy_D_corr_i;
					--Combined value
					when "1001" =>
						val_s			<= B_i(31 downto 0);
						data_rdy_s	<= B_data_rdy_i;
					when "1010" =>
						val_s			<= Bdot_i;
						data_rdy_s	<= Bdot_data_rdy_i;
					--White Rabbit value
					when "1011" =>
						val_s			<= wr_B_val_i;
						data_rdy_s	<= wr_drdy_i;
					when "1100" =>
						val_s			<= wr_Bdot_val_i;
						data_rdy_s	<= wr_drdy_i;
					when "1101" =>
						val_s			<= wr_I_val_i;
						data_rdy_s	<= wr_drdy_i;

					when others =>
						val_s			<= BKp1_F_i;
						data_rdy_s	<= B_data_rdy_F_i;
				end case;
			end if;
		end if;
	end process;
	
	--Mux to choose the range on the DAC
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				val_o			<= (others=>'0');
				data_rdy_o	<= '0';
			else
				case reg_val_sel_range_i is
					when x"0" =>
						val_o	<= val_s(NB_BITS_B_VALUE-1 downto 0);
					when x"1" =>
						val_o	<= val_s(NB_BITS_B_VALUE-2 downto 0)  & "0";
					when x"2" =>
						val_o	<= val_s(NB_BITS_B_VALUE-3 downto 0)  & "00";
					when x"3" =>
						val_o	<= val_s(NB_BITS_B_VALUE-4 downto 0)  & "000";
					when x"4" =>
						val_o	<= val_s(NB_BITS_B_VALUE-5 downto 0)  & "0000";
					when x"5" =>
						val_o	<= val_s(NB_BITS_B_VALUE-6 downto 0)  & "00000";
					when x"6" =>
						val_o	<= val_s(NB_BITS_B_VALUE-7 downto 0)  & "000000";
					when x"7" =>
						val_o	<= val_s(NB_BITS_B_VALUE-8 downto 0)  & "0000000";
					when x"8" =>
						val_o	<= val_s(NB_BITS_B_VALUE-9 downto 0)  & "00000000";
					when x"9" =>
						val_o	<= val_s(NB_BITS_B_VALUE-10 downto 0) & "000000000";
					when x"a" =>
						val_o	<= val_s(NB_BITS_B_VALUE-11 downto 0) & "0000000000";
					when x"b" =>
						val_o	<= val_s(NB_BITS_B_VALUE-12 downto 0) & "00000000000";
					when x"c" =>
						val_o	<= val_s(NB_BITS_B_VALUE-13 downto 0) & "000000000000";
					when x"d" =>
						val_o	<= val_s(NB_BITS_B_VALUE-14 downto 0) & "0000000000000";
					when x"e" =>
						val_o	<= val_s(NB_BITS_B_VALUE-15 downto 0) & "00000000000000";
					when x"f" =>
						val_o	<= val_s(NB_BITS_B_VALUE-16 downto 0) & "000000000000000";
					when others =>
						val_o	<= val_s;
				end case;
				data_rdy_o	<= data_rdy_s;
			end if;
		end if;
	end process;

end Behavioral;

