----------------------------------------------------------------------------------
-- Company:        CERN TE/MSC /MM
-- Engineer:       Daniel Oberson
-- 
-- Create Date:    10:18:42 07/17/2013 
-- Design Name: 
-- Module Name:    switch_dac - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity switch_dac is
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		--Value for DAC selectionned by Mux_dac_val
		val_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		data_rdy_i	: in std_logic;

		--Value for DAC for electronic calibration
		calib_DAC_wr_i 	: in std_logic_vector(23 downto 0);	
		calib_DAC_rd_o 	: out std_logic_vector(23 downto 0);
		calib_end_dac_rd_o: out std_logic;
		calib_end_dac_wr_o: out std_logic;
		
		--Value for DAC from the register controled by the PC
		reg_dac_bus_i	: in std_logic_vector(23 downto 0);
		
		--Control
		reg_val_sel_fpga_bus_i	: in std_logic;
		calib_status_i				: in std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);

		--Control bus for DAC
		end_dac_rd_i	: in std_logic;
		end_dac_wr_i	: in std_logic;
		DAC_rd_i 		: in std_logic_vector(23 downto 0);		-- From DAC registers read interface
		DAC_wr_o 		: out std_logic_vector(23 downto 0)	-- To DAC registers write interface
	);
end switch_dac;

architecture Behavioral of switch_dac is
		
	--Signal
	signal clk_250KHz_s : std_logic;
	signal tick_250KHz_s: std_logic;
	
	signal DAC_wr_data_s	: std_logic_vector(NB_BITS_DAC_VALUE-1 downto 0);
	
	signal calib_run_s	: std_logic;

	signal DAC_init_count_s : integer;
	signal DAC_init_s : std_logic;
	
begin
	
	calib_run_s <= '0' when (calib_status_i=IDLE_st or calib_status_i=NEXT_ZCYCLE_START_st or calib_status_i=DPOT_st) else '1';

	--Latch DAC data 24bits register
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then 
				DAC_wr_data_s <= (others=>'0');
				DAC_wr_o <= x"000000";	
				calib_DAC_rd_o <= (others=>'0');
				calib_end_dac_rd_o <= '0';
				calib_end_dac_wr_o <= '0';
			else
				if (reg_val_sel_fpga_bus_i='1' and calib_run_s='0') then
					if (tick_250KHz_s='1') then
						DAC_wr_o <= reg_dac_bus_i;
					end if;
					calib_DAC_rd_o     <= (others=>'0');
					calib_end_dac_rd_o <= '0';
					calib_end_dac_wr_o <= '0';
				elsif (calib_run_s='1') then
					if (tick_250KHz_s='1') then
						DAC_wr_o           <= calib_DAC_wr_i;
					end if;
					calib_DAC_rd_o     <= DAC_rd_i;
					calib_end_dac_rd_o <= end_dac_rd_i;
					calib_end_dac_wr_o <= end_dac_wr_i;
				else
					if (data_rdy_i='1' ) then
						DAC_wr_data_s <= WR_DAC_REGISTER & val_i(NB_BITS_B_VALUE-1 downto NB_BITS_B_VALUE-NB_BITS_DAC_VALUE+4);
					end if;
					if (tick_250KHz_s='1') then
						DAC_wr_o <= DAC_wr_data_s;
					end if;
					calib_DAC_rd_o     <= (others=>'0');
					calib_end_dac_rd_o <= '0';
					calib_end_dac_wr_o <= '0';
				end if;
			end if;
		end if;
	end process;

	--Generate the 250KHz for the DAC coomunication
	cmp_250KHz : clock_divider 
	generic map(
		g_f_in => 100000000, -- Input frequency in [Hz]
		g_f_out => 250000    -- Output frequency in [Hz]
	)
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,

		clk_o	=> clk_250KHz_s
	);

	cmp_tick_250KHs : RisingDetect 
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> clk_250KHz_s,
		s_o	=> tick_250KHz_s
	);

end Behavioral;

