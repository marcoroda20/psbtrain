----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    17:33:36 05/27/2013 
-- Design Name: 
-- Module Name:    calib - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.calib_pkg.all;
use work.utils_pkg.all;

entity calib is
	generic(g_simulation : integer := 0);
	port(
		clk_i 	: in std_logic;
		reset_i	: in std_logic;

		force_cal_o       : out std_logic;
		force_cal_i       : in std_logic;
		force_cal_load_o  : out std_logic;

		time_calib_reg_i	: in std_logic_vector(31 downto 0);	--1 sec resolution
		t_next_calib_o		: out std_logic_vector(31 downto 0);--1 sec resolution 
--		C0_i					: in std_logic;
		zero_cycle_i		: in std_logic;
		reg_calib_delay_i	: in std_logic_vector(31 downto 0);
		type_fine_offset_i: in std_logic;
					
		ADC_pos_ref_i		: in std_logic_vector(19 downto 0);
		nb_of_meas_reg_i	: in std_logic_vector(31 downto 0);

		open1_o	: out std_logic;
		gain1_o	: out std_logic;
		off1_o	: out std_logic;
		
		open2_o	: out std_logic;
		gain2_o	: out std_logic;
		off2_o	: out std_logic;

		DAC_wr_o 	: out std_logic_vector(23 downto 0);	-- To DAC registers write interface
		DAC_rd_i 	: in std_logic_vector(23 downto 0);		-- From DAC registers read interface
		end_dac_rd_i	: in std_logic;
		end_dac_wr_i	: in std_logic;
			
		adc_data_rdy_F_i	: in std_logic;
		adc_data_F_i		: in std_logic_vector(17 downto 0);
		adc_data_rdy_D_i	: in std_logic;
		adc_data_D_i		: in std_logic_vector(17 downto 0);

		val_pos_F_o		: out std_logic_vector(31 downto 0);
		val_neg_F_o		: out std_logic_vector(31 downto 0);
		val_pos_D_o		: out std_logic_vector(31 downto 0);
		val_neg_D_o		: out std_logic_vector(31 downto 0);

		gain_F_o				: out std_logic_vector(31 downto 0);
		offset_calc_F_o	: out std_logic_vector(31 downto 0);
		offset_meas_F_o	: out std_logic_vector(31 downto 0);
		gain_D_o				: out std_logic_vector(31 downto 0);
		offset_calc_D_o	: out std_logic_vector(31 downto 0);
		offset_meas_D_o	: out std_logic_vector(31 downto 0);
		
		dpot_done_i   : in std_logic;
		dpot_reset_o  : out std_logic;
		dpot_wr_o     : out std_logic;
		fine_offset_o : out std_logic_vector(9 downto 0);

		B_drdy_i	: in std_logic;
		B_i		: in std_logic_vector(31 downto 0);
		corr_fact_i : in std_logic_vector(31 downto 0);
		k_i         : in std_logic_vector(31 downto 0);
		step_dpot_i : in std_logic_vector(31 downto 0);
		
		error_status_o	: out std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
		calib_status_o	: out std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0)
	);
end calib;

architecture Behavioral of calib is

	--Signal
	signal start_switch_s	: std_logic;
	signal end_switch_s		: std_logic;
	signal switch_cmd_s		: calib_cmd;

	signal start_calib_s		: std_logic;
	
	signal start_dac_s			: std_logic;
	signal DAC_val_register_s	: std_logic_vector(19 downto 0);
	signal DAC_cmd_s				: DAC_cmd;
	signal end_dac_s				: std_logic;

	signal start_meas_s		: std_logic;
	signal end_meas_s			: std_logic;
	signal end_meas_F_s		: std_logic;
	signal end_meas_D_s		: std_logic;
	signal val_measured_F_s	: std_logic_vector(31 downto 0);
	signal val_measured_D_s	: std_logic_vector(31 downto 0);

	signal start_gain_offset_calc_s	: std_logic;
 	signal end_gain_offset_calc_s		: std_logic;
 	signal end_gain_offset_calc_F_s	: std_logic;
 	signal end_gain_offset_calc_D_s	: std_logic;
	
	signal gain_calc_F_s					: std_logic_vector(31 downto 0);
	signal offset_calc_F_s				: std_logic_vector(31 downto 0);
	signal gain_calc_D_s					: std_logic_vector(31 downto 0);
	signal offset_calc_D_s				: std_logic_vector(31 downto 0);

	signal gain_F_out_s			: std_logic_vector(31 downto 0);
	signal offset_calc_F_out_s	: std_logic_vector(31 downto 0);
	signal offset_meas_F_out_s	: std_logic_vector(31 downto 0);
	signal gain_D_out_s			: std_logic_vector(31 downto 0);
	signal offset_calc_D_out_s	: std_logic_vector(31 downto 0);
	signal offset_meas_D_out_s	: std_logic_vector(31 downto 0);

	signal val_pos_F_s	: std_logic_vector(31 downto 0);
	signal val_neg_F_s	: std_logic_vector(31 downto 0);
	signal val_pos_D_s	: std_logic_vector(31 downto 0);
	signal val_neg_D_s	: std_logic_vector(31 downto 0);

	signal start_fine_offset_s : std_logic;
	signal fine_offset_s       : std_logic_vector(9 downto 0);
	signal end_fine_offset_s   : std_logic;
	
	signal error_dac_s	: std_logic;
	signal error_calc_F_s: std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
	signal error_calc_D_s: std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
	signal calib_status_s: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);
	
begin
		
	--Calibration's cores
	cmp_ctrl_calib : ctrl_calib 
		generic map(g_simulation => g_simulation)
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			time_calib_reg_i	=> time_calib_reg_i,
			t_next_calib_o		=> t_next_calib_o,
			
			force_cal_o       => force_cal_o,
			force_cal_i       => force_cal_i,
			force_cal_load_o  => force_cal_load_o,

--			C0_i					=> C0_i,
			zero_cycle_i		=> zero_cycle_i,
			reg_calib_delay_i	=> reg_calib_delay_i,
			
			calib_status_i	=> calib_status_s,
			calib_status_o	=> calib_status_o,
			start_calib_o	=> start_calib_s
		);

	cmp_calibration : calibration 
		port map(				
			clk_i 	=> clk_i,
			reset_i	=> reset_i,
			
			start_cal_i			=> start_calib_s,
			
			start_switch_o			=> start_switch_s,
			end_switch_i			=> end_switch_s,
			switch_cmd_o			=> switch_cmd_s,
				
			start_dac_o				=> start_dac_s,
			DAC_val_register_o	=> DAC_val_register_s,
			DAC_cmd_o 				=> DAC_cmd_s,
			end_dac_i				=> end_dac_s,
			
			start_gain_offset_calc_o	=> start_gain_offset_calc_s,
			end_gain_offset_calc_i		=> end_gain_offset_calc_s,
			
			gain_calc_F_i			=> gain_calc_F_s,
			offset_calc_F_i		=> offset_calc_F_s,
			gain_calc_D_i			=> gain_calc_D_s,
			offset_calc_D_i		=> offset_calc_D_s,

			gain_F_o				=> gain_F_out_s,
			offset_calc_F_o	=> offset_calc_F_out_s,
			offset_meas_F_o	=> offset_meas_F_out_s,
			gain_D_o				=> gain_D_out_s,
			offset_calc_D_o	=> offset_calc_D_out_s,
			offset_meas_D_o	=> offset_meas_D_out_s,
		
			ADC_pos_ref_i	=> ADC_pos_ref_i,

			start_meas_o			=> start_meas_s,
			end_meas_i				=> end_meas_s,
			val_measured_F_i		=> val_measured_F_s,
			val_measured_D_i		=> val_measured_D_s,

			val_pos_F_o				=> val_pos_F_s,
			val_neg_F_o				=> val_neg_F_s,
			val_pos_D_o				=> val_pos_D_s,
			val_neg_D_o				=> val_neg_D_s,

			start_fine_offset_o => start_fine_offset_s,
			type_fine_offset_i  => type_fine_offset_i,
			fine_offset_i       => fine_offset_s,
			end_fine_offset_i   => end_fine_offset_s,

			dpot_done_i  => dpot_done_i,
			dpot_reset_o => dpot_reset_o,
			dpot_wr_o    => dpot_wr_o,
			fine_offset_o=> fine_offset_o,
			
			error_dac_i		=> error_dac_s,
			error_calc_F_i	=> error_calc_F_s,
			error_calc_D_i	=> error_calc_D_s,
			error_status_o	=> error_status_o,
			calib_status_o	=> calib_status_s
		);
	--Output of measurement for LCD display
	process (clk_i,reset_i) 
	begin
		if (reset_i='1') then
			val_pos_F_o <= (others=>'0');
			val_neg_F_o <= (others=>'0');
			val_pos_D_o <= (others=>'0');
			val_neg_D_o <= (others=>'0');
		
			gain_F_o				<= (others=>'0');
			offset_calc_F_o	<= (others=>'0');
			offset_meas_F_o	<= (others=>'0');
			gain_D_o				<= (others=>'0');
			offset_calc_D_o	<= (others=>'0');
			offset_meas_D_o	<= (others=>'0');
		elsif (clk_i'event and clk_i='1') then
			val_pos_F_o <= val_pos_F_s;
			val_neg_F_o <= val_neg_F_s;
			val_pos_D_o <= val_pos_D_s;
			val_neg_D_o <= val_neg_D_s;
		
			gain_F_o				<= gain_F_out_s;
			offset_calc_F_o	<= offset_calc_F_out_s;
			offset_meas_F_o	<= offset_meas_F_out_s;
			gain_D_o				<= gain_D_out_s;
			offset_calc_D_o	<= offset_calc_D_out_s;
			offset_meas_D_o	<= offset_meas_D_out_s;
		end if;
	end process;
	
	cmp_switch_crtl : switch_ctrl 
		generic map (g_simulation => g_simulation)
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			switch_cmd_i	=> switch_cmd_s,
			start_switch_i	=> start_switch_s,
			
			open1_o		=> open1_o,
			gain1_o		=> gain1_o,
			off1_o		=> off1_o,
			
			open2_o		=> open2_o,
			gain2_o		=> gain2_o,
			off2_o		=> off2_o,
			
			end_switch_o	=> end_switch_s
		);
	
	cmp_dac_cmd : calib_cmd_dac 
		 Port map( 
			clk_i 	=> clk_i,
			reset_i	=> reset_i,

			start_dac_i				=> start_dac_s,
			DAC_cmd_i 				=> DAC_cmd_s,
			DAC_val_register_i	=> DAC_val_register_s,

			DAC_wr_o 		=> DAC_wr_o,
			DAC_rd_i			=> DAC_rd_i,

			end_dac_rd_i	=> end_dac_rd_i,
			end_dac_wr_i	=> end_dac_wr_i,

			end_dac_o		=> end_dac_s,
			error_status_o	=> error_dac_s
		);

	cmp_mean_meas_F : mean_measurement 
		port map(
			clk_i 	=> clk_i,
			reset_i	=> reset_i,
			
			start_meas_i 	=> start_meas_s,
			adc_data_rdy_i	=> adc_data_rdy_F_i,
			adc_data_i		=> adc_data_F_i,
			
			nb_of_meas_reg_i	=> nb_of_meas_reg_i,
						
			end_meas_o		=> end_meas_F_s,
			val_mean_meas_o=> val_measured_F_s
		);

	cmp_mean_meas_D : mean_measurement 
		port map(
			clk_i 	=> clk_i,
			reset_i	=> reset_i,
			
			start_meas_i 	=> start_meas_s,
			adc_data_rdy_i	=> adc_data_rdy_D_i,
			adc_data_i		=> adc_data_D_i,
			
			nb_of_meas_reg_i	=> nb_of_meas_reg_i,
						
			end_meas_o		=> end_meas_D_s,
			val_mean_meas_o=> val_measured_D_s
		);
	end_meas_s <= end_meas_F_s and end_meas_D_s;

	cm_gain_offset_F : gain_offset_calc 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			start_gain_offset_calc_i=> start_gain_offset_calc_s,
			
			ADC_pos_ref_i	=> ADC_pos_ref_i,
			val_pos_i		=> val_pos_F_s,
			val_neg_i		=> val_neg_F_s,

			gain_o	=> gain_calc_F_s,
			offset_o	=> offset_calc_F_s,
			
			error_calc_o				=> error_calc_F_s,
			end_gain_offset_calc_o	=> end_gain_offset_calc_F_s
		);
		
	cm_gain_offset_D : gain_offset_calc 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			start_gain_offset_calc_i=> start_gain_offset_calc_s,
			
			ADC_pos_ref_i	=> ADC_pos_ref_i,
			val_pos_i		=> val_pos_D_s,
			val_neg_i		=> val_neg_D_s,

			gain_o	=> gain_calc_D_s,
			offset_o	=> offset_calc_D_s,
			
			error_calc_o				=> error_calc_D_s,
			end_gain_offset_calc_o	=> end_gain_offset_calc_D_s
		);
	end_gain_offset_calc_s <= end_gain_offset_calc_F_s and end_gain_offset_calc_D_s;

	cmp_fine_offset_calculation : fine_offset_calculation
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			start_i     => start_fine_offset_s,
			
			nb_sample_i => nb_of_meas_reg_i,
			corr_fact_i	=> corr_fact_i,
			k_i         => k_i,
			step_dpot_i => step_dpot_i,
			
			B_drdy_i => B_drdy_i,
			B_i      => B_i,
			
			fine_offset_o => fine_offset_s,
			end_o => end_fine_offset_s
		);

end Behavioral;

