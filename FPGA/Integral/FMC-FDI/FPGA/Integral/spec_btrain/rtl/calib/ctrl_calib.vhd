----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    16:25:07 05/30/2013 
-- Design Name: 
-- Module Name:    ctrl_calib - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.all;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity ctrl_calib is
	generic(g_simulation : integer := 0);
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		time_calib_reg_i	: in std_logic_vector(31 downto 0);	--1 sec resolution
		t_next_calib_o		: out std_logic_vector(31 downto 0);--1 sec resolution 
		
		force_cal_o       : out std_logic;
		force_cal_i       : in std_logic;
		force_cal_load_o  : out std_logic;

		zero_cycle_i		: in std_logic;
		reg_calib_delay_i	: in std_logic_vector(31 downto 0);
		
		calib_status_i	: in std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);
		calib_status_o	: out std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);
		start_calib_o	: out std_logic
	);
end ctrl_calib;

architecture Behavioral of ctrl_calib is

	type ctrl_calib_state is
	(
		COUNT_DOWN,
		WAIT_ZERO_CYCLE,
		WAIT_DELAY,
		START_CALIB,
		WAIT_END_CAL,
		LOAD_BCD_COUNTER,
		WAIT_END_LOAD
	);
	
	--Signal
	signal state_s			: ctrl_calib_state;
	
	signal cnt_delay_s	: integer;
	signal start_delay_s	: std_logic;
	signal end_delays_s	: std_logic;
	
	signal start_calib_s			: std_logic;
	signal start_calib_cnt_s	: std_logic;
	signal calib_run_s			: std_logic;

	signal zero_cycle_s			: std_logic;
	
	signal t_next_calib_s	: std_logic_vector(31 downto 0);--1 sec resolution 
	signal time_calib_reg_s	: std_logic_vector(31 downto 0);	--1 sec resolution
	signal load_bcd_cnt_s	: std_logic;
	signal en_bcd_cnt_s		: std_logic;
	
begin

	--State Machine
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				start_delay_s     <= '0';
				start_calib_s		<= '0';
				load_bcd_cnt_s		<= '0';
				en_bcd_cnt_s		<= '0';
				force_cal_load_o	<= '0';
				calib_status_o		<= IDLE_st;
				state_s <= WAIT_DELAY; --to calibrate the card at startup
			else
				case state_s is
					when COUNT_DOWN =>
						if (time_calib_reg_i/=time_calib_reg_s) then
							state_s <= LOAD_BCD_COUNTER;
						elsif (zero_cycle_s='1' and
							(start_calib_cnt_s='1' or force_cal_i='1')) then
								state_s <= WAIT_DELAY;
						elsif (zero_cycle_s='0' and start_calib_cnt_s='1' and force_cal_i='0') then
							state_s <= WAIT_ZERO_CYCLE;
						else
							state_s <= COUNT_DOWN;
						end if;
						start_delay_s     <= '0';
						start_calib_s     <= '0';
						load_bcd_cnt_s		<= '0';
						en_bcd_cnt_s      <= '1';
						force_cal_load_o	<= '0';
						calib_status_o		<= IDLE_st;
					when WAIT_ZERO_CYCLE =>
						if (time_calib_reg_i/=time_calib_reg_s) then
							state_s <= LOAD_BCD_COUNTER;
						elsif (zero_cycle_s='1' or force_cal_i='1') then
							state_s <= WAIT_DELAY;
						else
							state_s <= WAIT_ZERO_CYCLE;
						end if;
						start_delay_s     <= '0';
						start_calib_s		<= '0';
						load_bcd_cnt_s		<= '0';
						en_bcd_cnt_s		<= '0';
						force_cal_load_o	<= '0';
						calib_status_o		<= NEXT_ZCYCLE_START_st;
					when WAIT_DELAY =>
						if (end_delays_s='1') then
							state_s <= START_CALIB;
						else
							state_s <= WAIT_DELAY;
						end if;
						start_delay_s     <= '1';
						start_calib_s		<= '0';
						load_bcd_cnt_s		<= '0';
						en_bcd_cnt_s		<= '0';
						force_cal_load_o	<= '0';
						calib_status_o		<= NEXT_ZCYCLE_START_st;
					when START_CALIB =>
						if (calib_status_i=IDLE_st) then
							state_s <= START_CALIB;
						else
							state_s <= WAIT_END_CAL;
						end if;
						start_delay_s     <= '0';
						start_calib_s		<= '1';
						load_bcd_cnt_s		<= '0';
						en_bcd_cnt_s		<= '0';
						force_cal_load_o	<= '0';
						calib_status_o		<= calib_status_i;
					when WAIT_END_CAL =>
						if (calib_status_i=IDLE_st) then
							state_s <= LOAD_BCD_COUNTER;
						else
							state_s <= WAIT_END_CAL;
						end if;
						start_delay_s     <= '0';
						start_calib_s 		<= '0';
						load_bcd_cnt_s		<= '0';
						en_bcd_cnt_s		<= '0';
						force_cal_load_o	<= '0';
						calib_status_o		<= calib_status_i;
					when LOAD_BCD_COUNTER =>
						state_s <= WAIT_END_LOAD;
						start_delay_s     <= '0';
						start_calib_s		<= '0';
						load_bcd_cnt_s		<= '1';
						en_bcd_cnt_s		<= '0';
						time_calib_reg_s	<= time_calib_reg_i;
						if (force_cal_i='1') then
							force_cal_load_o	<= '1';
						else
							force_cal_load_o	<= '0';
						end if;
						calib_status_o		<= IDLE_st;
					when WAIT_END_LOAD =>
						if (t_next_calib_s=x"00000000") then
							state_s <= WAIT_END_LOAD;
						else
							state_s <= COUNT_DOWN;
						end if;
						start_delay_s     <= '0';
						start_calib_s		<= '0';
						load_bcd_cnt_s		<= '0';
						en_bcd_cnt_s		<= '1';
						force_cal_load_o	<= '0';
						calib_status_o		<= IDLE_st;
				end case;
			end if;
		end if;
	end process;

	force_cal_o <= '0';
		
	--BCD time counter process
	cmp_BCD_counter : BCD_counter 
		generic map(g_simulation => g_simulation)
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			bcd_time_i		=> time_calib_reg_i,
			load_bcd_cnt_i	=> load_bcd_cnt_s,
			en_bcd_cnt_i	=> en_bcd_cnt_s,
			bcd_countdown_o=> t_next_calib_s,
			zero_flag_o		=> start_calib_cnt_s
		);
	t_next_calib_o <= t_next_calib_s;
	
	--Time delay counter process
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				cnt_delay_s <= 0;
				end_delays_s <= '0';
			else
				if (start_delay_s='1') then
					if (cnt_delay_s>=to_integer(unsigned(reg_calib_delay_i))) then
						cnt_delay_s <= to_integer(unsigned(reg_calib_delay_i));
						end_delays_s <= '1';
					else
						cnt_delay_s <= cnt_delay_s+1;
						end_delays_s <= '0';
					end if;
				else
					cnt_delay_s <= 0;
					end_delays_s <= '0';
				end if;
			end if;	
		end if;	
	end process;

	--Rising edge for zero cycle
	cmp_zero_cycle : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> zero_cycle_i,
		s_o	=> zero_cycle_s
	);

	--Rising edge for start calibration bit
	cmp_start_calib : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> start_calib_s,
		s_o	=> start_calib_o
	);

end Behavioral;

