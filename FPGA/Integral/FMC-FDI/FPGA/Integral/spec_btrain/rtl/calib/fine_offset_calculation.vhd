----------------------------------------------------------------------------------
-- Company:        CERN TE/MSC/MM
-- Engineer:       Daniel Oberson
-- 
-- Create Date:    15:39:54 02/25/2015 
-- Design Name: 
-- Module Name:    fine_offset_calculation - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.utils_pkg.all;

entity fine_offset_calculation is
	port(
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		start_i     : in std_logic;
		
		nb_sample_i : in std_logic_vector(31 downto 0);
		corr_fact_i	: in std_logic_vector(31 downto 0);
		k_i         : in std_logic_vector(31 downto 0);
		step_dpot_i : in std_logic_vector(31 downto 0);
		
		B_drdy_i : in std_logic;
		B_i      : in std_logic_vector(31 downto 0);
		
		fine_offset_o : out std_logic_vector(9 downto 0);
		end_o : out std_logic
	);
end fine_offset_calculation;

architecture Behavioral of fine_offset_calculation is

	--Constant
	constant MIDDLE_RANGE_DPOT : std_logic_vector(11 downto 0):=x"200";

	--Type
	type fine_offset_state is
	(
		IDLE,
		LATCH_B1,
		WAIT_MEAS,
		LATCH_CALC1,CALC1,
		LATCH_CALC2,CALC2,
		LATCH_CALC3,CALC3,
		LATCH_CALC4,CALC4,
		LOAD_OUTPUT
	);

	--Signal
	signal state_s : fine_offset_state;

	signal start_s : std_logic;
	signal nb_sample_s : std_logic_vector(31 downto 0);
	signal count_samples_s : integer;
	signal B1_s : std_logic_vector(31 downto 0);
	signal B2_s : std_logic_vector(31 downto 0);
	signal delta_B_s : std_logic_vector(31 downto 0);
	signal B_drdy_s : std_logic;

	signal start_mult_s : std_logic;
	signal A_mult_36_s : std_logic_vector(35 downto 0);
	signal B_mult_36_s : std_logic_vector(35 downto 0);
	signal result_s : std_logic_vector(71 downto 0);	
	signal end_multiplier_s : std_logic;
	signal end_mult_s : std_logic;

	signal start_div_s : std_logic;
	signal A_div_s : std_logic_vector(63 downto 0);
	signal B_div_s : std_logic_vector(63 downto 0);
	signal div_sign_s : std_logic;
	signal end_divider_s : std_logic;
	signal end_div_s : std_logic;
	
	signal q_s : std_logic_vector(63 downto 0);
	signal f_s : std_logic_vector(63 downto 0);

	signal correction_s : std_logic_vector(12 downto 0);
	
begin

	process (clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				B1_s            <= (others=>'0');
				B2_s            <= (others=>'0');
				delta_B_s       <= (others=>'0');
				nb_sample_s     <= (others=>'0');
				count_samples_s <= 0;
				start_mult_s    <= '0';
				A_mult_36_s     <= (others=>'0');
				B_mult_36_s     <= (others=>'0');
				start_div_s     <= '0';
				A_div_s         <= (others=>'0');
				B_div_s         <= (others=>'0');
				div_sign_s      <= '0';
				fine_offset_o   <= (others=>'0');
				correction_s    <= (others=>'0');
				end_o           <= '0';
				state_s <= IDLE;
			else
				start_mult_s <= '0';
				start_div_s  <= '0';
				end_o <= '0';
				nb_sample_s <= "00" & nb_sample_i(nb_sample_i'length-1 downto 2);
				
				case state_s is
					when IDLE =>
						if (start_s='1') then
							state_s <= LATCH_B1;
						end if;
						end_o <= '1';

					when LATCH_B1 => --latch B1 and reset counter
						if (B_drdy_s='1') then
							state_s <= WAIT_MEAS;
							B1_s <= B_i;
						end if;
						count_samples_s <= 0;

					when WAIT_MEAS =>
						if (count_samples_s>=to_integer(unsigned(nb_sample_s))) then
							state_s <= LATCH_CALC1;
						end if;
						if (B_drdy_s='1') then
							count_samples_s <= count_samples_s + 1;
							B2_s <= B_i;
						end if;
						
					when LATCH_CALC1 => -- B2-B1 and Kf * corr_fact
						state_s <= CALC1;
						A_mult_36_s <= k_i(k_i'length-1) & k_i(k_i'length-1) &
                                 k_i(k_i'length-1) & k_i(k_i'length-1) & k_i;
						B_mult_36_s <= x"0" & corr_fact_i;						
					when CALC1 =>       -- B2-B1 and Kf * corr_fact
						if (end_mult_s='1') then
							state_s <= LATCH_CALC2;
							delta_B_s <= std_logic_vector(signed(B2_s)-signed(B1_s));
						end if;
						start_mult_s <= '1';
						
					when LATCH_CALC2 => -- Kf * corr_fact * N
						state_s <= CALC2;
						A_mult_36_s <= result_s(result_s'length-1) & result_s(result_s'length-1) & 
						               result_s(result_s'length-1) & result_s(result_s'length-1) & 
											result_s(62 downto 31);
						B_mult_36_s <= x"0" & nb_sample_s;
					when CALC2 =>       -- Kf * corr_fact * N
						if (end_mult_s='1') then
							state_s <= LATCH_CALC3;
						end if;
						start_mult_s <= '1';

					when LATCH_CALC3 => -- deltaB / (Kf * corr_fact * N)
						state_s <= CALC3;
						if (delta_B_s(delta_B_s'length-1)='0') then
							A_div_s <= delta_B_s & x"00000000";
							div_sign_s <= '0';
						else
							A_div_s <= std_logic_vector(signed(not(delta_B_s))+1) & x"00000000";
							div_sign_s <= '1';
						end if;
						B_div_s <= '0' & result_s(63 downto 1);
					when CALC3 =>       -- deltaB / (Kf * corr_fact * N)
						if (end_div_s='1') then
							state_s <= LATCH_CALC4;
						end if;
						start_div_s <= '1';
						
					when LATCH_CALC4 => -- (deltaB / (Kf * corr_fact * N))*step_dpot
						state_s <= CALC4;
						A_mult_36_s <= f_s(63 downto 28);
						B_mult_36_s <= x"0" & step_dpot_i;
					when CALC4 =>       -- (deltaB / (Kf * corr_fact * N))*step_dpot
						if (end_mult_s='1') then
							state_s <= LOAD_OUTPUT;
							if (div_sign_s='1') then
								correction_s <= std_logic_vector(signed('0' & MIDDLE_RANGE_DPOT)-signed('0' & result_s(61 downto 52)));
							else
								correction_s <= std_logic_vector(signed('0' & MIDDLE_RANGE_DPOT)+signed('0' & result_s(61 downto 52)));
							end if;
						end if;
						start_mult_s <= '1';
											
					when LOAD_OUTPUT =>
						state_s <= IDLE;
						fine_offset_o <= correction_s(9 downto 0);
						
				end case;
			end if;
		end if;
	end process;

	--Multiplier
	cmp_Mult36x36_signed : Mult36x36_signed
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,

		A_i	=> A_mult_36_s,
		B_i	=> B_mult_36_s,
				
		start_i	=> start_mult_s,

		result_o		=> result_s,
		result_rdy_o=> end_multiplier_s
	);

	--Divider
	cmp_divider : divider
	generic map(NB_BIT => 64)
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		a_i	=> A_div_s,
		b_i	=> B_div_s,

		start_divider_i	=> start_div_s,
		cmd_divider_i		=> '1',-- with the fractional part
		
		q_o	=> q_s,
		r_o	=> open,
		f_o	=> f_s,
		
		divide_by_0_o	=> open,
		end_divider_o	=> end_divider_s
	);
	
	--Rising edge for start
	cmp_start : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> start_i,
		s_o	=> start_s
	);

	--Rising edge for B_drdy
	cmp_B_drdy : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> B_drdy_i,
		s_o	=> B_drdy_s
	);

	--Rising edge for end_mult
	cmp_end_mult : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> end_multiplier_s,
		s_o	=> end_mult_s
	);

	--Rising edge for end_divider
	cmp_end_div : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> end_divider_s,
		s_o	=> end_div_s
	);

end Behavioral;

