----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    17:55:31 05/22/2013 
-- Design Name: 
-- Module Name:    offset_calc - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

library work;
use work.utils_pkg.all;

--1/2*Gcc*(val_pos_i + val_neg_i)
entity offset_calc is
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;

		start_offset_calc_i	: in std_logic;
		
		val_pos_i		: in std_logic_vector(31 downto 0);
		val_neg_i		: in std_logic_vector(31 downto 0);
		gain_i			: in std_logic_vector(31 downto 0);
		
		offset_output_o	: out std_logic_vector(31 downto 0);
		end_offset_calc_o	: out std_logic
	);
end offset_calc;

architecture Behavioral of offset_calc is

	--Type
	type offset_calc_state is
	(
		IDLE,
		LOAD_INPUT,
		OFFSET_CALC_SUM,
		OFFSET_CALC_MULT,
		LOAD_OUTPUT
	);
	
	--Signal
	signal state_s			: offset_calc_state;
	signal next_state_s	: offset_calc_state;

	signal val_pos_s	: std_logic_vector(35 downto 0);
	signal val_neg_s	: std_logic_vector(35 downto 0);
	signal val_s		: std_logic_vector(35 downto 0);

	signal start_mult_s		: std_logic;
	signal end_mult36x36_s	: std_logic;
	signal end_mult_s			: std_logic;

	signal sum_s		: std_logic_vector(35 downto 0);
	signal dsp48a_1_s	: std_logic_vector(35 downto 0);
	signal dsp48a_2_s	: std_logic_vector(35 downto 0);
	signal dsp48a_3_s	: std_logic_vector(35 downto 0);
	signal dsp48a_4_s	: std_logic_vector(35 downto 0);

	signal start_offset_calc_s	: std_logic;
	signal gain_s		: std_logic_vector(35 downto 0);
	signal offset_s	: std_logic_vector(71 downto 0);

begin
	
	--State machine
	process (state_s,start_offset_calc_s,end_mult_s)
	begin
		case state_s is
			when IDLE => 
				if (start_offset_calc_s='1') then
					next_state_s <= LOAD_INPUT;
				else
					next_state_s <= IDLE;
				end if;
			when LOAD_INPUT =>
				next_state_s <= OFFSET_CALC_SUM;	
			when OFFSET_CALC_SUM =>
				next_state_s <= OFFSET_CALC_MULT;	
			when OFFSET_CALC_MULT =>
				if (end_mult_s='1') then
					next_state_s <= LOAD_OUTPUT;	
				else
					next_state_s <= OFFSET_CALC_MULT;	
				end if;	
			when LOAD_OUTPUT =>	
				next_state_s <= IDLE;	
		end case;
	end process;
	
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				state_s <= IDLE;
			else
				state_s <= next_state_s;
			end if;
		end if;
	end process;

	--Signals of state machine
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				offset_output_o <= (others=>'0');
				end_offset_calc_o <= '0';
				val_s <= (others=>'0');
			else
				case state_s is
					when IDLE => 
						end_offset_calc_o	<= '1';
						start_mult_s		<= '0';
					when LOAD_INPUT => --for the signed multiplication
						val_pos_s <= val_pos_i(31) & val_pos_i & "000";
						val_neg_s <= val_neg_i(31) & val_neg_i & "000";
						gain_s <= "0000" & gain_i; 
						end_offset_calc_o	<= '0';
						start_mult_s		<= '0';
					when OFFSET_CALC_SUM => --val_pos_i + val_neg_i
						val_s <= val_pos_s + val_neg_s;
						end_offset_calc_o	<= '0';
						start_mult_s		<= '0';
					when OFFSET_CALC_MULT => --1/2*Gcc*val_s
						end_offset_calc_o	<= '0';
						start_mult_s		<= '1';
					when LOAD_OUTPUT =>
						--1/2*gcc*(Vpos+Vneg)	
						if (offset_s(34)='0') then
							offset_output_o	<= offset_s(66 downto 35);
						else
							offset_output_o	<= offset_s(66 downto 35)+1;
						end if;
						end_offset_calc_o	<= '0';
						start_mult_s		<= '0';
				end case;
			end if;
		end if;
	end process;
	
	--Mult with DSP48A
	cmp_mult : Mult36x36_signed 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			A_i	=> gain_s,
			B_i	=> val_s,
					
			start_i	=> start_mult_s,

			result_o		=> offset_s,
			result_rdy_o=> end_mult36x36_s
		);
	
	--Input rising edge
	cmp_start : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> start_offset_calc_i,
		s_o	=> start_offset_calc_s
	);

	cmp_end_offset : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> end_mult36x36_s,
		s_o	=> end_mult_s
	);

end Behavioral;

