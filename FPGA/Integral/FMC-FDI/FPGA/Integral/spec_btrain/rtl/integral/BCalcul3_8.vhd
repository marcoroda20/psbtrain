----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    17:56:02 01/17/2012 
-- Design Name: 
-- Module Name:    BCalcul3_8 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity BCalcul3_8 is
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;

		--Control input
		start_i	: in std_logic;
		init_Bk_i: in std_logic;
		C0_i		: in std_logic;
		
		int_range_sel_i : in std_logic_vector(3 downto 0);
		int_c0_used_i   : in std_logic;

		--Input information signals
		low_marker_i  : in std_logic;
		high_marker_i : in std_logic;

		--Marker values
		low_marker_val_i  : in std_logic_vector(31 downto 0);
		high_marker_val_i : in std_logic_vector(31 downto 0);

		--Data
		Bk_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		Vk_i			: in std_logic_vector(NB_BITS_ADC_VALUE-1 downto 0);
--		Bsmooth_i	: in std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0);
					
		--Output
		data_rdy_o	: out std_logic;
		BKp1_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0)
	);
end BCalcul3_8;

architecture Behavioral of BCalcul3_8 is

	--Bkm3_s+3/8*(Vkm3_s+3*Vkm2_s+3*Vkm1_s+Vk_s)
	--Type
	type calc_states is (
		IDLE,
		RESET_INTEGRAL,
--		LOW_MARKER,HIGH_MARKER,
		LOAD_INPUT,
		VK_3VK,
		SUM,
		SUM_3,
		BK_SUM_3_8,
--		ADD_SMOOTH,
		LOAD_OUTPUT
	);

	--Signals
	signal state_s : calc_states;

	signal Vk_s	: signed(31 downto 0);
	signal Vkm1_s	: signed(31 downto 0);
	signal Vkm2_s	: signed(31 downto 0);
	signal Vkm3_s	: signed(31 downto 0);
	
	signal Vkm3_3Vkm2_s	: signed(31 downto 0);
	signal Vk_3Vkm1_s		: signed(31 downto 0);

--	signal BKp1_s	: signed(31 downto 0);
	signal Bkm1_s	: signed(63 downto 0);
	signal Bkm2_s	: signed(63 downto 0);
	signal Bkm3_s	: signed(63 downto 0);

	signal sum_s	: signed(63 downto 0);
	signal sum_3_s	: signed(63 downto 0);
	signal bk_sum_3_8_s	: signed(63 downto 0);	

--	signal Bsmooth_s		: signed(31 downto 0);
--	signal smooth_error_s: signed(31 downto 0);

	signal start_s	: std_logic;
	signal C0_s		: std_logic;
	signal B_rst_s : std_logic;
	
	signal low_marker_pulse_s  : std_logic;
	signal low_marker_s        : std_logic;
	signal high_marker_pulse_s : std_logic;
	signal high_marker_s       : std_logic;
	
begin
			
	--State machine for Bkm3_s+3/8*(Vkm3_s+3*Vkm2_s+3*Vkm1_s+Vk_i) calculation
	process (clk_i) 
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then 
				state_s <= IDLE;
				Vk_s <= (others=>'0');
				Vkm1_s <= (others=>'0');
				Vkm2_s <= (others=>'0');
				Vkm3_s <= (others=>'0');
	--			BKp1_s <= (others=>'0');
				Bkm1_s <= (others=>'0');
				Bkm2_s <= (others=>'0');
				Bkm3_s <= (others=>'0');
				sum_s <= (others=>'0');
				sum_3_s <= (others=>'0');
				bk_sum_3_8_s <= (others=>'0');
				Vkm3_3Vkm2_s <= (others=>'0');
				Vk_3Vkm1_s <= (others=>'0');
	--			Bsmooth_s <= (others=>'0');
	--			smooth_error_s <= (others=>'0');
				data_rdy_o <= '0';
				BKp1_o <= (others=>'0');
				B_rst_s <= '0';
				low_marker_s <= '0';
				high_marker_s <= '0';
			else
				if ((C0_s='1')and(int_c0_used_i='1')) then
					B_rst_s <= '1';
				end if;
				if (low_marker_pulse_s='1') then
					low_marker_s <= '1';
				end if;
				if (high_marker_pulse_s='1') then
					high_marker_s <= '1';
				end if;
				case state_s is
					when IDLE =>
						if (B_rst_s='1') then
							state_s <= RESET_INTEGRAL;
--						elsif (low_marker_s='1') then
--							state_s <= LOW_MARKER;
--						elsif (high_marker_s='1') then
--							state_s <= HIGH_MARKER;
						elsif (start_s='1') then
							state_s <= LOAD_INPUT;
						end if;
						data_rdy_o <= '1';
					when RESET_INTEGRAL =>
						state_s <= IDLE;
						B_rst_s <= '0';
						Vk_s    <= (others=>'0');
						Vkm1_s  <= (others=>'0');
						Vkm2_s  <= (others=>'0');
						Vkm3_s  <= (others=>'0');

						Bkm1_s  <= (others=>'0');
						Bkm2_s  <= (others=>'0');
						Bkm3_s  <= (others=>'0');
						
						sum_s   <= (others=>'0');
						sum_3_s <= (others=>'0');
						
						bk_sum_3_8_s <= (others=>'0');
						Vkm3_3Vkm2_s <= (others=>'0');
						Vk_3Vkm1_s   <= (others=>'0');

						data_rdy_o <= '0';
						
					when LOAD_INPUT =>
						state_s <= VK_3VK;
						if (Vk_i(17)='0') then
							Vk_s <= signed(x"000" & "00" & Vk_i);
						else
							Vk_s <= signed(x"fff" & "11" & Vk_i);
						end if;
						data_rdy_o <= '0';
					when VK_3VK =>
						state_s <= SUM;
						Vkm3_3Vkm2_s <= Vkm3_s + Vkm2_s + Vkm2_s + Vkm2_s;
						Vk_3Vkm1_s <= Vkm1_s + Vkm1_s + Vkm1_s + Vk_s;
	--					Bsmooth_s <= signed(Bsmooth_i);
						data_rdy_o <= '0';
					when SUM =>
						state_s <= SUM_3;
						if (Vkm3_3Vkm2_s(31)='0') then
							if (Vk_3Vkm1_s(31)='0') then
								sum_s <= signed(x"00000000" & Vkm3_3Vkm2_s)+signed(x"00000000" & Vk_3Vkm1_s);
							else
								sum_s <= signed(x"00000000" & Vkm3_3Vkm2_s)+signed(x"ffffffff" & Vk_3Vkm1_s);
							end if;
						else
							if (Vk_3Vkm1_s(31)='0') then
								sum_s <= signed(x"ffffffff" & Vkm3_3Vkm2_s)+signed(x"00000000" & Vk_3Vkm1_s);
							else
								sum_s <= signed(x"ffffffff" & Vkm3_3Vkm2_s)+signed(x"ffffffff" & Vk_3Vkm1_s);
							end if;
						end if;
						data_rdy_o <= '0';
					when SUM_3 =>
						state_s <= BK_SUM_3_8;
						if (sum_s(63)='0') then
							sum_3_s <= sum_s + signed("0" & sum_s(61 downto 0) & "0");
						else
							sum_3_s <= sum_s + signed("1" & sum_s(61 downto 0) & "0");
						end if;
						data_rdy_o <= '0';
					when BK_SUM_3_8 =>
						state_s <= LOAD_OUTPUT;	
						if (sum_3_s(63)='0') then
							bk_sum_3_8_s <= Bkm3_s + signed("000" & sum_3_s(63 downto 3));
						else
							bk_sum_3_8_s <= Bkm3_s + signed("111" & sum_3_s(63 downto 3));
						end if;
						data_rdy_o <= '0';
	--				when ADD_SMOOTH =>
	--					state_s <= LOAD_OUTPUT;					
	--					data_rdy_o <= '0';
	--					if (bk_sum_3_8_s>Bkm1_s) then	--delta positive
	--						if (((bk_sum_3_8_s+smooth_error_s)-Bkm1_s) > Bsmooth_s) then
	--							smooth_error_s <= smooth_error_s + (bk_sum_3_8_s-Bkm1_s) - Bsmooth_s;
	--							BKp1_s <= bk_sum_3_8_s + Bsmooth_s;
	--						else
	--							smooth_error_s <= (others=>'0');
	--							BKp1_s <= bk_sum_3_8_s + smooth_error_s;
	--						end if;
	--					else									--delta negative
	--						if ((Bkm1_s-(bk_sum_3_8_s+smooth_error_s)) > Bsmooth_s) then
	--							smooth_error_s <= smooth_error_s - ((Bkm1_s-bk_sum_3_8_s) - Bsmooth_s);
	--							BKp1_s <= Bkm1_s - Bsmooth_s;
	--						else
	--							smooth_error_s <= (others=>'0');
	--							BKp1_s <= bk_sum_3_8_s + smooth_error_s;
	--						end if;
	--					end if;
					when LOAD_OUTPUT =>
						state_s <= IDLE;
						data_rdy_o <= '0';
						if (low_marker_s='1') then
							low_marker_s <= '0';
							BKp1_o <= low_marker_val_i;
							Bkm1_s <= signed(x"00000000" & low_marker_val_i);
						elsif (high_marker_s='1') then
							high_marker_s <= '0';
							BKp1_o <= high_marker_val_i;
							Bkm1_s <= signed(x"00000000" & high_marker_val_i);
						else
							Bkm1_s <= bk_sum_3_8_s;
							case int_range_sel_i is
								when x"0" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31 downto 0));
								when x"1" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+1 downto 1));
								when x"2" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+2 downto 2));
								when x"3" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+3 downto 3));
								when x"4" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+4 downto 4));
								when x"5" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+5 downto 5));
								when x"6" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+6 downto 6));
								when x"7" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+7 downto 7));
								when x"8" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+8 downto 8));
								when x"9" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+9 downto 9));
								when x"a" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+10 downto 10));
								when x"b" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+11 downto 11));
								when x"c" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+12 downto 12));
								when x"d" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+13 downto 13));
								when x"e" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+14 downto 14));
								when x"f" =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31+15 downto 15));
								when others =>
									BKp1_o <= std_logic_vector(bk_sum_3_8_s(31 downto 0));
							end case;
						end if;
						Vkm1_s <= Vk_s;
						Vkm2_s <= Vkm1_s;
						Vkm3_s <= Vkm2_s;
						Bkm2_s <= Bkm1_s;
						Bkm3_s <= Bkm2_s;
				end case;
			end if;
		end if;
	end process;

	cmp_C0 : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> C0_i,
			s_o	=> C0_s
		);

	cmp_start : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> start_i,
			s_o	=> start_s
		);

	cmp_low_marker : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> low_marker_i,
			s_o	=> low_marker_pulse_s
		);

	cmp_high_marker : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> high_marker_i,
			s_o	=> high_marker_pulse_s
		);

end Behavioral;