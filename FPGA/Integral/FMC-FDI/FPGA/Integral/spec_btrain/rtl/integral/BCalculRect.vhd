----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    17:14:33 06/03/2013 
-- Design Name: 
-- Module Name:    BCalculRect - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity BCalculRect is
	--Calcul :
	-- Bk_i+corr_fact_i*(Vk_i-Vo_i)+Bsmooth_i;
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;

		--Control input
		start_i	: in std_logic;
		init_Bk_i: in std_logic;
		int_range_sel_i : in std_logic_vector(3 downto 0);

		--Input information signals
		C0_i          : in std_logic;
		C0_used_i     : in std_logic;
		low_marker_i  : in std_logic;
		high_marker_i : in std_logic;

		soft_fixed_i    : in std_logic;
		soft_val_i      : in std_logic_vector(31 downto 0);

		--Marker values
		low_marker_val_i  : in std_logic_vector(31 downto 0);
		high_marker_val_i : in std_logic_vector(31 downto 0);

		--Data
		Bk_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		Bsmooth_i	: in std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0);
		corr_fact_i	: in std_logic_vector(31 downto 0);
		Vk_i			: in std_logic_vector(NB_BITS_ADC_VALUE-1 downto 0);
		Vo_i			: in std_logic_vector(31 downto 0);
					
		--Output
		data_rdy_o	   : out std_logic;
		fifo_wen_o	   : out std_logic;
		B_error_low_o  : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		B_error_high_o : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		BKp1_o		   : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0)
	);
end BCalculRect;

architecture Behavioral of BCalculRect is

	--Type
	type calc_states is (
		IDLE,
		RESET_INTEGRAL,
		VK_SUB_VO,
		CORR_FACT_MULT,
		ADD_BK,
		ADD_SMOOTH
	);
	
	--Signal
	signal state_s			: calc_states;

	signal start_s : std_logic;

	signal start_mult_s       : std_logic;
	signal start_mult_pulse_s : std_logic;
	signal end_mult_s    : std_logic;
	signal end_mult_in_s : std_logic;

--	signal Bk_s				: signed(31 downto 0);
	signal B_out_s			: std_logic_vector(39 downto 0);
	signal Bk_s				: std_logic_vector(39 downto 0);
	signal Bsmooth_s		: signed(31 downto 0);
	signal smooth_error_s: signed(31 downto 0);

	signal vk_sub_vo_32_s: signed(31 downto 0);
	signal vk_sub_vo_36_s: signed(35 downto 0);
	signal corr_fact_s	: signed(35 downto 0);
	signal result_mult_s	: std_logic_vector(71 downto 0);
--	signal Bk_corr_fact_s: signed(31 downto 0);
	signal Bk_corr_fact_s: std_logic_vector(39 downto 0);
	signal B_error_low_s : std_logic_vector(39 downto 0);
	signal B_error_high_s: std_logic_vector(39 downto 0);
	
	signal C0_pulse_s		: std_logic;
	signal B_rst_s : std_logic;

	signal force_val_s : std_logic;
	
	signal low_marker_pulse_s  : std_logic;
	signal low_marker_s        : std_logic;
	signal high_marker_pulse_s : std_logic;
	signal high_marker_s       : std_logic;

	signal low_marker_val_s  : std_logic_vector(39 downto 0);
	signal high_marker_val_s : std_logic_vector(39 downto 0);
	signal soft_fixed_val_s  : std_logic_vector(39 downto 0);
	
begin

	--State Machine
	--Calcul :
	-- Bk_i+corr_fact_i*(Vk_i-Vo_i)+Bsmooth_i;
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				state_s           <= IDLE;
				start_mult_s      <= '0';
				smooth_error_s    <= (others=>'0');
				corr_fact_s       <= (others=>'0');
				vk_sub_vo_32_s    <= (others=>'0');
				vk_sub_vo_36_s    <= (others=>'0');
				Bk_corr_fact_s    <= (others=>'0');
				B_out_s           <= (others=>'0');
				Bk_s              <= (others=>'0');
				Bsmooth_s         <= (others=>'0');
				B_rst_s           <= '0';
				low_marker_s      <= '0';
				high_marker_s     <= '0';
				force_val_s       <= '0';
				low_marker_val_s  <= (others=>'0');
				high_marker_val_s <= (others=>'0');
				soft_fixed_val_s  <= (others=>'0');
			else
				low_marker_val_s  <= "00" & low_marker_val_i & "000000";
				high_marker_val_s <= "00" & high_marker_val_i & "000000";
				soft_fixed_val_s  <= "00" & soft_val_i & "000000";
				if (C0_pulse_s='1') then
					B_rst_s <= '1';
				end if;
				if (low_marker_pulse_s='1') then
					low_marker_s <= '1';
				end if;
				if (high_marker_pulse_s='1') then
					high_marker_s <= '1';
				end if;
				case state_s is
					when IDLE =>
						if (start_s='1') then
							state_s <= VK_SUB_VO;
						end if;
						data_rdy_o <= '1';

					when RESET_INTEGRAL =>
						state_s <= IDLE;
						Bk_s           <= (others=>'0');
						B_out_s        <= (others=>'0');
						start_mult_s   <= '0';
						smooth_error_s <= (others=>'0');
						vk_sub_vo_32_s <= (others=>'0');
						vk_sub_vo_36_s <= (others=>'0');
						Bk_corr_fact_s <= (others=>'0');
					
					when VK_SUB_VO =>
						state_s        <= CORR_FACT_MULT;
						data_rdy_o     <= '0';
						vk_sub_vo_32_s <= signed(Vk_i) - signed(Vo_i);
						Bk_s           <= B_out_s;
						Bsmooth_s      <= signed(Bsmooth_i);
						corr_fact_s	   <= signed(x"0" & corr_fact_i);
						
					when CORR_FACT_MULT =>
						if (end_mult_s='1') then
							state_s <= ADD_BK;
						end if;
						data_rdy_o   <= '0';
						start_mult_s <= '1';
						if (vk_sub_vo_32_s(vk_sub_vo_32_s'length-1)='0') then
							vk_sub_vo_36_s <= signed(x"0" & vk_sub_vo_32_s);
						else
							vk_sub_vo_36_s <= signed(x"f" & vk_sub_vo_32_s);
						end if;
						
					when ADD_BK =>
						state_s      <= ADD_SMOOTH;
						data_rdy_o   <= '0';
						start_mult_s <= '0';
						if (result_mult_s(71)='0') then
							Bk_corr_fact_s <= signed('0' & result_mult_s(69 downto 31)) + signed(Bk_s);
						else
							Bk_corr_fact_s <= signed('1' & result_mult_s(69 downto 31)) + signed(Bk_s);
						end if;
						
					when ADD_SMOOTH =>
						state_s    <= IDLE;
						data_rdy_o <= '0';
						if ((B_rst_s='1')and(C0_used_i='1')) then
							B_rst_s <= '0';
							B_out_s <= (others=>'0');
							smooth_error_s <= (others=>'0');
							
						elsif (soft_fixed_i='1') then
							B_out_s <= soft_fixed_val_s;
							smooth_error_s <= (others=>'0');
							B_error_low_s <= signed(soft_fixed_val_s) - signed(Bk_corr_fact_s);

						elsif (low_marker_s='1') then
							low_marker_s <= '0';
							B_out_s     <= low_marker_val_s;
							smooth_error_s <= (others=>'0');
							B_error_low_s <= signed(low_marker_val_s) - signed(Bk_corr_fact_s);
--							if (low_marker_val_s+smooth_error_s>Bk_s) then	--delta positive
--								if ((low_marker_val_s+smooth_error_s-Bk_s) > Bsmooth_s) then
--									BKp1_o <= Bk_s + Bsmooth_s;
--									smooth_error_s <= (low_marker_val_s+smooth_error_s-Bk_s) - Bsmooth_s;
--								else
--									BKp1_o <= low_marker_val_s+smooth_error_s;
--									smooth_error_s <= (others=>'0');
--								end if;
--							else									          --delta negative
--								if (Bk_s-(low_marker_val_s+smooth_error_s) > Bsmooth_s) then
--									BKp1_o <= Bk_s - Bsmooth_s;
--									smooth_error_s <= Bsmooth_s - (Bk_s-(low_marker_val_s+smooth_error_s));
--								else
--									BKp1_o <= low_marker_val_s+smooth_error_s;
--									smooth_error_s <= (others=>'0');
--								end if;
--							end if;
						elsif (high_marker_s='1') then
							high_marker_s  <= '0';
							B_out_s        <= high_marker_val_s;
							smooth_error_s <= (others=>'0');
							B_error_high_s <= signed(low_marker_val_s) - signed(Bk_corr_fact_s);
--							if (high_marker_val_s+smooth_error_s>Bk_s) then	--delta positive
--								if ((high_marker_val_s+smooth_error_s-Bk_s) > Bsmooth_s) then
--									BKp1_o <= Bk_s + Bsmooth_s;
--									smooth_error_s <= (high_marker_val_s+smooth_error_s-Bk_s) - Bsmooth_s;
--								else
--									BKp1_o <= high_marker_val_s+smooth_error_s;
--									smooth_error_s <= (others=>'0');
--								end if;
--							else									          --delta negative
--								if (Bk_s-(high_marker_val_s+smooth_error_s) > Bsmooth_s) then
--									BKp1_o <= Bk_s - Bsmooth_s;
--									smooth_error_s <= Bsmooth_s - (Bk_s-(high_marker_val_s+smooth_error_s));
--								else
--									BKp1_o <= high_marker_val_s+smooth_error_s;
--									smooth_error_s <= (others=>'0');
--								end if;
--							end if;

						-- The error is always determined as positive value as the Bsmooth_s value.
						-- The sign of it is determined as addition or substraction
						
						else
--							B_out_s <= Bk_corr_fact_s + smooth_error_s;
							B_out_s <= Bk_corr_fact_s;
							smooth_error_s <= (others=>'0');
						end if;
--						elsif (Bk_corr_fact_s+smooth_error_s>Bk_s) then	--delta positive
--							if (((Bk_corr_fact_s+smooth_error_s)-Bk_s) > Bsmooth_s) then
--								BKp1_o <= Bk_s + Bsmooth_s;
--								smooth_error_s <= ((Bk_corr_fact_s+smooth_error_s)-Bk_s) - Bsmooth_s;
--							else
--								BKp1_o <= Bk_corr_fact_s + smooth_error_s;
--								smooth_error_s <= (others=>'0');
--							end if;
--						else									                  --delta negative
--							if ((Bk_s-(Bk_corr_fact_s+smooth_error_s)) > Bsmooth_s) then
--								BKp1_o <= Bk_s - Bsmooth_s;
--								smooth_error_s <= Bsmooth_s - (Bk_s-(Bk_corr_fact_s+smooth_error_s));
--							else
--								BKp1_o <= Bk_corr_fact_s + smooth_error_s;
--								smooth_error_s <= (others=>'0');
--							end if;
--						end if;
				end case;
			end if;
		end if;
	end process;
	BKp1_o <= B_out_s(37 downto 6);
	B_error_low_o  <= B_error_low_s(37 downto 6);
	B_error_high_o <= B_error_high_s(37 downto 6);

	--Multiplier
	cmp_muliplier : Mult36x36_signed 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			A_i	=> std_logic_vector(vk_sub_vo_36_s),
			B_i	=> std_logic_vector(corr_fact_s),
					
			start_i	=> start_mult_pulse_s,

			result_o		=> result_mult_s,
			result_rdy_o=> end_mult_in_s
		);


	--Input rising detect
	cmp_start_calc : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> start_i,
			s_o	=> start_s
		);
		
	cmp_start_mult : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> start_mult_s,
			s_o	=> start_mult_pulse_s
		);
		
	cmp_end_mult : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> end_mult_in_s,
			s_o	=> end_mult_s
		);
		
	cmp_C0 : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> C0_i,
			s_o	=> C0_pulse_s
		);

	cmp_low_marker : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> low_marker_i,
			s_o	=> low_marker_pulse_s
		);

	cmp_high_marker : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> high_marker_i,
			s_o	=> high_marker_pulse_s
		);

end Behavioral;

