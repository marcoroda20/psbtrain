----------------------------------------------------------------------------------
-- Company:			CERN/TE-MS-MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    18:21:32 12/04/2012 
-- Design Name: 
-- Module Name:    BTrain_integral - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.integral_pkg.all;

entity BTrain_integral is
	port (
		clk_i			: in std_logic;
		reset_i		: in std_logic;
	
		adc_data_i		: in std_logic_vector(17 downto 0);
		adc_data_rdy_i	: in std_logic;

		init_Bk_i		 : in std_logic;
		int_c0_used_i   : in std_logic;
		int_range_sel_i : in std_logic_vector(3 downto 0);
		soft_fixed_i    : in std_logic;
		soft_val_i      : in std_logic_vector(31 downto 0);

		--Register	
		reg_val_cfg_sel_integral_i : in std_logic_vector(1 downto 0);
		
		Bsmooth_i		: in std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0);
		corr_fact_i		: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		Vo_i				: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);

		--Input information signals
		low_marker_i  : in std_logic;
		high_marker_i : in std_logic;

		--Marker values
		low_marker_val_i  : in std_logic_vector(31 downto 0);
		high_marker_val_i : in std_logic_vector(31 downto 0);

		C0_i				: in std_logic;
		
		--Output
		B_error_low_o  : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		B_error_high_o : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		BKp1_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		B_data_rdy_o	: out std_logic
	);
end BTrain_integral;

architecture Behavioral of BTrain_integral is

	--Constant
	--for DAC_sel
	constant ZERO			: std_logic_vector(1 downto 0) := "00";
	constant INTEGRAL_3_8: std_logic_vector(1 downto 0) := "01";
	constant INTEGRAL_W_T: std_logic_vector(1 downto 0) := "10";
	constant ADC_VALUE	: std_logic_vector(1 downto 0) := "11";

	--Signal
	signal clk_buf_s		: std_logic;
	
	signal Bk_3_8_s			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal data_rdy_3_8_s	: std_logic;
	
	signal Bk_rect_s				: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal data_rdy_rect_s		: std_logic;
	
	signal Bk_s					: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal B_data_rdy_s		: std_logic;
	
	signal reg_dac_sel_range_s : std_logic_vector(3 downto 0);

	attribute keep : string;

	attribute keep of data_rdy_3_8_s : signal is "TRUE";
	attribute keep of data_rdy_rect_s : signal is "TRUE";

	
begin
	
--	--3/8 Integral
--	cmp_3_8_Integral : BCalcul3_8
--	--10 clock cycle is needed to generate each value
--	--  4 stages
--	--  can run more than 400MHz
--	--  deltat is not implemented
--	--  pipelining can not be used (Bk must be calculed)
--	--  between two start of calcul, the signal start_i must be at '0' for 9 clk cycle
--	port map(
--		clk_i		=> clk_i,
--		reset_i	=> reset_i,
--
--		--Control input
--		start_i	=> adc_data_rdy_i,
--		init_Bk_i=> '0',
--		C0_i		=> C0_i,
--
--		int_range_sel_i => int_range_sel_i,
--		int_c0_used_i   => int_c0_used_i,
--
--		--Input information signals
--		low_marker_i  => low_marker_i,
--		high_marker_i => high_marker_i,
--		--Marker values
--		low_marker_val_i  => low_marker_val_i,
--		high_marker_val_i => high_marker_val_i,
--
--		--Data
--		Bk_i			=> Bk_3_8_s,
--		Vk_i			=> adc_data_i,
----		Bsmooth_i	=> Bsmooth_i,
--					
--		--Output
--		data_rdy_o	=> data_rdy_3_8_s,
--		BKp1_o		=> Bk_3_8_s
--	);
	
	--Integral rectangular
	cmp_integral_rect : BCalculRect 
	--Calcul :
	-- Bk_i+corr_fact_i*(Vk_i-Vo_i)+Bsmooth_i;
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,

		--Control input
		start_i	=> adc_data_rdy_i,
		init_Bk_i=> '0',
		int_range_sel_i => int_range_sel_i,

		--Input information signals
		C0_i          => C0_i,
		C0_used_i     => int_c0_used_i,
		low_marker_i  => low_marker_i,
		high_marker_i => high_marker_i,
		--Marker values
		low_marker_val_i  => low_marker_val_i,
		high_marker_val_i => high_marker_val_i,

		soft_fixed_i   => soft_fixed_i,
		soft_val_i     => soft_val_i,

		--Data
		Bk_i			=> Bk_rect_s,
		Vk_i			=> adc_data_i,
		
		Bsmooth_i	=> Bsmooth_i,
		corr_fact_i	=> corr_fact_i,
		Vo_i			=> Vo_i,
--		deltat_i		=> deltat_i,
					
		--Output
		data_rdy_o	   => data_rdy_rect_s,
		fifo_wen_o	   => open,
		B_error_low_o  => B_error_low_o,
		B_error_high_o => B_error_high_o,
		BKp1_o		   => Bk_rect_s
	);
	B_data_rdy_o <= data_rdy_rect_s;
	BKp1_o <= Bk_rect_s;
	
--	cmp_mux_integral : mux_integral
--		port map(
--			clk_i		=> clk_i,
--			reset_i	=> reset_i,
--			
--			data_rdy_3_8_i	=> data_rdy_3_8_s,
--			BKp1_3_8_i		=> Bk_3_8_s,
--
--			data_rdy_rect_i	=> data_rdy_rect_s,
--			BKp1_rect_i		=> Bk_rect_s,
--			
--			reg_val_cfg_sel_integral_i => reg_val_cfg_sel_integral_i,
--			
--			--Output
--			data_rdy_o	=> B_data_rdy_o,
--			BKp1_o		=> BKp1_o
--		);

end Behavioral;

