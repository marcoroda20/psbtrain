----------------------------------------------------------------------------------
-- Company: 		CERN/TE-MS-MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    09:54:44 01/18/2013 
-- Design Name: 
-- Module Name:    B_FD_Sum - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: Calculate B[k] as a ponderation of Bf and Bd. The 
--              Recalculate Bdot as a ponderate difference between 
--              B[k] and B[k-1] 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 0.02 - Add recalculation of Bdot 2014_09_30
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity B_FD_Sum is
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;

		reg_f_b_coeff_i     : in std_logic_vector(31 downto 0);
		reg_d_b_coeff_i     : in std_logic_vector(31 downto 0);
		reg_kf_bdot_coeff_i : in std_logic_vector(31 downto 0);

		reg_intern_bdot_val_i : in std_logic_vector(2 downto 0);

		Bk_f_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		B_f_data_rdy_i	: in std_logic;

		Bk_d_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		B_d_data_rdy_i	: in std_logic;

		Bk_o				: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		B_data_rdy_o	: out std_logic;

		Bdot_o				: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		Bdot_data_rdy_o	: out std_logic
	);
end B_FD_Sum;

architecture Behavioral of B_FD_Sum is

	--Type
	type FD_sum_states is 
	(	
		IDLE,
		START_B_CALC,
		WAIT_B_END_CALC,
		LATCH_BK,
		LATCH_BDOT_VAL,
		FACT_MULT,
		LOAD_BDOT_MULT,
		DIFF,
		BDOT_KF_LOAD,
		BDOT_KF_END,
		LOAD_OUTPUT
	);

	--Constant
--	constant NBER_STAGES_INT : Integer := 9;
	constant WIDTH_DRDY : Integer := 4; --40;
	
	--Signal
	signal state_s	: FD_sum_states;

	signal Bk_f_in_s	: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal Bk_d_in_s	: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal f_sign_s   : std_logic;
	signal d_sign_s   : std_logic;

	signal Bk_f_drdy_s	: std_logic;
	signal Bk_d_drdy_s	: std_logic;

	signal B_in_drdy_s	: std_logic;
	signal B_in_drdy_rising_s	: std_logic;

	signal result_b_s    : std_logic_vector((2*NB_BITS_B_VALUE)-1 downto 0);
	signal result_bdot_s : std_logic_vector(71 downto 0);
	
	signal Bk_s      : std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal Bkm1_s    : std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	
	signal Bdot_s    : std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	
	signal start_calc_b_s : std_logic;
	signal end_calc_b_s   : std_logic;

	signal start_calc_bdot_s : std_logic;
	signal end_mult_s        : std_logic;
	signal end_mult_both_s   : std_logic;
	signal end_mult_Bk_s     : std_logic;
	signal end_mult_Bkm1_s   : std_logic;

	signal Bkm1_36_s : std_logic_vector(35 downto 0);
	signal Bk_36_s   : std_logic_vector(35 downto 0);
	signal fact_s    : std_logic_vector(35 downto 0);
	signal result_mult_Bk_s   : std_logic_vector(71 downto 0);
	signal result_mult_Bkm1_s : std_logic_vector(71 downto 0);
	signal mult_Bk_s          : std_logic_vector(63 downto 0);
	signal mult_Bkm1_s        : std_logic_vector(63 downto 0);
	signal diff_s             : std_logic_vector(63 downto 0);
	signal diff2_s            : std_logic_vector(31 downto 0);
	
	signal Bdot_before_kf_s : std_logic_vector(35 downto 0);
	signal result_Bdot_Kf_s : std_logic_vector(71 downto 0);
	
	signal start_Bdot_Kf_s    : std_logic;
	signal end_mult_Bdot_Kf_s : std_logic;
	
	signal end_s : std_logic;
	
	signal drdy_counter_s : integer;

begin

	--Input latch and Bk_data_ready calculation
	process (clk_i,reset_i) 
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				Bk_f_in_s	<= (others=>'0');
				Bk_d_in_s	<= (others=>'0');
			else
				--Input
				if (B_f_data_rdy_i='1') then
					Bk_f_in_s	<= Bk_f_i;
				end if;
				if (B_d_data_rdy_i='1') then
					Bk_d_in_s	<= Bk_d_i;
				end if;
			end if;
		end if;
	end process;

	--Next State
	process (clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				state_s	         <= IDLE;
				Bk_s              <= (others=>'0');	
				Bdot_s            <= (others=>'0');	
				Bkm1_s            <= (others=>'0');	
				start_calc_b_s    <= '0';
				start_calc_bdot_s <= '0';
				end_s			      <= '0';
				fact_s            <= (others=>'0');
				Bkm1_36_s         <= (others=>'0');
				Bk_36_s           <= (others=>'0');
				mult_Bk_s         <= (others=>'0');
				mult_Bkm1_s       <= (others=>'0');
				diff_s            <= (others=>'0');
				Bdot_before_kf_s  <= (others=>'0');
				start_Bdot_Kf_s   <= '0';
			else
				start_calc_b_s   <= '0';
				start_calc_bdot_s<= '0';
				end_s			     <= '0';
				start_Bdot_Kf_s  <= '0';
				case state_s is
					when IDLE =>
						if (B_in_drdy_rising_s = '1') then
							state_s <= START_B_CALC;
						end if;
						
					when START_B_CALC =>
						state_s <= WAIT_B_END_CALC;
						start_calc_b_s<= '1';
						
					when WAIT_B_END_CALC =>
						if (end_calc_b_s='1') then
							state_s <= LATCH_BK;
						end if;
						
					when LATCH_BK =>
						state_s <= LATCH_BDOT_VAL;
						Bkm1_s <= Bk_s;
						Bk_s <= result_b_s((2*NB_BITS_B_VALUE)-1 downto NB_BITS_B_VALUE);	
					
					when LATCH_BDOT_VAL =>
						state_s <= FACT_MULT;
						fact_s <= reg_kf_bdot_coeff_i(31) & reg_kf_bdot_coeff_i(31) & reg_kf_bdot_coeff_i(31) & reg_kf_bdot_coeff_i(31) & reg_kf_bdot_coeff_i;
						if Bkm1_s(Bkm1_s'length-1)='1' then
							Bkm1_36_s <= x"f" & Bkm1_s;
						else
							Bkm1_36_s <= x"0" & Bkm1_s;
						end if;
						if Bk_s(Bk_s'length-1)='1' then
							Bk_36_s <= x"f" & Bk_s;
						else
							Bk_36_s <= x"0" & Bk_s;
						end if;
					
					when FACT_MULT =>
						if (end_mult_s='1') then
							state_s <= LOAD_BDOT_MULT;
						end if;
						start_calc_bdot_s <= '1';
						end_mult_both_s <= end_mult_Bk_s or end_mult_Bkm1_s;
					
					when LOAD_BDOT_MULT =>
						state_s <= DIFF;
						if (result_mult_Bk_s(71)='0') then
							--mult_Bk_s <= '0' & result_mult_Bk_s(61 downto 31);
							mult_Bk_s <= '0' & result_mult_Bk_s(62 downto 0);
						else
							--mult_Bk_s <= '1' & result_mult_Bk_s(61 downto 31);
							mult_Bk_s <= '1' & result_mult_Bk_s(62 downto 0);
						end if;
						if (result_mult_Bkm1_s(71)='0') then
							--mult_Bkm1_s <= '0' & result_mult_Bkm1_s(61 downto 31);
							mult_Bkm1_s <= '0' & result_mult_Bkm1_s(62 downto 0);
						else
							--mult_Bkm1_s <= '1' & result_mult_Bkm1_s(61 downto 31);
							mult_Bkm1_s <= '1' & result_mult_Bkm1_s(62 downto 0);
						end if;
					
					when DIFF =>
						state_s <= BDOT_KF_LOAD;
						diff_s <= std_logic_vector(signed(mult_Bk_s)-signed(mult_Bkm1_s));	

					when BDOT_KF_LOAD =>
						state_s <= BDOT_KF_END;
						Bdot_before_kf_s <= diff_s(31) & diff_s(31) & diff_s(31) & diff_s(31) & diff_s(31 downto 0);
						start_Bdot_Kf_s <= '1';
						
					when BDOT_KF_END =>
						if (end_mult_Bdot_Kf_s='1') then
							state_s <= LOAD_OUTPUT;
						end if;

					when LOAD_OUTPUT =>
						state_s <= IDLE;  
						if (reg_intern_bdot_val_i="000") then
							Bdot_s <= result_Bdot_Kf_s(63 downto 32);
						elsif (reg_intern_bdot_val_i="001") then
							Bdot_s <= mult_Bkm1_s(63 downto 32);
						elsif (reg_intern_bdot_val_i="010") then
							Bdot_s <= mult_Bk_s(63 downto 32);
						elsif (reg_intern_bdot_val_i="011") then
							Bdot_s <= result_Bdot_Kf_s(31 downto 0);
						elsif (reg_intern_bdot_val_i="100") then
							Bdot_s <= Bdot_before_kf_s(31 downto 0);
						elsif (reg_intern_bdot_val_i="101") then
							Bdot_s <= diff2_s(31 downto 0);
						elsif (reg_intern_bdot_val_i="110") then
							if Bk_s(Bk_s'length-1)='1' then
								Bdot_s <= '1' & Bk_s(7 downto 0) & "000" & x"00000";
							else
								Bdot_s <= '0' & Bk_s(7 downto 0) & "000" & x"00000";
							end if;
						else 
							Bdot_s <= diff_s(31 downto 0);
						end if;
						end_s <= '1';
						
				end case;
			end if;
		end if;
	end process;

	-- g1*Bf + g2*Bd calculation
	cmp_B_F_D : Mult32x32_2chan
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			A_i	=> Bk_f_in_s,
			B_i	=> reg_f_b_coeff_i,
			
			C_i	=> Bk_d_in_s,
			D_i	=> reg_d_b_coeff_i,
			
			start_i	=> start_calc_b_s,

			result_o		=> result_b_s,
			result_rdy_o=> end_calc_b_s
		);
		
	-- Bdot scaling
	cmp_mult_Bkm1 : Mult36x36_signed 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			A_i	=> Bkm1_36_s,
			B_i	=> x"000800000",
					
			start_i	=> start_calc_bdot_s,

			result_o		=> result_mult_Bkm1_s,
			result_rdy_o=> end_mult_Bkm1_s
		);

	cmp_mult_Bk : Mult36x36_signed 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			A_i	=> Bk_36_s,
			B_i	=> x"000800000",
					
			start_i	=> start_calc_bdot_s,

			result_o		=> result_mult_Bk_s,
			result_rdy_o=> end_mult_Bk_s
		);

--	fact_s <= reg_kf_bdot_coeff_i(31) & reg_kf_bdot_coeff_i(31) & reg_kf_bdot_coeff_i(31) & reg_kf_bdot_coeff_i(31) & reg_kf_bdot_coeff_i;
	cmp_Bdot_kf : Mult36x36_signed 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			A_i	=> Bdot_before_kf_s,
			B_i	=> fact_s,
					
			start_i	=> start_Bdot_Kf_s,

			result_o		=> result_Bdot_Kf_s,
			result_rdy_o=> end_mult_Bdot_Kf_s
		);

	--Mux to choose the range on the DAC
	process (clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				Bk_o   <= (others=>'0');
				Bdot_o <= (others=>'0');
			else
				Bk_o   <= Bk_s(NB_BITS_B_VALUE-1 downto 0);
				Bdot_o <= Bdot_s(NB_BITS_B_VALUE-1 downto 0);
			end if;
		end if;
	end process;
	
	--Data ready out signal
	process (clk_i,reset_i)
	begin
		if (reset_i='1') then
			B_data_rdy_o    <= '0';
			Bdot_data_rdy_o <= '0';
			drdy_counter_s  <= 0;
		elsif (clk_i'event) and (clk_i='1') then
			if ((end_s='1') or (drdy_counter_s/=0 and drdy_counter_s<WIDTH_DRDY)) then
				B_data_rdy_o    <= '1';
				Bdot_data_rdy_o <= '1';
				drdy_counter_s  <= drdy_counter_s+1;
			else
				B_data_rdy_o    <= '0';
				Bdot_data_rdy_o <= '0';
				drdy_counter_s  <= 0;
			end if;
		end if;
	end process;

	--RisingDetect
	B_in_drdy_s <= B_f_data_rdy_i and B_d_data_rdy_i;

	cmp_drdy : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> B_in_drdy_s,
			s_o	=> B_in_drdy_rising_s
		);
		
	cmp_end_calc_bdot : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> end_mult_both_s,
			s_o	=> end_mult_s
		);
		
end Behavioral;

