----------------------------------------------------------------------------------
-- Company: CERN
-- Engineer: Daniel Oberson
-- 
-- Create Date:    17:25:08 01/20/2015 
-- Design Name: 
-- Module Name:    B_err_mult - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity B_err_mult is
	port(
		clk_i   : in std_logic;
		reset_i : in std_logic;

		coeff_i : in std_logic_vector(31 downto 0);

		B_err_i    : in std_logic_vector(31 downto 0);
		B_data_rdy_i : in std_logic;

		B_err_o  : out std_logic_vector(31 downto 0)
	);
end B_err_mult;

architecture Behavioral of B_err_mult is

	--Type
	type B_err_mult_states is 
	(	
		IDLE,
		START_CALC,
		WAIT_END_CALC,
		LOAD_OUTPUT
	);
	
	--Signal
	signal B_err_36_s : std_logic_vector(35 downto 0);
	signal k_36_s     : std_logic_vector(35 downto 0);
	
	signal result_s : std_logic_vector(71 downto 0);
	
	signal B_data_rdy_rising_s : std_logic;
	signal start_calc_s : std_logic;
	signal end_s : std_logic;

	signal state_s	: B_err_mult_states;

begin

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				state_s      <= IDLE;
				B_err_36_s   <= (others=>'0');
				k_36_s       <= (others=>'0');
				start_calc_s <= '0';
				B_err_o      <= (others=>'0');
			else
				start_calc_s <= '0';
				case state_s is
					when IDLE =>
						if (B_data_rdy_rising_s = '1') then
							state_s <= START_CALC;
						end if;
						
					when START_CALC =>
						state_s <= WAIT_END_CALC;
						start_calc_s <= '1';
						if B_err_i(B_err_i'length-1)='1' then
							B_err_36_s <= x"f" & B_err_i;
						else
							B_err_36_s <= x"0" & B_err_i;
						end if;
						if coeff_i(coeff_i'length-1)='1' then
							k_36_s <= x"f" & coeff_i;
						else
							k_36_s <= x"0" & coeff_i;
						end if;
							
					when WAIT_END_CALC =>
						if (end_s='1') then
							state_s <= LOAD_OUTPUT;
						end if;
						
					when LOAD_OUTPUT =>
						if (result_s(71)='0') then
							B_err_o <= '0' & result_s(61 downto 31);
						else
							B_err_o <= '1' & result_s(61 downto 31);
						end if;
					
				end case;
			end if;
		end if;
	end process;
						
	cmp_B_F_err : Mult36x36_signed 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			A_i	=> B_err_36_s,
			B_i	=> k_36_s,
					
			start_i	=> start_calc_s,

			result_o		=> result_s,
			result_rdy_o=> end_s
		);

	cmp_drdy : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> B_data_rdy_i,
			s_o	=> B_data_rdy_rising_s
		);

end Behavioral;

