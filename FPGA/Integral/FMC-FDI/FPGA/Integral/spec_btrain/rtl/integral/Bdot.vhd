----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    16:16:42 06/11/2013 
-- Design Name: 
-- Module Name:    Bdot - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity Bdot is
	--Calcul :
	-- Bdot = corr_fact_i*(Vk_i-Vo_i);
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;

		--Control input
		start_i	: in std_logic;

		--Data
		corr_fact_i	: in std_logic_vector(31 downto 0);
		Vk_i			: in std_logic_vector(NB_BITS_ADC_VALUE-1 downto 0);
		Vo_i			: in std_logic_vector(31 downto 0);
					
		--Output
		data_rdy_o	: out std_logic;
		Bdot_o		: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0)
	);
end Bdot;

architecture Behavioral of Bdot is

	--Type
	type calc_states is (
		IDLE,
		VK_SUB_VO,
		CORR_FACT_MULT,
		LOAD_OUTPUT
	);

	--Signal
	signal state_s			: calc_states;

	signal start_s : std_logic;

	signal start_mult_s	: std_logic;
	signal end_mult_s		: std_logic;
	signal end_mult_in_s	: std_logic;

	signal vk_sub_vo_32_s: signed(31 downto 0);
	signal vk_sub_vo_36_s: signed(35 downto 0);
	signal corr_fact_s	: signed(35 downto 0);
	signal result_mult_s	: std_logic_vector(71 downto 0);

begin

	--State Machine
	--Calcul :
	-- Bdot = corr_fact_i*(Vk_i-Vo_i);
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				state_s <= IDLE;
				start_mult_s <= '0';
				corr_fact_s <= (others=>'0');
				vk_sub_vo_32_s <= (others=>'0');
				vk_sub_vo_36_s <= (others=>'0');
				Bdot_o <= (others=>'0');
			else
				case state_s is
					when IDLE =>
						if (start_s='1') then
							state_s <= VK_SUB_VO;
						end if;
						data_rdy_o <= '1';
					when VK_SUB_VO =>
						state_s <= CORR_FACT_MULT;
						data_rdy_o <= '0';
						vk_sub_vo_32_s <= signed(Vk_i) - signed(Vo_i);
						corr_fact_s	<= signed(x"0" & corr_fact_i);
					when CORR_FACT_MULT =>
						if (end_mult_s='1') then
							state_s <= LOAD_OUTPUT;
						end if;
						data_rdy_o <= '0';
						start_mult_s <= '1';
						if (vk_sub_vo_32_s(vk_sub_vo_32_s'length-1)='0') then
							vk_sub_vo_36_s <= signed(x"0" & vk_sub_vo_32_s);
						else
							vk_sub_vo_36_s <= signed(x"f" & vk_sub_vo_32_s);
						end if;
					when LOAD_OUTPUT =>
						state_s <= IDLE;
						if (result_mult_s(71)='0') then
							Bdot_o <= '0' & result_mult_s(61 downto 31);
						else
							Bdot_o <= '1' & result_mult_s(61 downto 31);
						end if;
						start_mult_s <= '0';
						data_rdy_o <= '0';
				end case;
			end if;
		end if;
	end process;

	--Multiplier
	cmp_muliplier : Mult36x36_signed 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			A_i	=> std_logic_vector(vk_sub_vo_36_s),
			B_i	=> std_logic_vector(corr_fact_s),
					
			start_i	=> start_mult_s,

			result_o		=> result_mult_s,
			result_rdy_o=> end_mult_in_s
		);


	--Input rising detect
	cmp_start_calc : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> start_i,
			s_o	=> start_s
		);
		
	cmp_end_mult : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> end_mult_in_s,
			s_o	=> end_mult_s
		);

	
end Behavioral;

