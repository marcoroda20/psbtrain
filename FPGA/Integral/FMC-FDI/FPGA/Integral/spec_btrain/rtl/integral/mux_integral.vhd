----------------------------------------------------------------------------------
-- Company:        CERN TE/MSC/MM
-- Engineer:       Daniel Oberson
-- 
-- Create Date:    16:04:06 07/16/2013 
-- Design Name: 
-- Module Name:    mux_integral - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.fmc_adc2M18b2ch_pkg.all;

entity mux_integral is
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		data_rdy_3_8_i	: in std_logic;
		BKp1_3_8_i		: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);

		data_rdy_rect_i	: in std_logic;
		BKp1_rect_i		: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		
		reg_val_cfg_sel_integral_i : in std_logic_vector(1 downto 0);
		
		--Output
		data_rdy_o	: out std_logic;
		BKp1_o		: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0)
	);		
end mux_integral;

architecture Behavioral of mux_integral is

begin

	--Mux to choose the type of integral
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				data_rdy_o	<= '0';
				BKp1_o		<= (others=>'0');
			else
				case reg_val_cfg_sel_integral_i is 
					when "00" =>
						data_rdy_o	<= data_rdy_rect_i;
						BKp1_o		<= BKp1_rect_i;
					when "01" =>
						data_rdy_o	<= data_rdy_3_8_i;
						BKp1_o		<= BKp1_3_8_i;
					when others =>
						data_rdy_o	<= data_rdy_rect_i;
						BKp1_o		<= BKp1_rect_i;
					end case;
			end if;
		end if;
	end process;

end Behavioral;

