----------------------------------------------------------------------------------
-- Company: 		CERN/TE-MS-MM
-- Engineer: 		Daniel Oberson
-- 
-- Description: Package for SDB base on FMC ADC 100Ms/s design for SPEC
--              devloped by Matthieu Cattin.
--
----------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;

package sdb_meta_pkg is

  ------------------------------------------------------------------------------
  -- Meta-information sdb records
  ------------------------------------------------------------------------------

  -- Top module repository url
  constant c_SDB_REPO_URL : t_sdb_repo_url := (
    -- url (string, 63 char)
    repo_url => "TODO: url of repository from OHWR                              ");

  -- Synthesis informations
  constant c_SDB_SYNTHESIS : t_sdb_synthesis := (
    -- Top module name (string, 16 char)
    syn_module_name  => "btrain          ",
    -- Commit ID (hex string, 128-bit = 32 char)
    -- git log -1 --format="%H" | cut -c1-32
    syn_commit_id    => "dce448c21e7f9426dd5b39d1ef1d0496",
    -- Synthesis tool name (string, 8 char)
    syn_tool_name    => "ISE     ",
    -- Synthesis tool version (bcd encoded, 32-bit)
    syn_tool_version => x"00000134",
    -- Synthesis date (bcd encoded, 32-bit)
    syn_date         => x"20131107",
    -- Synthesised by (string, 15 char)
    syn_username     => "doberson       ");

  -- Integration record
  constant c_SDB_INTEGRATION : t_sdb_integration := (
    product     => (
      vendor_id => x"000000000000CE42",  -- CERN
      device_id => x"47c786a2",          -- echo "spec_btrain" | md5sum | cut -c1-8
      version   => x"00020000",          -- bcd encoded, [31:16] = major, [15:0] = minor
      date      => x"20130729",          -- yyyymmdd
      name      => "spec_btrain        "));


end sdb_meta_pkg;


package body sdb_meta_pkg is
end sdb_meta_pkg;
