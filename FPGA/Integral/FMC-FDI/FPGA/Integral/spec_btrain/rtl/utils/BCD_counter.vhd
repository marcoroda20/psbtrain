----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:43:41 07/08/2013 
-- Design Name: 
-- Module Name:    BCD_counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

library work;
use work.utils_pkg.all;

entity BCD_counter is
	generic(g_simulation : integer := 0);
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;

		bcd_time_i		: in std_logic_vector(31 downto 0);
		load_bcd_cnt_i	: in std_logic;
		en_bcd_cnt_i	: in std_logic;
		bcd_countdown_o: out std_logic_vector(31 downto 0);
		zero_flag_o		: out std_logic
	);
end BCD_counter;

architecture Behavioral of BCD_counter is

	--Constant
	constant BCD_COUNTER_FREQUENCY : integer := 1;
	constant BCD_COUNTER_FREQUENCY_SIM : integer := 10000000;

	--Signal
	signal sec_lsb_cnt	: std_logic_vector(3 downto 0);
	signal sec_msb_cnt	: std_logic_vector(3 downto 0);
	signal min_lsb_cnt	: std_logic_vector(3 downto 0);
	signal min_msb_cnt	: std_logic_vector(3 downto 0);
	signal hour_lsb_cnt	: std_logic_vector(3 downto 0);
	signal hour_msb_cnt	: std_logic_vector(3 downto 0);
	signal day_lsb_cnt	: std_logic_vector(3 downto 0);
	signal day_msb_cnt	: std_logic_vector(3 downto 0);

	signal bcd_countdown_s : std_logic_vector(31 downto 0);

	signal clk_1Hz_s : std_logic;
	signal tick1hz_s : std_logic;
  
	signal load_intern_s : std_logic;
  
	signal g_simulation_s : integer := 0;
  
begin

	process(clk_i,reset_i,tick1hz_s,en_bcd_cnt_i,load_intern_s,bcd_time_i,
		sec_lsb_cnt,sec_msb_cnt,min_lsb_cnt,min_msb_cnt,
		hour_lsb_cnt,hour_msb_cnt,day_lsb_cnt,day_msb_cnt)   
	begin
		if rising_edge(clk_i) then
		   if (reset_i='1') then
				sec_lsb_cnt  <= (others=>'0');
				sec_msb_cnt  <= (others=>'0');
				min_lsb_cnt  <= (others=>'0');
				min_msb_cnt  <= (others=>'0');
				hour_lsb_cnt <= (others=>'0');
				hour_msb_cnt <= (others=>'0');
				day_lsb_cnt  <= (others=>'0');
				day_msb_cnt  <= (others=>'0');
				load_intern_s <= '1';
			end if;
			if load_bcd_cnt_i='1' then
				load_intern_s <= '1';
			end if;
			if ( tick1hz_s='1' and en_bcd_cnt_i='1' ) then 
				if load_intern_s='1' then
					--Control of limit 
					if (bcd_time_i(3 downto 0) > 9) then
						sec_lsb_cnt  <= x"9";
					else
						sec_lsb_cnt  <= bcd_time_i(3 downto 0);
					end if;
					if (bcd_time_i(7 downto 4) > 5) then
						sec_msb_cnt <= x"5";
					else
						sec_msb_cnt <= bcd_time_i(7 downto 4);
					end if;
					if (bcd_time_i(11 downto 8) > 9) then
						min_lsb_cnt <= x"9";
					else
						min_lsb_cnt <= bcd_time_i(11 downto 8);
					end if;
					if (bcd_time_i(15 downto 12) > 5) then
						min_msb_cnt <= x"5";
					else
						min_msb_cnt <= bcd_time_i(15 downto 12);
					end if;
					
					if (bcd_time_i(23 downto 20) < 2) then
						if (bcd_time_i(19 downto 16) > 9) then
							hour_lsb_cnt <= x"9";
						else
							hour_lsb_cnt <= bcd_time_i(19 downto 16);
						end if;
					else
						if (bcd_time_i(19 downto 16) > 3) then
							hour_lsb_cnt <= x"3";
						else
							hour_lsb_cnt <= bcd_time_i(19 downto 16);
						end if;
					end if;
					if (bcd_time_i(23 downto 20) > 2) then
						hour_msb_cnt <= x"2";
					else
						hour_msb_cnt <= bcd_time_i(23 downto 20);
					end if;
					if (bcd_time_i(27 downto 24) > 9) then
						day_lsb_cnt <= x"9";
					else
						day_lsb_cnt <= bcd_time_i(27 downto 24);
					end if;
					if (bcd_time_i(31 downto 28) > 9) then
						day_msb_cnt <= x"9";
					else
						day_msb_cnt <= bcd_time_i(31 downto 28);
					end if;
					load_intern_s <= '0';
				else	
					if (day_msb_cnt=x"0" and day_lsb_cnt=x"0" and hour_msb_cnt=x"0" and hour_lsb_cnt=x"0" and 
						 min_msb_cnt=x"0" and min_lsb_cnt=x"0" and sec_msb_cnt=x"0" and sec_lsb_cnt=x"0") then
						sec_lsb_cnt  <= x"0";
						sec_msb_cnt  <= x"0";	
						min_lsb_cnt  <= x"0";	
						min_msb_cnt  <= x"0";		
						hour_lsb_cnt <= x"0";	
						hour_msb_cnt <= x"0";	
						day_lsb_cnt  <= x"0";		
						day_msb_cnt  <= x"0";		
					elsif (sec_lsb_cnt = 0) then
					  sec_lsb_cnt <= "1001"; -- 9
					  if (sec_msb_cnt = 0) then
							  sec_msb_cnt <= "0101"; -- 5			 
							  if (min_lsb_cnt = 0) then
									min_lsb_cnt <= "1001"; -- 9			 
									if (min_msb_cnt = 0) then
										 min_msb_cnt <= "0101"; -- 5	 				 
										 if (hour_lsb_cnt = 0)and(hour_msb_cnt = 0) then
											  hour_lsb_cnt <= "0011"; -- 3 
											  if (hour_msb_cnt = 0) then
													hour_msb_cnt <= "0010"; -- 2
													if (day_lsb_cnt = 0) then
														 day_lsb_cnt <= "1001";	-- 9				  
														 if (day_msb_cnt = 0) then
															  day_msb_cnt <= "1001"; -- 9							
														 else                    
															  day_msb_cnt <= day_msb_cnt - 1;
														 end if;						  
													else                    
														 day_lsb_cnt <= day_lsb_cnt - 1;	
													end if;
												end if;
										 elsif (hour_lsb_cnt = 0) then
											  hour_lsb_cnt <= "1001"; -- 9										 
											  if (hour_msb_cnt = 0) then
													hour_msb_cnt <= "0010"; -- 2
													if (day_lsb_cnt = 0) then
														 day_lsb_cnt <= "1001";	-- 9				  
														 if (day_msb_cnt = 0) then
															  day_msb_cnt <= "1001"; -- 9							
														 else                    
															  day_msb_cnt <= day_msb_cnt - 1;
														 end if;						  
													else                    
														 day_lsb_cnt <= day_lsb_cnt - 1;	
													end if;
											  else                    
													hour_msb_cnt <= hour_msb_cnt - 1;	
											  end if;
										 else
											  hour_lsb_cnt <= hour_lsb_cnt - 1;
										 end if;
									else
										 min_msb_cnt <= min_msb_cnt - 1;
									end if;					
							  else
									min_lsb_cnt <= min_lsb_cnt - 1;
							  end if;
						 else 
							  sec_msb_cnt <= sec_msb_cnt - 1;
						 end if;
					else
						 sec_lsb_cnt <= sec_lsb_cnt - 1;
					end if;			  
				end if;
			end if;
		end if;
	end process;
						
	--BCD time register
	process (clk_i,reset_i,day_msb_cnt,day_lsb_cnt,hour_msb_cnt,hour_lsb_cnt,
							min_msb_cnt,min_lsb_cnt,sec_msb_cnt,sec_lsb_cnt)
	begin
		if falling_edge(clk_i) then
			bcd_countdown_s <= day_msb_cnt & day_lsb_cnt & hour_msb_cnt & hour_lsb_cnt &
						min_msb_cnt & min_lsb_cnt & sec_msb_cnt & sec_lsb_cnt;
			if (bcd_countdown_s=x"00000000") then
				zero_flag_o <= '1';
			else
				zero_flag_o <= '0';
			end if;
		end if;
	end process;
	bcd_countdown_o <= bcd_countdown_s;

	gen_clk_1Hz : if g_simulation=0 generate
	cmp_clk_1Hz : clock_divider 
		generic map(
			g_f_in  => 100000000,		-- Input frequency in [Hz]
			g_f_out => BCD_COUNTER_FREQUENCY	-- Output frequency in [Hz]
		)
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			clk_o	=> clk_1Hz_s
		);
	end generate;
	gen_clk_1Hz_sim : if g_simulation=1 generate
		cmp_clk_1Hz : clock_divider 
		generic map(
			g_f_in  => 100000000,		-- Input frequency in [Hz]
			g_f_out => BCD_COUNTER_FREQUENCY_SIM	-- Output frequency in [Hz]
		)
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			clk_o	=> clk_1Hz_s
		);
	end generate;

	--Rising edge clk_1Hz_s
	cmp_start_calib : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> clk_1Hz_s,
		s_o	=> tick1hz_s
	);
	
end Behavioral;

