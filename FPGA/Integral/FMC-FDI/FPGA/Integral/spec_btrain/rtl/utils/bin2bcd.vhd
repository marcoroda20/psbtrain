----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC/MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    10:17:01 09/10/2013 
-- Design Name: 
-- Module Name:    bin2bcd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity bin2bcd is
	port(
		clk_i			: in std_logic;
		reset_i		: in std_logic;

		bin_word_i	: in std_logic_vector(23 downto 0);
		start_conv_i: in std_logic;
		
		bcd_1_o		: out std_logic_vector(3 downto 0);
		bcd_2_o		: out std_logic_vector(3 downto 0);
		bcd_3_o		: out std_logic_vector(3 downto 0);
		bcd_4_o		: out std_logic_vector(3 downto 0);
		bcd_5_o		: out std_logic_vector(3 downto 0);
		bcd_6_o		: out std_logic_vector(3 downto 0);
		bcd_7_o		: out std_logic_vector(3 downto 0);
		bcd_8_o		: out std_logic_vector(3 downto 0);
		rdy_o		: out std_logic
	);
end bin2bcd;

architecture Behavioral of bin2bcd is

	constant N_BIT_BIN : integer := 24;
	constant N_BIT_BCD : integer := 32;
	constant ADD_3 : std_logic := '0';
	constant SHIFT_1 : std_logic := '1';
	
	--signal
	signal bin_word_s : std_logic_vector(N_BIT_BIN-1 downto 0);
	signal bcd_s : std_logic_vector(N_BIT_BCD-1 downto 0);
	signal start_conv_s : std_logic;

	signal count_bit_s : integer := N_BIT_BIN;
	signal add_shift_s : std_logic;
	
begin
		
	--BIN2BCD conv
	process (clk_i,reset_i,start_conv_i,count_bit_s,bin_word_i,start_conv_i,start_conv_s,bcd_s,add_shift_s)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				count_bit_s <= 0;
				start_conv_s <= '0';
				add_shift_s <= SHIFT_1;
				bin_word_s <= (others=>'0');
				bcd_s <= (others=>'0');
			else
				if (start_conv_i='1' and start_conv_s='0') then
					count_bit_s <= N_BIT_BIN;
					bin_word_s <= bin_word_i;
					bcd_s <= (others=>'0');
					start_conv_s <= '1';
					add_shift_s <= SHIFT_1;
				elsif (start_conv_s='1') then
					if (add_shift_s=ADD_3) then
						add_shift_s <= SHIFT_1;
						--8 Test: 
						--  if > 4 + 3
						if (count_bit_s>0) then
							if (bcd_s(3 downto 0)>4) then
								bcd_s(3 downto 0) <= bcd_s(3 downto 0)+3;
							end if;
							if (bcd_s(7 downto 4)>4) then
								bcd_s(7 downto 4) <= bcd_s(7 downto 4)+3;
							end if;
							if (bcd_s(11 downto 8)>4) then
								bcd_s(11 downto 8) <= bcd_s(11 downto 8)+3;
							end if;
							if (bcd_s(15 downto 12)>4) then
								bcd_s(15 downto 12) <= bcd_s(15 downto 12)+3;
							end if;
							if (bcd_s(19 downto 16)>4) then
								bcd_s(19 downto 16) <= bcd_s(19 downto 16)+3;
							end if;
							if (bcd_s(23 downto 20)>4) then
								bcd_s(23 downto 20) <= bcd_s(23 downto 20)+3;
							end if;
							if (bcd_s(27 downto 24)>4) then
								bcd_s(27 downto 24) <= bcd_s(27 downto 24)+3;
							end if;
							if (bcd_s(31 downto 28)>4) then
								bcd_s(31 downto 28) <= bcd_s(31 downto 28)+3;
							end if;
						end if;
					elsif (add_shift_s=SHIFT_1) then
						add_shift_s <= ADD_3;
						--shift left 1
						if (count_bit_s>0) then
							bcd_s <= bcd_s(30 downto 0) & bin_word_s(count_bit_s-1);
						end if;
						--next bit
						count_bit_s <= count_bit_s-1;
					end if;
					--end of conv
					if (count_bit_s=0) then
						start_conv_s <= '0';
					end if;
				end if;	
			end if;
		end if;
	end process;
	
	--Output latch
	process (clk_i,reset_i,bcd_s,start_conv_s)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				bcd_1_o <= (others=>'0');
				bcd_2_o <= (others=>'0');
				bcd_3_o <= (others=>'0');
				bcd_4_o <= (others=>'0');
				bcd_5_o <= (others=>'0');
				bcd_6_o <= (others=>'0');
				bcd_7_o <= (others=>'0');
				bcd_8_o <= (others=>'0');
				rdy_o <= '0';
			else
				if (start_conv_s='0') then
					bcd_1_o <= bcd_s(3 downto 0);
					bcd_2_o <= bcd_s(7 downto 4);
					bcd_3_o <= bcd_s(11 downto 8);
					bcd_4_o <= bcd_s(15 downto 12);
					bcd_5_o <= bcd_s(19 downto 16);
					bcd_6_o <= bcd_s(23 downto 20);
					bcd_7_o <= bcd_s(27 downto 24);
					bcd_8_o <= bcd_s(31 downto 28);
					rdy_o <= '1';
				else 
					rdy_o <= '0';
				end if;
			end if;
		end if;
	end process;

end Behavioral;

