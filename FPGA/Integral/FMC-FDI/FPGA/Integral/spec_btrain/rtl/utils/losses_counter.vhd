----------------------------------------------------------------------------------
-- Company:        CERN TE/MSC/MM 
-- Engineer:       Daniel Oberson
-- 
-- Create Date:    11:31:57 02/18/2014 
-- Design Name: 
-- Module Name:    losses_counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.utils_pkg.all;

entity losses_counter is
	generic (
		g_max_time_counter : integer:=62500000
	);
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		lost_i     : in std_logic;
		lost_rdy_i : in std_logic;
		
		losses_value_o : out std_logic_vector(31 downto 0)
	);
end losses_counter;

architecture Behavioral of losses_counter is

  --Type
  type losses_state_t is (
    IDLE,
    COUNTER
  );

  --Signal
  signal time_counter_s : integer := 0;
  signal zero_cnt_s     : std_logic;
  
  signal counter_s      : integer := 0;
  signal state_s        : losses_state_t;
  signal lost_rdy_s     : std_logic;
  signal timing_s       : std_logic;
  
begin

	--Counter of losses
	process (clk_i,reset_i,lost_rdy_s,lost_i,timing_s)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				state_s <= IDLE;
				losses_value_o <= (others=>'0');
				counter_s <= 0;
			else
				case state_s is
					when IDLE => 
						losses_value_o <= 
							std_logic_vector(to_signed(counter_s,losses_value_o'length));
						counter_s <= 0;
						state_s <= COUNTER;
					when COUNTER => 
						if ((lost_rdy_s='1') and (lost_i='1')) then
							counter_s <= counter_s + 1; 
						end if;
						if (timing_s='1') then
							state_s <= IDLE;
						end if;
				end case;
			end if;
		end if;
	end process;

	--timing counter
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				time_counter_s <= 0;
				zero_cnt_s <= '0';
			else
				--No counter if g_max_time_counter = 0
				if (g_max_time_counter/=0) then
					if (time_counter_s <= (g_max_time_counter-1)) then
						time_counter_s <= time_counter_s+1;
						zero_cnt_s <= '0';
					else
						time_counter_s <= 0;
						zero_cnt_s <= '1';
					end if;
				else
					time_counter_s <= 0;
					zero_cnt_s <= '0';
				end if;
			end if;
		end if;
	end process;
	
	cmp_timing_tick : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> zero_cnt_s,
			s_o	=> timing_s
		);
		
	cmp_losses_rdy : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> lost_rdy_i,
			s_o	=> lost_rdy_s
		);

end Behavioral;

