----------------------------------------------------------------------------------
-- Company:       CERN TE/MSC/MM
-- Engineer:      Daniel Oberson
-- 
-- Create Date:    19:52:47 31/10/2014 
-- Design Name: 
-- Module Name:    maintain - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

entity maintain is
	generic(
		g_factor : integer := 10
	);
	port (
		clk_i			: in std_logic;
		reset_i		: in std_logic;
				
		s_i	: in std_logic;
		s_o	: out std_logic
	);
end maintain;

architecture Behavioral of maintain is

	signal count_ctrl_s : integer;
	signal s_keep_s : std_logic;
	signal s_intern_s : std_logic;

begin

	--Maintain control signal to see them after decim
	process(clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				s_keep_s     <= '0';
				count_ctrl_s <= 0;
			else
				s_intern_s <= s_i;
				if ( (count_ctrl_s<g_factor) and (count_ctrl_s>0) ) then
					count_ctrl_s <= count_ctrl_s+1;
					s_o <= s_keep_s;
				elsif (s_i/=s_intern_s and count_ctrl_s=0) then
					count_ctrl_s <= count_ctrl_s+1;
					s_keep_s <= s_i;
				else
					count_ctrl_s <= 0;
					s_o <= s_i;
				end if;
			end if;
		end if;
	end process;
	
end Behavioral;

