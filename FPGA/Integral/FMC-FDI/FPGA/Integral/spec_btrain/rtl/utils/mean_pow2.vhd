----------------------------------------------------------------------------------
-- Company:        CERN TE/MSC/MM
-- Engineer:       Daniel Oberson
-- 
-- Create Date:    11:45:56 09/20/2013 
-- Design Name: 
-- Module Name:    mean_pow2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.math_real.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library work;
use work.utils_pkg.all;

entity mean_pow2 is
	generic(
		 NB_BIT : integer := 18;  -- default: 18 bits
		 POW2SAMPLE : integer := 7  -- default: 2^7 sample
	);
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		val_i     : in std_logic_vector(NB_BIT-1 downto 0);
		new_val_i : in std_logic;
		
		val_o   : out std_logic_vector(NB_BIT-1 downto 0);
		rdy_o   : out std_logic
	);
end mean_pow2;

architecture Behavioral of mean_pow2 is

	--Constant
	constant CST_ZERO : std_logic_vector(POW2SAMPLE-1 downto 0) := (others=>'0');
	constant CST_ONE : std_logic_vector(POW2SAMPLE-1 downto 0) := (others=>'1');
	constant CST_CNT_MAX : integer := 2**POW2SAMPLE;
	--Signal
	signal acc_val_s : std_logic_vector(NB_BIT+POW2SAMPLE-1 downto 0);
	signal count_s   : integer;
	signal new_val_s : std_logic;
	
	signal rdy_s : std_logic;
	signal rdy_1_s : std_logic;

begin

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				val_o <= (others=>'0');
				acc_val_s <= (others=>'0');
				rdy_o <= '0';
				count_s <= 0;
			else
				rdy_1_s <= rdy_s;
				if (new_val_s='1') then
					count_s <= count_s+1;
					if (val_i(val_i'length-1)='0') then
						acc_val_s <= acc_val_s+(CST_ZERO&val_i);
					else
						acc_val_s <= acc_val_s+(CST_ONE&val_i);
					end if;
				end if;
				if ((count_s>=CST_CNT_MAX)or(count_s<0)) then
					val_o <= acc_val_s(NB_BIT+POW2SAMPLE-1 downto POW2SAMPLE);
					acc_val_s <= (others=>'0');
					rdy_s <= '1';
					count_s <= 0;
				else
					rdy_s <= '0';
				end if;
				if (rdy_s='1' and rdy_1_s='0')or(rdy_s='0' and rdy_1_s='1') then
					rdy_o <= '1';
				else
					rdy_o <= '0';
				end if;
			end if;
		end if;
	end process;

	cmp_new_val : RisingDetect 
		port map(
			clk_i   => clk_i,
			reset_i => reset_i,
			
			s_i => new_val_i,
			s_o => new_val_s
		);
		
end Behavioral;

