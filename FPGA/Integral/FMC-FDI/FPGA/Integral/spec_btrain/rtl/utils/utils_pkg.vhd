----------------------------------------------------------------------------------
-- Company: 		CERN/TE-MS-MM
-- Engineer: 		Daniel Oberson
-- 
-- Description: Package with some utils core
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package utils_pkg is

	function log2_ceil(N : natural) return positive;

	component syncData2clocks 
		generic(
			g_DATA_LENGTH : integer := 32
		);
		port(
			clk_wr_i : in std_logic;
			clk_rd_i : in std_logic;
			reset_i  : in std_logic;
			
			wr_i   : in std_logic;
			data_i : in std_logic_vector(g_DATA_LENGTH-1 downto 0);
			rd_o   : out std_logic;
			data_o : out std_logic_vector(g_DATA_LENGTH-1 downto 0)
		);
	end component;

	component BCD_counter 
		generic(g_simulation : integer := 0);
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;

			bcd_time_i		: in std_logic_vector(31 downto 0);
			load_bcd_cnt_i	: in std_logic;
			en_bcd_cnt_i	: in std_logic;
			bcd_countdown_o: out std_logic_vector(31 downto 0);
			zero_flag_o		: out std_logic
		);
	end component;
	
	component bin2bcd 
		port(
			clk_i			: in std_logic;
			reset_i		: in std_logic;

			bin_word_i	: in std_logic_vector(23 downto 0);
			start_conv_i: in std_logic;
			
			bcd_1_o		: out std_logic_vector(3 downto 0);
			bcd_2_o		: out std_logic_vector(3 downto 0);
			bcd_3_o		: out std_logic_vector(3 downto 0);
			bcd_4_o		: out std_logic_vector(3 downto 0);
			bcd_5_o		: out std_logic_vector(3 downto 0);
			bcd_6_o		: out std_logic_vector(3 downto 0);
			bcd_7_o		: out std_logic_vector(3 downto 0);
			bcd_8_o		: out std_logic_vector(3 downto 0);
			rdy_o		: out std_logic
		);
	end component;

	component Mult36x36_signed 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;

			A_i	: in std_logic_vector(35 downto 0);
			B_i	: in std_logic_vector(35 downto 0);
					
			start_i	: in std_logic;

			result_o		: out std_logic_vector(71 downto 0);	
			result_rdy_o: out std_logic
		);
	end component;

	component Mult32x32_signed 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;

			A_i	: in std_logic_vector(31 downto 0);
			B_i	: in std_logic_vector(31 downto 0);
					
			start_i	: in std_logic;

			result_o		: out std_logic_vector(63 downto 0);	
			result_rdy_o: out std_logic
		);
	end component;

	component Mult32x32_2chan 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;

			A_i	: in std_logic_vector(31 downto 0);
			B_i	: in std_logic_vector(31 downto 0);
			
			C_i	: in std_logic_vector(31 downto 0);
			D_i	: in std_logic_vector(31 downto 0);
			
			start_i	: in std_logic;

			result_o		: out std_logic_vector(63 downto 0);	
			result_rdy_o: out std_logic
		);
	end component;

	component divider 
		generic (NB_BIT : integer := 32);
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			a_i	: in std_logic_vector(NB_BIT-1 downto 0);
			b_i	: in std_logic_vector(NB_BIT-1 downto 0);

			start_divider_i	: in std_logic;
			cmd_divider_i		: in std_logic;
			
			q_o	: out std_logic_vector(NB_BIT-1 downto 0);
			r_o	: out std_logic_vector(NB_BIT-1 downto 0);
			f_o	: out std_logic_vector(NB_BIT-1 downto 0);
			
			divide_by_0_o	: out std_logic;
			end_divider_o	: out std_logic
		);
	end component;

	component C0_time 
		port(
			clk_i 	: in std_logic;
			reset_i	: in std_logic;

			B_i   : in std_logic_vector(31 downto 0);
			Bdot_i: in std_logic_vector(31 downto 0);
			C0_i  : in std_logic;
			
			reg_c0_time_i        : in std_logic_vector(31 downto 0);
			reg_b_c0_t_o         : out std_logic_vector(31 downto 0);
			reg_max_b_c0_t_i     : in std_logic_vector(31 downto 0);
			reg_max_b_c0_t_load_i: in std_logic;
			reg_max_b_c0_t_o     : out std_logic_vector(31 downto 0);

			reg_bdot_c0_t_o         : out std_logic_vector(31 downto 0);
			reg_max_bdot_c0_t_i     : in std_logic_vector(31 downto 0);
			reg_max_bdot_c0_t_load_i: in std_logic;
			reg_max_bdot_c0_t_o     : out std_logic_vector(31 downto 0)
		);
	end component;

	component wb_addr_decoder
	  generic
		 (
			g_WINDOW_SIZE  : integer := 18;   -- Number of bits to address periph on the board (32-bit word address)
			g_WB_SLAVES_NB : integer := 2
			);
	  port
		 (
			---------------------------------------------------------
			-- GN4124 core clock and reset
			clk_i   : in std_logic;
			rst_n_i : in std_logic;

			---------------------------------------------------------
			-- wishbone master interface
			wbm_adr_i   : in  std_logic_vector(31 downto 0);  -- Address
			wbm_dat_i   : in  std_logic_vector(31 downto 0);  -- Data out
			wbm_sel_i   : in  std_logic_vector(3 downto 0);   -- Byte select
			wbm_stb_i   : in  std_logic;                      -- Strobe
			wbm_we_i    : in  std_logic;                      -- Write
			wbm_cyc_i   : in  std_logic;                      -- Cycle
			wbm_dat_o   : out std_logic_vector(31 downto 0);  -- Data in
			wbm_ack_o   : out std_logic;                      -- Acknowledge
			wbm_stall_o : out std_logic;                      -- Stall

			---------------------------------------------------------
			-- wishbone slaves interface
			wb_adr_o   : out std_logic_vector(31 downto 0);                     -- Address
			wb_dat_o   : out std_logic_vector(31 downto 0);                     -- Data out
			wb_sel_o   : out std_logic_vector(3 downto 0);                      -- Byte select
			wb_stb_o   : out std_logic;                                         -- Strobe
			wb_we_o    : out std_logic;                                         -- Write
			wb_cyc_o   : out std_logic_vector(g_WB_SLAVES_NB-1 downto 0);       -- Cycle
			wb_dat_i   : in  std_logic_vector((32*g_WB_SLAVES_NB)-1 downto 0);  -- Data in
			wb_ack_i   : in  std_logic_vector(g_WB_SLAVES_NB-1 downto 0);       -- Acknowledge
			wb_stall_i : in  std_logic_vector(g_WB_SLAVES_NB-1 downto 0)        -- Stall
			);
	end component;

	component RisingDetect 
		port (
			clk_i			: in std_logic;
			reset_i		: in std_logic;
			
			s_i	: in std_logic;
			s_o	: out std_logic
		);
	end component;

	component monostable 
		generic(
			g_n_clk : integer := 4
		);
		port (
			clk_i			: in std_logic;
			reset_i		: in std_logic;
			
			s_i	: in std_logic;
			s_o	: out std_logic
		);
	end component;

	component maintain is
		generic(
			g_factor : integer := 100000
		);
		port (
			clk_i			: in std_logic;
			reset_i		: in std_logic;
			
			s_i	: in std_logic;
			s_o	: out std_logic
		);
	end component;

	component DelayLine
		generic(
			 REG_DELAY_LONG : integer := 5  -- default: 5 bits
		);
		Port ( 
			line_i 		: in std_logic;
			reg_delay_i	: in std_logic_vector(REG_DELAY_LONG-1 downto 0);
			line_o 		: out std_logic
		);
	end component;

	component spec_reset_gen 
	  port (
		 clk_sys_i : in std_logic;

		 rst_pcie_n_a_i   : in std_logic;
		 rst_button_n_a_i : in std_logic;

		 rst_n_o : out std_logic
		 );
	end component;
	
	component clock_divider 
	generic (
		 g_f_in : integer := 100000000;	-- Input frequency in [Hz]
		 g_f_out : integer := 1				-- Output frequency in [Hz]
		 );	
		 port (
			clk_i			: in std_logic;
			reset_i		: in std_logic;

			clk_o	: out std_logic
		);
	end component;
	
	component CLK_DIV_EVEN 
		 Generic ( n: natural := 40 ); -- n : Divisor Rate must be an EVEN number
		 Port ( CLK_IN : in std_logic; -- CLK_IN must have a 50% duty cycle
				  RESET : in std_logic;  -- Active high asynchronous reset
				  DIV_CLK_OUT : out std_logic);
	end component;

end utils_pkg;

package body utils_pkg is

  -----------------------------------------------------------------------------
  -- Returns log of 2 of a natural number
  -----------------------------------------------------------------------------
  function log2_ceil(N : natural) return positive is
  begin
    if N <= 2 then
      return 1;
    elsif N mod 2 = 0 then
      return 1 + log2_ceil(N/2);
    else
      return 1 + log2_ceil((N+1)/2);
    end if;
  end;

end utils_pkg;
