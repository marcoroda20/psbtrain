--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package wr_btrain_pkg is

	constant ID_BkFrame : std_logic_vector(7 downto 0) := x"42";
	constant ID_ImFrame : std_logic_vector(7 downto 0) := x"49";

	-- Frame description
	type btrainFrameCtrl is record
		 bit_14to15       : std_logic_vector(1 downto 0); --not used
		 d_low_marker_bit : std_logic;                    --low marker Defocus associated to the sent value 
		 f_low_marker_bit : std_logic;                    --low marker Focus associated to the sent value 
		 zero_cycle_bit   : std_logic;                    --zero signal associated to the sent value 
		 C0_bit           : std_logic;                    --C0 signal associated to the sent value 
		 error_bit        : std_logic;                    --Error detected before sending bit
		 sim_eff_bit      : std_logic;                    --Simulation or Effectiv bit
		 frame_type       : std_logic_vector(7 downto 0); --type => 0x42 for B field frame
		                                             --     => 0x49 for Imain frame
	end record btrainFrameCtrl;

	type btrainFrameValue is record
		 B     : std_logic_vector(31 downto 0);
		 Bdot  : std_logic_vector(31 downto 0);
		 G     : std_logic_vector(31 downto 0);
		 S     : std_logic_vector(31 downto 0);
	end record btrainFrameValue;

	type btrainFrame is record
		 ctrl    : btrainFrameCtrl;
		 value   : btrainFrameValue;
	end record btrainFrame;

	type ImFrameValue is record
		 I     : std_logic_vector(31 downto 0);
	end record ImFrameValue;

	type ImFrame is record
		 ctrl    : btrainFrameCtrl;
		 value   : ImFrameValue;
	end record ImFrame;

	--Component for Btrain WR
	component WRBTrain 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			simeff_bit_i   : in std_logic;
			error_bit_i    : in std_logic;
			d_low_marker_i : in std_logic;
			f_low_marker_i : in std_logic;
			C0_i           : in std_logic;
			zero_cycle_i   : in std_logic;
			
			reg_sample_period_i : in std_logic_vector(25 downto 0);

			rx_data_i    : in std_logic_vector(15 downto 0);
			rx_valid_i   : in std_logic;
			rx_first_i   : in std_logic;
			rx_lost_i    : in std_logic;
			rx_last_i    : in std_logic;

			tx_B_frame_i : in btrainFrameValue;
			tx_I_frame_i : in ImFrameValue;
			frame_ID_i	 : in std_logic_vector(7 downto 0);
			I_value_o    : out std_logic_vector(31 downto 0);
			new_I_rdy_o  : out std_logic;

			B_value_o      : out std_logic_vector(31 downto 0);
			Bdot_value_o   : out std_logic_vector(31 downto 0);
			G_value_o      : out std_logic_vector(31 downto 0);
			S_value_o      : out std_logic_vector(31 downto 0);
			d_low_marker_o : out std_logic;
			f_low_marker_o : out std_logic;
			C0_o           : out std_logic;
			zero_cycle_o   : out std_logic;
			eff_sim_o      : out std_logic;
			new_B_rdy_o    : out std_logic;
		
			losses_o     : out std_logic;
			losses_rdy_o : out std_logic;

			force_send_i : in std_logic;
			send_cnt_i   : in std_logic;
			tx_sync_o    : out std_logic;
			
			tx_last_o    : out std_logic;
			tx_flush_o   : out std_logic;
			tx_dreq_i    : in std_logic;
			tx_data_o    : out std_logic_vector(15 downto 0);
			tx_valid_o   : out std_logic
		);
	end component;

	component txCtrl
		generic (
			g_data_width : integer:=16
		);
		port(
			clk_i    : in std_logic;
			reset_i  : in std_logic;

			-- Input frame for BtrainWR
			tx_B_frame_i   : in btrainFrameValue;
			tx_I_frame_i   : in ImFrameValue;
			tx_sync_i      : in std_logic;
			force_send_i   : in std_logic;
			tx_sent_o      : out std_logic;
			send_cnt_i     : in std_logic;
			simeff_bit_i   : in std_logic;
			error_bit_i    : in std_logic;
			d_low_marker_i : in std_logic;
			f_low_marker_i : in std_logic;
			C0_i           : in std_logic;
			zero_cycle_i   : in std_logic;
			frame_ID_i	   : in std_logic_vector(7 downto 0);
			
			-- Data word to be sent.
			tx_data_o  : out std_logic_vector(g_data_width-1 downto 0);
			-- 1 indicates that the tx_data_o contains a valid data word.
			tx_valid_o : out std_logic;
			-- Synchronous data request: if active, the user may send a data word in
			-- the following clock cycle.
			tx_dreq_i  : in std_logic;
			-- Used to indicate the last data word in a larger block of samples.
			tx_last_o  : out std_logic := '0';
			-- Flush command. When asserted, the streamer will immediatly send out all
			-- the data that is stored in its TX buffer, ignoring g_tx_timeout.
			tx_flush_o : out std_logic := '0'	
		);
	end component;

	component rxCtrl 
		generic (
			g_data_width : integer:=16
		);
		port(
			clk_i   : in std_logic; 
			reset_i : in std_logic;
			
			rx_data_i  : in std_logic_vector(g_data_width-1 downto 0);
			rx_valid_i : in std_logic;
			rx_first_i : in std_logic;
			rx_last_i  : in std_logic;
					
			rxframe_ctrl_o   : out btrainFrameCtrl;
			Irxframe_value_o : out ImFrameValue;
			Brxframe_value_o : out btrainFrameValue;
			rxframe_valid_o  : out std_logic
		);
	end component;

	component SynchroGen 
		port(
			clk_i		: in std_logic; 
			reset_i	: in std_logic;

			reg_sample_period_i : in std_logic_vector(25 downto 0);
			send_tick_o       : out std_logic
		);
	end component;

	component losses_counter 
		generic (
			g_max_time_counter : integer:=62500000
		);
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			lost_i     : in std_logic;
			lost_rdy_i : in std_logic;
			
			losses_value_o : out std_logic_vector(31 downto 0)
		);
	end component;


	function from_std_logic_vector(Value: in std_logic_vector(127 downto 0)) return btrainFrameValue;
	function to_std_logic_vector(Value: in btrainFrameValue) return std_logic_vector;

end wr_btrain_pkg;

package body wr_btrain_pkg is

	--Functions to convert btrainFrameValue to/from std_logic_vector
	function to_std_logic_vector(Value: in btrainFrameValue) return std_logic_vector is
	begin
		return (Value.B & Value.Bdot & Value.G & Value.S);
	end to_std_logic_vector;
	
	function from_std_logic_vector(Value: in std_logic_vector(127 downto 0)) return btrainFrameValue is
			variable val: btrainFrameValue;
	begin
		val.B    := Value(Value'length-1                                           downto Value'length-val.B'length);
		val.Bdot := Value(Value'length-val.B'length-1                              downto Value'length-val.B'length-val.Bdot'length);
		val.G    := Value(Value'length-val.B'length-val.Bdot'length-1              downto Value'length-val.B'length-val.Bdot'length-val.G'length);
		val.S    := Value(Value'length-val.B'length-val.Bdot'length-val.G'length-1 downto 0);
		return (val);
	end from_std_logic_vector;
	
	--Functions to convert ImFrameValue to/from std_logic_vector
	function to_std_logic_vector(Value: in ImFrameValue) return std_logic_vector is
	begin
		return (Value.I);
	end to_std_logic_vector;
	
	function from_std_logic_vector(Value: in std_logic_vector(31 downto 0)) return ImFrameValue is
			variable val: ImFrameValue;
	begin
		val.I    := Value(Value'length-1 downto Value'length-val.I'length);
		return (val);
	end from_std_logic_vector;
	
end wr_btrain_pkg;