target = "xilinx"
action = "synthesis"

syn_device = "xc6slx100t"
syn_grade = "-3"
syn_package = "fgg484"
syn_top = "spec_top_fmc_FDI"
syn_project = "spec_top_fmc_FDI.xise"

files = ["../spec_top_fmc_FDI.ucf"]

modules = { "local" : "../rtl" }
