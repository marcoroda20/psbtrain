--------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer:		Daniel Oberson
--
-- Create Date:   17:22:24 06/05/2012
-- Design Name:   
-- Module Name:   C:/B-Train/FPGA/Integral/BMaster/TB_BCalcul3_8.vhd
-- Project Name:  BMaster
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: BCalcul3_8
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments: To run the testbench with Modelsim type the command:
--                      do run_calc_3_8.do
--
--                      To generate reference and result file, use 
--                      ../../MatlabUtil/Generate3_8.m 
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.std_logic_unsigned.all; 

use std.textio.all;

use work.Config_BTrain.all;
 
ENTITY TB_BCalcul3_8 IS
END TB_BCalcul3_8;
 
ARCHITECTURE behavior OF TB_BCalcul3_8 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
	component BCalcul3_8 is
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;

			--Control input
			start_i	: in std_logic;
			init_Bk_i: in std_logic;
			C0_i		: in std_logic;

			--Data
			Bk_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			Vk_i			: in std_logic_vector(NB_BITS_ADC_VALUE-1 downto 0);
						
			--Output
			data_rdy_o	: out std_logic;
			BKp1_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0)
		);
	end component;
	
   --Inputs
   signal clk_sti 		: std_logic := '0';
   signal reset_sti 		: std_logic := '0';
   signal start_sti 		: std_logic := '0';
   signal init_Bk_sti 	: std_logic := '0';	
   signal Bk_sti 			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0) := (others => '0');
   signal Vk_sti 			: std_logic_vector(NB_BITS_ADC_VALUE-1 downto 0) := (others => '0');
   signal C0_sti			: std_logic;

 	--Outputs
   signal data_rdy_obs	: std_logic;
   signal BKp1_obs			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);

   -- Clock period definitions
   constant clk_i_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
	uut: BCalcul3_8
		port map(
			clk_i 		=> clk_sti,
			reset_i 		=> reset_sti,

			--Control input
			start_i 		=> start_sti,
			init_Bk_i	=> init_Bk_sti,
			C0_i			=> C0_sti,

			--Data
			Bk_i 			=> Bk_sti,
			Vk_i			=> Vk_sti,
					
			--Output
			data_rdy_o	=> data_rdy_obs,
			BKp1_o		=> BKp1_obs
		);


   -- Clock process definitions
   clk_i_process : process
   begin
		clk_sti <= '0';
		wait for clk_i_period/2;
		clk_sti <= '1';
		wait for clk_i_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
	
	--To read values in file
	file coeff_f			: text open READ_MODE is "coeff3_8.dat"; -- fichier des coefficients pour les stimuli
	variable coeff_line	: line;	
	variable coeff_v 		: integer;
	variable length_v		: integer;
	
	file ref_f			: text open READ_MODE is "ref3_8.dat"; -- references
	variable ref_line	: line;	
	variable ref_v 	: integer;
	
   begin		
		--Initialisation de la simulation
		report ">>>>>> Start simulation";
		report ">>>>>>    Reset";
      reset_sti		<='1';
		start_sti		<='0';
		init_Bk_sti		<='0';
		readline(coeff_f,coeff_line);--first line header : 'Bk Bsmooth corr_fact Vk Vo deltat'
		readline(coeff_f,coeff_line);--second line with value
		read(coeff_line,coeff_v);
		Bk_sti			<=conv_std_logic_vector(coeff_v,NB_BITS_B_VALUE);
		read(coeff_line,coeff_v);
		Vk_sti			<=conv_std_logic_vector(coeff_v,NB_BITS_ADC_VALUE);
		C0_sti			<= '0';
		
		wait for 5*clk_i_period;
		
		--End reset
      reset_sti	<='0';
		wait for 10*clk_i_period;
		report ">>>>>>    End reset";
		wait for 1*clk_i_period;
		wait for 3 ns;

		--Start calcul after initialization
--		init_Bk_sti	<='1';--init Bk with Bk_i input signal
		start_sti	<='1';
		wait for 1*clk_i_period;
		start_sti	<='0';
		report ">>>>>>    After initialization";
		wait for 1*clk_i_period;
		
		--Read result after initialization
		while (data_rdy_obs='0') loop
			wait for clk_i_period;
		end loop;
		wait for 3 ns;
		
		--Reset 
		report ">>>>>>    Reset";
      reset_sti	<='1';
 		wait for 5*clk_i_period;
		reset_sti	<='0';
		report ">>>>>>    End reset";
		wait for 1*clk_i_period;
		wait for 3 ns;
		
		--Start integral
		readline(coeff_f,coeff_line);--third line : 'Signal'
		readline(coeff_f,coeff_line);--fourth line, length of signal
		read(coeff_line,length_v);
		
		for n in 0 to length_v-1 loop
			readline(coeff_f,coeff_line);--after the fourth line, values of signal
			read(coeff_line,coeff_v);
			Vk_sti <= conv_std_logic_vector(coeff_v,NB_BITS_ADC_VALUE);
			start_sti	<='1';
			wait for 1*clk_i_period;
			start_sti	<='0';
			wait for 9*clk_i_period;
		end loop;
	
      --End of test-bench
		wait for 50*clk_i_period;
		report ">>>>>> End of simulation";

      wait;
		
   end process;

	result_process : process

	--To write results of test-bench
	variable result_line : line;
	variable result_v 	: integer;
	file result_f 			: text open WRITE_MODE is "result3_8.dat"; 
	
	begin
		wait for 18*clk_i_period;
		while (true) loop
			while (data_rdy_obs='1') loop
				wait for 1 ns;
			end loop;
			while (data_rdy_obs='0') loop
				wait for 1 ns;
			end loop;
			result_v:=conv_integer(ieee.STD_LOGIC_ARITH.unsigned(BKp1_obs));
			write(result_line,result_v);
			writeline(result_f,result_line);
		end loop;
	end process;

END;
