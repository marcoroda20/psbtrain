--------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer:		Daniel Oberson
--
-- Create Date:   19:22:24 16/05/2013
-- Design Name:   
-- Module Name:   C:\PSBTrain\FPGA\Integral\spec_btrain\testbench\TB_BTrain.vhd
-- Project Name:  spec_btrain
-- Target Device:  
-- Tool versions:  
-- Description:   Testbench for calibration
-- Additional Comments: To run the testbench with Modelsim type the command:
--                      do run_calib.do
--
--------------------------------------------------------------------------------
LIBRARY ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.std_logic_unsigned.all; 

use std.textio.all;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.adc_dac_pkg.all;
use work.calib_pkg.all;
use work.dma_pkg.all;
--use work.lcd_pkg.all;
use work.utils_pkg.all;
--use work.wr_btrain_pkg.all;
--use work.sdb_meta_pkg.all;
--use work.gn4124_core_pkg.all;
--use work.ddr3_ctrl_pkg.all;
--use work.gencores_pkg.all;
--use work.wrcore_pkg.all;
--use work.wr_fabric_pkg.all;
--use work.wr_xilinx_pkg.all;
use work.wishbone_pkg.all;
--use work.streamers_pkg.all;
--use work.streamers_pkg.all; 

ENTITY TB_BTrain IS
END TB_BTrain;
 
ARCHITECTURE behavior OF TB_BTrain IS 
 	
   --Inputs
	signal clk_sti 		: std_logic := '0';
	signal clk_2x_sti		: std_logic := '0';
	signal clk_cnv_sti	: std_logic := '0';
	signal clk_16MHz_sti	: std_logic := '0';
	signal reset_sti 		: std_logic := '0';

	--ADC F
	signal sdo1_F_s	: std_logic;
	signal sdo2_F_s	: std_logic;
	signal sdo3_F_s	: std_logic;
	signal sdo4_F_s	: std_logic;
	signal sdo5_F_s	: std_logic;
	signal sdo6_F_s	: std_logic;
	signal cnt_bit_adc_F_s			: integer;
	signal cnt_start_bit_adc_F_s	: integer;
	--ADC D
	signal sdo1_D_s	: std_logic;
	signal sdo2_D_s	: std_logic;
	signal sdo3_D_s	: std_logic;
	signal sdo4_D_s	: std_logic;
	signal sdo5_D_s	: std_logic;
	signal sdo6_D_s	: std_logic;
	signal cnt_bit_adc_D_s			: integer;
	signal cnt_start_bit_adc_D_s	: integer;
	
	signal simulated_offset_sti	: std_logic_vector(17 downto 0):="00" & x"0011";--"11" & x"fff9";--"00" & x"0007";
	signal signal_input_adc_sti	: std_logic_vector(17 downto 0):="11" & x"ffff";--"01" & x"2345";
	
 	--Outputs			
	signal adc_F_obs 		: std_logic_vector(17 downto 0);
	signal adc_drdy_F_obs: std_logic;
	signal adc_D_obs 		: std_logic_vector(17 downto 0);
	signal adc_drdy_D_obs: std_logic;

	signal adc_cnv_obs : std_logic;

	signal adc_sdi_F_obs : std_logic;
	signal adc_sdo_F_sti : std_logic;
	signal adc_sck_F_obs : std_logic;
	signal adc_sdi_D_obs : std_logic;
	signal adc_sdo_D_sti : std_logic;
	signal adc_sck_D_obs : std_logic;

	signal DAC_data_obs		: std_logic_vector (NB_BITS_DAC_VALUE-1 downto 0);

	--Signal BTrain core
	signal C0_sti			: std_logic:='0';
	signal zero_cycle_sti: std_logic;
		
	signal switch_open1_obs	: std_logic;
	signal switch_gain1_obs	: std_logic;
	signal switch_off1_obs	: std_logic;
			
	signal switch_open2_obs	: std_logic;
	signal switch_gain2_obs	: std_logic;
	signal switch_off2_obs	: std_logic;
			
	signal error_status_s	: std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
	signal calib_status_s	: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);

	signal time_calib_reg_sti	: std_logic_vector(31 downto 0); --1 sec resolution
	signal reg_t_next_calib_obs: std_logic_vector(31 downto 0); --1 sec resolution

	signal reg_calib_delay_sti	: std_logic_vector(31 downto 0);

	signal val_pos_F_obs	: std_logic_vector(31 downto 0);
	signal val_neg_F_obs	: std_logic_vector(31 downto 0);
	signal val_pos_D_obs	: std_logic_vector(31 downto 0);
	signal val_neg_D_obs	: std_logic_vector(31 downto 0);

	signal force_cal_obs       : std_logic;
	signal force_cal_sti       : std_logic;
	signal force_cal_load_obs  : std_logic;

	signal ADC_ref_sti			: std_logic_vector(19 downto 0);
	signal nb_of_meas_reg_sti	: std_logic_vector(31 downto 0);

	signal gain_F_obs			: std_logic_vector(31 downto 0);
	signal offset_calc_F_obs: std_logic_vector(31 downto 0);
	signal offset_meas_F_obs: std_logic_vector(31 downto 0);
	signal gain_D_obs			: std_logic_vector(31 downto 0);
	signal offset_calc_D_obs: std_logic_vector(31 downto 0);
	signal offset_meas_D_obs: std_logic_vector(31 downto 0);

	signal reg_val_cfg_sel_integral_sti : std_logic_vector(1 downto 0);
	signal reg_val_sel_sti					: std_logic_vector(3 downto 0);
	signal reg_val_sel_range_sti			: std_logic_vector(3 downto 0);

	signal Bsmooth_F_sti		: std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0);
	signal corr_fact_F_sti	: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal Vo_F_sti			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);

	signal Bsmooth_D_sti		: std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0);
	signal corr_fact_D_sti	: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal Vo_D_sti			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);

	signal Bk_obs				: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal B_data_rdy_obs	: std_logic;
	signal Bdot_obs			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal Bdot_data_rdy_obs: std_logic;

	signal reg_f_b_coeff_sti	: std_logic_vector(31 downto 0);
	signal reg_d_b_coeff_sti	: std_logic_vector(31 downto 0);
	signal reg_f_bdot_coeff_sti: std_logic_vector(31 downto 0);
	signal reg_d_bdot_coeff_sti: std_logic_vector(31 downto 0);

	signal reg_adc_data_f_obs	: std_logic_vector(17 downto 0);
	signal adc_data_rdy_f_obs	: std_logic;
	signal reg_adc_data_d_obs	: std_logic_vector(17 downto 0);
	signal adc_data_rdy_d_obs	: std_logic;
			
	signal reg_adc_data_corr_f_obs	: std_logic_vector(17 downto 0);
	signal adc_corr_rdy_f_obs			: std_logic;
	signal reg_adc_data_corr_d_obs	: std_logic_vector(17 downto 0);
	signal adc_corr_rdy_d_obs	 		: std_logic;

	signal reg_dac_bus_sti				: std_logic_vector(23 downto 0);
	signal reg_val_sel_fpga_bus_sti	: std_logic;
	signal reg_dac_read_back_obs		: std_logic_vector(19 downto 0);

  signal ddr_test_en_sti  : std_logic;
  
	signal mem_range_obs		  : std_logic;
	signal acqu_end_obs     : std_logic_vector(1 downto 0);

	-- DDR wishbone interface
	signal wb_ddr_adr_obs   : std_logic_vector(31 downto 0);
	signal wb_ddr_dat_obs   : std_logic_vector(63 downto 0);
	signal wb_ddr_sel_obs   : std_logic_vector(7 downto 0);
	signal wb_ddr_stb_obs   : std_logic;
	signal wb_ddr_we_obs    : std_logic;
	signal wb_ddr_cyc_obs   : std_logic;
	signal wb_ddr_ack_sti   : std_logic := '1';
	signal wb_ddr_stall_sti : std_logic := '0';

	signal wr_B_val_sti     : std_logic_vector(31 downto 0);
	signal wr_Bdot_val_sti  : std_logic_vector(31 downto 0);
	signal wr_I_val_sti     : std_logic_vector(31 downto 0);
	signal wr_drdy_sti      : std_logic;


	--Signal
	signal start_cal_s		: std_logic;

	signal adc_F_s				: std_logic_vector(17 downto 0);
	signal adc_D_s				: std_logic_vector(17 downto 0);
	
	signal calib_DAC_wr_s		: std_logic_vector (23 downto 0);
	signal calib_DAC_rd_s		: std_logic_vector (23 downto 0);
	signal calib_end_dac_rd_s	: std_logic;
	signal calib_end_dac_wr_s	: std_logic;

	signal DAC_wr_s 		: std_logic_vector (23 downto 0);
	signal DAC_rd_s		: std_logic_vector (23 downto 0);
	signal end_dac_rd_s	: std_logic;
	signal end_dac_wr_s	: std_logic;

	signal SDI_s		: std_logic := '0';
	signal SDO_s		: std_logic;
	signal SCK_s		: std_logic;
	signal N_LDAC_s	: std_logic;
	signal N_SYNC_s	: std_logic := '1';
	signal DAC_spi_s      : std_logic_vector (23 downto 0) := (others=>'0');
	signal word_dac_spi_s : std_logic_vector (23 downto 0) := (others=>'0');
	signal dac_reg_out_s : std_logic_vector (23 downto 0) := (others=>'0');
	signal dac_control_s	 : std_logic_vector (23 downto 0) := (others=>'0');
	signal dac_software_s : std_logic_vector (23 downto 0) := (others=>'0');

	signal gain_F_s			: std_logic_vector(31 downto 0);
	signal offset_calc_F_s	: std_logic_vector(31 downto 0);
	signal offset_meas_F_s	: std_logic_vector(31 downto 0);
	signal gain_D_s			: std_logic_vector(31 downto 0);
	signal offset_calc_D_s	: std_logic_vector(31 downto 0);
	signal offset_meas_D_s	: std_logic_vector(31 downto 0);

	signal n_s : integer := 23;
	signal dac_read_back_s : std_logic := '0';
	
	--Inputs
	signal start_sti 		: std_logic := '0';
	signal init_Bk_sti 	: std_logic := '0';	
	signal Bk_sti 			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0) := (others => '0');
	signal Vk_sti 			: std_logic_vector(NB_BITS_ADC_VALUE-1 downto 0) := (others => '0');

	--Outputs
	signal data_rdy_obs	: std_logic;
	signal BKp1_obs			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
   
   
   -- Clock period definitions
   constant clk_i_period : time := 10 ns;
 
BEGIN

	-- Instantiate the Unit Under Test (UUT)
	cmp_BTrain : fmc_adc2M18b2ch
		generic map(g_simulation=>1)
		port map(
			clk_i			=> clk_sti,
			clk_10_i		=> clk_2x_sti,
			clk_adc_i		=> clk_2x_sti,

		reset_n_hw_i  => '1',
		reset_i		     => reset_sti,
		reset_hw_i    => '0',
		reset_sw_o    => open,
		en_hw_reset_o => open,
		init_done_i   => '1',


		--Input information signals
		F_low_marker_i => '0',
		F_high_marker_i=> '0',
		D_low_marker_i => '0',
		D_high_marker_i=> '0',
			C0_i				=> C0_sti,
			zero_cycle_i	=> zero_cycle_sti,
		
			--Switch
			switch_open1_o	=> switch_open1_obs,
			switch_gain1_o	=> switch_gain1_obs,
			switch_off1_o	=> switch_off1_obs,
			
			switch_open2_o	=> switch_open2_obs,
			switch_gain2_o	=> switch_gain2_obs,
			switch_off2_o	=> switch_off2_obs,
			
			error_status_o	=> error_status_s,
			calib_status_o	=> calib_status_s,

			--ADC
			CNV_o		=> adc_cnv_obs,
			
			SDI_F_o	=> adc_sdi_F_obs,
			SDO_F_i	=> adc_sdo_F_sti,
			SCK_F_o	=> adc_sck_F_obs,

			SDI_D_o	=> adc_sdi_D_obs,
			SDO_D_i	=> adc_sdo_D_sti,
			SCK_D_o	=> adc_sck_D_obs,
			

		-- CSR wishbone interface
		wb_csr_adr_i   => (others=>'0'),
		wb_csr_dat_i   => (others=>'0'),
		wb_csr_dat_o   => open,
		wb_csr_cyc_i   => '0',
		wb_csr_sel_i   => (others=>'0'),
		wb_csr_stb_i   => '0',
		wb_csr_we_i    => '0',
		wb_csr_ack_o   => open,
		wb_csr_stall_o => open,


		--Register	
		status_simeff_i          => '1',--effectiv
		reg_status_opsp_cmd_o    => open,
		reg_status_opsp_status_i => '1',--op
		
		reg_dio_input_i => (others=>'0'),
		reg_dio_dir_i   => "1110000000",
		reg_dio_cfg_o   => open,

		wr_B_val_i     => (others=>'0'),
		wr_Bdot_val_i  => (others=>'0'),
		wr_I_val_i     => (others=>'0'),
		wr_drdy_i      => '0',
		wr_C0_i        => '0',
		wr_zero_cycle_i=> '0',
		wr_eff_sim_i   => '0',

		Bk_o           => open,
		B_data_rdy_o   => open,
		Bdot_o			=> open,
		Bdot_data_rdy_o=> open,

		--WR-register
		wr_sample_period_o   => open,
		wr_ctrl_rx_latency_i => (others=>'0'),
		wr_ctrl_lost_frame_i => '0',
		wr_losses_i          => (others=>'0'),
		wr_ctrl_tm_valid_i   => '0',
		wr_lab_test_o        => open,
		wr_frame_type_o      => open,
		wr_send_ctrl_o       => open,

			--DAC			
			SDI_i		=> SDI_s,
			SDO_o		=> SDO_s,
			SCK_o		=> SCK_s,
			
	--		DMA wishbone
			wb_ddr_clk_i   => clk_sti,
			wb_ddr_adr_o   => wb_ddr_adr_obs,
			wb_ddr_dat_o   => wb_ddr_dat_obs,
			wb_ddr_sel_o   => wb_ddr_sel_obs,
			wb_ddr_stb_o   => wb_ddr_stb_obs,
			wb_ddr_we_o    => wb_ddr_we_obs,
			wb_ddr_cyc_o   => wb_ddr_cyc_obs,
			wb_ddr_ack_i   => wb_ddr_ack_sti,
			wb_ddr_stall_i => wb_ddr_stall_sti,

			--ddr_test_en_i => ddr_test_en_sti,
			
			mem_range_o		=> mem_range_obs,
			acqu_end_o   => acqu_end_obs,

			DAC_n_LDAC_o	=> N_LDAC_s,
			DAC_n_SYNC_o	=> N_SYNC_s,
			DAC_n_RESET_o	=> open,
			DAC_n_CLR_o		=> open,

		--LCD display I/O
		LCD_SDOUT	  => open,
		LCD_NCS		   => open,
		LCD_CLK		   => open,
		LCD_REG_SEL => open,
		LCD_ENABLE1 => open,
		LCD_ENABLE2 => open,
		NKEY1       => '0',
		NKEY2       => '0',
		NKEY3       => '0',
		NKEY4       => '0',

		dac_extern_clk_o => open,
		dac_extern_cs_o  => open,
		dac_extern_sdo_o => open

		);		  

   -- Clock process definitions
   clk_i_process :process
   begin
		clk_sti <= '0';
		wait for clk_i_period/2;
		clk_sti <= '1';
		wait for clk_i_period/2;
   end process;
	
   clk_2x_i_process :process
   begin
		clk_2x_sti <= '0';
		wait for clk_i_period/4;
		clk_2x_sti <= '1';
		wait for clk_i_period/4;
   end process;
 
   clk_cnv_process :process
   begin
		clk_cnv_sti <= '0';
		wait for 50*clk_i_period;
		clk_cnv_sti <= '1';
		wait for 50*clk_i_period;
   end process;
 
   clk_16MHz_process :process
   begin
		clk_16MHz_sti <= '0';
		wait for 6.25*clk_i_period;
		clk_16MHz_sti <= '1';
		wait for 6.25*clk_i_period;
   end process;

   -- ADC F process simulation
   ADC_F_process : process (adc_sck_F_obs,clk_sti,reset_sti,adc_data_rdy_f_obs,
						adc_F_s,cnt_bit_adc_F_s,cnt_start_bit_adc_F_s,DAC_spi_s,calib_status_s)
   begin
		if (reset_sti='1') then
			sdo6_F_s <= '0';
			cnt_bit_adc_F_s <= NB_BITS_ADC_VALUE-1;
			cnt_start_bit_adc_F_s <= 13;
			adc_F_s	<= (others=>'0');
		elsif (adc_sck_F_obs='1' and adc_sck_F_obs'event) then
			if (cnt_start_bit_adc_F_s=0) then --dummy first bit
				sdo6_F_s <= adc_F_s(cnt_bit_adc_F_s);
				if (cnt_bit_adc_F_s/=0) then
					cnt_bit_adc_F_s <= cnt_bit_adc_F_s-1;
				else
					cnt_bit_adc_F_s <= NB_BITS_ADC_VALUE-1;
				end if;
			else
				cnt_start_bit_adc_F_s <= cnt_start_bit_adc_F_s-1;
			end if;
		elsif (adc_data_rdy_f_obs='1' and adc_data_rdy_f_obs'event) then
			if (calib_status_s=OFFSET_st or calib_status_s=SWITCH_AS_OFFSET_st) then
				adc_F_s <= simulated_offset_sti;
			elsif (calib_status_s=MEAS_POS_st or calib_status_s=MEAS_NEG_st) then
				if (DAC_spi_s(23 downto 20)=AD5791_WR_VAL) then
					adc_F_s <= DAC_spi_s(19 downto 2) + simulated_offset_sti;
				end if;
			else
				adc_F_s <= signal_input_adc_sti;
			end if;
		end if;
   end process;
	
	process (clk_2x_sti,reset_sti,sdo1_F_s,sdo2_F_s,sdo3_F_s,sdo4_F_s,sdo5_F_s,sdo6_F_s)
	begin
 		--latency to simulate Hardware ADC
		if (reset_sti='1') then
			sdo5_F_s <= '0';
			sdo4_F_s <= '0';
			sdo3_F_s <= '0';
			sdo2_F_s <= '0';
			sdo1_F_s <= '0';
			adc_sdo_F_sti <= '0';
		elsif (clk_2x_sti='1' and clk_2x_sti'event) then
			sdo5_F_s <= sdo6_F_s;
			sdo4_F_s <= sdo5_F_s;
			sdo3_F_s <= sdo4_F_s;
			sdo2_F_s <= sdo3_F_s;
			sdo1_F_s <= sdo2_F_s;
			adc_sdo_F_sti <= sdo1_F_s;
		end if;
	end process;

   -- ADC D process simulation
   ADC_D_process : process (adc_sck_D_obs,clk_sti,reset_sti,adc_data_rdy_d_obs,
						adc_D_s,cnt_bit_adc_D_s,cnt_start_bit_adc_D_s,DAC_spi_s,calib_status_s)
   begin
		if (reset_sti='1') then
			sdo6_D_s <= '0';
			cnt_bit_adc_D_s <= NB_BITS_ADC_VALUE-1;
			cnt_start_bit_adc_D_s <= 13;
			adc_D_s	<= (others=>'0');
		elsif (adc_sck_D_obs='1' and adc_sck_D_obs'event) then
			if (cnt_start_bit_adc_D_s=0) then --dummy first bit
				sdo6_D_s <= adc_D_s(cnt_bit_adc_D_s);
				if (cnt_bit_adc_D_s/=0) then
					cnt_bit_adc_D_s <= cnt_bit_adc_D_s-1;
				else
					cnt_bit_adc_D_s <= NB_BITS_ADC_VALUE-1;
				end if;
			else
				cnt_start_bit_adc_D_s <= cnt_start_bit_adc_D_s-1;
			end if;
		elsif (adc_data_rdy_d_obs='1' and adc_data_rdy_d_obs'event) then
			if (calib_status_s=OFFSET_st or calib_status_s=SWITCH_AS_OFFSET_st) then
				adc_D_s <= simulated_offset_sti;
			elsif (calib_status_s=MEAS_POS_st or calib_status_s=MEAS_NEG_st) then
				if (DAC_spi_s(23 downto 20)=AD5791_WR_VAL) then
					adc_D_s <= DAC_spi_s(19 downto 2) + simulated_offset_sti;
				end if;
			else
				adc_D_s <= signal_input_adc_sti;
			end if;
		end if;
   end process;
	
	process (clk_2x_sti,reset_sti,sdo1_D_s,sdo2_D_s,sdo3_D_s,sdo4_D_s,sdo5_D_s,sdo6_D_s)
	begin
 		--latency to simulate Hardware ADC
		if (reset_sti='1') then
			sdo5_D_s <= '0';
			sdo4_D_s <= '0';
			sdo3_D_s <= '0';
			sdo2_D_s <= '0';
			sdo1_D_s <= '0';
			adc_sdo_D_sti <= '0';
		elsif (clk_2x_sti='1' and clk_2x_sti'event) then
			sdo5_D_s <= sdo6_D_s;
			sdo4_D_s <= sdo5_D_s;
			sdo3_D_s <= sdo4_D_s;
			sdo2_D_s <= sdo3_D_s;
			sdo1_D_s <= sdo2_D_s;
			adc_sdo_D_sti <= sdo1_D_s;
		end if;
	end process;

--	DAC_calib_process 
	process (clk_sti,N_SYNC_s,SCK_s,SDO_s,word_dac_spi_s,DAC_spi_s,n_s,dac_read_back_s,
				dac_reg_out_s,dac_control_s,dac_software_s)
	begin
		if (SCK_s'event and SCK_s='1') then
			if (N_SYNC_s='0') then
				if (dac_read_back_s='0') then
					word_dac_spi_s <= word_dac_spi_s (22 downto 0) & SDO_s;
				else
					case word_dac_spi_s(23 downto 20) is 
--						when WR_DAC_REGISTER =>
--							SDI_s <= dac_reg_out_s(n_s);
--						when WR_DAC_CONTROL =>
--							SDI_s <= dac_control_s(n_s);
--						when WR_DAC_CLR_CODE =>
--							SDI_s <= dac_reg_out_s(n_s);
--						when WR_DAC_SOFTWARE =>
--							SDI_s <= dac_software_s(n_s);
						when RD_DAC_REGISTER =>
							SDI_s <= dac_reg_out_s(n_s);
						when RD_DAC_CONTROL =>
							SDI_s <= dac_control_s(n_s);						
						when RD_DAC_CLR_CODE =>
							SDI_s <= dac_control_s(n_s);						
						when others =>	
					end case;
--					SDI_s <= word_dac_spi_s(n_s);
					n_s <= n_s - 1;
				end if;
				if (n_s=0) then
					dac_read_back_s <= '0';
					word_dac_spi_s <= (others=>'0');
				end if;
			else
				case word_dac_spi_s(23 downto 20) is 
					when WR_DAC_REGISTER =>
						dac_reg_out_s <= word_dac_spi_s;
					when WR_DAC_CONTROL =>
						dac_control_s <= word_dac_spi_s;
					when WR_DAC_CLR_CODE =>
						dac_reg_out_s <= word_dac_spi_s;						
					when WR_DAC_SOFTWARE =>
						dac_software_s <= word_dac_spi_s;
					when others =>	
				end case;
				n_s <= 23;
			end if;
		end if;

		if (clk_sti'event and clk_sti='1') then
			if (N_SYNC_s='1' and dac_read_back_s='0') then
				if (word_dac_spi_s(23 downto 20)=WR_DAC_REGISTER or word_dac_spi_s(23 downto 20)=WR_DAC_CONTROL or
					 word_dac_spi_s(23 downto 20)=WR_DAC_CLR_CODE or word_dac_spi_s(23 downto 20)=WR_DAC_SOFTWARE) then
					DAC_spi_s <= word_dac_spi_s;
					dac_read_back_s <= '0';
				elsif (word_dac_spi_s(23 downto 20)=RD_DAC_REGISTER or word_dac_spi_s(23 downto 20)=RD_DAC_CONTROL or
						 word_dac_spi_s(23 downto 20)=RD_DAC_CLR_CODE) then 
					dac_read_back_s <= '1';
				end if;
			end if;
		end if;
	end process;
	
   -- Stimulus process
   stim_proc: process

	--To read values in file
	file coeff_f			: text open READ_MODE is "coeff3_8.dat"; -- fichier des coefficients pour les stimuli
	variable coeff_line	: line;	
	variable coeff_v 		: integer;
	variable length_v		: integer;
	
	file ref_f			: text open READ_MODE is "ref3_8.dat"; -- references
	variable ref_line	: line;	
	variable ref_v 	: integer;
	
   begin		
		--Initialisation de la simulation
		report ">>>>>> Start simulation";
		report ">>>>>>    Init stimulation and Reset";
      reset_sti				<= '1';

		zero_cycle_sti       <= '0';
		time_calib_reg_sti	<= x"01000005";
		force_cal_sti        <= '0';
		ADC_ref_sti         	<= "01" & x"8000" & "00";
		
    ddr_test_en_sti <= '1';

		reg_calib_delay_sti	<= (others=>'0');--x"00000003";
		nb_of_meas_reg_sti   <= x"00000014";--(others=>'0');--x"00000005";	

		reg_val_cfg_sel_integral_sti <= (others=>'0');
		reg_val_sel_sti              <= "0010";
		reg_val_sel_range_sti        <= (others=>'0');

		Bsmooth_F_sti     <= x"00000050";
		corr_fact_F_sti   <= x"80000000";
		Vo_F_sti          <= x"00000000";
		Bsmooth_D_sti     <= x"00000050";
		corr_fact_D_sti   <= x"80000000";
		Vo_D_sti          <= x"00000000";

		reg_f_b_coeff_sti        <= x"80000000";
		reg_d_b_coeff_sti        <= x"80000000";
		reg_f_bdot_coeff_sti     <= x"80000000";
		reg_d_bdot_coeff_sti     <= x"80000000";
		reg_dac_bus_sti          <= x"000000";
		reg_val_sel_fpga_bus_sti <= '0';

		wr_B_val_sti     <= x"12345678";
		wr_Bdot_val_sti  <= x"87654321";
		wr_I_val_sti     <= x"11111111";
		wr_drdy_sti      <= '1';

		wait for 8*clk_i_period;

		--End reset
      reset_sti	<='0';
		wait for 10*clk_i_period;
		wait for 1*clk_i_period;
		wait for 1 ns;
		report ">>>>>>    End init and reset";

		wait for 6 us;

		--Start calibration
		time_calib_reg_sti	<= x"00000100";

		wait for 15 us;
		zero_cycle_sti			<= '1';
		wait for 3 us;
		zero_cycle_sti			<= '0';
		
		wait for 15 us;
		zero_cycle_sti			<= '1';
		wait for 3 us;
		zero_cycle_sti			<= '0';
		
		wait for 15 us;
		zero_cycle_sti			<= '1';
		wait for 3 us;
		zero_cycle_sti			<= '0';
		
		wait for 15 us;
		zero_cycle_sti			<= '1';
		wait for 3 us;
		zero_cycle_sti			<= '0';
		
		wait for 15 us;
		zero_cycle_sti			<= '1';
		wait for 3 us;
		zero_cycle_sti			<= '0';


		--Initialisation de la simulation
		report ">>>>>> Start simulation";
		report ">>>>>>    Reset";
      reset_sti		<='1';
		start_sti		<='0';
		init_Bk_sti		<='0';
		readline(coeff_f,coeff_line);--first line header : 'Bk Bsmooth corr_fact Vk Vo deltat'
		readline(coeff_f,coeff_line);--second line with value
		read(coeff_line,coeff_v);
		Bk_sti			<=conv_std_logic_vector(coeff_v,NB_BITS_B_VALUE);
		read(coeff_line,coeff_v);
		Vk_sti			<=conv_std_logic_vector(coeff_v,NB_BITS_ADC_VALUE);
		C0_sti			<= '0';
		
		wait for 5*clk_i_period;
		
		--End reset
      reset_sti	<='0';
		wait for 10*clk_i_period;
		report ">>>>>>    End reset";
		wait for 1*clk_i_period;
		wait for 3 ns;

		--Start calcul after initialization
--		init_Bk_sti	<='1';--init Bk with Bk_i input signal
		start_sti	<='1';
		wait for 1*clk_i_period;
		start_sti	<='0';
		report ">>>>>>    After initialization";
		wait for 1*clk_i_period;
		
		--Read result after initialization
		while (data_rdy_obs='0') loop
			wait for clk_i_period;
		end loop;
		wait for 3 ns;
		
		--Reset 
		report ">>>>>>    Reset";
      reset_sti	<='1';
 		wait for 5*clk_i_period;
		reset_sti	<='0';
		report ">>>>>>    End reset";
		wait for 1*clk_i_period;
		wait for 3 ns;
		
		--Start integral
		readline(coeff_f,coeff_line);--third line : 'Signal'
		readline(coeff_f,coeff_line);--fourth line, length of signal
		read(coeff_line,length_v);
		
		for n in 0 to length_v-1 loop
			readline(coeff_f,coeff_line);--after the fourth line, values of signal
			read(coeff_line,coeff_v);
			Vk_sti <= conv_std_logic_vector(coeff_v,NB_BITS_ADC_VALUE);
			start_sti	<='1';
			wait for 1*clk_i_period;
			start_sti	<='0';
			wait for 9*clk_i_period;
		end loop;
	
      --End of test-bench
		wait for 50*clk_i_period;
		report ">>>>>> End of simulation";

		
		wait;
		
	end process;

END;
