----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    08:25:31 06/04/2013 
-- Design Name: 
-- Module Name:    TB_BTrain_integral - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: To run the testbench with Modelsim type the command:
--                      do run_calc_wot.do
--
--                      To generate reference and result file, use 
--                      ../../MatlabUtil/Generate.m 
--
----------------------------------------------------------------------------------
LIBRARY ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.std_logic_unsigned.all; 

use std.textio.all;

use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity TB_BTrain_integral is
end TB_BTrain_integral;

architecture Behavioral of TB_BTrain_integral is

	--Component
component BTrain_integral is
	port (
		clk_i			: in std_logic;
		reset_i		: in std_logic;
	
		adc_data_i		: in std_logic_vector(17 downto 0);
		adc_data_rdy_i	: in std_logic;

		init_Bk_i		 : in std_logic;
		int_c0_used_i   : in std_logic;
		int_range_sel_i : in std_logic_vector(3 downto 0);

		--Register	
		reg_val_cfg_sel_integral_i : in std_logic_vector(1 downto 0);
		
		Bsmooth_i		: in std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0);
		corr_fact_i		: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		Vo_i				: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);

		--Input information signals
		low_marker_i  : in std_logic;
		high_marker_i : in std_logic;

		--Marker values
		low_marker_val_i  : in std_logic_vector(31 downto 0);
		high_marker_val_i : in std_logic_vector(31 downto 0);

		C0_i				: in std_logic;
		
		--Output
		BKp1_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		B_data_rdy_o	: out std_logic
	);
end component;

	--Inputs
	signal clk_sti 		    : std_logic := '0';
	signal reset_sti 	  	: std_logic := '0';
	signal start_sti 		  : std_logic := '0';
	signal init_Bk_sti 	 : std_logic := '0';	
	signal Bk_sti 			    : std_logic_vector(NB_BITS_B_VALUE-1 downto 0) := (others => '0');
	signal Bsmooth_sti 	 : std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0) := (others => '0');
	signal corr_fact_sti	: std_logic_vector(31 downto 0) := (others => '0');
	signal Vk_sti 			    : std_logic_vector(NB_BITS_ADC_VALUE-1 downto 0) := (others => '0');
	signal Vo_sti 		    	: std_logic_vector(31 downto 0) := (others => '0');
   
	signal C0_sti : std_logic := '0';
	
	signal low_marker_sti  : std_logic := '0';
	signal high_marker_sti : std_logic := '0';
	
	signal low_marker_val_sti  : std_logic_vector(31 downto 0) := (others => '0');
	signal high_marker_val_sti : std_logic_vector(31 downto 0) := (others => '0');
	
 	--Outputs
	signal data_rdy_obs	: std_logic;
	signal fifo_wen_obs	 : std_logic;
	signal BKp1_obs		   : std_logic_vector(NB_BITS_B_VALUE-1 downto 0);

	-- Clock period definitions
	constant clk_i_period : time := 10 ns;
	
begin

	-- Instantiate the Unit Under Test (UUT)
	uut: BTrain_integral
		port map(
			clk_i 		=> clk_sti,
			reset_i 		=> reset_sti,

			adc_data_i		=> Vk_sti,
		  adc_data_rdy_i	=> start_sti,

			--Register	
			reg_val_cfg_sel_integral_i => (others=>'0'),
			
			--Control input
			init_Bk_i	=> init_Bk_sti,
			int_c0_used_i   => '1',
			int_range_sel_i => (others=>'0'),

			--Input information signals
			C0_i          => C0_sti,
			low_marker_i  => low_marker_sti,
			high_marker_i => high_marker_sti,

			--Marker values
			low_marker_val_i  => low_marker_val_sti,
			high_marker_val_i => high_marker_val_sti,

			--Data
			Bsmooth_i	=> Bsmooth_sti,
			corr_fact_i	=> corr_fact_sti,
			Vo_i 			=> Vo_sti,
					
			--Output
			B_data_rdy_o	=> data_rdy_obs,
			BKp1_o		=> BKp1_obs
		);

   -- Clock process definitions
   clk_i_process : process
   begin
		clk_sti <= '0';
		wait for clk_i_period/2;
		clk_sti <= '1';
		wait for clk_i_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
	
	--To read values in file
	file coeff_f			: text open READ_MODE is "coeff.dat"; -- fichier des coefficients pour les stimuli
	variable coeff_line	: line;	
	variable coeff_v 		: integer;
	variable length_v		: integer;
	
   begin		
		--Initialisation de la simulation
		report ">>>>>> Start simulation";
		report ">>>>>>    Reset";
      reset_sti		<='1';
		start_sti		<='0';
		init_Bk_sti		<='0';
		readline(coeff_f,coeff_line);--first line header : 'Bk Bsmooth corr_fact Vk Vo deltat'
		readline(coeff_f,coeff_line);--second line with value
		read(coeff_line,coeff_v);
--		Bk_sti			<=conv_std_logic_vector(coeff_v,NB_BITS_B_VALUE);
		read(coeff_line,coeff_v);
		Bsmooth_sti		    <=conv_std_logic_vector(coeff_v,NB_BITS_BSMOOTH_VALUE);
		read(coeff_line,coeff_v);
		corr_fact_sti	    <=conv_std_logic_vector(coeff_v,32);
		read(coeff_line,coeff_v);
		Vk_sti			    <=conv_std_logic_vector(coeff_v,NB_BITS_ADC_VALUE);
		read(coeff_line,coeff_v);
		Vo_sti			    <=conv_std_logic_vector(coeff_v,32);
		read(coeff_line,coeff_v);
--		Vo_sti			    <=conv_std_logic_vector(coeff_v,32);
		read(coeff_line,coeff_v);
		low_marker_val_sti <=conv_std_logic_vector(coeff_v,32);
		read(coeff_line,coeff_v);
		high_marker_val_sti<=conv_std_logic_vector(coeff_v,32);
		
		wait for 5*clk_i_period;
				
		--End reset
      reset_sti	<='0';
		wait for 10*clk_i_period;
		report ">>>>>>    End reset";
		wait for 1*clk_i_period;
		wait for 3 ns;

		--Start calcul after initialization
--		init_Bk_sti	<='1';--init Bk with Bk_i input signal
		start_sti	<='1';
		wait for 1*clk_i_period;
		start_sti	<='0';
		report ">>>>>>    Start calculation";
		wait for 2*clk_i_period;
		
		--Read result after initialization
		while (data_rdy_obs='0') loop
			wait for clk_i_period;
		end loop;
		wait for 3 ns;
		
		--Reset 
		report ">>>>>>    Reset";
      reset_sti	<='1';
 		wait for 5*clk_i_period;
		reset_sti	<='0';
		report ">>>>>>    End reset";
		wait for 1*clk_i_period;
		wait for 3 ns;
		
		--Start integral
		readline(coeff_f,coeff_line);--third line : 'Signal'
		readline(coeff_f,coeff_line);--fourth line, length of signal
		read(coeff_line,length_v);
		
		for n in 0 to length_v-1 loop
			if ((n mod 20)=0) then
			 C0_sti <= '1';
			else 
			 C0_sti <= '0';
			end if;
			
			if ((n mod 20)=3) then
			 low_marker_sti <= '1';
			else 
			 low_marker_sti <= '0';
			end if;
			
			if ((n mod 20)=17) then
			 high_marker_sti <= '1';
			else 
			 high_marker_sti <= '0';
			end if;

			readline(coeff_f,coeff_line);--after the fourth line, values of signal
			read(coeff_line,coeff_v);
			Vk_sti <= conv_std_logic_vector(coeff_v,NB_BITS_ADC_VALUE);
			start_sti	<='1';
			wait for 1*clk_i_period;
			start_sti	<='0';
			while (data_rdy_obs='1') loop
				wait for 1 ns;
			end loop;
			while (data_rdy_obs='0') loop
				wait for 1 ns;
			end loop;
		end loop;
	
      --End of test-bench
		wait for 50*clk_i_period;
		report ">>>>>> End of simulation";

      wait;
		
   end process;

	result_process : process

	--To write results of test-bench
	variable result_line : line;
	variable result_v 	: integer;
	file result_f 			: text open WRITE_MODE is "result.dat"; 
	
	begin
		Bk_sti <= (others=>'0');
		wait for 18*clk_i_period;
		while (true) loop
			while (data_rdy_obs='1') loop
				wait for 1 ns;
			end loop;
			while (data_rdy_obs='0') loop
				wait for 1 ns;
			end loop;
			result_v:=conv_integer(ieee.STD_LOGIC_ARITH.unsigned(BKp1_obs));
			Bk_sti <= BKp1_obs;
			write(result_line,result_v);
			writeline(result_f,result_line);
		end loop;
	end process;
	
end Behavioral;

