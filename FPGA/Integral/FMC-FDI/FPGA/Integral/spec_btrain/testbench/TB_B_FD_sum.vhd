--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:22:24 06/05/2012
-- Design Name:   
-- Module Name:   C:/B-Train/FPGA/Integral/spec_btrain/TB_B_FD_sum.vhd
-- Project Name:  spec_btrain
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: B_FD_sum
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments: To run the testbench with Modelsim type the command:
--                      do run_FD_sum.do
--
--                      To generate reference and result file, use 
--                      ../../MatlabUtil/Generate_FD_sum.m 
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.std_logic_unsigned.all; 

use std.textio.all;

use work.fmc_adc2M18b2ch_pkg.all;
use work.integral_pkg.all;
use work.utils_pkg.all;
 
ENTITY TB_B_FD_sum IS
END TB_B_FD_sum;
 
ARCHITECTURE behavior OF TB_B_FD_sum IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
	component B_FD_Sum 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;

			reg_f_b_coeff_i     : in std_logic_vector(31 downto 0);
			reg_d_b_coeff_i     : in std_logic_vector(31 downto 0);
			reg_kf_bdot_coeff_i : in std_logic_vector(31 downto 0);

			Bk_f_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_f_data_rdy_i	: in std_logic;

			Bk_d_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_d_data_rdy_i	: in std_logic;

			Bk_o				: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_data_rdy_o	: out std_logic;

			Bdot_o				: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			Bdot_data_rdy_o	: out std_logic
		);
	end component;

	
   --Inputs
   signal clk_sti   : std_logic := '0';
   signal reset_sti : std_logic := '0';

	signal reg_f_coeff_sti	: std_logic_vector(31 downto 0):=(others=>'0');
	signal reg_d_coeff_sti	: std_logic_vector(31 downto 0):=(others=>'0');

	signal reg_kf_bdot_coeff_sti	: std_logic_vector(31 downto 0):=(others=>'0');

	signal Bk_f_sti			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0):=(others=>'0');
	signal B_f_data_rdy_sti	: std_logic:='0';

	signal Bk_d_sti			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0):=(others=>'0');
	signal B_d_data_rdy_sti	: std_logic:='0';

	--Output
	signal Bk_obs			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal B_data_rdy_obs: std_logic;

	signal Bdot_obs			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal Bdot_data_rdy_obs: std_logic;

   -- Clock period definitions
   constant clk_i_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
	uut : B_FD_Sum 
		port map(
			clk_i		=> clk_sti,
			reset_i	=> reset_sti,

			reg_f_b_coeff_i	=> reg_f_coeff_sti,
			reg_d_b_coeff_i	=> reg_d_coeff_sti,

			reg_kf_bdot_coeff_i	=> reg_kf_bdot_coeff_sti,

			Bk_f_i			=> Bk_f_sti,
			B_f_data_rdy_i	=> B_f_data_rdy_sti,

			Bk_d_i			=> Bk_d_sti,
			B_d_data_rdy_i	=> B_d_data_rdy_sti,

			Bk_o				=> Bk_obs,
			B_data_rdy_o	=> B_data_rdy_obs,

			Bdot_o				=> Bdot_obs,
			Bdot_data_rdy_o	=> Bdot_data_rdy_obs
		);

   -- Clock process definitions
   clk_i_process : process
   begin
		clk_sti <= '0';
		wait for clk_i_period/2;
		clk_sti <= '1';
		wait for clk_i_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
	
	--To read values in file
	file coeff_f			: text open READ_MODE is "FD_sum.dat"; -- fichier des coefficients pour les stimuli
	variable coeff_line	: line;	
	variable coeff_v 		: integer;
	variable length_v		: integer;
--	variable coeff_v 		: std_logic_vector(31 downto 0);
	
	file ref_f			: text open READ_MODE is "ref_FD_sum.dat"; -- references
	variable ref_line	: line;	
	variable ref_v 	: integer;
--	variable ref_v 	: std_logic_vector(31 downto 0);
	
   variable c: character;
	
   begin		
		--Initialisation de la simulation
		report ">>>>>> Start simulation";
		report ">>>>>>    Reset";
      reset_sti		<='1';
		readline(coeff_f,coeff_line);--first line header : 'Number of values '
		readline(coeff_f,coeff_line);--second line with value
		read(coeff_line,length_v);
		readline(coeff_f,coeff_line);--third line header : 'BF,GF,BD,GD,ScaleBdot'

		B_f_data_rdy_sti	<= '0';
		B_d_data_rdy_sti	<= '0';

		--Reset 
		report ">>>>>>    Reset";
      reset_sti	<='1';
 		wait for 5*clk_i_period;
		reset_sti	<='0';
		report ">>>>>>    End reset";
		wait for 1*clk_i_period;
		wait for 3 ns;
		
		--Start sum
		for n in 0 to length_v-1 loop
			readline(coeff_f,coeff_line);
			read(coeff_line,coeff_v);
			Bk_f_sti			<=conv_std_logic_vector(coeff_v,Bk_f_sti'length);
			read(coeff_line,c);
			read(coeff_line,coeff_v);
			reg_f_coeff_sti<=conv_std_logic_vector(coeff_v,reg_f_coeff_sti'length);
			read(coeff_line,c);
			read(coeff_line,coeff_v);
			Bk_d_sti			<=conv_std_logic_vector(coeff_v,Bk_d_sti'length);
			read(coeff_line,c);
			read(coeff_line,coeff_v);
			reg_d_coeff_sti<=conv_std_logic_vector(coeff_v,reg_d_coeff_sti'length);
			read(coeff_line,c);
			read(coeff_line,coeff_v);
			reg_kf_bdot_coeff_sti<=conv_std_logic_vector(coeff_v,reg_kf_bdot_coeff_sti'length);
			B_f_data_rdy_sti	<= '1';
			B_d_data_rdy_sti	<= '1';
			wait for 1*clk_i_period;
			B_f_data_rdy_sti	<= '0';
			B_d_data_rdy_sti	<= '0';
			while (B_data_rdy_obs='0') loop
				wait for 1 ns;
			end loop;
			while (B_data_rdy_obs='1') loop
				wait for 1 ns;
			end loop;
			wait for 5*clk_i_period;
		end loop;
	
      --End of test-bench
		wait for 500*clk_i_period;
		report ">>>>>> End of simulation";

      wait;
		
   end process;

	result_process : process

	--To write results of test-bench
	variable result_line : line;
	variable result_v 	: integer;
	file result_f 			: text open WRITE_MODE is "result_FD_sum.dat"; 
	
	begin
		wait for 5*clk_i_period;
--		while (true) loop
			while (B_data_rdy_obs='0') loop
				wait for 2 ns;
			end loop;
			while (B_data_rdy_obs='1') loop
				wait for 2 ns;
			end loop;
			report ">>>>  Read result";
--			result_v:=conv_integer(ieee.STD_LOGIC_ARITH.unsigned(Bk_obs));
			result_v:=conv_integer(Bk_obs);
			write(result_line,result_v);
			writeline(result_f,result_line);
--		end loop;
	end process;

END;
