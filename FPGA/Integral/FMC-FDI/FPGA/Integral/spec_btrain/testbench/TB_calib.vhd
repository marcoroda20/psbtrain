--------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer:		Daniel Oberson
--
-- Create Date:   19:22:24 16/05/2013
-- Design Name:   
-- Module Name:   C:\PSBTrain\FPGA\Integral\spec_btrain\testbench\TB_calib.vhd
-- Project Name:  spec_btrain
-- Target Device:  
-- Tool versions:  
-- Description:   Testbench for calibration
-- Additional Comments: To run the testbench with Modelsim type the command:
--                      do run_calib.do
--
--------------------------------------------------------------------------------
LIBRARY ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.std_logic_unsigned.all; 

use std.textio.all;

library work;
use work.btrain_pkg.all;
use work.adc_dac_pkg.all;
use work.calib_pkg.all;
 
ENTITY TB_calib IS
END TB_calib;
 
ARCHITECTURE behavior OF TB_calib IS 
 
--   -- Component Declaration for the Unit Under Test (UUT)
--	component btrain_calib is
--		generic(g_simulation : integer := 0);
--		port(
--			clk_i 	: in std_logic;
--			reset_i	: in std_logic;
--
--			--to test
--			adc_sync_i  : in std_logic_vector(2 downto 0);
--
--			force_cal_o       : out std_logic;
--			force_cal_i       : in std_logic;
--			force_cal_load_o  : out std_logic;
--
--			time_calib_reg_i	: in std_logic_vector(31 downto 0);	--1 sec resolution
--			t_next_calib_o		: out std_logic_vector(31 downto 0);--1 sec resolution 
--			C0_i					: in std_logic;
--			zero_cycle_i		: in std_logic;
--			reg_calib_delay_i	: in std_logic_vector(31 downto 0);
--						
--			ADC_pos_ref_i		: in std_logic_vector(19 downto 0);
--			nb_of_meas_reg_i	: in std_logic_vector(31 downto 0);
--
--			open1_o	: out std_logic;
--			gain1_o	: out std_logic;
--			off1_o	: out std_logic;
--			
--			open2_o	: out std_logic;
--			gain2_o	: out std_logic;
--			off2_o	: out std_logic;
--
--			DAC_wr_o 	: out std_logic_vector(23 downto 0);	-- To DAC registers write interface
--			DAC_rd_i 	: in std_logic_vector(23 downto 0);		-- From DAC registers read interface
--			end_dac_rd_i	: in std_logic;
--			end_dac_wr_i	: in std_logic;
--				
--			adc_data_rdy_F_i	: in std_logic;
--			adc_data_F_i		: in std_logic_vector(17 downto 0);
--			adc_data_rdy_D_i	: in std_logic;
--			adc_data_D_i		: in std_logic_vector(17 downto 0);
--
--			val_pos_F_o		: out std_logic_vector(31 downto 0);
--			val_neg_F_o		: out std_logic_vector(31 downto 0);
--			val_pos_D_o		: out std_logic_vector(31 downto 0);
--			val_neg_D_o		: out std_logic_vector(31 downto 0);
--
--			gain_F_o		: out std_logic_vector(31 downto 0);
--			offset_calc_F_o	: out std_logic_vector(31 downto 0);
--			offset_meas_F_o	: out std_logic_vector(31 downto 0);
--			gain_D_o		: out std_logic_vector(31 downto 0);
--			offset_calc_D_o	: out std_logic_vector(31 downto 0);
--			offset_meas_D_o	: out std_logic_vector(31 downto 0);
--			
--			error_status_o	: out std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
--			calib_status_o	: out std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0)
--		);
--	end component;
--	
--	component switch_dac
--		port(
--			clk_i		: in std_logic;
--			reset_i	: in std_logic;
--			
--			--Value for DAC selectionned by Mux_dac_val
--			val_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
--			data_rdy_i	: in std_logic;
--
--			--Value for DAC for electronic calibration
--			calib_DAC_wr_i 	: in std_logic_vector(23 downto 0);	
--			calib_DAC_rd_o 	: out std_logic_vector(23 downto 0);
--			calib_end_dac_rd_o: out std_logic;
--			calib_end_dac_wr_o: out std_logic;
--			
--			--Value for DAC from the register controled by the PC
--			reg_dac_bus_i	: in std_logic_vector(23 downto 0);
--			
--			--Control
--			reg_val_sel_fpga_bus_i	: in std_logic;
--			calib_status_i				: in std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);
--
--			--Control bus for DAC
--			end_dac_rd_i	: in std_logic;
--			end_dac_wr_i	: in std_logic;
--			DAC_rd_i 		: in std_logic_vector(23 downto 0);		-- From DAC registers read interface
--			DAC_wr_o 		: out std_logic_vector(23 downto 0)	-- To DAC registers write interface
--		);
--	end component;
--
--	component AD5791 
--		Port ( 
--			CLK, SDI : in  STD_LOGIC;
--			reset_i	: in std_logic;
--			SDO, SCK, N_LDAC, N_SYNC : out  STD_LOGIC;
--			N_RESET, N_CLR, END_WR_DAC, END_RD_DAC : out  STD_LOGIC;
--			REG_CONT : out  STD_LOGIC_VECTOR (23 downto 0);
--			DATA_WR : in  STD_LOGIC_VECTOR (23 downto 0) 
--		);				  
--	end component;
--
--	component Acquisition_AD7986_corr 
--		port(
--			clk_i			: in std_logic;
--			clk_adc_i		: in std_logic;
----			clk_gate_i	: in std_logic;
--			reset_i		: in std_logic;
--			clk_cnv_i	: in std_logic;
--
--			adc_sdo_i	: in std_logic;
--			adc_sdi_o	: out std_logic;
--			adc_sck_o	: out std_logic;
--			adc_cnv_o	: out std_logic;
--			
--			gain_i	: in std_logic_vector(31 downto 0);
--			offset_i : in std_logic_vector(31 downto 0);
--
--			adc_data_rdy_o	: out std_logic;
--			adc_data_o		: out std_logic_vector(17 downto 0);
--
--			adc_corr_rdy_o	: out std_logic;
--			adc_corr_o		: out std_logic_vector(17 downto 0)
--		);
--	end component;
	
   --Inputs
	signal clk_sti        : std_logic := '0';
	signal clk_adc_sti    : std_logic := '0';
	signal clk_cnv_sti    : std_logic := '0';
	signal reset_sti      : std_logic := '0';

	signal adc_sync_sti   : std_logic_vector(2 downto 0) := "010";

	signal force_cal_sti    	: std_logic;
	signal time_calib_reg_sti	: std_logic_vector(31 downto 0); --1 sec resolution
	signal zero_cycle_sti		: std_logic;
	signal C0_sti					: std_logic:='0';
	signal reg_calib_delay_sti	: std_logic_vector(31 downto 0);

	--ADC F
	signal adc_sdo_F_sti		: std_logic;
	signal sdo1_F_s			: std_logic;
	signal sdo2_F_s			: std_logic;
	signal cnt_bit_adc_F_s			: integer;
	signal cnt_start_bit_adc_F_s	: integer;
--	signal cnt_clk_F_s		: integer;
--	signal cnt_clk_D_s		: integer;
	--ADC D
	signal adc_sdo_D_sti		: std_logic;
	signal sdo1_D_s			: std_logic;
	signal sdo2_D_s			: std_logic;
	signal cnt_bit_adc_D_s			: integer;
	signal cnt_start_bit_adc_D_s	: integer;
	
	signal ADC_pos_ref_sti			: std_logic_vector(19 downto 0);
	signal nb_of_meas_reg_sti		: std_logic_vector(31 downto 0);
	signal simulated_val_pos_sti	: std_logic_vector(19 downto 0):= (others=>'0');--ADC_pos_ref_sti + x"0000a";--x"701fb";--ADC_pos_ref_sti + x"0000a";
	signal simulated_val_neg_sti	: std_logic_vector(19 downto 0):= (others=>'0');--not(ADC_pos_ref_sti)+ x"00001" - x"00011";--x"8fdc8";--not(ADC_pos_ref_sti)+ x"00001" - x"00011";
	signal simulated_offset_sti	: std_logic_vector(17 downto 0):= "00" & x"0013";--"11" & x"ffed";--"00" & x"0013";--"11" & x"fff9";--"00" & x"0007";
	signal signal_input_adc_sti	: std_logic_vector(17 downto 0):= "01" & x"2345";--"11" & x"ffff";
	
 	--Outputs			
	signal t_next_calib_obs		: std_logic_vector(31 downto 0); --1 sec resolution 
			
	signal force_cal_obs			: std_logic;
	signal force_cal_load_obs	: std_logic;

	signal adc_F_obs 		: std_logic_vector(17 downto 0);
	signal adc_drdy_F_obs: std_logic;
	signal adc_D_obs 		: std_logic_vector(17 downto 0);
	signal adc_drdy_D_obs: std_logic;

	signal adc_sdi_F_obs	: std_logic;
	signal adc_sck_F_obs	: std_logic;
	signal adc_cnv_F_obs	: std_logic;
	signal adc_sdi_D_obs	: std_logic;
	signal adc_sck_D_obs	: std_logic;
	signal adc_cnv_D_obs	: std_logic;

	signal open1_obs	: std_logic;
	signal gain1_obs	: std_logic;
	signal off1_obs	: std_logic;
			
	signal open2_obs	: std_logic;
	signal gain2_obs	: std_logic;
	signal off2_obs	: std_logic;

	signal val_pos_F_obs	: std_logic_vector(31 downto 0);
	signal val_neg_F_obs	: std_logic_vector(31 downto 0);
	signal val_pos_D_obs	: std_logic_vector(31 downto 0);
	signal val_neg_D_obs	: std_logic_vector(31 downto 0);

	signal error_status_obs	: std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);

	--Signal
	signal start_cal_s		: std_logic;
	signal calib_status_s	: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);

	signal adc_F_s				: std_logic_vector(17 downto 0);
	signal adc_D_s				: std_logic_vector(17 downto 0);
	
 	signal adc_data_F_s		: std_logic_vector(17 downto 0) := (others=>'0');
	signal adc_data_rdy_F_s	: std_logic := '0';
 	signal adc_data_D_s		: std_logic_vector(17 downto 0) := (others=>'0');
	signal adc_data_rdy_D_s	: std_logic := '0';


	signal calib_DAC_wr_s		: std_logic_vector (23 downto 0);
	signal calib_DAC_rd_s		: std_logic_vector (23 downto 0);
	signal calib_end_dac_rd_s	: std_logic;
	signal calib_end_dac_wr_s	: std_logic;

	signal DAC_wr_s 		: std_logic_vector (23 downto 0);
	signal DAC_rd_s		: std_logic_vector (23 downto 0);
	signal end_dac_rd_s	: std_logic;
	signal end_dac_wr_s	: std_logic;

	signal SDI_s		: std_logic := '0';
	signal SDO_s		: std_logic;
	signal SCK_s		: std_logic;
	signal N_LDAC_s	: std_logic;
	signal N_SYNC_s	: std_logic;
	signal DAC_spi_s	: std_logic_vector (23 downto 0) := (others=>'0');
	
	signal gain_F_s			: std_logic_vector(31 downto 0);
	signal offset_calc_F_s	: std_logic_vector(31 downto 0);
	signal offset_meas_F_s	: std_logic_vector(31 downto 0);
	signal gain_D_s			: std_logic_vector(31 downto 0);
	signal offset_calc_D_s	: std_logic_vector(31 downto 0);
	signal offset_meas_D_s	: std_logic_vector(31 downto 0);

  -- Clock period definitions
   constant clk_i_period : time := 10 ns;
 
BEGIN

	-- Instantiate the Unit Under Test (UUT)
		uut : btrain_calib 
		generic map(g_simulation => 1)
		port map(
			clk_i 	=> clk_sti,
			reset_i	=> reset_sti,

			time_calib_reg_i	=> time_calib_reg_sti,
			t_next_calib_o		=> t_next_calib_obs,
			
			force_cal_o       => force_cal_obs,
			force_cal_i       => force_cal_sti,
			force_cal_load_o  => force_cal_load_obs,

			zero_cycle_i	=> zero_cycle_sti,

			C0_i					=> C0_sti,
			reg_calib_delay_i	=> reg_calib_delay_sti,
						
			ADC_pos_ref_i		=> ADC_pos_ref_sti,
			nb_of_meas_reg_i	=> nb_of_meas_reg_sti,

			open1_o	=> open1_obs,
			gain1_o	=> gain1_obs,
			off1_o	=> off1_obs,
			
			open2_o	=> open2_obs,
			gain2_o	=> gain2_obs,
			off2_o	=> off2_obs,

			DAC_wr_o 	=> calib_DAC_wr_s,
			DAC_rd_i 	=> calib_DAC_rd_s,
			end_dac_rd_i	=> calib_end_dac_rd_s,
			end_dac_wr_i	=> calib_end_dac_wr_s,
				
			adc_data_rdy_F_i	=> adc_data_rdy_F_s,
			adc_data_F_i		=> adc_data_F_s,
			adc_data_rdy_D_i	=> adc_data_rdy_D_s,
			adc_data_D_i		=> adc_data_D_s,

			gain_F_o				=> gain_F_s,
			offset_calc_F_o	=> offset_calc_F_s,
			offset_meas_F_o	=> offset_meas_F_s,
			gain_D_o				=> gain_D_s,
			offset_calc_D_o	=> offset_calc_D_s,
			offset_meas_D_o	=> offset_meas_D_s,

			val_pos_F_o		=> val_pos_F_obs,
			val_neg_F_o		=> val_neg_F_obs,
			val_pos_D_o		=> val_pos_D_obs,
			val_neg_D_o		=> val_neg_D_obs,
			
			error_status_o	=> error_status_obs,
			calib_status_o	=> calib_status_s
		);

	cmp_AD7986_corr_F : Acquisition_AD7986_corr 
		port map(
			clk_i			=> clk_sti,
			clk_adc_i		=> clk_adc_sti,
			clk_cnv_i	=> clk_cnv_sti,
			reset_i		=> reset_sti,

			--to test
			adc_sync_i  => adc_sync_sti,

			adc_sdo_i	=> adc_sdo_F_sti,
			adc_sdi_o	=> adc_sdi_F_obs,
			adc_sck_o	=> adc_sck_F_obs,
			adc_cnv_o	=> adc_cnv_F_obs,
			
			gain_i	=> gain_F_s,
			offset_i => offset_meas_F_s,

			adc_data_rdy_o	=> adc_data_rdy_F_s,
			adc_data_o		=> adc_data_F_s,

			adc_corr_rdy_o	=> adc_drdy_F_obs,
			adc_corr_o		=> adc_F_obs
		);

	cmp_AD7986_corr_D : Acquisition_AD7986_corr 
		port map(
			clk_i			=> clk_sti,
			clk_adc_i		=> clk_adc_sti,
			clk_cnv_i	=> clk_cnv_sti,
			reset_i		=> reset_sti,

			--to test
			adc_sync_i  => adc_sync_sti,

			adc_sdo_i	=> adc_sdo_D_sti,
			adc_sdi_o	=> adc_sdi_D_obs,
			adc_sck_o	=> adc_sck_D_obs,
			adc_cnv_o	=> adc_cnv_D_obs,
			
			gain_i	=> gain_D_s,
			offset_i => offset_meas_D_s,

			adc_data_rdy_o	=> adc_data_rdy_D_s,
			adc_data_o		=> adc_data_D_s,

			adc_corr_rdy_o	=> adc_drdy_D_obs,
			adc_corr_o		=> adc_D_obs
		);

	cmp_switch_dac : switch_dac
		port map(
			clk_i		=> clk_sti,
			reset_i	=> reset_sti,
			
			--Value for DAC selectionned by Mux_dac_val
			val_i			=> x"87654321",--x"000" & "00" & adc_data_F_s,
			data_rdy_i	=> adc_data_rdy_F_s,

			--Value for DAC for electronic calibration
			calib_DAC_wr_i 	=> calib_DAC_wr_s,
			calib_DAC_rd_o 	=> calib_DAC_rd_s,
			calib_end_dac_rd_o=> calib_end_dac_rd_s,
			calib_end_dac_wr_o=> calib_end_dac_wr_s,

			--Value for DAC from the register controled by the PC
			reg_dac_bus_i	=> x"000000",
			
			--Control
			reg_val_sel_fpga_bus_i	=> '0',
			calib_status_i				=> calib_status_s,

			--Control bus for DAC
			end_dac_rd_i	=> end_dac_rd_s,
			end_dac_wr_i	=> end_dac_wr_s,
			DAC_rd_i 		=> DAC_rd_s,
			DAC_wr_o 		=> DAC_wr_s
		);

	cmp_dac : AD5791 
		Port map( 
			CLK		=> clk_sti,
			RESET	=> reset_sti,
			
			SDI		=> SDI_s,
			SDO		=> SDO_s,
			
			SCK		=> SCK_s,
			N_LDAC	=> N_LDAC_s,
			N_SYNC	=> N_SYNC_s,
			N_RESET	=> open,
			N_CLR		=> open,
			
			END_WR_DAC	=> end_dac_wr_s,
			END_RD_DAC	=> end_dac_rd_s,
			DAC_RD		=> DAC_rd_s,
			DAC_WR		=> DAC_wr_s
		);				  

   -- Clock process definitions
   clk_i_process :process
   begin
		clk_sti <= '0';
		wait for clk_i_period/2;
		clk_sti <= '1';
		wait for clk_i_period/2;
   end process;
	
   clk_adc_process :process
   begin
		clk_adc_sti <= '0';
		wait for 6.25 ns;
		clk_adc_sti <= '1';
		wait for 6.25 ns;
   end process;
 
   clk_cnv_process :process
   begin
		clk_cnv_sti <= '0';
		wait for 25*clk_i_period;
		clk_cnv_sti <= '1';
		wait for 25*clk_i_period;
   end process;
 
   -- ADC F process simulation
   ADC_F_process : process (adc_sck_F_obs,clk_sti,reset_sti,adc_data_rdy_F_s,
						simulated_val_neg_sti,simulated_val_pos_sti,
						adc_F_s,cnt_bit_adc_F_s,cnt_start_bit_adc_F_s,DAC_spi_s,calib_status_s)
   begin
		if (reset_sti='1') then
			sdo2_F_s <= '0';
			cnt_bit_adc_F_s <= NB_BITS_ADC_VALUE-1;
--			cnt_clk_F_s <= NB_BITS_ADC_VALUE+1;			
			cnt_start_bit_adc_F_s <= 13;
			adc_F_s	<= (others=>'0');
		elsif (adc_sck_F_obs='1' and adc_sck_F_obs'event) then
				sdo2_F_s <= adc_F_s(cnt_bit_adc_F_s);
						if (cnt_bit_adc_F_s/=0) then
							cnt_bit_adc_F_s <= cnt_bit_adc_F_s-1;
						else
							cnt_bit_adc_F_s <= NB_BITS_ADC_VALUE-1;
						end if;
		elsif (adc_data_rdy_F_s='1' and adc_data_rdy_F_s'event) then
			if (calib_status_s=OFFSET_st or calib_status_s=SWITCH_AS_OFFSET_st) then
				adc_F_s <= simulated_offset_sti;
			elsif (calib_status_s=MEAS_POS_st) then
				if (DAC_spi_s(23 downto 20)=AD5791_WR_VAL) then
					adc_F_s <= simulated_val_pos_sti(19 downto 2) + simulated_offset_sti;
				end if;
			elsif (calib_status_s=MEAS_NEG_st) then
				if (DAC_spi_s(23 downto 20)=AD5791_WR_VAL) then
					adc_F_s <= simulated_val_neg_sti(19 downto 2) + simulated_offset_sti;
				end if;
			else
				adc_F_s <= signal_input_adc_sti;
			end if;
		end if;
   end process;
	
	process (clk_adc_sti,reset_sti,sdo1_F_s,sdo2_F_s)
	begin
 		--latency to simulate Hardware ADC
		if (reset_sti='1') then
			sdo1_F_s <= '0';
			adc_sdo_F_sti <= '0';
		elsif (clk_adc_sti='1' and clk_adc_sti'event) then
			sdo1_F_s <= sdo2_F_s;
			adc_sdo_F_sti <= sdo1_F_s;
		end if;
	end process;

   -- ADC D process simulation
   ADC_D_process : process (adc_sck_D_obs,clk_sti,reset_sti,adc_data_rdy_D_s,
						adc_D_s,cnt_bit_adc_D_s,cnt_start_bit_adc_D_s,DAC_spi_s,calib_status_s)
   begin
		if (reset_sti='1') then
			sdo2_D_s <= '0';
			cnt_bit_adc_D_s <= NB_BITS_ADC_VALUE-1;
			adc_D_s	<= (others=>'0');
		elsif (adc_sck_D_obs='1' and adc_sck_D_obs'event) then
				sdo2_D_s <= adc_D_s(cnt_bit_adc_D_s);
				if (cnt_bit_adc_D_s/=0) then
					cnt_bit_adc_D_s <= cnt_bit_adc_D_s-1;
				else
--					cnt_clk_D_s <= NB_BITS_ADC_VALUE+1;
				end if;
		elsif (adc_data_rdy_D_s='1' and adc_data_rdy_D_s'event) then
			if (calib_status_s=OFFSET_st or calib_status_s=SWITCH_AS_OFFSET_st) then
				adc_D_s <= simulated_offset_sti;
			elsif (calib_status_s=MEAS_POS_st) then
				if (DAC_spi_s(23 downto 20)=AD5791_WR_VAL) then
					adc_D_s <= simulated_val_pos_sti(19 downto 2) + simulated_offset_sti;
--					adc_D_s <= DAC_spi_s(19 downto 2) + simulated_offset_sti;
				end if;
			elsif (calib_status_s=MEAS_NEG_st) then
				if (DAC_spi_s(23 downto 20)=AD5791_WR_VAL) then
					adc_D_s <= simulated_val_neg_sti(19 downto 2) + simulated_offset_sti;
--					adc_D_s <= DAC_spi_s(19 downto 2) + simulated_offset_sti;
				end if;
			else
				adc_D_s <= signal_input_adc_sti;
			end if;
		end if;
   end process;
	
	process (clk_adc_sti,reset_sti,sdo1_D_s,sdo2_D_s)
	begin
 		--latency to simulate Hardware ADC
		if (reset_sti='1') then
			sdo1_D_s <= '0';
			adc_sdo_D_sti <= '0';
		elsif (clk_adc_sti='1' and clk_adc_sti'event) then
			sdo1_D_s <= sdo2_D_s;
			adc_sdo_D_sti <= sdo1_D_s;
		end if;
	end process;

--	DAC_calib_process 
	process
	begin
		--CONFIG DAC AD5791 sim
		wait for 1 ns;
		while (N_SYNC_s='1') loop
			wait for 1 ns;
		end loop;
		--send value to DAC
		for n in 0 to 23 loop
			C0_sti <= not C0_sti;
			while (SCK_s='0') loop
				wait for 1 ns;
			end loop;
			DAC_spi_s <= DAC_spi_s (22 downto 0) & SDO_s;
			while (SCK_s='1') loop
				wait for 1 ns;
			end loop;
		end loop;
		wait for 1*clk_i_period;
		while (N_SYNC_s='1') loop
			wait for 1 ns;
		end loop;
		while (N_SYNC_s='0') loop
			wait for 1 ns;
		end loop;
		while (N_SYNC_s='1') loop
			wait for 1 ns;
		end loop;
		--read value from DAC
		for n in 0 to 23 loop
			while (clk_sti='0') loop
				wait for 1 ns;
			end loop;
			SDI_s <= DAC_spi_s (23-n);
			while (clk_sti='1') loop
				wait for 1 ns;
			end loop;
		end loop;
	end process;
	
   -- Stimulus process
   stim_proc: process
   begin		
		--Initialisation de la simulation
		report ">>>>>> Start simulation";
		report ">>>>>>    Init stimulation and Reset";
      reset_sti				<= '1';
		ADC_pos_ref_sti		<= "01" & x"8000" & "00";


		nb_of_meas_reg_sti	<= x"00000005";	

		force_cal_sti			<= '0';
		time_calib_reg_sti	<= x"01000005";
		reg_calib_delay_sti	<= x"10000007";
		zero_cycle_sti			<= '0';
		wait for 8*clk_i_period;

		--End reset
      reset_sti	<='0';
		wait for 10*clk_i_period;
		report ">>>>>>    End init and reset";
		wait for 1*clk_i_period;
		wait for 1 ns;

		wait for 6 us;

		--Start calibration
		simulated_val_pos_sti<= ADC_pos_ref_sti + x"0000a";--x"701fb";--ADC_pos_ref_sti + x"0000a";
		simulated_val_neg_sti<= not(ADC_pos_ref_sti)+ x"00001" - x"00011";--x"8fdc8";--not(ADC_pos_ref_sti)+ x"00001" - x"00011";
		
		time_calib_reg_sti	<= x"00000005";

		force_cal_sti			<= '0';
		reg_calib_delay_sti	<= x"00000007";
		zero_cycle_sti			<= '1';
		wait for 3 us;
		zero_cycle_sti			<= '0';
		
		wait;
		
	end process;

END;
