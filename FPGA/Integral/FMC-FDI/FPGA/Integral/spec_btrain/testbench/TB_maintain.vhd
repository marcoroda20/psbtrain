--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:44:20 11/06/2014
-- Design Name:   
-- Module Name:   C:/PSBTrain/FPGA/Integral/spec_btrain/testbench/TB_maintain.vhd
-- Project Name:  spec_btrain
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: maintain
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_maintain IS
END TB_maintain;
 
ARCHITECTURE behavior OF TB_maintain IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT maintain
    PORT(
         clk_i : IN  std_logic;
         reset_i : IN  std_logic;
         s_i : IN  std_logic;
         s_o : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk_i : std_logic := '0';
   signal reset_i : std_logic := '0';
   signal s_i : std_logic := '0';

 	--Outputs
   signal s_o : std_logic;

   -- Clock period definitions
   constant clk_i_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: maintain PORT MAP (
          clk_i => clk_i,
          reset_i => reset_i,
          s_i => s_i,
          s_o => s_o
        );

   -- Clock process definitions
   clk_i_process :process
   begin
		clk_i <= '0';
		wait for clk_i_period/2;
		clk_i <= '1';
		wait for clk_i_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 20 ns;	
      s_i <= '0';
		reset_i <= '1';
      wait for clk_i_period*10;
		reset_i <= '0';
      wait for clk_i_period*10;
		
      -- insert stimulus here 
      s_i <= '1';
		wait for clk_i_period;
      s_i <= '0';

      wait for clk_i_period*20;
      s_i <= '1';

      wait for clk_i_period*20;
      s_i <= '0';
		wait for clk_i_period;
      s_i <= '1';
		
      wait;
   end process;

END;
