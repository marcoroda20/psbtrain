//
// White Rabbit for the new BTrain 
//
//
// Objectives:
// - test the WR functionality for the new BTrain
//
// Brief description:
// Testbench instantiates two SPEC cards connected to each other via a Gigabit
// Ethernet link. 
// BTrainSpec sends frames to WRListenerSec, which decode thess frames.

`timescale 10fs/10fs // need very fine timestep to correctly simulate the GTP.

module main;

   // Parameters
   
   // Reference clock period.
   parameter g_ref_clock_period = 8ns;

   reg clk_20m = 0, clk_ref = 0;
   reg [1:0] button = 'b11; 
   reg pulse_in = 0;
   reg reset_hw = 1;
   reg L_RST_N = 'b1;
   // Generate the reference clock
   always #(g_ref_clock_period / 2) clk_ref <= ~clk_ref;

   // Generate the 20 MHz VCXO clock
   always #(50ns / 2) clk_20m <= ~clk_20m; 
  
   wire [4:0] dio_out_b;
      
   // This time we have two SPECs talking to each other in the same testbench
   spec_top_fmc_adc2M18b2ch
     #(
       .g_simulation (1)
    ) spec_btrainA (
           
      // Local oscillator
      .clk_125m_pllref_p_i (clk_ref),
      .clk_125m_pllref_n_i (~clk_ref),

      .fpga_pll_ref_clk_101_p_i (clk_ref),
      .fpga_pll_ref_clk_101_n_i (~clk_ref),

      .clk_20MHz_i(clk_20m),
      .reset_n_hw_i(reset_hw),

      // Connect the gigabit output of one SPEC with the RX input of the other,
      // and vice-versa.
      .sfp_txp_o(a_to_b_p),
      .sfp_txn_o(a_to_b_n),

      .sfp_rxp_i(b_to_a_p),
      .sfp_rxn_i(b_to_a_n),
      
      // Carrier font panel LEDs
      //led_red_sfp_o   : out std_logic;
      //led_green_sfp_o : out std_logic;

      .L_RST_N(L_RST_N),
      .aux_buttons_i( button )
      //     .dio_p_i( {3'b0, pulse_in, 1'b0} ),
      //     .dio_n_i( {3'b1, ~pulse_in, 1'b1} )
  
  );

   spec_top_fmc_adc2M18b2ch
     #(
       .g_simulation (1)
    ) spec_btrainB (
        .clk_125m_pllref_p_i (clk_ref),
        .clk_125m_pllref_n_i (~clk_ref),

        .fpga_pll_ref_clk_101_p_i (clk_ref),
        .fpga_pll_ref_clk_101_n_i (~clk_ref),

        .clk_20MHz_i(clk_20m),
        .reset_n_hw_i(reset_hw),

           // Connect the gigabit output of one SPEC with the RX input of the other,
           // and vice-versa.
        .sfp_txp_o(b_to_a_p),
        .sfp_txn_o(b_to_a_n),

        .sfp_rxp_i(a_to_b_p),
        .sfp_rxn_i(a_to_b_n),
       
        .L_RST_N(L_RST_N),
        .aux_buttons_i(button)
         //  .dio_p_o ( dio_out_b )
    );
   

   // observe the link LEDs on both sides, and tell us when the link is ready.
   wire link_up_a = spec_btrainA.U_The_WR_Core.led_link_o;
   wire link_up_b = spec_btrainB.U_The_WR_Core.led_link_o;

  initial begin
      // wait until both SPECs see the Ethernet link. Otherwise the packet we're going 
      // to send might end up in void...
      wait(link_up_a == 1'b1 && link_up_b == 1'b1);

     forever begin // send a pulse every 30 us;
        #5us;
        button = 'b11; 
        #1us;
        button = 'b01; 
        #1us;
        button = 'b11;
        #50us;
     end end
   
endmodule // main

