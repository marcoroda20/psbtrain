vlog -sv main.sv 
vsim -L unisim -L secureip work.main -voptargs="+acc" -suppress 8684,8683
set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave_wr.do
run 300us
wave zoomfull
radix -hex
