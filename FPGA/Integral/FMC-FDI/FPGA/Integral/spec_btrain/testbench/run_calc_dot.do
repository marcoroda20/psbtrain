vlib work

vcom -93 ../rtl/fmc_adc2M18b2ch_pkg.vhd 
vcom -93 ../rtl/calib/calib_pkg.vhd 
vcom -93 ../rtl/integral/integral_pkg.vhd 
vcom -93 ../rtl/adc_dac/adc_dac_pkg.vhd
vcom -93 ../rtl/streamers16bit/streamers_pkg.vhd 
vcom -93 ../rtl/wr/wr_btrain_pkg.vhd 
vcom -93 ../rtl/lcd/lcd_pkg.vhd 
vcom -93 ../rtl/dma/dma_pkg.vhd 
vcom -93 ../rtl/utils/utils_pkg.vhd 
vcom -93 ../rtl/sdb/sdb_meta_pkg.vhd 

vcom -93 ../rtl/utils/RisingDetect.vhd
vcom -93 ../rtl/utils/Mult36x36_signed.vhd
vcom -93 ../rtl/integral/Bdot.vhd

vcom -93 TB_Bdot.vhd 

vsim -L unisim work.TB_Bdot -voptargs="+acc"

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave_dot.do

run 100us
wave zoomfull
radix -hex
