vlib work

vcom -93 ../rtl/Config_BTrain.vhd

vcom -93 ../rtl/utils/RisingDetect.vhd
vcom -93 ../rtl/utils/divider.vhd
vcom -93 ../rtl/adc_dac/ADOverRange.vhd

vcom -93 TB_OverRange.vhd 

vsim -L unisim work.TB_OverRange -voptargs="+acc"

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave_calc_overrange.do

run 100us
wave zoomfull
radix -hex
