vlib work

vcom -93 ../rtl/btrain_pkg.vhd

vcom -93 ../rtl/utils/utils_pkg.vhd
vcom -93 ../rtl/utils/RisingDetect.vhd
vcom -93 ../rtl/utils/divider.vhd
vcom -93 ../rtl/utils/Mult36x36_signed.vhd
vcom -93 ../rtl/utils/BCD_counter.vhd
vcom -93 ../rtl/utils/clock_divider.vhd

vcom -93 ../rtl/calib/calib_pkg.vhd
vcom -93 ../rtl/calib/ctrl_calib.vhd
vcom -93 ../rtl/calib/calibration.vhd
vcom -93 ../rtl/calib/switch_ctrl.vhd
vcom -93 ../rtl/calib/calib_cmd_dac.vhd
vcom -93 ../rtl/calib/mean_measurement.vhd
vcom -93 ../rtl/calib/gain_offset_calc.vhd
vcom -93 ../rtl/calib/offset_calc.vhd
vcom -93 ../rtl/calib/btrain_calib.vhd

vcom -93 ../rtl/adc_dac/adc_dac_pkg.vhd
vcom -93 ../rtl/adc_dac/switch_dac.vhd
vcom -93 ../rtl/adc_dac/AD5791.vhd
vcom -93 ../rtl/adc_dac/AD7986.vhd
vcom -93 ../rtl/adc_dac/correction.vhd
vcom -93 ../rtl/adc_dac/Acquisition_AD7986_corr.vhd

#vcom -93 calib.vhd 
vcom -93 TB_calib.vhd 

vsim -L unisim work.TB_calib -voptargs="+acc"

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave_calib.do

run 200us
wave zoomfull
radix -hex
