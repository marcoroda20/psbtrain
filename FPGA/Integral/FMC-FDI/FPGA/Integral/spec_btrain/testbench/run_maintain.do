vlib work

vcom -93 ../rtl/utils/maintain.vhd 

vcom -93 TB_maintain.vhd 

vsim -L unisim work.TB_maintain -voptargs="+acc"

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave_maintain.do

run 100us
wave zoomfull
radix -hex
