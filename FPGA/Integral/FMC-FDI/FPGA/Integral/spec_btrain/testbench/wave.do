onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /main/spec_btrainA/clk_sys
add wave -noupdate /main/spec_btrainA/clk_ref
add wave -noupdate /main/spec_btrainA/clk_gtp
add wave -noupdate -radix hexadecimal /main/spec_btrainA/reset_s
add wave -noupdate /main/L_RST_N
add wave -noupdate -radix hexadecimal /main/spec_btrainA/reset_n_s
add wave -noupdate /main/button
add wave -noupdate -radix hexadecimal /main/spec_btrainA/start_timer_reset_s
add wave -noupdate -radix hexadecimal /main/spec_btrainA/start_timer_counter_s
add wave -noupdate -radix hexadecimal /main/spec_btrainA/aux_buttons_i
add wave -noupdate /main/spec_btrainA/button_send_s
add wave -noupdate /main/spec_btrainA/button_send_km1_s
add wave -noupdate -radix hexadecimal /main/spec_btrainA/sys_clk_pll_locked_s
add wave -noupdate -radix hexadecimal /main/spec_btrainB/led_s
add wave -noupdate /main/spec_btrainA/led_sfp_green_o
add wave -noupdate -divider {SPEC RESET GEN}
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/clk_sys_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/rst_pcie_n_a_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/rst_button_n_a_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/rst_n_o
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/powerup_cnt
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/button_synced_n
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/pcie_synced_n
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/powerup_n
add wave -noupdate -divider {SPEC A - TX Streamer}
add wave -noupdate -radix hexadecimal /main/spec_btrainA/frame_cnt_s
add wave -noupdate -radix hexadecimal /main/spec_btrainA/tx_data
add wave -noupdate -radix hexadecimal /main/spec_btrainA/tx_data_new_s
add wave -noupdate -radix hexadecimal /main/spec_btrainA/tx_force_valid
add wave -noupdate -radix hexadecimal /main/spec_btrainA/tx_dreq
add wave -noupdate -radix hexadecimal /main/spec_btrainA/tx_dreq_d0
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_TX_Streamer/tx_data_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_TX_Streamer/tx_valid_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_TX_Streamer/tx_dreq_o
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_TX_Streamer/src_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_TX_Streamer/src_o
add wave -noupdate -divider {SPEC A - PHY}
add wave -noupdate -radix hexadecimal /main/spec_btrainA/phy_tx_data
add wave -noupdate -radix hexadecimal /main/spec_btrainA/phy_tx_k
add wave -noupdate -radix hexadecimal /main/spec_btrainA/phy_tx_disparity
add wave -noupdate -radix hexadecimal /main/spec_btrainA/phy_tx_enc_err
add wave -noupdate -divider {SPEC B - PHY}
add wave -noupdate -radix hexadecimal /main/spec_btrainB/phy_rx_data
add wave -noupdate -radix hexadecimal /main/spec_btrainB/phy_rx_rbclk
add wave -noupdate -radix hexadecimal /main/spec_btrainB/phy_rx_k
add wave -noupdate -radix hexadecimal /main/spec_btrainB/phy_rx_enc_err
add wave -noupdate -divider {SPEC B - RX streamer}
add wave -noupdate -radix hexadecimal /main/spec_btrainB/U_RX_Streamer/snk_i
add wave -noupdate -radix hexadecimal /main/spec_btrainB/U_RX_Streamer/snk_o
add wave -noupdate -radix hexadecimal /main/spec_btrainB/U_RX_Streamer/rx_data_o
add wave -noupdate -radix hexadecimal /main/spec_btrainB/U_RX_Streamer/rx_valid_o
add wave -noupdate -radix hexadecimal /main/spec_btrainB/U_RX_Streamer/rx_dreq_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/led_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {12669868730 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 152
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 fs} {456907141120 fs}
