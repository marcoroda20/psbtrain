onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_btrain/clk_sti
add wave -noupdate /tb_btrain/reset_sti
add wave -noupdate -divider DMA
add wave -noupdate /tb_btrain/cmp_BTrain/adc_data_rdy_F_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/sync_data_valid_s
add wave -noupdate /tb_btrain/cmp_BTrain/mem_range_o
add wave -noupdate /tb_btrain/cmp_BTrain/acqu_end_o
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/switch_mem_range_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/acqu_end_s
add wave -noupdate -radix unsigned /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/ram_addr_cnt_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/sync_data_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/fifo_data_cnt_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/data_in_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/data_word_s
add wave -noupdate -divider {WB DDR}
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/wb_ddr_stall_t
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/wb_ddr_fifo_din
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/wb_ddr_fifo_dout
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/wb_ddr_fifo_empty
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/wb_ddr_fifo_full
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/wb_ddr_fifo_wr
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/wb_ddr_fifo_rd
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/wb_ddr_fifo_dreq
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/wb_ddr_fifo_wr_en
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acqu_ddr_ctrl/cmp_wb_ddr_fifo/U_Inferred_FIFO/count_o
add wave -noupdate -divider Calibration
add wave -noupdate -divider Decimator
add wave -noupdate -radix hexadecimal /tb_btrain/cmp_BTrain/cmp_decimator/decim_factor_i
add wave -noupdate -radix hexadecimal /tb_btrain/cmp_BTrain/cmp_decimator/data_i
add wave -noupdate -radix hexadecimal /tb_btrain/cmp_BTrain/cmp_decimator/data_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_decimator/d_rdy_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_decimator/d_rdy_s
add wave -noupdate -radix hexadecimal /tb_btrain/cmp_BTrain/cmp_decimator/data_keep_s
add wave -noupdate -radix hexadecimal /tb_btrain/cmp_BTrain/cmp_decimator/data_out_s
add wave -noupdate -radix hexadecimal /tb_btrain/cmp_BTrain/cmp_decimator/data_o
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_decimator/d_rdy_o
add wave -noupdate -divider Acquisition
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/cmp_Acquisition_AD7986_corr/adc_data_rdy_o
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/cmp_Acquisition_AD7986_corr/adc_data_o
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_Acquisition_AD7986_corr/adc_data_rdy_o
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_Acquisition_AD7986_corr/adc_data_o
add wave -noupdate -divider Timing
add wave -noupdate /tb_btrain/cmp_BTrain/calib_status_o
add wave -noupdate -divider Mux
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_mux_dac_val/reg_val_sel_range_i
add wave -noupdate -radix binary /tb_btrain/cmp_BTrain/cmp_mux_dac_val/reg_val_sel_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_mux_dac_val/adc_data_F_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_switch_dac/reg_dac_bus_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_switch_dac/calib_DAC_wr_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_mux_dac_val/val_o
add wave -noupdate /tb_btrain/cmp_BTrain/data_rdy_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_switch_dac/clk_250KHz_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_switch_dac/calib_status_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_switch_dac/calib_run_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_switch_dac/DAC_wr_o
add wave -noupdate -divider DAC
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/adc_data_o
add wave -noupdate /tb_btrain/cmp_BTrain/DAC_n_SYNC_o
add wave -noupdate /tb_btrain/cmp_BTrain/DAC_n_RESET_o
add wave -noupdate /tb_btrain/cmp_BTrain/DAC_n_LDAC_o
add wave -noupdate /tb_btrain/cmp_BTrain/DAC_n_CLR_o
add wave -noupdate /tb_btrain/cmp_BTrain/DAC_end_wr_s
add wave -noupdate /tb_btrain/cmp_BTrain/DAC_end_rd_s
add wave -noupdate /tb_btrain/cmp_BTrain/DAC_data_wr_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_switch_dac/DAC_wr_o
add wave -noupdate /tb_btrain/cmp_BTrain/DAC_data_out_s
add wave -noupdate -divider Correction
add wave -noupdate -radix decimal /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/cmp_Acquisition_AD7986_corr/cmp_correction/adc_i
add wave -noupdate -radix hexadecimal /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/cmp_Acquisition_AD7986_corr/cmp_correction/gain_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/cmp_Acquisition_AD7986_corr/cmp_correction/output_mult_s
add wave -noupdate -radix hexadecimal /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/cmp_Acquisition_AD7986_corr/cmp_correction/offset_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/cmp_Acquisition_AD7986_corr/cmp_correction/state_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/cmp_Acquisition_AD7986_corr/cmp_correction/start_mult_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/cmp_Acquisition_AD7986_corr/cmp_correction/end_m_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/cmp_Acquisition_AD7986_corr/cmp_correction/end_mult_s
add wave -noupdate -radix decimal /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/cmp_Acquisition_AD7986_corr/cmp_correction/adc_o
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_F/cmp_Acquisition_AD7986_corr/adc_corr_rdy_o
add wave -noupdate -divider {DAC Simulation}
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_switch_dac/tick_250KHz_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_switch_dac/DAC_wr_o
add wave -noupdate /tb_btrain/cmp_BTrain/DAC_n_SYNC_o
add wave -noupdate /tb_btrain/SCK_s
add wave -noupdate /tb_btrain/SDI_s
add wave -noupdate /tb_btrain/SDO_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_DAC/s2p
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_DAC/nop_sync_dec
add wave -noupdate /tb_btrain/word_dac_spi_s
add wave -noupdate /tb_btrain/dac_reg_out_s
add wave -noupdate /tb_btrain/dac_control_s
add wave -noupdate /tb_btrain/dac_software_s
add wave -noupdate /tb_btrain/dac_read_back_s
add wave -noupdate -radix decimal /tb_btrain/n_s
add wave -noupdate /tb_btrain/DAC_spi_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_DAC/end_wr_cycle
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_DAC/end_rd_cycle
add wave -noupdate -divider Integral
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/BKp1_o
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/B_data_rdy_o
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/Bk_3_8_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/Bk_rect_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/Bsmooth_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/C0_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/Vo_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/adc_data_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/adc_data_rdy_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/clk_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/corr_fact_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/data_rdy_3_8_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/data_rdy_rect_s
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/high_marker_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/high_marker_val_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/init_Bk_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/int_c0_used_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/int_range_sel_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/low_marker_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/low_marker_val_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/reg_val_cfg_sel_integral_i
add wave -noupdate /tb_btrain/cmp_BTrain/cmp_acquisition_core_D/cmp_BTrain_integral/reset_i
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9685 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 356
configure wave -valuecolwidth 425
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {9386 ns} {9950 ns}
