onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_bcalcul/uut/clk_i
add wave -noupdate /tb_bcalcul/uut/reset_i
add wave -noupdate /tb_bcalcul/uut/start_i
add wave -noupdate -divider {Input values}
add wave -noupdate /tb_bcalcul/uut/init_Bk_i
add wave -noupdate /tb_bcalcul/uut/Vk_i
add wave -noupdate /tb_bcalcul/uut/Vo_i
add wave -noupdate /tb_bcalcul/uut/Bk_i
add wave -noupdate /tb_bcalcul/uut/Bsmooth_i
add wave -noupdate /tb_bcalcul/uut/deltat_i
add wave -noupdate /tb_bcalcul/uut/corr_fact_i
add wave -noupdate -divider {Output values}
add wave -noupdate /tb_bcalcul/uut/BKp1_o
add wave -noupdate /tb_bcalcul/uut/fifo_wen_o
add wave -noupdate /tb_bcalcul/uut/data_rdy_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {239 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 269
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ns} {394 ns}
