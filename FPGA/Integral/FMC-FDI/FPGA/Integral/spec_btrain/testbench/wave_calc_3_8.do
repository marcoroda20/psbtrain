onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_bcalcul3_8/uut/reset_i
add wave -noupdate /tb_bcalcul3_8/uut/clk_i
add wave -noupdate -divider Input
add wave -noupdate /tb_bcalcul3_8/uut/start_i
add wave -noupdate /tb_bcalcul3_8/uut/init_Bk_i
add wave -noupdate -radix decimal /tb_bcalcul3_8/uut/Vk_i
add wave -noupdate -radix decimal /tb_bcalcul3_8/uut/Bk_i
add wave -noupdate /tb_bcalcul3_8/uut/C0_i
add wave -noupdate -divider {State Machine}
add wave -noupdate /tb_bcalcul3_8/uut/state_s
add wave -noupdate -divider Output
add wave -noupdate -radix decimal /tb_bcalcul3_8/uut/BKp1_o
add wave -noupdate /tb_bcalcul3_8/uut/data_rdy_o
add wave -noupdate -divider {Intern calculation}
add wave -noupdate -radix decimal /tb_bcalcul3_8/uut/Vkm3_3Vkm2_s
add wave -noupdate -radix decimal /tb_bcalcul3_8/uut/Vk_3Vkm1_s
add wave -noupdate -radix decimal /tb_bcalcul3_8/uut/sum_s
add wave -noupdate -radix decimal /tb_bcalcul3_8/uut/sum_3_s
add wave -noupdate /tb_bcalcul3_8/uut/Bkm3_s
add wave -noupdate -radix decimal /tb_bcalcul3_8/uut/bk_sum_3_8_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2295 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 274
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ns} {1694 ns}
