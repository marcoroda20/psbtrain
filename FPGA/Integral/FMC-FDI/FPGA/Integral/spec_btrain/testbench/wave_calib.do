onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_calib/clk_sti
add wave -noupdate /tb_calib/reset_sti
add wave -noupdate -divider Stimuli
add wave -noupdate /tb_calib/zero_cycle_sti
add wave -noupdate /tb_calib/force_cal_sti
add wave -noupdate /tb_calib/force_cal_obs
add wave -noupdate /tb_calib/force_cal_load_obs
add wave -noupdate -divider {BCD Counter and time}
add wave -noupdate /tb_calib/uut/reg_calib_delay_i
add wave -noupdate /tb_calib/uut/cmp_ctrl_calib/t_next_calib_o
add wave -noupdate -divider {State Machine}
add wave -noupdate -height 16 /tb_calib/uut/cmp_calibration/state_s
add wave -noupdate -height 16 /tb_calib/uut/cmp_calibration/state_s
add wave -noupdate /tb_calib/N_SYNC_s
add wave -noupdate /tb_calib/uut/cmp_calibration/switch_cmd_o
add wave -noupdate /tb_calib/uut/cmp_switch_crtl/wait_counter_s
add wave -noupdate /tb_calib/uut/cmp_switch_crtl/start_switch_i
add wave -noupdate /tb_calib/uut/cmp_switch_crtl/end_switch_o
add wave -noupdate -divider Ctrl_TB_calib
add wave -noupdate /tb_calib/uut/cmp_ctrl_calib/start_calib_o
add wave -noupdate -height 16 /tb_calib/uut/cmp_ctrl_calib/state_s
add wave -noupdate /tb_calib/uut/cmp_ctrl_calib/time_calib_reg_s
add wave -noupdate /tb_calib/uut/zero_cycle_i
add wave -noupdate /tb_calib/uut/force_cal_i
add wave -noupdate /tb_calib/uut/cmp_ctrl_calib/cnt_delay_s
add wave -noupdate /tb_calib/uut/cmp_ctrl_calib/end_delays_s
add wave -noupdate -divider Switch
add wave -noupdate /tb_calib/uut/start_switch_s
add wave -noupdate /tb_calib/uut/end_switch_s
add wave -noupdate /tb_calib/open1_obs
add wave -noupdate /tb_calib/gain1_obs
add wave -noupdate /tb_calib/off1_obs
add wave -noupdate /tb_calib/open2_obs
add wave -noupdate /tb_calib/gain2_obs
add wave -noupdate /tb_calib/off2_obs
add wave -noupdate -divider {DAC command}
add wave -noupdate -height 16 /tb_calib/uut/cmp_dac_cmd/state_s
add wave -noupdate /tb_calib/uut/cmp_dac_cmd/start_dac_i
add wave -noupdate /tb_calib/uut/cmp_dac_cmd/end_dac_o
add wave -noupdate /tb_calib/uut/cmp_dac_cmd/DAC_val_register_i
add wave -noupdate -divider {ADC simulation}
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/serial2paral_falling_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/serial2paral_rising_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_off_read_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_off_read_1_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_off_read_2_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_on_read_1_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_on_read_2_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_read_on_s
add wave -noupdate /tb_calib/clk_cnv_sti
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_n_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_p_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_n_1_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_p_1_s
add wave -noupdate -height 16 /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/state_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/counter_acquisition_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/counter_data_conversion_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/clk_i
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sync_i
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_on_read_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_cnv_o
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_o
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/load_data_out_1_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/load_data_out_s
add wave -noupdate -radix unsigned /tb_calib/cnt_bit_adc_F_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_off_read_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_sck_read_on_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_cnv_n_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_cnv_p_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_cnv_n_1_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_AD7986/adc_cnv_p_1_s
add wave -noupdate /tb_calib/sdo1_F_s
add wave -noupdate /tb_calib/sdo2_F_s
add wave -noupdate /tb_calib/adc_data_rdy_F_s
add wave -noupdate /tb_calib/adc_data_rdy_D_s
add wave -noupdate /tb_calib/adc_data_F_s
add wave -noupdate /tb_calib/adc_data_D_s
add wave -noupdate -divider Measurement_F
add wave -noupdate /tb_calib/simulated_val_pos_sti
add wave -noupdate /tb_calib/simulated_val_neg_sti
add wave -noupdate -height 16 /tb_calib/uut/cmp_calibration/state_s
add wave -noupdate -height 16 /tb_calib/uut/cmp_mean_meas_F/state_s
add wave -noupdate -radix decimal /tb_calib/uut/cmp_mean_meas_F/adc_data_i
add wave -noupdate /tb_calib/uut/cmp_mean_meas_F/start_meas_i
add wave -noupdate -radix decimal /tb_calib/uut/cmp_mean_meas_F/acc_data_input_s
add wave -noupdate -radix decimal /tb_calib/uut/cmp_mean_meas_F/acc_data_s
add wave -noupdate -radix decimal /tb_calib/uut/cmp_mean_meas_F/nb_of_meas_reg_i
add wave -noupdate /tb_calib/uut/cmp_mean_meas_F/nb_of_meas_cnt_s
add wave -noupdate /tb_calib/uut/cmp_mean_meas_F/divider_output_s
add wave -noupdate /tb_calib/uut/cmp_mean_meas_F/divisor_s
add wave -noupdate /tb_calib/uut/cmp_mean_meas_F/cmp_mean_divider/sign_s
add wave -noupdate -radix decimal /tb_calib/uut/cmp_mean_meas_F/val_mean_meas_o
add wave -noupdate -divider Measurement_F
add wave -noupdate -height 16 /tb_calib/uut/cmp_mean_meas_D/state_s
add wave -noupdate /tb_calib/uut/cmp_mean_meas_D/val_mean_meas_o
add wave -noupdate -height 16 /tb_calib/uut/cmp_calibration/state_s
add wave -noupdate /tb_calib/DAC_spi_s
add wave -noupdate /tb_calib/SDI_s
add wave -noupdate /tb_calib/SDO_s
add wave -noupdate /tb_calib/SCK_s
add wave -noupdate -divider Divider
add wave -noupdate -height 16 /tb_calib/uut/cm_gain_offset_F/cmp_gain_divider/state_s
add wave -noupdate /tb_calib/uut/cm_gain_offset_F/cmp_gain_divider/a_i
add wave -noupdate /tb_calib/uut/cm_gain_offset_F/cmp_gain_divider/b_i
add wave -noupdate /tb_calib/uut/cm_gain_offset_F/cmp_gain_divider/q_o
add wave -noupdate /tb_calib/uut/cm_gain_offset_F/cmp_gain_divider/r_o
add wave -noupdate /tb_calib/uut/cm_gain_offset_F/cmp_gain_divider/f_o
add wave -noupdate -divider {Gain Offset Calculation}
add wave -noupdate -height 16 /tb_calib/uut/cm_gain_offset_F/state_s
add wave -noupdate /tb_calib/uut/cm_gain_offset_F/ADC_pos_ref_i
add wave -noupdate -radix hexadecimal /tb_calib/uut/cm_gain_offset_F/cmp_offet_calc/val_pos_i
add wave -noupdate -radix hexadecimal /tb_calib/uut/cm_gain_offset_F/cmp_offet_calc/val_neg_i
add wave -noupdate -radix hexadecimal /tb_calib/uut/cm_gain_offset_F/cmp_offet_calc/val_s
add wave -noupdate -radix hexadecimal /tb_calib/uut/cm_gain_offset_D/cmp_offet_calc/val_s
add wave -noupdate /tb_calib/uut/cm_gain_offset_F/dividend_s
add wave -noupdate /tb_calib/uut/cm_gain_offset_F/divisor_s
add wave -noupdate /tb_calib/uut/cmp_calibration/offset_calc_F_o
add wave -noupdate /tb_calib/uut/cmp_calibration/offset_meas_F_o
add wave -noupdate /tb_calib/uut/cmp_calibration/offset_calc_D_o
add wave -noupdate /tb_calib/uut/cmp_calibration/offset_meas_D_o
add wave -noupdate /tb_calib/uut/cmp_calibration/gain_F_o
add wave -noupdate /tb_calib/uut/cmp_calibration/gain_D_o
add wave -noupdate -height 16 /tb_calib/uut/cmp_calibration/state_s
add wave -noupdate -divider {Switch DAC}
add wave -noupdate /tb_calib/cmp_switch_dac/reg_val_sel_fpga_bus_i
add wave -noupdate /tb_calib/cmp_switch_dac/calib_run_s
add wave -noupdate /tb_calib/cmp_switch_dac/calib_end_dac_wr_o
add wave -noupdate /tb_calib/cmp_switch_dac/calib_end_dac_rd_o
add wave -noupdate /tb_calib/cmp_switch_dac/calib_DAC_wr_i
add wave -noupdate /tb_calib/cmp_switch_dac/calib_DAC_rd_o
add wave -noupdate /tb_calib/cmp_switch_dac/DAC_wr_o
add wave -noupdate /tb_calib/cmp_switch_dac/DAC_rd_i
add wave -noupdate /tb_calib/cmp_switch_dac/end_dac_wr_i
add wave -noupdate /tb_calib/cmp_switch_dac/end_dac_rd_i
add wave -noupdate /tb_calib/uut/cmp_dac_cmd/DAC_cmd_i
add wave -noupdate -divider AD5791
add wave -noupdate -height 16 /tb_calib/uut/cmp_calibration/state_s
add wave -noupdate /tb_calib/cmp_dac/DATA_WR
add wave -noupdate /tb_calib/cmp_dac/data_modif
add wave -noupdate /tb_calib/N_SYNC_s
add wave -noupdate -height 16 /tb_calib/uut/cmp_calibration/state_s
add wave -noupdate /tb_calib/DAC_wr_s
add wave -noupdate /tb_calib/DAC_spi_s
add wave -noupdate /tb_calib/cmp_dac/s2p
add wave -noupdate /tb_calib/cmp_dac/SDI
add wave -noupdate /tb_calib/cmp_dac/SDO
add wave -noupdate /tb_calib/cmp_dac/SCK
add wave -noupdate -divider {ADC without correction}
add wave -noupdate /tb_calib/adc_data_rdy_F_s
add wave -noupdate /tb_calib/adc_data_F_s
add wave -noupdate /tb_calib/adc_data_rdy_D_s
add wave -noupdate /tb_calib/adc_data_D_s
add wave -noupdate -divider {Output Measurement}
add wave -noupdate /tb_calib/uut/cmp_calibration/dac_nvref_s
add wave -noupdate /tb_calib/uut/cmp_calibration/dac_pvref_s
add wave -noupdate /tb_calib/uut/val_pos_F_o
add wave -noupdate /tb_calib/uut/val_pos_D_o
add wave -noupdate /tb_calib/uut/val_neg_F_o
add wave -noupdate /tb_calib/uut/val_neg_D_o
add wave -noupdate -divider {Gain divider}
add wave -noupdate -radix hexadecimal /tb_calib/uut/cm_gain_offset_F/gain_o
add wave -noupdate /tb_calib/uut/cm_gain_offset_F/cmp_gain_divider/q_o
add wave -noupdate /tb_calib/uut/cm_gain_offset_F/cmp_gain_divider/f_o
add wave -noupdate -divider Mult
add wave -noupdate /tb_calib/cmp_AD7986_corr_D/cmp_correction/cmp_Mult36x36_signed/start_i
add wave -noupdate /tb_calib/cmp_AD7986_corr_D/cmp_correction/cmp_Mult36x36_signed/A_i
add wave -noupdate /tb_calib/cmp_AD7986_corr_D/cmp_correction/cmp_Mult36x36_signed/B_i
add wave -noupdate /tb_calib/cmp_AD7986_corr_D/cmp_correction/cmp_Mult36x36_signed/result_o
add wave -noupdate /tb_calib/cmp_AD7986_corr_D/cmp_correction/cmp_Mult36x36_signed/result_rdy_o
add wave -noupdate -divider Correction
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_correction/adc_o
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_correction/adc_drdy_o
add wave -noupdate /tb_calib/cmp_AD7986_corr_D/cmp_correction/adc_o
add wave -noupdate /tb_calib/cmp_AD7986_corr_D/cmp_correction/adc_drdy_o
add wave -noupdate /tb_calib/adc_drdy_F_obs
add wave -noupdate /tb_calib/adc_F_obs
add wave -noupdate /tb_calib/adc_drdy_D_obs
add wave -noupdate /tb_calib/adc_D_obs
add wave -noupdate -height 16 /tb_calib/uut/cmp_calibration/state_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_correction/adc_i
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_correction/gain_i
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_correction/adc_o
add wave -noupdate -radix decimal /tb_calib/cmp_AD7986_corr_F/cmp_correction/output_mult_s
add wave -noupdate -radix decimal /tb_calib/cmp_AD7986_corr_F/cmp_correction/offset_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_correction/adc_s
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_correction/gain_i
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_correction/offset_i
add wave -noupdate /tb_calib/cmp_AD7986_corr_F/cmp_correction/offset_i
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {13473338 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 234
configure wave -valuecolwidth 77
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2801724 ps} {63636316 ps}
