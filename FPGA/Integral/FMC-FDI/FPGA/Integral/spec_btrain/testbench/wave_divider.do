onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_divider/uut/clk_i
add wave -noupdate /tb_divider/uut/reset_i
add wave -noupdate /tb_divider/uut/NB_BIT
add wave -noupdate -divider {State machine}
add wave -noupdate /tb_divider/uut/start_divider_i
add wave -noupdate /tb_divider/uut/start_divider_s
add wave -noupdate /tb_divider/uut/next_state_s
add wave -noupdate -divider Input
add wave -noupdate /tb_divider/uut/a_i
add wave -noupdate /tb_divider/uut/b_i
add wave -noupdate -divider {Internal signals}
add wave -noupdate /tb_divider/uut/q_s
add wave -noupdate /tb_divider/uut/r_s
add wave -noupdate -divider Output
add wave -noupdate /tb_divider/uut/divide_by_0_o
add wave -noupdate /tb_divider/uut/end_divider_o
add wave -noupdate /tb_divider/uut/q_o
add wave -noupdate /tb_divider/uut/r_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1109 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 152
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1417 ns} {2875 ns}
