onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_bdot/uut/clk_i
add wave -noupdate /tb_bdot/uut/reset_i
add wave -noupdate -divider Input
add wave -noupdate -radix decimal /tb_bdot/uut/corr_fact_i
add wave -noupdate -radix decimal /tb_bdot/uut/Vk_i
add wave -noupdate -radix decimal /tb_bdot/uut/Vo_i
add wave -noupdate -divider {State Machine}
add wave -noupdate /tb_bdot/uut/state_s
add wave -noupdate /tb_bdot/uut/start_s
add wave -noupdate /tb_bdot/uut/start_mult_s
add wave -noupdate /tb_bdot/uut/end_mult_s
add wave -noupdate /tb_bdot/uut/end_mult_in_s
add wave -noupdate -divider Output
add wave -noupdate /tb_bdot/uut/data_rdy_o
add wave -noupdate -radix decimal /tb_bdot/uut/Bdot_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {265 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {5 ns} {893 ns}
