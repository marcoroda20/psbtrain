#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <time.h>
#include "bmeas-lib.h"

int main (int argc, char * argv []) {
	int fd,i,busNo;
	int32_t max,index_max;
	struct bmeasd_reg     read_reg;
	//struct bmeasd_dma     dma;
	struct bmeasd_dma_tag dma_tag;
	struct bmeasd_cycle   test_cycle;
	//struct firmware       frmw_name;
	//uint32_t *last_cycle_data;
	char filename[13];// = "/dev/fmc-0300";
	int arg_index = 0;
	int m;
	double diff;
	uint32_t last_dma_tag=0;
	uint32_t last_cycle_address;
	struct timespec tstart={0,0}, tend={0,0};
	
	test_cycle.pdata = (unsigned int *)malloc(4*DMA_DATA_LENGTH*SIZE_DMA_DATA);
	test_cycle.size = DMA_DATA_LENGTH;

	busNo=3;
	if ( (argc-arg_index)>=3) 
	{
		if (strcmp(argv[1], cmd_mBUS) == 0)  
		{
			arg_index += 2;
			busNo=atoi(argv[2]);
			//filename[10]=*argv[2];
		}
	}
	sprintf(filename,"/dev/fmc-0%d00",busNo);
	fd = open(filename, O_RDWR);

	if (fd == -1)
	{
		printf("Error in opening file \n");
		exit(-1);
	}
	printf("File opened \n");

	if ( (argc-arg_index)>=2) 
	{

		if (strcmp(argv[1+arg_index], cmd_READ) == 0)  
		{
			printf("Argument : %s\n",cmd_READ);
			//READ example
			if ( (argc-arg_index)>=3) {
				sscanf(argv[2+arg_index], "%x", &read_reg.addr);
			}else{
				read_reg.addr = 0x5000;
			}
			printf("Address = %08x\n",read_reg.addr);
			ioctl(fd,BMEAS_IOC_READ_REG,&read_reg);   
			printf("BMEAS_IOC_READ_REG : %08x\n",BMEAS_IOC_READ_REG);
			printf("READ register %08x => %08x\n",read_reg.addr,read_reg.data);
		}
		if (strcmp(argv[1+arg_index], cmd_WRITE) == 0)  
		{
			//WRITE example
			if ( (argc-arg_index)>=3) {
				sscanf(argv[2+arg_index], "%x", &read_reg.addr);
			}else{
				read_reg.addr = 0x5000;
			}
			printf("Addresse = %08x\n",read_reg.addr);
			if ( (argc-arg_index)>=4) {
				sscanf(argv[3+arg_index], "%x", &read_reg.data);
			}
			else{
				ioctl(fd,BMEAS_IOC_READ_REG,&read_reg);   
				read_reg.data = read_reg.data | 0x80;
			}
			printf("Data     = %08x\n",read_reg.data);
			printf("WRITE register %08x => %08x\n",read_reg.addr,read_reg.data);
			ioctl(fd,BMEAS_IOC_WRITE_REG,&read_reg);  
			printf("after write reg. %08x => %08x\n",read_reg.addr,read_reg.data);
		}
		if (strcmp(argv[1+arg_index], cmd_START_DMA) == 0)  
		{
			printf("Argument : %s\n",cmd_START_DMA);
			//dma.size=DMA_DATA_LENGTH;
			//printf("dma.size : %d\n",dma.size);
			//ioctl(fd,BMEAS_IOC_START_DMA,&dma);  //ioctl call
			ioctl(fd,BMEAS_IOC_START_DMA);  //ioctl call
		}
		if (strcmp(argv[1+arg_index], cmd_STOP_DMA) == 0)  
		{
			printf("Argument : %s\n",cmd_STOP_DMA);
			ioctl(fd,BMEAS_IOC_STOP_DMA);  //ioctl call
		}
		if (strcmp(argv[1+arg_index], cmd_DMA_TAG) == 0)
		{
			printf("Argument : %s\n",cmd_DMA_TAG);
			ioctl(fd,BMEAS_IOC_DMA_TAG,&dma_tag);  //ioctl call
			printf("DMA tag : %d\n",dma_tag.dma_tag);
		}
		if (strcmp(argv[1+arg_index], cmd_LAST_CYCLE) == 0)
		{
			max=0;
			printf("Argument : %s\n",cmd_LAST_CYCLE);
			//read the adress of the last sample for the size of last cycle
			read_reg.addr = 0x5010;
			ioctl(fd,BMEAS_IOC_READ_REG,&read_reg);
			last_cycle_address = read_reg.data;
			test_cycle.size = (last_cycle_address)*4;

			ioctl(fd,BMEAS_IOC_LAST_CYCLE,&test_cycle);  //ioctl call
			for (i=0;i<test_cycle.size/2;i++)
			{
				if ((i>=0)&(i<16)){
					//printf("Buffer[%d] : 0x%04x \n",i,*(((unsigned short *)test_cycle.pdata)+i));
					printf("Buffer[%d] : 0x%08x \n",i,*((test_cycle.pdata)+i));
				}
				if (i>DMA_DATA_LENGTH-17){
					//printf("Buffer[%d] : 0x%04x \n",i,*(((unsigned short *)test_cycle.pdata)+i));
					printf("Buffer[%d] : 0x%08x \n",i,*((test_cycle.pdata)+i));
				}
				if (i%4==2)
				{
					if (((int32_t)*((test_cycle.pdata)+i))>max) 
					{
						max = *((test_cycle.pdata)+i);
						index_max = i/4;
					}
				}
			}
			printf("Max B int32_t : %d with the index : %d \n",max,index_max);
			printf("Size of buffer : %ld \n",test_cycle.size);

		}
		if (strcmp(argv[1+arg_index], cmd_FORCE_DMA) == 0)
		{
			printf("Argument : %s\n",cmd_FORCE_DMA);
			ioctl(fd,BMEAS_IOC_FORCE_DMA,&dma_tag);
			printf("Force DMA tag : %d\n",dma_tag.dma_tag);
		}
		if (strcmp(argv[1+arg_index], cmd_TIMING) == 0)
		{
			/*
			ioctl(fd,BMEAS_IOC_START_DMA);  //ioctl call
			clock_gettime(CLOCK_MONOTONIC, &tstart);
			for (m=0;m<5000;m++)
			{
				ioctl(fd,BMEAS_IOC_DMA_TAG,&dma_tag);  //ioctl call
				if (dma_tag.dma_tag!=last_dma_tag)
				{
					clock_gettime(CLOCK_MONOTONIC, &tend);
					diff = ((double)tend.tv_sec + 1.0e-9*tend.tv_nsec) - ((double)tstart.tv_sec + 1.0e-9*tstart.tv_nsec);
					printf("Difference of timing %.5f seconds\n",diff);
					last_dma_tag=dma_tag.dma_tag;
					clock_gettime(CLOCK_MONOTONIC, &tstart);
				}
				usleep(10000);
			}
			ioctl(fd,BMEAS_IOC_STOP_DMA);  //ioctl call
			*/
		}
	}
	close(fd);
	printf("File closed \n");
	return 0;
}
