clc,close all;
% Generate Coefficient and Reference files

Bk = 0;
Vk = 0;
Bsmooth = 800;

f=20;
t=(0:0.001:0.5);
signal = floor(1000*cos(2*pi*f*t+pi/2));

pathname = '..\spec_btrain\testbench';

[Bk]=writefile3_8(Bk,Vk,Bsmooth,signal,pathname);
