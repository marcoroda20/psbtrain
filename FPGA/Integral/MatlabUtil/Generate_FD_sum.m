clc,close all;
% Generate FD_sum example

pathname=['C:\PSBTrain\FPGA\Integral\spec_btrain\testbench'];

BF = [
12345
65535
2^30
2^20
2^31-1
2^17-1
1
];

GF = [
(-2)^30
(-2)^30
(-2)^30
(-2)^30
(-2)^30
(-2)^30
(-2)^30
% 54321
% 1024
% 2^30
% 2^20-1
% 2^31-1
% 2^19-1
% 0
];

BD = [
12345
65535
2^30
2^20
2^31-1
2^17-1
1
];

GD = [
(-2)^30
(-2)^30
(-2)^30
(-2)^30
(-2)^30
(-2)^30
(-2)^30
% 234
% 2401
% 2^31-1
% 2^19-1
% 2^31-1
% 0
% 2^23
];

ScaleBdot = [
1
1
1
1
1
1
1

% (-2)^30
% (-2)^30
% (-2)^30
% (-2)^30
% (-2)^30
% (-2)^30
% (-2)^30
];

result=floor((BF.*GF+BD.*GD)/2^32);

if (strcmp(pathname,'')==1)
    fid_sti = fopen('FD_sum.dat', 'w');
    fid_ref = fopen('ref_FD_sum.dat', 'w');
else
    fid_sti = fopen([pathname '\FD_sum.dat'], 'w');
    fid_ref = fopen([pathname '\ref_FD_sum.dat'], 'w');
end

%% Write files
[count] = fprintf(fid_sti,'Number of values \n');
[count] = fprintf(fid_sti,'%d \n', length(result));
[count] = fprintf(fid_sti,'BF,GainF,BD,GainD ScaleBdot\n');
% [count] = fprintf(fid_sti,'%u,%u,%u,%u \n',[BF GF BD GD]');
[count] = fprintf(fid_sti,'%20.0f %20.0f %20.0f %20.0f %20.0f \n',[BF GF BD GD ScaleBdot]');

%% Write reference file : 
[count] = fprintf(fid_ref,'%20.0f \n',result);

fclose(fid_sti);
fclose(fid_ref);


