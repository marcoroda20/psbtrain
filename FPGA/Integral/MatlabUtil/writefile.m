%
% Generation of file for simulation of Marco's integral
%
% TestBench : 


function [Bkp1]=writefile(Bk,Bsmooth,corr_fact,Vk,Vo,deltat,low_marker,high_marker,signal,pathname)

if nargin<10
    pathname=[''];
end

if (strcmp(pathname,'')==1)
    fid_sti = fopen('coeff.dat', 'w');
    fid_ref = fopen('ref.dat', 'w');
else
    fid_sti = fopen([pathname '\coeff.dat'], 'w');
    fid_ref = fopen([pathname '\ref.dat'], 'w');
end

%% Write coefficient file
% coefficient
[count] = fprintf(fid_sti,'Bk Bsmooth corr_fact Vk Vo deltat low_marker high_marker \n');
fact=floor(corr_fact*2^31);

if fact>=2^31
    corr_reg = fact-2^32;
else
    corr_reg = fact;
end
[count] = fprintf(fid_sti,'%ld %ld %ld %ld %ld %ld %ld %ld \n', [Bk Bsmooth corr_reg Vk Vo deltat low_marker high_marker]);
% signal %ld
[count] = fprintf(fid_sti,'Signal : \n');
[count] = fprintf(fid_sti,'%d \n',length(signal));
for n=1:length(signal)
    [count] = fprintf(fid_sti,'%d\n',signal(n));
end

%% Write reference file : 
Bkp1=floor(Bk+(fact/2^31)*(Vk-Vo));%+Bsmooth;
[count] = fprintf(fid_ref,'After initialization : \n');
[count] = fprintf(fid_ref,'%d \n',Bkp1);
% integral of the signal
[count] = fprintf(fid_ref,'Signal : \n');
Bk=Bkp1;
Bkp1=zeros(length(signal),1);
error_smooth=zeros(length(signal)+1,1);

for n=1:length(signal)
    if (mod(n-1,20)==0) && (n~=1)
       Bkp1(n)=0;
       error_smooth(n+1)=0;
%         if abs(-1*Bk+error_smooth(n))>Bsmooth
%            Bkp1(n)=floor(Bk+...
%                sign(-1*Bk+error_smooth(n))*Bsmooth);
%            error_smooth(n+1)=floor(error_smooth(n)-...
%                Bk-sign(-1*Bk+error_smooth(n))*Bsmooth);
%         else
%            Bkp1(n)=floor(error_smooth(n));
%            error_smooth(n+1)=0;
%         end
        
    elseif (mod(n-1,20)==3)
        if abs(low_marker-Bk+error_smooth(n))>Bsmooth
           Bkp1(n)=floor(Bk+...
               sign(low_marker-Bk+error_smooth(n))*Bsmooth);
           error_smooth(n+1)=floor(error_smooth(n)+...
               (low_marker-Bk)-...
               sign(low_marker-Bk+error_smooth(n))*Bsmooth);
        else
           Bkp1(n)=floor(low_marker+error_smooth(n));
           error_smooth(n+1)=0;
        end
    
    elseif (mod(n-1,20)==17)
        if abs(high_marker-Bk+error_smooth(n))>Bsmooth
           Bkp1(n)=floor(Bk+...
               sign(high_marker-Bk+error_smooth(n))*Bsmooth);
           error_smooth(n+1)=floor(error_smooth(n)+...
               (high_marker-Bk)-...
               sign(high_marker-Bk+error_smooth(n))*Bsmooth);
        else
           Bkp1(n)=floor(high_marker+error_smooth(n));
           error_smooth(n+1)=0;
        end
    
    else
        if abs((fact/2^31)*(signal(n)-Vo)+error_smooth(n))>Bsmooth
           Bkp1(n)=floor(Bk+...
               sign((fact/2^31)*(signal(n)-Vo)+error_smooth(n))*Bsmooth);
           error_smooth(n+1)=floor(error_smooth(n)+...
               (fact/2^31)*(signal(n)-Vo)-...
               sign((fact/2^31)*(signal(n)-Vo)+error_smooth(n))*Bsmooth);
        else
           Bkp1(n)=floor(Bk+(fact/2^31)*(signal(n)-Vo)+error_smooth(n));
           error_smooth(n+1)=0;
        end
    end        
    [count] = fprintf(fid_ref,'%d \n',Bkp1(n));
    Bk=Bkp1(n);
    if Bkp1(n)==-33
            Bkp1(n)
    end
    if error_smooth(n+1)==515
            error_smooth(n)
    end
end

figure;
subplot(3,1,1)
plot(signal,'b');
ylabel('B')
hold on;
grid on;
subplot(3,1,2)
plot(Bkp1/2^31,'r');
hold on;
grid on;
plot(error_smooth,'k:');
subplot(3,1,3)
plot(error_smooth,'b');
grid on;

fclose(fid_sti);
fclose(fid_ref);
