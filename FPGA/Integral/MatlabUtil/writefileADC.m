%
% Generation of file for simulation of ADC acquisition
%


function [signal18bits]=writefileADC(signal,pathname)

V_max=5;

if nargin<2
    pathname=[''];
end

if (strcmp(pathname,'')==1)
    fid_sti = fopen('adc.dat', 'w');
else
    fid_sti = fopen([pathname '\adc.dat'], 'w');
end

%% Write ADC value file
signal18bits = round(2^17*signal/V_max);
% signal
for n=1:length(signal18bits)
    [count] = fprintf(fid_sti,'%d\n',signal18bits(n));
end

fclose(fid_sti);
