----------------------------------------------------------------------------------
-- Company: CERN TE/MSC-MM
-- Engineer: David Giloteaux
-- 
-- Create Date:    11:59:06 07/03/2015 
-- Design Name: 
-- Module Name:    Bup_Bdown - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: Bup / Bdown 32-Bit counter
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
-- library UNISIM;
-- use UNISIM.VComponents.all;

entity Bup_Bdown is

    Port ( Gbl_Reset_i    : in  STD_LOGIC; --Active High
	        C0_Reset_i     : in  STD_LOGIC; --Active High
           Bup_i          : in  STD_LOGIC; --Active High
           Bdown_i        : in  STD_LOGIC; -- Active High
			  --C0 pulse programmable delay in clock period number
			  --pdly_cst_i     : in  STD_LOGIC_VECTOR (19 downto 0);
			  Bupdown_drdy_o : out STD_LOGIC; -- Active High
           B_Cnt_o        : out STD_LOGIC_VECTOR (31 downto 0);
			  Scale_B_Cnt_o  : out STD_LOGIC_VECTOR (31 downto 0); 
           Clk_i          : in  STD_LOGIC ); --10MHz
			  
end Bup_Bdown;

architecture Behavioral of Bup_Bdown is

signal ce_cnt_s, tcpdlycnt_s, tc_pdly_cnt_s           : std_logic := '0';  
signal pdly_cnt_s, pdly_cst_s                         : std_logic_vector(19 downto 0) := x"00000"; 

signal Bup1_s, Bup2_s, Bup_rise_s, Bup_fall_s         : std_logic := '0'; 
signal Bdown1_s, Bdown2_s, Bdown_rise_s, Bdown_fall_s : std_logic := '0';

signal C0_rst_rise1_s, C0_rst_rise2_s, C0_rst_rise_s  : std_logic := '0';
signal C0_rst_fall1_s, C0_rst_fall2_s, C0_rst_fall_s  : std_logic := '0';
signal C0_rst_s                                       : std_logic := '0';

--signal scaling_cst_s												: std_logic_vector(10 downto 0) := "000" & x"00"; 
--signal Bup_Bdown_Cnt_s                                : std_logic_vector(31 downto 0) := x"00000000"; 
--signal Scale_BupBdownCnt_s                            : std_logic_vector(42 downto 0) := "000" & x"0000000000";
constant scaling_cst_s                                : unsigned(42 downto 0) := "000" & x"00000003E8"; -- x1000 => multiply by "0x3E8"
signal Bup_Bdown_Cnt_s                                : std_logic_vector(31 downto 0) := x"00000000";
signal Scale_BupBdownCnt_s                            : unsigned(42 downto 0) := "000" & x"0000000000";

 
begin

process(Clk_i)
begin
    if falling_edge(Clk_i) then
	    if Gbl_reset_i = '1' then
		    C0_rst_rise1_s <= '0';
			 C0_rst_rise2_s <= '0';
			 C0_rst_rise_s  <= '0';
			 pdly_cst_s     <= x"00000";
			 ce_cnt_s       <= '0';
			 tc_pdly_cnt_s  <= '0';
			 Bup1_s         <= '0';
			 Bup2_s         <= '0';
			 Bup_rise_s     <= '0';
			 Bdown1_s       <= '0';
			 Bdown2_s       <= '0';
			 Bdown_rise_s   <= '0';
			 C0_rst_s       <= '0';
       else
          --One clock cycle long C0 reset pulse : high during a falling edge clock		 
	       C0_rst_rise1_s <= C0_Reset_i;
		    C0_rst_rise2_s <= C0_rst_rise1_s;
		    C0_rst_rise_s  <= C0_rst_rise1_s and ( not C0_rst_rise2_s );			 
		    ------Start delay counter for C0 pulse-------
			 --pdly_cst_s <= pdly_cst_i;
			 pdly_cst_s <= x"00000";
          if tcpdlycnt_s = '1' then   
             ce_cnt_s <= '0';
          elsif C0_rst_fall_s = '1' then
             ce_cnt_s <= C0_rst_fall_s;
          end if;
	       -- Does the C0 pulse delay is equal to zero ?			 
			 if pdly_cst_s = 0 then
			    tc_pdly_cnt_s <= C0_rst_fall_s;
			 else
			    tc_pdly_cnt_s <= tcpdlycnt_s;
          end if;				 
			 --C0_rst_s <= tc_pdly_cnt_s;	
          C0_rst_s <= C0_rst_rise_s;			 
		    ----------------------------------------------
--	       if C0_rst_fall_s = '1' then
--		       Bup1_s       <= '0';
--			    Bup2_s       <= '0';
--			    Bup_rise_s   <= '0';
--			    Bdown1_s     <= '0';
--			    Bdown2_s     <= '0';
--			    Bdown_rise_s <= '0';		 
--		    else
		       --Rising & falling edge detection of Bup pulse
		       Bup1_s <= not Bup_i;
			    Bup2_s <= Bup1_s;
			    Bup_rise_s <= Bup1_s and ( not Bup2_s );
				 Bup_fall_s <= ( not Bup1_s ) and Bup2_s;
			    --Rising & falling edge detection of Bdown pulse
		       Bdown1_s <= not Bdown_i;
			    Bdown2_s <= Bdown1_s;
			    Bdown_rise_s <= Bdown1_s and ( not Bdown2_s );
				 Bdown_fall_s <= ( not Bdown1_s ) and Bdown2_s;				 
--		    end if;		 
	    end if;
	 end if;
end process;			 

process(Clk_i)
begin
    if rising_edge(Clk_i) then
	    if Gbl_reset_i = '1' then
		    C0_rst_fall1_s  <= '0';
		    C0_rst_fall2_s  <= '0';
		    C0_rst_fall_s   <= '0';
          pdly_cnt_s      <= (others => '0');
          Bup_Bdown_Cnt_s <= (others => '0');
          Scale_BupBdownCnt_s	<= (others => '0');				
		 else
		    --C0 reset pulse with one clock cycle long  : high during a rising edge clock
	       C0_rst_fall1_s <= C0_Reset_i;
		    C0_rst_fall2_s <= C0_rst_fall1_s;
		    C0_rst_fall_s  <= C0_rst_fall1_s and ( not C0_rst_fall2_s );
		    -------Pulse delay counter (PDLY cnt)---------
		    if tc_pdly_cnt_s = '1' then 
             pdly_cnt_s <= (others => '0');
          elsif ce_cnt_s = '1' then
             pdly_cnt_s <= pdly_cnt_s + 1;
          end if;
          -----------------------------------------------		 
		    --if tc_pdly_cnt_s = '1' then
			 if C0_rst_rise_s = '1' then
		       Bup_Bdown_Cnt_s <= x"00000000";
				 Scale_BupBdownCnt_s <= (others => '0');
		    else
		       if Bup_rise_s = '1' then
				    --Count UP
                Bup_Bdown_Cnt_s <= Bup_Bdown_Cnt_s + 1;
					 Scale_BupBdownCnt_s <= Scale_BupBdownCnt_s + scaling_cst_s;
             elsif Bdown_rise_s = '1' then
				    --Count DOWN
                Bup_Bdown_Cnt_s <= Bup_Bdown_Cnt_s - 1;
					 Scale_BupBdownCnt_s <= Scale_BupBdownCnt_s - scaling_cst_s;
             end if;
          end if;
       end if;			 
    end if;
end process;

--Delay settings if clock input frequency = 10 MHz :
--pdly_cst_s x 100ns = pulse delay.
tcpdlycnt_s <= '1' when pdly_cnt_s = pdly_cst_s else '0'; 

--White Rabbit Register refresh signal.
--Bupdown_drdy_o <= Bup_fall_s or Bdown_fall_s or C0_rst_s;
--
----Adjusting the scale from 10nT (WR frame LSB) to 10uT resolution for the white rabbit frame (x1000)
--scaling_cst_s       <= "011" & x"E8"; -- x1000 => multiply by "0x3E8"
--Scale_BupBdownCnt_s <= scaling_cst_s * Bup_Bdown_Cnt_s;
--B_Cnt_o             <= Bup_Bdown_Cnt_s; 	   
--Scale_B_Cnt_o       <= Scale_BupBdownCnt_s(42) & Scale_BupBdownCnt_s(30 downto 0); 

Bupdown_drdy_o <= Bup_fall_s or Bdown_fall_s or C0_rst_s;

--Adjusting the scale from 10nT (WR frame LSB) to 10uT resolution for the white rabbit frame (x1000)
-- scaling_cst_s       <= "011" & x"E8"; -- x1000 => multiply by "0x3E8"
-- Scale_BupBdownCnt_s <= scaling_cst_s * Bup_Bdown_Cnt_s;
B_Cnt_o             <= Bup_Bdown_Cnt_s; 	    
Scale_B_Cnt_o       <= std_logic_vector(Scale_BupBdownCnt_s(42) & Scale_BupBdownCnt_s(30 downto 0)); 
	
end Behavioral;

