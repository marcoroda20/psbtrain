----------------------------------------------------------------------------------
-- Company: CERN TE/MSC-MM
-- Engineer: David Giloteaux
-- 
-- Create Date:    11:59:06 07/03/2015 
-- Design Name: 
-- Module Name:    Bup_Bdown - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: Bup / Bdown 32-Bit counter
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
-- library UNISIM;
-- use UNISIM.VComponents.all;

entity Bup_Bdown is

    Port ( 
		Gbl_Reset      : in  STD_LOGIC; --Active High
	   C0_Reset       : in  STD_LOGIC; --Active High
		Bup            : in  STD_LOGIC; --Active High
		Bdown          : in  STD_LOGIC; -- Active High

	   --C0 pulse programmable delay in clock period number
	   pdly_cst       : in  STD_LOGIC_VECTOR (19 downto 0);
	   Bupdown_drdy   : out STD_LOGIC; -- Active High
		B_Cnt          : out STD_LOGIC_VECTOR (31 downto 0);
		Clk            : in  STD_LOGIC ); --10MHz
			  
end Bup_Bdown;

architecture Behavioral of Bup_Bdown is

-- Signals 
signal ce_cnt_s, tcpdlycnt_s, tc_pdly_cnt_s           : std_logic := '0';  
signal pdly_cnt_s, pdly_cst_s                         : std_logic_vector(19 downto 0) := x"00000"; 

signal Bup1_s, Bup2_s, Bup_rise_s, Bup_fall_s         : std_logic := '0'; 
signal Bdown1_s, Bdown2_s, Bdown_rise_s, Bdown_fall_s : std_logic := '0';

signal C0_rst_rise1_s, C0_rst_rise2_s, C0_rst_rise_s  : std_logic := '0';
signal C0_rst_fall1_s, C0_rst_fall2_s, C0_rst_fall_s  : std_logic := '0';
signal C0_rst_s                                       : std_logic := '0';
 
signal Bup_Bdown_Cnt_s                                : std_logic_vector(31 downto 0) := x"00000000"; 

begin
	
	process(Clk) -- Procces #1
	begin
		 if falling_edge(Clk) then -- Initialization if 'Gbl_reset' in falling edge
			 if Gbl_reset = '1' then
				 C0_rst_rise1_s <= '0';
				 C0_rst_rise2_s <= '0';
				 C0_rst_rise_s  <= '0';
				 pdly_cst_s     <= x"00000";
				 ce_cnt_s       <= '0';
				 tc_pdly_cnt_s  <= '0';
				 Bup1_s         <= '0';
				 Bup2_s         <= '0';
				 Bup_rise_s     <= '0';
				 Bdown1_s       <= '0';
				 Bdown2_s       <= '0';
				 Bdown_rise_s   <= '0';
				 C0_rst_s       <= '0';			 
			 else
				 --One clock cycle long C0 reset pulse : high during a falling edge clock		 
				 C0_rst_rise1_s <= C0_Reset;
				 C0_rst_rise2_s <= C0_rst_rise1_s;
				 C0_rst_rise_s  <= C0_rst_rise1_s and ( not C0_rst_rise2_s );			 
				 ------Start delay counter for C0 pulse-------
				 pdly_cst_s <= pdly_cst;
				 if tcpdlycnt_s = '1' then   
					 ce_cnt_s <= '0';
				 elsif C0_rst_fall_s = '1' then
					 ce_cnt_s <= C0_rst_fall_s;
				 end if;
				 -- The C0 pulse delay is equal to zero ??			 
				 if pdly_cst = 0 then
					 tc_pdly_cnt_s <= C0_rst_fall_s;
				 else
					 tc_pdly_cnt_s <= tcpdlycnt_s;
				 end if;				 
				 C0_rst_s <= tc_pdly_cnt_s;			 
				 ----------------------------------------------
				 if C0_rst_fall_s = '1' then
					 Bup1_s       <= '0';
					 Bup2_s       <= '0';
					 Bup_rise_s   <= '0';
					 Bdown1_s     <= '0';
					 Bdown2_s     <= '0';
					 Bdown_rise_s <= '0';		 
				 else
					 --Rising & falling edge detection of Bup pulse
					 Bup1_s <= Bup;
					 Bup2_s <= Bup1_s;
					 Bup_rise_s <= Bup1_s and ( not Bup2_s );
					 Bup_fall_s <= ( not Bup1_s ) and Bup2_s;
					 --Rising & falling edge detection of Bdown pulse
					 Bdown1_s <= Bdown;
					 Bdown2_s <= Bdown1_s;
					 Bdown_rise_s <= Bdown1_s and ( not Bdown2_s );
					 Bdown_fall_s <= ( not Bdown1_s ) and Bdown2_s;				 
				 end if;		 
			 end if;
		 end if;
	end process;			 

	process(Clk) -- Procces #2
	begin
		 if rising_edge(Clk) then -- Initialization if 'Gbl_reset' in rising edge
			 if Gbl_reset = '1' then
				 C0_rst_fall1_s  <= '0';
				 C0_rst_fall2_s  <= '0';
				 C0_rst_fall_s   <= '0';
				 pdly_cnt_s      <= (others => '0');
				 Bup_Bdown_Cnt_s <= (others => '0');			 
			 else
				 --One clock cycle long C0 reset pulse : high during a rising edge clock
				 C0_rst_fall1_s <= C0_Reset;
				 C0_rst_fall2_s <= C0_rst_fall1_s;
				 C0_rst_fall_s  <= C0_rst_fall1_s and ( not C0_rst_fall2_s );
				 -------Pulse delay counter (PDLY cnt)---------
				 if tc_pdly_cnt_s = '1' then 
					 pdly_cnt_s <= (others => '0');
				 elsif ce_cnt_s = '1' then
					 pdly_cnt_s <= pdly_cnt_s + 1;
				 end if;
				 -----------------------------------------------		 
				 if tc_pdly_cnt_s = '1' then
					 Bup_Bdown_Cnt_s <= x"00000000";
				 else
					 if Bup_rise_s = '1' then
						 --Count UP
						 Bup_Bdown_Cnt_s <= Bup_Bdown_Cnt_s + 1;
					 elsif Bdown_rise_s = '1' then
						 --Count down
						 Bup_Bdown_Cnt_s <= Bup_Bdown_Cnt_s - 1;
					 end if;
				 end if;
			 end if;			 
		 end if;
	end process;

--Delay settings if clock input frequency = 10 MHz:
--pdly_cst_s x 100ns = pulse delay.
tcpdlycnt_s <= '1' when pdly_cnt_s = pdly_cst_s else '0'; 

--White Rabbit Register refresh signal.
Bupdown_drdy <= Bup_fall_s or Bdown_fall_s or C0_rst_s;

--Don't forget to adjust the scale from 10uT to 10nT resolution for the white rabbit frame (x1000)
B_Cnt <= Bup_Bdown_Cnt_s; 	   

end Behavioral;

