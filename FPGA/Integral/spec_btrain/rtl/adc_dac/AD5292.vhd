----------------------------------------------------------------------------------
-- Company:        CERN TE/MSC/MM
-- Engineer:       Daniel Oberson
-- 
-- Create Date:    13:46:44 02/23/2015 
-- Design Name: 
-- Module Name:    AD5292 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.utils_pkg.all;

entity AD5292 is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		dpot_reset_i : in std_logic;
		dpot_wr_i    : in std_logic;
--		dpot_en_i    : in std_logic;

		data_1_i   : in  std_logic_vector (9 downto 0);
		data_2_i   : in  std_logic_vector (9 downto 0);
		
		sclk_o       : out  std_logic;
		sdout_o      : out  std_logic;
		sdin_i       : in  std_logic;
		rdy1_i       : in  std_logic; -- READY active low
		rdy2_i       : in  std_logic;
		dpot_reset_o : out  std_logic;
		n_sync_o     : out  std_logic;
		done_o       : out  std_logic
	);
end AD5292;

architecture Behavioral of AD5292 is
	
	--Constant
	constant NBRE_SPI_CLOCK     : integer := 32;
	constant MAX_WAIT_SYNC      : integer := 2;
	constant MAX_WAIT_RESET     : integer := 20;
	constant MAX_WAIT_COUNT_SPI : integer := 4;

	constant CMD_DPOT_WR : std_logic_vector(3 downto 0) := "0001";
	constant CMD_DPOT_EN : std_logic_vector(3 downto 0) := "0110";
	constant DATA_EN     : std_logic_vector(9 downto 0) := "0000000010";

	--Type
	type dpot_states is (
		IDLE,
		RESET,
		ENABLE_DPOT,
		LOAD_BUS,
		SYNC_START,
		SCLK_GEN,
		SYNC_END,
		WAIT_RDY_START,
		WAIT_RDY_END
	);
	
	type spi_state is (
		IDLE,
		LOAD_DATA,
		SPI_SCLK_ONE_LOAD_SDO,
		SPI_SCLK_ONE_WAIT,
		COUNT_CLK,
		SPI_SCLK_ZERO,
		END_SPI
	);
	
	--Signal
	signal dpot_states_s : dpot_states;
	signal spi_state_s   : spi_state;

	signal bus_spi_s : std_logic_vector(31 downto 0);
	signal spi_data_s	: std_logic_vector(31 downto 0);
	
	signal dpot_reset_s : std_logic;
	signal dpot_wr_s    : std_logic;
	
	signal sck_gen_s        : std_logic;
	signal sck_gen_rising_s : std_logic;
	
	signal rdy_s : std_logic;
	
	signal sclk_s  : std_logic;
	signal sdout_s : std_logic;

	signal flag_en_s     : std_logic;
	signal start_wr_spi_s: std_logic;
	signal end_sck_gen_s : std_logic;
	
	signal count_wait_s    : integer;
	signal count_end_spi_s : integer;
	
	signal count_one_s  : integer;
	signal count_zero_s : integer;
	signal count_clk_s  : integer;
	
begin
	
	--To construct command for AD5292
	process(clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				dpot_states_s<= IDLE;
				count_wait_s <= 0;
				sck_gen_s    <= '0';
				rdy_s        <= '0';
				dpot_reset_o <= '1';
				n_sync_o     <= '1';
				flag_en_s    <= '0';
				bus_spi_s    <= (others=>'0');
			else
				rdy_s <= rdy1_i and rdy2_i;
				case dpot_states_s is
					when IDLE =>
						if (dpot_reset_s='1') then
							dpot_states_s <= RESET;
						elsif (dpot_wr_s='1') then
							dpot_states_s <= LOAD_BUS;
						end if;
						dpot_reset_o <= '1';
						n_sync_o     <= '1';
						sck_gen_s    <= '0';
						done_o       <= '1';
						count_wait_s <= 0;
						
					when RESET =>
						if (count_wait_s>=(MAX_WAIT_RESET-1)) then
							dpot_states_s <= WAIT_RDY_START;
						end if;
						dpot_reset_o <= '0';
						flag_en_s    <= '1';
						done_o       <= '0';
						count_wait_s <= count_wait_s + 1;
						
					when ENABLE_DPOT =>
						dpot_states_s <= SYNC_START;
						bus_spi_s     <= "00" & CMD_DPOT_EN & DATA_EN & "00" & CMD_DPOT_EN & DATA_EN;
						flag_en_s     <= '0';
						done_o        <= '0';
						count_wait_s  <= 0;
						
					when LOAD_BUS =>
						dpot_states_s <= SYNC_START;
						bus_spi_s     <= "00" & CMD_DPOT_WR & data_1_i & "00" & CMD_DPOT_WR & data_2_i;
						done_o        <= '0';
						count_wait_s  <= 0;
						
					when SYNC_START =>
						if (count_wait_s>=MAX_WAIT_SYNC) then
							dpot_states_s <= SCLK_GEN;
						end if;
						n_sync_o     <= '0';
						count_wait_s <= count_wait_s + 1;

					when SCLK_GEN =>
						if (end_sck_gen_s='1') then
							dpot_states_s <= SYNC_END;
						end if;
						sck_gen_s <= '1';
						n_sync_o     <= '0';
						count_wait_s <= 0;
						
					when SYNC_END =>
						if (count_wait_s>=MAX_WAIT_SYNC) then
							dpot_states_s <= WAIT_RDY_START;
						end if;
						sck_gen_s <= '0';
						n_sync_o     <= '0';
						count_wait_s <= count_wait_s + 1;
						
					when WAIT_RDY_START =>
						if (rdy_s='0') then
							dpot_states_s <= WAIT_RDY_END;
						end if;
						dpot_reset_o <= '1';
						n_sync_o     <= '1';
					
					when WAIT_RDY_END =>
						if (rdy_s='1') then
							if (flag_en_s='1') then
								dpot_states_s <= ENABLE_DPOT;
							else
								dpot_states_s <= IDLE;
							end if;
						end if;
						n_sync_o     <= '1';

				end case;
			end if;
		end if;
	end process;

	
	--To construct send values to AD5292
	process(clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				spi_state_s   <= IDLE;
				spi_data_s    <= (others=>'0');
				end_sck_gen_s <= '0';
				count_one_s <= 0;
				count_zero_s <= 0;
				count_clk_s   <= 0;
				sclk_o        <= '0';
				sdout_o       <= '0';
			else
			
				case spi_state_s is
					when IDLE =>
						if (sck_gen_rising_s='1') then
							spi_state_s <= LOAD_DATA;
						end if;
						end_sck_gen_s <= '0';
						count_one_s <= 0;
						count_zero_s <= 0;
						count_clk_s <= 0;
						sclk_o  <= '0';
						
					when LOAD_DATA =>
						spi_state_s <= SPI_SCLK_ONE_LOAD_SDO;
						spi_data_s <= bus_spi_s;
						
					when SPI_SCLK_ONE_LOAD_SDO =>
						spi_state_s <= SPI_SCLK_ONE_WAIT;
						count_one_s <= count_one_s + 1;
						sclk_o  <= '1';
--						sdout_o <= spi_data_s(count_clk_s);
						sdout_o <= spi_data_s(spi_data_s'length-1);
						for index in 30 downto 0 loop
							spi_data_s(index+1) <= spi_data_s(index);
						end loop;

					when SPI_SCLK_ONE_WAIT =>
						if (count_one_s>=(MAX_WAIT_COUNT_SPI-1)) then
							spi_state_s <= COUNT_CLK;
						end if;
						count_one_s <= count_one_s + 1;

					when COUNT_CLK =>
						spi_state_s <= SPI_SCLK_ZERO;
						count_one_s <= 0;
						count_zero_s<= 0;
						count_clk_s <= count_clk_s + 1;

					when SPI_SCLK_ZERO =>
						if (count_zero_s>=MAX_WAIT_COUNT_SPI) then
							if (count_clk_s>=NBRE_SPI_CLOCK) then
								spi_state_s <= END_SPI;
							else
								spi_state_s <= SPI_SCLK_ONE_LOAD_SDO;
							end if;
						end if;
						count_zero_s <= count_zero_s + 1;
						sclk_o  <= '0';
						
					when END_SPI =>
						spi_state_s <= IDLE;
						end_sck_gen_s <= '1';

				end case;
			end if;
		end if;
	end process;

	--Rising edge detection for command reset 
	cmp_dpot_reset : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> dpot_reset_i,
		s_o	=> dpot_reset_s
	);

	--Rising edge detection for command write 
	cmp_dpot_write : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> dpot_wr_i,
		s_o	=> dpot_wr_s
	);

	--Rising edge detection for command enable 
	cmp_dpot_en : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> sck_gen_s,
		s_o	=> sck_gen_rising_s
	);

end Behavioral;

