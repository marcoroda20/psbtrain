----------------------------------------------------------------------------------
-- Company: CERN TE/MSC-MM
-- Engineer: David Giloteaux
-- 
-- Create Date:    18:19:37 07/26/2012 
-- Design Name: 
-- Module Name:    AD5791 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: AD5791 20-Bit DAC Interface
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;

entity AD5791 is

    Port ( CLK : in  STD_LOGIC; -- CLOCK = 100MHz
	        SDI : in  STD_LOGIC;
	        --DAC_CE : in  STD_LOGIC;
			  RESET : in std_logic;
			  SDO, SCK, N_LDAC, N_SYNC : out  STD_LOGIC;
           N_RESET, N_CLR, END_WR_DAC, END_RD_DAC : out  STD_LOGIC;
			  DAC_WR : in  STD_LOGIC_VECTOR (23 downto 0);
			  DAC_RD : out  STD_LOGIC_VECTOR (23 downto 0) );
			  
end AD5791;

architecture Behavioral of AD5791 is

-- This subtype will constrain counter width. 
-- Tell to the synthesizer how many bits wide the generated counter should be.
subtype divtype is natural range 0 to 6; -- 0 to 7-1 = 0 to 7-1=6
signal counter: divtype;

signal en_tff1 : std_logic :='0';
signal fallsync_en_tff1: std_logic :='1';
signal en_tff2 : std_logic := '0';
signal div1 : std_logic :='1';
signal div2 : std_logic :='1';
signal tick_70ns, tick1, tick2, tick3, tick_rise_70ns, tick_fall_70ns : std_logic :='0';

signal D0_1, D0_2, D1_1, D1_2, D2_1, D2_2 : std_logic := '0';
signal D3_1, D3_2, D4_1, D4_2, D5_1, D5_2 : std_logic := '0';
signal D6_1, D6_2, D7_1, D7_2, D8_1, D8_2 : std_logic := '0';
signal D9_1, D9_2, D10_1, D10_2, D11_1, D11_2 : std_logic := '0';
signal D12_1, D12_2, D13_1, D13_2, D14_1, D14_2 : std_logic := '0';
signal D15_1, D15_2, D16_1, D16_2, D17_1, D17_2 : std_logic := '0';
signal D18_1, D18_2, D19_1, D19_2, D20_1, D20_2 : std_logic := '0';
signal D21_1, D21_2, D22_1, D22_2, D23_1, D23_2 : std_logic := '0';

signal rd_or_wr, sel_cycle, end_cycle, sel_nop_data : std_logic := '0';
signal data_modif, ena_cycle, reg_modif, rm1 : std_logic := '0';
signal wr_load_p2s, wr_reset_p2s, rd_load_p2s, rd_reset_p2s : std_logic := '0';
signal start_wr, stopwr, stop_wr, busy_wr, start_wr_cycle, end_wr_cycle, spi_wr : std_logic := '0';
signal start_rd, stoprd, stop_rd, busy_rd, start_rd_cycle, end_rd_cycle, spi_rd : std_logic := '0';
signal wr_sync, wr_sync1, wr_sync2, rd_sync, ldac_or_clr, ldac, clr : std_logic := '0';
signal wr_ena, rd_ena, spi_busy, busy_init, start_nop1 : std_logic := '0';
signal nop_cond, start_nop, ld_nop, sel_nop, start_rdbck, shift_snd : std_logic := '0';

signal wr_gate24_cnt, rd_gate24_cnt  : std_logic_vector(4 downto 0) := "00000";
signal wr_p2s_data, wr_idata, rd_p2s_data, rd_idata, s2p : std_logic_vector(23 downto 0) := x"000000";
signal wr_mode, rd_mode : std_logic_vector(1 downto 0) := "00";

signal reg_addr, wr_reg_addr : std_logic_vector(3 downto 0) := x"0";
signal wr_sdout, rd_sdout, shift_spi_rd, nop_sync, nop_sync_dec, shift_nop_sync, drdy : std_logic := '0';

--signal drdy1, drdy2, tick : std_logic := '0';

signal rst1, rst2, tick_rst_fall, tick_rst_rise, init_rst_rise : std_logic := '0';
signal ce_rdly_cnt, tc_rdly_cnt, tcrdlycnt : std_logic := '0';
signal rdly_cnt, rdly : std_logic_vector(23 downto 0) := x"000000";

signal ewc1, ewc2, start_dac, ena_dac, latch_data : std_logic := '0';

--For output latch
signal REG_CONT_latch, DATA_WR : STD_LOGIC_VECTOR (23 downto 0) := x"000000";

--signal DATA_CNT : STD_LOGIC_VECTOR (23 downto 0) := x"000000";
--signal cnt : std_logic_vector(2 downto 0) := "000";

begin

------------------------ tick = 70 ns (100MHz divide by 7 = 14.29MHz)------------------------------
process(clk)
begin    
	 if rising_edge(clk) then
	    if RESET = '1' then 
		    counter <= 0;
		 else
   		 if ( counter = 6 ) then -- Counter count to 7-1 = 7-1 = 6 on the rising edge of CLK
			      counter <= 0;
			 else
               counter <= counter + 1;
          end if;
       end if;
	 end if;
end process;

--Enable the Toggle Flip-Flop div1
en_tff1 <= '1' when counter = 0 else '0';	          
				 
process (clk)
begin
    if falling_edge(clk) then 
		 if RESET = '1' then 
			 fallsync_en_tff1 <= '1';
		 else
		    fallsync_en_tff1 <= en_tff1; --synchronize en_tff1 with the falling edge of CLK
		 end if;
	 end if;
end process;

--Enable the Toggle Flip-Flop div2
en_tff2 <= '1' when counter = 4 else '0'; -- counter = (((n-1)/2)+1)= ( ( (7-1)/2 ) + 1 ) = 4
					 
process(clk)
begin                        
	 if rising_edge(clk) then
       if RESET = '1' then 
		     div1 <= '1';
	    elsif fallsync_en_tff1 = '1' then
	        div1 <= not div1;
	    end if;		 
	 end if;
end process;

process(clk)
begin
    if falling_edge(clk) then
	    if RESET = '1' then
          div2 <= '1';
		 elsif (en_tff2 = '1') then
		    div2 <= not div2;
       end if;
     end if;
end process;

-- Because of the constant 90 phase offset, only one transition 
-- occurs at a time on	the XOR input, effectively eliminating any
-- glitches on the output waveform.

tick_70ns <= div1 xor div2;
----------------------------------------------------------------------------------------------
process(clk)
begin
    if falling_edge(clk) then
	    tick1 <= tick_70ns;
		 tick_rise_70ns <= tick_70ns and ( not tick1 );	
       tick3 <= tick2;		 
	 end if;
end process;

process(clk)
begin
    if rising_edge(clk) then
	    tick2 <= tick_70ns;
		 tick_fall_70ns <= ( not tick_70ns ) and ( tick1 );
    end if;
end process;

------------------------------------------------------------------------------------------------------------------------
-- Maximum SCLK frequency is 35MHZ for write mode and 16MHz for readback mode.
-- We choose SCLK = 14.29MHz for write and readback mode.

-- Bus state changing detector to generate the update DAC signal.

-- DAC transfert function in differential with D in straight binary code :
-- [ (Vrefp - Vrefn) x D x 2  / ( 2^20 - 1 ) ] + ( 2 x Vrefn )
-- with Vrefp = +5V - 1LSB and Vrefn = -5V with 1 LSB = 10 / 2^20

process(clk)
begin
    if rising_edge(clk) then
	    rst1 <= RESET;
		 rst2 <= rst1;
		 tick_rst_fall <= ( not rst1 ) and rst2;
		 tick_rst_rise <= rst1 and ( not rst2 );
	 end if;
end process;

-------------- Delay between reset ending and the DAC enable command = x"200000" -------------------

--Start Reset 
process (clk)
begin
   if rising_edge(clk) then 
		if ( RESET='1' ) then
			  rdly_cnt   <= (others=>'0');
			  DATA_WR    <= (others=>'0');
			  busy_init  <= '1';
			  ena_dac    <= '0';
		else
			--Counter for 500us delay after reset
			if (rdly_cnt < 50000) then
				 rdly_cnt  <= rdly_cnt + 1;
				 DATA_WR   <= (others=>'0');
				 busy_init <= '1';
			else
				 busy_init    <= '0';
				 if ( latch_data= '0' ) then
					  DATA_WR <= c_CFG_DAC5791;--gain of 2 configuration => RBUF bit = 0
				 else
					  DATA_WR <= DAC_WR;
				 end if;
			end if;
			--Signal after initialization
			if start_dac = '1' then
				ena_dac <= start_dac;
			end if;
			---------With a clock enable signal-----------------
			--drdy1 <= DAC_CE;
			--drdy2 <= drdy1;
			--tick  <= drdy1 and ( not drdy2 );
			--ce_dac_reg <= tick and ( not spi_busy );
			-----------------------------------------
		end if;
   end if;
end process;

---------------------------------AD5791 interface test------------------------------------

process(clk)
begin
	if falling_edge(clk) then	   
		if (RESET='1') then
			 ewc1       <= '0';
			 ewc2       <= '0';
			 latch_data <= '0';
			 start_dac  <= '0';
		else
			 latch_data <= ena_dac;		 
			 ---------With a clock enable signal-----------------
			 --latch_data <= ena_dac and ce_dac_reg;
			 ---------For simulation-----------------
			 --if latch_data = '1' then
			 --   cnt <= cnt + 1;
			 --end if;
			 ---------------------------------------
			 --MODIFIED 30/07/2015
			 --ewc1      <= end_wr_cycle;
			 --ewc2      <= ewc1;
			 --start_dac <= ( not ewc1 ) and ewc2;
          start_dac <= end_wr_cycle; 
			
		end if;
	end if;
end process;

-------------------------------For simulation-------------------------------
--process (clk)
--begin
    --if rising_edge(clk) then
	    --if    cnt = 0 then
          --DATA_CNT <= x"900000"; --0V
       --elsif cnt = 1 then
		    --DATA_CNT <= x"120000"; --+2.5V
		 --elsif cnt = 2 then
		    --DATA_CNT <= x"900000"; --+5V
		 --elsif cnt = 3 then
		    --DATA_CNT <= x"170000"; -- +8.75V
		 --elsif cnt = 4 then
		    --DATA_CNT <= x"900000"; -- +10V - 1LSB
		 --elsif cnt = 5 then
		    --DATA_CNT <= x"190000"; -- -8.75V
		 --elsif cnt = 6 then
		    --DATA_CNT <= x"A00000"; -- -5V
		 --elsif cnt = 7 then
		    --DATA_CNT <= x"1E0000"; -- -2.5V
	    --end if;	
	 --end if;	 
--end process;
------------------------------------------------------------------------------
process (clk)
begin
		if falling_edge(clk) then
		   if tick_rst_rise = '1' then
			   D0_1 <= '0';
				D0_2 <= '0';
				D1_1 <= '0';
				D1_2 <= '0';
				D2_1 <= '0';
				D2_2 <= '0';
				D3_1 <= '0';
				D3_2 <= '0';
				D4_1 <= '0';
				D4_2 <= '0';
				D5_1 <= '0';
				D5_2 <= '0';
				D6_1 <= '0';
				D6_2 <= '0';
				D7_1 <= '0';
				D7_2 <= '0';
				D8_1 <= '0';
				D8_2 <= '0';
				D9_1 <= '0';
				D9_2 <= '0';
				D10_1 <= '0';
				D10_2 <= '0';
				D11_1 <= '0';
				D11_2 <= '0';
				D12_1 <= '0';
				D12_2 <= '0';
				D13_1 <= '0';
				D13_2 <= '0';
				D14_1 <= '0';
				D14_2 <= '0';
				D15_1 <= '0';
				D15_2 <= '0';
				D16_1 <= '0';
				D16_2 <= '0';
				D17_1 <= '0';
				D17_2 <= '0';
				D18_1 <= '0';
				D18_2 <= '0';
				D19_1 <= '0';
				D19_2 <= '0';
				D20_1 <= '0';
				D20_2 <= '0';
				D21_1 <= '0';
				D21_2 <= '0';
				D22_1 <= '0';
				D22_2 <= '0';
				D23_1 <= '0';
				D23_2 <= '0';
				ena_cycle <= '0';
				reg_addr  <= x"0";
				reg_modif <= '0';
				rd_or_wr  <= '0';			
		   elsif tick_fall_70ns = '1' then
				if  spi_busy = '0'  then
					 D0_1 <= DATA_WR(0);
				    D0_2 <= D0_1;
				    D1_1 <= DATA_WR(1);
				    D1_2 <= D1_1;
				    D2_1 <= DATA_WR(2);
				    D2_2 <= D2_1;
				    D3_1 <= DATA_WR(3);
				    D3_2 <= D3_1;
				    D4_1 <= DATA_WR(4);
				    D4_2 <= D4_1;
				    D5_1 <= DATA_WR(5);
				    D5_2 <= D5_1;
				    D6_1 <= DATA_WR(6);
			       D6_2 <= D6_1;
				    D7_1 <= DATA_WR(7);
				    D7_2 <= D7_1;
				    D8_1 <= DATA_WR(8);
				    D8_2 <= D8_1;
				    D9_1 <= DATA_WR(9);
				    D9_2 <= D9_1;
				    D10_1 <= DATA_WR(10);
				    D10_2 <= D10_1;
				    D11_1 <= DATA_WR(11);
				    D11_2 <= D11_1;
				    D12_1 <= DATA_WR(12);
				    D12_2 <= D12_1;
				    D13_1 <= DATA_WR(13);
				    D13_2 <= D13_1;
				    D14_1 <= DATA_WR(14);
				    D14_2 <= D14_1;
				    D15_1 <= DATA_WR(15);
				    D15_2 <= D15_1;
				    D16_1 <= DATA_WR(16);
				    D16_2 <= D16_1;
				    D17_1 <= DATA_WR(17);
				    D17_2 <= D17_1;
				    D18_1 <= DATA_WR(18);
				    D18_2 <= D18_1;
				    D19_1 <= DATA_WR(19);
				    D19_2 <= D19_1;
				    D20_1 <= DATA_WR(20);
				    D20_2 <= D20_1;
				    D21_1 <= DATA_WR(21);
				    D21_2 <= D21_1;
				    D22_1 <= DATA_WR(22);
				    D22_2 <= D22_1;
				    D23_1 <= DATA_WR(23);
				    D23_2 <= D23_1;
                data_modif <= ( D0_1 xor D0_2 )   or ( D1_1 xor D1_2 )   or ( D2_1 xor D2_2 )   or ( D3_1 xor D3_2 )   or				
				                  ( D4_1 xor D4_2 )   or ( D5_1 xor D5_2 )   or ( D6_1 xor D6_2 )   or ( D7_1 xor D7_2 )   or	
				                  ( D8_1 xor D8_2 )   or ( D9_1 xor D9_2 )   or ( D10_1 xor D10_2 ) or ( D11_1 xor D11_2 ) or
				                  ( D12_1 xor D12_2 ) or ( D13_1 xor D13_2 ) or ( D14_1 xor D14_2 ) or ( D15_1 xor D15_2 ) or	
				                  ( D16_1 xor D16_2 ) or ( D17_1 xor D17_2 ) or ( D18_1 xor D18_2 ) or ( D19_1 xor D19_2 ) or
				                  ( D20_1 xor D20_2 ) or ( D21_1 xor D21_2 ) or ( D22_1 xor D22_2 ) or ( D23_1 xor D23_2 ); 
				    reg_addr <= D23_1 & D22_1 & D21_1 & D20_1;
				    -- Enable a write or a readback cycle if the address is  valid
				    case reg_addr is
		               when x"0"   => ena_cycle <= '1'; -- No operation
					      when x"8"   => ena_cycle <= '1'; -- No operation
					      when x"1"   => ena_cycle <= '1'; -- Write to the DAC register
					      when x"2"   => ena_cycle <= '1'; -- Write to the control register
					      when x"3"   => ena_cycle <= '1'; -- Write to the clearcode register
					      when x"4"   => ena_cycle <= '1'; -- Write to the software control register (write only)
					      when x"9"   => ena_cycle <= '1'; -- Read from the DAC register
					      when x"A"   => ena_cycle <= '1'; -- Read from the control register
					      when x"B"   => ena_cycle <= '1'; -- Read from the clearcode register					  
				         when others => ena_cycle <= '0';
			        end case;
                 reg_modif <= ena_cycle and data_modif;
                 --Readback mode or write mode : Bit23 test ?					  
                 rd_or_wr <= D23_1;  -- "1" = Readback / "0" = Write					  
				end if;
			end if;
		end if;
end process;
			  
process (clk)
begin
	 if rising_edge(clk) then
	      if init_rst_rise = '1' then
			   sel_cycle <= '0';
		      rm1 <= '0';			
	      elsif tick_rise_70ns = '1' then
		      if ( end_cycle ) = '1' then
			        sel_cycle <= '0';
			   elsif ( reg_modif ) = '1' then
                 sel_cycle <= reg_modif and rd_or_wr; --sel_cycle = "1": readback cycle / sel_cycle = "0": write cycle
            end if;			 
		      rm1 <= reg_modif;
			end if;
	 end if;
end process;

end_cycle <= end_wr_cycle or end_rd_cycle;
spi_busy <= busy_wr or busy_rd or busy_init; --Inhibit the data register updating

-- Write or Readback function selection
wr_ena <= rm1 and ( not sel_cycle );
rd_ena <= rm1 and sel_cycle;

process (clk)
begin
	if falling_edge(clk) then
	   if tick_rst_rise = '1' then
		   start_wr_cycle <= '0';
		   start_rd_cycle <= '0';		
	   elsif tick_fall_70ns = '1' then
		   start_wr_cycle <= wr_ena;
		   start_rd_cycle <= rd_ena;
	   end if;
	end if;
end process;

process (clk)
begin
	if rising_edge(clk) then
	   if init_rst_rise = '1' then
		   start_wr <= '0';
		   start_rd <= '0';		
	   elsif tick_rise_70ns = '1' then
		   start_wr <= start_wr_cycle;
		   start_rd <= start_rd_cycle;
		end if;
	end if;
end process;

-------------------------------Write cycle process--------------------------------------

process (clk)
begin
	if rising_edge(clk) then
	    if init_rst_rise = '1' then
		    busy_wr <= '0'; 
	    elsif tick_rise_70ns = '1' then
			 if end_wr_cycle = '1' then       
			    busy_wr <= '0'; 
		    elsif start_wr_cycle = '1' then
			    busy_wr <= start_wr_cycle;    
		    end if;
	    end if;
	end if;
end process;			

wr_load_p2s <= start_wr_cycle;
			
process (clk)
begin
	if falling_edge(clk) then
	      if tick_rst_rise = '1' then
			   spi_wr <= '0'; 
		      stop_wr <= '0'; 
		      end_wr_cycle <= '0';			
	      elsif tick_fall_70ns = '1' then
		      if stopwr = '1' then       
			      spi_wr <= '0'; 
		      elsif start_wr = '1' then
			      spi_wr <= start_wr;    
		      end if;
		      stop_wr <= stopwr;  -- Resets the write-gate24 counter
		      end_wr_cycle <= ldac_or_clr;
         end if;				
	end if;
end process;

--Count 24 clock rising edges for write cycle
process (clk)
begin
	if rising_edge(clk) then
	   if init_rst_rise ='1' then
		    wr_gate24_cnt <= (others => '0');
		    wr_sync <= '0';
		    wr_sync1 <= '0';
		    wr_sync2 <= '0';
	    elsif tick_rise_70ns = '1' then
		    if stop_wr = '1' then
			    wr_gate24_cnt <= (others => '0');
		    elsif spi_wr = '1' then
			    wr_gate24_cnt <= wr_gate24_cnt + 1;
		    end if;
	       wr_sync <= spi_wr; --Write cycle sync signal
	       wr_sync1 <= wr_sync;
	       wr_sync2 <= wr_sync1;
       end if;			 
	end if;
end process;

ldac_or_clr <= ( not wr_sync1 ) and wr_sync2;
stopwr <= '1' when wr_gate24_cnt = 24 else '0'; --high during 24 clock falling edges

--register type to write

wr_reg_addr <= D23_2 & D22_2 & D21_2 & D20_2;

ldac 		<= ldac_or_clr when wr_reg_addr = x"1" else '0'; -- Write to DAC register
clr  		<= ldac_or_clr when wr_reg_addr = x"3" else '0'; -- Write to clearcode register  
--softctrl	<= ldac_or_clr when wr_reg_addr = x"4" else '0'; -- Write to software control register  
            
--Write to the control (x"2") and software control (x"4") registers : ldac and clr are set to logic '0'

wr_mode <= spi_wr & wr_load_p2s;

wr_reset_p2s <= stop_wr; -- synchronous reset of the 24-Bit parallel to serial converter

--24-Bit parallel to serial converter with the MSB bit first----

process (clk)
begin
	if rising_edge(clk) then
	  if init_rst_rise = '1' then
	     wr_sdout <= '0';
		  wr_idata <= x"000000";
	  elsif tick_rise_70ns = '1' then
		  if wr_reset_p2s = '1' then 
			  wr_sdout <= '0';
			  wr_idata <= x"000000";
		  else				
			  case wr_mode is			
				  when "00" => null;                     -- no operation
				  when "01" => wr_idata <= DATA_WR;      -- load operation
				  when "10" => wr_sdout <= wr_idata(23); -- shift left
						for mloop in 22 downto 0 loop
							wr_idata(mloop+1) <= wr_idata(mloop);
						end loop;
				  when others => null;                   -- no operation otherwise
			  end case;
		  end if;
	   end if;
	end if;
end process;

--------------------------Read cycle process--------------------------------------
process (clk)
begin
	if rising_edge(clk) then
	  if init_rst_rise = '1' then
		   busy_rd <= '0';
	  elsif tick_rise_70ns = '1' then
		  if end_rd_cycle = '1' then       
			  busy_rd <= '0'; 
		  elsif start_rd_cycle = '1' then
			  busy_rd <= start_rd_cycle;    
		  end if;
	  end if;
	end if;
end process;			

rd_load_p2s <= start_rd_cycle or ld_nop;
--Start readback of a register or start a no operation condition
start_rdbck <= busy_rd and ( start_rd or start_nop ); 

process (clk)
begin
	if falling_edge(clk) then
	   if tick_rst_rise = '1' then
		    spi_rd <= '0'; 
		    stop_rd <= '0'; 
		    nop_cond <= '0'; 
		    ld_nop <= '0'; 
		    end_rd_cycle <= '0';		 
	   elsif tick_fall_70ns = '1' then
		      if stoprd = '1' then        
			      spi_rd <= '0'; 
		      elsif start_rdbck = '1' then
			      spi_rd <= start_rdbck;    
		      end if;
	         stop_rd <= stoprd;    -- Resets the read-gate24 counter
	         nop_cond <= stop_rd;
	         ld_nop <= nop_cond;
	         end_rd_cycle <= shift_snd and ( not sel_nop_data);
		end if;
	end if;
end process;

process (clk)
begin
	if rising_edge(clk) then
	   if init_rst_rise = '1' then
		   sel_nop_data <= '0';
		   shift_snd <= '0';
		   start_nop1 <= '0';
		   start_nop <= '0';		
	   elsif tick_rise_70ns = '1' then
		   if end_rd_cycle = '1' then
			   sel_nop_data <= '0';
		   elsif nop_cond = '1' then
			   sel_nop_data <= not sel_nop_data;
		   end if;
	      shift_snd <= sel_nop_data;
	      start_nop1 <= ld_nop;
	      start_nop <= start_nop1;
		end if;
	end if;
end process;

--Count 24 clock rising edges for write cycle
process (clk)
begin
	if rising_edge(clk) then
	   if init_rst_rise = '1' then
		   rd_gate24_cnt <= (others => '0');
		   shift_spi_rd <= '0';		
	   elsif tick_rise_70ns = '1' then
		   if stop_rd = '1' then
			   rd_gate24_cnt <= (others => '0');
		   elsif spi_rd = '1' then
			   rd_gate24_cnt <= rd_gate24_cnt + 1;
		   end if;
		   shift_spi_rd <= spi_rd; --Readback cycle sync signal
	    end if;
	end if;
end process;

stoprd <= '1' when rd_gate24_cnt = 24 else '0'; --high during 24 clock falling edges

rd_mode <= spi_rd & rd_load_p2s;

rd_reset_p2s <= stop_rd; -- synchronous reset of the 24-Bit parallel to serial converter

rd_p2s_data <= x"000000" when sel_nop_data = '1' else DATA_WR;

--24-Bit parallel to serial converter with the MSB bit first----

process (clk)
begin		
	if rising_edge(clk) then
	   if init_rst_rise = '1' then
		   rd_sdout <= '0';
		   rd_idata <= x"000000";		
	   elsif tick_rise_70ns = '1' then
		   if rd_reset_p2s = '1' then 
				rd_sdout <= '0';
				rd_idata <= x"000000";
		   else				
			   case rd_mode is			
				     when "00" => null;                     -- no operation
				     when "01" => rd_idata <= rd_p2s_data;  -- load operation
				     when "10" => rd_sdout <= rd_idata(23); -- shift left
					  for mloop in 22 downto 0 loop
							         rd_idata(mloop+1) <= rd_idata(mloop);
					  end loop;
				     when others => null;                   -- no operation otherwise
			    end case;
		    end if;
		 end if;
	end if;
end process;

rd_sync <= ( spi_rd and sel_nop_data ) or ( shift_spi_rd and ( not ( sel_nop_data ) ) );

nop_sync <= shift_spi_rd and sel_nop_data;

process (clk) 
begin
	if falling_edge(clk) then
	   if tick_rst_rise = '1' then
		   nop_sync_dec <= '0';
		   shift_nop_sync <= '0';		
	   elsif tick_fall_70ns = '1' then
		   nop_sync_dec <= nop_sync;
		   shift_nop_sync <= nop_sync_dec;
		end if;
	end if;
end process;

-- 24-Bit serial to parallel converter ( Data from AD5791 SDO pin connected to SDI port)
process (clk)      
begin         
	if rising_edge(clk) then
	   if init_rst_rise = '1' then
		   s2p <= (others=>'0');		
      elsif ( nop_sync_dec and tick_rise_70ns ) = '1' then	
         s2p <= s2p(22 downto 0) & SDI; 
      end if;		  
	end if;    
end process;

--Register contents reading : data ready for parallel data
drdy <= ( not nop_sync_dec ) and shift_nop_sync;

--Parallel data 
process (clk)      
begin         
	if falling_edge(clk) then
	   if tick_rst_rise = '1' then
		   REG_CONT_latch <= x"000000";
	   elsif tick_fall_70ns = '1' then
         if drdy = '1' then
            REG_CONT_latch <= s2p;
         end if;
		end if;
	end if;
end process;

DAC_RD <= REG_CONT_latch(23 downto 0);		 
  
----------------------------------------------------------------
N_SYNC     <= not ( wr_sync or rd_sync );
N_RESET    <= '1';--not RESET;
N_CLR      <= not clr;
N_LDAC     <= not ldac;
SDO        <= wr_sdout or rd_sdout;
SCK        <= tick2 or tick3;
END_WR_DAC <= end_wr_cycle;
END_RD_DAC <= end_rd_cycle;
----------------------------------------------------------------
		
end Behavioral;

