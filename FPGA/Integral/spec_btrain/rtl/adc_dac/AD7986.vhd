----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    09:01:13 03/09/2013 
-- Design Name: 
-- Module Name:    AD7986 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

use work.utils_pkg.all;

entity AD7986 is
    Port ( 
		clk_i			: in std_logic;
		reset_i		: in std_logic;
		clk_cnv_i	: in std_logic;
		
		--to test
		adc_sync_i  : in std_logic_vector(2 downto 0);
		
		adc_sdo_i	: in std_logic;
		adc_sdi_o	: out std_logic;
		adc_sck_o	: out std_logic;
		adc_bsck_i  : in std_logic;
		adc_cnv_o	: out std_logic;
				
		adc_data_rdy_o	: out std_logic;
		adc_data_o		: out std_logic_vector(17 downto 0)
	);		
end AD7986;

architecture Behavioral of AD7986 is

	--Constant
	constant COUNT_DATA_CONVERSION: integer := 13;
	constant COUNT_CNV_2 : integer := 14;
	constant COUNT_WAIT_2 : integer := 1;
	constant COUNT_ACQUISITION: integer := 5;

	constant N_INV : integer := 12;
	--Type
	type adc_states_t is (
		IDLE,RESET,
		START_CNV_1,
		WAIT_1,
		CONVERSION,
		CNV_2,
		WAIT_2,
		ACQUISITION
	);

	--Signal Declaration
	signal state_s	: adc_states_t;

	signal clk_cnv_rising_s	: std_logic;
	
	signal reset_on_s       : std_logic;
	signal adc_sck_p_s		: std_logic;
	signal adc_sck_n_s		: std_logic;
	signal adc_sck_p_1_s		: std_logic;
	signal adc_sck_n_1_s		: std_logic;
	signal adc_sck_p_2_s		: std_logic;
	signal adc_sck_n_2_s		: std_logic;
	
	signal adc_sck_on_read_s    : std_logic;
	signal adc_sck_on_read_1_s  : std_logic;
	signal adc_sck_on_read_2_s  : std_logic;
	signal adc_sck_on_read_3_s  : std_logic;
	signal adc_sck_on_read_4_s  : std_logic;
	
	signal adc_sck_off_read_s   : std_logic;
	signal adc_sck_off_read_1_s : std_logic;
	signal adc_sck_off_read_2_s : std_logic;
	signal adc_sck_off_read_3_s : std_logic;
	signal adc_sck_off_read_4_s : std_logic;
	
	signal clk_n_s              : std_logic;

	signal adc_cnv_n_s : std_logic;
	signal adc_cnv_p_s : std_logic;
	signal adc_cnv_n_1_s : std_logic;
	signal adc_cnv_p_1_s : std_logic;
	signal adc_cnv_n_2_s : std_logic;
	signal adc_cnv_p_2_s : std_logic;

	signal adc_sck_read_on_s : std_logic;

	signal load_data_out_s	: std_logic;
	signal load_data_out_1_s: std_logic;
	signal serial2paral_falling_s	: std_logic_vector(17 downto 0);
	signal serial2paral_rising_s	: std_logic_vector(17 downto 0);

	signal serial2paral_rising_bsck_s  : std_logic_vector(17 downto 0);
	signal serial2paral_falling_bsck_s : std_logic_vector(17 downto 0);
	
	signal counter_data_conversion_s	: std_logic_vector(3 downto 0);
	signal counter_cnv_2_s				: std_logic_vector(4 downto 0);
	signal counter_wait_2_s				: std_logic_vector(4 downto 0);
	signal counter_acquisition_s		: std_logic_vector(2 downto 0);

	signal late_s : std_logic_vector(15 downto 0);

	attribute keep : string;
	attribute keep of late_s : signal is "TRUE";

	attribute keep of adc_sdo_i : signal is "TRUE";
	attribute keep of adc_cnv_o : signal is "TRUE";
	attribute keep of adc_sck_o : signal is "TRUE";
	
begin


--State Machine
process (clk_i)
begin
	if rising_edge(clk_i) then
		if (reset_i='1') then
			reset_on_s   <= '1';
--			adc_sdi_o    <= '0';
		end if;
		
		adc_sdi_o <= '1';
		load_data_out_s	<= '0';	
		case state_s is
			when IDLE =>
				if (reset_on_s='1') then
					state_s <= RESET;
				else
					if (clk_cnv_rising_s = '1') then
						state_s <= START_CNV_1;
						adc_cnv_p_s				<= '1';
					end if;
				end if;
				adc_sck_p_s       <= '0';
			when RESET =>
				if (reset_i='0') then
					state_s <= IDLE;
				end if;
				reset_on_s   <= '0';
				adc_cnv_p_s  <= '0';
				adc_sck_p_s  <= '0';
				load_data_out_1_s	<= '0';	
			when START_CNV_1 =>
				state_s <= WAIT_1;
				adc_cnv_p_s       <= '0';
				adc_sck_p_s       <= '0';
			when WAIT_1 =>
				state_s <= CONVERSION;
				adc_cnv_p_s       <= '0';
				adc_sck_p_s       <= '0';
			when CONVERSION =>
				if (counter_data_conversion_s >= 
						conv_std_logic_vector(COUNT_DATA_CONVERSION,counter_data_conversion_s'length)) then
					state_s <= CNV_2;
					adc_sck_p_s <= '0';
					adc_cnv_p_s <= '1';
				else
					adc_sck_p_s <= '1';
					adc_cnv_p_s <= '0';
				end if;
			when CNV_2 =>
				if (counter_cnv_2_s >= 
						conv_std_logic_vector(COUNT_CNV_2,counter_cnv_2_s'length)) then
					state_s <= WAIT_2;
				end if;
				adc_cnv_p_s       <= '1';					
				adc_sck_p_s       <= '0';
			when WAIT_2 =>
				if (counter_wait_2_s >= 
						conv_std_logic_vector(COUNT_WAIT_2,counter_wait_2_s'length)) then
					state_s <= ACQUISITION;
				end if;
				adc_cnv_p_s       <= '0';
				adc_sck_p_s       <= '0';
				load_data_out_s   <= '1';	
				load_data_out_1_s <= '1';		
			when ACQUISITION =>
				if (counter_acquisition_s >= 
						conv_std_logic_vector(COUNT_ACQUISITION,counter_acquisition_s'length)) then
					state_s <= IDLE;
					adc_sck_p_s <= '0';
				else
					adc_sck_p_s <= '1';
				end if;
				adc_cnv_p_s       <= '0';
		end case;
	end if;
	if falling_edge(clk_i) then
		if (state_s=RESET) then
			adc_cnv_n_s   <= '0';
			adc_sck_n_s   <= '0';
		else
			if (load_data_out_1_s='1') then
				if ((state_s=ACQUISITION and counter_acquisition_s >= 
						conv_std_logic_vector(COUNT_ACQUISITION,counter_acquisition_s'length)) or
						(state_s=CONVERSION and counter_data_conversion_s >= 
						conv_std_logic_vector(COUNT_DATA_CONVERSION,counter_data_conversion_s'length))) then
					adc_sck_n_s <= '0';
				elsif ((state_s=CONVERSION and counter_data_conversion_s < 
						conv_std_logic_vector(COUNT_DATA_CONVERSION,counter_data_conversion_s'length)) or
						(state_s=ACQUISITION)) then
					adc_sck_n_s <= '1';
				end if;
			end if;
			if (state_s=CNV_2) then
				adc_cnv_n_s	<= '1';
			elsif ((state_s=WAIT_1) or (state_s=WAIT_2)) then
				adc_cnv_n_s	<= '0';
			end if;
		end if;
	end if;
end process;

--Counter for timing
process (clk_i)
begin
	if rising_edge(clk_i) then
			if (state_s=RESET) then
				counter_data_conversion_s	<= (others=>'0');
				counter_cnv_2_s				<= (others=>'0');
				counter_acquisition_s		<= (others=>'0');
				counter_wait_2_s				<= (others=>'0');
			elsif (state_s=IDLE) then
				counter_data_conversion_s	<= (others=>'0');
				counter_cnv_2_s				<= (others=>'0');
				counter_acquisition_s		<= (others=>'0');
			elsif (state_s=CONVERSION) then
				counter_data_conversion_s	<= counter_data_conversion_s+1;
			elsif (state_s=CNV_2) then
				counter_data_conversion_s	<= (others=>'0');
				counter_cnv_2_s				<= counter_cnv_2_s+1;
			elsif (state_s=WAIT_2) then
				counter_wait_2_s 				<= counter_wait_2_s+1;
				counter_cnv_2_s				<= (others=>'0');
			elsif (state_s=ACQUISITION) then
				counter_acquisition_s		<= counter_acquisition_s+1;
				counter_wait_2_s				<= (others=>'0');
			end if;
	end if;
end process;

--Synchronisation of cmd signal of sck and cnv
process (clk_i) 
begin
	if falling_edge(clk_i) then
		if (state_s=RESET) then
			adc_sck_n_1_s <= '0';
			adc_cnv_n_1_s <= '0';
			adc_sck_n_2_s <= '0';
			adc_cnv_n_2_s <= '0';
		else
			adc_sck_n_1_s <= adc_sck_n_s;
			adc_cnv_n_1_s <= adc_cnv_n_s;
			adc_sck_n_2_s <= adc_sck_n_1_s;
			adc_cnv_n_2_s <= adc_cnv_n_1_s;
		end if;
	end if;
end process;
process (clk_i) 
begin
	if rising_edge(clk_i) then
		if (state_s=RESET) then
			adc_sck_p_1_s <= '0';
			adc_cnv_p_1_s <= '0';
			adc_sck_p_2_s <= '0';
			adc_cnv_p_2_s <= '0';
		else
			adc_sck_p_1_s <= adc_sck_p_s;
			adc_cnv_p_1_s <= adc_cnv_p_s;
			adc_sck_p_2_s <= adc_sck_p_1_s;
			adc_cnv_p_2_s <= adc_cnv_p_1_s;
		end if;
	end if;
end process;
--adc_cnv_o <= adc_cnv_n_1_s or adc_cnv_p_1_s;
adc_cnv_o <= adc_cnv_n_s or adc_cnv_p_s;

--Mux to be able to adapte read clk of deserialisation
process (clk_i) 
begin
	if falling_edge(clk_i) then
		if (state_s=RESET) then
			adc_sck_on_read_s	<= '0';
			adc_sck_on_read_1_s	<= '0';
			adc_sck_on_read_2_s	<= '0';
			adc_sck_on_read_3_s	<= '0';
			adc_sck_on_read_4_s	<= '0';
		else
			adc_sck_on_read_1_s <= adc_sck_p_s;
			adc_sck_on_read_2_s <= adc_sck_on_read_1_s;
			adc_sck_on_read_3_s <= adc_sck_on_read_2_s;
			adc_sck_on_read_4_s <= adc_sck_on_read_3_s;
		end if;	
		case adc_sync_i(1 downto 0) is
			when "00" =>
				adc_sck_on_read_s   <= adc_sck_on_read_1_s;
			when "01" =>
				adc_sck_on_read_s   <= adc_sck_on_read_2_s;
			when "10" =>
				adc_sck_on_read_s   <= adc_sck_on_read_3_s;
			when "11" =>
				adc_sck_on_read_s   <= adc_sck_on_read_4_s;
			when others =>
				adc_sck_on_read_s   <= adc_sck_on_read_1_s;
		end case;
	end if;	
	if falling_edge(clk_i) then
		if (state_s=RESET) then
			adc_sck_off_read_s   <= '0';
			adc_sck_off_read_1_s <= '0';
			adc_sck_off_read_2_s <= '0';
			adc_sck_off_read_3_s <= '0';
			adc_sck_off_read_4_s <= '0';
		else
			adc_sck_off_read_1_s <= adc_sck_n_s;
			adc_sck_off_read_2_s <= adc_sck_off_read_1_s;
			adc_sck_off_read_3_s <= adc_sck_off_read_2_s;
			adc_sck_off_read_4_s <= adc_sck_off_read_3_s;
		end if;	
		case adc_sync_i(1 downto 0) is
			when "00" =>
				adc_sck_off_read_s   <= adc_sck_off_read_1_s;
			when "01" =>
				adc_sck_off_read_s   <= adc_sck_off_read_2_s;
			when "10" =>
				adc_sck_off_read_s   <= adc_sck_off_read_3_s;
			when "11" =>
				adc_sck_off_read_s   <= adc_sck_off_read_4_s;
			when others =>
				adc_sck_off_read_s   <= adc_sck_off_read_1_s;
		end case;
	end if;	
end process;

adc_sck_read_on_s <= adc_sck_off_read_s when adc_sync_i(2)='0' else adc_sck_on_read_s;

--Deserialise in function of adc_sync cmd value
process (clk_i) 
begin 	
   if rising_edge(clk_i) then 
      if (state_s=RESET) then 
			serial2paral_rising_s <= (others=>'0'); 
		else
			if (load_data_out_1_s='1' and adc_sck_read_on_s='1' and adc_sync_i(2)='0')  then
				serial2paral_rising_s <= serial2paral_rising_s(16 downto 0) & adc_sdo_i;                       
			end if;
      end if; 
   end if; 
   if falling_edge(clk_i) then 
      if (state_s=RESET) then 
			serial2paral_falling_s <= (others=>'0'); 
		else
			if (load_data_out_1_s='1' and adc_sck_read_on_s='1' and adc_sync_i(2)='1')  then
				serial2paral_falling_s <= serial2paral_falling_s(16 downto 0) & adc_sdo_i;                       
			end if;
      end if; 
   end if; 
end process; 

--Serial to par with the back sck
process(adc_bsck_i,reset_i)
begin
   if rising_edge(adc_bsck_i) then 
      if (reset_i='1') then 
			serial2paral_rising_bsck_s <= (others=>'0'); 
		else
			serial2paral_rising_bsck_s <= serial2paral_rising_bsck_s(16 downto 0) & adc_sdo_i;                       
      end if; 
   end if; 
   if falling_edge(adc_bsck_i) then 
      if (reset_i='1') then 
			serial2paral_falling_bsck_s <= (others=>'0'); 
		else
			serial2paral_falling_bsck_s <= serial2paral_falling_bsck_s(16 downto 0) & adc_sdo_i;                       
      end if; 
   end if; 
end process;

--Resynchronize output
process(clk_i)
begin
	if rising_edge(clk_i) then
		if (reset_on_s='1') then
			adc_data_rdy_o			<= '0';
			adc_data_o				<= (others=>'0');
		else
			adc_data_rdy_o			<= load_data_out_s;
			if (load_data_out_s='1' and adc_sync_i(2)='1') then
				if (adc_sync_i(1 downto 0)="11") then
					adc_data_o <= serial2paral_falling_bsck_s;
				else
					adc_data_o <= serial2paral_falling_s;
				end if;
			elsif (load_data_out_s='1' and adc_sync_i(2)='0') then
				if (adc_sync_i(1 downto 0)="11") then
					adc_data_o <= serial2paral_rising_bsck_s;
				else
					adc_data_o <= serial2paral_rising_s;
				end if;
			end if;
		end if;
	end if;
end process;

--ADC_SCK generation
clk_n_s <= not clk_i;

   cmp_sck_out : ODDR2
   generic map(
      DDR_ALIGNMENT => "NONE", -- Sets output alignment to "NONE", "C0", "C1" 
      INIT => '0', -- Sets initial state of the Q output to '0' or '1'
      SRTYPE => "SYNC") -- Specifies "SYNC" or "ASYNC" set/reset
   port map (
      Q => adc_sck_o,      -- 1-bit output data
      C0 => clk_i,         -- 1-bit clock input
      C1 => clk_n_s,       -- 1-bit clock input
      CE => '1',           -- 1-bit clock enable input
      D0 => '0',           -- 1-bit data input (associated with C0)
      D1 => adc_sck_p_s,   -- 1-bit data input (associated with C1)
      R => '0',--reset_i,        -- 1-bit reset input
      S => '0'             -- 1-bit set input
   );

	cmp_rising_cnv : RisingDetect 
		port map (
			clk_i   => clk_i,
			reset_i => reset_i,
			
			s_i => clk_cnv_i,
			s_o => clk_cnv_rising_s
		);

end Behavioral;

