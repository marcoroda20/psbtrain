----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    08:53:02 06/12/2013 
-- Design Name: 
-- Module Name:    ADOverRange - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity ADOverRange is
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		nb_overrange_i		: in std_logic_vector(15 downto 0);
		max_adc_overr_i	: in std_logic_vector(17 downto 0);
		clear_overrange_i	: in std_logic;
		
		adc_data_rdy_i	: in std_logic;
		adc_data_i		: in std_logic_vector(17 downto 0);
		
		overrange_o		: out std_logic
	);
end ADOverRange;

architecture Behavioral of ADOverRange is

	--Constant
	constant NB_BIT_DIVIDER : Integer := 32;

	--Signal
	signal data_rdy_s 	: std_logic;

	signal acc_value_s	: std_logic_vector(31 downto 0);
	signal sum_value_s	: std_logic_vector(31 downto 0);
	signal divisor_s		: std_logic_vector(31 downto 0);
	
	signal count_nb_val_s	: std_logic_vector(15 downto 0);
	
	signal start_div_s		: std_logic;
	signal end_divider_s		: std_logic;
	signal end_div_s			: std_logic;
	signal divider_output_s	: std_logic_vector(31 downto 0);
	signal result_div_s		: std_logic_vector(17 downto 0);
	
	signal clear_overrange_s	: std_logic;
	signal overrange_s			: std_logic;
	
begin

	--Accumulation of ADC value and divider
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				count_nb_val_s <= (others=>'0');
				acc_value_s <= (others=>'0');
				sum_value_s <= (others=>'0');
			else
				if (data_rdy_s='1') then
					if (count_nb_val_s < nb_overrange_i-1) then
						if adc_data_i(adc_data_i'length-1)='0' then
							sum_value_s <= sum_value_s + adc_data_i;
						else
							sum_value_s <= sum_value_s + 1 + not adc_data_i;
						end if;
						start_div_s <= '0';
						count_nb_val_s <= count_nb_val_s + 1;
					else
						if adc_data_i(adc_data_i'length-1)='0' then
							acc_value_s <= sum_value_s + adc_data_i;
						else
							acc_value_s <= sum_value_s + 1 + not adc_data_i;
						end if;
						sum_value_s <= (others=>'0');
						start_div_s <= '1';
						count_nb_val_s <= (others=>'0');
					end if;
				end if;
			end if;
		end if;
	end process;

	divisor_s <= x"0000" & nb_overrange_i;
	
	cmp_mean_divider : divider 
		generic map (NB_BIT=>NB_BIT_DIVIDER)
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			a_i	=> acc_value_s,
			b_i	=> divisor_s,

			start_divider_i	=> start_div_s,
			cmd_divider_i		=> '0',
			
			q_o	=> divider_output_s,
			r_o	=> open,
			f_o	=> open,

			divide_by_0_o	=> open,
			end_divider_o	=> end_divider_s
		);
	result_div_s <= divider_output_s(17 downto 0);
	
	--Output overrange
	overrange_o <= overrange_s;
	process (clk_i) 
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				overrange_s <= '0';
			else
				if (overrange_s='0') then
					if (result_div_s >= max_adc_overr_i) then
						overrange_s <= '1';
					else
						overrange_s <= '0';
					end if;
				else
					if (clear_overrange_i='1') then
						overrange_s <= '0';
						clear_overrange_s <= '0';
					end if;
				end if;
			end if;
		end if;
	end process;
	
	--ADC data ready input and end divider
	cmp_adc_input : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> adc_data_rdy_i,
			s_o	=> data_rdy_s
		);

	cmp_end_div : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> end_divider_s,
			s_o	=> end_div_s
		);

end Behavioral;

