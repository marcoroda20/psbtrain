----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    12:00:37 05/28/2013 
-- Design Name: 
-- Module Name:    Acquisition_AD7986_corr - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.adc_dac_pkg.all;

entity Acquisition_AD7986_corr is
	port(
		clk_i			: in std_logic;
		clk_adc_i	: in std_logic;
		reset_i		: in std_logic;
		clk_cnv_i	: in std_logic;

		adc_sync_i  : in std_logic_vector(2 downto 0);

		adc_sdo_i	: in std_logic;
		adc_sdi_o	: out std_logic;
		adc_sck_o	: out std_logic;
		adc_bsck_i  : in std_logic;
		adc_cnv_o	: out std_logic;
		
		gain_i	: in std_logic_vector(31 downto 0);
		offset_i : in std_logic_vector(31 downto 0);

		adc_data_rdy_o	: out std_logic;
		adc_data_o		: out std_logic_vector(17 downto 0);

		adc_corr_rdy_o	: out std_logic;
		adc_corr_o		: out std_logic_vector(17 downto 0)
	);
end Acquisition_AD7986_corr;

architecture Behavioral of Acquisition_AD7986_corr is
	
	--Signal
	signal adc_data_rdy_s	: std_logic;
	signal adc_data_s			: std_logic_vector(17 downto 0);
	
begin

	cmp_AD7986 : AD7986 
		 Port map( 
			clk_i			=> clk_adc_i,
			reset_i		=> reset_i,
			clk_cnv_i	=> clk_cnv_i,
			
			adc_sync_i  => adc_sync_i,

			adc_sdo_i	=> adc_sdo_i,
			adc_sdi_o	=> adc_sdi_o,
			adc_sck_o	=> adc_sck_o,
			adc_bsck_i  => adc_bsck_i,
			adc_cnv_o	=> adc_cnv_o,
			
			adc_data_rdy_o	=> adc_data_rdy_s,
			adc_data_o		=> adc_data_s
		);		

	adc_data_rdy_o	<= adc_data_rdy_s;
	adc_data_o		<= adc_data_s;

	cmp_correction : correction 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			adc_i			=> adc_data_s,
			adc_drdy_i	=> adc_data_rdy_s,

			gain_i	=> gain_i,
			offset_i	=> offset_i,

			adc_o			=> adc_corr_o,
			adc_drdy_o	=> adc_corr_rdy_o
		);

end Behavioral;

