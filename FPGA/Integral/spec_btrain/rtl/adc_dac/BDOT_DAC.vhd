----------------------------------------------------------------------------------
-- Company: CERN - TE/MSC-MM
-- Engineer: David Giloteaux
-- 
-- Create Date:    09:12:49 06/06/2013 
-- Design Name: 
-- Module Name:    BDOT_DAC - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library work;
use work.utils_pkg.all;

entity BDOT_DAC is
    Port ( clk_i, reset_i : in  STD_LOGIC;        -- Clock period = 12.5 ns or 10ns (80MHz or 100MHz)
	        Bdot_i : in STD_LOGIC_VECTOR(17 downto 0); -- in straight binary code
			  Bdot_Valid_i : in STD_LOGIC; -- High for at least two clock periods (falling edge synchronous)
           dac_cs_o : out  STD_LOGIC;
           dac_clk_o : out  STD_LOGIC;
           dac_sdo_o : out  STD_LOGIC := '0' );
end BDOT_DAC;

architecture Behavioral of BDOT_DAC is

--Signal to garanted 2 period high for Bdot_Valid_i
signal Bdot_Valid_s : std_logic;
signal Bdot_Valid_rising_s : std_logic;

signal Bdot_Data, idata : std_logic_vector (17 downto 0):= "00" & x"0000";
signal rst1, rst2, init_rst, ld_p2s, st1 : std_logic := '0';
signal start_spi, spi_cs, stopcnt, stop_cnt, rst_p2s, daccs : std_logic := '0';
signal gate18_cnt : std_logic_vector (4 downto 0) := '0' & x"0";
signal cnt_3bits : std_logic_vector (2 downto 0) := "000";
signal mode : std_logic_vector (1 downto 0):= "00";

signal clk_div2, clk1_div2, clk2_div2, tick_rise_div2, tick_fall_div2 : std_logic := '0';

begin

-- Components to garantied 2 period high for Bdot_Valid_i
cmp_RisingDetect_Bdot_valid : RisingDetect 
	port map(
		clk_i	  => clk_i,
		reset_i => reset_i,
		
		s_i => Bdot_Valid_i,
		s_o => Bdot_Valid_rising_s
	);
	
cmp_monostable_Bdot_valid : monostable 
	generic map(
		g_n_clk => 2
	)
	port map(
		clk_i	  => clk_i,
		reset_i => reset_i,
		
		s_i => Bdot_Valid_rising_s,
		s_o => Bdot_Valid_s
	);
	
-----------------------Divide clock by 2 & tick fall or rise signals---------------------------------
process (clk_i)
begin
    if rising_edge(clk_i) then
        clk_div2 <= not(clk_div2);        		  
	 end if;
	 if falling_edge(clk_i) then
	     clk1_div2      <= clk_div2;
		  clk2_div2      <= clk1_div2;
		  tick_rise_div2 <= clk1_div2 and ( not clk2_div2 );
	     tick_fall_div2 <= ( not clk1_div2 ) and clk2_div2;
    end if; 	
end process;

----------------------- 18-Bit parallel to serial converter with the MSB bit first ------------------

process (clk_i)
begin
		if rising_edge(clk_i) then
		   if tick_rise_div2 = '1' then
		      if Bdot_Valid_s = '1' then
				   Bdot_Data <= Bdot_i;
			   end if;
		      rst1  <= Bdot_Valid_s;		
			   rst2  <= rst1;
         end if;
         if tick_fall_div2 = '1' then
		      st1 <= rst2;	
         end if;							
		end if;
end process;

init_rst  <= rst1;
ld_p2s    <= rst2;
start_spi <= st1;

process (clk_i)
begin
		if rising_edge(clk_i) then
		   if tick_rise_div2 = '1' then
		      if stopcnt = '1' then        
				   spi_cs <= '0'; 
			   elsif start_spi = '1' then
			      spi_cs <= start_spi;    
			   end if;
		      stop_cnt <= stopcnt; -- Resets the gate18 counter
			end if;
         if tick_fall_div2 = '1' then
			    if stop_cnt = '1' then
				    gate18_cnt <= (others => '0');
			    elsif spi_cs = '1' then
			       gate18_cnt <= gate18_cnt + 1; --Count 18 clock rising edges for write cycle
			    end if;
				 daccs <= spi_cs;
		   end if;			
		end if;
end process;

stopcnt <= '1' when gate18_cnt = 18 else '0'; -- high during 18 clock rising edges
rst_p2s <= stop_cnt or init_rst;              -- synchronous reset of the 18-Bit parallel to serial converter
mode    <= spi_cs & ld_p2s;

process (clk_i)
begin
		if rising_edge(clk_i) then
		   if tick_fall_div2 = '1' then
		      if rst_p2s = '1' then 
				   dac_sdo_o <= '0';
               idata     <= "00" & x"0000";
            else				
				   case mode is			
					     when "00" => null;                              -- no operation
					     when "01" => idata     <= Bdot_Data;            -- load operation
					     when "10" => dac_sdo_o <= idata(17);            -- dac_sdo_o = sn74ahc595d SER data pin
							            for mloop in 16 downto 0 loop      -- shift left 
								             idata(mloop+1) <= idata(mloop);
							            end loop;
					     when others => null;                            -- no operation otherwise
				   end case;
			   end if;
			end if;
		end if;
end process;

dac_cs_o  <= not daccs; --sn74ahc595d register clock RCLK
dac_clk_o <= clk_div2;  --sn74ahc595d gated serial register clock SRCLK
						  
----------------------------------------------------------------------------------------------------------------

end Behavioral;

