----------------------------------------------------------------------------------
-- Company: 		CERN/TE-MS-MM
-- Engineer: 		Daniel Oberson
-- 
-- Description: Package for ADC and DAC for spec_btrain
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.fmc_adc2M18b2ch_pkg.all;

package adc_dac_pkg is

	component Acquisition_AD7986_corr 
		port(
			clk_i			: in std_logic;
			clk_adc_i	: in std_logic;
			reset_i		: in std_logic;
			clk_cnv_i	: in std_logic;

			adc_sdo_i	: in std_logic;
			adc_sdi_o	: out std_logic;
			adc_sck_o	: out std_logic;
			adc_bsck_i  : in std_logic;
			adc_cnv_o	: out std_logic;
			
			--to test
			adc_sync_i  : in std_logic_vector(2 downto 0);

			gain_i	: in std_logic_vector(31 downto 0);
			offset_i : in std_logic_vector(31 downto 0);

			adc_data_rdy_o	: out std_logic;
			adc_data_o		: out std_logic_vector(17 downto 0);

			adc_corr_rdy_o	: out std_logic;
			adc_corr_o		: out std_logic_vector(17 downto 0)
		);
	end component;

	component acquisition_core 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			--ADC control bus
			clk_adc_i	: in std_logic;
			clk_cnv_i	: in std_logic;

			--to test
			adc_sync_i  : in std_logic_vector(2 downto 0);

			adc_sdo_i	: in std_logic;
			adc_sdi_o	: out std_logic;
			adc_sck_o	: out std_logic;
			adc_bsck_i  : in std_logic;
			adc_cnv_o	: out std_logic;

			--Correction coefficient
			gain_i	: in std_logic_vector(31 downto 0);
			offset_i : in std_logic_vector(31 downto 0);
			
			--Register to choose the type of integral calculation
			reg_val_cfg_sel_integral_i : in std_logic_vector(1 downto 0);
			reg_int_c0_used_i          : in std_logic;
			reg_int_range_sel_i        : in std_logic_vector(3 downto 0);
			soft_fixed_i               : in std_logic;
			soft_val_i                 : in std_logic_vector(31 downto 0);

			--Register
			Bsmooth_i	: in std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0);
			corr_fact_i	: in std_logic_vector(NB_BITS_A_GAIN_VALUE-1 downto 0);
			Vo_i			: in std_logic_vector(NB_BITS_OFFSET_VALUE-1 downto 0);

			--Input information signals
			low_marker_i  : in std_logic;
			high_marker_i : in std_logic;

			--Marker values
			low_marker_val_i  : in std_logic_vector(31 downto 0);
			high_marker_val_i : in std_logic_vector(31 downto 0);

			C0_i		      : in std_logic;
			calib_status_i : in std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);
			
			--Output
			B_error_low_o  : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_error_high_o : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);

			adc_data_rdy_o	: out std_logic;
			adc_data_o		: out std_logic_vector(17 downto 0);

			adc_corr_rdy_o	: out std_logic;
			adc_corr_o		: out std_logic_vector(17 downto 0);
			
			B_data_rdy_o	: out std_logic;
			BKp1_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			
			Bdot_data_rdy_o: out std_logic;
			Bdot_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0)
		);
	end component;

	component AD5791 
		Port ( 
			CLK : in  STD_LOGIC; -- CLOCK = 100MHz
			SDI : in  STD_LOGIC;
			--DAC_CE : in  STD_LOGIC;
			RESET : in std_logic;
			SDO, SCK, N_LDAC, N_SYNC : out  STD_LOGIC;
			N_RESET, N_CLR, END_WR_DAC, END_RD_DAC : out  STD_LOGIC;
			DAC_WR : in  STD_LOGIC_VECTOR (23 downto 0);
			DAC_RD : out  STD_LOGIC_VECTOR (23 downto 0) 
		);	  
	end component;

	component BDOT_DAC
    Port ( clk_i, reset_i : in  STD_LOGIC;        -- Clock period = 12.5 ns or 10ns (80MHz or 100MHz)
	        Bdot_i : in STD_LOGIC_VECTOR(17 downto 0); -- in straight binary code
			  Bdot_Valid_i : in STD_LOGIC; -- High for at least two clock periods (falling edge synchronous)
           dac_cs_o : out  STD_LOGIC;
           dac_clk_o : out  STD_LOGIC;
           dac_sdo_o : out  STD_LOGIC := '0' );
	end component;
	
		
	component Bup_Bdown is
		 Port ( 
			Gbl_Reset_i      : in  STD_LOGIC; --Active High
			C0_Reset_i       : in  STD_LOGIC; --Active High
			Bup_i            : in  STD_LOGIC; --Active High
			Bdown_i          : in  STD_LOGIC; -- Active High     
			Bupdown_drdy_o   : out STD_LOGIC; -- Active High
			B_Cnt_o          : out STD_LOGIC_VECTOR (31 downto 0);
			Scale_B_Cnt_o	  : out STD_LOGIC_VECTOR (31 downto 0);
			Clk_i            : in  STD_LOGIC --10MHz or 20MHz		
			--C0 pulse programmable delay in clock period number
			--pdly_cst_i       : in  STD_LOGIC_VECTOR (19 downto 0);
			); 		  
	end component;

	component AD7986 
		 Port ( 
			clk_i			: in std_logic;
			reset_i		: in std_logic;
			clk_cnv_i	: in std_logic;
			
			--to test
			adc_sync_i  : in std_logic_vector(2 downto 0);

			adc_sdo_i	: in std_logic;
			adc_sdi_o	: out std_logic;
			adc_sck_o	: out std_logic;
			adc_bsck_i  : in std_logic;
			adc_cnv_o	: out std_logic;
					
			adc_data_rdy_o	: out std_logic;
			adc_data_o		: out std_logic_vector(17 downto 0)
		);		
	end component;

	component correction 
		port(
			clk_i 	: in std_logic;
			reset_i : in std_logic;
			
			adc_i : in std_logic_vector(17 downto 0);
			adc_drdy_i : in std_logic;
		
			gain_i : in std_logic_vector(31 downto 0);
			offset_i : in std_logic_vector(31 downto 0);
			
			adc_o : out std_logic_vector(17 downto 0);
			adc_drdy_o : out std_logic
		);
	end component;

	component ADOverRange 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			nb_overrange_i		: in std_logic_vector(15 downto 0);
			max_adc_overr_i	: in std_logic_vector(17 downto 0);
			clear_overrange_i	: in std_logic;
			
			adc_data_rdy_i	: in std_logic;
			adc_data_i		: in std_logic_vector(17 downto 0);
			
			overrange_o		: out std_logic
		);
	end component;

	component mux_dac_val 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			--Bk
			BKp1_F_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_data_rdy_F_i	: in std_logic;

			BKp1_D_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_data_rdy_D_i	: in std_logic;

			--B
			B_i            : in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_data_rdy_i   : in std_logic;

			--Bdot
			Bdot_F_i				: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			Bdot_data_rdy_F_i	: in std_logic;

			Bdot_D_i				: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			Bdot_data_rdy_D_i	: in std_logic;

			--Bdot
			Bdot_i            : in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			Bdot_data_rdy_i   : in std_logic;
		
			--ADC value
			adc_data_F_i		: in std_logic_vector(17 downto 0);
			adc_data_rdy_F_i	: in std_logic;

			adc_data_D_i		: in std_logic_vector(17 downto 0);
			adc_data_rdy_D_i	: in std_logic;

			adc_data_F_corr_i		: in std_logic_vector(17 downto 0);
			adc_data_rdy_F_corr_i: in std_logic;

			adc_data_D_corr_i		: in std_logic_vector(17 downto 0);
			adc_data_rdy_D_corr_i: in std_logic;

			--White Rabbit value
			wr_B_val_i     : in std_logic_vector(31 downto 0);
			wr_Bdot_val_i  : in std_logic_vector(31 downto 0);
			
			wr_G_val_i        : in std_logic_vector(31 downto 0); -- 23/10/2015
			wr_S_val_i        : in std_logic_vector(31 downto 0); -- 23/10/2015
			
			wr_I_val_i     : in std_logic_vector(31 downto 0);
			wr_drdy_i      : in std_logic;
			wr_I_drdy_i    : in std_logic;                        -- 23/10/2015

			--register
			reg_val_sel_i			: in std_logic_vector(3 downto 0);
			reg_val_sel_range_i	: in std_logic_vector(3 downto 0);
			
			--Output
			val_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			data_rdy_o	: out std_logic
		);
	end component;

	component switch_dac 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			--Value for DAC selectionned by Mux_dac_val
			val_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			data_rdy_i	: in std_logic;

			--Value for DAC for electronic calibration
			calib_DAC_wr_i 	: in std_logic_vector(23 downto 0);	
			calib_DAC_rd_o 	: out std_logic_vector(23 downto 0);
			calib_end_dac_rd_o: out std_logic;
			calib_end_dac_wr_o: out std_logic;
			
			--Value for DAC from the register controled by the PC
			reg_dac_bus_i	: in std_logic_vector(23 downto 0);
			
			--Control
			reg_val_sel_fpga_bus_i	: in std_logic;
			calib_status_i				: in std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);

			--Control bus for DAC
			end_dac_rd_i	: in std_logic;
			end_dac_wr_i	: in std_logic;
			DAC_rd_i 		: in std_logic_vector(23 downto 0);		-- From DAC registers read interface
			DAC_wr_o 		: out std_logic_vector(23 downto 0)	-- To DAC registers write interface
		);
	end component;

	component AD5292_SPI 
		Port (
			ctrl_bus_i   : in  STD_LOGIC_VECTOR (31 downto 0);
			clk_i        : in STD_LOGIC;
			dpot_reset_i : in STD_LOGIC;
			sclk_o       : out  STD_LOGIC;
			sdout_o      : out  STD_LOGIC;
			sdin_i       : in  STD_LOGIC;
			rdy1_i       : in  STD_LOGIC; --ready active low
			rdy2_i       : in  STD_LOGIC;	--ready active low		
			gbl_reset_i  : in STD_LOGIC;  --active low reset
			dpot_reset_o : out  STD_LOGIC;
			n_sync_o     : out  STD_LOGIC
		);
	end component;

	component AD5292
		port (
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			dpot_reset_i : in std_logic;
			dpot_wr_i    : in std_logic;

			data_1_i   : in  std_logic_vector (9 downto 0);
			data_2_i   : in  std_logic_vector (9 downto 0);
			
			sclk_o       : out  std_logic;
			sdout_o      : out  std_logic;
			sdin_i       : in  std_logic;
			rdy1_i       : in  std_logic; -- READY active low
			rdy2_i       : in  std_logic;
			dpot_reset_o : out  std_logic;
			n_sync_o     : out  std_logic;
			done_o       : out  std_logic
		);
	end component;

end adc_dac_pkg;

