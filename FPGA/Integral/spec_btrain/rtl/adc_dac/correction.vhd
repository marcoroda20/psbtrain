----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson 
-- 
-- Create Date:    09:10:43 05/23/2013 
-- Design Name: 
-- Module Name:    correction - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity correction is
	port(
		clk_i 	: in std_logic;
		reset_i : in std_logic;
		
		adc_i : in std_logic_vector(17 downto 0);
		adc_drdy_i : in std_logic;
	
		gain_i : in std_logic_vector(31 downto 0);
		offset_i : in std_logic_vector(31 downto 0);
		
		adc_o : out std_logic_vector(17 downto 0);
		adc_drdy_o : out std_logic
	);
end correction;

architecture Behavioral of correction is

	--type
	type corr_state is
	(
		IDLE,
		LOAD_INPUT,CORR_MULT,CORR_SUB,LOAD_OUTPUT,OVERRANGE
	);

	--Signal
	signal state_s			: corr_state;
	
	signal adc_s 			: std_logic_vector(65 downto 0);
	
	signal gain_s	: std_logic_vector(32 downto 0);
	signal offset_s: std_logic_vector(35 downto 0);
	
	signal A_s	: std_logic_vector(35 downto 0);
	signal B_s	: std_logic_vector(35 downto 0);
	signal output_mult_s : std_logic_vector(71 downto 0);
	
	signal start_mult_s	: std_logic;
	signal end_mult_s		: std_logic;
	signal end_m_s			: std_logic;
	
	attribute keep : string;
	attribute keep of offset_s : signal is "TRUE";
begin

	--State machine
	process (clk_i,reset_i,adc_i,adc_s,gain_i,offset_i,offset_s,output_mult_s)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				start_mult_s <= '0';
				adc_s <= (others=>'0');			
				adc_o <= (others=>'0');
				adc_drdy_o <= '0';
				state_s <= IDLE;
			else
				case state_s is
					when IDLE => 
						if (adc_drdy_i='1') then
							state_s <= LOAD_INPUT;
						end if;
						adc_drdy_o <= '0';
					when LOAD_INPUT =>
						state_s <= CORR_SUB;
						adc_drdy_o <= '0';
						A_s <= adc_i(17) & adc_i(17) & adc_i(17) & adc_i(17) & adc_i & "00" & x"000";
						B_s <= x"0" & gain_i;
						if (offset_i(31)='0') then
							offset_s <= "0000" & offset_i;
						else
							offset_s <= "1111" & offset_i;
						end if;
					when CORR_SUB =>
						state_s <= CORR_MULT;
						adc_drdy_o <= '0';
						start_mult_s <= '0';
						A_s <= A_s - offset_s;
					when CORR_MULT =>
						if (end_mult_s='1') then
							state_s <= LOAD_OUTPUT;
						end if;
						adc_drdy_o <= '0';
						start_mult_s <= '1';
					when LOAD_OUTPUT =>
						state_s <= OVERRANGE;
						adc_s <= output_mult_s(65 downto 0);
					when OVERRANGE=>
						state_s <= IDLE;
						adc_drdy_o <= '1';
						--Saturation to avoid overflow
						if (adc_s(63 downto 45)>=("001" & x"FFFF")) then
							adc_o <= "01" & x"FFFF";
						elsif (adc_s(63 downto 45)<=("110" & x"0000")) then
							adc_o <= "10" & x"0000";
						else
							adc_o <= adc_s(62 downto 45) + adc_s(44);
						end if;
				end case;
			end if;
		end if;
	end process;
		
	--Mult
	cmp_Mult36x36_signed : Mult36x36_signed 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			A_i	=> A_s,
			B_i	=> B_s,
					
			start_i	=> start_mult_s,

			result_o		=> output_mult_s,
			result_rdy_o=> end_m_s
		);
	
	cmp_end_mult : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> end_m_s,
			s_o	=> end_mult_s
		);
	
end Behavioral;

