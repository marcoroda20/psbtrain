----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    15:16:59 17/05/20123
-- Design Name: 
-- Module Name:    calib_cmd_dac - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity calib_cmd_dac is
    Port ( 
		clk_i 	: in std_logic;
		reset_i	: in std_logic;

		start_dac_i 			: in std_logic;
		DAC_cmd_i 				: in DAC_cmd;								--Command Bus for the DAC
		DAC_val_register_i	: in std_logic_vector(19 downto 0);	--Input DAC register

		DAC_wr_o 			: out std_logic_vector(23 downto 0);	-- To DAC registers write interface
		DAC_rd_i 			: in std_logic_vector(23 downto 0);		-- From DAC registers read interface

		end_dac_rd_i	: in std_logic;
		end_dac_wr_i	: in std_logic;

		end_dac_o		: out std_logic;
		error_status_o	: out std_logic
	);
end calib_cmd_dac;

architecture Behavioral of calib_cmd_dac is

	--Type
	type state_cmd_dac is
	(
		IDLE,
		LOAD_CMD,
		WR_DAC,RD_DAC,
		CHECK_DAC
	);
	
	--Signal
	signal state_s 		: state_cmd_dac;
	
	signal start_dac_s	: std_logic;
	signal end_dac_wr_s	: std_logic;
	signal end_dac_rd_s	: std_logic;
	
	signal dac_cal_wr_s : std_logic_vector(23 downto 0);
	signal dac_cal_rd_s : std_logic_vector(23 downto 0);
	
	signal DAC_cmd_s : DAC_cmd;		
	
begin

	--State machine
	process (clk_i) 
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				error_status_o <= '0';
				dac_cal_wr_s <= AD5791_NOOP & x"00000";
				DAC_wr_o <= x"000000";
				state_s <= IDLE;
			else
				case state_s is
					when IDLE => 
						if (start_dac_s='1') then
							state_s <= LOAD_CMD;
							DAC_cmd_s <= DAC_cmd_i;
						end if;
						end_dac_o <= '1';
					when LOAD_CMD => 
						state_s <= WR_DAC;
						if (DAC_cmd_s=DAC_CMD_NOOP) then
							dac_cal_wr_s <= AD5791_NOOP & x"00000";
							dac_cal_rd_s <= AD5791_NOOP & x"00000";
						elsif (DAC_cmd_s=DAC_CMD_CFG) then
							dac_cal_wr_s <= AD5791_WR_CTRL & x"00002";
							dac_cal_rd_s <= AD5791_RD_CTRL & x"00000";
						elsif (DAC_cmd_s=DAC_CMD_POS or DAC_cmd_s=DAC_CMD_NEG) then
							dac_cal_wr_s <= AD5791_WR_VAL & DAC_val_register_i;
							dac_cal_rd_s <= AD5791_RD_VAL & x"00000";
						end if;
						end_dac_o <= '0';
					when WR_DAC => 
						if (end_dac_wr_s='1') then
							state_s <= RD_DAC;
						end if;
						DAC_wr_o <= dac_cal_wr_s;
						end_dac_o <= '0';
					when RD_DAC => 
						if (end_dac_rd_s='1') then
							state_s <= CHECK_DAC;
						end if;
						DAC_wr_o <= dac_cal_rd_s;
						end_dac_o <= '0';
					when CHECK_DAC => 
						state_s <= IDLE;				
						if (DAC_rd_i(19 downto 0)=dac_cal_wr_s(19 downto 0)) then
							error_status_o <= '0';
						else
							error_status_o <= '1';
						end if;
						end_dac_o <= '0';
				end case;
			end if;
		end if;
	end process;
	
	--Input rising edge
	cmp_start_dac : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> start_dac_i,
			s_o	=> start_dac_s
		);

	cmp_end_dac_rd : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> end_dac_rd_i,
			s_o	=> end_dac_rd_s
		);

	cmp_end_dac_wr : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> end_dac_wr_i,
			s_o	=> end_dac_wr_s
		);

end Behavioral;

