--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

library work;
use work.fmc_adc2m18b2ch_pkg.all;

package calib_pkg is

	component calib
		generic(g_simulation : integer := 0);
		port(
			clk_i 	: in std_logic;
			reset_i	: in std_logic;

			force_cal_o       : out std_logic;
			force_cal_i       : in std_logic;
			force_cal_load_o  : out std_logic;

			time_calib_reg_i	: in std_logic_vector(31 downto 0);	--1 sec resolution
			t_next_calib_o		: out std_logic_vector(31 downto 0);--1 sec resolution 
--			C0_i					: in std_logic;
			zero_cycle_i		: in std_logic;
			reg_calib_delay_i	: in std_logic_vector(31 downto 0);
			type_fine_offset_i: in std_logic;
						
			ADC_pos_ref_i		: in std_logic_vector(19 downto 0);
			nb_of_meas_reg_i	: in std_logic_vector(31 downto 0);

			open1_o	: out std_logic;
			gain1_o	: out std_logic;
			off1_o	: out std_logic;
			
			open2_o	: out std_logic;
			gain2_o	: out std_logic;
			off2_o	: out std_logic;

			DAC_wr_o 	: out std_logic_vector(23 downto 0);	-- To DAC registers write interface
			DAC_rd_i 	: in std_logic_vector(23 downto 0);		-- From DAC registers read interface
			end_dac_rd_i	: in std_logic;
			end_dac_wr_i	: in std_logic;
				
			adc_data_rdy_F_i	: in std_logic;
			adc_data_F_i		: in std_logic_vector(17 downto 0);
			adc_data_rdy_D_i	: in std_logic;
			adc_data_D_i		: in std_logic_vector(17 downto 0);

			val_pos_F_o		: out std_logic_vector(31 downto 0);
			val_neg_F_o		: out std_logic_vector(31 downto 0);
			val_pos_D_o		: out std_logic_vector(31 downto 0);
			val_neg_D_o		: out std_logic_vector(31 downto 0);

			gain_F_o				: out std_logic_vector(31 downto 0);
			offset_calc_F_o	: out std_logic_vector(31 downto 0);
			offset_meas_F_o	: out std_logic_vector(31 downto 0);
			gain_D_o				: out std_logic_vector(31 downto 0);
			offset_calc_D_o	: out std_logic_vector(31 downto 0);
			offset_meas_D_o	: out std_logic_vector(31 downto 0);
			
			dpot_done_i   : in std_logic;
			dpot_reset_o  : out std_logic;
			dpot_wr_o     : out std_logic;
			fine_offset_o : out std_logic_vector(9 downto 0);

			B_drdy_i	: in std_logic;
			B_i		: in std_logic_vector(31 downto 0);
			corr_fact_i : in std_logic_vector(31 downto 0);
			k_i         : in std_logic_vector(31 downto 0);
			step_dpot_i : in std_logic_vector(31 downto 0);

			error_status_o	: out std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
			calib_status_o	: out std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0)
		);
	end component;

	component calib_cmd_dac 
		 Port ( 
			clk_i 	: in std_logic;
			reset_i	: in std_logic;

			start_dac_i 			: in std_logic;
			DAC_cmd_i 				: in DAC_cmd;								--Command Bus for the DAC
			DAC_val_register_i	: in std_logic_vector(19 downto 0);	--Input DAC register

			DAC_wr_o 			: out std_logic_vector(23 downto 0);	-- To DAC registers write interface
			DAC_rd_i 			: in std_logic_vector(23 downto 0);		-- From DAC registers read interface

			end_dac_rd_i	: in std_logic;
			end_dac_wr_i	: in std_logic;

			end_dac_o		: out std_logic;
			error_status_o	: out std_logic
		);
	end component;

	component calibration 
		port(
			clk_i 	: in std_logic;
			reset_i	: in std_logic;
			
			start_cal_i	: in std_logic;
			
			start_switch_o	: out std_logic;
			end_switch_i	: in std_logic;
			switch_cmd_o	: out calib_cmd;
			
			start_dac_o				: out std_logic;
			DAC_val_register_o	: out std_logic_vector (19 downto 0);
			DAC_cmd_o 				: out DAC_cmd;									
			end_dac_i				: in std_logic;
		
			start_gain_offset_calc_o	: out std_logic;
			end_gain_offset_calc_i		: in std_logic;
				
			gain_calc_F_i		: in std_logic_vector(31 downto 0);
			offset_calc_F_i	: in std_logic_vector(31 downto 0);
			gain_calc_D_i		: in std_logic_vector(31 downto 0);
			offset_calc_D_i	: in std_logic_vector(31 downto 0);

			gain_F_o				: out std_logic_vector(31 downto 0);
			offset_calc_F_o	: out std_logic_vector(31 downto 0);
			offset_meas_F_o	: out std_logic_vector(31 downto 0);
			gain_D_o				: out std_logic_vector(31 downto 0);
			offset_calc_D_o	: out std_logic_vector(31 downto 0);
			offset_meas_D_o	: out std_logic_vector(31 downto 0);
			
			ADC_pos_ref_i	: in std_logic_vector(19 downto 0);

			start_meas_o	: out std_logic;
			end_meas_i		: in std_logic;

			val_measured_F_i	: in std_logic_vector(31 downto 0);
			val_pos_F_o			: out std_logic_vector(31 downto 0);
			val_neg_F_o			: out std_logic_vector(31 downto 0);
			val_measured_D_i	: in std_logic_vector(31 downto 0);
			val_pos_D_o			: out std_logic_vector(31 downto 0);
			val_neg_D_o			: out std_logic_vector(31 downto 0);

			start_fine_offset_o : out std_logic;
			type_fine_offset_i  : in std_logic;
			fine_offset_i       : in std_logic_vector(9 downto 0);
			end_fine_offset_i   : in std_logic;

			dpot_done_i   : in std_logic;
			dpot_reset_o  : out std_logic;
			dpot_wr_o     : out std_logic;
			fine_offset_o : out std_logic_vector(9 downto 0);

			--calibration status register
			error_dac_i		: in std_logic;
			error_calc_F_i	: in std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
			error_calc_D_i	: in std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
			error_status_o	: out std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
			calib_status_o	: out std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0)
		);
	end component;

	component ctrl_calib 
		generic(g_simulation : integer := 0);
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			time_calib_reg_i	: in std_logic_vector(31 downto 0);	--1 sec resolution
			t_next_calib_o		: out std_logic_vector(31 downto 0);--1 sec resolution 
			
			force_cal_o       : out std_logic;
			force_cal_i       : in std_logic;
			force_cal_load_o  : out std_logic;

--			C0_i					: in std_logic;
			zero_cycle_i		: in std_logic;
			reg_calib_delay_i	: in std_logic_vector(31 downto 0);
			
			calib_status_i	: in std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);
			calib_status_o	: out std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);
			start_calib_o	: out std_logic
		);
	end component;
	
	component gain_offset_calc 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			start_gain_offset_calc_i: in std_logic;
			
			ADC_pos_ref_i	: in std_logic_vector(19 downto 0);
			val_pos_i		: in std_logic_vector(31 downto 0);
			val_neg_i		: in std_logic_vector(31 downto 0);
			
			gain_o	: out std_logic_vector(31 downto 0);
			offset_o	: out std_logic_vector(31 downto 0);
			
			error_calc_o				: out std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
			end_gain_offset_calc_o	: out std_logic
		);
	end component;	

	component mean_measurement 
		port(
			clk_i 	: in std_logic;
			reset_i	: in std_logic;
			
			start_meas_i 	: in std_logic;
			adc_data_rdy_i	: in std_logic;
			adc_data_i		: in std_logic_vector(17 downto 0);
			
			nb_of_meas_reg_i	: in std_logic_vector(31 downto 0);

			end_meas_o		: out std_logic;
			val_mean_meas_o: out std_logic_vector(31 downto 0)
		);
	end component;

	component offset_calc 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;

			start_offset_calc_i	: in std_logic;
			
			val_pos_i		: in std_logic_vector(31 downto 0);
			val_neg_i		: in std_logic_vector(31 downto 0);
			gain_i			: in std_logic_vector(31 downto 0);
			
			offset_output_o	: out std_logic_vector(31 downto 0);
			end_offset_calc_o	: out std_logic
		);
	end component;

	component switch_ctrl 
		generic(g_simulation : integer := 0);
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			switch_cmd_i	: in calib_cmd;
			start_switch_i	: in std_logic;
			
			open1_o	: out std_logic;
			gain1_o	: out std_logic;
			off1_o	: out std_logic;
			
			open2_o	: out std_logic;
			gain2_o	: out std_logic;
			off2_o	: out std_logic;
			
			end_switch_o	: out std_logic
		);
	end component;

	component fine_offset_calculation 
		port(
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			start_i     : in std_logic;
			
			nb_sample_i : in std_logic_vector(31 downto 0);
			corr_fact_i	: in std_logic_vector(31 downto 0);
			k_i         : in std_logic_vector(31 downto 0);
			step_dpot_i : in std_logic_vector(31 downto 0);
			
			B_drdy_i : in std_logic;
			B_i      : in std_logic_vector(31 downto 0);
			
			fine_offset_o : out std_logic_vector(9 downto 0);
			end_o : out std_logic
		);
	end component;

end package calib_pkg;

