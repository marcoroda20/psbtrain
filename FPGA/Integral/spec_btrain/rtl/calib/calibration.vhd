----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    10:52:23 05/16/2013 
-- Design Name: 
-- Module Name:    calibration - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity calibration is
	port(
		clk_i 	: in std_logic;
		reset_i	: in std_logic;
		
		start_cal_i	: in std_logic;
		
		start_switch_o	: out std_logic;
		end_switch_i	: in std_logic;
		switch_cmd_o	: out calib_cmd;
		
		start_dac_o				: out std_logic;
		DAC_val_register_o	: out std_logic_vector (19 downto 0);
		DAC_cmd_o 				: out DAC_cmd;									
		end_dac_i				: in std_logic;
	
		start_gain_offset_calc_o	: out std_logic;
		end_gain_offset_calc_i		: in std_logic;
			
		gain_calc_F_i		: in std_logic_vector(31 downto 0);
		offset_calc_F_i	: in std_logic_vector(31 downto 0);
		gain_calc_D_i		: in std_logic_vector(31 downto 0);
		offset_calc_D_i	: in std_logic_vector(31 downto 0);

		gain_F_o				: out std_logic_vector(31 downto 0);
		offset_calc_F_o	: out std_logic_vector(31 downto 0);
		offset_meas_F_o	: out std_logic_vector(31 downto 0);
		gain_D_o				: out std_logic_vector(31 downto 0);
		offset_calc_D_o	: out std_logic_vector(31 downto 0);
		offset_meas_D_o	: out std_logic_vector(31 downto 0);
		
		ADC_pos_ref_i	: in std_logic_vector(19 downto 0);

		start_meas_o	: out std_logic;
		end_meas_i		: in std_logic;

		val_measured_F_i	: in std_logic_vector(31 downto 0);
		val_pos_F_o			: out std_logic_vector(31 downto 0);
		val_neg_F_o			: out std_logic_vector(31 downto 0);
		val_measured_D_i	: in std_logic_vector(31 downto 0);
		val_pos_D_o			: out std_logic_vector(31 downto 0);
		val_neg_D_o			: out std_logic_vector(31 downto 0);

		type_fine_offset_i  : in std_logic;
		start_fine_offset_o : out std_logic;
		fine_offset_i       : in std_logic_vector(9 downto 0);
		end_fine_offset_i   : in std_logic;

		dpot_done_i   : in std_logic;
		dpot_reset_o  : out std_logic;
		dpot_wr_o     : out std_logic;
		fine_offset_o : out std_logic_vector(9 downto 0);

		--calibration status register
		error_dac_i		: in std_logic;
		error_calc_F_i	: in std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
		error_calc_D_i	: in std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
		error_status_o	: out std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
		calib_status_o	: out std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0)
	);
end calibration;

architecture Behavioral of calibration is

	constant MAX_WAIT_STAB_B_CNT : integer := 50000;

	--Type
	type calibration_states is 
	(	IDLE,
		START_CAL,
		WAIT_STAB_B,
		DPOT_RESET,DPOT_MIDLLE_RANGE,FINE_OFFSET_MEAS,DPOT_WRITE,
		SWITCH_AS_INPUT_CMD,
		SWITCH_AS_GAIN_CMD,SWITCH_AS_OFFSET_CMD,
		SWITCH_AS_DPOT_OFFSET_CMD,
		CONFIG_DAC,END_CONFIG_DAC,
		DAC_POS_WR,DAC_NEG_WR,
		MEAS_POS,MEAS_NEG,MEAS_OFFSET,
		LOAD_VAL_POS,LOAD_VAL_NEG,LOAD_VAL_OFFSET,
		GAIN_OFFSET_CALC,
		LOAD_GAIN_OFFSET_REG
	);
		
	--Signal
	signal state_s			: calibration_states;
	
	signal start_cal_s	: std_logic;
	
	signal end_switch_s				: std_logic;
	signal end_dac_s					: std_logic;
	signal end_meas_s					: std_logic;
	signal end_gain_offset_calc_s	: std_logic;

	signal dac_pvref_s : std_logic_vector(19 downto 0);
	signal dac_nvref_s : std_logic_vector(19 downto 0);

	signal offset_meas_F_s	: std_logic_vector(31 downto 0);
	signal offset_meas_D_s	: std_logic_vector(31 downto 0);
	
	signal zero_cycle_s		: std_logic;	
	signal zero_cycle_n_s	: std_logic;	
	
	signal dpot_done_s : std_logic;
	signal end_fine_s  : std_logic;
	
	signal wait_counter_s : integer;

begin

	--State Machine
	process (clk_i) 
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				val_pos_F_o <= (others=>'0');
				val_neg_F_o <= (others=>'0');
				val_pos_D_o <= (others=>'0');
				val_neg_D_o <= (others=>'0');
				dac_pvref_s <= (others=>'0');
				dac_nvref_s <= (others=>'0');
				gain_F_o <= x"80000000"; -- Set F-gcc to 1.000000 at initialization
				gain_D_o <= x"80000000"; -- Set D-gcc to 1.000000 at initialization
				offset_calc_F_o <= (others=>'0');
				offset_meas_F_o <= (others=>'0');
				offset_calc_D_o <= (others=>'0');
				offset_meas_D_o <= (others=>'0');
				gain_F_o <= x"80000000"; -- Set F-gcc to 1.000000 at initialization
				offset_calc_F_o <= (others=>'0');
				offset_meas_F_o <= (others=>'0');
				offset_meas_F_s <= (others=>'0');
				gain_D_o <= x"80000000"; -- Set D-gcc to 1.000000 at initialization
				offset_calc_D_o <= (others=>'0');
				offset_meas_D_o <= (others=>'0');
				offset_meas_D_s <= (others=>'0');
				dpot_reset_o    <= '0';
				dpot_wr_o       <= '0';
				wait_counter_s <= 0;
				fine_offset_o   <= (others=>'0');
				start_fine_offset_o <= '0';
				error_status_o <= NO_ERROR_st;
				state_s <= IDLE;
			else
				start_fine_offset_o <= '0';
				dpot_reset_o        <= '0';
				dpot_wr_o           <= '0';
				start_switch_o      <= '0';
				
				--Value of positiv and negativ
				dac_pvref_s <= ADC_pos_ref_i;
				dac_nvref_s <= ( not ADC_pos_ref_i) + 1;
				
				--Value of offset measured
				offset_meas_F_o <= offset_meas_F_s;
				offset_meas_D_o <= offset_meas_D_s;
				case state_s is
					when IDLE =>
						if (start_cal_s='1') then
							state_s <= START_CAL;
						end if;
						start_switch_o 				<= '0';
						switch_cmd_o 					<= SWITCH_INPUT;
						dpot_reset_o               <= '0';
						dpot_wr_o                  <= '0';
						start_dac_o						<= '0';
						start_meas_o 					<= '0';
						start_gain_offset_calc_o	<= '0';
						wait_counter_s             <= 0;
						calib_status_o					<= IDLE_st;
						
					when START_CAL =>
						state_s <= DPOT_RESET;
						start_switch_o 	<= '0';
						calib_status_o		<= DPOT_st;

					when DPOT_RESET =>
						if (dpot_done_s='1') then
							state_s <= DPOT_MIDLLE_RANGE;
						end if;
						start_switch_o 	<= '0';
						dpot_reset_o      <= '1';
						dpot_wr_o         <= '0';
						fine_offset_o     <= "10" & x"00";
						calib_status_o		<= DPOT_st;
						
					when DPOT_MIDLLE_RANGE =>
						if (dpot_done_s='1') then
							state_s <= SWITCH_AS_OFFSET_CMD;
						end if;
						start_switch_o 	<= '0';
						dpot_reset_o      <= '0';
						dpot_wr_o         <= '1';
						calib_status_o		<= DPOT_st;
						
					when SWITCH_AS_GAIN_CMD =>
						if (end_switch_s='1') then
							state_s <= DAC_POS_WR;
						end if;
						start_switch_o 				<= '1';
						switch_cmd_o 					<= SWITCH_GAIN;
						calib_status_o					<= SWITCH_AS_GAIN_st;
						
					when CONFIG_DAC =>
						if (end_dac_s='1') then
							state_s <= END_CONFIG_DAC;
						end if;
						start_dac_o						<= '1';
						DAC_val_register_o         <= c_CFG_DAC5791(19 downto 0);
						DAC_cmd_o 						<= DAC_CMD_CFG;
						calib_status_o					<= CONFIG_DAC_st;
						
					when END_CONFIG_DAC =>
						state_s <= DAC_POS_WR;
						start_dac_o						<= '0';
						
					when DAC_POS_WR =>
						if (end_dac_s='1') then
							state_s <= MEAS_POS;
						end if;
						start_dac_o						<= '1';
						start_switch_o 				<= '0';
						DAC_val_register_o			<= dac_pvref_s;
						DAC_cmd_o 						<= DAC_CMD_POS;
						calib_status_o					<= MEAS_POS_st;
						
					when MEAS_POS =>
						if (end_meas_s='1') then
							state_s <= LOAD_VAL_POS;
						end if;
						start_dac_o						<= '0';
						start_meas_o 					<= '1';
						
					when LOAD_VAL_POS =>
						state_s <= DAC_NEG_WR;
						start_meas_o 					<= '0';
						val_pos_F_o                <= val_measured_F_i - offset_meas_F_s;
						val_pos_D_o                <= val_measured_D_i - offset_meas_D_s;
						
					when DAC_NEG_WR =>
						if (end_dac_s='1') then
							state_s <= MEAS_NEG;
						end if;
						start_dac_o						<= '1';
						DAC_val_register_o			<= dac_nvref_s;
						DAC_cmd_o 						<= DAC_CMD_NEG;
						calib_status_o					<= MEAS_NEG_st;
						
					when MEAS_NEG =>
						if (end_meas_s='1') then
							state_s <= LOAD_VAL_NEG;
						end if;
						start_dac_o						<= '0';
						start_meas_o 					<= '1';
						
					when LOAD_VAL_NEG =>
						state_s <= GAIN_OFFSET_CALC;
						start_meas_o 					<= '0';
						val_neg_F_o                <= val_measured_F_i - offset_meas_F_s;
						val_neg_D_o                <= val_measured_D_i - offset_meas_D_s;
						
					when GAIN_OFFSET_CALC =>
						if (end_gain_offset_calc_s='1') then
							state_s <= LOAD_GAIN_OFFSET_REG;
						end if;
						start_gain_offset_calc_o	<= '1';
						calib_status_o					<= CALC_st;
						
					when LOAD_GAIN_OFFSET_REG =>
						state_s <= SWITCH_AS_DPOT_OFFSET_CMD;
						start_gain_offset_calc_o	<= '0';
						start_switch_o 	         <= '0';
						gain_F_o                   <= gain_calc_F_i;
						gain_D_o                   <= gain_calc_D_i;
						offset_calc_F_o            <= offset_calc_F_i;
						offset_calc_D_o            <= offset_calc_D_i;
						
					when SWITCH_AS_OFFSET_CMD =>
						if (end_switch_s='1') then
							state_s <= MEAS_OFFSET;
						end if;
						if (type_fine_offset_i='1') then
							switch_cmd_o 		<= SWITCH_INPUT;
						else
							switch_cmd_o 		<= SWITCH_OFFSET;
						end if;
						start_switch_o 	<= '1';
						dpot_reset_o      <= '0';
						dpot_wr_o         <= '0';
						calib_status_o		<= SWITCH_AS_OFFSET_st;
						
					when MEAS_OFFSET =>
						if (end_meas_s='1') then
							state_s <= LOAD_VAL_OFFSET;
						end if;
						start_switch_o 				<= '0';
						start_meas_o 					<= '1';
						calib_status_o					<= OFFSET_st;
						
					when LOAD_VAL_OFFSET =>
						state_s <= SWITCH_AS_GAIN_CMD;
						start_switch_o 				<= '0';
						start_meas_o 					<= '0';
						offset_meas_F_s            <= val_measured_F_i;
						offset_meas_D_s            <= val_measured_D_i;
						
					when SWITCH_AS_INPUT_CMD =>
						if (end_switch_s='1') then
								state_s <= IDLE;
						end if;
						start_switch_o 				<= '1';
						switch_cmd_o 					<= SWITCH_INPUT;
						dpot_reset_o               <= '0';
						calib_status_o					<= SWITCH_AS_INPUT_st;

					when SWITCH_AS_DPOT_OFFSET_CMD =>
						if (end_switch_s='1') then
							state_s <= WAIT_STAB_B;
						end if;
						if (type_fine_offset_i='1') then
							switch_cmd_o 		<= SWITCH_INPUT;
						else
							switch_cmd_o 		<= SWITCH_OFFSET;
						end if;
						start_switch_o 				<= '1';
						dpot_reset_o               <= '0';
						calib_status_o					<= SWITCH_AS_OFFSET_st;

					when WAIT_STAB_B =>
						if (wait_counter_s>=MAX_WAIT_STAB_B_CNT) then
							state_s <= FINE_OFFSET_MEAS;
						end if;
						wait_counter_s <= wait_counter_s+1;

					when FINE_OFFSET_MEAS =>
						if (end_fine_s='1') then
							state_s <= DPOT_WRITE;
							fine_offset_o   <= fine_offset_i;
						end if;
						start_fine_offset_o <= '1';
						calib_status_o      <= DPOT_st;
						
					when DPOT_WRITE =>
						if (dpot_done_s='1') then
							if (type_fine_offset_i='1') then
								state_s <= IDLE;
							else
								state_s <= SWITCH_AS_INPUT_CMD;
							end if;
						end if;
						dpot_wr_o       <= '1';
						calib_status_o  <= DPOT_st;
					
				end case;

				--Error
				if (error_dac_i='1') then
					error_status_o <= ERROR_DAC_st;
				end if;
				if (error_calc_F_i/=ERROR_OFFSET_st or error_calc_D_i/=ERROR_OFFSET_st) then
					error_status_o <= ERROR_OFFSET_st;
				end if;
				if (error_calc_F_i/=ERROR_GAIN_st or error_calc_D_i/=ERROR_GAIN_st) then
					error_status_o <= ERROR_GAIN_st;
				end if;
			end if;
		end if;
	end process;
		
	--Input Rising detect
	cmp_start_cal : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> start_cal_i,
		s_o	=> start_cal_s
	);

	cmp_end_switch : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> end_switch_i,
		s_o	=> end_switch_s
	);
	
	cmp_end_cfg_dac : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> end_dac_i,
		s_o	=> end_dac_s
	);
	
	cmp_end_meas : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> end_meas_i,
		s_o	=> end_meas_s
	);
	
	cmp_end_gain : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> end_gain_offset_calc_i,
		s_o	=> end_gain_offset_calc_s
	);
		
	cmp_dpot_done : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> dpot_done_i,
		s_o	=> dpot_done_s
	);
		
	cmp_end_fine : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> end_fine_offset_i,
		s_o	=> end_fine_s
	);
		
end Behavioral;

