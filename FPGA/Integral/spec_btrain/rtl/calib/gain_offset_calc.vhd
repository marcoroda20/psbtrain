----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    16:33:52 05/22/2013 
-- Design Name: 
-- Module Name:    gain_offset_calc - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;
use work.calib_pkg.all;

entity gain_offset_calc is
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		start_gain_offset_calc_i: in std_logic;
		
		ADC_pos_ref_i	: in std_logic_vector(19 downto 0);
		val_pos_i		: in std_logic_vector(31 downto 0);
		val_neg_i		: in std_logic_vector(31 downto 0);
		
		gain_o	: out std_logic_vector(31 downto 0);
		offset_o	: out std_logic_vector(31 downto 0);
		
		error_calc_o				: out std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
		end_gain_offset_calc_o	: out std_logic
	);
end gain_offset_calc;

architecture Behavioral of gain_offset_calc is

	--Constant
	constant NB_BIT_DIVIDER : Integer := 32;

	--Type
	type gain_offset_calc_state is
	(
		IDLE,
		GAIN_CALCULATION,OFFSET_CALCULATION,
		LOAD_OUTPUT
	);
	
	--Signal
	signal state_s			: gain_offset_calc_state;

	signal start_gain_offset_calc_s	: std_logic;
	
	signal dividend_s	: std_logic_vector(NB_BIT_DIVIDER downto 0);
	signal divisor_s	: std_logic_vector(NB_BIT_DIVIDER downto 0);
	signal quotient_output_s		: std_logic_vector(NB_BIT_DIVIDER downto 0);
	signal fraction_output_s		: std_logic_vector(NB_BIT_DIVIDER downto 0);
	signal divider_output_s			: std_logic_vector(NB_BIT_DIVIDER-1 downto 0);
	signal start_gain_divider_s	: std_logic;
	signal end_divider_s   			: std_logic;
	signal end_gain_calc_s			: std_logic;
	
	signal start_offset_calc_s	: std_logic;
	signal end_offset_s			: std_logic;
	signal end_offset_calc_s	: std_logic;
	signal offset_output_s		: std_logic_vector(31 downto 0);	
		
begin

	--TODO Error detection for Offset and Gain
	error_calc_o <= (others=>'0');

	--State machine
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				start_gain_divider_s <= '0';
				start_offset_calc_s <= '0';
				dividend_s <= (others=>'0');
				divisor_s <= (others=>'0');
				gain_o <= x"80000000"; -- Set gcc to 1.000000 at initialization
				offset_o <= (others=>'0');
				end_gain_offset_calc_o <= '0';
				state_s <= IDLE;
			else
				if (fraction_output_s(1)='0') then
					divider_output_s <= quotient_output_s(0) & fraction_output_s(NB_BIT_DIVIDER downto 2);				
				else
					divider_output_s <= quotient_output_s(0) & fraction_output_s(NB_BIT_DIVIDER downto 2) + 1;				
				end if;
				case state_s is
					when IDLE => 
						if (start_gain_offset_calc_s='1') then
							state_s <= GAIN_CALCULATION;
						end if;
						start_gain_divider_s		<= '0';
						start_offset_calc_s		<= '0';
						end_gain_offset_calc_o	<= '1';
					when GAIN_CALCULATION =>
						if (end_gain_calc_s='1') then
--							state_s <= OFFSET_CALCULATION;	
							state_s <= LOAD_OUTPUT;	
						end if;
						start_gain_divider_s		<= '1';
						start_offset_calc_s		<= '0';
						dividend_s					<= '0' & ADC_pos_ref_i(18 downto 0) & x"000" & '0'; -- uint32(2*ADC_ref)
						divisor_s					<= (val_pos_i(val_pos_i'length-1) & val_pos_i) -
															 (val_neg_i(val_neg_i'length-1) & val_neg_i); -- uint32
						end_gain_offset_calc_o	<= '0';
					when OFFSET_CALCULATION => 
						if (end_offset_calc_s='1') then
							state_s <= LOAD_OUTPUT;	
						end if;
						start_gain_divider_s		<= '0';
						start_offset_calc_s		<= '1';
						end_gain_offset_calc_o	<= '0';
					when LOAD_OUTPUT =>	
						state_s <= IDLE;	
						start_gain_divider_s		<= '0';
						start_offset_calc_s		<= '0';
						gain_o						<= divider_output_s;
--						offset_o						<= offset_output_s;
						offset_o						<= (others=>'0');
						end_gain_offset_calc_o	<= '0';
				end case;
			end if;
		end if;
	end process;

	--Divider for gain calculation
	cmp_gain_divider : divider
		generic map (NB_BIT => NB_BIT_DIVIDER+1)
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			a_i	=> dividend_s,
			b_i	=> divisor_s,

			start_divider_i	=> start_gain_divider_s,
			cmd_divider_i		=> '1',
			
			q_o	=> quotient_output_s,
			r_o	=> open,
			f_o	=> fraction_output_s,
			
			divide_by_0_o	=> open,
			end_divider_o	=> end_divider_s
		);
	

	cmp_offet_calc : offset_calc
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			start_offset_calc_i	=> start_offset_calc_s,

			val_pos_i		=> val_pos_i,
			val_neg_i		=> val_neg_i,
			gain_i			=> divider_output_s,

			offset_output_o	=> offset_output_s,
			end_offset_calc_o	=> end_offset_s
		);

	--Input rising edge
	cmp_start : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> start_gain_offset_calc_i,
		s_o	=> start_gain_offset_calc_s
	);

	cmp_end_gain_calc : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> end_divider_s,
		s_o	=> end_gain_calc_s
	);

	cmp_end_offset_calc : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> end_offset_s,
		s_o	=> end_offset_calc_s
	);

end Behavioral;

