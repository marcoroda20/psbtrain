----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    09:14:48 05/21/2013 
-- Design Name: 
-- Module Name:    mean_measurement - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity mean_measurement is
	port(
		clk_i 	: in std_logic;
		reset_i	: in std_logic;
		
		start_meas_i 	: in std_logic;
		adc_data_rdy_i	: in std_logic;
		adc_data_i		: in std_logic_vector(17 downto 0);
		
		nb_of_meas_reg_i	: in std_logic_vector(31 downto 0);

		end_meas_o		: out std_logic;
		val_mean_meas_o: out std_logic_vector(31 downto 0)
	);
end mean_measurement;

architecture Behavioral of mean_measurement is
	
	--Constant
	constant NB_BIT_DIVIDER : Integer := 48;

	--Type
	type mean_meas_state is
	(
		IDLE,
		STABILIZATION,
		WAIT_MEAS,ACC_MEAS,
		MEAN_CALC,
		LOAD_OUTPUT
	);

	--Signal
	signal state_s 		: mean_meas_state;
	signal next_state_s	: mean_meas_state;

	signal start_meas_s		: std_logic;
	signal adc_data_rdy_s	: std_logic;
	signal nb_of_meas_cnt_s	: integer;
	signal acc_data_s			: unsigned(47 downto 0);
	signal acc_data_input_s	: unsigned(47 downto 0);
	signal start_mean_calc_s: std_logic;
	
	signal divisor_s					: std_logic_vector(47 downto 0);
	signal divider_output_s			: std_logic_vector(47 downto 0);
	signal divider_output_frac_s	: std_logic_vector(47 downto 0);
	signal divider_result_s			: std_logic_vector(31 downto 0);
	
	signal end_divider_s		: std_logic;
	signal end_mean_calc_s	: std_logic;
	signal val_mean_meas_s	: std_logic_vector(31 downto 0);
	
begin

	--For the accumalator
	acc_data_input_s <=  x"000000" & unsigned(adc_data_i) & "000000" when adc_data_i(17)='0' 
							else x"ffffff" & unsigned(adc_data_i) & "000000" ;
--	acc_data_input_s <=  x"000000" & unsigned(adc_data_i) & "000000";
	
	--State machine
	process (state_s,start_meas_s,adc_data_rdy_s,end_mean_calc_s,
					val_mean_meas_s,nb_of_meas_cnt_s,nb_of_meas_reg_i)
	begin
		case state_s is
			when IDLE => 
				if (start_meas_s='1') then
					next_state_s <= STABILIZATION;
--					next_state_s <= WAIT_MEAS;
				else
					next_state_s <= IDLE;
				end if;
			when STABILIZATION =>
				if (adc_data_rdy_s='1') then
					next_state_s <= WAIT_MEAS;
				else
					next_state_s <= STABILIZATION;
				end if;
			when WAIT_MEAS =>
				if (adc_data_rdy_s='1') then
					next_state_s <= ACC_MEAS;	
				else
					next_state_s <= WAIT_MEAS;	
				end if;
			when ACC_MEAS => 
				if (nb_of_meas_cnt_s<to_integer(unsigned(nb_of_meas_reg_i)-1)) then
					next_state_s <= WAIT_MEAS;	
				else
					next_state_s <= MEAN_CALC;	
				end if;
			when MEAN_CALC => 
				if (end_mean_calc_s='1') then
					next_state_s <= LOAD_OUTPUT;	
				else
					next_state_s <= MEAN_CALC;	
				end if;
			when LOAD_OUTPUT =>	
				next_state_s <= IDLE;	
		end case;
	end process;
	
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				state_s <= IDLE;
			else
				state_s <= next_state_s;
			end if;
		end if;
	end process;

	--Signal of state machine
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				val_mean_meas_o	<= (others=>'0');
				nb_of_meas_cnt_s	<= 0;
				end_meas_o			<= '0';
				val_mean_meas_s	<= (others=>'0');
				acc_data_s			<= (others=>'0');
				start_mean_calc_s	<= '0';
			else
	--			if (val_mean_meas_s(23)='0') then
					val_mean_meas_o	<= val_mean_meas_s;
	--			else
	--				val_mean_meas_o	<= x"ff" & val_mean_meas_s(23 downto 0);
	--			end if;
				case state_s is
					when IDLE => 
						nb_of_meas_cnt_s	<= 0;
						end_meas_o			<= '1';
						acc_data_s			<= (others=>'0');
						start_mean_calc_s	<= '0';
					when STABILIZATION =>
						end_meas_o			<= '0';
						start_mean_calc_s	<= '0';
					when WAIT_MEAS =>
						end_meas_o			<= '0';
						start_mean_calc_s	<= '0';
					when ACC_MEAS => 
						nb_of_meas_cnt_s	<= nb_of_meas_cnt_s + 1;
						end_meas_o			<= '0';
						acc_data_s			<= acc_data_s + acc_data_input_s;
						start_mean_calc_s	<= '0';
					when MEAN_CALC => 
						end_meas_o			<= '0';
						start_mean_calc_s	<= '1';
					when LOAD_OUTPUT =>	
						end_meas_o			<= '0';
	--					val_mean_meas_s	<= divider_output_s(31 downto 0);
						val_mean_meas_s	<= divider_result_s;
						start_mean_calc_s	<= '0';
				end case;
			end if;
		end if;
	end process;

	--Divider for mean calculation
	divisor_s <= x"0000" & nb_of_meas_reg_i;
	
	cmp_mean_divider : divider 
		generic map (NB_BIT=>NB_BIT_DIVIDER)
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			a_i	=> std_logic_vector(acc_data_s),
			b_i	=> divisor_s,

			start_divider_i	=> start_mean_calc_s,
			cmd_divider_i		=> '1',
			
			q_o	=> divider_output_s,
			r_o	=> open,
			f_o	=> divider_output_frac_s,

			divide_by_0_o	=> open,
			end_divider_o	=> end_divider_s
		);
	divider_result_s <= divider_output_s(23 downto 0) & divider_output_frac_s(47 downto 40);


	--Input rising edge
	cmp_start_mean_meas : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> start_meas_i,
			s_o	=> start_meas_s
		);
		
	cmp_adc_data_rdy : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> adc_data_rdy_i,
			s_o	=> adc_data_rdy_s
		);
	
	cmp_end_mean_calc : RisingDetect
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> end_divider_s,
			s_o	=> end_mean_calc_s
		);
			
end Behavioral;

