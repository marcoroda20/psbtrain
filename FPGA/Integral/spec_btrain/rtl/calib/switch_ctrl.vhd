----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    14:09:52 05/16/2013 
-- Design Name: 
-- Module Name:    switch_ctrl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.utils_pkg.all;

entity switch_ctrl is
	generic(g_simulation : integer := 0);
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		switch_cmd_i	: in calib_cmd;
		start_switch_i	: in std_logic;
		
		open1_o	: out std_logic;
		gain1_o	: out std_logic;
		off1_o	: out std_logic;
		
		open2_o	: out std_logic;
		gain2_o	: out std_logic;
		off2_o	: out std_logic;
		
		end_switch_o	: out std_logic
	);
end switch_ctrl;

architecture Behavioral of switch_ctrl is

	constant MAX_WAIT_COUNT : integer := 100000;--20000;
	constant MAX_WAIT_COUNT_SIM : integer := 10;

	--Type
	type switch_states is
	(
		IDLE,
		LOAD_CALIB_CMD,
		SWITCH_OPEN,SWITCH_CLOSE,
		WAIT_SWITCH_OPEN,WAIT_SWITCH_CLOSE
	);
	
	--Signal
	signal state_s			: switch_states;
	
	signal start_switch_s : std_logic;
	signal switch_cmd_s	 : calib_cmd;
	signal wait_counter_s : integer;
	
begin

	--State Machine
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				state_s <= IDLE;
				wait_counter_s <= 0;
				switch_cmd_s <= SWITCH_OFFSET;
				open1_o <= SWITCH_CLOSE_CMD;
				gain1_o <= SWITCH_OPEN_CMD;
				off1_o  <= SWITCH_OPEN_CMD;
				open2_o <= SWITCH_CLOSE_CMD;
				gain2_o <= SWITCH_OPEN_CMD;
				off2_o  <= SWITCH_OPEN_CMD;
			else
				case state_s is
					when IDLE =>
						if (start_switch_s='1') then
							state_s <= LOAD_CALIB_CMD;
						end if;
						end_switch_o <= '1';
					when LOAD_CALIB_CMD =>
						state_s <= SWITCH_OPEN;
						end_switch_o <= '0';
						switch_cmd_s <= switch_cmd_i;
					when SWITCH_OPEN =>
						state_s <= WAIT_SWITCH_OPEN;
						end_switch_o <= '0';
						open1_o <= SWITCH_OPEN_CMD;
						gain1_o <= SWITCH_OPEN_CMD;
						off1_o  <= SWITCH_OPEN_CMD;
						open2_o <= SWITCH_OPEN_CMD;
						gain2_o <= SWITCH_OPEN_CMD;
						off2_o  <= SWITCH_OPEN_CMD;
					when WAIT_SWITCH_OPEN =>
						if ((wait_counter_s>=MAX_WAIT_COUNT and g_simulation=0) or
							 (wait_counter_s>=MAX_WAIT_COUNT_SIM and g_simulation=1)) then
							state_s <= SWITCH_CLOSE;
						end if;
						end_switch_o <= '0';
					when SWITCH_CLOSE =>
						state_s <= WAIT_SWITCH_CLOSE;
						end_switch_o <= '0';
						case switch_cmd_s is
							when SWITCH_INPUT =>
								open1_o <= SWITCH_CLOSE_CMD;
								gain1_o <= SWITCH_OPEN_CMD;
								off1_o  <= SWITCH_OPEN_CMD;
								open2_o <= SWITCH_CLOSE_CMD;
								gain2_o <= SWITCH_OPEN_CMD;
								off2_o  <= SWITCH_OPEN_CMD;
							when SWITCH_GAIN =>
								open1_o <= SWITCH_OPEN_CMD;
								gain1_o <= SWITCH_CLOSE_CMD;
								off1_o  <= SWITCH_OPEN_CMD;
								open2_o <= SWITCH_OPEN_CMD;
								gain2_o <= SWITCH_CLOSE_CMD;
								off2_o  <= SWITCH_OPEN_CMD;
							when SWITCH_OFFSET =>
								open1_o <= SWITCH_OPEN_CMD;
								gain1_o <= SWITCH_OPEN_CMD;
								off1_o  <= SWITCH_CLOSE_CMD;
								open2_o <= SWITCH_OPEN_CMD;
								gain2_o <= SWITCH_OPEN_CMD;
								off2_o  <= SWITCH_CLOSE_CMD;
						end case;
					when WAIT_SWITCH_CLOSE =>
						if ((wait_counter_s>=MAX_WAIT_COUNT and g_simulation=0) or
							 (wait_counter_s>=MAX_WAIT_COUNT_SIM and g_simulation=1)) then
							state_s <= IDLE;
						end if;
						end_switch_o <= '0';
				end case;
				--Counter
				if (state_s=WAIT_SWITCH_OPEN or state_s=WAIT_SWITCH_CLOSE) then
					wait_counter_s <= wait_counter_s+1;
				else
					wait_counter_s <= 0;
				end if;
			end if;
		end if;
	end process;
	
	--Input rising edge
	cmp_start_switch_input : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> start_switch_i,
		s_o	=> start_switch_s
	);


end Behavioral;

