----------------------------------------------------------------------------------
-- Company:        CERN TE/MSC/MM
-- Engineer:       Daniel Oberson
-- 
-- Create Date:    10:55:41 12/17/2013 
-- Design Name: 
-- Module Name:    decimator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

library work;
use work.utils_pkg.all;

entity decimator is
	generic(
      g_DATA_LENGTH  : integer := 16
	);
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;

		decim_factor_i : in std_logic_vector(31 downto 0);
		
		data_i	: in std_logic_vector(g_DATA_LENGTH-1 downto 0);
		d_rdy_i	: in std_logic;

		data_o	: out std_logic_vector(g_DATA_LENGTH-1 downto 0);
		d_rdy_o	: out std_logic
	);
end decimator;

architecture Behavioral of decimator is

	--Signal
	signal data_s	: std_logic_vector(g_DATA_LENGTH-1 downto 0);
	signal d_rdy_s	: std_logic;
	signal count_s : integer;
		
begin
	
	--Input Latch
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				data_s <= (others=>'0');
			else
				if (d_rdy_s='1') then
					data_s <= data_i;
				end if;
			end if;
		end if;
	end process;	

	--Decimator
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				d_rdy_o    <= '0';
				count_s    <= 0;
				data_o     <= (others=>'0');
			else
				--Counter of data to decimate
				if (d_rdy_s='1') then
					if (count_s<to_integer(unsigned(decim_factor_i))-1) then
						count_s <= count_s + 1;
					else
						count_s <= 0;
					end if;
				end if;
				--Output latch of data decimated
				if ((d_rdy_s='1') and (count_s=to_integer(unsigned(decim_factor_i))-1)) then
					data_o <= data_s;
					d_rdy_o <= d_rdy_s;
				else
					d_rdy_o <= '0';
				end if;
			end if;
		end if;
	end process;
	
	--Data ready input pulse
	cmp_acqu_end : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> d_rdy_i,
			s_o	=> d_rdy_s
		);

end Behavioral;

