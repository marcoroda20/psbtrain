----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
-- Company:			CERN/TE-MS-MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    18:21:32 12/04/2012 
-- Design Name: 
-- Module Name:    fmc-adc2M18b2ch - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.lcd_pkg.all;
use work.sdb_meta_pkg.all;
use work.wishbone_pkg.all;
use work.calib_pkg.all;
use work.integral_pkg.all;
use work.utils_pkg.all;
use work.adc_dac_pkg.all;
use work.dma_pkg.all;

entity fmc_adc2M18b2ch is
	generic(g_simulation : integer := 0);
	port (
		clk_i			: in std_logic;
		clk_10_i	   : in std_logic;
		clk_adc_i	: in std_logic;

		reset_n_hw_i  : in std_logic;
		reset_i		  : in std_logic;
		reset_hw_i    : in std_logic;
		reset_sw_o    : out std_logic;
		en_hw_reset_o : out std_logic;
		init_done_i   : in std_logic;

		--Input information signals
		F_low_marker_i : in std_logic;
		F_high_marker_i: in std_logic;
		D_low_marker_i : in std_logic;
		D_high_marker_i: in std_logic;
		C0_i				: in std_logic;
		zero_cycle_i	: in std_logic;

		f_low_marker_m_i : in std_logic;
		f_high_marker_m_i: in std_logic;
		d_low_marker_m_i : in std_logic;
		d_high_marker_m_i: in std_logic;
		C0_m_i           : in std_logic;
		zero_cycle_m_i   : in std_logic;
	
		--Switch
		switch_open1_o	: out std_logic;
		switch_gain1_o	: out std_logic;
		switch_off1_o	: out std_logic;
		
		switch_open2_o	: out std_logic;
		switch_gain2_o	: out std_logic;
		switch_off2_o	: out std_logic;
		
		error_status_o	: out std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
		calib_status_o	: out std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);

		--ADC
		CNV_o		: out std_logic;
		
		SDI_F_o	: out std_logic;
		SDO_F_i	: in std_logic;
		SCK_F_o	: out std_logic;
		BSCK_F_i	: in std_logic;

		SDI_D_o	: out std_logic;
		SDO_D_i	: in std_logic;
		SCK_D_o	: out std_logic;
		BSCK_D_i	: in std_logic;
		
		-- CSR wishbone interface
		wb_csr_adr_i   : in  std_logic_vector(31 downto 0);
		wb_csr_dat_i   : in  std_logic_vector(31 downto 0);
		wb_csr_dat_o   : out std_logic_vector(31 downto 0);
		wb_csr_cyc_i   : in  std_logic;
		wb_csr_sel_i   : in  std_logic_vector(3 downto 0);
		wb_csr_stb_i   : in  std_logic;
		wb_csr_we_i    : in  std_logic;
		wb_csr_ack_o   : out std_logic;
		wb_csr_stall_o : out std_logic;

		--Register	
		status_simeff_i          : in std_logic;
		reg_status_opsp_cmd_o    : out std_logic;
		reg_status_opsp_status_i : in std_logic;
		
		reg_dio_input_i : in std_logic_vector(9 downto 0);
		reg_dio_dir_i   : in std_logic_vector(9 downto 0);
		reg_dio_cfg_o   : out std_logic_vector(9 downto 0);

		wr_B_val_i        : in std_logic_vector(31 downto 0);
		wr_Bdot_val_i     : in std_logic_vector(31 downto 0);
		wr_G_val_i        : in std_logic_vector(31 downto 0); -- 23/10/2015
		wr_S_val_i        : in std_logic_vector(31 downto 0); -- 23/10/2015
		 
		wr_drdy_i         : in std_logic;
		wr_I_val_i        : in std_logic_vector(31 downto 0);		
		wr_I_drdy_i       : in std_logic; -- 23/10/2015
		
		wr_d_low_marker_i : in std_logic;
		wr_f_low_marker_i : in std_logic;
		wr_C0_i           : in std_logic;
		wr_zero_cycle_i   : in std_logic;
		wr_eff_sim_i      : in std_logic;

		Bk_o           : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		B_data_rdy_o   : out std_logic;
		Bdot_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
		Bdot_data_rdy_o: out std_logic;

		--WR-register
		wr_sample_period_o   : out std_logic_vector(25 downto 0);
		wr_ctrl_rx_latency_i : in std_logic_vector(27 downto 0);
		wr_ctrl_lost_frame_i : in std_logic;
		wr_losses_i          : in std_logic_vector(31 downto 0);
		wr_ctrl_tm_valid_i   : in std_logic;
		wr_lab_test_o        : out std_logic;
		wr_frame_type_o      : out std_logic;
		wr_send_ctrl_o       : out std_logic;

		--DAC
		SDI_i			: in std_logic;
		SDO_o			: out std_logic;
		SCK_o			: out std_logic;
		
		--Bup Bdown counter
      bup_old_i   : in std_logic;
      bdown_old_i	: in std_logic;	
		
		
		
		--Digital potentiometer SPI
		DPOT_SCLK_o  : out std_logic;
		DPOT_SYNC_o  : out std_logic;
		DPOT_DIN_o   : out std_logic;
		DPOT_RESET_o : out std_logic;
		DPOT_RDY1_i  : in std_logic;
		DPOT_RDY2_i  : in std_logic;
		DPOT_SDO_i   : in std_logic;
		
		-- DDR wishbone interface
		wb_ddr_clk_i   : in  std_logic;
		wb_ddr_adr_o   : out std_logic_vector(31 downto 0);
		wb_ddr_dat_o   : out std_logic_vector(63 downto 0);
		wb_ddr_sel_o   : out std_logic_vector(7 downto 0);
		wb_ddr_stb_o   : out std_logic;
		wb_ddr_we_o    : out std_logic;
		wb_ddr_cyc_o   : out std_logic;
		wb_ddr_ack_i   : in  std_logic;
		wb_ddr_stall_i : in  std_logic;
		
		mem_range_o		: out std_logic;
		acqu_end_o     : out std_logic_vector(1 downto 0);

		DAC_n_LDAC_o	: out std_logic;
		DAC_n_SYNC_o	: out std_logic;
		DAC_n_RESET_o	: out std_logic;
		DAC_n_CLR_o		: out std_logic;
		
		--LCD display I/O
		LCD_SDOUT	: out std_logic := '0'; -- LCD Data in serial (SPI transmission)
		LCD_NCS		: out std_logic;        -- LCD Data chip select (SPI transmission)
		LCD_CLK		: out std_logic;        -- LCD clock (SPI transmission)
		LCD_REG_SEL : out std_logic; 
		LCD_ENABLE1 : out std_logic; 
		LCD_ENABLE2 : out std_logic; 
		NKEY1       : in std_logic;         -- Key n1 keypad
		NKEY2       : in std_logic;         -- Key n2 keypad
		NKEY3       : in std_logic;         -- Key n3 keypad
		NKEY4       : in std_logic;         -- Key n4 keypad

		dac_extern_clk_o : out std_logic;
		dac_extern_cs_o  : out std_logic;
		dac_extern_sdo_o : out std_logic;
		
		old_bup_bdown_couter  : out std_logic_vector (31 downto 0);
		wr_scale_bupbdown_cnt : out std_logic_vector (31 downto 0);
		old_bup_bdown_dr      : out std_logic

	);
end fmc_adc2M18b2ch;

architecture Behavioral of fmc_adc2M18b2ch is

  ------------------------------------------------------------------------------
  -- SDB crossbar constants declaration
  --
  -- WARNING: All address in sdb and crossbar are BYTE addresses!
  ------------------------------------------------------------------------------

  -- Number of master port(s) on the wishbone crossbar
  constant c_NUM_WB_MASTERS : integer := 3;

  -- Number of slave port(s) on the wishbone crossbar
  constant c_NUM_WB_SLAVES : integer := 1;

  -- Wishbone master(s)
  constant c_WB_MASTER : integer := 0;

  -- Wishbone slave(s)
  constant c_SLAVE_FMC_INTEGRAL : integer := 0;  -- FMC Peak Detector mezzanine
  constant c_SLAVE_FMC_I2C      : integer := 1;  -- FMC I2C mezzanine
  constant c_SLAVE_FMC_ONEWIRE  : integer := 2;  -- FMC onewire interface

  --FMC
  constant c_FMC_PEAK_DETECTOR_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",              -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"0000000000000fff",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000605",
        version   => x"00000001",
        date      => x"20131107",
        name      => "WB-integral        ")));

  constant c_FMC_ONEWIRE_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",              -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"0000000000000007",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000606",
        version   => x"00000001",
        date      => x"20131107",
        name      => "WB-Onewire.Control ")));

  constant c_FMC_I2C_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",              -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"000000000000001F",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000607",
        version   => x"00000001",
        date      => x"20131107",
        name      => "WB-I2C.Control     ")));
		  
  -- sdb header address
  constant c_SDB_ADDRESS : t_wishbone_address := x"00000000";

  -- Wishbone crossbar layout
  constant c_INTERCONNECT_LAYOUT : t_sdb_record_array(2 downto 0) :=
    (
      0 => f_sdb_embed_device(c_FMC_PEAK_DETECTOR_SDB_DEVICE, x"00001000"),
      1 => f_sdb_embed_device(c_FMC_I2C_SDB_DEVICE, x"00002000"),
      2 => f_sdb_embed_device(c_FMC_ONEWIRE_SDB_DEVICE, x"00002100")
      );
   
	constant c_DDR_COUNTER_SIM : Integer := 12; -- 0.096us when clk_sys_i = 125MHz
	constant c_DATA_LENGTH : Integer := 128;    -- nb bits of data to save to DDR
--	constant c_DATA_LENGTH : Integer := 160;    -- nb bits of data to save to DDR (added Bup_Bdown counter)
	constant c_KEEP_SIZE : Integer := 64;     -- nb bits keeped to avoid to loose 
                                               -- it because of a to big decimation
															  
	------------------------------------------------------------------------------
	-- Signals declaration
	------------------------------------------------------------------------------

	--Signal
	signal reset_n_s    : std_logic;
	signal reset_n_hw_s : std_logic;

	--WBreg
	-- Port for BIT field: 'DDR Samples enable' in reg: 'General Status and Control Register'
	signal reg_status_samples_wr_en_s               : std_logic;
	-- Port for BIT field: 'ADC or ADC correction' in reg: 'General Status and Control Register'
	signal reg_status_adc_or_corr_s                 : std_logic;
	-- Ports for BIT field: 'Ack of Error' in reg: 'General Status and Control Register'
	signal reg_status_ack_error_o_s                 : std_logic;
	signal reg_status_ack_error_i_s                 : std_logic;
	signal reg_status_ack_error_load_s              : std_logic;
	-- Port for std_logic_vector field: 'Error code' in reg: 'General Status and Control Register'
	signal reg_status_error_code_s                  : std_logic_vector(3 downto 0);
	-- Port for std_logic_vector field: 'Calibration state' in reg: 'General Status and Control Register'
	signal reg_status_calib_state_s                 : std_logic_vector(3 downto 0);
	-- Port for BIT field: 'Enable test DDR' in reg: 'General Status and Control Register'
	signal reg_status_ddr_tst_s                     : std_logic;
	-- Port for std_logic_vector field: 'Local or WR values' in reg: 'General Status and Control Register'
	signal reg_status_local_or_wr_s                 : std_logic_vector(2 downto 0);
	-- Port for std_logic_vector field: 'decim_factor' in reg: 'Decimator'
	signal reg_decim_factor_s                       : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'DDR range' in reg: 'Memory Range'
	signal reg_ddr_range_s                          : std_logic_vector(24 downto 0);
	-- Port for std_logic_vector field: 'Last sample' in reg: 'Last sample'
	signal reg_last_addr_s                          : std_logic_vector(24 downto 0);
	-- Port for std_logic_vector field: 'ADC F limit' in reg: 'ADC F limit and overrange'
	signal reg_adc_ctrl_f_limit_s                   : std_logic_vector(17 downto 0);
	-- Port for BIT field: 'ADC F overrange' in reg: 'ADC F limit and overrange'
	signal reg_adc_ctrl_f_overrange_s               : std_logic;
	-- Port for std_logic_vector field: 'ADC D limit' in reg: 'ADC D limit and overrange'
	signal reg_adc_ctrl_d_limit_s                   : std_logic_vector(17 downto 0);
	-- Port for BIT field: 'ADC D overrange' in reg: 'ADC D limit and overrange'
	signal reg_adc_ctrl_d_overrange_s               : std_logic;
	-- Port for std_logic_vector field: 'OFFSET1' in reg: 'Offset F'
	signal reg_offset_calc_corr_1_o_s               : std_logic_vector(31 downto 0);
	signal reg_offset_calc_corr_1_load_s            : std_logic;
	-- Port for std_logic_vector field: 'OFFSET_MEAS1' in reg: 'Offset measured F'
	signal reg_offset_meas_corr_1_o_s               : std_logic_vector(31 downto 0);
	signal reg_offset_meas_corr_1_load_s            : std_logic;
	-- Port for std_logic_vector field: 'OFFSET2' in reg: 'Offset D'
	signal reg_offset_calc_corr_2_o_s               : std_logic_vector(31 downto 0);
	signal reg_offset_calc_corr_2_load_s            : std_logic;
	-- Port for std_logic_vector field: 'OFFSET_MEAS2' in reg: 'Offset measured D'
	signal reg_offset_meas_corr_2_o_s               : std_logic_vector(31 downto 0);
	signal reg_offset_meas_corr_2_load_s            : std_logic;
	-- Port for std_logic_vector field: 'GAIN1' in reg: 'Gain correction F'
	signal reg_gain_corr_1_o_s                      : std_logic_vector(31 downto 0);
	signal reg_gain_corr_1_load_s                   : std_logic;
	-- Port for std_logic_vector field: 'GAIN2' in reg: 'Gain correction D'
	signal reg_gain_corr_2_o_s                      : std_logic_vector(31 downto 0);
	signal reg_gain_corr_2_load_s                   : std_logic;
	-- Port for std_logic_vector field: 'Integral range' in reg: 'Integral configuration.'
	signal reg_int_range_sel_s                      : std_logic_vector(3 downto 0);
	-- Port for BIT field: 'Marker configuration' in reg: 'Integral configuration.'
	signal reg_int_marker_cfg_s                     : std_logic;
	-- Port for BIT field: 'C0 integral reset' in reg: 'Integral configuration.'
	signal reg_int_c0_used_s                        : std_logic;
	-- Port for BIT field: 'Software low channel 1 marker bit' in reg: 'Integral configuration.'
	signal reg_int_soft_low_1_marker_s              : std_logic;
	-- Port for BIT field: 'Software high channel 1 marker bit' in reg: 'Integral configuration.'
	signal reg_int_soft_high_1_marker_s             : std_logic;
	-- Port for BIT field: 'Software low channel 2 marker bit' in reg: 'Integral configuration.'
	signal reg_int_soft_low_2_marker_s              : std_logic;
	-- Port for BIT field: 'Software high channel 2 marker bit' in reg: 'Integral configuration.'
	signal reg_int_soft_high_2_marker_s             : std_logic;
	-- Port for BIT field: 'Software channel 1 fixed value' in reg: 'Integral configuration.'
	signal reg_int_soft_fixed_1_cmd_s               : std_logic;
	-- Port for BIT field: 'Software channel 2 fixed value' in reg: 'Integral configuration.'
	signal reg_int_soft_fixed_2_cmd_s               : std_logic;
	-- Port for std_logic_vector field: 'integral_sel' in reg: 'Value for DAC and integral configuration and selection.'
	signal reg_val_cfg_sel_integral_s               : std_logic_vector(1 downto 0);
	-- Port for std_logic_vector field: 'val_sel' in reg: 'Value for DAC and integral configuration and selection.'
	signal reg_val_cfg_sel_output_s                 : std_logic_vector(3 downto 0);
	-- Ports for BIT field: 'DAC_data_source' in reg: 'Value for DAC and integral configuration and selection.'
	signal reg_val_cfg_fpga_bus_s                   : std_logic;
	-- Port for std_logic_vector field: 'Val_range' in reg: 'Value for DAC and integral configuration and selection.'
	signal reg_val_cfg_range_s                      : std_logic_vector(3 downto 0);
	-- Port for BIT field: 'Offset Meas/Calc' in reg: 'Value for DAC and integral configuration and selection.'
	signal reg_val_cfg_meas_calc_s                  : std_logic;
	-- Port for BIT field: 'Bdot from ADCs or recalculated from B' in reg: 'Value for DAC and integral configuration and selection.'
	signal reg_val_cfg_bdot_type_s                  : std_logic;
	-- Port for std_logic_vector field: 'DAC_read_back' in reg: 'DAC read back register'
	signal reg_dac_read_back_s                      : std_logic_vector(19 downto 0);
	-- Port for std_logic_vector field: 'DAC_bus' in reg: 'DAC bus value'
	signal reg_dac_bus_s                            : std_logic_vector(23 downto 0);
	-- Port for std_logic_vector field: 'Vref' in reg: 'Calibration configuration'
	signal reg_calcfg_vref_s                        : std_logic_vector(19 downto 0);
	-- Port for std_logic_vector field: 'Max Gain' in reg: 'Calibration configuration'
	signal reg_calcfg_max_gain_s                    : std_logic_vector(4 downto 0);
	-- Port for std_logic_vector field: 'Max Offset' in reg: 'Calibration configuration'
	signal reg_calcfg_max_offset_s                  : std_logic_vector(4 downto 0);
	-- Ports for BIT field: 'FORCECAL' in reg: 'Calibration configuration'
	signal reg_calcfg_force_cal_o_s                 : std_logic;
	signal user_calcfg_force_cal_o_s                : std_logic;
	signal reg_calcfg_force_cal_i_s                 : std_logic;
	signal reg_calcfg_force_cal_load_s              : std_logic;
	signal user_calcfg_force_cal_load_s             : std_logic;
	-- Port for BIT field: 'Type of fine offset calibration.' in reg: 'Calibration configuration'
	signal reg_calcfg_type_fine_offset_s         : std_logic;
	-- Port for std_logic_vector field: 'T_Calib' in reg: 'Time between two calibration'
	signal reg_t_calib_s                            : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'T_Next_Calib' in reg: 'Time before next calibration'
	signal reg_t_next_calib_s                       : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'KF_B_COEFF' in reg: 'KF Coefficient for B'
	signal reg_kf_b_coeff_s                         : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'KD_B_COEFF' in reg: 'KD Coefficient for B'
	signal reg_kd_b_coeff_s                         : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'KF_Bdot_COEFF' in reg: 'KF Coefficient for Bdot'
	signal reg_kf_bdot_coeff_s                      : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'KD_Bdot_COEFF' in reg: 'KD Coefficient for Bdot'
	signal reg_kd_bdot_coeff_s                      : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'Bdot calculation' in reg: 'Bdot intern calculation values'
	signal reg_intern_bdot_val_s                    : std_logic_vector(2 downto 0);
	-- Port for std_logic_vector field: 'D-potentiometer LSB' in reg: 'D-potentiometer LSB'
	signal reg_dpot_lsb_s                           : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'Digital Potentiometer value channel D' in reg: 'Digital Potentiometers values'
	signal reg_dpot_value_channel_d_s               : std_logic_vector(9 downto 0);
	-- Port for std_logic_vector field: 'Digital Potentiometer value channel F' in reg: 'Digital Potentiometers values'
	signal reg_dpot_value_channel_f_s               : std_logic_vector(9 downto 0);
	-- Port for std_logic_vector field: 'Digital potentiometer command bits' in reg: 'Digital Protentiometer control command bits'
	signal reg_cmd_dpot_s                           : std_logic_vector(1 downto 0);
	-- Port for std_logic_vector field: 'Digital Potentiometer channel F' in reg: 'Digital Potentiometer channel F'
	signal reg_dpot_bus_channel_f_s                 : std_logic_vector(15 downto 0);
	-- Port for std_logic_vector field: 'Digital Potentiometer channel D' in reg: 'Digital Potentiometer channel D'
	signal reg_dpot_bus_channel_d_s                 : std_logic_vector(15 downto 0);
	-- Port for std_logic_vector field: 'diff_B_D_low' in reg: 'Difference of B at marker D low'
	signal reg_diff_b_d_low_s                       : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'diff_B_F_low' in reg: 'Difference of B at marker F low'
	signal reg_diff_b_f_low_s                       : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'diff_B_D_high' in reg: 'Difference of B at marker D high'
	signal reg_diff_b_d_high_s                      : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'diff_B_F_high' in reg: 'Difference of B at marker F high'
	signal reg_diff_b_f_high_s                      : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'VoF' in reg: 'VoF (Drift)'
	signal reg_vof_s                                : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'VoD' in reg: 'VoD (Drift)'
	signal reg_vod_s                                : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'max_step_b_f' in reg: 'MaxStepB F'
	signal reg_max_step_b_f_s                       : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'max_step_b_d' in reg: 'MaxStepB D'
	signal reg_max_step_b_d_s                       : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'FCOIL' in reg: 'Surface F Coil'
	signal reg_f_coil_s                             : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'DCOIL' in reg: 'Surface D Coil'
	signal reg_d_coil_s                             : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'B_LCD' in reg: 'B LCD coeff'
	signal reg_b_coeff_lcd_s                        : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'Bdot_LCD' in reg: 'Bdot LCD coeff'
	signal reg_bdot_coeff_lcd_s                     : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'calib_nb_sample' in reg: 'Number of measurement for calibration'
   signal reg_calib_nb_sample_s                    : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'calib_delay' in reg: 'Calibration delay after Zero Cycle'
	signal reg_calib_delay_s                        : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'Sample Period' in reg: 'Control of WhiteRabbit'
	signal reg_wr_sample_period_s                   : std_logic_vector(25 downto 0);
	-- Port for std_logic_vector field: 'rx tag ctrl' in reg: 'White Rabbit tag ctrl rx register'
	signal reg_rx_ctrl_tag_s                        : std_logic_vector(27 downto 0);
	-- Port for std_logic_vector field: 'tx tag ctrl' in reg: 'White Rabbit tag ctrl tx register'
	signal reg_tx_ctrl_tag_s                        : std_logic_vector(27 downto 0);
	-- Port for std_logic_vector field: 'sync F' in reg: 'ADC sync'
	signal reg_adc_sync_f_s                         : std_logic_vector(2 downto 0);
	-- Port for std_logic_vector field: 'sync D' in reg: 'ADC sync'
	signal reg_adc_sync_d_s                         : std_logic_vector(2 downto 0);
	-- Port for std_logic_vector field: 'F low marker' in reg: 'F low marker value'
	signal reg_f_low_val_s                          : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'D low marker' in reg: 'D low marker value'
	signal reg_d_low_val_s                          : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'F high marker' in reg: 'F high marker value'
	signal reg_f_high_val_s                         : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'D high marker' in reg: 'D high marker value'
	signal reg_d_high_val_s                         : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'Time of B after C0.' in reg: 'C0+T'
	signal reg_c0_time_s                            : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'B after C0+T' in reg: 'B after C0+T'
	signal reg_b_c0_t_s                             : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'Max B after C0+T' in reg: 'Max B after C0+T'
	signal reg_max_b_c0_t_o_s                       : std_logic_vector(31 downto 0);
	signal reg_max_b_c0_t_i_s                       : std_logic_vector(31 downto 0);
	signal reg_max_b_c0_t_load_s                    : std_logic;
	-- Port for std_logic_vector field: 'Bdot after C0+T' in reg: 'Bdot after C0+T'
	signal reg_bdot_c0_t_s                          : std_logic_vector(31 downto 0);
	-- Port for std_logic_vector field: 'Max Bdot after C0+T' in reg: 'Max Bdot after C0+T'
	signal reg_max_bdot_c0_t_o_s                    : std_logic_vector(31 downto 0);
	signal reg_max_bdot_c0_t_i_s                    : std_logic_vector(31 downto 0);
	signal reg_max_bdot_c0_t_load_s                 : std_logic;
	-- Port for signed field: 'Software B channel 1 value' in reg: 'Software B channel 1 value'
	signal reg_soft_1_val_s                         : std_logic_vector(31 downto 0);
	-- Port for signed field: 'Software B channel 2 value' in reg: 'Software B channel 2 value'
	signal reg_soft_2_val_s                         : std_logic_vector(31 downto 0);

	signal calib_status_s     : std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);
	signal lcd_calib_status_reg_s : std_logic_vector(CALIB_STATUS_LCD_BUS_LENGTH-1 downto 0);
	signal error_status_s : std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);

	--Signals
	signal dma_C0_s : std_logic;
	
	signal LCD_reg_wr_sample_period_s : std_logic_vector(31 downto 0);

	signal val_pos_F_s	: std_logic_vector(31 downto 0);
	signal val_neg_F_s	: std_logic_vector(31 downto 0);
	signal val_pos_D_s	: std_logic_vector(31 downto 0);
	signal val_neg_D_s	: std_logic_vector(31 downto 0);

	signal gain_F_s			: std_logic_vector(31 downto 0);
	signal offset_calc_F_s	: std_logic_vector(31 downto 0);
	signal offset_meas_F_s	: std_logic_vector(31 downto 0);
	signal offset_F_s       : std_logic_vector(31 downto 0);
	signal gain_D_s			: std_logic_vector(31 downto 0);
	signal offset_calc_D_s	: std_logic_vector(31 downto 0);
	signal offset_meas_D_s	: std_logic_vector(31 downto 0);
	signal offset_D_s       : std_logic_vector(31 downto 0);

	signal adc_data_rdy_F_s	: std_logic;
	signal adc_data_F_s		: std_logic_vector(17 downto 0);
	signal adc_data_rdy_D_s	: std_logic;
	signal adc_data_D_s		: std_logic_vector(17 downto 0);

	signal adc_corr_rdy_F_s	: std_logic;
	signal adc_corr_F_s		: std_logic_vector(17 downto 0);
	signal adc_corr_rdy_D_s	: std_logic;
	signal adc_corr_D_s		: std_logic_vector(17 downto 0);

	signal wr_Bdot_val_s            : std_logic_vector(31 downto 0);
	signal wr_B_val_s               : std_logic_vector(31 downto 0);
	
	signal wr_G_val_s               : std_logic_vector(31 downto 0); -- 23/10/2015
	signal wr_S_val_s               : std_logic_vector(31 downto 0); -- 23/10/2015
	signal wr_I_val_s               : std_logic_vector(31 downto 0); -- 23/10/2015
	
	signal wr_old_BupBdow_counter_s : std_logic_vector(31 downto 0);
	signal teste_value_s            : std_logic_vector(31 downto 0):= x"12345678";

	signal adc_corr_D_signed_s : std_logic_vector(31 downto 0);
	signal adc_corr_F_signed_s : std_logic_vector(31 downto 0);
	signal adc_data_D_signed_s : std_logic_vector(31 downto 0);
	signal adc_data_F_signed_s : std_logic_vector(31 downto 0);

	signal DAC_end_wr_latch_s	: std_logic;
	signal DAC_end_rd_latch_s	: std_logic;
	signal DAC_data_latch_s		: std_logic_vector (NB_BITS_DAC_VALUE-1 downto 0);

	signal calib_DAC_wr_s : std_logic_vector(23 downto 0);	
	signal calib_DAC_rd_s : std_logic_vector(23 downto 0);
	signal calib_end_dac_rd_s : std_logic;
	signal calib_end_dac_wr_s : std_logic;

	signal DAC_data_out_s : std_logic_vector(NB_BITS_DAC_VALUE-1 downto 0);
	signal DAC_end_wr_s : std_logic;
	signal DAC_end_rd_s : std_logic;

	signal DAC_data_wr_s : std_logic_vector(NB_BITS_DAC_VALUE-1 downto 0);
	signal DAC_calib_wr_s : std_logic_vector(NB_BITS_DAC_VALUE-1 downto 0);
	signal CNV_F_s : std_logic;
	signal CNV_D_s : std_logic;
	
	signal cmd_dpot_reset_s : std_logic;
	signal cmd_dpot_wr_s    : std_logic;
	signal dpot_done_s      : std_logic;
	signal dpot_value_1_s   : std_logic_vector(9 downto 0);
	signal dpot_value_2_s   : std_logic_vector(9 downto 0);
	signal calib_dpot_reset_s : std_logic;
	signal calib_dpot_wr_s    : std_logic;

	signal Bk_F_s : std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal B_data_rdy_F_s : std_logic;
	signal Bk_D_s : std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal B_data_rdy_D_s : std_logic;
	signal Bdot_F_s : std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal Bdot_data_rdy_F_s : std_logic;
	signal Bdot_D_s : std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal Bdot_data_rdy_D_s : std_logic;

	signal Bk_s             : std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal Bk_eff_sim_s     : std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal Bk_36_s          : std_logic_vector(35 downto 0);
	signal B_data_rdy_s     : std_logic;
	signal B_data_rdy_buf_s : std_logic;

	signal Bdot_s               : std_logic_vector(NB_BITS_B_VALUE-1 downto 0):= x"12345678";
	signal Bdot_eff_sim_s       : std_logic_vector(NB_BITS_B_VALUE-1 downto 0):= x"12345678";
	signal Bdot_ad_s            : std_logic_vector(NB_BITS_B_VALUE-1 downto 0):= x"12345678";
	signal Bdot_fd_s            : std_logic_vector(NB_BITS_B_VALUE-1 downto 0):= x"12345678";
	signal Bdot_data_rdy_s      : std_logic;
	signal Bdot_ad_data_rdy_s   : std_logic;
	signal Bdot_fd_data_rdy_s   : std_logic;
	signal Bdot_ad_data_rdy_1_s : std_logic;
	signal Bdot_fd_data_rdy_1_s : std_logic;

	signal Bdot_dac_extern_s : std_logic_vector(17 downto 0);

	signal data_rdy_decim_s    : std_logic;
	signal counter_sim_ddr_s   : integer;
	signal data_sim_ddr_s      : unsigned(c_DATA_LENGTH-1 downto 0);
   signal data_before_decim_s : std_logic_vector(c_DATA_LENGTH-1 downto 0);
	signal data_after_decim_s  : std_logic_vector(c_DATA_LENGTH-1 downto 0);
   signal data_ddr_rdy_s      : std_logic;
	
	signal val_s : std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal data_rdy_s : std_logic;
	
	signal diff_b_d_low_s  : std_logic_vector(31 downto 0);
	signal diff_b_f_low_s  : std_logic_vector(31 downto 0);
	signal diff_b_d_high_s : std_logic_vector(31 downto 0);
	signal diff_b_f_high_s : std_logic_vector(31 downto 0);

	signal f_low_marker_s : std_logic;
	signal f_high_marker_s: std_logic;
	signal d_low_marker_s : std_logic;
	signal d_high_marker_s: std_logic;

	signal clk_2MHz_s : std_logic;
	signal clk_250KHz_s : std_logic;
	signal clk_dac_s : std_logic;

	--TO TEST
	signal ctrl_bus_s : std_logic_vector(31 downto 0);

	-- Wishbone buse(s) from crossbar master port(s)
	signal cnx_master_out : t_wishbone_master_out_array(c_NUM_WB_MASTERS-1 downto 0);
	signal cnx_master_in  : t_wishbone_master_in_array(c_NUM_WB_MASTERS-1 downto 0);

	-- Wishbone buse(s) to crossbar slave port(s)
	signal cnx_slave_out : t_wishbone_slave_out_array(c_NUM_WB_SLAVES-1 downto 0);
	signal cnx_slave_in  : t_wishbone_slave_in_array(c_NUM_WB_SLAVES-1 downto 0);
	
    -- Old Bup Bdown counter signal 
	signal reg_old_bup_bdown_bup_bdown_counter_s : std_logic_vector(31 downto 0);
	signal Bup_Bdown_wr_s                        : std_logic_vector(31 downto 0) := x"00000000";
	signal Scale_Bup_Bdown_wr_s                  : std_logic_vector(31 downto 0) := x"00000000";
	signal b_old_drdy_s                          : std_logic;
	
begin


	------------------------------------------------------------------------------
	-- CSR wishbone crossbar
	------------------------------------------------------------------------------
	cmp_sdb_crossbar : xwb_sdb_crossbar
	 generic map (
		g_num_masters => c_NUM_WB_SLAVES,
		g_num_slaves  => c_NUM_WB_MASTERS,
		g_registered  => true,
		g_wraparound  => true,
		g_layout      => c_INTERCONNECT_LAYOUT,
		g_sdb_addr    => c_SDB_ADDRESS)
	 port map (
		clk_sys_i => clk_i,
		rst_n_i   => reset_n_s,
		slave_i   => cnx_slave_in,
		slave_o   => cnx_slave_out,
		master_i  => cnx_master_in,
		master_o  => cnx_master_out);

	--Reset
	reset_n_s <= not(reset_i);
	
	-- Connect crossbar slave port to entity port
	cnx_slave_in(c_WB_MASTER).adr <= wb_csr_adr_i;
	cnx_slave_in(c_WB_MASTER).dat <= wb_csr_dat_i;
	cnx_slave_in(c_WB_MASTER).sel <= wb_csr_sel_i;
	cnx_slave_in(c_WB_MASTER).stb <= wb_csr_stb_i;
	cnx_slave_in(c_WB_MASTER).we  <= wb_csr_we_i;
	cnx_slave_in(c_WB_MASTER).cyc <= wb_csr_cyc_i;

	wb_csr_dat_o   <= cnx_slave_out(c_WB_MASTER).dat;
	wb_csr_ack_o   <= cnx_slave_out(c_WB_MASTER).ack;
	wb_csr_stall_o <= cnx_slave_out(c_WB_MASTER).stall;

	------------------------------------------------------------------------------
	-- Spec fmc-adc2M18b2ch Register 
	--    Register to configure acquisition, integral and communication :
	--    	----
	------------------------------------------------------------------------------
	cmp_fmc_adc2M18b2ch_reg : fmc_adc2M18b2ch_reg 
	port map(
		rst_n_i              => reset_n_s,
		clk_sys_i            => clk_i,

		wb_adr_i             => cnx_master_out(c_SLAVE_FMC_INTEGRAL).adr(8 downto 2),  -- cnx_master_out.adr is byte address
		wb_dat_i             => cnx_master_out(c_SLAVE_FMC_INTEGRAL).dat,
		wb_dat_o             => cnx_master_in(c_SLAVE_FMC_INTEGRAL).dat,
		wb_cyc_i             => cnx_master_out(c_SLAVE_FMC_INTEGRAL).cyc,
		wb_sel_i             => cnx_master_out(c_SLAVE_FMC_INTEGRAL).sel,
		wb_stb_i             => cnx_master_out(c_SLAVE_FMC_INTEGRAL).stb,
		wb_we_i              => cnx_master_out(c_SLAVE_FMC_INTEGRAL).we,
		wb_ack_o             => cnx_master_in(c_SLAVE_FMC_INTEGRAL).ack,
		wb_stall_o           => open,

		-- Port for BIT field: 'Operational-Spare status bit' in reg: 'General Status and Control Register'
		reg_status_opsp_status_i         => reg_status_opsp_status_i,
		-- Port for BIT field: 'Operational-Spare cmd bit' in reg: 'General Status and Control Register'
		reg_status_opsp_cmd_o            => reg_status_opsp_cmd_o,
		-- Port for BIT field: 'Simulation or effective bit' in reg: 'General Status and Control Register'
		reg_status_simeff_i              => status_simeff_i,
		-- Port for BIT field: 'Enable test DDR' in reg: 'General Status and Control Register'
		reg_status_ddr_tst_o             => reg_status_ddr_tst_s,
		-- Port for BIT field: 'Enable HW reset' in reg: 'General Status and Control Register'
		reg_status_en_hw_reset_o         => en_hw_reset_o,
		-- Port for BIT field: 'Reset HW input' in reg: 'General Status and Control Register'
		reg_status_reset_hw_i            => reset_hw_i,
		-- Port for BIT field: 'Reset SW input' in reg: 'General Status and Control Register'
		reg_status_reset_sw_o            => reset_sw_o,
		-- Port for BIT field: 'DDR Samples enable' in reg: 'General Status and Control Register'
		reg_status_samples_wr_en_o       => reg_status_samples_wr_en_s,
		-- Port for BIT field: 'Initialization done bit' in reg: 'General Status and Control Register'
		reg_status_init_done_i           => init_done_i,
		-- Port for BIT field: 'ADC or ADC correction' in reg: 'General Status and Control Register'
		reg_status_adc_or_corr_o         => reg_status_adc_or_corr_s,
		-- Ports for BIT field: 'Ack of Error' in reg: 'General Status and Control Register'
		reg_status_ack_error_o           => reg_status_ack_error_o_s,
		reg_status_ack_error_i           => reg_status_ack_error_i_s,
		reg_status_ack_error_load_o      => reg_status_ack_error_load_s,
		-- Port for std_logic_vector field: 'Error code' in reg: 'General Status and Control Register'
		reg_status_error_code_i          => reg_status_error_code_s,
		-- Port for std_logic_vector field: 'Calibration state' in reg: 'General Status and Control Register'
		reg_status_calib_state_i         => calib_status_s(3 downto 0),
		-- Port for std_logic_vector field: 'Local or WR values' in reg: 'General Status and Control Register'
		reg_status_local_or_wr_o         => reg_status_local_or_wr_s,
		-- Port for std_logic_vector field: 'DIO' in reg: 'Direction of digital I/O'
		reg_dio_dir_i							=> reg_dio_dir_i,
		-- Port for std_logic_vector field: 'Configuration' in reg: 'Digital I/O'
		reg_dio_cfg_o                    => reg_dio_cfg_o,
		-- Port for std_logic_vector field: 'Input' in reg: 'Direction of digital I/O'
		reg_dio_input_i                   => reg_dio_input_i,
		-- Port for std_logic_vector field: 'decim_factor' in reg: 'Decimator'
		reg_decim_factor_o               => reg_decim_factor_s,
		-- Port for std_logic_vector field: 'DDR range' in reg: 'Memory Range'
		reg_ddr_range_o                  => reg_ddr_range_s,
		-- Port for std_logic_vector field: 'Last sample' in reg: 'Last sample'
		reg_last_addr_i                  => reg_last_addr_s,
		-- Port for std_logic_vector field: 'ADC1' in reg: 'ADC1 data'
		reg_adc_data_1_i						=> adc_data_F_s,
		-- Port for std_logic_vector field: 'ADC2' in reg: 'ADC2 data'
		reg_adc_data_2_i						=> adc_data_D_s,
		-- Port for std_logic_vector field: 'ADC1corr' in reg: 'ADC F corr'
		reg_adc_data_corr_1_i            => adc_corr_F_s,
		-- Port for std_logic_vector field: 'ADC2corr' in reg: 'ADC D corr'
		reg_adc_data_corr_2_i            => adc_corr_D_s,
		-- Port for std_logic_vector field: 'ADC F limit' in reg: 'ADC F limit and overrange'
		reg_adc_ctrl_f_limit_o           => reg_adc_ctrl_f_limit_s,
		-- Port for BIT field: 'ADC F overrange' in reg: 'ADC F limit and overrange'
		reg_adc_ctrl_f_overrange_o       => reg_adc_ctrl_f_overrange_s,
		-- Port for std_logic_vector field: 'ADC D limit' in reg: 'ADC D limit and overrange'
		reg_adc_ctrl_d_limit_o           => reg_adc_ctrl_d_limit_s,
		-- Port for BIT field: 'ADC D overrange' in reg: 'ADC D limit and overrange'
		reg_adc_ctrl_d_overrange_o       => reg_adc_ctrl_d_overrange_s,
		-- Port for std_logic_vector field: 'OFFSET1' in reg: 'Offset ADC1'
		reg_offset_calc_corr_1_o			=> reg_offset_calc_corr_1_o_s,
		reg_offset_calc_corr_1_i			=> offset_calc_F_s,
		reg_offset_calc_corr_1_load_o		=> reg_offset_calc_corr_1_load_s,
		-- Port for std_logic_vector field: 'OFFSET_MEAS1' in reg: 'Offset measured F'
		reg_offset_meas_corr_1_o         => reg_offset_meas_corr_1_o_s,
		reg_offset_meas_corr_1_i         => offset_meas_F_s,
		reg_offset_meas_corr_1_load_o    => reg_offset_meas_corr_1_load_s,
		-- Port for std_logic_vector field: 'OFFSET2' in reg: 'Offset ADC2'
		reg_offset_calc_corr_2_o         => reg_offset_calc_corr_2_o_s,
		reg_offset_calc_corr_2_i         => offset_calc_D_s,
		reg_offset_calc_corr_2_load_o    => reg_offset_calc_corr_2_load_s,
		-- Port for std_logic_vector field: 'OFFSET_MEAS2' in reg: 'Offset measured D'
		reg_offset_meas_corr_2_o         => reg_offset_meas_corr_2_o_s,
		reg_offset_meas_corr_2_i         => offset_meas_D_s,
		reg_offset_meas_corr_2_load_o    => reg_offset_meas_corr_2_load_s,
		-- Port for std_logic_vector field: 'GAIN1' in reg: 'Gain correction value channel 1'
		reg_gain_corr_1_o						=> reg_gain_corr_1_o_s,
		reg_gain_corr_1_i						=> gain_F_s,
		reg_gain_corr_1_load_o				=> reg_gain_corr_1_load_s,
		-- Port for std_logic_vector field: 'GAIN2' in reg: 'Gain correction value channel 2'
		reg_gain_corr_2_o						=> reg_gain_corr_2_o_s,
		reg_gain_corr_2_i						=> gain_D_s,
		reg_gain_corr_2_load_o				=> reg_gain_corr_2_load_s,
		-- Port for std_logic_vector field: 'Integral range' in reg: 'Integral configuration.'
		reg_int_range_sel_o              => reg_int_range_sel_s,
		-- Port for BIT field: 'Marker configuration' in reg: 'Integral configuration.'
		reg_int_marker_cfg_o             => reg_int_marker_cfg_s,
		-- Port for BIT field: 'C0 integral reset' in reg: 'Integral configuration.'
		reg_int_c0_used_o                => reg_int_c0_used_s,
		-- Port for BIT field: 'Software low channel 1 marker bit' in reg: 'Integral configuration.'
		reg_int_soft_low_1_marker_o      => reg_int_soft_low_1_marker_s,
		-- Port for BIT field: 'Software high channel 1 marker bit' in reg: 'Integral configuration.'
		reg_int_soft_high_1_marker_o     => reg_int_soft_high_1_marker_s,
		-- Port for BIT field: 'Software low channel 2 marker bit' in reg: 'Integral configuration.'
		reg_int_soft_low_2_marker_o      => reg_int_soft_low_2_marker_s,
		-- Port for BIT field: 'Software high channel 2 marker bit' in reg: 'Integral configuration.'
		reg_int_soft_high_2_marker_o     => reg_int_soft_high_2_marker_s,
		-- Port for BIT field: 'Software channel 1 fixed value' in reg: 'Integral configuration.'
		reg_int_soft_fixed_1_cmd_o       => reg_int_soft_fixed_1_cmd_s,
		-- Port for BIT field: 'Software channel 2 fixed value' in reg: 'Integral configuration.'
		reg_int_soft_fixed_2_cmd_o       => reg_int_soft_fixed_2_cmd_s,
		-- Port for std_logic_vector field: 'integral_sel' in reg: 'Value for DAC and integral configuration and selection.'
		reg_val_cfg_sel_integral_o       => reg_val_cfg_sel_integral_s,
		-- Port for std_logic_vector field: 'DAC_sel' in reg: 'DAC selection'
		reg_val_cfg_sel_output_o         => reg_val_cfg_sel_output_s,
		-- Ports for BIT field: 'DAC_data_source' in reg: 'DAC selection'
		reg_val_cfg_fpga_bus_o           => reg_val_cfg_fpga_bus_s,
		-- Port for std_logic_vector field: 'DAC_range' in reg: 'DAC selection'
		reg_val_cfg_range_o					=> reg_val_cfg_range_s,
		-- Port for BIT field: 'Offset Meas/Calc' in reg: 'Value for DAC and integral configuration and selection.'
		reg_val_cfg_meas_calc_o          => reg_val_cfg_meas_calc_s,
		-- Port for BIT field: 'Bdot from ADCs or recalculated from B' in reg: 'Value for DAC and integral configuration and selection.'
		reg_val_cfg_bdot_type_o          => reg_val_cfg_bdot_type_s,
		-- Port for std_logic_vector field: 'DAC_ctr_reg' in reg: 'DAC control register'
		reg_dac_read_back_i					=> reg_dac_read_back_s,
		-- Port for std_logic_vector field: 'DAC_bus' in reg: 'DAC bus value'
		reg_dac_bus_o                    => reg_dac_bus_s,
		-- Port for std_logic_vector field: 'Vref' in reg: 'Calibration configuration'
		reg_calcfg_vref_o                => reg_calcfg_vref_s,
		-- Port for std_logic_vector field: 'Max Gain' in reg: 'Calibration configuration'
		reg_calcfg_max_gain_o            => reg_calcfg_max_gain_s,
		-- Port for std_logic_vector field: 'Max Offset' in reg: 'Calibration configuration'
		reg_calcfg_max_offset_o          => reg_calcfg_max_offset_s,
		-- Ports for BIT field: 'FORCECAL' in reg: 'Calibration configuration'
		reg_calcfg_force_cal_o           => reg_calcfg_force_cal_o_s,
		reg_calcfg_force_cal_i           => reg_calcfg_force_cal_i_s,
		reg_calcfg_force_cal_load_o      => reg_calcfg_force_cal_load_s,
		-- Port for BIT field: 'Type of fine offset calibration.' in reg: 'Calibration configuration'
		reg_calcfg_type_fine_offset_o    => reg_calcfg_type_fine_offset_s,
		-- Port for std_logic_vector field: 'T_Calib' in reg: 'Time between two calibration'
		reg_t_calib_o                    => reg_t_calib_s,
		-- Port for std_logic_vector field: 'T_Next_Calib' in reg: 'Time before next calibration'
		reg_t_next_calib_i               => reg_t_next_calib_s,
		-- Port for std_logic_vector field: 'F_B_COEFF' in reg: 'F Coefficient for B'
		reg_kf_b_coeff_o                  => reg_kf_b_coeff_s,
		-- Port for std_logic_vector field: 'D_B_COEFF' in reg: 'D Coefficient for B'
		reg_kd_b_coeff_o                  => reg_kd_b_coeff_s,
		-- Port for std_logic_vector field: 'F_Bdot_COEFF' in reg: 'F Coefficient for Bdot'
		reg_kf_bdot_coeff_o               => reg_kf_bdot_coeff_s,
		-- Port for std_logic_vector field: 'D_Bdot_COEFF' in reg: 'D Coefficient for Bdot'
		reg_kd_bdot_coeff_o               => reg_kd_bdot_coeff_s,
		-- Port for std_logic_vector field: 'Bdot calculation' in reg: 'Bdot intern calculation values'
		reg_intern_bdot_val_o             => reg_intern_bdot_val_s,
		-- Port for std_logic_vector field: 'D-potentiometer LSB' in reg: 'D-potentiometer LSB'
		reg_dpot_lsb_o                    => reg_dpot_lsb_s,
		-- Port for std_logic_vector field: 'Digital Potentiometer value channel D' in reg: 'Digital Potentiometers values'
		reg_dpot_value_channel_d_i        => reg_dpot_value_channel_d_s,
		-- Port for std_logic_vector field: 'Digital Potentiometer value channel F' in reg: 'Digital Potentiometers values'
		reg_dpot_value_channel_f_i        => reg_dpot_value_channel_f_s,
		-- Port for std_logic_vector field: 'Digital potentiometer command bits' in reg: 'Digital Protentiometer control command bits'
		reg_dpot_cmd_o                    => reg_cmd_dpot_s,
		-- Port for std_logic_vector field: 'Digital Potentiometer channel F' in reg: 'Digital Potentiometer channel F'
		reg_dpot_bus_channel_f_o          => reg_dpot_bus_channel_f_s,
		-- Port for std_logic_vector field: 'Digital Potentiometer channel D' in reg: 'Digital Potentiometer channel D'
		reg_dpot_bus_channel_d_o          => reg_dpot_bus_channel_d_s,
		-- Port for std_logic_vector field: 'diff_B_D_low' in reg: 'Difference of B at marker D low'
		reg_diff_b_d_low_i                => reg_diff_b_d_low_s,
		-- Port for std_logic_vector field: 'diff_B_F_low' in reg: 'Difference of B at marker F low'
		reg_diff_b_f_low_i                => reg_diff_b_f_low_s,
		-- Port for std_logic_vector field: 'diff_B_D_high' in reg: 'Difference of B at marker D high'
		reg_diff_b_d_high_i               => reg_diff_b_d_high_s,
		-- Port for std_logic_vector field: 'diff_B_F_high' in reg: 'Difference of B at marker F high'
		reg_diff_b_f_high_i               => reg_diff_b_f_high_s,
		-- Port for std_logic_vector field: 'VoF' in reg: 'VoF (Drift)'
		reg_vof_o                         => reg_vof_s,
		-- Port for std_logic_vector field: 'VoD' in reg: 'VoD (Drift)'
		reg_vod_o                         => reg_vod_s,
		-- Port for std_logic_vector field: 'max_step_b_f' in reg: 'MaxStepB F'
		reg_max_step_b_f_o                => reg_max_step_b_f_s,
		-- Port for std_logic_vector field: 'max_step_b_d' in reg: 'MaxStepB D'
		reg_max_step_b_d_o                => reg_max_step_b_d_s,
		-- Port for std_logic_vector field: 'FCOIL' in reg: 'Surface F Coil'
		reg_f_coil_o                      => reg_f_coil_s,
		-- Port for std_logic_vector field: 'DCOIL' in reg: 'Surface D Coil'
		reg_d_coil_o                      => reg_d_coil_s,
		-- Port for std_logic_vector field: 'B_LCD' in reg: 'B LCD coeff'
		reg_b_coeff_lcd_o                 => reg_b_coeff_lcd_s,
		-- Port for std_logic_vector field: 'Bdot_LCD' in reg: 'Bdot LCD coeff'
		reg_bdot_coeff_lcd_o              => reg_bdot_coeff_lcd_s,
		-- Port for std_logic_vector field: 'calib_nb_sample' in reg: 'Number of measurement for calibration'
		reg_calib_nb_sample_o             => reg_calib_nb_sample_s,
		-- Port for std_logic_vector field: 'calib_delay' in reg: 'Calibration delay after Zero Cycle'
		reg_calib_delay_o                 => reg_calib_delay_s,
		-- Port for std_logic_vector field: 'Sample Period' in reg: 'Control of WhiteRabbit'
		reg_wr_sample_period_o            => reg_wr_sample_period_s,
		-- Port for BIT field: 'Send counter ctrl bit' in reg: 'Control of WhiteRabbit'
		reg_wr_send_ctrl_o                => wr_send_ctrl_o,
		-- Port for BIT field: 'Frame type ' in reg: 'Control of WhiteRabbit'
		reg_wr_frame_type_o               => wr_frame_type_o,
		-- Port for BIT field: 'Laboratory test' in reg: 'Control of WhiteRabbit'
		reg_wr_lab_test_o                 => wr_lab_test_o,
		-- Port for std_logic_vector field: 'rx Latency' in reg: 'White Rabbit Control register'
		reg_wr_ctrl_rx_latency_i          => wr_ctrl_rx_latency_i,
		-- Port for BIT field: 'Lost frame' in reg: 'White Rabbit Control register'
		reg_wr_ctrl_lost_frame_i          => wr_ctrl_lost_frame_i,
		-- Port for BIT field: 'Synchronization valid' in reg: 'White Rabbit Control register'
		reg_wr_ctrl_tm_valid_i            => wr_ctrl_tm_valid_i,
		-- Port for std_logic_vector field: 'rx tag ctrl' in reg: 'White Rabbit tag ctrl rx register'
		reg_rx_ctrl_tag_i                 => reg_rx_ctrl_tag_s,
		-- Port for std_logic_vector field: 'tx tag ctrl' in reg: 'White Rabbit tag ctrl tx register'
		reg_tx_ctrl_tag_i                 => reg_tx_ctrl_tag_s,
		-- Port for std_logic_vector field: 'WhiteRabbit losses' in reg: 'WR losses'
		reg_wr_losses_i                   => wr_losses_i,
		-- Port for std_logic_vector field: 'sync F' in reg: 'ADC sync'
		reg_adc_sync_f_o                  => reg_adc_sync_f_s,
		-- Port for std_logic_vector field: 'sync D' in reg: 'ADC sync'
		reg_adc_sync_d_o                  => reg_adc_sync_d_s,
		-- Port for std_logic_vector field: 'F low marker' in reg: 'F low marker value'
		reg_f_low_val_o                   => reg_f_low_val_s,
		-- Port for std_logic_vector field: 'D low marker' in reg: 'D low marker value'
		reg_d_low_val_o                   => reg_d_low_val_s,
		-- Port for std_logic_vector field: 'F high marker' in reg: 'F high marker value'
		reg_f_high_val_o                  => reg_f_high_val_s,
		-- Port for std_logic_vector field: 'D high marker' in reg: 'D high marker value'
		reg_d_high_val_o                  => reg_d_high_val_s,
		-- Port for std_logic_vector field: 'Time of B after C0.' in reg: 'C0+T'
		reg_c0_time_o                     => reg_c0_time_s,
		-- Port for std_logic_vector field: 'B after C0+T' in reg: 'B after C0+T'
		reg_b_c0_t_i                      => reg_b_c0_t_s,
		-- Port for std_logic_vector field: 'Max B after C0+T' in reg: 'Max B after C0+T'
		reg_max_b_c0_t_o                  => reg_max_b_c0_t_o_s,
		reg_max_b_c0_t_i                  => reg_max_b_c0_t_i_s,
		reg_max_b_c0_t_load_o             => reg_max_b_c0_t_load_s,
		-- Port for std_logic_vector field: 'Bdot after C0+T' in reg: 'Bdot after C0+T'
		reg_bdot_c0_t_i                   => reg_bdot_c0_t_s,
		-- Port for std_logic_vector field: 'Max Bdot after C0+T' in reg: 'Max Bdot after C0+T'
		reg_max_bdot_c0_t_o               => reg_max_bdot_c0_t_o_s,
		reg_max_bdot_c0_t_i               => reg_max_bdot_c0_t_i_s,
		reg_max_bdot_c0_t_load_o          => reg_max_bdot_c0_t_load_s,
		-- Port for signed field: 'Software B channel 1 value' in reg: 'Software B channel 1 value'
		reg_soft_1_val_o                  => reg_soft_1_val_s,
		-- Port for signed field: 'Software B channel 2 value' in reg: 'Software B channel 2 value'
		reg_soft_2_val_o                      => reg_soft_2_val_s,
		reg_old_bup_bdown_bup_bdown_counter_i => reg_old_bup_bdown_bup_bdown_counter_s
	);
	reg_status_ack_error_i_s <= '0' when (error_status_s = "0000") else '1';
	wr_sample_period_o <= reg_wr_sample_period_s;

	--Clock generation
	cmp_2MHz : CLK_DIV_EVEN
	generic map( n => 40 ) -- n : Divisor Rate must be an EVEN number
	port map(
		CLK_IN		=> clk_adc_i,
		RESET			=> reset_i,
		DIV_CLK_OUT	=> clk_2MHz_s
	);

	cmp_250KHz : CLK_DIV_EVEN
	generic map( n => 400 ) -- n : Divisor Rate must be an EVEN number
	port map(
		CLK_IN		=> clk_i,
		RESET			=> reset_i,
		DIV_CLK_OUT	=> clk_250KHz_s
	);

	cmp_calib : calib 
		generic map(g_simulation => g_simulation)
		port map(
			clk_i 	=> clk_i,
			reset_i	=> reset_i,

			force_cal_o       => user_calcfg_force_cal_o_s,
			force_cal_i       => reg_calcfg_force_cal_o_s,
			force_cal_load_o  => user_calcfg_force_cal_load_s,

			time_calib_reg_i	=> reg_t_calib_s,
			t_next_calib_o		=> reg_t_next_calib_s,
			zero_cycle_i		=> zero_cycle_i,
			reg_calib_delay_i	=> reg_calib_delay_s,
			type_fine_offset_i=> reg_calcfg_type_fine_offset_s,
			
			ADC_pos_ref_i		=> reg_calcfg_vref_s,
			nb_of_meas_reg_i	=> reg_calib_nb_sample_s,

			open1_o	=> switch_open1_o,
			gain1_o	=> switch_gain1_o,
			off1_o	=> switch_off1_o,
			
			open2_o	=> switch_open2_o,
			gain2_o	=> switch_gain2_o,
			off2_o	=> switch_off2_o,

			DAC_wr_o 	=> calib_DAC_wr_s,
			DAC_rd_i 	=> calib_DAC_rd_s,
			end_dac_rd_i	=> calib_end_dac_rd_s,
			end_dac_wr_i	=> calib_end_dac_wr_s,
	
			adc_data_rdy_F_i	=> adc_data_rdy_F_s,
			adc_data_F_i		=> adc_data_F_s,
			adc_data_rdy_D_i	=> adc_data_rdy_D_s,
			adc_data_D_i		=> adc_data_D_s,

			val_pos_F_o	=> val_pos_F_s,
			val_neg_F_o	=> val_neg_F_s,
			val_pos_D_o	=> val_pos_D_s,
			val_neg_D_o	=> val_neg_D_s,

			gain_F_o				=> gain_F_s,
			offset_calc_F_o	=> offset_calc_F_s,
			offset_meas_F_o	=> offset_meas_F_s,
			gain_D_o				=> gain_D_s,
			offset_calc_D_o	=> offset_calc_D_s,
			offset_meas_D_o	=> offset_meas_D_s,
			
			dpot_done_i  => dpot_done_s,
			dpot_reset_o => calib_dpot_reset_s,
			dpot_wr_o    => calib_dpot_wr_s,
			fine_offset_o=> reg_dpot_value_channel_f_s,
	
			B_drdy_i	=> B_data_rdy_s,
			B_i		=> Bk_s,
			corr_fact_i => reg_f_coil_s,
			k_i         => reg_kf_b_coeff_s,
			step_dpot_i => reg_dpot_lsb_s,
		
			error_status_o	=> error_status_s,
			calib_status_o	=> calib_status_s
		);
		
	--error and calib status
	error_status_o	<= error_status_s;
	calib_status_o	<= calib_status_s;
		
	--Gain and Offset output register
	offset_F_s	<= offset_meas_F_s when (reg_val_cfg_meas_calc_s='1') else offset_calc_F_s;
	offset_D_s	<= offset_meas_D_s when (reg_val_cfg_meas_calc_s='1') else offset_calc_D_s;

	--Process to select external of software marker signal
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				f_low_marker_s  <= '0';
				f_high_marker_s <= '0';
				d_low_marker_s  <= '0';
				d_high_marker_s <= '0';
			else
				if (reg_int_marker_cfg_s='1') then
					f_low_marker_s  <= reg_int_soft_low_1_marker_s;
					f_high_marker_s <= reg_int_soft_high_1_marker_s;
					d_low_marker_s  <= reg_int_soft_low_2_marker_s;
					d_high_marker_s <= reg_int_soft_high_2_marker_s;
				else
					f_low_marker_s  <= F_low_marker_i;
					f_high_marker_s <= F_high_marker_i;
					d_low_marker_s  <= D_low_marker_i;
					d_high_marker_s <= D_high_marker_i;
				end if;
			end if;
		end if;
	end process;

	cmp_acquisition_core_F : acquisition_core 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			--ADC control bus
			clk_adc_i	=> clk_adc_i,
			clk_cnv_i	=> clk_2MHz_s,

			--to test
			adc_sync_i  => reg_adc_sync_f_s,

			adc_sdo_i	=> SDO_F_i,
			adc_sdi_o	=> SDI_F_o,
			adc_sck_o	=> SCK_F_o,
			adc_bsck_i  => BSCK_F_i,
			adc_cnv_o	=> CNV_F_s,

			--Correction coefficient
			gain_i	=> gain_F_s,
			offset_i => offset_F_s,
			
			--Register to choose the type of integral calculation
			reg_val_cfg_sel_integral_i => reg_val_cfg_sel_integral_s,
			reg_int_c0_used_i          => reg_int_c0_used_s,
			reg_int_range_sel_i        => reg_int_range_sel_s,
			soft_fixed_i               => reg_int_soft_fixed_1_cmd_s,
			soft_val_i                 => reg_soft_1_val_s,
			
			--Register
			Bsmooth_i	=> reg_max_step_b_f_s,
			corr_fact_i	=> reg_f_coil_s,
			Vo_i			=> reg_vof_s,

			--Input information signals
			low_marker_i  => f_low_marker_s,
			high_marker_i => f_high_marker_s,

			--Marker values
			low_marker_val_i  => reg_f_low_val_s,
			high_marker_val_i => reg_f_high_val_s,

			C0_i		=> C0_i,
			calib_status_i => calib_status_s,
			
			--Output
			B_error_low_o  => diff_b_f_low_s,
			B_error_high_o => diff_b_f_high_s,

			adc_data_rdy_o	=> adc_data_rdy_F_s,
			adc_data_o		=> adc_data_F_s,

			adc_corr_rdy_o	=> adc_corr_rdy_F_s,
			adc_corr_o		=> adc_corr_F_s,
			
			B_data_rdy_o	=> B_data_rdy_F_s,
			BKp1_o			=> Bk_F_s,
			
			Bdot_data_rdy_o=> Bdot_data_rdy_F_s,
			Bdot_o			=> Bdot_F_s
		);
	
	--Take in account the K_F coefficient for B_err_low/high
	cmp_B_err_F_low : B_err_mult 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			coeff_i => reg_kf_b_coeff_s,

			B_err_i    => diff_b_f_low_s,
			B_data_rdy_i => B_data_rdy_F_s,

			B_err_o  => reg_diff_b_f_low_s
		);

	cmp_B_err_F_high : B_err_mult 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			coeff_i => reg_kf_b_coeff_s,

			B_err_i    => diff_b_f_high_s,
			B_data_rdy_i => B_data_rdy_F_s,

			B_err_o  => reg_diff_b_f_high_s
		);

	cmp_acquisition_core_D : acquisition_core 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			--ADC control bus
			clk_adc_i	=> clk_adc_i,
			clk_cnv_i	=> clk_2MHz_s,

			--to test
			adc_sync_i  => reg_adc_sync_d_s,

			adc_sdo_i	=> SDO_D_i,
			adc_sdi_o	=> SDI_D_o,
			adc_sck_o	=> SCK_D_o,
			adc_bsck_i  => BSCK_D_i,
			adc_cnv_o	=> CNV_D_s,

			--Correction coefficient
			gain_i	=> gain_D_s,
			offset_i => offset_D_s,
			
			--Register to choose the type of integral calculation
			reg_val_cfg_sel_integral_i => reg_val_cfg_sel_integral_s,
			reg_int_c0_used_i          => reg_int_c0_used_s,
			reg_int_range_sel_i        => reg_int_range_sel_s,
			soft_fixed_i               => reg_int_soft_fixed_2_cmd_s,
			soft_val_i                 => reg_soft_2_val_s,
			
			--Register
			Bsmooth_i	=> reg_max_step_b_d_s,
			corr_fact_i	=> reg_d_coil_s,
			Vo_i			=> reg_vod_s,

			--Input information signals
			low_marker_i  => d_low_marker_s,
			high_marker_i => d_high_marker_s,

			--Marker values
			low_marker_val_i  => reg_d_low_val_s,
			high_marker_val_i => reg_d_high_val_s,

			C0_i		=> C0_i,
			calib_status_i => calib_status_s,
			
			--Output
			B_error_low_o  => diff_b_d_low_s,
			B_error_high_o => diff_b_d_high_s,

			adc_data_rdy_o	=> adc_data_rdy_D_s,
			adc_data_o		=> adc_data_D_s,

			adc_corr_rdy_o	=> adc_corr_rdy_D_s,
			adc_corr_o		=> adc_corr_D_s,
			
			B_data_rdy_o	=> B_data_rdy_D_s,
			BKp1_o			=> Bk_D_s,
			
			Bdot_data_rdy_o=> Bdot_data_rdy_D_s,
			Bdot_o			=> Bdot_D_s
		);
	CNV_o <= CNV_F_s and CNV_D_s;		

	--Take in account the K_D coefficient for B_err_low/high
	cmp_B_err_D_low : B_err_mult 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			coeff_i => reg_kd_b_coeff_s,

			B_err_i    => diff_b_d_low_s,
			B_data_rdy_i => B_data_rdy_D_s,

			B_err_o  => reg_diff_b_d_low_s
		);

	cmp_B_err_D_high : B_err_mult 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			coeff_i => reg_kd_b_coeff_s,

			B_err_i    => diff_b_d_high_s,
			B_data_rdy_i => B_data_rdy_D_s,

			B_err_o  => reg_diff_b_d_high_s
		);
	
	cmp_B_FD_sum : B_FD_Sum 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			reg_f_b_coeff_i	=> reg_kf_b_coeff_s,
			reg_d_b_coeff_i	=> reg_kd_b_coeff_s,
			reg_kf_bdot_coeff_i => reg_kf_bdot_coeff_s,

			reg_intern_bdot_val_i => reg_intern_bdot_val_s,

			Bk_f_i			=> Bk_F_s,
			B_f_data_rdy_i	=> B_data_rdy_F_s,

			Bk_d_i			=> Bk_D_s,
			B_d_data_rdy_i	=> B_data_rdy_D_s,

			Bk_o				=> Bk_s,
			B_data_rdy_o	=> B_data_rdy_s,

			Bdot_o				=> Bdot_fd_s,
			Bdot_data_rdy_o	=> Bdot_fd_data_rdy_s
		);
	
	cmp_Bdot_FD_sum : B_FD_Sum 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			reg_f_b_coeff_i	=> reg_kf_bdot_coeff_s,
			reg_d_b_coeff_i	=> reg_kd_bdot_coeff_s,
			reg_kf_bdot_coeff_i => reg_kf_bdot_coeff_s,
			
			reg_intern_bdot_val_i => reg_intern_bdot_val_s,

			Bk_f_i			=> Bdot_F_s,
			B_f_data_rdy_i	=> Bdot_data_rdy_F_s,

			Bk_d_i			=> Bdot_D_s,
			B_d_data_rdy_i	=> Bdot_data_rdy_D_s,

			Bk_o				=> Bdot_ad_s,
			B_data_rdy_o	=> Bdot_ad_data_rdy_s,

			Bdot_o				=> open,
			Bdot_data_rdy_o	=> open 
		);
			
	--Output Bk and Bdot selection process
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				Bk_o                 <= (others=>'0');
				Bdot_s               <= (others=>'0');
				Bdot_o               <= (others=>'0');
				Bdot_data_rdy_o      <= '0';
				B_data_rdy_o         <= '0';
				Bdot_data_rdy_s      <= '0';
				Bdot_ad_data_rdy_1_s <= '0';
				Bdot_fd_data_rdy_1_s <= '0';
				B_data_rdy_buf_s     <= '0';
			else
				if ((calib_status_s=IDLE_st)or
				    (calib_status_s=NEXT_ZCYCLE_START_st)) then --keep last value during calibration
					if (status_simeff_i='1') then
						Bk_eff_sim_s   <= Bk_s;
						Bdot_eff_sim_s <= Bdot_s;
						Bk_o   <= Bk_s;
						Bdot_o <= Bdot_s;
					else
						Bk_eff_sim_s   <= Bk_s(31) & Bk_s(31) & Bk_s(30 downto 1);
						Bdot_eff_sim_s <= Bdot_s(31) & Bdot_s(31) & Bdot_s(30 downto 1);
						Bk_o   <= Bk_s(31) & Bk_s(31) & Bk_s(30 downto 1);
						Bdot_o <= Bdot_s(31) & Bdot_s(31) & Bdot_s(30 downto 1);
					end if;
					B_data_rdy_buf_s <= B_data_rdy_s;
				else
					B_data_rdy_buf_s <= adc_data_rdy_D_s;
				end if;
				if (reg_val_cfg_bdot_type_s='0') then
					if (Bdot_ad_data_rdy_1_s='0' and Bdot_ad_data_rdy_s='1') then
						Bdot_s <= Bdot_ad_s;
					end if;
					Bdot_ad_data_rdy_1_s <= Bdot_ad_data_rdy_s;
					Bdot_data_rdy_s <= Bdot_ad_data_rdy_s;
				else
					if (Bdot_fd_data_rdy_1_s='0' and Bdot_fd_data_rdy_s='1') then
						Bdot_s <= Bdot_fd_s;
					end if;
					Bdot_fd_data_rdy_1_s <= Bdot_fd_data_rdy_s;
					Bdot_data_rdy_s <= Bdot_fd_data_rdy_s;
				end if;
				Bdot_data_rdy_o <= Bdot_data_rdy_s;
--				B_data_rdy_o <= B_data_rdy_s;			
				B_data_rdy_o <= B_data_rdy_buf_s;			
			end if;
		end if;
	end process;

	cmp_mux_dac_val : mux_dac_val
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			--Bk
			BKp1_F_i			=> Bk_F_s,
			B_data_rdy_F_i	=> B_data_rdy_F_s,

			BKp1_D_i			=> Bk_D_s,
			B_data_rdy_D_i	=> B_data_rdy_D_s,

			--B
			B_i            => Bk_eff_sim_s,
			B_data_rdy_i   => B_data_rdy_s,

			--Bdot
			Bdot_F_i				=> Bdot_F_s,
			Bdot_data_rdy_F_i	=> Bdot_data_rdy_F_s,

			Bdot_D_i				=> Bdot_D_s,
			Bdot_data_rdy_D_i	=> Bdot_data_rdy_D_s,

			--Bdot
			Bdot_i            => Bdot_eff_sim_s,
			Bdot_data_rdy_i   => Bdot_data_rdy_s,

			--ADC value
			adc_data_F_i		=> adc_data_F_s,
			adc_data_rdy_F_i	=> adc_data_rdy_F_s,

			adc_data_D_i		=> adc_data_D_s,
			adc_data_rdy_D_i	=> adc_data_rdy_D_s,

			adc_data_F_corr_i		=> adc_corr_F_s,
			adc_data_rdy_F_corr_i=> adc_corr_rdy_F_s,

			adc_data_D_corr_i		=> adc_corr_D_s,
			adc_data_rdy_D_corr_i=> adc_corr_rdy_D_s,

			--White Rabbit value
			wr_B_val_i     => wr_B_val_i,
			wr_Bdot_val_i  => wr_Bdot_val_i,  
			
			wr_G_val_i     => wr_G_val_i,     --23/10/2015
			wr_S_val_i     => wr_S_val_i,     --23/10/2015
			
			wr_I_val_i     => wr_I_val_i, 
			wr_I_drdy_i    => wr_I_drdy_i,     --23/10/2015
			wr_drdy_i      => wr_drdy_i,

			--register
			reg_val_sel_i			=> reg_val_cfg_sel_output_s,
			reg_val_sel_range_i	=> reg_val_cfg_range_s,
			
			--Output
			val_o			=> val_s,
			data_rdy_o	=> data_rdy_s
		);
		
	cmp_switch_dac : switch_dac 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			--Value for DAC selectionned by Mux_dac_val
			val_i			=> val_s,
			data_rdy_i	=> data_rdy_s,

			--Value for DAC for electronic calibration
			calib_DAC_wr_i 	=> calib_DAC_wr_s,
			calib_DAC_rd_o 	=> calib_DAC_rd_s,
			calib_end_dac_rd_o=> calib_end_dac_rd_s,
			calib_end_dac_wr_o=> calib_end_dac_wr_s,
			
			--Value for DAC from the register controled by the PC
			reg_dac_bus_i	=> reg_dac_bus_s,
			
			--Control
			reg_val_sel_fpga_bus_i	=> reg_val_cfg_fpga_bus_s,
			calib_status_i				=> calib_status_s,

			--Control bus for DAC
			end_dac_rd_i	=> DAC_end_rd_s,
			end_dac_wr_i	=> DAC_end_wr_s,
			DAC_rd_i 		=> DAC_data_out_s,
			DAC_wr_o 		=> DAC_data_wr_s
		);

	--DAC
	cmp_DAC : AD5791
		Port map( 
			CLK    => clk_i,
			RESET  => reset_i,
			
			SDI  => SDI_i,
			SDO  => SDO_o,
			SCK  => SCK_o,
			
			N_LDAC  => DAC_n_LDAC_o,
			N_SYNC  => DAC_n_SYNC_o,
			N_RESET => DAC_n_RESET_o,
			N_CLR   => DAC_n_CLR_o,
			
			END_WR_DAC => DAC_end_wr_s,
			END_RD_DAC => DAC_end_rd_s,
			DAC_WR     => DAC_data_wr_s,
			DAC_RD     => DAC_data_out_s
		);	 
		
	--Detection of Data written in DAC
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				reg_dac_read_back_s <= (others=>'0');
			else
				if (DAC_end_wr_s='1') then
					case DAC_data_wr_s(23 downto 20) is
						when WR_DAC_REGISTER => 
							reg_dac_read_back_s <= DAC_data_wr_s(19 downto 0);
						when WR_DAC_CONTROL =>
							reg_dac_read_back_s <= DAC_data_wr_s(19 downto 0);
						when WR_DAC_CLR_CODE =>
							reg_dac_read_back_s <= DAC_data_wr_s(19 downto 0);
						when others =>
					end case;
				end if;
				if (DAC_end_rd_s='1') then
					case DAC_data_wr_s(23 downto 20) is
						when RD_DAC_REGISTER =>
							reg_dac_read_back_s <= DAC_data_out_s(19 downto 0);
						when RD_DAC_CONTROL =>
							reg_dac_read_back_s <= DAC_data_out_s(19 downto 0);
						when RD_DAC_CLR_CODE =>
							reg_dac_read_back_s <= DAC_data_out_s(19 downto 0);
						when others =>
					end case;
				end if;
			end if;
		end if;
	end process;
	
	------------------------------------------------------
	--- Wishbone register of the old Bup-Bdown counter --- 
	------------------------------------------------------
	
	cmp_old_bup_bdown_counter : Bup_Bdown
	Port map ( 
		Gbl_Reset_i       => reset_i,    
	   C0_Reset_i        => C0_i,
		Bup_i             => bup_old_i,
		Bdown_i           => bdown_old_i,   
		Bupdown_drdy_o    => b_old_drdy_s,
 		B_Cnt_o           => Bup_Bdown_wr_s,
		Scale_B_Cnt_o     => Scale_Bup_Bdown_wr_s,
		Clk_i             => clk_10_i
		--C0 pulse programmable delay in clock period number
	   --pdly_cst_i       : in  STD_LOGIC_VECTOR (19 downto 0)
		); 		
		
		--reg_old_bup_bdown_bup_bdown_counter_s <= Bup_Bdown_wr_s;
		--reg_old_bup_bdown_bup_bdown_counter_s <= x"cafecafe";
		
--	getting_couter_output: process (clk_10_i)
	getting_couter_output: process (clk_i) -- use wishbonne clock 
	begin
		if rising_edge(clk_i) then
			if (reset_i = '1') then
				reg_old_bup_bdown_bup_bdown_counter_s <= (others=>'0');
			else
				if (b_old_drdy_s = '1') then 
					reg_old_bup_bdown_bup_bdown_counter_s <= Bup_Bdown_wr_s;				
				end if;				
		  end if;
	  end if;  
	old_bup_bdown_couter  <= Bup_Bdown_wr_s;
	wr_scale_bupbdown_cnt <= Scale_Bup_Bdown_wr_s;
	old_bup_bdown_dr      <= b_old_drdy_s; 
	end process;

	-- Processes to construct sample to DDR and simulate acquisition in Modelsim
	--Construct data for DDR before decim
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				data_before_decim_s      <= (others=>'0');
				data_ddr_rdy_s	          <= '0';
				counter_sim_ddr_s        <= 0;
				data_sim_ddr_s           <= (others=>'0');
				
				wr_Bdot_val_s            <= (others=>'0');
				wr_B_val_s               <= (others=>'0');
				wr_G_val_s               <= (others=>'0'); --23/10/2015
				wr_S_val_s               <= (others=>'0'); --23/10/2015
				wr_I_val_s               <= (others=>'0'); --23/10/2015
				
				wr_old_BupBdow_counter_s <= (others=>'0');
				
				adc_corr_D_signed_s      <= (others=>'0');
				adc_corr_F_signed_s      <= (others=>'0');
				adc_data_D_signed_s      <= (others=>'0');
				adc_data_F_signed_s      <= (others=>'0');
			else
				if (wr_drdy_i='1') then
					wr_Bdot_val_s            <= wr_Bdot_val_i;
					wr_B_val_s               <= wr_B_val_i;
					wr_G_val_s               <= wr_G_val_i; --23/10/2015
					wr_S_val_s               <= wr_S_val_i; --23/10/2015
					wr_old_BupBdow_counter_s <= Scale_Bup_Bdown_wr_s; 
				end if;
				
				if (wr_I_drdy_i='1') then
					wr_I_val_s  <=  wr_I_val_i;
				end if;
	
--				if (data_ddr_rdy_s='1' and B_data_rdy_s='0') then
				if (data_ddr_rdy_s='1' and B_data_rdy_buf_s='0') then
					adc_corr_D_signed_s <= adc_corr_D_s(17) & adc_corr_D_s(17) & adc_corr_D_s(17) & adc_corr_D_s(17) & 
												  adc_corr_D_s(17) & adc_corr_D_s(17) & adc_corr_D_s(17) & adc_corr_D_s(17) & 
												  adc_corr_D_s(17) & adc_corr_D_s(17) & adc_corr_D_s(17) & adc_corr_D_s(17) & 
												  adc_corr_D_s(17) & adc_corr_D_s(17) & adc_corr_D_s;
					adc_corr_F_signed_s <= adc_corr_F_s(17) & adc_corr_F_s(17) & adc_corr_F_s(17) & adc_corr_F_s(17) & 
												  adc_corr_F_s(17) & adc_corr_F_s(17) & adc_corr_F_s(17) & adc_corr_F_s(17) & 
												  adc_corr_F_s(17) & adc_corr_F_s(17) & adc_corr_F_s(17) & adc_corr_F_s(17) & 
												  adc_corr_F_s(17) & adc_corr_F_s(17) & adc_corr_F_s;
					adc_data_D_signed_s <= adc_data_D_s(17) & adc_data_D_s(17) & adc_data_D_s(17) & adc_data_D_s(17) & 
												  adc_data_D_s(17) & adc_data_D_s(17) & adc_data_D_s(17) & adc_data_D_s(17) & 
												  adc_data_D_s(17) & adc_data_D_s(17) & adc_data_D_s(17) & adc_data_D_s(17) & 
												  adc_data_D_s(17) & adc_data_D_s(17) & adc_data_D_s;
					adc_data_F_signed_s <= adc_data_F_s(17) & adc_data_F_s(17) & adc_data_F_s(17) & adc_data_F_s(17) & 
												  adc_data_F_s(17) & adc_data_F_s(17) & adc_data_F_s(17) & adc_data_F_s(17) & 
												  adc_data_F_s(17) & adc_data_F_s(17) & adc_data_F_s(17) & adc_data_F_s(17) & 
												  adc_data_F_s(17) & adc_data_F_s(17) & adc_data_F_s;
				end if;
				if (g_simulation = 0) then
--					if (data_ddr_rdy_s='0' and B_data_rdy_s='1') then
					if (data_ddr_rdy_s='0' and B_data_rdy_buf_s='1') then
						case reg_status_local_or_wr_s is
							-- B local values --
							when "000" =>
								data_before_decim_s	<= "0000000" & d_high_marker_m_i & "0000000" & d_low_marker_m_i &
										"0000000" & f_high_marker_m_i & "0000000" & f_low_marker_m_i &
										"00000000"                    & "0000000" & zero_cycle_m_i &
										"0000000" & status_simeff_i   & "0000000" & C0_m_i &
										Bdot_eff_sim_s & Bk_eff_sim_s;
							-- Values received by WR, B & Bdot --
							when "001" =>
								data_before_decim_s	<= "0000000" & d_high_marker_m_i & "0000000" & wr_d_low_marker_i &
										"0000000" & f_high_marker_m_i & "0000000" & wr_f_low_marker_i &
										"00000000"                    & "0000000" & wr_zero_cycle_i &
										"0000000" & wr_eff_sim_i      & "0000000" & wr_C0_i &
										wr_Bdot_val_s & wr_B_val_s;	
							-- Values received by WR, I, G & S --
							when "010" =>
								data_before_decim_s	<= x"000000AA" &
										wr_I_val_s &
										wr_Bdot_val_s & 
										wr_S_val_s;										
						-- ADC local values --
							when "011" =>
								data_before_decim_s	<= adc_corr_D_signed_s &
										adc_corr_F_signed_s &
										adc_data_D_signed_s &
										adc_data_F_signed_s;
							-- B side by side values --
							when "100" =>
								data_before_decim_s	<= Bk_F_s &
										Bk_D_s &
										Bdot_F_s &
										Bdot_D_s;
							when others =>
								data_before_decim_s	<= "0000000" & d_high_marker_m_i & "0000000" & wr_d_low_marker_i &
										"0000000" & f_high_marker_m_i & "0000000" & wr_f_low_marker_i &
										"00000000"                    & "0000000" & wr_zero_cycle_i &
										"0000000" & wr_eff_sim_i      & "0000000" & wr_C0_i &
										wr_Bdot_val_s & wr_B_val_s;								
						end case;
					end if;
--					data_ddr_rdy_s	<= B_data_rdy_s;
					data_ddr_rdy_s	<= B_data_rdy_buf_s;
					counter_sim_ddr_s <= 0;
				else
					data_before_decim_s <= std_logic_vector(data_sim_ddr_s);
					if (counter_sim_ddr_s < (c_DDR_COUNTER_SIM-1)) then
						counter_sim_ddr_s <= counter_sim_ddr_s+1;
						data_ddr_rdy_s	<= '0';
					else
						counter_sim_ddr_s <= 0;
						data_ddr_rdy_s	<= '1';
						data_sim_ddr_s <= data_sim_ddr_s + 1;
					end if;
				end if;
			end if;
		end if;
	end process;

	--Write to DDR				
	cmp_decimator : decimator 
		generic map(
			g_DATA_LENGTH  => c_DATA_LENGTH
		)
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			decim_factor_i => reg_decim_factor_s,

			data_i	=> data_before_decim_s,
			d_rdy_i	=> data_ddr_rdy_s,

			data_o	=> data_after_decim_s,
			d_rdy_o	=> data_rdy_decim_s
		);

	--swith between local of wr C0 for DMA transfer
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				dma_C0_s <= '0';
			else
				if (reg_status_local_or_wr_s="001" or reg_status_local_or_wr_s="011" or reg_status_local_or_wr_s="010") then
					dma_C0_s <= wr_C0_i;
				else
					dma_C0_s <= C0_i;
				end if;
			end if;
		end if;
	end process;
	
	cmp_acqu_ddr_ctrl : acqu_ddr_ctrl 
		generic map(
			g_sim => g_simulation,
			g_WORD_LENGTH => 64,
			g_DATA_LENGTH => c_DATA_LENGTH
		)
		port map(
			clk_i   => clk_i,
			reset_i => reset_i,
			
			--Control
			samples_wr_en_i   => reg_status_samples_wr_en_s,
			ddr_test_en_i     => reg_status_ddr_tst_s,
			ddr_range_i       => reg_ddr_range_s,
			last_addr_o       => reg_last_addr_s,
			
			--Data
			sync_data_i       => data_after_decim_s,
			sync_data_valid_i => data_rdy_decim_s,
			C0_i              => dma_C0_s, --C0_i,

			-- DDR wishbone interface
			wb_ddr_clk_i   => wb_ddr_clk_i,
			wb_ddr_adr_o   => wb_ddr_adr_o,
			wb_ddr_dat_o   => wb_ddr_dat_o,
			wb_ddr_sel_o   => wb_ddr_sel_o,
			wb_ddr_stb_o   => wb_ddr_stb_o,
			wb_ddr_we_o    => wb_ddr_we_o,
			wb_ddr_cyc_o   => wb_ddr_cyc_o,
			wb_ddr_ack_i   => wb_ddr_ack_i,
			wb_ddr_stall_i => wb_ddr_stall_i,

			--To test
			ram_addr_o => open,
			ram_data_o => open,
			ram_wr_o   => open,

			mem_range_o => mem_range_o,
			acqu_end_o  => acqu_end_o
		);
		
	--DAC extern for Bdot value as current
	cmp_BDOT_DAC : BDOT_DAC
    port map(
		clk_i    => clk_i,
		reset_i  => reset_i,
		
		Bdot_i       => Bdot_dac_extern_s,
		Bdot_Valid_i => Bdot_data_rdy_s,
		
		dac_cs_o   => dac_extern_cs_o,
		dac_clk_o  => dac_extern_clk_o,
		dac_sdo_o  => dac_extern_sdo_o
	);
	Bdot_dac_extern_s <= (not Bdot_s(31)) & Bdot_s(30 downto 14);

	cmp_C0_time : C0_time 
		port map(
			clk_i 	=> clk_i,
			reset_i	=> reset_i,

			B_i     => Bk_eff_sim_s,
			Bdot_i  => Bdot_eff_sim_s,
			C0_i    => C0_i,
			
			reg_c0_time_i        => reg_c0_time_s,
			reg_b_c0_t_o         => reg_b_c0_t_s,
			reg_max_b_c0_t_i     => reg_max_b_c0_t_o_s,
			reg_max_b_c0_t_load_i=> reg_max_b_c0_t_load_s,
			reg_max_b_c0_t_o     => reg_max_b_c0_t_i_s,
			
			reg_bdot_c0_t_o         => reg_bdot_c0_t_s,
			reg_max_bdot_c0_t_i     => reg_max_bdot_c0_t_o_s,
			reg_max_bdot_c0_t_load_i=> reg_max_bdot_c0_t_load_s,
			reg_max_bdot_c0_t_o     => reg_max_bdot_c0_t_i_s
		);

	--For test potentiometer's SPI state machin
	cmp_AD5292 : AD5292
		port map(
			clk_i   => clk_i,
			reset_i => reset_i,
			
			dpot_reset_i => cmd_dpot_reset_s,
			dpot_wr_i    => cmd_dpot_wr_s,

			data_1_i   => dpot_value_1_s,
			data_2_i   => dpot_value_2_s,
			
			sclk_o       => DPOT_SCLK_o,
			sdout_o      => DPOT_DIN_o,
			sdin_i       => DPOT_SDO_i,
			rdy1_i       => DPOT_RDY1_i,
			rdy2_i       => DPOT_RDY2_i,
			dpot_reset_o => DPOT_RESET_o,
			n_sync_o     => DPOT_SYNC_o,
			done_o       => dpot_done_s
		);

	process (clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				cmd_dpot_reset_s <= '0';
				cmd_dpot_wr_s    <= '0';
			else
				if (calib_status_s=IDLE_st) then
					cmd_dpot_reset_s <= reg_cmd_dpot_s(0);
					cmd_dpot_wr_s    <= reg_cmd_dpot_s(1);
					dpot_value_1_s   <= reg_dpot_bus_channel_f_s(9 downto 0);
					dpot_value_2_s   <= reg_dpot_bus_channel_d_s(9 downto 0);
				else
					cmd_dpot_reset_s <= calib_dpot_reset_s;
					cmd_dpot_wr_s    <= calib_dpot_wr_s;
					dpot_value_1_s   <= reg_dpot_value_channel_f_s;
					dpot_value_2_s   <= reg_dpot_value_channel_d_s;
				end if;
				--for the fine offset, we work only on the first channel
				reg_dpot_value_channel_d_s <= reg_dpot_bus_channel_d_s(9 downto 0);
			end if;
		end if;
	end process;
		
	--Calib Status conversion for LCD display
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				lcd_calib_status_reg_s <= LCD_WAIT_CALIB_st;
			else
				case calib_status_s is
					when IDLE_st =>
						lcd_calib_status_reg_s <= LCD_WAIT_CALIB_st;
					when 	NEXT_ZCYCLE_START_st =>
						lcd_calib_status_reg_s <= LCD_NEXT_CYCLE_START_st;
					when DPOT_RESET_st =>
						lcd_calib_status_reg_s <= LCD_MEAS_POS_st;
					when SWITCH_AS_GAIN_st =>
						lcd_calib_status_reg_s <= LCD_MEAS_POS_st;
					when CONFIG_DAC_st =>
						lcd_calib_status_reg_s <= LCD_MEAS_POS_st;
					when MEAS_POS_st =>
						lcd_calib_status_reg_s <= LCD_MEAS_POS_st;
					when MEAS_NEG_st =>
						lcd_calib_status_reg_s <= LCD_MEAS_NEG_st;
					when CALC_st =>
						lcd_calib_status_reg_s <= LCD_MEAS_NEG_st;
					when SWITCH_AS_OFFSET_st =>
						lcd_calib_status_reg_s <= LCD_OFFSET_st;
					when OFFSET_st =>
						lcd_calib_status_reg_s <= LCD_OFFSET_st;
					when SWITCH_AS_INPUT_st =>
						lcd_calib_status_reg_s <= LCD_OFFSET_st;
					when DPOT_st =>
						lcd_calib_status_reg_s <= LCD_OFFSET_st;
					when others =>
						lcd_calib_status_reg_s <= LCD_WAIT_CALIB_st;
				end case;
			end if;
		end if;
	end process;

	--LCD Display
	reset_n_hw_s <= not reset_n_hw_i;
	cmp_LCD : LCD4x40_Interface 
		Port map(
			CLK	=> clk_10_i,
         CLK_100MHz_i => clk_i,
			RESET => reset_n_hw_s,--not reset_n_hw_i,--reset_s,

			Fraw_ADC		=> adc_data_F_s,
			Fraw_DRDY	=> adc_data_rdy_F_s,
			Draw_ADC		=> adc_data_D_s,
			Draw_DRDY	=> adc_data_rdy_D_s,
			Fcal_ADC		=> adc_corr_F_s,
			Fcal_DRDY	=> adc_corr_rdy_F_s,
			Dcal_ADC		=> adc_corr_D_s,
			Dcal_DRDY	=> adc_corr_rdy_D_s,

			BCD_COUNTDOWN	=> reg_t_next_calib_s,-- Next calibration in BCD format from register

			F_LIM		=> reg_adc_ctrl_f_limit_s,
			D_LIM		=> reg_adc_ctrl_d_limit_s,
			F_OVR		=> reg_adc_ctrl_f_overrange_s,
			D_OVR		=> reg_adc_ctrl_d_overrange_s,
			
			F_PREF	=> val_pos_F_s(31 downto 14),
			D_PREF	=> val_pos_D_s(31 downto 14),
			F_NREF	=> val_neg_F_s(31 downto 14),
			D_NREF	=> val_neg_D_s(31 downto 14),
			
			--TODO add copy of register for FMR to BTrain card for LCD
			--TODO add trigger delay for display
			FL_Wwid	=> x"00000000",
			FH_Wwid	=> x"00000000",
			DL_Wwid	=> x"00000000",
			DH_Wwid	=> x"00000000", 
			FL_Wdly	=> x"00000000",
			FH_Wdly	=> x"00000000",
			DL_Wdly	=> x"00000000",
			DH_Wdly	=> x"00000000", 
			
			FL_Twid	=> x"00000000",
			FH_Twid	=> x"00000000",
			DL_Twid	=> x"00000000",
			DH_Twid	=> x"00000000", 
			
			F_GCC		=> gain_F_s,
			D_GCC		=> gain_D_s,
			C_DAC		=> reg_dac_read_back_s,

			F_OCC		=> offset_meas_F_s(31 downto 14),
			D_OCC		=> offset_meas_D_s(31 downto 14),
			AF_COIL	=> reg_f_coil_s,	-- TODO Determine the range of coil surface (until 3m2)
			AD_COIL	=> reg_d_coil_s,	-- TODO Determine the range of coil surface (until 3m2)
			
			KF_BMEAS => reg_kf_b_coeff_s,
			KD_BMEAS => reg_kd_b_coeff_s,
			KF_BDOT	=> reg_kf_bdot_coeff_s,
			KD_BDOT	=> reg_kd_bdot_coeff_s,
			
			B_DOT		 => Bdot_s,
			B_DOT_DRDY=> Bdot_data_rdy_s,
			
			B_MEAS 	  => Bk_eff_sim_s,
			B_MEAS_DRDY=> B_data_rdy_s,
			
			B_HOLD_TIME => reg_c0_time_s,
			B_HOLD      => reg_b_c0_t_s,
			B_THRESHOLD => reg_max_b_c0_t_i_s,

			Bdot_HOLD      => reg_bdot_c0_t_s,
			Bdot_THRESHOLD => reg_max_bdot_c0_t_i_s,
			
			--TODO add timing delay to start calibration after zero cycle signal
			Z0_DLY 		=> reg_calib_delay_s,	
			WR_UPD 		=> LCD_reg_wr_sample_period_s,

			CAL_STATUS	=> lcd_calib_status_reg_s,
			--TODO sortir les signaux pour les errreurs de calibration ERROR_LCD_BUS_LENGTH
			--     add maximum limit of difference between two value of offset and/or gain
			CAL_ERROR	=> NO_ERROR_st,--calib_error_reg_s,

			LCD_RD_NWR 	=> open,
			LCD_REG_SEL => LCD_REG_SEL, 
			LCD_ENABLE1 => LCD_ENABLE1,
			LCD_ENABLE2 => LCD_ENABLE2,
			LCD_SDOUT	=> LCD_SDOUT,
			LCD_NCS		=> LCD_NCS,
			LCD_CLK		=> LCD_CLK,
			
			--TODO feedback from commutation front end
			OPER_SPARE		=> reg_status_opsp_status_i,
			EFF_SIMUL		=> status_simeff_i,

			NKEY1 => NKEY1,
			NKEY2 => NKEY2,
			NKEY3 => NKEY3,
			NKEY4 => NKEY4
		);
		
	LCD_reg_wr_sample_period_s <= "000000" & reg_wr_sample_period_s;
		
end Behavioral;

