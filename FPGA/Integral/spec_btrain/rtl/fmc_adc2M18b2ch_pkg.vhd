--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package fmc_adc2m18b2ch_pkg is

	constant NB_BITS_B_VALUE			: Integer := 32;
	constant NB_BITS_BSMOOTH_VALUE	: Integer := NB_BITS_B_VALUE;
	constant NB_BITS_A_GAIN_VALUE		: Integer := NB_BITS_B_VALUE;
	constant NB_BITS_ADC_VALUE			: Integer := 18;
	constant NB_BITS_DAC_VALUE			: Integer := 24;
	constant NB_BITS_OFFSET_VALUE		: Integer := NB_BITS_B_VALUE;
	constant NB_BITS_DELTA_T_VALUE	: Integer := NB_BITS_B_VALUE;

	constant NB_BITS_DSP48A1_INPUT	: Integer := 18;
	constant NB_BITS_DSP48A1_OUTPUT	: Integer := 48;

	constant NB_SYS_IO	: integer := 10;		
	
   constant c_CFG_DAC5791 : std_logic_vector (23 downto 0) := x"200000";
   constant WR_DAC_REGISTER	: std_logic_vector(3 downto 0) := x"1";
	constant WR_DAC_CONTROL		: std_logic_vector(3 downto 0) := x"2";
	constant WR_DAC_CLR_CODE	: std_logic_vector(3 downto 0) := x"3";
	constant WR_DAC_SOFTWARE	: std_logic_vector(3 downto 0) := x"4";
	constant RD_DAC_REGISTER	: std_logic_vector(3 downto 0) := x"9";
	constant RD_DAC_CONTROL		: std_logic_vector(3 downto 0) := x"A";
	constant RD_DAC_CLR_CODE	: std_logic_vector(3 downto 0) := x"B";
	
	constant SWITCH_OPEN_CMD	: std_logic := '0';
	constant SWITCH_CLOSE_CMD	: std_logic := '1';
	
	type calib_cmd is
	(
		SWITCH_INPUT,
		SWITCH_GAIN,SWITCH_OFFSET
	);

	type DAC_cmd is
	(
		DAC_CMD_NOOP,
		DAC_CMD_CFG,
		DAC_CMD_POS,DAC_CMD_NEG
	);

	-- Logic level for effective/simul bit
	constant STATUS_EFFECTIVE : std_logic := '1';
	constant STATUS_SIMUL     : std_logic := '0';

	--AD5791 DAC control command
	constant AD5791_NOOP 			: std_logic_vector(3 downto 0) := x"0";
	constant AD5791_WR_VAL 			: std_logic_vector(3 downto 0) := x"1";
	constant AD5791_WR_CTRL 		: std_logic_vector(3 downto 0) := x"2";
	constant AD5791_WR_CLEARCODE 	: std_logic_vector(3 downto 0) := x"3";
	constant AD5791_WR_SOFTCODE 	: std_logic_vector(3 downto 0) := x"4";
	constant AD5791_RD_VAL 			: std_logic_vector(3 downto 0) := x"9";
	constant AD5791_RD_CTRL 		: std_logic_vector(3 downto 0) := x"A";
	constant AD5791_RD_CLEARCODE 	: std_logic_vector(3 downto 0) := x"B";

	--Calibration Status bus -> IDLE/RUNNING bit and status
	constant CALIB_STATUS_BUS_LENGTH : integer := 5; 
	constant IDLE_st					: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0) := "0" & x"0";
	constant NEXT_ZCYCLE_START_st	: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0) := "0" & x"1";
	constant SWITCH_AS_GAIN_st		: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0) := "1" & x"2";
	constant CONFIG_DAC_st			: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0) := "1" & x"3";
	constant MEAS_POS_st				: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0) := "1" & x"4";
	constant MEAS_NEG_st				: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0) := "1" & x"5";
	constant CALC_st					: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0) := "1" & x"6";
	constant SWITCH_AS_OFFSET_st	: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0) := "1" & x"7";
	constant OFFSET_st				: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0) := "1" & x"8";
	constant SWITCH_AS_INPUT_st	: std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0) := "1" & x"9";
	constant DPOT_RESET_st        : std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0) := "1" & x"a";
	constant DPOT_st              : std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0) := "1" & x"b";
	
	constant CALIB_STATUS_LCD_BUS_LENGTH : integer := 4; 
	constant LCD_WAIT_CALIB_st			: std_logic_vector(CALIB_STATUS_LCD_BUS_LENGTH-1 downto 0) := x"0";
	constant LCD_NEXT_CYCLE_START_st	: std_logic_vector(CALIB_STATUS_LCD_BUS_LENGTH-1 downto 0) := x"1";
	constant LCD_MEAS_POS_st			: std_logic_vector(CALIB_STATUS_LCD_BUS_LENGTH-1 downto 0) := x"2";
	constant LCD_MEAS_NEG_st			: std_logic_vector(CALIB_STATUS_LCD_BUS_LENGTH-1 downto 0) := x"3";
	constant LCD_OFFSET_st				: std_logic_vector(CALIB_STATUS_LCD_BUS_LENGTH-1 downto 0) := x"4";
	
	constant ERROR_LCD_BUS_LENGTH : integer := 4; 
	constant NO_ERROR_st				: std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0) := x"0";
	constant ERROR_OFFSET_st		: std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0) := x"1";
	constant ERROR_GAIN_st			: std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0) := x"2";
	constant ERROR_DAC_st			: std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0) := x"3";

	component fmc_adc2m18b2ch 
		generic(g_simulation : integer := 0);
		port (
			clk_i			: in std_logic;
			clk_10_i	   : in std_logic;
			clk_adc_i	: in std_logic;

			reset_n_hw_i  : in std_logic;
			reset_i		  : in std_logic;
			reset_hw_i    : in std_logic;
			reset_sw_o    : out std_logic;
			en_hw_reset_o : out std_logic;
			init_done_i   : in std_logic;

			--Input information signals
			F_low_marker_i : in std_logic;
			F_high_marker_i: in std_logic;
			D_low_marker_i : in std_logic;
			D_high_marker_i: in std_logic;
			C0_i				: in std_logic;
			zero_cycle_i	: in std_logic;
		
			f_low_marker_m_i : in std_logic;
			f_high_marker_m_i: in std_logic;
			d_low_marker_m_i : in std_logic;
			d_high_marker_m_i: in std_logic;
			C0_m_i           : in std_logic;
			zero_cycle_m_i   : in std_logic;
			
			--Switch
			switch_open1_o	: out std_logic;
			switch_gain1_o	: out std_logic;
			switch_off1_o	: out std_logic;
			
			switch_open2_o	: out std_logic;
			switch_gain2_o	: out std_logic;
			switch_off2_o	: out std_logic;
			
			error_status_o	: out std_logic_vector(ERROR_LCD_BUS_LENGTH-1 downto 0);
			calib_status_o	: out std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);

			--ADC
			CNV_o		: out std_logic;
			
			SDI_F_o	: out std_logic;
			SDO_F_i	: in std_logic;
			SCK_F_o	: out std_logic;
			BSCK_F_i	: in std_logic;

			SDI_D_o	: out std_logic;
			SDO_D_i	: in std_logic;
			SCK_D_o	: out std_logic;
			BSCK_D_i	: in std_logic;
			
			-- CSR wishbone interface
			wb_csr_adr_i   : in  std_logic_vector(31 downto 0);
			wb_csr_dat_i   : in  std_logic_vector(31 downto 0);
			wb_csr_dat_o   : out std_logic_vector(31 downto 0);
			wb_csr_cyc_i   : in  std_logic;
			wb_csr_sel_i   : in  std_logic_vector(3 downto 0);
			wb_csr_stb_i   : in  std_logic;
			wb_csr_we_i    : in  std_logic;
			wb_csr_ack_o   : out std_logic;
			wb_csr_stall_o : out std_logic;

			--Register	
			status_simeff_i          : in std_logic;
			reg_status_opsp_cmd_o    : out std_logic;
			reg_status_opsp_status_i : in std_logic;
			
			reg_dio_input_i : in std_logic_vector(9 downto 0);
			reg_dio_dir_i   : in std_logic_vector(9 downto 0);
--			reg_dio_dir_o   : out std_logic_vector(9 downto 0);
			reg_dio_cfg_o   : out std_logic_vector(9 downto 0);

			wr_B_val_i        : in std_logic_vector(31 downto 0);
			wr_Bdot_val_i     : in std_logic_vector(31 downto 0);
			
			wr_G_val_i        : in std_logic_vector(31 downto 0); -- 23/10/2015
			wr_S_val_i        : in std_logic_vector(31 downto 0); -- 23/10/2015
			wr_I_drdy_i       : in std_logic;                     -- 23/10/2015			
			
			wr_I_val_i        : in std_logic_vector(31 downto 0);
			wr_drdy_i         : in std_logic;
			wr_d_low_marker_i : in std_logic;
			wr_f_low_marker_i : in std_logic;
			wr_C0_i           : in std_logic;
			wr_zero_cycle_i   : in std_logic;
			wr_eff_sim_i      : in std_logic;

			Bk_o           : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_data_rdy_o   : out std_logic;
			Bdot_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			Bdot_data_rdy_o: out std_logic;

			--WR-register
			wr_sample_period_o   : out std_logic_vector(25 downto 0);
			wr_ctrl_rx_latency_i : in std_logic_vector(27 downto 0);
			wr_ctrl_lost_frame_i : in std_logic;
			wr_losses_i          : in std_logic_vector(31 downto 0);
			wr_ctrl_tm_valid_i   : in std_logic;
			wr_lab_test_o        : out std_logic;
			wr_frame_type_o      : out std_logic;
			wr_send_ctrl_o       : out std_logic;

			--DAC
			SDI_i			: in std_logic;
			SDO_o			: out std_logic;
			SCK_o			: out std_logic;
			
			--Digital potentiometer SPI
			DPOT_SCLK_o  : out std_logic;
			DPOT_SYNC_o  : out std_logic;
			DPOT_DIN_o   : out std_logic;
			DPOT_RESET_o : out std_logic;
			DPOT_RDY1_i  : in std_logic;
			DPOT_RDY2_i  : in std_logic;
			DPOT_SDO_i   : in std_logic;

			-- DDR wishbone interface
			wb_ddr_clk_i   : in  std_logic;
			wb_ddr_adr_o   : out std_logic_vector(31 downto 0);
			wb_ddr_dat_o   : out std_logic_vector(63 downto 0);
			wb_ddr_sel_o   : out std_logic_vector(7 downto 0);
			wb_ddr_stb_o   : out std_logic;
			wb_ddr_we_o    : out std_logic;
			wb_ddr_cyc_o   : out std_logic;
			wb_ddr_ack_i   : in  std_logic;
			wb_ddr_stall_i : in  std_logic;

			mem_range_o		: out std_logic;
			acqu_end_o     : out std_logic_vector(1 downto 0);

			DAC_n_LDAC_o	: out std_logic;
			DAC_n_SYNC_o	: out std_logic;
			DAC_n_RESET_o	: out std_logic;
			DAC_n_CLR_o		: out std_logic;

			--LCD display I/O
			LCD_SDOUT	: out std_logic := '0'; -- LCD Data in serial (SPI transmission)
			LCD_NCS		: out std_logic;        -- LCD Data chip select (SPI transmission)
			LCD_CLK		: out std_logic;        -- LCD clock (SPI transmission)
			LCD_REG_SEL : out std_logic; 
			LCD_ENABLE1 : out std_logic; 
			LCD_ENABLE2 : out std_logic; 
			NKEY1       : in std_logic;         -- Key n1 keypad
			NKEY2       : in std_logic;         -- Key n2 keypad
			NKEY3       : in std_logic;         -- Key n3 keypad
			NKEY4       : in std_logic;         -- Key n4 keypad

			dac_extern_clk_o : out std_logic;
			dac_extern_cs_o  : out std_logic;
			dac_extern_sdo_o : out std_logic;
			
			bup_old_i   : in std_logic;
		   bdown_old_i : in std_logic;
			
			old_bup_bdown_couter  : out std_logic_vector (31 downto 0);
			wr_scale_bupbdown_cnt : out std_logic_vector (31 downto 0);
	      old_bup_bdown_dr      : out std_logic
		);
	end component;

	component fmc_adc2m18b2ch_reg
	  port (
			rst_n_i                                  : in     std_logic;
			clk_sys_i                                : in     std_logic;
			wb_adr_i                                 : in     std_logic_vector(6 downto 0);
			wb_dat_i                                 : in     std_logic_vector(31 downto 0);
			wb_dat_o                                 : out    std_logic_vector(31 downto 0);
			wb_cyc_i                                 : in     std_logic;
			wb_sel_i                                 : in     std_logic_vector(3 downto 0);
			wb_stb_i                                 : in     std_logic;
			wb_we_i                                  : in     std_logic;
			wb_ack_o                                 : out    std_logic;
			wb_stall_o                               : out    std_logic;
			-- Port for BIT field: 'Operational-Spare status bit' in reg: 'General Status and Control Register'
			reg_status_opsp_status_i                 : in     std_logic;
			-- Port for BIT field: 'Operational-Spare cmd bit' in reg: 'General Status and Control Register'
			reg_status_opsp_cmd_o                    : out    std_logic;
			-- Port for BIT field: 'Simulation or effective bit' in reg: 'General Status and Control Register'
			reg_status_simeff_i                      : in     std_logic;
			-- Port for BIT field: 'Enable test DDR' in reg: 'General Status and Control Register'
			reg_status_ddr_tst_o                     : out    std_logic;
			-- Port for BIT field: 'Enable HW reset' in reg: 'General Status and Control Register'
			reg_status_en_hw_reset_o                 : out    std_logic;
			-- Port for BIT field: 'Reset HW input' in reg: 'General Status and Control Register'
			reg_status_reset_hw_i                    : in     std_logic;
			-- Port for BIT field: 'Reset SW input' in reg: 'General Status and Control Register'
			reg_status_reset_sw_o                    : out    std_logic;
			-- Port for BIT field: 'DDR Samples enable' in reg: 'General Status and Control Register'
			reg_status_samples_wr_en_o               : out    std_logic;
			-- Port for BIT field: 'Initialization done bit' in reg: 'General Status and Control Register'
			reg_status_init_done_i                   : in     std_logic;
			-- Port for BIT field: 'ADC or ADC correction' in reg: 'General Status and Control Register'
			reg_status_adc_or_corr_o                 : out    std_logic;
			-- Ports for BIT field: 'Ack of Error' in reg: 'General Status and Control Register'
			reg_status_ack_error_o                   : out    std_logic;
			reg_status_ack_error_i                   : in     std_logic;
			reg_status_ack_error_load_o              : out    std_logic;
			-- Port for std_logic_vector field: 'Error code' in reg: 'General Status and Control Register'
			reg_status_error_code_i                  : in     std_logic_vector(3 downto 0);
			-- Port for std_logic_vector field: 'Calibration state' in reg: 'General Status and Control Register'
			reg_status_calib_state_i                 : in     std_logic_vector(3 downto 0);
			-- Port for std_logic_vector field: 'Local or WR values' in reg: 'General Status and Control Register'
			reg_status_local_or_wr_o                 : out    std_logic_vector(2 downto 0);
			-- Port for std_logic_vector field: 'Direction' in reg: 'Digital I/O'
			reg_dio_dir_i                            : in     std_logic_vector(9 downto 0);
			-- Port for std_logic_vector field: 'Configuration' in reg: 'Digital I/O'
			reg_dio_cfg_o                            : out    std_logic_vector(9 downto 0);
			-- Port for std_logic_vector field: 'Input' in reg: 'Digital I/O'
			reg_dio_input_i                          : in     std_logic_vector(9 downto 0);
			-- Port for std_logic_vector field: 'decim_factor' in reg: 'Decimator'
			reg_decim_factor_o                       : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'DDR range' in reg: 'Memory Range'
			reg_ddr_range_o                          : out    std_logic_vector(24 downto 0);
			-- Port for std_logic_vector field: 'Last sample' in reg: 'Last sample'
			reg_last_addr_i                          : in     std_logic_vector(24 downto 0);
			-- Port for std_logic_vector field: 'ADC1' in reg: 'ADC F'
			reg_adc_data_1_i                         : in     std_logic_vector(17 downto 0);
			-- Port for std_logic_vector field: 'ADC2' in reg: 'ADC D'
			reg_adc_data_2_i                         : in     std_logic_vector(17 downto 0);
			-- Port for std_logic_vector field: 'ADC1corr' in reg: 'ADC F corr'
			reg_adc_data_corr_1_i                    : in     std_logic_vector(17 downto 0);
			-- Port for std_logic_vector field: 'ADC2corr' in reg: 'ADC D corr'
			reg_adc_data_corr_2_i                    : in     std_logic_vector(17 downto 0);
			-- Port for std_logic_vector field: 'ADC F limit' in reg: 'ADC F limit and overrange'
			reg_adc_ctrl_f_limit_o                   : out    std_logic_vector(17 downto 0);
			-- Port for BIT field: 'ADC F overrange' in reg: 'ADC F limit and overrange'
			reg_adc_ctrl_f_overrange_o               : out    std_logic;
			-- Port for std_logic_vector field: 'ADC D limit' in reg: 'ADC D limit and overrange'
			reg_adc_ctrl_d_limit_o                   : out    std_logic_vector(17 downto 0);
			-- Port for BIT field: 'ADC D overrange' in reg: 'ADC D limit and overrange'
			reg_adc_ctrl_d_overrange_o               : out    std_logic;
			-- Port for std_logic_vector field: 'OFFSET_CALC1' in reg: 'Offset calculated F'
			reg_offset_calc_corr_1_o                 : out    std_logic_vector(31 downto 0);
			reg_offset_calc_corr_1_i                 : in     std_logic_vector(31 downto 0);
			reg_offset_calc_corr_1_load_o            : out    std_logic;
			-- Port for std_logic_vector field: 'OFFSET_MEAS1' in reg: 'Offset measured F'
			reg_offset_meas_corr_1_o                 : out    std_logic_vector(31 downto 0);
			reg_offset_meas_corr_1_i                 : in     std_logic_vector(31 downto 0);
			reg_offset_meas_corr_1_load_o            : out    std_logic;
			-- Port for std_logic_vector field: 'OFFSET_CALC2' in reg: 'Offset calculated D'
			reg_offset_calc_corr_2_o                 : out    std_logic_vector(31 downto 0);
			reg_offset_calc_corr_2_i                 : in     std_logic_vector(31 downto 0);
			reg_offset_calc_corr_2_load_o            : out    std_logic;
			-- Port for std_logic_vector field: 'OFFSET_MEAS2' in reg: 'Offset measured D'
			reg_offset_meas_corr_2_o                 : out    std_logic_vector(31 downto 0);
			reg_offset_meas_corr_2_i                 : in     std_logic_vector(31 downto 0);
			reg_offset_meas_corr_2_load_o            : out    std_logic;
			-- Port for std_logic_vector field: 'GAIN1' in reg: 'Gain correction F'
			reg_gain_corr_1_o                        : out    std_logic_vector(31 downto 0);
			reg_gain_corr_1_i                        : in     std_logic_vector(31 downto 0);
			reg_gain_corr_1_load_o                   : out    std_logic;
			-- Port for std_logic_vector field: 'GAIN2' in reg: 'Gain correction D'
			reg_gain_corr_2_o                        : out    std_logic_vector(31 downto 0);
			reg_gain_corr_2_i                        : in     std_logic_vector(31 downto 0);
			reg_gain_corr_2_load_o                   : out    std_logic;
			-- Port for std_logic_vector field: 'Integral range' in reg: 'Integral configuration.'
			reg_int_range_sel_o                      : out    std_logic_vector(3 downto 0);
			-- Port for BIT field: 'Marker configuration' in reg: 'Integral configuration.'
			reg_int_marker_cfg_o                     : out    std_logic;
			-- Port for BIT field: 'C0 integral reset' in reg: 'Integral configuration.'
			reg_int_c0_used_o                        : out    std_logic;
			-- Port for BIT field: 'Software low channel 1 marker bit' in reg: 'Integral configuration.'
			reg_int_soft_low_1_marker_o              : out    std_logic;
			-- Port for BIT field: 'Software high channel 1 marker bit' in reg: 'Integral configuration.'
			reg_int_soft_high_1_marker_o             : out    std_logic;
			-- Port for BIT field: 'Software low channel 2 marker bit' in reg: 'Integral configuration.'
			reg_int_soft_low_2_marker_o              : out    std_logic;
			-- Port for BIT field: 'Software high channel 2 marker bit' in reg: 'Integral configuration.'
			reg_int_soft_high_2_marker_o             : out    std_logic;
			-- Port for BIT field: 'Software channel 1 fixed value' in reg: 'Integral configuration.'
			reg_int_soft_fixed_1_cmd_o               : out    std_logic;
			-- Port for BIT field: 'Software channel 2 fixed value' in reg: 'Integral configuration.'
			reg_int_soft_fixed_2_cmd_o               : out    std_logic;
			-- Port for std_logic_vector field: 'integral_sel' in reg: 'Value for DAC and integral configuration and selection.'
			reg_val_cfg_sel_integral_o               : out    std_logic_vector(1 downto 0);
			-- Port for std_logic_vector field: 'val_sel' in reg: 'Value for DAC and integral configuration and selection.'
			reg_val_cfg_sel_output_o                 : out    std_logic_vector(3 downto 0);
			-- Port for BIT field: 'DAC_data_source' in reg: 'Value for DAC and integral configuration and selection.'
			reg_val_cfg_fpga_bus_o                   : out    std_logic;
			-- Port for std_logic_vector field: 'Val_range' in reg: 'Value for DAC and integral configuration and selection.'
			reg_val_cfg_range_o                      : out    std_logic_vector(3 downto 0);
			-- Port for BIT field: 'Offset Meas/Calc' in reg: 'Value for DAC and integral configuration and selection.'
			reg_val_cfg_meas_calc_o                  : out    std_logic;
			-- Port for BIT field: 'Bdot from ADCs or recalculated from B' in reg: 'Value for DAC and integral configuration and selection.'
			reg_val_cfg_bdot_type_o                  : out    std_logic;
			-- Port for std_logic_vector field: 'DAC_read_back' in reg: 'DAC read back register'
			reg_dac_read_back_i                      : in     std_logic_vector(19 downto 0);
			-- Port for std_logic_vector field: 'DAC_bus' in reg: 'DAC bus value'
			reg_dac_bus_o                            : out    std_logic_vector(23 downto 0);
			-- Port for std_logic_vector field: 'Vref' in reg: 'Calibration configuration'
			reg_calcfg_vref_o                        : out    std_logic_vector(19 downto 0);
			-- Port for std_logic_vector field: 'Max Gain' in reg: 'Calibration configuration'
			reg_calcfg_max_gain_o                    : out    std_logic_vector(4 downto 0);
			-- Port for std_logic_vector field: 'Max Offset' in reg: 'Calibration configuration'
			reg_calcfg_max_offset_o                  : out    std_logic_vector(4 downto 0);
			-- Ports for BIT field: 'FORCECAL' in reg: 'Calibration configuration'
			reg_calcfg_force_cal_o                   : out    std_logic;
			reg_calcfg_force_cal_i                   : in     std_logic;
			reg_calcfg_force_cal_load_o              : out    std_logic;
			-- Port for BIT field: 'Type of fine offset calibration.' in reg: 'Calibration configuration'
			reg_calcfg_type_fine_offset_o            : out    std_logic;
			-- Port for std_logic_vector field: 'T_Calib' in reg: 'Time between two calibration'
			reg_t_calib_o                            : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'T_Next_Calib' in reg: 'Time before next calibration'
			reg_t_next_calib_i                       : in     std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'KF_B_COEFF' in reg: 'KF Coefficient for B'
			reg_kf_b_coeff_o                         : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'KD_B_COEFF' in reg: 'KD Coefficient for B'
			reg_kd_b_coeff_o                         : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'KF_Bdot_COEFF' in reg: 'KF Coefficient for Bdot'
			reg_kf_bdot_coeff_o                      : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'KD_Bdot_COEFF' in reg: 'KD Coefficient for Bdot'
			reg_kd_bdot_coeff_o                      : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'Bdot calculation' in reg: 'Bdot intern calculation values'
			reg_intern_bdot_val_o                    : out    std_logic_vector(2 downto 0);
			-- Port for std_logic_vector field: 'D-potentiometer LSB' in reg: 'D-potentiometer LSB'
			reg_dpot_lsb_o                           : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'Digital Potentiometer value channel D' in reg: 'Digital Potentiometers values'
			reg_dpot_value_channel_d_i               : in     std_logic_vector(9 downto 0);
			-- Port for std_logic_vector field: 'Digital Potentiometer value channel F' in reg: 'Digital Potentiometers values'
			reg_dpot_value_channel_f_i               : in     std_logic_vector(9 downto 0);
			-- Port for std_logic_vector field: 'Digital potentiometer command bits' in reg: 'Digital Protentiometer control command bits'
			reg_dpot_cmd_o                           : out    std_logic_vector(1 downto 0);
			-- Port for std_logic_vector field: 'Digital Potentiometer channel F' in reg: 'Digital Potentiometer channel F'
			reg_dpot_bus_channel_f_o                 : out    std_logic_vector(15 downto 0);
			-- Port for std_logic_vector field: 'Digital Potentiometer channel D' in reg: 'Digital Potentiometer channel D'
			reg_dpot_bus_channel_d_o                 : out    std_logic_vector(15 downto 0);
			-- Port for std_logic_vector field: 'diff_B_D_low' in reg: 'Difference of B at marker D low'
			reg_diff_b_d_low_i                       : in     std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'diff_B_F_low' in reg: 'Difference of B at marker F low'
			reg_diff_b_f_low_i                       : in     std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'diff_B_D_high' in reg: 'Difference of B at marker D high'
			reg_diff_b_d_high_i                      : in     std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'diff_B_F_high' in reg: 'Difference of B at marker F high'
			reg_diff_b_f_high_i                      : in     std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'VoF' in reg: 'VoF (Drift)'
			reg_vof_o                                : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'VoD' in reg: 'VoD (Drift)'
			reg_vod_o                                : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'max_step_b_f' in reg: 'MaxStepB F'
			reg_max_step_b_f_o                       : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'max_step_b_d' in reg: 'MaxStepB D'
			reg_max_step_b_d_o                       : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'FCOIL' in reg: 'Surface F Coil'
			reg_f_coil_o                             : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'DCOIL' in reg: 'Surface D Coil'
			reg_d_coil_o                             : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'B_LCD' in reg: 'B LCD coeff'
			reg_b_coeff_lcd_o                        : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'Bdot_LCD' in reg: 'Bdot LCD coeff'
			reg_bdot_coeff_lcd_o                     : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'calib_nb_sample' in reg: 'Number of measurement for calibration'
			reg_calib_nb_sample_o                    : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'calib_delay' in reg: 'Calibration delay after start of Zero Cycle'
			reg_calib_delay_o                        : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'Sample Period' in reg: 'Control of WhiteRabbit'
			reg_wr_sample_period_o                   : out    std_logic_vector(25 downto 0);
			-- Port for BIT field: 'Send counter ctrl bit' in reg: 'Control of WhiteRabbit'
			reg_wr_send_ctrl_o                       : out    std_logic;
			-- Port for BIT field: 'Frame type' in reg: 'Control of WhiteRabbit'
			reg_wr_frame_type_o                      : out    std_logic;
			-- Port for BIT field: 'Laboratory test' in reg: 'Control of WhiteRabbit'
			reg_wr_lab_test_o                        : out    std_logic;
			-- Port for std_logic_vector field: 'rx Latency' in reg: 'White Rabbit Control register'
			reg_wr_ctrl_rx_latency_i                 : in     std_logic_vector(27 downto 0);
			-- Port for BIT field: 'Lost frame' in reg: 'White Rabbit Control register'
			reg_wr_ctrl_lost_frame_i                 : in     std_logic;
			-- Port for BIT field: 'Synchronization valid' in reg: 'White Rabbit Control register'
			reg_wr_ctrl_tm_valid_i                   : in     std_logic;
			-- Port for std_logic_vector field: 'rx tag ctrl' in reg: 'White Rabbit tag ctrl rx register'
			reg_rx_ctrl_tag_i                        : in     std_logic_vector(27 downto 0);
			-- Port for std_logic_vector field: 'tx tag ctrl' in reg: 'White Rabbit tag ctrl tx register'
			reg_tx_ctrl_tag_i                        : in     std_logic_vector(27 downto 0);
			-- Port for std_logic_vector field: 'WhiteRabbit losses' in reg: 'WR losses'
			reg_wr_losses_i                          : in     std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'sync F' in reg: 'ADC sync'
			reg_adc_sync_f_o                         : out    std_logic_vector(2 downto 0);
			-- Port for std_logic_vector field: 'sync D' in reg: 'ADC sync'
			reg_adc_sync_d_o                         : out    std_logic_vector(2 downto 0);
			-- Port for std_logic_vector field: 'F low marker' in reg: 'F low marker value'
			reg_f_low_val_o                          : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'D low marker' in reg: 'D low marker value'
			reg_d_low_val_o                          : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'F high marker' in reg: 'F high marker value'
			reg_f_high_val_o                         : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'D high marker' in reg: 'D high marker value'
			reg_d_high_val_o                         : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'Time of B after C0.' in reg: 'C0+T'
			reg_c0_time_o                            : out    std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'B after C0+T' in reg: 'B after C0+T'
			reg_b_c0_t_i                             : in     std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'Max B after C0+T' in reg: 'Max B after C0+T'
			reg_max_b_c0_t_o                         : out    std_logic_vector(31 downto 0);
			reg_max_b_c0_t_i                         : in     std_logic_vector(31 downto 0);
			reg_max_b_c0_t_load_o                    : out    std_logic;
			-- Port for std_logic_vector field: 'Bdot after C0+T' in reg: 'Bdot after C0+T'
			reg_bdot_c0_t_i                          : in     std_logic_vector(31 downto 0);
			-- Port for std_logic_vector field: 'Max Bdot after C0+T' in reg: 'Max Bdot after C0+T'
			reg_max_bdot_c0_t_o                      : out    std_logic_vector(31 downto 0);
			reg_max_bdot_c0_t_i                      : in     std_logic_vector(31 downto 0);
			reg_max_bdot_c0_t_load_o                 : out    std_logic;
			-- Port for signed field: 'Software B channel 1 value' in reg: 'Software B channel 1 value'
			reg_soft_1_val_o                         : out    std_logic_vector(31 downto 0);
			-- Port for signed field: 'Software B channel 2 value' in reg: 'Software B channel 2 value'
			reg_soft_2_val_o                         : out    std_logic_vector(31 downto 0);		
			-- Port for std_logic_vector field: 'Bup Bdown counter' in reg: 'Old Bup Bdown counter'
			reg_old_bup_bdown_bup_bdown_counter_i    : in     std_logic_vector(31 downto 0)
		);
	end component;

	component carrier_csr 
		port (
			rst_n_i                                  : in     std_logic;
			clk_sys_i                                : in     std_logic;
			wb_adr_i                                 : in     std_logic_vector(1 downto 0);
			wb_dat_i                                 : in     std_logic_vector(31 downto 0);
			wb_dat_o                                 : out    std_logic_vector(31 downto 0);
			wb_cyc_i                                 : in     std_logic;
			wb_sel_i                                 : in     std_logic_vector(3 downto 0);
			wb_stb_i                                 : in     std_logic;
			wb_we_i                                  : in     std_logic;
			wb_ack_o                                 : out    std_logic;
			wb_stall_o                               : out    std_logic;
			-- Port for std_logic_vector field: 'PCB revision' in reg: 'Carrier type and PCB version'
			carrier_csr_carrier_pcb_rev_i            : in     std_logic_vector(3 downto 0);
			-- Port for std_logic_vector field: 'Reserved register' in reg: 'Carrier type and PCB version'
			carrier_csr_carrier_reserved_o           : out    std_logic_vector(11 downto 0);
			-- Port for std_logic_vector field: 'Carrier type' in reg: 'Carrier type and PCB version'
			carrier_csr_carrier_type_i               : in     std_logic_vector(15 downto 0);
			-- Port for BIT field: 'FMC presence' in reg: 'Status'
			carrier_csr_stat_fmc_pres_i              : in     std_logic;
			-- Port for BIT field: 'GN4142 core P2L PLL status' in reg: 'Status'
			carrier_csr_stat_p2l_pll_lck_i           : in     std_logic;
			-- Port for BIT field: 'System clock PLL status' in reg: 'Status'
			carrier_csr_stat_sys_pll_lck_i           : in     std_logic;
			-- Port for BIT field: 'DDR3 calibration status' in reg: 'Status'
			carrier_csr_stat_ddr3_cal_done_i         : in     std_logic;
			-- Port for std_logic_vector field: 'Reserved' in reg: 'Status'
			carrier_csr_stat_reserved_o              : out    std_logic_vector(27 downto 0);
			-- Port for std_logic_vector field: 'LED' in reg: 'Control'
			carrier_csr_ctrl_led_o                   : out    std_logic_vector(3 downto 0);
			-- Port for std_logic_vector field: 'Reserved' in reg: 'Control'
			carrier_csr_ctrl_reserved_o              : out    std_logic_vector(27 downto 0);
			-- Port for BIT field: 'State of the reset line' in reg: 'Reset Register'
			carrier_csr_rst_fmc0_n_o                 : out    std_logic;
			-- Port for std_logic_vector field: 'Reserved' in reg: 'Reset Register'
			carrier_csr_rst_reserved_o               : out    std_logic_vector(30 downto 0)
		);
	end component;

end package fmc_adc2m18b2ch_pkg;

