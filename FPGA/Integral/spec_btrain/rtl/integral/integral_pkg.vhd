--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

library work;
use work.fmc_adc2M18b2ch_pkg.all;

package integral_pkg is

	component B_FD_Sum 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;

			reg_f_b_coeff_i     : in std_logic_vector(31 downto 0);
			reg_d_b_coeff_i     : in std_logic_vector(31 downto 0);
			reg_kf_bdot_coeff_i : in std_logic_vector(31 downto 0);

			reg_intern_bdot_val_i : in std_logic_vector(2 downto 0);

			Bk_f_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_f_data_rdy_i	: in std_logic;

			Bk_d_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_d_data_rdy_i	: in std_logic;

			Bk_o				: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_data_rdy_o	: out std_logic;

			Bdot_o				: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			Bdot_data_rdy_o	: out std_logic
		);
	end component;

	component B_err_mult 
		port(
			clk_i   : in std_logic;
			reset_i : in std_logic;

			coeff_i : in std_logic_vector(31 downto 0);

			B_err_i    : in std_logic_vector(31 downto 0);
			B_data_rdy_i : in std_logic;

			B_err_o  : out std_logic_vector(31 downto 0)
		);
	end component;

	component BCalcul3_8
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;

			--Control input
			start_i	: in std_logic;
			init_Bk_i: in std_logic;
			C0_i		: in std_logic;

			int_range_sel_i : in std_logic_vector(3 downto 0);
			int_c0_used_i   : in std_logic;

			--Input information signals
			low_marker_i  : in std_logic;
			high_marker_i : in std_logic;

			--Marker values
			low_marker_val_i  : in std_logic_vector(31 downto 0);
			high_marker_val_i : in std_logic_vector(31 downto 0);

			--Data
			Bk_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			Vk_i			: in std_logic_vector(NB_BITS_ADC_VALUE-1 downto 0);
	--		Bsmooth_i	: in std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0);
						
			--Output
			B_error_low_o  : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_error_high_o : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);

			data_rdy_o	: out std_logic;
			BKp1_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0)
		);
	end component;

	component BCalculRect
		--Calcul :
		-- Bk_i+corr_fact_i*(Vk_i-Vo_i)+Bsmooth_i;
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;

			--Control input
			start_i	: in std_logic;
			init_Bk_i: in std_logic;
			int_range_sel_i : in std_logic_vector(3 downto 0);

			--Input information signals
			C0_i          : in std_logic;
			C0_used_i     : in std_logic;
			low_marker_i  : in std_logic;
			high_marker_i : in std_logic;

			--Marker values
			low_marker_val_i  : in std_logic_vector(31 downto 0);
			high_marker_val_i : in std_logic_vector(31 downto 0);

			soft_fixed_i    : in std_logic;
			soft_val_i      : in std_logic_vector(31 downto 0);

			--Data
			Bk_i			: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			Bsmooth_i	: in std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0);
			corr_fact_i	: in std_logic_vector(31 downto 0);
			Vk_i			: in std_logic_vector(NB_BITS_ADC_VALUE-1 downto 0);
			Vo_i			: in std_logic_vector(31 downto 0);
						
			--Output
			data_rdy_o	   : out std_logic;
			fifo_wen_o	   : out std_logic;
			B_error_low_o  : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_error_high_o : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			BKp1_o		   : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0)
		);
	end component;

	component BTrain_integral 
		port (
			clk_i			: in std_logic;
			reset_i		: in std_logic;
		
			adc_data_i		: in std_logic_vector(17 downto 0);
			adc_data_rdy_i	: in std_logic;

			init_Bk_i		 : in std_logic;
			int_c0_used_i   : in std_logic;
			int_range_sel_i : in std_logic_vector(3 downto 0);

			soft_fixed_i    : in std_logic;
			soft_val_i      : in std_logic_vector(31 downto 0);

			--Register	
			reg_val_cfg_sel_integral_i : in std_logic_vector(1 downto 0);
			
			Bsmooth_i		: in std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0);
			corr_fact_i		: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			Vo_i				: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);

			--Input information signals
			low_marker_i  : in std_logic;
			high_marker_i : in std_logic;
			
			--Marker values
			low_marker_val_i  : in std_logic_vector(31 downto 0);
			high_marker_val_i : in std_logic_vector(31 downto 0);

			C0_i				: in std_logic;
			
			--Output
			B_error_low_o  : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_error_high_o : out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			BKp1_o			: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			B_data_rdy_o	: out std_logic
		);
	end component;

	component mux_integral 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			data_rdy_3_8_i	: in std_logic;
			BKp1_3_8_i		: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);

			data_rdy_rect_i	: in std_logic;
			BKp1_rect_i		: in std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
			
			reg_val_cfg_sel_integral_i : in std_logic_vector(1 downto 0);
			
			--Output
			data_rdy_o	: out std_logic;
			BKp1_o		: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0)
		);		
	end component;

	component Bdot 
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;

			--Control input
			start_i	: in std_logic;

			--Data
			corr_fact_i	: in std_logic_vector(31 downto 0);
			Vk_i			: in std_logic_vector(NB_BITS_ADC_VALUE-1 downto 0);
			Vo_i			: in std_logic_vector(31 downto 0);
						
			--Output
			data_rdy_o	: out std_logic;
			Bdot_o		: out std_logic_vector(NB_BITS_B_VALUE-1 downto 0)
		);
	end component;

end package integral_pkg;

