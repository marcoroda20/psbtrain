----------------------------------------------------------------------------------
-- Company: CERN TE/MSC-MM
-- Engineer: David Giloteaux
-- 
-- Create Date:    14:02:29 11/19/2012 
-- Design Name: 
-- Module Name:    LCD4x40_Interface - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 40x4 LCD interface 
--              
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.MATH_REAL.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

library work;
use work.utils_pkg.all;

entity LCD4x40_Interface is

    Port ( Fraw_ADC : in STD_LOGIC_VECTOR (17 downto 0); -- F-ADC values without calibration
           Draw_ADC : in STD_LOGIC_VECTOR (17 downto 0); -- D-ADC values without calibration
			  Fcal_ADC : in STD_LOGIC_VECTOR (17 downto 0); -- F-ADC values with calibration
           Dcal_ADC : in STD_LOGIC_VECTOR (17 downto 0); -- D-ADC values with calibration
			  Fraw_DRDY, Draw_DRDY, Fcal_DRDY, Dcal_DRDY : in STD_LOGIC; -- F&D Data ready for cal and uncal ADC
			  F_LIM : in STD_LOGIC_VECTOR (17 downto 0); -- F : +/- ADC input limits
			  D_LIM : in STD_LOGIC_VECTOR (17 downto 0); -- D : +/- ADC input limits
			  F_PREF, D_PREF, F_NREF, D_NREF : in STD_LOGIC_VECTOR (17 downto 0); -- F&D positive and negative acquired values after +/-Vdac 
			  FL_Wwid, FH_Wwid, DL_Wwid, DH_Wwid : in STD_LOGIC_VECTOR (31 downto 0); -- F&D High & Low window widths
			  FL_Wdly, FH_Wdly, DL_Wdly, DH_Wdly : in STD_LOGIC_VECTOR (31 downto 0); -- F&D High & Low window delays
			  FL_Twid, FH_Twid, DL_Twid, DH_Twid : in STD_LOGIC_VECTOR (31 downto 0); -- F&D High & Low trigger delays
           F_GCC : in STD_LOGIC_VECTOR (31 downto 0); -- F-gain calibration coefficient
           D_GCC : in STD_LOGIC_VECTOR (31 downto 0); -- D-gain calibration coefficient
			  F_OCC : in STD_LOGIC_VECTOR (17 downto 0); -- F-offset calibration coefficient
			  D_OCC : in STD_LOGIC_VECTOR (17 downto 0); -- D-offset calibration coefficient
			  C_DAC : in STD_LOGIC_VECTOR (19 downto 0); -- Calibration DAC in volt (read register) 
			  F_OVR, D_OVR : in STD_LOGIC; -- F & D ADC over-range
           AF_COIL : in STD_LOGIC_VECTOR (31 downto 0); -- F coil area
			  AD_COIL : in STD_LOGIC_VECTOR (31 downto 0); -- D coil area
           KF_BMEAS : in STD_LOGIC_VECTOR (31 downto 0); -- F-Ponderation coefficient for Bmeas
			  KD_BMEAS : in STD_LOGIC_VECTOR (31 downto 0); -- D-Ponderation coefficient for Bmeas
			  KF_BDOT : in STD_LOGIC_VECTOR (31 downto 0); -- F-Ponderation coefficient for Bdot 
			  KD_BDOT : in STD_LOGIC_VECTOR (31 downto 0); -- D-Ponderation coefficient for Bdot
           B_DOT : in STD_LOGIC_VECTOR (31 downto 0); -- dB/dt 
           B_DOT_DRDY : in STD_LOGIC;
			  B_MEAS : in STD_LOGIC_VECTOR (31 downto 0); -- Measured magnetic field
			  B_MEAS_DRDY : in STD_LOGIC;
			  B_HOLD_TIME : in STD_LOGIC_VECTOR (31 downto 0); -- Latched magnetic field at C0 + B_HOLD_TIME (in ms)
			  B_HOLD : in STD_LOGIC_VECTOR (31 downto 0); -- Latched magnetic field
			  B_THRESHOLD : in STD_LOGIC_VECTOR (31 downto 0); -- Threshold of the latched magnetic field
			  Bdot_HOLD : in STD_LOGIC_VECTOR (31 downto 0); -- Latched magnetic field
			  Bdot_THRESHOLD : in STD_LOGIC_VECTOR (31 downto 0); -- Threshold of the latched magnetic field
			  Z0_DLY : in STD_LOGIC_VECTOR (31 downto 0); -- Delay apply to the zero cycle pulse
			  BCD_COUNTDOWN : in STD_LOGIC_VECTOR (31 downto 0); -- Calibration time countdown in BCD format
			  WR_UPD : in STD_LOGIC_VECTOR (31 downto 0); -- White Rabbit update rate 
			  CAL_STATUS : in STD_LOGIC_VECTOR (3 downto 0); -- Calibration status
			  CAL_ERROR : in STD_LOGIC_VECTOR (3 downto 0); -- Calibration error
           LCD_RD_NWR : out STD_LOGIC; -- LCD read/write signal (not used) always in write mode
			  LCD_REG_SEL : out STD_LOGIC; -- LCD register select signal
           LCD_ENABLE1 : out STD_LOGIC; -- Line 1 & 2 LCD enable signal
           LCD_ENABLE2 : out STD_LOGIC; -- Line 3 & 4 LCD enable signal
			  LCD_SDOUT : out STD_LOGIC := '0'; -- LCD Data in serial (SPI transmission)
			  LCD_NCS : out STD_LOGIC; -- LCD Data chip select (SPI transmission)
			  LCD_CLK : out STD_LOGIC; -- LCD clock (SPI transmission)
			  OPER_SPARE : in STD_LOGIC; -- Operational/ Spare syatem selection 
			  EFF_SIMUL : in STD_LOGIC; -- Effective / Simulated magnetic field
           CLK : in STD_LOGIC; -- 10MHz
           CLK_100MHz_i : in STD_LOGIC; -- 100MHz
			  RESET : in STD_LOGIC := '1'; -- Active high
           NKEY1 : in STD_LOGIC;   -- Key n1 keypad
           NKEY2 : in STD_LOGIC;   -- Key n2 keypad
           NKEY3 : in STD_LOGIC;   -- Key n3 keypad
           NKEY4 : in STD_LOGIC ); -- Key n4 keypad
			  
end LCD4x40_Interface;

architecture Behavioral of LCD4x40_Interface is

-----------* Implements an continuous overwrite Line1 -> Line2 ->Line3...---------------------------
-----------* No effort made to read from the LCD controller.----------------------------------------
-----------* All is based on delays supposed to match execution time--------------------------------
-----------* from the data sheet (the LCD busy flag is not tested). --------------------------------
-----------* A FormFeed (0Ch = Ctrl-L) clears the screen. ------------------------------------------
-----------* Possible enhancements : interpreting other ASCII codes as commands --------------------

type State_type is (Boot,        Initial, Display, Entrymode, Clear,
                    Address_set, Waiting, Verify,  Putchar,   Homecursor );
signal State : State_type;

-- Function set for 8-bit data transfer and 2-line display
constant SET   : std_logic_vector(7 downto 0) := "00111000";  -- 0011 N F xx

-- Display ON, with cursor.**I set the last digit to zero so the cursor does not blink**
constant DON   : std_logic_vector(7 downto 0) := "00001100";

-- Set Entry Mode to increment cursor automatically after each character displayed.
--**If the last digit is set to 1, then the whole display is shifted to the left.**
constant SEM   : std_logic_vector(7 downto 0) := "00000110";

-- Clear screen command.
constant CLR   : std_logic_vector(7 downto 0) := "00000001";

-- DD RAM address set
constant DDRAS : std_logic_vector(7 downto 0) := "10000000";

-- Home cursor
constant HOME1 : std_logic_vector(7 downto 0) := "00000010"; -- set addr to Beg of line 1 or line 3
constant HOME2 : std_logic_vector(7 downto 0) := "11000000"; -- set addr to Beg of line 2 or line 4

-- Timing Constants :

constant Big_delay    : integer := 7500;  -- 30 ms (7500 x 4usec = 30ms)
constant Clr_delay    : integer := 500;   -- 2 ms (500 x 4usec = 2ms)
constant Small_delay  : integer := 12;    -- > 39 us after E down
constant En_delay     : integer := 2;

constant CST_CNT_LCD           : integer := 40;
constant CST_CNT_STATE_MACHINE : integer := CST_CNT_LCD/4;
constant CST_CNT_REFRESH       : integer := CST_CNT_LCD*20;

component mean_pow2
	generic(
		 NB_BIT : integer := 18;  -- default: 18 bits
		 POW2SAMPLE : integer := 7  -- default: 2^7 sample
	);
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		val_i     : in std_logic_vector(NB_BIT-1 downto 0);
		new_val_i : in std_logic;
		
		val_o   : out std_logic_vector(NB_BIT-1 downto 0);
		rdy_o   : out std_logic
	);
end component;

--Signal
signal div1, en_tff1, CLK_1MHz   : std_logic :='0';
signal clk1_1MHz, clk2_1MHz, tick_rise_1MHz, tick_fall_1MHz : std_logic :='0';
signal div2, en_tff2, clk_250KHz, clk1_250KHz, clk2_250KHz : std_logic :='0';
signal div3, en_tff3, clk_12KHz5, clk1_12KHz5, clk2_12KHz5 : std_logic :='0';
signal tick_rise_250KHz, tick_rise_12KHz5, tick_12KHz5 : std_logic :='0';

subtype divtype100 is natural range 0 to CST_CNT_STATE_MACHINE-1; -- 0 to n-1 = 0 to 100-1=99
subtype divtype400 is natural range 0 to CST_CNT_LCD-1; -- 0 to n-1 = 0 to 400-1=399
subtype divtype8000 is natural range 0 to CST_CNT_REFRESH-1; -- 0 to n-1 = 0 to 8000-1=7999
-- This subtype will constrain counter width. 
-- Tell to the synthesizer how many bits wide the generated counter should be.
signal cnt100: divtype100;
signal cnt400: divtype400;
signal cnt8000: divtype8000;		  

signal DavM, LcdRegSel  : std_logic :='0';
signal DataM, lcd_din, lcd_dout : std_logic_vector (7 downto 0):= x"00";
signal char_pos : std_logic_vector (6 downto 0):= "0000000";
signal sel_cst_data : std_logic_vector (10 downto 0):= x"00" & "000";
signal bin_word : std_logic_vector (23 downto 0):= x"000000";
signal bcd_1, bcd_10, bcd_100, bcd_1000, bcd_10000, bcd_100000, bcd_1000000, bcd_10000000 : std_logic_vector (3 downto 0):= x"0";
signal bcd1, bcd10, bcd100, bcd1000, bcd10000, bcd100000, bcd1000000, bcd10000000 : std_logic_vector (3 downto 0):= x"0";
signal bcd_rdy_s, bcd1_rdy, bcd2_rdy, end_bin2bcd : std_logic;

signal sel_cnt, sel_data_mux : std_logic_vector(2 downto 0):= "000";
signal Position : integer range 0 to 80; -- two lines of 40 characters
signal Count    : integer range 0 to Big_delay;

signal Lcd_Busy, Lcd_Enable, Mid_Line1, End_Line1, Mid_Line2, End_Line2, Pos_Zero : std_logic :='0';
signal ml1_1, ml1_2, el1_1, el1_2, ml2_1, ml2_2, el2_1, el2_2, pz1, pz2, pz_fall : std_logic :='0';
signal start_ml1, start_ml2, start_en2, start_el1, ce_cnt_mux : std_logic :='0';
signal lb1, lb2, first_busy_falling, start_char_cnt, ce_char_cnt : std_logic :='0';
signal Dav, el1, el_1, strobe_en1, strobe_en2, en_pz, test2 : std_logic :='0';
signal nstrobe_en1, nstrobe_en2, n_strobe_en2, end_wr_page : std_logic :='0';

signal ce_reg1, ce_reg2, ce_reg : std_logic :='0';
signal sel_conv, conv_in, conv : std_logic_vector (26 downto 0):= "000" & x"000000";

signal flwdly, flwwid, fhwdly, fhwwid, dhwdly, dhwwid, dlwdly, dlwwid : std_logic_vector (26 downto 0):= "000" & x"000000";
signal fltwid, fhtwid, dltwid, dhtwid, z0dly : std_logic_vector (26 downto 0):= "000" & x"000000";

signal fadccal, dadccal, fadcraw, dadcraw, bdot, bmeas, flim, dlim, fpref, fnref, dpref, dnref, focc, docc, cdac, wrupd : std_logic_vector (26 downto 0):= "000" & x"000000";
signal bhold, bthres, bholdtime : std_logic_vector (26 downto 0):= "000" & x"000000";
signal bdothold, bdotthres : std_logic_vector (26 downto 0):= "000" & x"000000";
signal adc_cst, conv_cst, convcst, dac_cst, bmeas_cst, bdot_cst, tms_cst, tus_cst, tcalib_cst, wrupd_cst : std_logic_vector (35 downto 0):= x"000000000";
signal bholdtime_cst : std_logic_vector (35 downto 0):= x"000000000";
signal scale_conv : std_logic_vector (63 downto 0):= x"0000000000000000";
signal convsign : std_logic :='0';

signal ce1_reg, ce_mult, end_mult_s, end1_mult, end2_mult, latch_mult : std_logic := '0';--Mult36x36
signal latch1_mult, latch_bin2bcd : std_logic := '0';--Mult36x36
signal conv_in_36b_s : std_logic_vector(35 downto 0) := x"000000000";--Mult36x36
signal scale_conv_72b_s : std_logic_vector(71 downto 0):= x"000000000000000000";--Mult36x36

signal dmm_ascii, mm_ascii, cm_ascii, dm_ascii : std_logic_vector (7 downto 0):= x"30";-- 0 character
signal m_ascii, c_ascii, d_ascii, u_ascii : std_logic_vector (7 downto 0):= x"30"; -- 0 character
signal dmmascii, mmascii, cmascii, dmascii : std_logic_vector (7 downto 0):= x"30";-- 0 character
signal mascii, cascii, dascii, uascii : std_logic_vector (7 downto 0):= x"30"; -- 0 character

signal char_sel, conv_sign : std_logic_vector (7 downto 0):= x"00";
signal ascii_P1L12, ascii_P1L34 : std_logic_vector (7 downto 0):= x"20"; -- blank character
signal ascii_P2L12, ascii_P2L34 : std_logic_vector (7 downto 0):= x"20"; -- blank character
signal ascii_P3L12, ascii_P3L34 : std_logic_vector (7 downto 0):= x"20"; -- blank character
signal ascii_P4L12, ascii_P4L34 : std_logic_vector (7 downto 0):= x"20"; -- blank character

signal ascii_P1_1L12, ascii_P1_1L34 : std_logic_vector (7 downto 0):= x"20"; -- blank character
signal ascii_P2_1L12, ascii_P2_1L34 : std_logic_vector (7 downto 0):= x"20"; -- blank character
signal ascii_P3_1L12, ascii_P3_1L34 : std_logic_vector (7 downto 0):= x"20"; -- blank character
signal ascii_P4_1L12, ascii_P4_1L34 : std_logic_vector (7 downto 0):= x"20"; -- blank character

signal F_y_or_blank, F_e_or_n, F_s_or_o : std_logic_vector (7 downto 0):= x"20"; -- blank character
signal D_y_or_blank, D_e_or_n, D_s_or_o : std_logic_vector (7 downto 0):= x"20"; -- blank character

signal M_or_S, E_or_I, A_or_M, S_or_U : std_logic_vector (7 downto 0):= x"20"; -- blank character   

signal o_or_s, e_or_a, a_or_e, t_or_blank, i_or_blank : std_logic_vector (7 downto 0):= x"20"; -- blank character 
signal o_or_blank, n_or_blank, a_or_blank, l_or_blank : std_logic_vector (7 downto 0):= x"20"; -- blank character 

signal wwaae, aappx, iippt, ttllr, bk_bk_yya, tz_bk_bk_c : std_logic_vector (7 downto 0):= x"20"; -- blank character  
signal ie_p_m_t, mrVV_bk, eoddo, bk_bk_aaf : std_logic_vector (7 downto 0):= x"20"; -- blank character     
signal ecccf, ly_bk_bk_s, ac_bk_bk_e, pl_bk_bk_t : std_logic_vector (7 downto 0):= x"20"; -- blank character     
signal se_bk_bk_bk, e_bk_bk_bk_bk, d_bk_bk_bk_bk : std_logic_vector (7 downto 0):= x"20"; -- blank character   

signal nffdd, o_4m, bk_ogog, efafa, rfifi, rsnsn, oe_bk_e_bk : std_logic_vector (7 downto 0):= x"20"; -- blank character    
signal rtete, s_bk_r_bk_r, bk_erer, bk_roro, bk_rrrr, bk_o_bk_o_bk, bk_r_bk_r_bk : std_logic_vector (7 downto 0):= x"20"; -- blank character    

signal sel_l12_l34 : std_logic :='0';

signal P1_ascii, P2_ascii, P3_ascii, P4_ascii, Px_ascii : std_logic_vector (7 downto 0):= x"20";
signal P1_1_ascii, P2_1_ascii, P3_1_ascii, P4_1_ascii  : std_logic_vector (7 downto 0):= x"20";

signal sel_page : std_logic_vector (7 downto 0):= x"02";
signal sel_Px, sel_sub_px  : std_logic_vector (3 downto 0):= x"0";

signal key1, key2, key3, key4, rkey1, rkey2, rkey3, rkey4 : std_logic :='0';
signal key1_1, key1_2, key2_1, key2_2, key3_1, key3_2, key4_1, key4_2 : std_logic :='0';
signal J1, J2, J3, J4, K1, K2, K3, K4, QJK1, QJK2, QJK3, QJK4 : std_logic :='0';
signal state1, state2, state3, state4, sel_px1, sel_px2, sel_px3, sel_px4 : std_logic :='0';
signal ewrp1, ewrp2, ce_key : std_logic :='0';

signal fractpart, gcc0, gcc1, gcc2, gcc3, gcc4, gcc5 : std_logic_vector (31 downto 0):= x"00000000";
signal gcc0x2, gcc1x2, gcc2x2, gcc3x2, gcc4x2, gcc5x2 : std_logic_vector (35 downto 0):= x"000000000";
signal gcc0x8, gcc1x8, gcc2x8, gcc3x8, gcc4x8, gcc5x8 : std_logic_vector (35 downto 0):= x"000000000";
signal gcc0x10, gcc1x10, gcc2x10, gcc3x10, gcc4x10, gcc5x10 : std_logic_vector (35 downto 0):= x"000000000";
signal bcd1_10, bcd1_100, bcd1_1000, bcd1_10000, bcd1_100000, bcd1_1000000 : std_logic_vector (3 downto 0):= x"0";


signal Vfiraw, Vdiraw, Vforaw, Vdoraw, k, freg_adc, fadcraw_filt, dadcraw_filt : std_logic_vector (17 downto 0):= "00" & x"0000";
signal Mfraw, Mdraw, Vforaw_Out, Vdoraw_Out : std_logic_vector (35 downto 0):= x"000000000";

signal Vfical, Vdical, Vfocal, Vdocal, fadccal_filt, dadccal_filt : std_logic_vector (17 downto 0):= "00" & x"0000";
signal Mfcal, Mdcal, Vfocal_Out, Vdocal_Out : std_logic_vector (35 downto 0):= x"000000000";
signal Frawdrdy, Ffilt_DRDY, Drawdrdy, Dfilt_DRDY : std_logic :='0';
signal Fcaldrdy, Fcal_filt_DRDY, Dcaldrdy, Dcal_filt_DRDY : std_logic :='0';

signal B_DOT_filt       : STD_LOGIC_VECTOR (31 downto 0); -- dB/dt 
signal B_DOT_filt_DRDY  : STD_LOGIC;
signal B_MEAS_filt      : STD_LOGIC_VECTOR (31 downto 0); -- Measured magnetic field
signal B_MEAS_filt_DRDY : STD_LOGIC;

signal stopcnt, stop_cnt, spi_cs, ld_p2s, rst_p2s, lcd_cs : std_logic :='0';
signal mode : std_logic_vector (1 downto 0):= "00";
signal gate8_cnt : std_logic_vector (3 downto 0):= x"0";
signal idata : std_logic_vector (7 downto 0):= x"00";

signal lcd1_e1, lcd2_e1, lcd3_e1, lcd4_e1, lcd5_e1, lcdenable1 : std_logic := '0';
signal lcd1_e2, lcd2_e2, lcd3_e2, lcd4_e2, lcd5_e2, lcdenable2 : std_logic := '0';
signal lcd_en, rst1, rst2, load1, st1, st2, start_spi, init_rst, rclk : std_logic := '0';

signal page1_1, page2_1, page3_1, page4_1 : std_logic := '0';

signal sec_lsb, sec_msb, min_lsb, min_msb   : std_logic_vector(7 downto 0) := x"20";
signal hour_lsb, hour_msb, day_lsb, day_msb : std_logic_vector(7 downto 0) := x"20";

signal us_ms_bht, us_ms_wru : std_logic := '0';
signal uorm_wru, uorm_bht : std_logic_vector(7 downto 0):= x"20";

attribute INIT : string; --string type if you want to INIT a bus
attribute INIT of QJK2: signal is "1"; -- Displays LCD Page 2 at initialization

attribute keep : string;
attribute keep of conv_in_36b_s : signal is "TRUE";
attribute keep of scale_conv_72b_s : signal is "TRUE";
attribute keep of ce_reg : signal is "TRUE";
attribute keep of end_mult_s : signal is "TRUE";
attribute keep of bin_word : signal is "TRUE";

attribute keep of nstrobe_en1 : signal is "TRUE";
attribute keep of nstrobe_en2 : signal is "TRUE";
attribute keep of State : signal is "TRUE";

attribute keep of fadcraw_filt : signal is "TRUE";
attribute keep of Ffilt_DRDY : signal is "TRUE";

begin

----------------------------------------  100,  400 and  8000 clock dividers ----------------------------------------			 

process(CLK)
begin
   if rising_edge(CLK) then 				
	   if ( RESET = '1' ) then			                                 
           cnt100  <= 0;    -- n - 1 = 100 - 1 = 99              
			  cnt400  <= 0;    -- n - 1 = 400 - 1 = 399
			  cnt8000 <= 0; -- n - 1 = 8000 - 1 = 7999
			  div1    <= '0';
			  div2    <= '0';
			  div3    <= '0';
      else
		    if (cnt100 >= CST_CNT_STATE_MACHINE-1) then    -- Counter count to 99 on the rising edge of CLK
		        cnt100 <= 0;          
		    else 
		        cnt100 <= cnt100 + 1;
			 end if;
			 
			 if (cnt400 >= CST_CNT_LCD-1) then  -- Counter count to 399 on the rising edge of CLK
		        cnt400 <= 0;          
		    else 
		        cnt400 <= cnt400 + 1;
          end if;
			 
			 if (cnt8000 >= CST_CNT_REFRESH-1) then  -- Counter count to 7999 on the rising edge of CLK
				  cnt8000 <= 0;          
		    else 
				  cnt8000 <= cnt8000 + 1;
          end if;
			 if (en_tff1 = '1') then 
		        div1 <= not(div1);
	       end if;
			 if (en_tff2 = '1') then 
		        div2 <= not(div2);
          end if;
			 if (en_tff3 = '1') then 
			     div3 <= not(div3);
          end if;
       end if;
   end if;
end process;

-- Enable the Toggle Flip-Flop Div1, Div2 and Div3

en_tff1 <= '1' when ( ( cnt100 = 0 ) or ( cnt100 = CST_CNT_STATE_MACHINE/2 ) ) else '0'; -- 0 or n/2 => 0 or 100 /2
en_tff2 <= '1' when ( ( cnt400 = 0 ) or ( cnt400 = CST_CNT_LCD/2 ) ) else '0'; -- 0 or n/2 => 0 or 400 /2
en_tff3 <= '1' when ( ( cnt8000 = 0 ) or ( cnt8000 = CST_CNT_REFRESH/2 ) ) else '0'; -- 0 or n/2 => 0 or 8000 /2

CLK_1MHz   <= div1;
clk_250KHz <= div2;
clk_12KHz5 <= div3;

----------------------------------- Tick 1MHz rise & fall, Tick 250KHz rise and Tick 12KH5 rise-----------------------------------------

process (CLK)
begin
	if rising_edge(CLK) then
		if (RESET='1') then
			clk1_1MHz     <= '0';	   
			clk2_1MHz     <= '0';
			clk1_250KHz   <= '0';
			clk2_250KHz   <= '0';
			clk1_12KHz5   <= '0';
			clk2_12KHz5   <= '0';
		else
			clk1_1MHz <= clk_1MHz;		
			clk2_1MHz <= clk1_1MHz;
			clk1_250KHz <= clk_250KHz;
			clk2_250KHz <= clk1_250KHz;
			clk1_12KHz5 <= clk_12KHz5;
			clk2_12KHz5 <= clk1_12KHz5;			
		end if;
	end if;
	if falling_edge(CLK) then 
		if (RESET='1') then
			tick_rise_1MHz   <= '0';	
			tick_fall_1MHz   <= '0';
         tick_rise_250KHz <= '0';
         tick_rise_12KHz5 <= '0';			
		else
			tick_rise_1MHz   <= clk1_1MHz and ( not clk2_1MHz );
			tick_fall_1MHz   <= ( not clk1_1MHz ) and clk2_1MHz;
         tick_rise_250KHz <= clk1_250KHz and ( not clk2_250KHz );
         tick_rise_12KHz5 <= clk1_12KHz5 and ( not clk2_12KHz5 );					         			
		end if;
	end if;
end process;

------------------------------------ LCD state machine ---------------------------------------------
process (CLK)
begin
    
    if rising_edge(CLK) then 
		 if RESET = '1' then
		  	 State        <= Boot;
			 Position     <= 0;
			 Count        <= 0;
			 DavM         <= '0';
			 Lcd_Enable   <= '0';
			 LcdRegSel    <= '1';
			 Lcd_Busy     <= '1';
			 lcd_dout     <= (others=>'0');
			 DataM        <= x"00";
	 else
			 if ( Dav = '1' and tick_rise_1MHz = '1' ) then
					DavM  <= '1';
					DataM <= lcd_din;
					Lcd_Busy  <= '1';
			 end if;
			 
			 if tick_rise_250KHz = '1' then	 
        
		    case State is

          when Boot =>                  -- Power up wait 30 ms
            LcdRegSel    <= '1';
            Lcd_Enable   <= '0';
            Lcd_Busy <= '1';
            if Count >= Big_delay then
              LcdRegSel <= '0';
              Count     <= 0;
              State     <= Initial;
            else
              Count <= Count + 1;
            end if;

          when Initial =>               -- Set Function & Mode
            lcd_dout  <= SET;
            LcdRegSel <= '0';
            if Count = 1 then
              Lcd_Enable <= '1';
            elsif Count >= En_delay then
              Lcd_Enable <= '0';
            end if;
            if Count >= Small_delay then
              State <= EntryMode;
              Count <= 0;
            else
              Count <= Count+1;
            end if;

          when EntryMode =>             -- set Entry Mode,
            lcd_dout  <= SEM;
            LcdRegSel <= '0';
            if Count = 1 then
              Lcd_Enable <= '1';
            elsif Count >= En_delay then
              Lcd_Enable <= '0';
            end if;
            if Count >= Small_delay then
              State <= Display;
              Count <= 0;
            else
              Count <= Count+1;
            end if;

          when Display =>               -- set Display ON
            lcd_dout    <= DON;
            LcdRegSel   <= '0';
            if Count = 1 then
              Lcd_Enable <= '1';
            elsif Count >= En_delay then
              Lcd_Enable <= '0';
            end if;
            if Count >= Small_delay then
              State <= Clear;
              Count <= 0;
            else
              Count <= Count+1;
            end if;

          when Clear =>                 -- Clear the screen
            lcd_dout  <= CLR;
            Position  <= 0;
            LcdRegSel <= '0';
            if Count = 1 then
              Lcd_Enable <= '1';
            elsif Count >= En_delay then
              Lcd_Enable <= '0';
            end if;
            if Count >= Clr_delay then
              Count <= 0;
              State <= Address_set;
            else
              Count <= Count+1;
            end if;

          when Address_set =>           -- DD RAM address set
            lcd_dout  <= DDRAS;
            LcdRegSel <= '0';
            if Count = 1 then
              Lcd_Enable <= '1';
            elsif Count >= En_delay then
              Lcd_Enable <= '0';
            end if;
            if Count >= Small_delay then
              State <= Waiting;
              Count <= 0;
            else
              Count <= Count+1;
            end if;

          when Waiting =>               -- Waits for input
            if DavM = '1' then
                 DavM      <= '0';
                 State     <= Verify;
                 LcdRegSel <= '1';
            else
                 Lcd_Busy <= '0';
            end if;

          when Verify =>
              if DataM = x"0C" then     -- FormFeed => Clear Screen
                State <= Clear;
              elsif Position = 40 then  -- end of line 1
                lcd_dout <= HOME2;
                State    <= HomeCursor;
              elsif Position = 80 then  -- end of line 2
                Position <= 0;
                lcd_dout <= HOME1;
                State    <= HomeCursor;
              else
                lcd_dout <= DataM;
                State    <= Putchar;
              end if;

          when Putchar =>               -- Display the character on the LCD
            LcdRegSel <= '1';
            if Count = 1 then
              Lcd_Enable <= '1';
            elsif Count >= En_delay then
              Lcd_Enable <= '0';
            end if;
            if Count >= Small_delay then
              Position <= Position + 1;
              Count    <= 0;
              State    <= Waiting;
            else
              Count <= Count + 1;
            end if;

          when HomeCursor =>
            LcdRegSel <= '0';
            if Count = 1 then
              Lcd_Enable <= '1';
            elsif Count >= En_delay then
              Lcd_Enable <= '0';
            end if;
            if Count >= Clr_delay then
              lcd_dout <= DataM;
              State    <= Putchar;
              Count    <= 0;
            else
              Count <= Count+1;
            end if;

        end case;
      end if;
    end if;
 end if;
end process;
  
Mid_Line1 <= '1' when Position = 20 else '0';
End_Line1 <= '1' when Position = 40 else '0';
Mid_Line2 <= '1' when Position = 60 else '0';
End_Line2 <= '1' when Position = 80 else '0';
Pos_Zero  <= '1' when Position = 0  else '0';

LCD_RD_NWR <= '0'; -- LCD output : always in Write mode...

char_pos <= conv_std_logic_vector(Position, 7);

process (CLK)
begin    
	 if CLK'event and CLK = '1' then
		if ( RESET='1') then
				ml1_1 <= '0';
				ml1_2 <= '0'; 
				el1_1 <= '0';
				el1_2 <= '0';
				ml2_1 <= '0';
				ml2_2 <= '0'; 
				el2_1 <= '0';
				el2_2 <= '0';
				pz1 <=   '0';
				pz2 <=   '0';
		elsif tick_fall_1MHz = '1' then
				ml1_1 <= Mid_Line1;
				ml1_2 <= ml1_1; 
				el1_1 <= End_Line1;
				el1_2 <= el1_1;
				ml2_1 <= Mid_Line2;
				ml2_2 <= ml2_1; 
				el2_1 <= End_Line2;
				el2_2 <= el2_1;
				pz1 <= Pos_Zero;
				pz2 <= pz1;
		end if;
	 end if;
end process;

start_en2 <= el2_1 and ( not el2_2 );
start_ml1 <= ml1_1 and ( not ml1_2 );
start_el1 <= el1_1 and ( not el1_2 );
start_ml2 <= ml2_1 and ( not ml2_2 );
ce_cnt_mux <= start_el1 or start_ml1 or start_en2 or start_ml2;
pz_fall <= ( not pz1 ) and pz2;

process (CLK)
begin    
	 if CLK'event and CLK = '1' then
		 if (RESET='1') then
			  lb1   <= '0';
			  lb2   <= '0';
		 elsif tick_rise_1MHz = '1' then
			  lb1   <= Lcd_Busy;
			  lb2   <= lb1;
		 end if;
	 end if;
end process;

first_busy_falling <= ( ( not lb1 ) and lb2 ) and ( not LcdRegSel );

process (CLK) 
begin	
   if CLK'event and CLK = '1' then
	   if (RESET='1') then
		   sel_cnt <= "000";
      elsif ( ce_cnt_mux ='1' and tick_rise_1MHz = '1' ) then
         sel_cnt <= sel_cnt + 1;
      end if;
   end if;
end process;

sel_data_mux <= sel_cnt;

process (CLK)
begin	
	if CLK'event and CLK = '1' then
		if (RESET='1') then
			 en_pz <= '0';
		elsif ( pz_fall = '1' and tick_rise_1MHz = '1' ) then
		    en_pz <= pz_fall;
		end if;
	end if;
end process;

test2 <= Pos_Zero and en_pz;

LCD_REG_SEL <= LcdRegSel; -- Output for LCD display

process (CLK)
begin
	if CLK'event and CLK = '1' then
	    if ( RESET='1' ) then
		    start_char_cnt <= '0';
	    elsif ( first_busy_falling = '1' and tick_fall_1MHz = '1' ) then
		    start_char_cnt <= first_busy_falling;
		 end if;
	end if;
end process;

ce_char_cnt <= tick_rise_12KHz5 and start_char_cnt; --OK

process (CLK)
begin
	if CLK'event and CLK = '1' then
	    if ( RESET='1' ) then
		    el1 <= '0';
		    el_1 <= '0';
		 else
	       if ( ( ce_char_cnt and tick_rise_1MHz ) = '1' ) then
		         el1 <= End_Line1;
	       end if;
			 if tick_rise_1MHz = '1' then
		         el_1 <= el1;
			 end if;
		 end if;
	end if;
end process;	

Dav <= ( not el_1 ) and ce_char_cnt and ( not test2 );  

process (CLK)
begin
	if CLK'event and CLK = '1' then
	   if ( RESET='1' ) then
		    strobe_en1 <= '0';
	   elsif ( ( start_en2 and tick_rise_1MHz ) = '1' ) then
		    strobe_en1 <= not strobe_en1;
		end if;
	end if;
end process;

nstrobe_en1 <= not strobe_en1;
nstrobe_en2 <= not strobe_en2;

lcdenable1 <= Lcd_Enable and nstrobe_en1; 

process (CLK)
begin	
	if CLK'event and CLK = '1' then
	    if ( RESET='1' ) then
		      strobe_en2 <= '0';
	    elsif ( ( start_en2 or first_busy_falling ) and tick_rise_1MHz ) = '1' then
		      strobe_en2 <= not strobe_en2;
		 end if;
	end if;
end process;

lcdenable2 <= Lcd_Enable and nstrobe_en2; 

process (CLK)
begin	
   if CLK'event and CLK = '1' then --Shift to compensate the serial transmission time
      if ( RESET='1' ) then
	        lcd_en  <= '0'; 
	        lcd1_e1 <= '0'; 
	        lcd2_e1 <= '0'; 
	        lcd3_e1 <= '0'; 
	        lcd4_e1 <= '0'; 
	        lcd5_e1 <= '0'; 
	        lcd1_e2 <= '0'; 
	        lcd2_e2 <= '0'; 
	        lcd3_e2 <= '0'; 
	        lcd4_e2 <= '0'; 
	        lcd5_e2 <= '0'; 
	    elsif ( tick_rise_1MHz and tick_rise_250KHz ) = '1' then -- was tick_250KHz before
		     lcd_en  <= lcdenable1 or lcdenable2; 
		     lcd1_e1 <= lcdenable1;
			  lcd2_e1 <= lcd1_e1;
           lcd3_e1 <= lcd2_e1;
			  lcd4_e1 <= lcd3_e1;
			  lcd5_e1 <= lcd4_e1;
			  lcd1_e2 <= lcdenable2;
			  lcd2_e2 <= lcd1_e2;
           lcd3_e2 <= lcd2_e2;
			  lcd4_e2 <= lcd3_e2;
			  lcd5_e2 <= lcd4_e2;
		  end if;
	 end if;
end process;

LCD_ENABLE1 <= lcd5_e1;-- Output for LCD display
LCD_ENABLE2 <= lcd5_e2;-- Output for LCD display

process (CLK)
begin    
	 if CLK'event and CLK = '1' then
	    if ( RESET='1') then
		   n_strobe_en2 <= '0';
		 elsif ( tick_rise_1MHz and tick_rise_250KHz ) = '1' then -- was tick_250KHz before
	      n_strobe_en2 <= nstrobe_en2;
		 end if;
	 end if;
end process;

end_wr_page <= n_strobe_en2 and nstrobe_en1;

---------------------------------- F raw or D raw ADC mean -----------------------------

cmp_mean_Fraw : mean_pow2
	generic map(
		 NB_BIT => 18,  -- 18 bits
		 POW2SAMPLE => 16  -- 2^16 samples
	)
	port map(
		clk_i		=> CLK_100MHz_i,
		reset_i	=> RESET,
		
		val_i     => Fraw_ADC,
		new_val_i => Fraw_DRDY,
		
		val_o   => fadcraw_filt,
		rdy_o   => Ffilt_DRDY
	);
cmp_mean_Draw : mean_pow2
	generic map(
		 NB_BIT => 18,  -- 18 bits
		 POW2SAMPLE => 16  -- 2^16 samples
	)
	port map(
		clk_i		=> CLK_100MHz_i,
		reset_i	=> RESET,
		
		val_i     => Draw_ADC,
		new_val_i => Draw_DRDY,
		
		val_o   => dadcraw_filt,
		rdy_o   => Dfilt_DRDY
	);
cmp_mean_Frawcal : mean_pow2
	generic map(
		 NB_BIT => 18,  -- 18 bits
		 POW2SAMPLE => 16  -- 2^16 samples
	)
	port map(
		clk_i		=> CLK_100MHz_i,
		reset_i	=> RESET,
		
		val_i     => Fcal_ADC,
		new_val_i => Fcal_DRDY,
		
		val_o   => fadccal_filt,
		rdy_o   => Fcal_filt_DRDY
	);
cmp_mean_Drawcal : mean_pow2
	generic map(
		 NB_BIT => 18,  -- 18 bits
		 POW2SAMPLE => 16  -- 2^16 samples
	)
	port map(
		clk_i		=> CLK_100MHz_i,
		reset_i	=> RESET,
		
		val_i     => Dcal_ADC,
		new_val_i => Dcal_DRDY,
		
		val_o   => dadccal_filt,
		rdy_o   => Dcal_filt_DRDY
	);

cmp_mean_B_meas : mean_pow2
	generic map(
		 NB_BIT => 32,  -- 32 bits
		 POW2SAMPLE => 16  -- 2^16 samples
	)
	port map(
		clk_i		=> CLK_100MHz_i,
		reset_i	=> RESET,
		
		val_i     => B_MEAS,
		new_val_i => B_MEAS_DRDY,
		
		val_o   => B_MEAS_filt,
		rdy_o   => B_MEAS_filt_DRDY
	);

cmp_mean_Bdot : mean_pow2
	generic map(
		 NB_BIT => 32,  -- 32 bits
		 POW2SAMPLE => 16  -- 2^16 samples
	)
	port map(
		clk_i		=> CLK_100MHz_i,
		reset_i	=> RESET,
		
		val_i     => B_DOT,
		new_val_i => B_DOT_DRDY,
		
		val_o   => B_DOT_filt,
		rdy_o   => B_DOT_filt_DRDY
	);

-----------------------------------18-Bit ADC : binary code normalized in volt----------------------
-----------------------------------20-Bit DAC : binary code normalized in volt----------------------

-- Normalized converters to 27-Bit

cdac <= C_DAC(19) & C_DAC(19) & C_DAC(19) & C_DAC(19) &
        C_DAC(19) & C_DAC(19) & C_DAC(19) & C_DAC;
		  
--bdot  <= B_DOT_filt(17) & B_DOT_filt(17) & B_DOT_filt(17) & B_DOT_filt(17) & 
         --B_DOT_filt(17) & B_DOT_filt(17) & B_DOT_filt(17) & B_DOT_filt(17) & B_DOT_filt(17) & B_DOT_filt;

fpref <= F_PREF(17) & F_PREF(17) & F_PREF(17) & F_PREF(17) & 
         F_PREF(17) & F_PREF(17) & F_PREF(17) & F_PREF(17) & F_PREF(17) & F_PREF;

fnref <= F_NREF(17) & F_NREF(17) & F_NREF(17) & F_NREF(17) & 
         F_NREF(17) & F_NREF(17) & F_NREF(17) & F_NREF(17) & F_NREF(17) & F_NREF;			


dpref <= D_PREF(17) & D_PREF(17) & D_PREF(17) & D_PREF(17) & 
         D_PREF(17) & D_PREF(17) & D_PREF(17) & D_PREF(17) & D_PREF(17) & D_PREF; 

dnref <= D_NREF(17) & D_NREF(17) & D_NREF(17) & D_NREF(17) &
         D_NREF(17) & D_NREF(17) & D_NREF(17) & D_NREF(17) & D_NREF(17) & D_NREF; 
			
focc <= F_OCC(17) & F_OCC(17) & F_OCC(17) & F_OCC(17) &
        F_OCC(17) & F_OCC(17) & F_OCC(17) & F_OCC(17) & F_OCC(17) & F_OCC;

docc <= D_OCC(17) & D_OCC(17) & D_OCC(17) & D_OCC(17) &
        D_OCC(17) & D_OCC(17) & D_OCC(17) & D_OCC(17) & D_OCC(17) & D_OCC;
		  
fadcraw <= fadcraw_filt(17) & fadcraw_filt(17) & fadcraw_filt(17) & fadcraw_filt(17) & 
           fadcraw_filt(17) & fadcraw_filt(17) & fadcraw_filt(17) & fadcraw_filt(17) & fadcraw_filt(17) & fadcraw_filt;

dadcraw <= dadcraw_filt(17) & dadcraw_filt(17) & dadcraw_filt(17) & dadcraw_filt(17) &
           dadcraw_filt(17) & dadcraw_filt(17) & dadcraw_filt(17) & dadcraw_filt(17) & dadcraw_filt(17) & dadcraw_filt;

fadccal <= fadccal_filt(17) & fadccal_filt(17) & fadccal_filt(17) & fadccal_filt(17) & 
           fadccal_filt(17) & fadccal_filt(17) & fadccal_filt(17) & fadccal_filt(17) & fadccal_filt(17) & fadccal_filt;

dadccal <= dadccal_filt(17) & dadccal_filt(17) & dadccal_filt(17) & dadccal_filt(17) & 
			  dadccal_filt(17) & dadccal_filt(17) & dadccal_filt(17) & dadccal_filt(17) & dadccal_filt(17) & dadccal_filt;

flim <= F_LIM(17) & F_LIM(17) & F_LIM(17) & F_LIM(17) &
        F_LIM(17) & F_LIM(17) & F_LIM(17) & F_LIM(17) & F_LIM(17) & F_LIM;

dlim <= D_LIM(17) & D_LIM(17) & D_LIM(17) & D_LIM(17) & 
        D_LIM(17) & D_LIM(17) & D_LIM(17) & D_LIM(17) & D_LIM(17) & D_LIM;

z0dly  <= '0' & Z0_DLY(25 downto 0); 

flwdly <= '0' & FL_Wdly(25 downto 0); 
flwwid <= '0' & FL_Wwid(25 downto 0);

fltwid <= "00000000000" & FL_Twid(15 downto 0);

fhwdly <= '0' & FH_Wdly(25 downto 0); 
fhwwid <= '0' & FH_Wwid(25 downto 0);
 
fhtwid <= "00000000000" & FH_Twid(15 downto 0); 

dlwdly <= '0' & DL_Wdly(25 downto 0); 
dlwwid <= '0' & DL_Wwid(25 downto 0);

dltwid <= "00000000000" & DL_Twid(15 downto 0);

dhwdly <= '0' & DH_Wdly(25 downto 0); 
dhwwid <= '0' & DH_Wwid(25 downto 0);
 
dhtwid <= "00000000000" & DH_Twid(15 downto 0); 

--Max value B_MEAS_filt(31 downto 0) =(31FFFFCE)hex = 8.388.607,50 uT
bmeas  <= B_MEAS_filt(31 downto 5) + B_MEAS_filt(4);
bhold  <= B_HOLD(31 downto 5) + B_HOLD(4);
bthres <= B_THRESHOLD(31 downto 5) + B_THRESHOLD(4);
--Max value B_DOT_filt(31 downto 0) =(00FFFFFF)hex = 16.777.215 uT/s
bdot  <= B_DOT_filt(31) & B_DOT_filt(25 downto 0);
bdothold <= Bdot_HOLD(31) & Bdot_HOLD(25 downto 0);	
bdotthres <= Bdot_THRESHOLD(31) & Bdot_THRESHOLD(25 downto 0);	

process (CLK)
begin	
   if CLK'event and CLK = '1' then
	   if ( RESET='1' ) then
		    ce_reg1 <= '0';
		    ce_reg2 <= '0';
		    us_ms_bht <= '0';
			 bholdtime_cst <= x"000000000";
			 bholdtime <= "000" & x"000000";
			 us_ms_wru <= '0';
			 wrupd_cst <= x"000000000";
			 wrupd <= "000" & x"000000";
      else	
		    ce_reg1 <= first_busy_falling or ce_cnt_mux;
		    ce_reg2 <= ce_reg1;
			 --Max value of B_HOLD_TIME => (63FFFF9C)hex = 16,777215 sec
			 if ( B_HOLD_TIME > 1677721 ) then -- LSB max value : (2^24 -1 ) / 10ns
				us_ms_bht <= '1';
				bholdtime_cst <= x"00A3D70A4";-- round (64 x 0.01 x 2^28) in hex
				bholdtime <= '0' & B_HOLD_TIME(31 downto 6);
			 else
				us_ms_bht <= '0';
				bholdtime_cst <= x"0A0000000"; -- x 10ns
				bholdtime <= '0' & B_HOLD_TIME(25 downto 0);
			 end if;
			 --Max value of WR_UPD => (63FFFF9C)hex = 16,777215 sec
			 if ( WR_UPD > 1048575 ) then -- LSB max value : (2^24 -1 ) / 16ns
				us_ms_wru <= '1';
				wrupd_cst <= x"010624DD3";-- round (64 x 0.016 x 2^28) in hex
				wrupd <= ( '0' & WR_UPD(31 downto 6) ) + WR_UPD(5);
			 else
				us_ms_wru <= '0';
				wrupd_cst <= x"100000000"; -- x 16ns
				wrupd <= '0' & WR_UPD(25 downto 0);
			 end if;		
		 end if;       	  		 
	end if;
end process;

ce_reg <= ( not ce_reg1 ) and ce_reg2;

process (CLK)
begin	
   if CLK'event and CLK = '0' then
	   if ce_reg = '1' then
	      conv_in <= sel_conv; -- Selected Data Converter Registered			
		end if;
		ce1_reg <= ce_reg;
		ce_mult <= ce1_reg;
				
		end1_mult <= not end_mult_s;
		end2_mult <= end1_mult;
		latch_mult <= ( not end1_mult ) and end2_mult; 
		latch1_mult <= latch_mult;
		
		latch_bin2bcd <= latch1_mult;
		
		bcd1_rdy <= not bcd_rdy_s;
		bcd2_rdy <= bcd1_rdy;
		end_bin2bcd <= ( not bcd1_rdy ) and bcd2_rdy;       		
	end if;
end process;

--------------------------------------- Converters scaling constants --------------------------------------------------

-- for Fraw_ADC, Draw_ADC, Fcal_ADC, Dcal_ADC, F_OCC, D_OCC, F_PREF, F_NREF, D_PREF and D_NREF.
adc_cst       <= x"4C4B40000"; 
-- multiply by 19.0736844 V => (13.12D)hex
dac_cst       <= x"1312D0000"; 
-- Bdot in volt : multiply by 76.29394 V = (4C.4B4)hex = x"4C4B40000"
-- Bdot in uT/s : multiply by 1 
bdot_cst      <= x"010000000"; 
-- multiply by 0.01 (32 * 0.01 * 2^28 ) => 10nT en uT
bmeas_cst  <= x"0051EB852"; 
-- multiply by 0.02 ( 20 ns en s )
tms_cst    <= x"00051EB85"; 
-- multiply by 20
tus_cst    <= x"140000000"; 

-----------------------------------------------------------------------------------------------------------------------
--Mult36x36_signed
conv_in_36b_s <= x"00" & '0' & conv;

cmp_Mult36x36_signed : Mult36x36_signed
	port map(
		clk_i		=> CLK,
		reset_i	=> RESET,

		A_i	=> conv_in_36b_s,
		B_i	=> conv_cst,
				
		start_i	=> ce_mult,

		result_o		 => scale_conv_72b_s,
		result_rdy_o => end_mult_s
	);

-- 1 LSB ADC in V = (20 x 10^6) / 2^18
-- 1 LSB ADC x 2^28 = = (4C.4B40000)hex = ADC scale factor

-- DAC transfert function in differential with D in straight binary code :
-- [ (Vrefp - Vrefn) x D x 2  / ( 2^20 - 1 ) ] + ( 2 x Vrefn )
-- with Vrefp = +5V - 1LSB and Vrefn = -5V with 1 LSB = 10 / 2^20

process (CLK)
begin	
   if CLK'event and CLK = '1' then
	   if RESET = '1' then
		   scale_conv <= x"0000000000000000";
			bin_word <= x"000000";
		elsif latch_mult = '1' then
		   --Mult36x36_signed
		   scale_conv <= scale_conv_72b_s(63 downto 0);		   
		end if;
		bin_word <= scale_conv(51 downto 28) + scale_conv(27);
	 end if;
end process;
							 
process (CLK)
begin	
   if CLK'event and CLK = '1' then
	      conv_cst <= convcst;
	      convsign <= conv_in(26);
		   if convsign = '0' then
		      conv <= conv_in;
			   conv_sign <= x"2B"; -- "+" ascii character			
		   else
		      conv <= ( not conv_in ) + 1;
			   conv_sign <= x"2D"; -- "-" ascii character
		   end if;
	end if;
end process;

--------------------------------- 24-Bit Binary to 8-Digit BCD converter ---------------------------
----------------------------------------- in parallel mode -----------------------------------------
------------------------------ based on "shift and add 3 algorithm" --------------------------------

cmp_bin2bcd : bin2bcd
	port map(
		clk_i			=> CLK,
		reset_i		=> RESET,

		bin_word_i	=> bin_word,
		start_conv_i=> latch_bin2bcd,
		
		bcd_1_o		=> bcd1,
		bcd_2_o		=> bcd10,
		bcd_3_o		=> bcd100,
		bcd_4_o		=> bcd1000,
		bcd_5_o		=> bcd10000,
		bcd_6_o		=> bcd100000,
		bcd_7_o		=> bcd1000000,
		bcd_8_o		=> bcd10000000,
    	rdy_o		   => bcd_rdy_s
	);

process (CLK)
begin	
   if CLK'event and CLK = '1' then	
		if end_bin2bcd = '1' then
			bcd_1 <= bcd1;
			bcd_10 <= bcd10;
		   bcd_100 <= bcd100;
			bcd_1000 <= bcd1000;
			bcd_10000 <= bcd10000;
			bcd_100000 <= bcd100000;
			bcd_1000000 <= bcd1000000;
			bcd_10000000 <= bcd10000000;
		end if;
	end if;
end process;

-------------------------------------------------- ASCII characters tables ---------------------------------------
-- The LCD display is artificially shared in 8 zones of 20 characters
   --Line1 : zone0 01--20 zone1 21--40
	--Line2 : zone2 41--60 zone3 61--80
	--Line3 : zone4 01--20 zone5 21--40
	--Line4 : zone6 41--60 zone7 61--80
	
-- sel_cst_data explanation : ( x"01" = page 1 )     & ( "000" = LCD zone0 ) 
--                          : ( x"22" = sub page 2 ) & ( "111" = LCD zone7 ) 
--                          : ( x"02" = page 2 )     & ( "001" = LCD zone1 ) 
--                          : ( x"88" = sub page 4 ) & ( "101" = LCD zone5 ) 
-- LCD display: ------------------------
--              |  zone 0  ||  zone 1  | line 1
--              |  zone 2  ||  zone 3  | line 2
--              |  zone 4  ||  zone 5  | line 3
--              |  zone 6  ||  zone 7  | line 4
--              ------------------------
process (CLK)
begin
    if CLK'event and CLK = '1' then
	    sel_cst_data <= sel_sub_px & sel_px & sel_data_mux; -- sub page & page number (1, 2, 4 and 8) & LCD zone (0 to 7)
       case sel_cst_data is
		    when ( x"01" & "010" ) => sel_conv  <= fadcraw;
			                           convcst   <= adc_cst;
			 when ( x"01" & "011" ) => sel_conv  <= dadcraw;
			                           convcst   <= adc_cst;
			 when ( x"01" & "100" ) => sel_conv  <= fadccal;
			                           convcst   <= adc_cst;
			 when ( x"01" & "101" ) => sel_conv  <= dadccal;
			                           convcst   <= adc_cst;
			 when ( x"11" & "000" ) => sel_conv  <= flim;
			                           convcst   <= adc_cst;
			 when ( x"11" & "001" ) => sel_conv  <= dlim;
			                           convcst   <= adc_cst;
			 when ( x"11" & "011" ) => convcst   <= wrupd_cst;
			                           sel_conv  <= wrupd;
			 when ( x"02" & "010" ) => convcst   <= bmeas_cst;
			                           sel_conv  <= bmeas;												
			 when ( x"02" & "011" ) => convcst   <= bdot_cst;
			                           sel_conv  <= bdot;
												
			 when ( x"22" & "010" ) => convcst   <= bdot_cst;
			                           sel_conv  <= bdothold;
			 when ( x"22" & "011" ) => convcst   <= bdot_cst;
			                           sel_conv  <= bdotthres;												
			 when ( x"22" & "100" ) => convcst   <= bmeas_cst;
			                           sel_conv  <= bhold;
			 when ( x"22" & "101" ) => convcst   <= bmeas_cst;
			                           sel_conv  <= bthres;												
			 when ( x"22" & "111" ) => convcst   <= bholdtime_cst;
			                           sel_conv  <= bholdtime;	
												
			 when ( x"04" & "110" ) => convcst   <= dac_cst;
			                           sel_conv  <= cdac;
			 when ( x"04" & "111" ) => convcst   <= tms_cst;
			                           sel_conv  <= z0dly;
			 when ( x"08" & "010" ) => convcst   <= tms_cst;
			                           sel_conv  <= flwdly; 
			 when ( x"08" & "011" ) => convcst   <= tms_cst;
			                           sel_conv  <= dlwdly;
			 when ( x"08" & "100" ) => convcst   <= tms_cst;
			                           sel_conv  <= flwwid; 
			 when ( x"08" & "101" ) => convcst   <= tms_cst;
			                           sel_conv  <= dlwwid;
			 when ( x"08" & "110" ) => convcst   <= tus_cst;
			                           sel_conv  <= fltwid;
			 when ( x"08" & "111" ) => convcst   <= tus_cst;
			                           sel_conv  <= dltwid;
			 when ( x"88" & "000" ) => convcst   <= tms_cst;
			                           sel_conv  <= fhwdly;
			 when ( x"88" & "001" ) => convcst   <= tms_cst;
			                           sel_conv  <= dhwdly;
			 when ( x"88" & "010" ) => convcst   <= tms_cst;
			                           sel_conv  <= fhwwid;
			 when ( x"88" & "011" ) => convcst   <= tms_cst;
			                           sel_conv  <= dhwwid;
			 when ( x"88" & "100" ) => convcst   <= tus_cst;
			                           sel_conv  <= fhtwid;
			 when ( x"88" & "101" ) => convcst   <= tus_cst;
			                           sel_conv  <= dhtwid;
			 when ( x"44" & "000" ) => sel_conv  <= fpref;
			                           convcst   <= adc_cst;
			 when ( x"44" & "001" ) => sel_conv  <= dpref;
			                           convcst   <= adc_cst;
			 when ( x"44" & "010" ) => sel_conv  <= fnref;
			                           convcst   <= adc_cst;
			 when ( x"44" & "011" ) => sel_conv  <= dnref;
			                           convcst   <= adc_cst;
          when ( x"44" & "110" ) => sel_conv  <= focc;
			                           convcst   <= adc_cst;
          when ( x"44" & "111" ) => sel_conv  <= docc;
			                           convcst   <= adc_cst;
          when others            => convcst   <= adc_cst;
			                           sel_conv  <= fadcraw;			 
       end case;			 
		 case sel_cst_data is
          when ( x"44" & "100" ) => dmmascii  <= x"30";
			                           mmascii   <= x"3" & "000" & F_GCC(31);           -- integer part of F-gcc convert in ASCII
                                    cmascii   <= x"3" & bcd1_10;
                                    dmascii   <= x"3" & bcd1_100;
                                    mascii    <= x"3" & bcd1_1000;	
												cascii    <= x"3" & bcd1_10000;
												dascii    <= x"3" & bcd1_100000;
												uascii    <= x"3" & bcd1_1000000;
												fractpart <= F_GCC(30 downto 0) & '0';
   		 when ( x"44" & "101" ) => dmmascii  <= x"30";
			                           mmascii   <= x"3" & "000" & D_GCC(31);           -- integer part of D-gcc convert in ASCII
												cmascii   <= x"3" & bcd1_10;
												dmascii   <= x"3" & bcd1_100;
												mascii    <= x"3" & bcd1_1000;
												cascii    <= x"3" & bcd1_10000;
												dascii    <= x"3" & bcd1_100000;
												uascii    <= x"3" & bcd1_1000000;
												fractpart <= D_GCC(30 downto 0) & '0';							 
			 when ( x"22" & "000" ) => mmascii   <= x"3" & "00" & AF_COIL(31 downto 30);-- integer part of Af-coil convert in ASCII
			                           cmascii   <= x"3" & bcd1_10;
												dmascii   <= x"3" & bcd1_100;
												mascii    <= x"3" & bcd1_1000;	
												cascii    <= x"3" & bcd1_10000;
												dascii    <= x"3" & bcd1_100000;
												uascii    <= x"3" & bcd1_1000000;
												fractpart <= AF_COIL(29 downto 0) & "00";
		    when ( x"22" & "001" ) => mmascii   <= x"3" & "00" & AD_COIL(31 downto 30);-- integer part of Ad_coil convert in ASCII
			                           cmascii   <= x"3" & bcd1_10;
												dmascii   <= x"3" & bcd1_100;
												mascii    <= x"3" & bcd1_1000;
												cascii    <= x"3" & bcd1_10000;
												dascii    <= x"3" & bcd1_100000;
												uascii    <= x"3" & bcd1_1000000;
												fractpart <= AD_COIL(29 downto 0) & "00"; 
			 when ( x"02" & "100" ) => mmascii   <= x"3" & "000" & KF_BMEAS(31);        -- integer part of kf_Bmeas convert in ASCII
			                           cmascii   <= x"3" & bcd1_10;
												dmascii   <= x"3" & bcd1_100;
												mascii    <= x"3" & bcd1_1000;	
												cascii    <= x"3" & bcd1_10000;
												dascii    <= x"3" & bcd1_100000;
												uascii    <= x"3" & bcd1_1000000;
												fractpart <= KF_BMEAS(30 downto 0) & '0';
			 when ( x"02" & "101" ) => mmascii   <= x"3" & "000" & KF_BDOT(31);         -- integer part of kf_Bdot convert in ASCII
			                           cmascii   <= x"3" & bcd1_10;
												dmascii   <= x"3" & bcd1_100;
												mascii    <= x"3" & bcd1_1000;
												cascii    <= x"3" & bcd1_10000;
												dascii    <= x"3" & bcd1_100000;
												uascii    <= x"3" & bcd1_1000000;
												fractpart <= KF_BDOT(30 downto 0) & '0';
			 when ( x"02" & "110" ) => mmascii   <= x"3" & "000" & KD_BMEAS(31);        -- integer part of kd_Bmeas convert in ASCII
			                           cmascii   <= x"3" & bcd1_10;
												dmascii   <= x"3" & bcd1_100;
												mascii    <= x"3" & bcd1_1000;	
												cascii    <= x"3" & bcd1_10000;
												dascii    <= x"3" & bcd1_100000;
												uascii    <= x"3" & bcd1_1000000;
												fractpart <= KD_BMEAS(30 downto 0) & '0';
			 when ( x"02" & "111" ) => mmascii   <= x"3" & "000" & KD_BDOT(31);         -- integer part of kf_Bdot convert in ASCII
			                           cmascii   <= x"3" & bcd1_10;
												dmascii   <= x"3" & bcd1_100;
												mascii    <= x"3" & bcd1_1000;	
												cascii    <= x"3" & bcd1_10000;
												dascii    <= x"3" & bcd1_100000;
												uascii    <= x"3" & bcd1_1000000;
												fractpart <= KD_BDOT(30 downto 0) & '0';
			 when others            => dmmascii  <= x"3" & bcd_10000000;
			                           mmascii   <= x"3" & bcd_1000000;
												cmascii   <= x"3" & bcd_100000;
												dmascii   <= x"3" & bcd_10000;
                                    mascii    <= x"3" & bcd_1000;
                                    cascii    <= x"3" & bcd_100;
                                    dascii    <= x"3" & bcd_10;
												uascii    <= x"3" & bcd_1;
                                    fractpart <= x"00000000";                                   		                           
		 end case;
       gcc0 <= fractpart;--32-Bit fractionnal part to BCD digit converter
		 gcc0x8 <= '0' & gcc0 & "000";
		 gcc0x2 <= "000" & gcc0 & '0';
		 gcc0x10 <= gcc0x8 + gcc0x2;

		 bcd1_10 <= gcc0x10(35 downto 32);

		 gcc1 <= gcc0x10(31 downto 0);
		 gcc1x8 <= '0' & gcc1 & "000";
		 gcc1x2 <= "000" & gcc1 & '0';
		 gcc1x10 <= gcc1x8 + gcc1x2;

		 bcd1_100 <= gcc1x10(35 downto 32);

		 gcc2 <= gcc1x10(31 downto 0);
		 gcc2x8 <= '0' & gcc2 & "000";
		 gcc2x2 <= "000" & gcc2 & '0';
		 gcc2x10 <= gcc2x8 + gcc2x2;

		 bcd1_1000 <= gcc2x10(35 downto 32);

		 gcc3 <= gcc2x10(31 downto 0);
		 gcc3x8 <= '0' & gcc3 & "000";
		 gcc3x2 <= "000" & gcc3 & '0';
		 gcc3x10 <= gcc3x8 + gcc3x2;

		 bcd1_10000 <= gcc3x10(35 downto 32);

		 gcc4 <= gcc3x10(31 downto 0);
		 gcc4x8 <= '0' & gcc4 & "000";
		 gcc4x2 <= "000" & gcc4 & '0';
		 gcc4x10 <= gcc4x8 + gcc4x2;

		 bcd1_100000 <= gcc4x10(35 downto 32);

		 gcc5 <= gcc4x10(31 downto 0);
		 gcc5x8 <= '0' & gcc5 & "000";
		 gcc5x2 <= "000" & gcc5 & '0';
		 gcc5x10 <= gcc5x8 + gcc5x2;

		 bcd1_1000000 <= gcc5x10(35 downto 32);		 
	 end if;
end process;
			 
process (CLK)
begin	
   if CLK'event and CLK = '1' then
	   char_sel  <= '0'  & char_pos;
	   u_ascii <= uascii;
		d_ascii <= dascii;
		c_ascii <= cascii;
		m_ascii <= mascii;
		dm_ascii <= dmascii;
		cm_ascii <= cmascii;
		mm_ascii <= mmascii;
		dmm_ascii <= dmmascii;
		--if convsign = '0' then
		   --conv_sign <= x"2B";
      --else
         --conv_sign <= x"2D";
      --end if;			
	 end if;
end process;

F_y_or_blank <= x"79" when F_OVR = '1' else x"20"; -- y or blank
F_e_or_n     <= x"65" when F_OVR = '1' else x"6E"; -- e or n
F_s_or_o     <= x"73" when F_OVR = '1' else x"6F"; -- s or o

D_y_or_blank <= x"79" when D_OVR = '1' else x"20"; -- y or blank
D_e_or_n     <= x"65" when D_OVR = '1' else x"6E"; -- e or n
D_s_or_o     <= x"73" when D_OVR = '1' else x"6F"; -- s or o

--OPER_SPARE = 1 --> operational / OPER_SPARE = 0 --> spare

o_or_s     <= x"6F" when OPER_SPARE = '1' else x"73"; -- o or s
e_or_a     <= x"65" when OPER_SPARE = '1' else x"61"; -- e or a
a_or_e     <= x"61" when OPER_SPARE = '1' else x"65"; -- a or e
t_or_blank <= x"74" when OPER_SPARE = '1' else x"20"; -- t or blank
i_or_blank <= x"69" when OPER_SPARE = '1' else x"20"; -- i or blank
o_or_blank <= x"6F" when OPER_SPARE = '1' else x"20"; -- o or blank
n_or_blank <= x"6E" when OPER_SPARE = '1' else x"20"; -- n or blank
a_or_blank <= x"61" when OPER_SPARE = '1' else x"20"; -- a or blank
l_or_blank <= x"6C" when OPER_SPARE = '1' else x"20"; -- l or blank

--EFF_SIMUL = 1 --> effective = B-MEAS / EFF_SIMUL = 0 --> simulated = B-SIMU 

M_or_S     <= x"4D" when EFF_SIMUL = '1' else x"53"; -- M or S
E_or_I     <= x"45" when EFF_SIMUL = '1' else x"49"; -- E or I
A_or_M     <= x"41" when EFF_SIMUL = '1' else x"4D"; -- A or M
S_or_U     <= x"53" when EFF_SIMUL = '1' else x"55"; -- S or U

uorm_wru   <= x"6D" when us_ms_wru = '1' else x"E4"; -- u or m
uorm_bht   <= x"6D" when us_ms_bht = '1' else x"E4"; -- u or m

----------------------------------------------------BCD down counter-------------------------------------------------------

--BCD digit converted in ASCII
sec_lsb  <= x"3" & BCD_COUNTDOWN(3 downto 0);	  
sec_msb  <= x"3" & BCD_COUNTDOWN(7 downto 4);
min_lsb  <= x"3" & BCD_COUNTDOWN(11 downto 8);	  
min_msb  <= x"3" & BCD_COUNTDOWN(15 downto 12);
hour_lsb <= x"3" & BCD_COUNTDOWN(19 downto 16);	  
hour_msb <= x"3" & BCD_COUNTDOWN(23 downto 20);
day_lsb  <= x"3" & BCD_COUNTDOWN(27 downto 24);	  
day_msb  <= x"3" & BCD_COUNTDOWN(31 downto 28);

--------------------------------------------CALIBRATION STATUS--------------------------------------------------------------

with CAL_STATUS select

         wwaae <= x"77" when x"0",  -- wait time elapsed
             	   x"77" when x"1",  -- wait zero cycle
					   x"61" when x"2",  -- apply +Vdac
					   x"61" when x"3",  -- apply -Vdac
					   x"65" when x"4",  -- extract offset
						x"20" when others; -- blank character by default	

with CAL_STATUS select

         aappx <= x"61" when x"0",  -- wait time elapsed
             	   x"61" when x"1",  -- wait zero cycle
					   x"70" when x"2",  -- apply +Vdac
					   x"70" when x"3",  -- apply -Vdac
					   x"78" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default

with CAL_STATUS select

         iippt <= x"69" when x"0",  -- wait time elapsed
             	   x"69" when x"1",  -- wait zero cycle
					   x"70" when x"2",  -- apply +Vdac
					   x"70" when x"3",  -- apply -Vdac
					   x"74" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default

with CAL_STATUS select

         ttllr <= x"74" when x"0",  -- wait time elapsed
             	   x"74" when x"1",  -- wait zero cycle
					   x"6C" when x"2",  -- apply +Vdac
					   x"6C" when x"3",  -- apply -Vdac
					   x"72" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default

with CAL_STATUS select

     bk_bk_yya <= x"20" when x"0",  -- wait time elapsed
             	   x"20" when x"1",  -- wait zero cycle
					   x"79" when x"2",  -- apply +Vdac
					   x"79" when x"3",  -- apply -Vdac
					   x"61" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default
						
with CAL_STATUS select

    tz_bk_bk_c <= x"74" when x"0",  -- wait time elapsed
             	   x"7A" when x"1",  -- wait zero cycle
					   x"20" when x"2",  -- apply +Vdac
					   x"20" when x"3",  -- apply -Vdac
					   x"63" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default
						
with CAL_STATUS select

      ie_p_m_t <= x"69" when x"0",  -- wait time elapsed
             	   x"65" when x"1",  -- wait zero cycle
					   x"2B" when x"2",  -- apply +Vdac
					   x"2D" when x"3",  -- apply -Vdac
					   x"74" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default

with CAL_STATUS select

       mrVV_bk <= x"6D" when x"0",  -- wait time elapsed
             	   x"72" when x"1",  -- wait zero cycle
					   x"56" when x"2",  -- apply +Vdac
					   x"56" when x"3",  -- apply -Vdac
					   --x"20" when x"4",  -- Extract offset
					   x"20" when others; -- blank character by default

with CAL_STATUS select

         eoddo <= x"65" when x"0",  -- wait time elapsed
             	   x"6F" when x"1",  -- wait zero cycle
					   x"64" when x"2",  -- apply +Vdac
					   x"64" when x"3",  -- apply -Vdac
					   x"6F" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default
						
with CAL_STATUS select

     bk_bk_aaf <= --x"20" when x"0",  -- wait time elapsed
             	   --x"20" when x"1",  -- wait zero cycle
					   x"61" when x"2",  -- apply +Vdac
					   x"61" when x"3",  -- apply -Vdac
					   x"66" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default
						
with CAL_STATUS select

         ecccf <= x"65" when x"0",  -- wait time elapsed
             	   x"63" when x"1",  -- wait zero cycle
					   x"63" when x"2",  -- apply +Vdac
					   x"63" when x"3",  -- apply -Vdac
					   x"66" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default

with CAL_STATUS select

    ly_bk_bk_s <= x"6C" when x"0",  -- wait time elapsed
             	   x"79" when x"1",  -- wait zero cycle
					   --x"20" when x"2",  -- apply +Vdac
					   --x"20" when x"3",  -- apply -Vdac
					   x"73" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default
						
with CAL_STATUS select

    ac_bk_bk_e <= x"61" when x"0",  -- wait time elapsed
             	   x"63" when x"1",  -- wait zero cycle
					   --x"20" when x"2",  -- apply +Vdac
					   --x"20" when x"3",  -- apply -Vdac
					   x"65" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default
						
with CAL_STATUS select

    pl_bk_bk_t <= x"70" when x"0",  -- wait time elapsed
             	   x"6C" when x"1",  -- wait zero cycle
					   --x"20" when x"2",  -- apply +Vdac
					   --x"20" when x"3",  -- apply -Vdac
					   x"74" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default
						
with CAL_STATUS select

   se_bk_bk_bk <= x"73" when x"0",  -- wait time elapsed
             	   x"65" when x"1",  -- wait zero cycle
					   --x"20" when x"2",  -- apply +Vdac
					   --x"20" when x"3",  -- apply -Vdac
					   --x"20" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default
						
with CAL_STATUS select

 e_bk_bk_bk_bk <= x"65" when x"0",  -- wait time elapsed
             	   --x"20" when x"1",  -- wait zero cycle
					   --x"20" when x"2",  -- apply +Vdac
					   --x"20" when x"3",  -- apply -Vdac
					   --x"20" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default
						
with CAL_STATUS select

 d_bk_bk_bk_bk <= x"64" when x"0",  -- wait time elapsed
             	   --x"20" when x"1",  -- wait zero cycle
					   --x"20" when x"2",  -- apply +Vdac
					   --x"20" when x"3",  -- apply -Vdac
					   --x"20" when x"4",  -- extract offset
					   x"20" when others; -- blank character by default
   
with CAL_ERROR select

         nFFDD <= x"6E" when x"0",  -- no errors
                  x"46" when x"1",  -- F-offset error
			         x"46" when x"2",  -- F-gain error
			         x"44" when x"3",  -- D-offset error
			         x"44" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

          o_4m <= x"6F" when x"0",  -- no errors
                  x"2D" when x"1",  -- F-offset error
			         x"2D" when x"2",  -- F-gain error
			         x"2D" when x"3",  -- D-offset error
			         x"2D" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

       bk_ogog <= x"20" when x"0",  -- no errors
                  x"6F" when x"1",  -- F-offset error
			         x"67" when x"2",  -- F-gain error
			         x"6F" when x"3",  -- D-offset error
			         x"67" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

         efafa <= x"65" when x"0",  -- no errors
                  x"66" when x"1",  -- F-offset error
			         x"61" when x"2",  -- F-gain error
			         x"66" when x"3",  -- D-offset error
			         x"61" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

         rfifi <= x"72" when x"0",  -- no errors
                  x"66" when x"1",  -- F-offset error
			         x"69" when x"2",  -- F-gain error
			         x"66" when x"3",  -- D-offset error
			         x"69" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

         rsnsn <= x"72" when x"0",  -- no errors
                  x"73" when x"1",  -- F-offset error
			         x"6E" when x"2",  -- F-gain error
			         x"73" when x"3",  -- D-offset error
			         x"6E" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

    oe_bk_e_bk <= x"6F" when x"0",  -- no errors
                  x"65" when x"1",  -- F-offset error
			         --x"20" when x"2",  -- F-gain error
			         x"65" when x"3",  -- D-offset error
			         --x"20" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

         rtete <= x"72" when x"0",  -- no errors
                  x"74" when x"1",  -- F-offset error
			         x"65" when x"2",  -- F-gain error
			         x"74" when x"3",  -- D-offset error
			         x"65" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

   s_bk_r_bk_r <= x"73" when x"0",  -- no errors
                  --x"20" when x"1",  -- F-offset error
			         x"72" when x"2",  -- F-gain error
			         --x"20" when x"3",  -- D-offset error
			         x"72" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

   s_bk_r_bk_r <= x"73" when x"0",  -- no errors
                  --x"20" when x"1",  -- F-offset error
			         x"72" when x"2",  -- F-gain error
			         --x"20" when x"3",  -- D-offset error
			         x"72" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

       bk_erer <= --x"20" when x"0",  -- no errors
                  x"65" when x"1",  -- F-offset error
			         x"72" when x"2",  -- F-gain error
			         x"65" when x"3",  -- D-offset error
			         x"72" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

       bk_roro <= --x"20" when x"0",  -- no errors
                  x"72" when x"1",  -- F-offset error
			         x"6F" when x"2",  -- F-gain error
			         x"72" when x"3",  -- D-offset error
			         x"6F" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

       bk_rrrr <= --x"20" when x"0",  -- no errors
                  x"72" when x"1",  -- F-offset error
			         x"72" when x"2",  -- F-gain error
			         x"72" when x"3",  -- D-offset error
			         x"72" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

  bk_o_bk_o_bk <= --x"20" when x"0",  -- no errors
                  x"6F" when x"1",  -- F-offset error
			         --x"20" when x"2",  -- F-gain error
			         x"6F" when x"3",  -- D-offset error
			         --x"20" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
with CAL_ERROR select

  bk_r_bk_r_bk <= --x"20" when x"0",  -- no errors
                  x"72" when x"1",  -- F-offset error
			         --x"20" when x"2",  -- F-gain error
			         x"72" when x"3",  -- D-offset error
			         --x"20" when x"4",  -- D-gain error
			         x"20" when others; -- blank character by default
						
-----------------------------------Page 1 - Line 1 & Line 2-----------------------------------------

with char_sel select

             -- Page 1 Line 1
ascii_P1L12 <= x"20"     when x"00", -- blank		1
	            x"20"     when x"01", -- blank		2
				   x"46"     when x"02", -- F				3
				   x"6F"     when x"03", -- o				4
				   x"63"     when x"04", -- c				5
				   x"75"     when x"05", -- u				6
				   x"73"     when x"06", -- s				7
				   x"69"     when x"07", -- i				8
				   x"6E" 	 when x"08", -- n				9
				   x"67"  	 when x"09", -- g				10
				   x"20"     when x"0A", -- blank		11
				   x"7F" 	 when x"0B", -- <-			12
				   x"20" 	 when x"0C", -- blank		13
				   x"41"		 when x"0D", -- A				14
				   x"44"     when x"0E", -- D				15
				   x"43"		 when x"0F", -- C				16
				   x"20"		 when x"10", -- blank		17
				   x"43"     when x"11", -- C				18
				   x"48"     when x"12", -- H				19
				   x"41"     when x"13", -- A				20
				   x"4E"     when x"14", -- N				21
				   x"4E"     when x"15", -- N				22
	            x"45"     when x"16", -- E				23
				   x"4C"     when x"17", -- L				24
				   x"53"     when x"18", -- S				25
				   x"20"     when x"19", -- blank		26
				   x"7E"     when x"1A", -- ->		   27
				   x"20"     when x"1B", -- blank		28
				   x"44" 	 when x"1C", -- D				29
				   x"65"		 when x"1D", -- e				30
				   x"66" 	 when x"1E", -- f				31
				   x"6F"     when x"1F", -- o				32
				   x"63" 	 when x"20", -- c				33
				   x"75" 	 when x"21", -- u				34
				   x"73"		 when x"22", -- s				35
				   x"69" 	 when x"23", -- i				36
				   x"6E"		 when x"24", -- n				37
				   x"67"		 when x"25", -- g				38
				   x"20"     when x"26", -- blank		39
				   x"20"     when x"27", -- blank		40
               -- Page 1 Line 2
               x"46"     when x"28", -- F				41
	            x"2D"     when x"29", -- -				42
				   x"72"     when x"2A", -- r				43
				   x"61"     when x"2B", -- a				44
				   x"77"     when x"2C", -- w				45
				   x"3A"     when x"2D", -- :				46
				   x"20"     when x"2E", -- blank		47
           conv_sign     when x"2F", -- + or -		48
			  dmm_ascii 	 when x"30", -- 0 or 1		49
			   mm_ascii  	 when x"31", -- 0 to 9		50
				   x"2E"     when x"32", -- .				51
			   cm_ascii  	 when x"33", -- 0 to 9		52
			   dm_ascii  	 when x"34", -- 0 to 9		53
			    m_ascii	 	 when x"35", -- 0 to 9		54
			    c_ascii   	 when x"36", -- 0 to 9		55
			    d_ascii	 	 when x"37", -- 0 to 9		56
			    u_ascii	 	 when x"38", -- 0 to 9		57
				   x"20"     when x"39", -- blank		58
				   x"56"     when x"3A", -- V				59
				   x"7C"     when x"3B", -- |				60
				   x"7C"     when x"3C", -- |				61
               x"44"     when x"3D", -- D				62
	            x"2D"     when x"3E", -- -				63
				   x"72"     when x"3F", -- r				64
				   x"61"     when x"40", -- a				65
				   x"77"     when x"41", -- w				66
				   x"3A"     when x"42", -- :				67
				   x"20"     when x"43", -- blank		68
			  conv_sign 	 when x"44", -- + or -		69
			  dmm_ascii 	 when x"45", -- 0 or 1		70
			   mm_ascii 	 when x"46", -- 0 to 9		71
				   x"2E"     when x"47", -- .				72
			   cm_ascii  	 when x"48", -- 0 to 9		73
			   dm_ascii 	 when x"49", -- 0 to 9		74
			    m_ascii		 when x"4A", -- 0 to 9		75
			    c_ascii  	 when x"4B", -- 0 to 9		76
			    d_ascii		 when x"4C", -- 0 to 9		77
			    u_ascii		 when x"4D", -- 0 to 9		78
				   x"20"     when x"4E", -- blank		79
				   x"56"     when x"4F", -- V				80				 
               x"20"     when x"50", -- blank		81
               x"20"     when others;-- blank
					
-----------------------------------Page 1 - Line 3 & Line 4-----------------------------------------					

with char_sel select					
             -- Page 1 Line 3
ascii_P1L34 <= x"46"     when x"00", -- F				1
               x"2D"     when x"01", -- -				2	
               x"63"     when x"02", -- c				3
					x"61"     when x"03", -- a				4
					x"6C"     when x"04", -- l				5
					x"3A"     when x"05", -- :				6
				   x"20"     when x"06", -- blank		7
			  conv_sign 	 when x"07", -- + or -		8
			  dmm_ascii  	 when x"08", -- 0 to 1		9
			   mm_ascii  	 when x"09", -- 0 to 9		10
					x"2E"     when x"0A", -- .				11
			   cm_ascii  	 when x"0B", -- 0 to 9		12
			   dm_ascii  	 when x"0C", -- 0 to 9		13
			    m_ascii  	 when x"0D", -- 0 to 9		14
			    c_ascii     when x"0E", -- 0 to 9		15
			    d_ascii   	 when x"0F", -- 0 to 9		16
			    u_ascii   	 when x"10", -- 0 to 9		17
					x"20"     when x"11", -- blank		18
					x"56"     when x"12", -- V				19
					x"7C"     when x"13", -- |				20
				   x"7C"     when x"14", -- |				21
					x"44"     when x"15", -- D				22
               x"2D"     when x"16", -- -				23	
               x"63"     when x"17", -- c				24
					x"61"     when x"18", -- a				25
					x"6C"     when x"19", -- l				26
					x"3A"     when x"1A", -- :				27
				   x"20"     when x"1B", -- blank		28
			  conv_sign 	 when x"1C", -- + or -		29
			  dmm_ascii 	 when x"1D", -- 0 or 1		30
				mm_ascii 	 when x"1E", -- 0 to 9		31
					x"2E"     when x"1F", -- .				32
				cm_ascii 	 when x"20", -- 0 to 9		33
				dm_ascii 	 when x"21", -- 0 to 9		34
			    m_ascii  	 when x"22", -- 0 to 9		35
				 c_ascii  	 when x"23", -- 0 to 9		36
				 d_ascii   	 when x"24", -- 0 to 9		37
				 u_ascii   	 when x"25", -- 0 to 9		38
					x"20"     when x"26", -- blank		39
					x"56"     when x"27", -- V				40
               -- Page 1 Line 4
				   x"46"     when x"28", -- F				41
					x"2D"     when x"29", -- -				42	
               x"6F"     when x"2A", -- o				43
					x"76"     when x"2B", -- v				44
					x"72"     when x"2C", -- r				45
					x"3A"     when x"2D", -- :				46
					x"20"     when x"2E", -- blank		47
					x"20"     when x"2F", -- blank		48
				   x"20"     when x"30", -- blank		49
				   x"20"     when x"31", -- blank		50
				   x"20"     when x"32", -- blank		51
		  F_y_or_blank     when x"33", -- y or blank	52
			   F_e_or_n     when x"34", -- e or n		53
			   F_s_or_o	    when x"35", -- s	or o  	54
				   x"20"     when x"36", -- blank		55
				   x"20"	    when x"37", -- blank		56
		         x"20" 	 when x"38", -- blank   	57
			      x"20"     when x"39", -- blank		58
			      x"20"     when x"3A", -- blank		59
					x"7C"     when x"3B", -- |				60
				   x"7C"     when x"3C", -- |				61
					x"44"     when x"3D", -- D				62
               x"2D"     when x"3E", -- -				63
					x"6F"     when x"3F", -- o				64
					x"76"     when x"40", -- v				65
					x"72"     when x"41", -- r				66
					x"3A"     when x"42", -- :				67
	            x"20"     when x"43", -- blank	   68
			      x"20"     when x"44", -- blank   	69
			      x"20"     when x"45", -- blank		70
				   x"20"     when x"46", -- blank		71
					x"20"     when x"47", -- blank		72
	     D_y_or_blank     when x"48", -- y or blank	73
				D_e_or_n     when x"49", -- e or n		74
				D_s_or_o     when x"4A", -- s or o		75
					x"20"     when x"4B", -- blank		76
					x"20"     when x"4C", -- blank		77
		         x"20"     when x"4D", -- blank   	78
	            x"20"     when x"4E", -- blank    	79
			      x"20"     when x"4F", -- blank		80	
				   x"46"     when x"50", -- F				81				 
               x"20"     when others;-- blank

--------------------------------- Page 1_1 - Line 1 & Line 2 -----------------------------------------
					
with char_sel select

             -- Page 1_1 Line 1
ascii_P1_1L12 <= x"46"     when x"00", -- F				1
                 x"6C"     when x"01", -- l				2	
                 x"69"     when x"02", -- i				3
					  x"6D"     when x"03", -- m 				4
					  x"3A"     when x"04", -- : 				5
					  x"2B"     when x"05", -- +		  		6
				     x"2F"     when x"06", -- / 				7
					  x"2D"     when x"07", -- - 				8
			    dmm_ascii	   when x"08", -- 0 or 1 		9
				  mm_ascii     when x"09", -- 0 to 9		10
					  x"2E"     when x"0A", -- .				11
				  cm_ascii 	   when x"0B", -- 0 to 9   	12
				  dm_ascii     when x"0C", -- 0 to 9 		13
				   m_ascii     when x"0D", -- 0 to 9		14
				   c_ascii     when x"0E", -- 0 to 9		15
				   d_ascii     when x"0F", -- 0 to 9		16
					u_ascii     when x"10", -- 0 to 9		17
				     x"20"     when x"11", -- blank			18
					  x"56"     when x"12", -- V				19
					  x"7C"     when x"13", -- |			   20 | = 7C
				     x"7C"     when x"14", -- |				21 | = 7C
					  x"44"     when x"15", -- D				22
                 x"6C"     when x"16", -- l				23	
                 x"69"     when x"17", -- i				24
					  x"6D"     when x"18", -- m				25
					  x"3A"     when x"19", -- :				26
					  x"2B"     when x"1A", -- +				27
				     x"2F"     when x"1B", -- /				28
					  x"2D"     when x"1C", -- -				29
			    dmm_ascii		when x"1D", -- 0 or 1		30
				  mm_ascii  	when x"1E", -- 0 to 9		31
					  x"2E"		when x"1F", -- .				32
				  cm_ascii  	when x"20", -- 0 to 9		33
				  dm_ascii     when x"21", -- 0 to 9		34
				   m_ascii  	when x"22", -- 0 to 9		35
				   c_ascii   	when x"23", -- 0 to 9		36
				   d_ascii  	when x"24", -- 0 to 9		37
					u_ascii     when x"25", -- 0 to 9		38
					  x"20"     when x"26", -- blank	 		39
					  x"56"     when x"27", -- V        	40
					-- Page 1_1 Line 2
                 x"57"     when x"28", -- W				41
					  x"68"     when x"29", -- h				42	
                 x"69"     when x"2A", -- i				43
				     x"74"     when x"2B", -- t				44
				     x"65"     when x"2C", -- e				45
					  x"20"     when x"2D", -- blank			46
					  x"52"     when x"2E", -- R				47
					  x"61"     when x"2F", -- a				48
				     x"62"     when x"30", -- b				49
				     x"62"     when x"31", -- b 				50
				     x"69"     when x"32", -- i				51
				     x"74"		when x"33", -- t				52
				     x"20"     when x"34", -- blank			53
				     x"75"     when x"35", -- u				54
				     x"70"     when x"36", -- p				55
				     x"64"     when x"37", -- d				56
				     x"61"     when x"38", -- a				57
				     x"74"     when x"39", -- t	  			58
				     x"65"     when x"3A", -- e				59
					  x"20"     when x"3B", -- blank			60 | = 7C
				     x"72"     when x"3C", -- r				61 | = 7C
					  x"61"     when x"3D", -- a				62
                 x"74"     when x"3E", -- t				63
					  x"65"     when x"3F", -- e				64
					  x"3A"     when x"40", -- :				65
					  x"20"     when x"41", -- blank			66
					  x"20"     when x"42", -- blank			67
					  x"20"     when x"43", -- blank			68					  
				 dmm_ascii   	when x"44", -- 0 or 1		69
			     mm_ascii 	   when x"45", -- 0 to 9   	70
				  cm_ascii  	when x"46", -- 0 to 9		71
				  dm_ascii     when x"47", -- 0 to 9		72
				   m_ascii   	when x"48", -- 0 to 9		73
				   x"2E"  	   when x"49", -- .				74
				   c_ascii	 	when x"4A", -- 0 to 9		75
				   d_ascii  	when x"4B",--  0 to 9   	76
				   u_ascii	 	when x"4C", -- 0 to 9		77
				     x"20"	 	when x"4D", -- blank			78
				  uorm_wru     when x"4E", -- u or m		79
				     x"73"     when x"4F", -- s				80
					  x"46"     when x"50", -- F				81				 
                 x"20"     when others;-- blank				  
					
--------------------------------- Page 1_1 - Line 3 & Line 4 -----------------------------------------
					
with char_sel select					
             -- Page 1_1 Line 3
ascii_P1_1L34 <= x"20"     when x"00", -- blank			1
                 x"20"     when x"01", -- blank			2	
                 x"20"     when x"02", -- blank			3
					  x"20"     when x"03", -- blank 		4
					  x"20"     when x"04", -- blank 		5
					  x"20"     when x"05", -- blank			6
				     x"20"     when x"06", -- blank 		7
					  x"20"     when x"07", -- blank 		8
					  x"20"	   when x"08", -- blank 		9
					  x"20"     when x"09", -- blank	  		10
					  x"20"     when x"0A", -- blank			11
					  x"20" 	   when x"0B", -- blank   		12
					  x"20"     when x"0C", -- blank			13
					  x"20"     when x"0D", -- blank			14
					  x"20"     when x"0E", -- blank			15
					  x"20"     when x"0F", -- blank	 		16
					  x"20"     when x"10", -- blank			17
				     x"20"     when x"11", -- blank			18
					  x"20"     when x"12", -- blank			19
					  x"20"     when x"13", -- blank			20 | = 7C
				     x"20"     when x"14", -- blank			21 | = 7C
					  x"20"     when x"15", -- blank			22
                 x"20"     when x"16", -- blank			23	
                 x"20"     when x"17", -- blank			24
					  x"20"     when x"18", -- blank			25
					  x"20"     when x"19", -- blank			26
					  x"20"     when x"1A", -- blank			27
				     x"20"     when x"1B", -- blank			28
					  x"20"     when x"1C", -- blank			29
					  x"20"		when x"1D", -- blank			30
					  x"20"     when x"1E", -- blank			31
					  x"20"		when x"1F", -- blank			32
					  x"20"     when x"20", -- blank			33
					  x"20"     when x"21", -- blank			34
					  x"20"     when x"22", -- blank			35
					  x"20"     when x"23", -- blank			36
					  x"20"     when x"24", -- blank			37
					  x"20"     when x"25", -- blank			38
					  x"20"     when x"26", -- blank  	   39
					  x"20"     when x"27", -- blank       40
               -- Page 1_1 Line 4
				     x"20"     when x"28", -- blank			41
					  x"20"     when x"29", -- blank			42	
                 x"20"     when x"2A", -- blank			43
				     x"20"     when x"2B", -- blank			44
				     x"20"     when x"2C", -- blank			45
					  x"20"     when x"2D", -- blank			46
					  x"20"     when x"2E", -- blank			47
					  x"20"     when x"2F", -- blank			48
				     x"20"     when x"30", -- blank			49
				     x"20"     when x"31", -- blank 		50
				     x"20"     when x"32", -- blank			51
				     x"20"		when x"33", -- blank			52
				     x"20"     when x"34", -- blank			53
				     x"20"     when x"35", -- blank			54
				     x"20"     when x"36", -- blank			55
				     x"20"     when x"37", -- blank			56
				     x"20"     when x"38", -- blank			57
				     x"20"     when x"39", -- blank	  		58
				     x"20"     when x"3A", -- blank			59
					  x"20"     when x"3B", -- blank			60 | = 7C
				     x"20"     when x"3C", -- blank			61 | = 7C
					  x"20"     when x"3D", -- blank			62
                 x"20"     when x"3E", -- blank			63
					  x"20"     when x"3F", -- blank			64
					  x"20"     when x"40", -- blank			65
					  x"20"     when x"41", -- blank			66
					  x"20"     when x"42", -- blank			67
					  x"20"     when x"43", -- blank			68
					  x"20"     when x"44", -- blank  		69
					  x"20"     when x"45", -- blank		   70
				     x"20"     when x"46", -- blank	   	71
				     x"20"	   when x"47", -- blank			72
				 	  x"20"     when x"48", -- blank	   	73
					  x"20"     when x"49", -- blank	   	74
				     x"20"     when x"4A", -- blank	   	75
				     x"20"     when x"4B", -- blank	   	76
					  x"20"     when x"4C", -- blank		   77
					  x"20"     when x"4D", -- blank	      78
					  x"20"     when x"4E", -- blank    	79
					  x"20"     when x"4F", -- blank		   80	
				     x"20"     when x"50", -- blank			81				 
                 x"20"     when others;-- blank
				
--------------------------------- Page 2 - Line 1 & Line 2 -----------------------------------------

with char_sel select

             -- Page 2 Line 1
ascii_P2L12 <= x"20"     when x"00", -- blank		1
	            x"20"     when x"01", -- blank		2
				   x"20"     when x"02", -- blank		3
				   x"20"     when x"03", -- blank		4
				   x"20"     when x"04", -- blank		5
				   x"20"     when x"05", -- blank		6
				   x"20"     when x"06", -- blank		7
               x"20"     when x"07", -- blank		8
					x"50"     when x"08", -- P				9
					x"53"     when x"09", -- S				10
					x"20"     when x"0A", -- blank		11
					x"42"     when x"0B", -- B				12
					x"2D"     when x"0C", -- -				13
					x"54"	    when x"0D", -- T				14
					x"52"     when x"0E", -- R				15
					x"41"	    when x"0F", -- A				16
					x"49"	    when x"10", -- I				17
					x"4E"     when x"11", -- N				18
					x"20"     when x"12", -- blank		19
				   x"3A"     when x"13", -- :				20
				   x"20"     when x"14", -- blank		21
              o_or_s     when x"15", -- o or s		22
	            x"70"     when x"16", -- p    p		23
	           e_or_a     when x"17", -- e or a		24
		         x"72"     when x"18", -- r    r		25
		        a_or_e     when x"19", -- a or e		26
		        t_or_blank when x"1A", -- t or blank	27
		        i_or_blank when x"1B", -- i or blank	28
		        o_or_blank when x"1C", -- o or blank	29
		        n_or_blank when x"1D", -- n or blank	30
		        a_or_blank when x"1E", -- a or blank	31
		        l_or_blank when x"1F", -- l or blank	32
		         x"20"  	 when x"20", -- blank		33
			      x"20"     when x"21", -- blank		34
		         x"20"	    when x"22", -- blank		35
		         x"20"     when x"23", -- blank		36
			      x"20"     when x"24", -- blank		37
				   x"20"	    when x"25", -- blank		38
			      x"20"     when x"26", -- blank		39
			      x"20"     when x"27", -- blank		40	
					-- Page 2 Line 2
               x"42"     when x"28", -- B				41
	            x"2D"     when x"29", -- -				42
				   M_or_S    when x"2A", -- M or S		43
				   E_or_I    when x"2B", -- E or I		44
				   A_or_M    when x"2C", -- A	or M		45
				   S_or_U    when x"2D", -- S	or U		46
				   x"3A"     when x"2E", -- :				47
				   conv_sign when x"2F", -- + or -		48
				   x"20" 	 when x"30", -- blank		49
				   mm_ascii  when x"31", -- 0 to 9		50
				   x"2E"     when x"32", -- .				51
				   cm_ascii  when x"33", -- 0 to 9		52
				   dm_ascii  when x"34", -- 0 to 9		53
				   m_ascii	 when x"35", -- 0 to 9		54
				   c_ascii   when x"36", -- 0 to 9		55
				   d_ascii	 when x"37", -- 0 to 9		56
				   u_ascii	 when x"38", -- 0 to 9		57
				   x"20"     when x"39", -- blank		58
				   x"54"     when x"3A", -- T				59
				   x"7C"     when x"3B", -- |				60
				   x"7C"     when x"3C", -- |				61
				   x"64"     when x"3D", -- d				62
	            x"42"     when x"3E", -- B				63
				   x"20"     when x"3F", -- blank		64
				   x"3A"     when x"40", -- :				65
				 	x"20"		 when x"41", -- blank		66
			  conv_sign 	 when x"42", -- + or -		67
			  dmm_ascii  	 when x"43", -- 0 to 1		68
				mm_ascii	 	 when x"44", -- 0 to 9		69
				   x"2E"  	 when x"45", -- .				70
				cm_ascii		 when x"46", -- 0 to 9				71
			   dm_ascii  	 when x"47", -- 0 to 9		72
				 m_ascii     when x"48", -- 0 to 9		73
				 c_ascii     when x"49", -- 0 to 9		74
				 d_ascii   	 when x"4A", -- 0 to 9		75
			    u_ascii		 when x"4B", -- 0 to 9		76
					x"20"		 when x"4C", -- blank		77
				   x"54"     when x"4D", -- T				78
				   x"2F"     when x"4E", -- /				79
				   x"73"     when x"4F", -- s				80
					x"20"     when x"50", -- blank		81
               x"20"     when others;-- blank      
					
--------------------------------- Page 2 - Line 3 & Line 4 -----------------------------------------
					
with char_sel select					
             -- Page 2 Line 3
ascii_P2L34 <= x"6B"     when x"00", -- k				1
               x"66"     when x"01", -- f				2	
               x"2D"     when x"02", -- -				3
					x"42"     when x"03", -- B   			4
					x"6D"     when x"04", -- m 			5
					x"65"     when x"05", -- e				6
				   x"61"     when x"06", -- a 			7
					x"73"     when x"07", -- s 			8
					x"3A"	    when x"08", -- : 			9
					x"20"     when x"09", -- blank	   10
					mm_ascii  when x"0A", -- 0 to 1		11
					x"2E" 	 when x"0B", -- .     		12
					cm_ascii  when x"0C", -- 0 to 9		13
					dm_ascii  when x"0D", -- 0 to 9		14
					m_ascii   when x"0E", -- 0 to 9		15
					c_ascii   when x"0F", -- 0 to 9		16
					d_ascii   when x"10", -- 0 to 9		17
				   u_ascii   when x"11", -- 0 to 9		18
					x"20"   	 when x"12", -- blank		19
					x"7C"     when x"13", -- |				20
				   x"7C"     when x"14", -- |				21
					x"6B"     when x"15", -- k				22
               x"66"     when x"16", -- f				23	
               x"2D"     when x"17", -- -				24
					x"64"     when x"18", -- d				25
					x"42"     when x"19", -- B				26
					x"20"     when x"1A", -- blank		27
				   x"20"     when x"1B", -- blank		28
					x"3A"     when x"1C", -- :				29
					x"20"		 when x"1D", -- blank		30
					mm_ascii  when x"1E", -- 0 to 1		31
					x"2E"		 when x"1F", -- .				32
					cm_ascii  when x"20", -- 0 to 9		33
					dm_ascii  when x"21",--  0 to 9		34
					m_ascii   when x"22", -- 0 to 9		35
					c_ascii   when x"23", -- 0 to 9		36
					d_ascii   when x"24", -- 0 to 9		37
					u_ascii   when x"25", -- 0 to 9		38
					x"20"     when x"26", -- blank      39
					x"20"     when x"27", -- blank      40
               -- Page 2 Line 4
				   x"6B"     when x"28", -- k				41
					x"64"     when x"29", -- d				42	
               x"2D"     when x"2A", -- -				43
					x"42"     when x"2B", -- B				44
					x"6D"     when x"2C", -- m				45
					x"65"     when x"2D", -- e				46
					x"61"     when x"2E", -- a				47
					x"73"     when x"2F", -- s				48
				   x"3A"     when x"30", -- :				49
				   x"20"     when x"31", -- blank 		50
				   mm_ascii  when x"32", -- 0 to 1		51
				   x"2E"		 when x"33", -- .				52
				   cm_ascii  when x"34", -- 0 to 9		53
				   dm_ascii  when x"35", -- 0 to 9		54
				   m_ascii   when x"36", -- 0 to 9		55
				   c_ascii   when x"37", -- 0 to 9		56
				   d_ascii   when x"38", -- 0 to 9		57
				   u_ascii   when x"39", -- 0 to 9	  	58
				   x"20"     when x"3A", -- blank		59
					x"7C"     when x"3B", -- |				60
				   x"7C"     when x"3C", -- |				61
					x"6B"     when x"3D", -- k				62
               x"64"     when x"3E", -- d				63
					x"2D"     when x"3F", -- -				64
					x"64"     when x"40", -- d				65
					x"42"     when x"41", -- B				66
					x"20"     when x"42", -- blank		67
					x"20"     when x"43", -- blank		68
					x"3A"     when x"44", -- :  			69
					x"20"     when x"45", -- blank		70
				   mm_ascii  when x"46", -- 0 to 1		71
					x"2E"	    when x"47", -- .				72
					cm_ascii  when x"48", -- 0 to 9		73
					dm_ascii  when x"49", -- 0 to 9		74
					m_ascii   when x"4A", -- 0 to 9		75
					c_ascii   when x"4B", -- 0 to 9		76
					d_ascii   when x"4C", -- 0 to 9		77
					u_ascii   when x"4D", -- 0 to 9		78
					x"20"     when x"4E", -- blank 	   79
					x"20"     when x"4F", -- blank		80	
				   x"6B"     when x"50", -- k				81				 
               x"20"     when others;-- blank

--------------------------------- Page 2_1 - Line 1 & Line 2 -----------------------------------------
					
with char_sel select

             -- Page 2_1 Line 1
ascii_P2_1L12 <= x"41"     when x"00", -- A			1
					  x"66"     when x"01", -- f			2
				     x"2D"     when x"02", -- -			3
				     x"63"     when x"03", -- c			4
				     x"6F"     when x"04", -- o			5
				     x"69"     when x"05", -- i			6
				     x"6C"     when x"06", -- l			7
                 x"3A"     when x"07", -- :			8
				  mm_ascii     when x"08", -- 0 to 3	9
					  x"2E"     when x"09", -- .			10
				  cm_ascii     when x"0A", -- 0 to 9	11
				  dm_ascii     when x"0B", -- 0 to 9	12
				   m_ascii     when x"0C", -- 0 to 9	13
				   c_ascii	   when x"0D", -- 0 to 9	14
				   d_ascii     when x"0E", -- 0 to 9	15
				   u_ascii	   when x"0F", -- 0 to 9	16
					  x"20"	   when x"10", -- blank		17
					  x"6D"     when x"11", -- m			18
					  x"32"     when x"12", -- 2			19
				     x"7C"     when x"13", -- |			20
				     x"7C"     when x"14", -- |			21
                 x"41"  	when x"15", -- A			22
	              x"64"     when x"16", -- d    		23
					  x"2D"     when x"17", -- -			24
		           x"63"     when x"18", -- c			25
		           x"6F"     when x"19", -- o			26
		           x"69"	   when x"1A", -- i			27
		           x"6C" 		when x"1B", -- l			28
		           x"3A" 		when x"1C", -- :			29
		        mm_ascii     when x"1D", -- 0 to 3	30
		           x"2E"     when x"1E", -- .			31
		        cm_ascii     when x"1F", -- 0 to 9	32
		        dm_ascii     when x"20", -- 0 to 9	33
			      m_ascii     when x"21", -- 0 to 9	34
		         c_ascii	   when x"22", -- 0 to 9	35
		         d_ascii     when x"23", -- 0 to 9	36
			      u_ascii     when x"24", -- 0 to 9	37
				     x"20" 	   when x"25", -- blank		38
			        x"6D"     when x"26", -- m			39
			        x"32"     when x"27", -- 2			40	
					-- Page 2_1 Line 2
                 x"64"     when x"28", -- d			41
	              x"42"     when x"29", -- B			42
				     x"2D"     when x"2A", -- -			43
				     x"48"     when x"2B", -- H			44
				     x"44"     when x"2C", -- D			45
				     x"3A"     when x"2D", -- :			46
				 conv_sign     when x"2E", -- + or -	47
			    dmm_ascii   	when x"2F", -- 0 or 1	48
			     mm_ascii 	   when x"30", -- 0 to 9   49
					  x"2E"  	when x"31", -- .			50
				  cm_ascii     when x"32", -- 0 to 9	51
				  dm_ascii   	when x"33", -- 0 to 9	52
				   m_ascii 	   when x"34", -- 0 to 9	53
				   c_ascii	 	when x"35", -- 0 to 9	54
				   d_ascii  	when x"36",--  0 to 9   55
				   u_ascii	 	when x"37", -- 0 to 9	56
				     x"54"	 	when x"38", -- T			57
					  x"2F"     when x"39", -- /			58
				     x"73"     when x"3A", -- s			59
				     x"7C"     when x"3B", -- |			60 | = 7C
				     x"7C"     when x"3C", -- |			61 | = 7C
				     x"64"     when x"3D", -- d			62
	              x"42"     when x"3E", -- B			63
				     x"2D"     when x"3F", -- -			64
				     x"54"     when x"40", -- T			65
				     x"48"     when x"41", -- H			66
				     x"3A"     when x"42", -- :			67
			    conv_sign     when x"43", -- + or -	68
				 dmm_ascii 		when x"44", -- 0 to 1	69
				  mm_ascii     when x"45", -- 0 to 9	70
				     x"2E"     when x"46", -- .			71
				  cm_ascii		when x"47", -- 0 to 9	72
				  dm_ascii     when x"48", -- 0 to 9	73
				   m_ascii     when x"49", -- 0 to 9	74
				   c_ascii	   when x"4A", -- 0 to 9	75
				   d_ascii     when x"4B", -- 0 to 9	76
				   u_ascii	   when x"4C", -- 0 to 9	77
					  x"54"     when x"4D", -- T			78
				     x"2F"     when x"4E", -- /			79
				     x"73"     when x"4F", -- s			80
					  x"41"     when x"50", -- A			81
                 x"20"     when others;-- blank      
					
--------------------------------- Page 2_1 - Line 3 & Line 4 -----------------------------------------
					
with char_sel select					
             -- Page 2_1 Line 3
ascii_P2_1L34 <= x"20"     when x"00", -- blank			1
                 x"42"     when x"01", -- B				2	
                 x"2D"     when x"02", -- -				3
					  x"48"     when x"03", -- H 				4
					  x"44"     when x"04", -- D				5
					  x"3A"     when x"05", -- :				6
		       conv_sign     when x"06", -- + or - 		7
					  x"20"		when x"07", -- blank 		8
				  mm_ascii	   when x"08", -- 0 to 9 		9
				     x"2E"     when x"09", -- .				10
				  cm_ascii     when x"0A", -- 0 to 9		11
				  dm_ascii 	   when x"0B", -- 0 to 9  		12
				   m_ascii     when x"0C", -- 0 to 9		13
				   c_ascii    	when x"0D", -- 0 to 9		14
				   d_ascii     when x"0E", -- 0 to 9		15
				   u_ascii     when x"0F", -- 0 to 9		16
				     x"20"     when x"10", -- blank			17
				     x"54"     when x"11", -- T				18
					  x"20"     when x"12", -- blank			19
					  x"7C"     when x"13", -- |				20 | = 7C
				     x"7C"     when x"14", -- |				21 | = 7C
					  x"20"     when x"15", -- blank			22
                 x"42"     when x"16", -- B				23	
                 x"2D"     when x"17", -- -				24
					  x"54"     when x"18", -- T				25
					  x"48"     when x"19", -- H 				26
					  x"3A"     when x"1A", -- :				27
				 conv_sign     when x"1B", -- + or -		28
			        x"20"     when x"1C", -- blank			29
				  mm_ascii		when x"1D", -- 0 to 9		30
				     x"2E"	   when x"1E", -- .				31
				  cm_ascii		when x"1F", -- 0 to 9		32
				  dm_ascii     when x"20", -- 0 to 9		33
				   m_ascii     when x"21", -- 0 to 9		34
				   c_ascii     when x"22", -- 0 to 9		35
				   d_ascii     when x"23", -- 0 to 9		36
				   u_ascii     when x"24", -- 0 to 9		37
				     x"20"     when x"25", -- blank		   38
					  x"54"     when x"26", -- T  	 		39
					  x"20"     when x"27", -- blank   		40
               -- Page 2_1 Line 4
				     x"64"     when x"28", -- d				41
					  x"42"     when x"29", -- B				42	
                 x"20"     when x"2A", -- blank			43
				     x"26"     when x"2B", -- &				44
				     x"20"     when x"2C", -- blank			45
					  x"42"     when x"2D", -- B				46
					  x"2D"     when x"2E", -- -				47
					  x"48"     when x"2F", -- H				48
				     x"4F"     when x"30", -- O				49
				     x"4C"     when x"31", -- L 				50
				     x"44"     when x"32", -- D				51
				     x"20"		when x"33", -- blank			52
				     x"6C"     when x"34", -- l				53
				     x"61"     when x"35", -- a				54
				     x"74"     when x"36", -- t				55
				     x"63"     when x"37", -- c				56
				     x"68"     when x"38", -- h				57
				     x"65"     when x"39", -- e	  			58
				     x"64"     when x"3A", -- d				59
					  x"20"     when x"3B", -- blank			60 | = 7C
				     x"61"     when x"3C", -- a				61 | = 7C
					  x"74"     when x"3D", -- t			   62
                 x"20"     when x"3E", -- blank			63
				     x"43" 		when x"3F", -- C				64
				     x"30"  	when x"40", -- 0				65
				     x"20"  	when x"41", -- blank			66
				     x"2B"  	when x"42", -- +				67
					  x"20"  	when x"43", -- blank	   	68
			    dmm_ascii     when x"44", -- 0 to 1  		69
			     mm_ascii     when x"45", -- 0 to 9	   70
			     cm_ascii     when x"46", -- 0 to 9	  	71
			     dm_ascii	   when x"47", -- 0 to 9		72
				   m_ascii     when x"48", -- 0 to 9	  	73
				     x"2E"     when x"49", -- .  			74
				   c_ascii     when x"4A", -- 0 to 9  		75
				   d_ascii     when x"4B", -- 0 to 9   	76
				   u_ascii     when x"4C", -- 0 to 9	   77
					  x"20"     when x"4D", -- blank	      78
				  uorm_bht     when x"4E", -- u or m 		79
					  x"73"     when x"4F", -- s		  		80	
				     x"20"     when x"50", -- blank	  		81				 
                 x"20"     when others;-- blank

--------------------------------- Page 3 - Line 1 & Line 2 -----------------------------------------

with char_sel select
               -- Page 3 Line 1
ascii_P3L12 <= x"20"     when x"00", -- blank		1
				   x"20"     when x"01", -- blank		2
				   x"20"	    when x"02", -- blank		3
				   x"20"  	 when x"03", -- blank		4
				   x"20"     when x"04", -- blank		5
				   x"20"	    when x"05", -- blank		6
               x"43"     when x"06", -- C				7
					x"41"     when x"07", -- A				8	
               x"4C"     when x"08", -- L				9
					x"20"     when x"09", -- blank		10
					x"53"     when x"0A", -- S				11
					x"54"     when x"0B", -- T				12
					x"41"     when x"0C", -- A		  		13
					x"54"		 when x"0D", -- T				14
				   x"55" 	 when x"0E", -- U				15
				   x"53"		 when x"0F", -- S				16
				   x"3A"     when x"10", -- :				17
				   x"20"  	 when x"11", -- blank		18
		         wwaae     when x"12", --   w   or   w   or   a   or   a   or   e		19
				   aappx	 	 when x"13", --   a   or   a   or   p   or   p   or   x		20 | = 7C
				   iippt		 when x"14", --   i   or   i   or   p   or   p   or   t		21 | = 7C
				   ttllr	 	 when x"15", --   t   or   t   or   l   or   l   or   r		22
			  bk_bk_yya		 when x"16", -- blank or blank or   y   or   y   or   a		23
			 tz_bk_bk_c     when x"17", --   t   or   z   or blank or blank or   c		24
			   ie_p_m_t     when x"18", --   i   or   e   or   +   or   -   or   t		25
		       mrVV_bk     when x"19", --   m   or   r   or   V   or   V   or blank	26 
				   eoddo     when x"1A", --   e   or   o   or   d   or   d   or   o		27
           bk_bk_aaf     when x"1B", -- blank or blank or   a   or   a   or   f		28
	            ecccf     when x"1C", --   e   or   c   or   c   or   c   or   f		29
			 ly_bk_bk_s     when x"1D", --   l   or   y   or blank or blank or   s		30
		    ac_bk_bk_e     when x"1E", --   a   or   c   or blank or blank or   e		31
		    pl_bk_bk_t     when x"1F", --   p   or   l   or blank or blank or   t		32
			se_bk_bk_bk     when x"20", --   s   or   e   or blank or blank or blank	33
		 e_bk_bk_bk_bk     when x"21", --   e   or blank or blank or blank or blank	34
		 d_bk_bk_bk_bk     when x"22", --   d   or blank or blank or blank or blank	35
				   x"20"     when x"23", -- blank		36
				   x"20"	    when x"24", -- blank		37
				   x"20"	    when x"25", -- blank		38
				   x"20"     when x"26", -- blank		39
				   x"20"     when x"27", -- blank		40				 
               -- Page 3 Line 2					
					x"43"     when x"28", -- C				41
	            x"61"     when x"29", -- a				42
				   x"6C"     when x"2A", -- l				43
				   x"20"     when x"2B", -- blank		44
				   x"69"     when x"2C", -- i				45
				   x"6E"     when x"2D", -- n				46
				   x"3A"		 when x"2E", -- :				47
				   x"20"     when x"2F", -- blank		48
				 day_msb	    when x"30", -- 0 to 9		49
				 day_lsb	    when x"31", -- 0 to 9		50
				   x"20"		 when x"32", -- blank		51
				   x"44"     when x"33", -- D				52
				   x"61"  	 when x"34", -- a				53
				   x"79"     when x"35", -- y		      54
				   x"20"     when x"36", -- blank		55
				hour_msb  	 when x"37", -- 0 to 2     56
				hour_lsb	    when x"38", -- 0 to 9		57
				   x"48"     when x"39", -- H				58
				   x"3A"     when x"3A", -- :				59
				 min_msb     when x"3B", -- 0 to 5		60 | = 7C
				 min_lsb     when x"3C", -- 0 to 9		61 | = 7C
				   x"6D"     when x"3D", -- m				62
				   x"3A"     when x"3E", -- :				63	
             sec_msb     when x"3F", -- 0 to 5		64
				 sec_lsb     when x"40", -- 0 to 9		65
					x"73"     when x"41", -- s				66
					x"20"     when x"42", -- blank		67
				   x"20"     when x"43", -- blank		68
			      x"20" 	 when x"44", -- blank		69
			      x"20" 	 when x"45", -- blank		70
			      x"20"  	 when x"46", -- blank		71
				   x"20"     when x"47", -- blank		72
			      x"20"  	 when x"48", -- blank		73
				   x"20"  	 when x"49", -- blank		74
			      x"20"	 	 when x"4A", -- blank		75
			      x"20"  	 when x"4B", -- blank		76
			      x"20"		 when x"4C", -- blank		77
				   x"20"		 when x"4D", -- blank		78
				   x"20"     when x"4E", -- blank		79
				   x"20"     when x"4F", -- blank		80
					x"20"     when x"50", -- blank		81
               x"20"     when others;-- blank
					
--------------------------------- Page 3 - Line 3 & Line 4 -----------------------------------------
					
with char_sel select					
             -- Page 3 Line 3
ascii_P3L34 <= x"43"     when x"00", -- C				1
               x"61"     when x"01", -- a				2	
               x"6C"     when x"02", -- l				3
					x"20"     when x"03", -- blank		4
					x"65"     when x"04", -- e				5
					x"72"     when x"05", -- r				6
				   x"72"     when x"06", -- r				7
					x"6F"     when x"07", -- o				8
					x"72"     when x"08", -- r				9
					x"73"		 when x"09", -- s				10
					x"3A"     when x"0A", -- :				11
					x"20"     when x"0B", -- blank		12
					nffdd     when x"0C", --   n   or   f   or   f   or   d   or   d				13
					 o_4m     when x"0D", --   o   or   -   or   -   or   -   or   -				14
				 bk_ogog     when x"0E", -- blank or   o   or   g   or   o   or   g				15
					efafa     when x"0F", --   e   or   f   or   a   or   f   or   a				16
					rfifi     when x"10", --   r   or   f   or   i   or   f   or   i				17
					rsnsn     when x"11", --   r   or   s   or   n   or   s   or   n				18
	       oe_bk_e_bk     when x"12", --   o   or   e   or blank or   e   or blank			19
					rtete     when x"13", --   r	 or   t   or   e   or   t   or   e	 			20 | = 7C
		   s_bk_r_bk_r     when x"14", --   s	 or blank or   r	 or blank or   r				21 | = 7C
			    bk_erer     when x"15", -- blank or   e	 or   r   or   e   or   r				22
             bk_roro     when x"16", -- blank or   r   or   o	 or   r   or   o			   23	
             bk_rrrr     when x"17", -- blank or   r	 or   r	 or   r   or   r			   24
	     bk_o_bk_o_bk     when x"18", -- blank or   o	 or blank or   o   or blank   		25
		  bk_r_bk_r_bk     when x"19", -- blank or   r	 or blank or   r	 or blank		   26
					x"20"     when x"1A", -- blank 		27
				   x"20"     when x"1B", -- blank		28
					x"20"     when x"1C", -- blank		29
					x"20"     when x"1D", -- blank		30
					x"20"     when x"1E", -- blank		31
					x"20"     when x"1F", -- blank		32
					x"20"     when x"20", -- blank		33
					x"20"     when x"21", -- blank		34
					x"20"     when x"22", -- blank		35
					x"20"     when x"23", -- blank		36
					x"20"     when x"24", -- blank		37
					x"20"     when x"25", -- blank		38
					x"20"     when x"26", -- blank		39
					x"20"     when x"27", -- blank  		40
               -- Page 3 Line 4
				   x"43"     when x"28", -- C				41
					x"2D"     when x"29", -- -				42	
               x"44"     when x"2A", -- D				43
					x"41"     when x"2B", -- A				44
					x"43"     when x"2C", -- C				45
					x"3A"     when x"2D", -- :				46
					x"20"     when x"2E", -- blank		47
			  conv_sign     when x"2F", -- + or -		48
			  dmm_ascii     when x"30", -- 0 0r 1		49
			   mm_ascii     when x"31", -- 0 to 9		50
				   x"2E"     when x"32", -- .				51
			   cm_ascii     when x"33", -- 0 to 9		52
			   dm_ascii     when x"34", -- 0 to 9		53
			    m_ascii	    when x"35", -- 0 to 9		54
			    c_ascii     when x"36", -- 0 to 9		55
			    d_ascii	    when x"37", -- 0 to 9		56
			    u_ascii	    when x"38", -- 0 to 9		57
				   x"20"     when x"39", -- blank	  	58
				   x"56"     when x"3A", -- V				59
					x"7C"     when x"3B", -- |				60 | = 7C
				   x"7C"     when x"3C", -- |				61 | = 7C
					x"5A"     when x"3D", -- Z				62
					x"30"     when x"3E", -- 0				63	
               x"64"     when x"3F", -- d				64
					x"65"     when x"40", -- e				65
					x"6C"     when x"41", -- l				66
					x"61"     when x"42", -- a				67
					x"79"     when x"43", -- y				68
					x"3A" 	 when x"44", -- :				69
			      x"20" 	 when x"45", -- blank		70
			   cm_ascii  	 when x"46", -- 0 to 9		71
				dm_ascii     when x"47", -- 0 to 9		72
			    m_ascii  	 when x"48", -- 0 to 9		73
				   x"2E"  	 when x"49", -- .				74
			    c_ascii	 	 when x"4A", -- 0 to 9		75
			    d_ascii  	 when x"4B", -- 0 to 9		76
			    u_ascii		 when x"4C", -- 0 to 9		77
				   x"20"		 when x"4D", -- blank		78
				   x"6D"     when x"4E", -- m				79
				   x"73"     when x"4F", -- s				80
					x"43"     when x"50", -- C				81
               x"20"     when others;-- blank
					
--------------------------------- Page 3_1 - Line 1 & Line 2 -----------------------------------------
					
with char_sel select

             -- Page 3_1 Line 1
ascii_P3_1L12 <= x"2B"     when x"00", -- +			1
	              x"46"    	when x"01", -- F			2
				     x"61"    	when x"02", -- a			3
				     x"63"    	when x"03", -- c			4
					  x"71"    	when x"04", -- q			5
					  x"3A"    	when x"05", -- :			6
				     x"20"    	when x"06", -- blank		7
				 conv_sign 		when x"07", -- + or -	8
				 dmm_ascii 	   when x"08", -- 0 or 1	9
				  mm_ascii  	when x"09", -- 0 to 9	10
				     x"2E"    	when x"0A", -- .			11
				  cm_ascii 		when x"0B", -- 0 to 9	12
				  dm_ascii 	 	when x"0C", -- 0 to 9	13
				   m_ascii	 	when x"0D", -- 0 to 9	14
				   c_ascii   	when x"0E", -- 0 to 9	15
				   d_ascii	 	when x"0F", -- 0 to 9	16
			      u_ascii	   when x"10", -- 0 to 9	17
					  x"20"     when x"11", -- blank		18
					  x"56"     when x"12", -- V			19
				     x"7C"     when x"13", -- blank		20 | = 7C
				     x"7C"     when x"14", -- blank		21 | = 7C
                 x"2B"     when x"15", -- +			22
	              x"44"     when x"16", -- D			23
				     x"61"     when x"17", -- a			24
				     x"63"     when x"18", -- c			25
				     x"71"     when x"19", -- q			26
				     x"3A"     when x"1A", -- :			27
				     x"20"     when x"1B", -- blank		28
			    conv_sign 		when x"1C", -- + or -	29
			    dmm_ascii 		when x"1D", -- 0 or 1	30
			     mm_ascii  	when x"1E", -- 0 to 9	31
			        x"2E"     when x"1F", -- .			32
				  cm_ascii  	when x"20", -- 0 to 9	33
				  dm_ascii  	when x"21", -- 0 to 9	34
				   m_ascii		when x"22", -- 0 to 9	35
				   c_ascii  	when x"23", -- 0 to 9	36
				   d_ascii		when x"24", -- 0 to 9	37
				   u_ascii	 	when x"25", -- 0 to 9	38
				   x"20"     	when x"26", -- blank		39
				   x"56"     	when x"27", -- V			40	
					-- Page 3_1 Line 2
               x"2D"     	when x"28", -- -			41
	            x"46"     	when x"29", -- F			42
				   x"61"     	when x"2A", -- a			43
				   x"63"     	when x"2B", -- c			44
				   x"71"     	when x"2C", -- q			45
				   x"3A"     	when x"2D", -- :			46
				   x"20"     	when x"2E", -- blank		47
           conv_sign 	 	when x"2F", -- + or -	48
			  dmm_ascii 	 	when x"30", -- 0 or 1	49
			   mm_ascii 	 	when x"31", -- 0 to 9	50
			      x"2E"     	when x"32", -- .			51
			   cm_ascii  	 	when x"33", -- 0 to 9	52
			   dm_ascii  	 	when x"34", -- 0 to 9	53
			    m_ascii	 	 	when x"35", -- 0 to 9	54
			    c_ascii     	when x"36", -- 0 to 9	55
				 d_ascii	  	 	when x"37", -- 0 to 9	56
			    u_ascii	 	 	when x"38", -- 0 to 9	57
				   x"20"     	when x"39", -- blank		58
				   x"56"    	when x"3A", -- V			59
				   x"7C"    	when x"3B", -- blank		60 | = 7C
				   x"7C"     	when x"3C", -- blank		61 | = 7C
				   x"2D"     	when x"3D", -- -			62
	            x"44"     	when x"3E", -- D			63
				   x"61"     	when x"3F", -- a			64
				   x"63"     	when x"40", -- c			65
				   x"71"     	when x"41", -- q			66
				   x"3A"     	when x"42", -- :			67
				   x"20"     	when x"43", -- blank		68
			  conv_sign 		when x"44", -- + or -	69
			  dmm_ascii 		when x"45", -- 0 or 1	70
			   mm_ascii  		when x"46", -- 0 to 9	71
				   x"2E"     	when x"47", -- .			72
			   cm_ascii  		when x"48", -- 0 to 9	73
			   dm_ascii  		when x"49", -- 0 to 9	74
			    m_ascii	 		when x"4A", -- 0 to 9	75
			    c_ascii   		when x"4B", -- 0 to 9	76
			    d_ascii	 		when x"4C", -- 0 to 9	77
			    u_ascii	 		when x"4D", -- 0 to 9	78
				   x"20"     	when x"4E", -- blank		79
				   x"56"     	when x"4F", -- V			80
			      x"2B"     	when x"50", -- +			81
               x"20"     	when others;-- blank      
					
--------------------------------- Page 3_1 - Line 3 & Line 4 -----------------------------------------
					
with char_sel select					
             -- Page 3_1 Line 3
ascii_P3_1L34 <= x"46"     when x"00", -- F			1
					  x"2D"     when x"01", -- -			2
				     x"67"     when x"02", -- g			3
				     x"63"     when x"03", -- c			4
				     x"63"     when x"04", -- c			5
				     x"3A"     when x"05", -- :			6
				     x"20"     when x"06", -- blank		7
                 x"20"     when x"07", -- blank		8
				     x"20"     when x"08", -- blank		9
					  mm_ascii  when x"09", -- 0 or 1	10
				     x"2E"     when x"0A", -- .		11
				     cm_ascii  when x"0B", -- 0 to 9	12
				     dm_ascii  when x"0C", -- 0 to 9	13
				     m_ascii	when x"0D", -- 0 to 9	14
				     c_ascii   when x"0E", -- 0 to 9	15
				     d_ascii   when x"0F", -- 0 to 9	16
					  u_ascii	when x"10", -- 0 to 9	17
					  x"20"     when x"11", -- blank		18
					  x"20"     when x"12", -- blank		19
				     x"7C"     when x"13", -- |  		20 | = 7C
				     x"7C"     when x"14", -- |  		21 | = 7C
                 x"44"  	when x"15", -- D			22
	              x"2D"     when x"16", -- - 			23
					  x"67"     when x"17", -- g			24
		           x"63"     when x"18", -- c			25
		           x"63"     when x"19", -- c			26
		           x"3A"	   when x"1A", -- :			27
		           x"20" 		when x"1B", -- blank		28
		           x"20" 		when x"1C", -- blank		29
		           x"20"     when x"1D", -- blank		30
		        mm_ascii     when x"1E", -- 0 or 1	31
		           x"2E"     when x"1F", -- .			32
		        cm_ascii     when x"20", -- 0 to 9	33
			     dm_ascii     when x"21", -- 0 to 9	34
		         m_ascii	   when x"22", -- 0 to 9	35
		         c_ascii     when x"23", -- 0 to 9	36
			      d_ascii     when x"24", -- 0 to 9	37
				   u_ascii 	   when x"25", -- 0 to 9	38
			        x"20"     when x"26", -- blank		39
			        x"20"     when x"27", -- blank		40
               -- Page 3_1 Line 4
				     x"46"     when x"28", -- F			41
					  x"2D"     when x"29", -- -			42	
                 x"6F"     when x"2A", -- o			43
				     x"63"     when x"2B", -- c			44
				     x"63"     when x"2C", -- c			45
					  x"3A"     when x"2D", -- :			46
					  x"20"     when x"2E", -- blank		47
			    conv_sign     when x"2F", -- + or -	48
				 dmm_ascii     when x"30", -- 0 to 1	49
				  mm_ascii     when x"31", -- 0 to 9 	50
				     x"2E"     when x"32", -- .			51
				  cm_ascii		when x"33", -- 0 to 9	52
				  dm_ascii     when x"34", -- 0 to 9	53
				   m_ascii     when x"35", -- 0 to 9	54
				   c_ascii     when x"36", -- 0 to 9	55
				   d_ascii     when x"37", -- 0 to 9	56
				   u_ascii     when x"38", -- 0 to 9	57
				     x"20"     when x"39", -- blank	 	58
				     x"56"     when x"3A", -- V			59
					  x"7C"     when x"3B", -- |			60 | = 7C
				     x"7C"     when x"3C", -- |			61 | = 7C
					  x"44"     when x"3D", -- D			62
                 x"2D"     when x"3E", -- -			63
					  x"6F"     when x"3F", -- o			64
					  x"63"     when x"40", -- c			65
					  x"63"     when x"41", -- c			66
					  x"3A"     when x"42", -- :			67
					  x"20"     when x"43", -- blank		68
				 conv_sign     when x"44", -- + or -  	69
				 dmm_ascii     when x"45", -- 0 or 1	70
				  mm_ascii     when x"46", -- 0 to 9  	71
				     x"2E"	   when x"47", -- .			72
				  cm_ascii     when x"48", -- 0 to 9	73
				  dm_ascii     when x"49", -- 0 to 9  	74
				   m_ascii     when x"4A", -- 0 to 9  	75
				   c_ascii     when x"4B", -- 0 to 9	76
				   d_ascii     when x"4C", -- 0 to 9   77
				   u_ascii     when x"4D", -- 0 to 9   78
					  x"20"     when x"4E", -- blank   	79
					  x"56"     when x"4F", -- V		   80	
				     x"46"     when x"50", -- F			81				 
                 x"20"     when others;-- blank
					
--------------------------------- Page 4 - Line 1 & Line 2 -----------------------------------------

with char_sel select

             -- Page 4 Line 1
ascii_P4L12 <= x"20"     when x"00", -- blank		1
	            x"20"     when x"01", -- blank		2
				   x"20"     when x"02", -- blank		3
				   x"20"     when x"03", -- blank		4
				   x"20"     when x"04", -- blank		5
				   x"46"     when x"05", -- F				6
				   x"49"     when x"06", -- I				7
				   x"45"     when x"07", -- E				8
				   x"4C" 	 when x"08", -- L				9
				   x"44"		 when x"09", -- D				10
				   x"20"     when x"0A", -- blank		11
		   	   x"4D"     when x"0B", -- M				12
				   x"41"     when x"0C", -- A	      	13
			      x"52"	    when x"0D", -- R				14
			      x"4B"     when x"0E", -- K				15
			      x"45"	    when x"0F", -- E				16
				   x"52"	    when x"10", -- R				17
				   x"53"     when x"11", -- S				18
				   x"3A"     when x"12", -- :				19
				   x"57"     when x"13", -- W				20
				   x"69"     when x"14", -- i				21
				   x"6E"     when x"15", -- n		      22
	            x"64"     when x"16", -- d		      23
				   x"6F"     when x"17", -- o		      24
				   x"77"     when x"18", -- w		      25
				   x"73"     when x"19", -- s		      26
				   x"26"     when x"1A", -- &	    		27
				   x"54"     when x"1B", -- T		 		28
				   x"72"     when x"1C", -- r			   29
				   x"69"     when x"1D", -- i				30
			      x"67"     when x"1E", -- g 			31
			      x"67"     when x"1F", -- g				32
			      x"65"     when x"20", -- e				33
				   x"72"     when x"21", -- r				34
			      x"73"	    when x"22", -- s				35
			      x"20"     when x"23", -- blank		36
			      x"20"	    when x"24", -- blank		37
				   x"20"	    when x"25", -- blank		38
				   x"20"     when x"26", -- blank		39
				   x"20"     when x"27", -- blank		40
               -- Page 4 Line 2
               x"57"     when x"28", -- W				41
	            x"64"     when x"29", -- d				42
				   x"6C"     when x"2A", -- l				43
				   x"79"     when x"2B", -- y				44
				   x"2D"     when x"2C", -- -				45
				   x"46"     when x"2D", -- F				46
				   x"4C"     when x"2E", -- L				47
               x"3A"     when x"2F", -- :				48
				   x"20"     when x"30", -- blank		49
				cm_ascii     when x"31", -- 0 to 9		50
			   dm_ascii     when x"32", -- 0 to 9		51
			    m_ascii     when x"33", -- 0 to 9		52
				   x"2E"     when x"34", -- .				53
				 c_ascii 	 when x"35", -- 0 to 9		54
			    d_ascii   	 when x"36", -- 0 to 9		55
			    u_ascii 	 when x"37", -- 0 to 9		56
				   x"20" 	 when x"38", -- blank		57
				   x"6D"     when x"39", -- m				58
				   x"73"     when x"3A", -- s				59
				   x"7C"     when x"3B", -- |				60
				   x"7C"     when x"3C", -- |				61
               x"57"     when x"3D", -- W				62
	            x"64"     when x"3E", -- d				63
				   x"6C"     when x"3F", -- l				64
				   x"79"     when x"40", -- y				65
				   x"2D"     when x"41", -- -				66
				   x"44"     when x"42", -- D				67
				   x"4C"     when x"43", -- L				68
				   x"3A"     when x"44", -- :    		69
				   x"20"     when x"45", -- blank		70
			   cm_ascii  	when x"46", -- 0 to 9		71
			   dm_ascii	 	when x"47", -- 0 to 9		72
			    m_ascii  	when x"48", -- 0 to 9 		73
				   x"2E"    when x"49", -- .				74
			    c_ascii	 	when x"4A", -- 0 to 9		75
			    d_ascii  	when x"4B", -- 0 to 9		76
			    u_ascii	 	when x"4C", -- 0 to 9		77
				   x"20"	    when x"4D", -- blank		78
				   x"6D"     when x"4E", -- m				79
				   x"73"     when x"4F", -- s				80				 
               x"20"     when x"50", -- blank		81
               x"20"     when others;-- blank
					
--------------------------------- Page 4 - Line 3 & Line 4 -----------------------------------------
					
with char_sel select

             -- Page 4 Line 3
ascii_P4L34 <= x"57"     when x"00", -- W				1
	            x"77"     when x"01", -- w				2
				   x"69"     when x"02", -- i				3
				   x"64"     when x"03", -- d				4
				   x"2D"     when x"04", -- -				5
				   x"46"     when x"05", -- F				6
				   x"4C"     when x"06", -- L				7
				   x"3A"     when x"07", -- :				8
				   x"20" 	 when x"08", -- blank		9
				cm_ascii		 when x"09", -- 0 to 9		10
				dm_ascii     when x"0A", -- 0 to 9		11
		   	 m_ascii     when x"0B", -- 0 to 9		12
				   x"2E"     when x"0C", -- .	      	13
			    c_ascii	    when x"0D", -- 0 to 9		14
			    d_ascii     when x"0E", -- 0 to 9		15
			    u_ascii	    when x"0F", -- 0 to 9		16
				   x"20"	    when x"10", -- blank		17
				   x"6D"     when x"11", -- m				18
				   x"73"     when x"12", -- s				19
				   x"7C"     when x"13", -- |				20
				   x"7C"     when x"14", -- |				21
				   x"57"     when x"15", -- W		      22
	            x"77"     when x"16", -- w		      23
				   x"69"     when x"17", -- i		      24
				   x"64"     when x"18", -- d		      25
				   x"2D"     when x"19", -- -		      26
				   x"44"     when x"1A", -- D		      27
				   x"4C"     when x"1B", -- L				28
				   x"3A"     when x"1C", -- :				29
				   x"20"     when x"1D", -- blank		30
			   cm_ascii     when x"1E", -- 0 to 9		31
			   dm_ascii     when x"1F", -- 0 to 9		32
			    m_ascii     when x"20", -- 0 to 9		33
				   x"2E"     when x"21", -- .				34
			    c_ascii	    when x"22", -- 0 to 9		35
			    d_ascii     when x"23", -- 0 to 9		36
			    u_ascii	    when x"24", -- 0 to 9		37
				   x"20"	    when x"25", -- blank		38
				   x"6D"     when x"26", -- m				39
				   x"73"     when x"27", -- s				40
               -- Page 4 Line 4
               x"54"     when x"28", -- T				41
	            x"77"     when x"29", -- w				42
				   x"69"     when x"2A", -- i				43
				   x"64"     when x"2B", -- d				44
				   x"2D"     when x"2C", -- -				45
				   x"46"     when x"2D", -- F				46
				   x"4C"     when x"2E", -- L				47
               x"3A"     when x"2F", -- :				48
				   x"20"     when x"30", -- blank		49
				cm_ascii     when x"31", -- 0 to 9		50
			   dm_ascii     when x"32", -- 0 to 9		51
			    m_ascii     when x"33", -- 0 to 9		52
				   x"2E"     when x"34", -- .				53
				 c_ascii 	 when x"35", -- 0 to 9		54
			    d_ascii   	 when x"36", -- 0 to 9		55
			    u_ascii 	 when x"37", -- 0 to 9		56
				   x"20" 	 when x"38", -- blank		57
				   x"E4"     when x"39", -- 				58
				   x"73"     when x"3A", -- s				59
				   x"7C"     when x"3B", -- |				60
				   x"7C"     when x"3C", -- |				61
               x"54"     when x"3D", -- T				62
	            x"77"     when x"3E", -- w				63
				   x"69"     when x"3F", -- i				64
				   x"64"     when x"40", -- d				65
				   x"2D"     when x"41", -- -				66
				   x"44"     when x"42", -- D				67
				   x"4C"     when x"43", -- L				68
				   x"3A"     when x"44", -- :    		69
				   x"20"     when x"45", -- blank		70
			   cm_ascii  	 when x"46", -- 0 to 9		71
			   dm_ascii	 	 when x"47", -- 0 to 9		72
			    m_ascii  	 when x"48", -- 0 to 9 		73
				   x"2E"     when x"49", -- .				74
			    c_ascii	 	 when x"4A", -- 0 to 9		75
			    d_ascii  	 when x"4B", -- 0 to 9		76
			    u_ascii	 	 when x"4C", -- 0 to 9		77
				   x"20"	    when x"4D", -- blank		78
				   x"E4"     when x"4E", -- 				79
				   x"73"     when x"4F", -- s				80				 
               x"57"     when x"50", -- W				81
               x"20"     when others;-- blank

--------------------------------- Page 4_1 - Line 1 & Line 2 -----------------------------------------
					
with char_sel select

             -- Page 4_1 Line 1
ascii_P4_1L12 <= x"57"     when x"00", -- W			1
					  x"64"     when x"01", -- d			2
				     x"6C"     when x"02", -- l			3
				     x"79"     when x"03", -- y			4
				     x"2D"     when x"04", -- -			5
				     x"46"     when x"05", -- F			6
				     x"48"     when x"06", -- H			7
                 x"3A"     when x"07", -- :			8
				     x"20"     when x"08", -- blank		9
				  cm_ascii     when x"09", -- 0 to 9	10
				  dm_ascii     when x"0A", -- 0 to 9	11
			      m_ascii     when x"0B", -- 0 to 9	12
				     x"2E"     when x"0C", -- .			13
				   c_ascii	   when x"0D", -- 0 to 9	14
				   d_ascii     when x"0E", -- 0 to 9	15
				   u_ascii	   when x"0F", -- 0 to 9	16
					  x"20"	   when x"10", -- blank		17
					  x"6D"     when x"11", -- m			18
					  x"73"     when x"12", -- s			19
				     x"7C"     when x"13", -- |		   20 | = 7C
				     x"7C"     when x"14", -- |		   21 | = 7C
                 x"57"  	when x"15", -- W			22
	              x"64"     when x"16", -- d 			23
					  x"6C"     when x"17", -- l			24
		           x"79"     when x"18", -- y			25
		           x"2D"     when x"19", -- -			26
		           x"44"	   when x"1A", -- D			27
		           x"48" 		when x"1B", -- H			28
		           x"3A" 		when x"1C", -- :			29
		           x"20"     when x"1D", -- blank		30
		        cm_ascii     when x"1E", -- 0 to 9	31
		        dm_ascii  	when x"1F", -- 0 to 9	32
		         m_ascii  	when x"20", -- 0 to 9	33
			        x"2E"     when x"21", -- .			34
		         c_ascii  	when x"22", -- 0 to 9	35
		         d_ascii  	when x"23", -- 0 to 9	36
			      u_ascii  	when x"24", -- 0 to 9	37
				     x"20" 	   when x"25", -- blank		38
			        x"6D"     when x"26", -- m			39
			        x"73"     when x"27", -- s			40	
					-- Page 4_1 Line 2
                 x"57"     when x"28", -- W			41
	              x"77"     when x"29", -- w			42
				     x"69"     when x"2A", -- i			43
				     x"64"     when x"2B", -- d			44
				     x"2D"     when x"2C", -- -			45
				     x"46"     when x"2D", -- F			46
				     x"48"     when x"2E", -- H			47
				     x"3A"   	when x"2F", -- :			48
				     x"20" 	   when x"30", -- blank		49
				  cm_ascii  	when x"31", -- 0 to 9	50
				  dm_ascii     when x"32", -- 0 to 9	51
				   m_ascii  	when x"33", -- 0 to 9	52
				     x"2E"  	when x"34", -- .			53
				   c_ascii	 	when x"35", -- 0 to 9	54
				   d_ascii  	when x"36",--  0 to 9	55
				   u_ascii	 	when x"37", -- 0 to 9	56
				     x"20"	 	when x"38", -- blank		57
				     x"6D"     when x"39", -- m			58
				     x"73"     when x"3A", -- s			59
				     x"7C"     when x"3B", -- |		   60 | = 7C
				     x"7C"     when x"3C", -- |			61 | = 7C
				     x"57"     when x"3D", -- W			62
	              x"77"     when x"3E", -- w			63
				     x"69"     when x"3F", -- i			64
				     x"64"     when x"40", -- d			65
				     x"2D"     when x"41", -- -			66
				     x"44"     when x"42", -- D			67
				     x"48"     when x"43", -- H			68
				     x"3A" 		when x"44", -- :			69
				     x"20"     when x"45", -- blank		70
			     cm_ascii     when x"46", -- 0 to 9	71
				  dm_ascii		when x"47", -- 0 to 9	72
			      m_ascii     when x"48", -- 0 to 9	73
				     x"2E"     when x"49", -- .			74
				   c_ascii	   when x"4A", -- 0 to 9	75
				   d_ascii     when x"4B", -- 0 to 9	76
				   u_ascii	   when x"4C", -- 0 to 9	77
				     x"20"     when x"4D", -- blank		78
				     x"6D"     when x"4E", -- m			79
				     x"73"     when x"4F", -- s			80
					  x"57"     when x"50", -- W			81
                 x"20"     when others;-- blank      
					
--------------------------------- Page 4_1 - Line 3 & Line 4 -----------------------------------------
					
with char_sel select					
             -- Page 4_1 Line 3
ascii_P4_1L34 <= x"54"     when x"00", -- T			1
                 x"77"     when x"01", -- w			2	
                 x"69"     when x"02", -- i			3
					  x"64"     when x"03", -- d 			4
					  x"2D"     when x"04", -- - 			5
					  x"46"     when x"05", -- F			6
				     x"48"     when x"06", -- H 			7
					  x"3A"     when x"07", -- : 			8
				     x"20"	   when x"08", -- blank 	9
				  cm_ascii     when x"09", -- 0 to 9	10
				  dm_ascii     when x"0A", -- 0 to 9	11
			      m_ascii 	   when x"0B", -- 0 to 9  	12
				     x"2E"     when x"0C", -- .			13
				   c_ascii     when x"0D", -- 0 to 9	14
				   d_ascii     when x"0E", -- 0 to 9	15
				   u_ascii     when x"0F", -- 0 to 9	16
					  x"20"     when x"10", -- blank		17
				     x"E4"     when x"11", -- 			18
					  x"73"     when x"12", -- s			19
					  x"7C"     when x"13", -- |			20 | = 7C
				     x"7C"     when x"14", -- |			21 | = 7C
					  x"54"     when x"15", -- T			22
                 x"77"     when x"16", -- w			23	
                 x"69"     when x"17", -- i			24
					  x"64"     when x"18", -- d			25
					  x"2D"     when x"19", -- -			26
					  x"44"     when x"1A", -- D			27
				     x"48"     when x"1B", -- H			28
					  x"3A"     when x"1C", -- :			29
					  x"20"		when x"1D", -- blank		30
				  cm_ascii     when x"1E", -- 0 to 9	31
				  dm_ascii		when x"1F", -- 0 to 9	32
				   m_ascii     when x"20", -- 0 to 9	33
					  x"2E"     when x"21", -- .			34
				   c_ascii     when x"22", -- 0 to 9	35
				   d_ascii     when x"23", -- 0 to 9	36
				   u_ascii     when x"24", -- 0 to 9	37
					  x"20"     when x"25", -- blank		38
					  x"E4"     when x"26", --         39
					  x"73"     when x"27", -- s     	40
               -- Page 4_1 Line 4
				     x"20"     when x"28", -- blank		41
					  x"20"     when x"29", -- blank		42	
                 x"20"     when x"2A", -- blank		43
				     x"20"     when x"2B", -- blank		44
				     x"20"     when x"2C", -- blank		45
					  x"20"     when x"2D", -- blank		46
					  x"20"     when x"2E", -- blank		47
					  x"20"     when x"2F", -- blank		48
				     x"41"     when x"30", -- A			49
				     x"6C"     when x"31", -- l 			50
				     x"6C"     when x"32", -- l			51
				     x"20"		when x"33", -- blank		52
				     x"64"     when x"34", -- d			53
				     x"65"     when x"35", -- e			54
				     x"6C"     when x"36", -- l			55
				     x"61"     when x"37", -- a			56
				     x"79"     when x"38", -- y			57
				     x"73"     when x"39", -- s	  		58
				     x"20"     when x"3A", -- blank		59
					  x"66"     when x"3B", -- f			60 | = 7C
				     x"72"     when x"3C", -- r			61 | = 7C
					  x"6F"     when x"3D", -- o			62
                 x"6D"     when x"3E", -- m			63
					  x"20"     when x"3F", -- blank		64
					  x"43"     when x"40", -- C			65
					  x"30"     when x"41", -- 0			66
					  x"20"     when x"42", -- blank		67
					  x"70"     when x"43", -- p			68
					  x"75"     when x"44", -- u  		69
					  x"6C"     when x"45", -- l		   70
				     x"73"     when x"46", -- s	   	71
				     x"65"	   when x"47", -- e			72
				 	  x"20"     when x"48", -- blank	  	73
					  x"20"     when x"49", -- blank	  	74
				     x"20"     when x"4A", -- blank	  	75
				     x"20"     when x"4B", -- blank	 	76
					  x"20"     when x"4C", -- blank		77
					  x"20"     when x"4D", -- blank	   78
					  x"20"     when x"4E", -- blank 	79
					  x"20"     when x"4F", -- blank	   80	
				     x"54"     when x"50", -- T			81				 
                 x"20"     when others;-- blank
					
---------------------------------------------- LCD pages selection --------------------------------------------------------

sel_l12_l34 <= ( not nstrobe_en1 ) and nstrobe_en2;

process (CLK)
begin	
	if CLK'event and CLK = '1' then
	   if sel_l12_l34 = '0' then
		   P1_ascii <= ascii_P1L12;
			P1_1_ascii <= ascii_P1_1L12;
			P2_ascii <= ascii_P2L12;
			P2_1_ascii <= ascii_P2_1L12;
			P3_ascii <= ascii_P3L12;
			P3_1_ascii <= ascii_P3_1L12;
			P4_ascii <= ascii_P4L12;
			P4_1_ascii <= ascii_P4_1L12;			
		else
		   P1_ascii <= ascii_P1L34;
			P1_1_ascii <= ascii_P1_1L34;
			P2_ascii <= ascii_P2L34;
			P2_1_ascii <= ascii_P2_1L34;
			P3_ascii <= ascii_P3L34;
			P3_1_ascii <= ascii_P3_1L34;
			P4_ascii <= ascii_P4L34;
			P4_1_ascii <= ascii_P4_1L34;			
		end if;
	 end if;
end process;

--------------------------------------------------- KeyPad interface ---------------------------------------------------

-- Active low push-button NKEY

key1 <= not NKEY1;
key2 <= not NKEY2;
key3 <= not NKEY3;
key4 <= not NKEY4;

process (CLK)
begin	
	if CLK'event and CLK = '1' then
		if ( RESET='1' ) then
			key1_1 <= '0';
			key1_2 <= '0';
			key2_1 <= '0';
			key2_2 <= '0';
			key3_1 <= '0';
			key3_2 <= '0';
			key4_1 <= '0';
			key4_2 <= '0';
		elsif tick_rise_1MHz = '1' then
			key1_1 <= key1;
			key1_2 <= key1_1;
			key2_1 <= key2;
			key2_2 <= key2_1;
			key3_1 <= key3;
			key3_2 <= key3_1;
			key4_1 <= key4;
			key4_2 <= key4_1;
		end if;
	end if;
end process;

-- rising edge detection
rkey1 <= key1_1 and ( not key1_2 );
rkey2 <= key2_1 and ( not key2_2 );
rkey3 <= key3_1 and ( not key3_2 );
rkey4 <= key4_1 and ( not key4_2 );

J1 <= rkey1;
K1 <= rkey2 or rkey3 or rkey4;

process(CLK, RESET) 
begin
   if ( RESET = '1' ) then
	     QJK1 <= '0';
   elsif (CLK'event and CLK = '1') then
	  if tick_fall_1MHz = '1' then
      if (J1 = '0' and K1 = '0') then
          QJK1 <= QJK1; -- hold : no change
		elsif (J1 = '1' and K1 ='0') then
		    QJK1 <= '1';  -- set
		elsif (J1 = '0' and K1 ='1') then
		    QJK1 <= '0';  -- reset
		elsif (J1 = '1' and K1 ='1') then
		    QJK1 <= QJK1; -- hold : no change
		end if;
	  end if;
	end if;
end process;	

state1 <= QJK1;

J2 <= rkey2;
K2 <= rkey1 or rkey3 or rkey4;

process(CLK, RESET) 
begin
   if ( RESET = '1' ) then
	     QJK2 <= '1';
   elsif (CLK'event and CLK = '1') then
	  if tick_fall_1MHz = '1' then
      if (J2 = '0' and K2 = '0') then
          QJK2 <= QJK2; -- hold : no change
		elsif (J2 = '1' and K2 ='0') then
		    QJK2 <= '1';  -- set
		elsif (J2 = '0' and K2 ='1') then
		    QJK2 <= '0';  -- reset
		elsif (J2 = '1' and K2 ='1') then
		    QJK2 <= QJK2; -- hold : no change
		end if;
	  end if;
	end if;
end process;			

state2 <= QJK2;

J3 <= rkey3;
K3 <= rkey1 or rkey2 or rkey4;

process(CLK, RESET) 
begin
   if ( RESET = '1' ) then
	     QJK3 <= '0';
   elsif (CLK'event and CLK = '1') then
	  if tick_fall_1MHz = '1' then
      if (J3 = '0' and K3 = '0') then
          QJK3 <= QJK3; -- hold : no change
		elsif (J3 = '1' and K3 ='0') then
		    QJK3 <= '1';  -- set
		elsif (J3 = '0' and K3 ='1') then
		    QJK3 <= '0';  -- reset
		elsif (J3 = '1' and K3 ='1') then
		    QJK3 <= QJK3; -- hold : no change
		end if;
	  end if;
	end if;
end process;			

state3 <= QJK3;

J4 <= rkey4;
K4 <= rkey1 or rkey2 or rkey3;

process(CLK, RESET) 
begin
   if ( RESET = '1' ) then
	     QJK4 <= '0';
   elsif (CLK'event and CLK = '1') then
	  if tick_fall_1MHz = '1' then
      if (J4 = '0' and K4 = '0') then
          QJK4 <= QJK4; -- hold : no change
		elsif (J4 = '1' and K4 ='0') then
		    QJK4 <= '1';  -- set
		elsif (J4 = '0' and K4 ='1') then
		    QJK4 <= '0';  -- reset
		elsif (J4 = '1' and K4 ='1') then
		    QJK4 <= QJK4; -- hold : no change
		end if;
	  end if;
	end if;
end process;			

state4 <= QJK4;

process(CLK) 
begin   
	if (CLK'event and CLK = '1') then
	   if ( RESET='1' ) then
	        ewrp1 <= '0';
	        ewrp2 <= '0';
	   elsif tick_fall_1MHz = '1' then
	        ewrp1 <= end_wr_page;
	        ewrp2 <= ewrp1;
	   end if;
	end if;
end process;

ce_key <= ( not ewrp1 ) and ewrp2;

process(CLK) -- Wait end of write status before page changing.
begin
	 if (CLK'event and CLK = '1') then
	    if ( ce_key and tick_rise_1MHz ) = '1' then
		    sel_px1 <= state1;
			 sel_px2 <= state2;
			 sel_px3 <= state3;
			 sel_px4 <= state4;			 
		 end if;
	 end if;
end process;

sel_px <= sel_px4 & sel_px3 & sel_px2 & sel_px1;
--sel_cst_data <= sel_sub_px & sel_px & sel_data_mux; -- sub page & page number (1 to 4) & LCD zone (0 to 7)
sel_page <= sel_sub_px & sel_px;	 

with sel_page select

Px_ascii <= P1_ascii   when x"01",
            P1_1_ascii when x"11",
            P2_ascii   when x"02",
				P2_1_ascii when x"22",
				P3_ascii   when x"04",
				P3_1_ascii when x"44",
				P4_ascii   when x"08",
				P4_1_ascii when x"88",
				P2_ascii   when others;

process(CLK) 
begin
	if (CLK'event and CLK = '1') then
	    if tick_fall_1MHz = '1' then
		    lcd_din <= Px_Ascii; -- To LCD data register
		 end if;
	end if;
end process;

-----------------------------------------------------Sub pages selection---------------------------------------------------

process(CLK) 
begin
	if (CLK'event and CLK = '1') then
	   if ( RESET='1' ) then
		    page1_1 <= '0';
			 page2_1 <= '0';
			 page3_1 <= '0';
			 page4_1 <= '0';
		elsif tick_fall_1MHz = '1' then
			if K1 = '1' then 
				page1_1 <= '0';
			elsif ( rkey1 and sel_px1 ) = '1' then
				page1_1 <= not page1_1;
			end if;
			if K2 = '1' then 
				page2_1 <= '0';
			elsif ( rkey2 and sel_px2 ) = '1' then
		      page2_1 <= not page2_1;
		   end if;
			if K3 = '1' then 
		      page3_1 <= '0';
	      elsif ( rkey3 and sel_px3 ) = '1' then
		      page3_1 <= not page3_1;
		   end if;
			if K4 = '1' then 
		      page4_1 <= '0';
	      elsif ( rkey4 and sel_px4 ) = '1' then
		      page4_1 <= not page4_1;
		   end if;			
		end if;
	end if;
end process;

sel_sub_px <= page4_1 & page3_1 & page2_1 & page1_1;

-------------------------------------- 8-Bit parallel to serial converter with the MSB bit first ----------------------------
------------------------------------------------ LCD data convert in serial -------------------------------------------------
process (CLK)
begin	
	if CLK'event and CLK = '1' then
		if (RESET='1') then
			rst1  <= '0';
			rst2  <= '0';
			load1 <= '0';
		elsif tick_rise_1MHz = '1' then
			rst1  <= lcd_en;
			rst2  <= rst1;
			load1 <= rst2;
		end if;
	end if;
end process;

init_rst <= rst1 and ( not rst2 );
ld_p2s <= rst2 and ( not load1 );

process (CLK)
begin	
	if CLK'event and CLK = '1' then
		if ( RESET='1' ) then
			st1 <= '0';
			st2 <= '0';
		elsif tick_fall_1MHz = '1' then
			st1 <= load1;
			st2 <= st1;
		end if;
	end if;
end process;

start_spi <= st1 and ( not st2 );

process (CLK)
begin	
	if CLK'event and CLK = '1' then
	    if (RESET='1') then
				spi_cs <= '0'; 
				stop_cnt <= '0'; 
				rclk <= '0';
       elsif tick_rise_1MHz = '1' then			
				if stopcnt = '1' then        
					spi_cs <= '0'; 
				elsif start_spi = '1' then
					spi_cs <= start_spi;    
				end if;
		      stop_cnt <= stopcnt;    -- Resets the gate8 counter
		      rclk <= stop_cnt;
		 end if;
	end if;
end process;

--Count 8 clock rising edges for write cycle
process (CLK)
begin	
	if CLK'event and CLK = '1' then
	   if (RESET='1') then
				gate8_cnt <= (others => '0'); 
		else 
		  if tick_fall_1MHZ = '1' then
			  if stop_cnt = '1' then
				  gate8_cnt <= (others => '0');
			  elsif spi_cs = '1' then
				  gate8_cnt <= gate8_cnt + 1;
			  end if;
		  end if;
	   end if;
	end if;
end process;

stopcnt <= '1' when gate8_cnt = 8 else '0'; --high during 8 clock rising edges

mode <= spi_cs & ld_p2s;

rst_p2s <= stop_cnt or init_rst; -- synchronous reset of the 8-Bit parallel to serial converter

process (CLK)
begin	
	if CLK'event and CLK = '1' then
		if tick_fall_1MHz = '1' then
		  if rst_p2s = '1' then 
				LCD_SDOUT <= '0';
				idata <= x"00";
		  else				
			case mode is			
				when "00" => null;                     -- no operation
				when "01" => idata <= lcd_dout;        -- load operation
				when "10" => LCD_SDOUT <= idata(7);    -- shift left
						for mloop in 6 downto 0 loop     -- LCD_SDOUT = sn74ahc595d SER data pin
							idata(mloop+1) <= idata(mloop);
						end loop;
				when others => null;                   -- no operation otherwise
			 end case;
		   end if;
		 end if;
	end if;
end process;

process (CLK)
begin	
	if CLK'event and CLK = '1' then
		if ( RESET='1' ) then
			 lcd_cs <= '0';
		elsif tick_fall_1MHz = '1' then
			   lcd_cs <= spi_cs;
		end if;
	end if;
end process;

LCD_NCS <= rclk;                --sn74ahc595d register clock RCLK
LCD_CLK <= CLK_1MHz and lcd_cs; --sn74ahc595d gated serial register clock SRCLK
										  
end Behavioral;



