--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package lcd_pkg is

	component LCD4x40_Interface 

		 Port ( Fraw_ADC : in STD_LOGIC_VECTOR (17 downto 0); -- F-ADC values without calibration
				  Draw_ADC : in STD_LOGIC_VECTOR (17 downto 0); -- D-ADC values without calibration
				  Fcal_ADC : in STD_LOGIC_VECTOR (17 downto 0); -- F-ADC values with calibration
				  Dcal_ADC : in STD_LOGIC_VECTOR (17 downto 0); -- D-ADC values with calibration
				  Fraw_DRDY, Draw_DRDY, Fcal_DRDY, Dcal_DRDY : in STD_LOGIC; -- F&D Data ready for cal and uncal ADC
				  F_LIM : in STD_LOGIC_VECTOR (17 downto 0); -- F : +/- ADC input limits
				  D_LIM : in STD_LOGIC_VECTOR (17 downto 0); -- D : +/- ADC input limits
				  F_PREF, D_PREF, F_NREF, D_NREF : in STD_LOGIC_VECTOR (17 downto 0); -- F&D positive and negative acquired values after +/-Vdac 
				  FL_Wwid, FH_Wwid, DL_Wwid, DH_Wwid : in STD_LOGIC_VECTOR (31 downto 0); -- F&D High & Low window widths
				  FL_Wdly, FH_Wdly, DL_Wdly, DH_Wdly : in STD_LOGIC_VECTOR (31 downto 0); -- F&D High & Low window delays
				  FL_Twid, FH_Twid, DL_Twid, DH_Twid : in STD_LOGIC_VECTOR (31 downto 0); -- F&D High & Low trigger delays
				  F_GCC : in STD_LOGIC_VECTOR (31 downto 0); -- F-gain calibration coefficient
				  D_GCC : in STD_LOGIC_VECTOR (31 downto 0); -- D-gain calibration coefficient
				  F_OCC : in STD_LOGIC_VECTOR (17 downto 0); -- F-offset calibration coefficient
				  D_OCC : in STD_LOGIC_VECTOR (17 downto 0); -- D-offset calibration coefficient
				  C_DAC : in STD_LOGIC_VECTOR (19 downto 0); -- Calibration DAC in volt (read register) 
				  F_OVR, D_OVR : in STD_LOGIC; -- F & D ADC over-range
				  AF_COIL : in STD_LOGIC_VECTOR (31 downto 0); -- F coil area
				  AD_COIL : in STD_LOGIC_VECTOR (31 downto 0); -- D coil area
				  KF_BMEAS : in STD_LOGIC_VECTOR (31 downto 0); -- F-Ponderation coefficient for Bmeas
				  KD_BMEAS : in STD_LOGIC_VECTOR (31 downto 0); -- D-Ponderation coefficient for Bmeas
				  KF_BDOT : in STD_LOGIC_VECTOR (31 downto 0); -- F-Ponderation coefficient for Bdot 
				  KD_BDOT : in STD_LOGIC_VECTOR (31 downto 0); -- D-Ponderation coefficient for Bdot
				  B_DOT : in STD_LOGIC_VECTOR (31 downto 0); -- dB/dt 
				  B_DOT_DRDY : in STD_LOGIC;
				  B_MEAS : in STD_LOGIC_VECTOR (31 downto 0); -- Measured magnetic field
				  B_MEAS_DRDY : in STD_LOGIC;
			     B_HOLD_TIME : in STD_LOGIC_VECTOR (31 downto 0); -- Latched magnetic field at C0 + B_HOLD_TIME (in ms)
				  B_HOLD : in STD_LOGIC_VECTOR (31 downto 0); -- Latched magnetic field
			     B_THRESHOLD : in STD_LOGIC_VECTOR (31 downto 0); -- Threshold of the latched magnetic field
				  Bdot_HOLD : in STD_LOGIC_VECTOR (31 downto 0); -- Latched magnetic field
			     Bdot_THRESHOLD : in STD_LOGIC_VECTOR (31 downto 0); -- Threshold of the latched magnetic field
				  Z0_DLY : in STD_LOGIC_VECTOR (31 downto 0); -- Delay apply to the zero cycle pulse
				  BCD_COUNTDOWN : in STD_LOGIC_VECTOR (31 downto 0); -- Calibration time countdown in BCD format
				  WR_UPD : in STD_LOGIC_VECTOR (31 downto 0); -- White Rabbit update rate 
				  CAL_STATUS : in STD_LOGIC_VECTOR (3 downto 0); -- Calibration status
				  CAL_ERROR : in STD_LOGIC_VECTOR (3 downto 0); -- Calibration error
				  LCD_RD_NWR : out STD_LOGIC; -- LCD read/write signal (not used) always in write mode
				  LCD_REG_SEL : out STD_LOGIC; -- LCD register select signal
				  LCD_ENABLE1 : out STD_LOGIC; -- Line 1 & 2 LCD enable signal
				  LCD_ENABLE2 : out STD_LOGIC; -- Line 3 & 4 LCD enable signal
				  LCD_SDOUT : out STD_LOGIC := '0'; -- LCD Data in serial (SPI transmission)
				  LCD_NCS : out STD_LOGIC; -- LCD Data chip select (SPI transmission)
				  LCD_CLK : out STD_LOGIC; -- LCD clock (SPI transmission)
				  OPER_SPARE : in STD_LOGIC; -- Operational/ Spare syatem selection 
				  EFF_SIMUL : in STD_LOGIC; -- Effective / Simulated magnetic field
				  CLK : in STD_LOGIC; -- 10MHz
				  CLK_100MHz_i : in STD_LOGIC; -- 100MHz
				  RESET : in STD_LOGIC := '1'; -- Active high
				  NKEY1 : in STD_LOGIC;   -- Key n1 keypad
				  NKEY2 : in STD_LOGIC;   -- Key n2 keypad
				  NKEY3 : in STD_LOGIC;   -- Key n3 keypad
				  NKEY4 : in STD_LOGIC ); -- Key n4 keypad
				  
	end component;

end lcd_pkg;

package body lcd_pkg is

---- Example 1
--  function <function_name>  (signal <signal_name> : in <type_declaration>  ) return <type_declaration> is
--    variable <variable_name>     : <type_declaration>;
--  begin
--    <variable_name> := <signal_name> xor <signal_name>;
--    return <variable_name>; 
--  end <function_name>;

---- Example 2
--  function <function_name>  (signal <signal_name> : in <type_declaration>;
--                         signal <signal_name>   : in <type_declaration>  ) return <type_declaration> is
--  begin
--    if (<signal_name> = '1') then
--      return <signal_name>;
--    else
--      return 'Z';
--    end if;
--  end <function_name>;

---- Procedure Example
--  procedure <procedure_name>  (<type_declaration> <constant_name>  : in <type_declaration>) is
--    
--  begin
--    
--  end <procedure_name>;
 
end lcd_pkg;
