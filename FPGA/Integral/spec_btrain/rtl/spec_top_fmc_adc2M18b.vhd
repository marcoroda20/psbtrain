----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    10:29:08 06/15/2012 
-- Design Name: 
-- Module Name:    spec_top_fmc_adc2M18b - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versionsp: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.fmc_adc2M18b2ch_pkg.all;
use work.lcd_pkg.all;
use work.utils_pkg.all;
use work.wr_btrain_pkg.all;
use work.sdb_meta_pkg.all;
use work.gn4124_core_pkg.all;
use work.ddr3_ctrl_pkg.all;
use work.gencores_pkg.all;
use work.wrcore_pkg.all;
use work.wr_fabric_pkg.all;
use work.wr_xilinx_pkg.all;
use work.wishbone_pkg.all;
use work.streamers_pkg.all;

entity spec_top_fmc_adc2m18b is
  generic (
    -- Simulation mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the testbench.
    -- Its purpose is to reduce some internal counters/timeouts to speed up simulations.
    g_simulation : integer := 0
    );
	port(
      -- Local oscillator
      clk_20MHz_i : in std_logic;      -- 20MHz VCXO clock
		reset_n_hw_i  : in std_logic;
		
      clk_125m_pllref_p_i : in std_logic;  -- 125 MHz PLL reference
      clk_125m_pllref_n_i : in std_logic;

      fpga_pll_ref_clk_101_p_i : in std_logic;  -- Dedicated clock for Xilinx GTP transceiver
      fpga_pll_ref_clk_101_n_i : in std_logic;

      -- Auxiliary pins
      aux_leds_o    : out std_logic_vector(3 downto 0);
      aux_buttons_i : in  std_logic_vector(1 downto 0);

      -- PCB version
      pcb_ver_i : in std_logic_vector(3 downto 0);

		-- I2C bus connected to the EEPROM on the DIO mezzanine. This EEPROM is used
		-- for storing WR Core's configuration parameters.
		fmc_scl_b : inout std_logic;
		fmc_sda_b : inout std_logic;

      -- FMC slot management
      fmc0_prsnt_m2c_n_i : in std_logic;  -- Mezzanine present (active low)
		
      -- FMC 1-wire interface (DS18B20 thermometer + unique ID)
      fmc_one_wire_b : inout std_logic;    -- 1-Wire interface to DS18B20

      -- Carrier 1-wire interface (DS18B20 thermometer + unique ID)
      carrier_one_wire_b : inout std_logic;-- 1-Wire interface to DS18B20

      -- GN4124 interface
      L_CLKp       : in    std_logic;                      -- Local bus clock (frequency set in GN4124 config registers)
      L_CLKn       : in    std_logic;                      -- Local bus clock (frequency set in GN4124 config registers)
      L_RST_N      : in    std_logic;                      -- Reset from GN4124 (RSTOUT18_N)
      P2L_RDY      : out   std_logic;                      -- Rx Buffer Full Flag
      P2L_CLKn     : in    std_logic;                      -- Receiver Source Synchronous Clock-
      P2L_CLKp     : in    std_logic;                      -- Receiver Source Synchronous Clock+
      P2L_DATA     : in    std_logic_vector(15 downto 0);  -- Parallel receive data
      P2L_DFRAME   : in    std_logic;                      -- Receive Frame
      P2L_VALID    : in    std_logic;                      -- Receive Data Valid
      P_WR_REQ     : in    std_logic_vector(1 downto 0);   -- PCIe Write Request
      P_WR_RDY     : out   std_logic_vector(1 downto 0);   -- PCIe Write Ready
      RX_ERROR     : out   std_logic;                      -- Receive Error
      L2P_DATA     : out   std_logic_vector(15 downto 0);  -- Parallel transmit data
      L2P_DFRAME   : out   std_logic;                      -- Transmit Data Frame
      L2P_VALID    : out   std_logic;                      -- Transmit Data Valid
      L2P_CLKn     : out   std_logic;                      -- Transmitter Source Synchronous Clock-
      L2P_CLKp     : out   std_logic;                      -- Transmitter Source Synchronous Clock+
      L2P_EDB      : out   std_logic;                      -- Packet termination and discard
      L2P_RDY      : in    std_logic;                      -- Tx Buffer Full Flag
      L_WR_RDY     : in    std_logic_vector(1 downto 0);   -- Local-to-PCIe Write
      P_RD_D_RDY   : in    std_logic_vector(1 downto 0);   -- PCIe-to-Local Read Response Data Ready
      TX_ERROR     : in    std_logic;                      -- Transmit Error
      VC_RDY       : in    std_logic_vector(1 downto 0);   -- Channel ready
      GPIO         : inout std_logic_vector(1 downto 0);   -- GPIO[0] -> GN4124 GPIO8
                                                           -- GPIO[1] -> GN4124 GPIO9
      -- DDR3 interface
      DDR3_CAS_N   : out   std_logic;
      DDR3_CK_N    : out   std_logic;
      DDR3_CK_P    : out   std_logic;
      DDR3_CKE     : out   std_logic;
      DDR3_LDM     : out   std_logic;
      DDR3_LDQS_N  : inout std_logic;
      DDR3_LDQS_P  : inout std_logic;
      DDR3_ODT     : out   std_logic;
      DDR3_RAS_N   : out   std_logic;
      DDR3_RESET_N : out   std_logic;
      DDR3_UDM     : out   std_logic;
      DDR3_UDQS_N  : inout std_logic;
      DDR3_UDQS_P  : inout std_logic;
      DDR3_WE_N    : out   std_logic;
      DDR3_DQ      : inout std_logic_vector(15 downto 0);
      DDR3_A       : out   std_logic_vector(13 downto 0);
      DDR3_BA      : out   std_logic_vector(2 downto 0);
      DDR3_ZIO     : inout std_logic;
      DDR3_RZQ     : inout std_logic;
		
      -- FMC slot	
		ADC_CLK_P_i		: in std_logic;
		ADC_CLK_N_i		: in std_logic;

		--FMC slot DIO
		SYS_DIO_io	: inout std_logic_vector(NB_SYS_IO-1 downto 0);
		DIR_DIO_o	: out std_logic_vector(NB_SYS_IO-1 downto 0);
	
		--LCD display I/O
		LCD_SDOUT	: out STD_LOGIC := '0'; -- LCD Data in serial (SPI transmission)
		LCD_NCS		: out STD_LOGIC;        -- LCD Data chip select (SPI transmission)
		LCD_CLK		: out STD_LOGIC;        -- LCD clock (SPI transmission)
		N_KEY1		: in STD_LOGIC;         -- Key n1 keypad
		N_KEY2		: in STD_LOGIC;         -- Key n2 keypad
		N_KEY3		: in STD_LOGIC;         -- Key n3 keypad
		N_KEY4		: in STD_LOGIC;         -- Key n4 keypad

		--FMC slot Differential IO
		opsp_cmd_o	  : out std_logic;
		opsp_status_i : in std_logic;
		
		--ADC
		ADC_CNV_o		: out std_logic;
		
		ADC1_SDI_o		: out std_logic;
		ADC1_SDO_i		: in std_logic;
		ADC1_SCK_o		: out std_logic;

		ADC2_SDI_o		: out std_logic;
		ADC2_SDO_i		: in std_logic;
		ADC2_SCK_o		: out std_logic;
		
		--DAC
		ISO_DAC_SDIN_o		: out std_logic;
		ISO_DAC_SDO_i		: in std_logic;
		ISO_DAC_SCLK_o		: out std_logic;
		
		ISO_DAC_LDAC_o	: out std_logic;
		ISO_DAC_SYNC_o	: out std_logic;
		ISO_DAC_RESET_o: out std_logic;
		ISO_DAC_CLR_o	: out std_logic;
		
		--Digital Input/Output
		OFF1_BIT_o	: out std_logic;
		GAIN1_BIT_o	: out std_logic;
		OPEN1_BIT_o	: out std_logic;

		OFF2_BIT_o	: out std_logic;
		GAIN2_BIT_o	: out std_logic;
		OPEN2_BIT_o	: out std_logic;
		
		FM_TRIG_o		: out std_logic;
		OVER_RANGE_o	: out std_logic;
		OPER_SPARE_o	: out std_logic;

		AUTO_CAL_o		: out std_logic;

		--DAC for VCXO
		dac_sclk_o  : out std_logic;
		dac_din_o   : out std_logic;
		dac_clr_n_o : out std_logic;
		dac_cs1_n_o : out std_logic;
		dac_cs2_n_o : out std_logic;

      -------------------------------------------------------------------------
      -- SFP pins
      -------------------------------------------------------------------------

      sfp_txp_o : out std_logic;
      sfp_txn_o : out std_logic;

      sfp_rxp_i : in std_logic;
      sfp_rxn_i : in std_logic;

		-- SFP MOD_DEF0 pin (used as a tied-to-ground SFP insertion detect line)
		sfp_det_i         : in    std_logic;
		-- SFP MOD_DEF1 pin (SCL line of the I2C EEPROM inside the SFP)
		sfp_scl_b         : inout std_logic;
		-- SFP MOD_DEF1 pin (SDA line of the I2C EEPROM inside the SFP)
		sfp_sda_b         : inout std_logic;
		-- SFP RATE_SELECT pin. Unused for most SFPs, in our case tied to 0.
		sfp_rate_select_b : inout std_logic;
		-- SFP laser fault detection pin. Unused in our design.
		sfp_tx_fault_i    : in    std_logic;
		-- SFP laser disable line. In our case, tied to GND.
		sfp_tx_disable_o  : out   std_logic;
		-- SFP-provided loss-of-link detection. We don't use it as Ethernet PCS
		-- has its own loss-of-sync detection mechanism.
		sfp_los_i         : in    std_logic;

		-- Green LED next to the SFP: indicates if the link is up.
		led_sfp_green_o : out std_logic;

		-- Red LED next to the SFP: blinking indicates that packets are being
		-- transferred.
		led_sfp_red_o : out std_logic;

      -----------------------------------------
      --UART
      -----------------------------------------
      uart_rxd_i : in  std_logic;
      uart_txd_o : out std_logic;
		
      -----------------------------------------
      --Control bus for DAC card (output current and tension)
      -----------------------------------------
		dac_extern_clk_o : out std_logic;
		dac_extern_cs_o  : out std_logic;
		dac_extern_sdo_o : out std_logic
	);
end spec_top_fmc_adc2m18b;

architecture Behavioral of spec_top_fmc_adc2m18b is

  ------------------------------------------------------------------------------
  -- SDB crossbar constants declaration
  --
  -- WARNING: All address in sdb and crossbar are BYTE addresses!
  ------------------------------------------------------------------------------

  -- Number of master port(s) on the wishbone crossbar
  constant c_NUM_WB_MASTERS : integer := 7;

  -- Number of slave port(s) on the wishbone crossbar
  constant c_NUM_WB_SLAVES : integer := 1;

  -- Wishbone master(s)
  constant c_MASTER_GENNUM : integer := 0;

  -- Wishbone slave(s)
  constant c_SLAVE_DMA       : integer := 0;  -- DMA controller in the Gennum core
  constant c_SLAVE_ONEWIRE   : integer := 1;  -- Carrier onewire interface
  constant c_SLAVE_SPEC_CSR  : integer := 2;  -- SPEC control and status registers
  constant c_SLAVE_INT       : integer := 3;  -- Interrupt controller
  constant c_SLAVE_FMC       : integer := 4;  -- FMC Peak Detector mezzanine

  -- Devices sdb description
  constant c_DMA_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"000000000000003F",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000601",
        version   => x"00000001",
        date      => x"20131107",
        name      => "WB-DMA.Control     ")));

  constant c_ONEWIRE_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"0000000000000007",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000602",
        version   => x"00000001",
        date      => x"20131107",
        name      => "WB-Onewire.Control ")));

  constant c_SPEC_CSR_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"000000000000001F",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000603",
        version   => x"00000001",
        date      => x"20131107",
        name      => "WB-SPEC-CSR        ")));

  constant c_INT_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"000000000000000F",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"00000604",
        version   => x"00000001",
        date      => x"20121116",
        name      => "WB-Int.Control     ")));

  -- f_xwb_bridge_manual_sdb(size, sdb_addr)
  constant c_FMC_SDB_BRIDGE : t_sdb_bridge := f_xwb_bridge_manual_sdb(x"00001fff", x"00004000");

  -- sdb header address
  constant c_SDB_ADDRESS : t_wishbone_address := x"00000000";

  -- Wishbone crossbar layout
  constant c_INTERCONNECT_LAYOUT : t_sdb_record_array(7 downto 0) :=
    (
      0 => f_sdb_embed_device(c_DMA_SDB_DEVICE, x"00001000"),
      1 => f_sdb_embed_device(c_ONEWIRE_SDB_DEVICE, x"00001100"),
      2 => f_sdb_embed_device(c_SPEC_CSR_SDB_DEVICE, x"00001200"),
      3 => f_sdb_embed_device(c_INT_SDB_DEVICE, x"00002000"),
      4 => f_sdb_embed_bridge(c_FMC_SDB_BRIDGE, x"00004000"),
      5 => f_sdb_embed_repo_url(c_SDB_REPO_URL),
      6 => f_sdb_embed_synthesis(c_SDB_SYNTHESIS),
      7 => f_sdb_embed_integration(c_SDB_INTEGRATION)
      );

  ------------------------------------------------------------------------------
  -- Other constants declaration
  ------------------------------------------------------------------------------

	--Simulation
	constant c_G_SIMULATON   : integer := g_simulation;
	
	--Carrier
	constant c_CARRIER_TYPE  : std_logic_vector(15 downto 0) := X"0002";

	--Start reset timer
	constant START_RESET_TIMER_SIM: integer := 20;
	constant START_RESET_TIMER		: integer := 100000000; -- 1s with a 100MHz clock
	--Start reset timer
	constant START_INIT_TIMER_SIM : integer := 2*START_RESET_TIMER_SIM;
	constant START_INIT_TIMER     : integer := 2*START_RESET_TIMER; -- 2s with a 100MHz clock
	
	--Button freeze timer
	constant BUTTON_FREEZE_SIM : integer := 100;
	constant BUTTON_FREEZE     : integer := 62500000; -- 1 sec latency with 62MHz

	--White Rabbit
	constant c_STREAMER_ETHERTYPE : std_logic_vector(15 downto 0) := x"dbff";	
	 
	--Delay line
	constant REG_DELAY_LONG : integer := 2;
		
	component irq_controller 
		port (
			-- Clock, reset
			clk_i   : in std_logic;
			rst_n_i : in std_logic;

			-- Interrupt sources input, must be 1 clk_i tick long
			irq_src_p_i : in std_logic_vector(31 downto 0);

			-- IRQ pulse output
			irq_p_o : out std_logic;

			-- Wishbone interface
			wb_adr_i : in  std_logic_vector(1 downto 0);
			wb_dat_i : in  std_logic_vector(31 downto 0);
			wb_dat_o : out std_logic_vector(31 downto 0);
			wb_cyc_i : in  std_logic;
			wb_sel_i : in  std_logic_vector(3 downto 0);
			wb_stb_i : in  std_logic;
			wb_we_i  : in  std_logic;
			wb_ack_o : out std_logic
		);
	end component;

--	component test_reg 
--		port (
--			rst_n_i                                  : in     std_logic;
--			wb_clk_i                                 : in     std_logic;
--			wb_data_i                                : in     std_logic_vector(31 downto 0);
--			wb_data_o                                : out    std_logic_vector(31 downto 0);
--			wb_cyc_i                                 : in     std_logic;
--			wb_sel_i                                 : in     std_logic_vector(3 downto 0);
--			wb_stb_i                                 : in     std_logic;
--			wb_we_i                                  : in     std_logic;
--			wb_ack_o                                 : out    std_logic;
--			-- Port for std_logic_vector field: 'LedReg' in reg: 'Test register SLV'
--			test_reg_trslv_led_o                     : out    std_logic_vector(3 downto 0);
--			test_reg_trslv_led_i                     : in     std_logic_vector(3 downto 0);
--			test_reg_trslv_led_load_o                : out    std_logic;
--			-- Port for std_logic_vector field: '28-bit value' in reg: 'Test register SLV'
--			test_reg_trslv_value_slv_o               : out    std_logic_vector(27 downto 0);
--			test_reg_trslv_value_slv_i               : in     std_logic_vector(27 downto 0);
--			test_reg_trslv_value_slv_load_o          : out    std_logic
--		);
--	end component;

  --Signal
	--System clocks
	signal sys_clk_fb_ref_s		: std_logic;
	signal sys_clk_fb_s			: std_logic;
	signal sys_clk_10_s			: std_logic;
	signal sys_clk_10_buf_s		: std_logic;
	signal sys_clk_62_5_s 		: std_logic; 
	signal sys_clk_62_5_buf_s	: std_logic; 
	signal sys_clk_100_s 		: std_logic; 
	signal sys_clk_100_buf_s	: std_logic; 
	signal ddr_clk_s 				: std_logic; 
	signal ddr_clk_buf_s 		: std_logic; 
	signal sys_clk_pll_locked_s: std_logic;
	
	signal sys_clk_in_cryst_s	    : std_logic; 
	signal sys_clk_in_oscil_s	    : std_logic; 
	signal sys_clk_in_oscil_test_s : std_logic;
	signal sys_clk_fb_1_s			 : std_logic;
	signal sys_clk_pll_locked_1_s	 : std_logic;

	signal clk_wr_sys				 : std_logic;
	signal pllout_clk_sys       : std_logic;
	signal pllout_clk_dmtd      : std_logic;
	signal pllout_clk_fb_pllref : std_logic;
	signal pllout_clk_fb_dmtd   : std_logic;

	signal clk_125m_pllref  		: std_logic;
	signal clk_125m_pllref_buff	: std_logic;
	signal clk_dmtd_s        		: std_logic;

	signal ADC_CLK_s  : std_logic; 
	signal ADC1_SCK_s : std_logic;
	signal ADC2_SCK_s : std_logic;

	--Reset
	signal reset_s         : std_logic; 
	signal reset_n_s       : std_logic;
	signal but_pci_rst_n_s : std_logic;
	signal sw_rst_fmc0_n_s : std_logic;

	signal reset_hw_s    : std_logic;
	signal reset_sw_s    : std_logic;
	signal en_hw_reset_s : std_logic;	

	signal start_timer_reset_s		: std_logic := '0';
	signal start_timer_counter_s	: integer := 0;	

	signal init_done_s          : std_logic := '0';
	signal start_init_counter_s : integer := 0;	
	
	-- Port for std_logic_vector field: 'Configuration' in reg: 'Digital I/O'
	signal reg_dio_cfg_s : std_logic_vector(9 downto 0);

	-- LCLK from GN4124 used as system clock
	signal l_clk : std_logic;

	-- Dedicated clock for GTP transceiver
	signal gtp_clk_s : std_logic;

	-- GN4124
	signal gn4124_status_s  : std_logic_vector(31 downto 0);
	signal p2l_pll_locked_s	: std_logic;
	
	-- DDR3
	signal ddr3_status_s     : std_logic_vector(31 downto 0);
	signal ddr3_calib_done_s : std_logic;

	-- Wishbone buse(s) from crossbar master port(s)
	signal cnx_master_out : t_wishbone_master_out_array(c_NUM_WB_MASTERS-1 downto 0);
	signal cnx_master_in  : t_wishbone_master_in_array(c_NUM_WB_MASTERS-1 downto 0);

	-- Wishbone buse(s) to crossbar slave port(s)
	signal cnx_slave_out : t_wishbone_slave_out_array(c_NUM_WB_SLAVES-1 downto 0);
	signal cnx_slave_in  : t_wishbone_slave_in_array(c_NUM_WB_SLAVES-1 downto 0);

	-- Wishbone address from GN4124 core (32-bit word address)
	signal gn_wb_adr : std_logic_vector(31 downto 0);

	-- Wishbone address from to DMA controller (32-bit word address)
	signal dma_ctrl_wb_adr : std_logic_vector(31 downto 0);

	-- GN4124 core DMA port to DDR wishbone bus
	signal wb_dma_adr   : std_logic_vector(31 downto 0);
	signal wb_dma_dat_i : std_logic_vector(31 downto 0);
	signal wb_dma_dat_o : std_logic_vector(31 downto 0);
	signal wb_dma_sel   : std_logic_vector(3 downto 0);
	signal wb_dma_cyc   : std_logic;
	signal wb_dma_stb   : std_logic;
	signal wb_dma_we    : std_logic;
	signal wb_dma_ack   : std_logic;
	signal wb_dma_stall : std_logic;
	signal wb_dma_err   : std_logic;
	signal wb_dma_rty   : std_logic;
	signal wb_dma_int   : std_logic;

	-- FMC BTrain core to DDR wishbone bus
	signal wb_ddr_adr   : std_logic_vector(31 downto 0);
	signal wb_ddr_dat_o : std_logic_vector(63 downto 0);
	signal wb_ddr_sel   : std_logic_vector(7 downto 0);
	signal wb_ddr_cyc   : std_logic;
	signal wb_ddr_stb   : std_logic;
	signal wb_ddr_we    : std_logic;
	signal wb_ddr_ack   : std_logic;
	signal wb_ddr_stall : std_logic;

	signal wb0_data_s   : std_logic_vector(63 downto 0);

	-- Interrupts stuff
	signal dma_irq_s           : std_logic_vector(1 downto 0);
	signal irq_sources_s       : std_logic_vector(31 downto 0);
	signal irq_to_gn4124_s     : std_logic;
	signal ddr_wr_fifo_empty   : std_logic;
	signal ddr_wr_fifo_empty_d : std_logic;
	signal ddr_wr_fifo_empty_p : std_logic;
	signal acq_end_irq_p       : std_logic_vector(1 downto 0);

	signal mem_range_s      : std_logic;
	signal acqu_end_s       : std_logic_vector(1 downto 0);
	signal acq_end          : std_logic_vector(1 downto 0);
	signal last_acq_end     : std_logic_vector(1 downto 0);
	signal acqu_end_force_s : std_logic;

	-- Carrier 1-wire
	signal carrier_owr_en : std_logic_vector(0 downto 0);
	signal carrier_owr_i  : std_logic_vector(0 downto 0);

	--WR 
	signal dac_hpll_load_p1 : std_logic;
	signal dac_dpll_load_p1 : std_logic;
	signal dac_hpll_data    : std_logic_vector(15 downto 0);
	signal dac_dpll_data    : std_logic_vector(15 downto 0);

	signal pps_p :std_logic;

	signal phy_tx_data      : std_logic_vector(7 downto 0);
	signal phy_tx_k         : std_logic;
	signal phy_tx_disparity : std_logic;
	signal phy_tx_enc_err   : std_logic;
	signal phy_rx_data      : std_logic_vector(7 downto 0);
	signal phy_rx_rbclk     : std_logic;
	signal phy_rx_k         : std_logic;
	signal phy_rx_enc_err   : std_logic;
	signal phy_rx_bitslide  : std_logic_vector(3 downto 0);
	signal phy_rst          : std_logic;
	signal phy_loopen       : std_logic;
	
	-- Timing interface
	signal tm_time_valid : std_logic;
	signal tm_tai        : std_logic_vector(39 downto 0);
	signal tm_cycles     : std_logic_vector(27 downto 0);

	-- TX streamer signals
	signal tx_sync_s                     : std_logic;
	signal tx_tag_cycles                 : std_logic_vector(27 downto 0);
	signal tx_tag_valid                  : std_logic;
	signal tx_data                       : std_logic_vector(15 downto 0);
	signal tx_valid, tx_dreq             : std_logic;

	signal tx_last, tx_flush : std_logic;
	signal rx_first, rx_last, rx_lost : std_logic;
	
	signal losses_s, losses_rdy_s : std_logic;

	signal sync_force_send_s   : std_logic;
	signal force_send_s        : std_logic;
	signal count_sync_s        : integer :=0;
	signal latch_val_s         : std_logic;
	signal status_error_code_s : std_logic_vector(3 downto 0);

	signal wr_sample_period_s   : std_logic_vector(25 downto 0);
	signal wr_ctrl_rx_latency_s : std_logic_vector(27 downto 0);
	signal wr_ctrl_lost_frame_s : std_logic;
	signal wr_losses_s          : std_logic_vector(31 downto 0);
	signal wr_lab_test_s        : std_logic;
	signal wr_frame_type_s      : std_logic;
	signal wr_send_ctrl_s       : std_logic;

	signal tx_B_frame_s   : btrainFrameValue;
	signal tx_I_frame_s   : ImFrameValue;
	signal ID_frame_s     : std_logic_vector(7 downto 0);
	signal error_wr_bit_s : std_logic;
	signal I_s         : std_logic_vector(31 downto 0);
	signal new_I_rdy_s : std_logic;
	signal wr_B_s      : std_logic_vector(31 downto 0);
	signal wr_Bdot_s   : std_logic_vector(31 downto 0);
	signal wr_G_s      : std_logic_vector(31 downto 0);
	signal wr_S_s      : std_logic_vector(31 downto 0);
	signal new_B_rdy_s : std_logic;
	signal wr_drdy_s   : std_logic;

	signal clk_sys_wr	: std_logic;
	signal clk_ref	: std_logic;
	
	signal led_s : std_logic_vector(3 downto 0);
		
	-- RX streamer signals
	signal rx_data  : std_logic_vector(15 downto 0);
	signal rx_valid : std_logic;

	signal sfp_scl_out, sfp_sda_out   : std_logic;
	signal fmc_scl_out, fmc_sda_out   : std_logic;
	signal owr_enable, owr_in         : std_logic_vector(1 downto 0);

	-- Fabric interface signals, passing packets between the WR Core and the streamers
	signal wrcore_snk_out : t_wrf_sink_out;
	signal wrcore_snk_in  : t_wrf_sink_in;
	signal wrcore_src_out : t_wrf_source_out;
	signal wrcore_src_in  : t_wrf_source_in;
	
	signal rd_seq_no_s   : std_logic_vector(14 downto 0);
	
	--Switch
	signal switch_open1_s	: std_logic := SWITCH_CLOSE_CMD;
	signal switch_gain1_s	: std_logic := SWITCH_OPEN_CMD;
	signal switch_off1_s 	: std_logic := SWITCH_OPEN_CMD;
		
	signal switch_open2_s	: std_logic := SWITCH_CLOSE_CMD;
	signal switch_gain2_s	: std_logic := SWITCH_OPEN_CMD;
	signal switch_off2_s 	: std_logic := SWITCH_OPEN_CMD;

	--LCD display I/O
	signal LCD_REG_SEL_s	: STD_LOGIC; -- LCD register select signal
	signal LCD_ENABLE1_s	: STD_LOGIC; -- Line 1 & 2 LCD enable signal
	signal LCD_ENABLE2_s	: STD_LOGIC; -- Line 3 & 4 LCD enable signal
	
	--BTrain
	signal SYS_DIO_in_s  : std_logic_vector(NB_SYS_IO-1 downto 0);
	signal SYS_DIO_out_s : std_logic_vector(NB_SYS_IO-1 downto 0);
	
	signal C0_pulse_in_s	: std_logic;

	--Marker signals
	signal f_low_marker_s	: std_logic;
	signal f_high_marker_s	: std_logic;
	signal d_low_marker_s	: std_logic;
	signal d_high_marker_s	: std_logic;
	
	signal zero_cycle_s	: std_logic;
	
	--Correction Register	
	signal Bsmooth_F_s	: std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0) := x"7fffffff";--(others=>'0');
	signal corr_fact_F_s	: std_logic_vector(NB_BITS_B_VALUE-1 downto 0) := (others=>'0');
	signal Vo_F_s			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0) := (others=>'0');

	signal Bsmooth_D_s	: std_logic_vector(NB_BITS_BSMOOTH_VALUE-1 downto 0) := x"7fffffff";--(others=>'0');
	signal corr_fact_D_s	: std_logic_vector(NB_BITS_B_VALUE-1 downto 0) := (others=>'0');
	signal Vo_D_s			: std_logic_vector(NB_BITS_B_VALUE-1 downto 0) := (others=>'0');

	signal Bk_s    : std_logic_vector(NB_BITS_B_VALUE-1 downto 0);
	signal B_data_rdy_s : std_logic;

	signal Bdot_s    : std_logic_vector(NB_BITS_B_VALUE-1 downto 0):= x"12345678";
	signal Bdot_data_rdy_s : std_logic;

	--Register
	signal calib_status_reg_s     : std_logic_vector(CALIB_STATUS_BUS_LENGTH-1 downto 0);
	signal lcd_calib_status_reg_s : std_logic_vector(CALIB_STATUS_LCD_BUS_LENGTH-1 downto 0);

	signal val_pos_F_s	: std_logic_vector(31 downto 0);
	signal val_neg_F_s	: std_logic_vector(31 downto 0);
	signal val_pos_D_s	: std_logic_vector(31 downto 0);
	signal val_neg_D_s	: std_logic_vector(31 downto 0);

	signal status_simeff_s                      : std_logic;
	signal reg_dio_dir_s                            : std_logic_vector(9 downto 0);
	signal reg_dio_input_s                          : std_logic_vector(9 downto 0);
	signal not_dio_dir_s : std_logic_vector(9 downto 0);

	--Test register
	signal I_tx_ttl_s : std_logic;--modified to WR test	
	signal I_rx_ttl_s : std_logic;--modified to WR test	
	signal B_tx_ttl_s : std_logic;--modified to WR test	
	signal B_rx_ttl_s : std_logic;--modified to WR test	

--	signal trslv_led_s			: std_logic_vector(3 downto 0);
--	signal trslv_value_slv_s	: std_logic_vector(27 downto 0);
--
--	signal test_reg_trslv_led_buff_s : std_logic_vector(3 downto 0);
--	signal test_reg_trslv_led_load_s : std_logic;
--
--	signal test_reg_trslv_value_slv_buff_s : std_logic_vector(27 downto 0);
--	signal test_reg_trslv_value_slv_load_s : std_logic;
			
	attribute keep : string;
	attribute keep of ADC_CLK_s : signal is "TRUE";	

	signal late_s : std_logic_vector((2**(REG_DELAY_LONG+1))-4 downto 0);
	attribute keep of late_s : signal is "TRUE";

	attribute keep of sys_clk_10_s : signal is "TRUE";
	attribute keep of clk_sys_wr : signal is "TRUE";
	
	attribute keep of sync_force_send_s : signal is "TRUE";

	attribute keep of rx_data : signal is "TRUE";
	attribute keep of rx_valid : signal is "TRUE";
	attribute keep of rx_first : signal is "TRUE";
	attribute keep of rx_last : signal is "TRUE";
	attribute keep of I_s : signal is "TRUE";
	attribute keep of new_I_rdy_s : signal is "TRUE";
	attribute keep of wr_B_s : signal is "TRUE";
	attribute keep of wr_Bdot_s : signal is "TRUE";
	attribute keep of new_B_rdy_s : signal is "TRUE";
	attribute keep of tx_data : signal is "TRUE";
	attribute keep of tx_valid : signal is "TRUE";
	attribute keep of tx_flush : signal is "TRUE";

begin

  ------------------------------------------------------------------------------
  -- Clock 80MHz for ADC 
  ------------------------------------------------------------------------------
	
   IBUFDS_ADC_CLK_s : IBUFGDS
   generic map (
      DIFF_TERM => TRUE, 		-- Differential Termination 
      IBUF_LOW_PWR => FALSE,	-- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD => "LVDS_25")
   port map (
      O => ADC_CLK_s,	-- Clock buffer output
      I => ADC_CLK_P_i,	-- Diff_p clock buffer input
      IB => ADC_CLK_N_i	-- Diff_n clock buffer input
   );
	
  ------------------------------------------------------------------------------
  -- Clocks distribution from 20MHz oscillator
  --  20.000 MHz input clock
  --  62.500 MHz clk_dmtd for WhiteRabbit core
  ------------------------------------------------------------------------------

  cmp_sys_clk_buf : BUFG
    port map (
      I => clk_20MHz_i,
      O => sys_clk_in_cryst_s);

  cmp_sys_clk_cryst : PLL_BASE
    generic map (
      BANDWIDTH          => "OPTIMIZED",
      CLK_FEEDBACK       => "CLKFBOUT",
      COMPENSATION       => "INTERNAL",
      DIVCLK_DIVIDE      => 1,
      CLKFBOUT_MULT      => 50,
      CLKFBOUT_PHASE     => 0.000,
      CLKOUT0_DIVIDE     => 16,         -- 62.5 MHz
      CLKOUT0_PHASE      => 0.000,
      CLKOUT0_DUTY_CYCLE => 0.500,
      CLKOUT1_DIVIDE     => 16,
      CLKOUT1_PHASE      => 0.000,
      CLKOUT1_DUTY_CYCLE => 0.500,
      CLKOUT2_DIVIDE     => 16,
      CLKOUT2_PHASE      => 0.000,
      CLKOUT2_DUTY_CYCLE => 0.500,
      CLKIN_PERIOD       => 50.0,
      REF_JITTER         => 0.016)
    port map (
      CLKFBOUT => pllout_clk_fb_dmtd,
      CLKOUT0  => pllout_clk_dmtd,
      CLKOUT1  => open,
      CLKOUT2  => open,
      CLKOUT3  => open,
      CLKOUT4  => open,
      CLKOUT5  => open,
      LOCKED   => open,
      RST      => '0',
      CLKFBIN  => pllout_clk_fb_dmtd,
      CLKIN    => sys_clk_in_cryst_s);

  cmp_clk_dmtd_buf : BUFG
    port map (
      O => clk_dmtd_s,
      I => pllout_clk_dmtd);
		
  ------------------------------------------------------------------------------
  -- Clocks distribution from 20MHz oscillator
  --             input clock,  gtp_clk_s,     used by WhiteRabbit (gtp)
  -- 125.000 MHz input clock,  clk_ref        used by WhiteRabbit (timing ref)
  --  62.500 MHz system clock, clk_sys_wr     used by WhiteRabbit (system clock)
  -- 100.000 MHz system clock, sys_clk_100_s, main system clock
  --  10.000 MHz system clock, sys_clk_10_s,  used by LCD_Display
  -- 333.333 MHz system clock, ddr_clk_s,     used by DDR
  ------------------------------------------------------------------------------

	cmp_gtp_clk_buf : IBUFGDS
		generic map(
			DIFF_TERM    => true,
			IBUF_LOW_PWR => true,
			IOSTANDARD   => "DEFAULT")
		port map (
			O  => gtp_clk_s,
			I  => fpga_pll_ref_clk_101_p_i,
			IB => fpga_pll_ref_clk_101_n_i
		);

	cmp_pllrefclk_buf : IBUFGDS
		generic map (
			DIFF_TERM    => true,             -- Differential Termination
			IBUF_LOW_PWR => true,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
			IOSTANDARD   => "DEFAULT")
		port map (
			O  => clk_ref,            -- Buffer output
			I  => clk_125m_pllref_p_i,  -- Diff_p buffer input (connect directly to top-level port)
			IB => clk_125m_pllref_n_i  -- Diff_n buffer input (connect directly to top-level port)
		);

	cmp_sys_clk : PLL_BASE
	 generic map (
		BANDWIDTH          => "OPTIMIZED",
		CLK_FEEDBACK       => "CLKFBOUT",
		COMPENSATION       => "INTERNAL",
		DIVCLK_DIVIDE      => 1, --125 ref
		CLKFBOUT_MULT      => 8,
		CLKFBOUT_PHASE     => 0.000,
		CLKOUT0_DIVIDE     => 16,
		CLKOUT0_PHASE      => 0.000,
		CLKOUT0_DUTY_CYCLE => 0.500,
		CLKOUT1_DIVIDE     => 100,
		CLKOUT1_PHASE      => 0.000,
		CLKOUT1_DUTY_CYCLE => 0.500,
		CLKOUT2_DIVIDE     => 3,
		CLKOUT2_PHASE      => 0.000,
		CLKOUT2_DUTY_CYCLE => 0.500,
		CLKOUT3_DIVIDE     => 5,
		CLKOUT3_PHASE      => 0.000,
		CLKOUT3_DUTY_CYCLE => 0.500,
		CLKOUT4_DIVIDE     => 63,
		CLKOUT4_PHASE      => 0.000, 
		CLKOUT4_DUTY_CYCLE => 0.500,
		CLKOUT5_DIVIDE     => 10,
		CLKOUT5_PHASE      => 0.000, 
		CLKOUT5_DUTY_CYCLE => 0.500,
		CLKIN_PERIOD       => 8.0,
		REF_JITTER         => 0.016)
	 port map (
		CLKFBOUT => sys_clk_fb_s,
		CLKOUT0  => pllout_clk_sys,
		CLKOUT1  => sys_clk_10_buf_s,
		CLKOUT2  => ddr_clk_buf_s,
		CLKOUT3  => open,
		CLKOUT4  => open,
		CLKOUT5  => sys_clk_100_buf_s,
		LOCKED   => sys_clk_pll_locked_s,
		RST      => '0',
		CLKFBIN  => sys_clk_fb_s,
		CLKIN    => clk_ref);

	cmp_clk_62_5_buf : BUFG
	 port map (
		O => clk_sys_wr,
		I => pllout_clk_sys);
		
	cmp_clk_100_buf : BUFG
	 port map (
		O => sys_clk_100_s,
		I => sys_clk_100_buf_s);

	cmp_clk_10_buf : BUFG
	 port map (
		O => sys_clk_10_s,
		I => sys_clk_10_buf_s);

	cmp_ddr_clk_buf : BUFG
	 port map (
		O => ddr_clk_s,
		I => ddr_clk_buf_s);

	--	NOT USED  Local clock from gennum LCLK
	cmp_l_clk_buf : IBUFDS
		generic map (
			DIFF_TERM    => false,            -- Differential Termination
			IBUF_LOW_PWR => false,             -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
			IOSTANDARD   => "DEFAULT")
		port map (
			O  => l_clk,                      -- Buffer output
			I  => L_CLKp,                     -- Diff_p buffer input (connect directly to top-level port)
			IB => L_CLKn                      -- Diff_n buffer input (connect directly to top-level port)
		);

	------------------------------------------------------------------------------
	-- Power On reset, reset button, PCI reset
	------------------------------------------------------------------------------
	U_Reset_Gen : spec_reset_gen
		port map (
			clk_sys_i        => sys_clk_100_s,
			rst_pcie_n_a_i   => L_RST_N,
			rst_button_n_a_i => aux_buttons_i(0),
			rst_n_o          => but_pci_rst_n_s
		);

	process (sys_clk_100_s)
	begin
		if rising_edge(sys_clk_100_s) then
			-- Start reset system
			if (start_timer_counter_s>=START_RESET_TIMER and c_G_SIMULATON=0) then
				-- Hard/Software general system reset
				if (status_simeff_s=STATUS_SIMUL and 
						((reset_n_hw_i='0' and en_hw_reset_s='1')or(reset_sw_s='1'))) then
					reset_hw_s <= '1';
					start_timer_reset_s   <= '0';
					start_timer_counter_s <= 0;
				else
					reset_hw_s <= '0';
					start_timer_reset_s   <= '1';
					start_timer_counter_s <= start_timer_counter_s;
				end if;
			elsif (start_timer_counter_s>=START_RESET_TIMER_SIM and c_G_SIMULATON=1) then 
				-- Hard/Software general system reset
				if (status_simeff_s=STATUS_SIMUL and 
						((reset_n_hw_i='0' and en_hw_reset_s='1')or(reset_sw_s='1'))) then
					reset_hw_s <= '1';
					start_timer_reset_s   <= '0';
					start_timer_counter_s <= 0;
				else
					reset_hw_s <= '0';
					start_timer_reset_s   <= '1';
					start_timer_counter_s <= start_timer_counter_s;
				end if;
			else
				start_timer_counter_s <= start_timer_counter_s+1;
			end if;
			reset_n_s <= but_pci_rst_n_s and sys_clk_pll_locked_s and start_timer_reset_s;
			reset_s <= not(but_pci_rst_n_s and sys_clk_pll_locked_s and start_timer_reset_s);

			-- Start init system
			if ((start_init_counter_s>=START_INIT_TIMER and c_G_SIMULATON=0)or
					(start_init_counter_s>=START_INIT_TIMER_SIM and c_G_SIMULATON=1)) then
				init_done_s <= '1';
				start_init_counter_s <= start_init_counter_s;
			else
				init_done_s <= '0';
				start_init_counter_s <= start_init_counter_s+1;
			end if;
		end if;
	end process;
	
	------------------------------------------------------------------------------
	-- GN4124 interface
	------------------------------------------------------------------------------
	cmp_gn4124_core : gn4124_core
	port map(
		rst_n_a_i       => L_RST_N,
		status_o        => gn4124_status_s,
		-- P2L Direction Source Sync DDR related signals
		p2l_clk_p_i     => P2L_CLKp,
		p2l_clk_n_i     => P2L_CLKn,
		p2l_data_i      => P2L_DATA,
		p2l_dframe_i    => P2L_DFRAME,
		p2l_valid_i     => P2L_VALID,
		-- P2L Control
		p2l_rdy_o       => P2L_RDY,
		p_wr_req_i      => P_WR_REQ,
		p_wr_rdy_o      => P_WR_RDY,
		rx_error_o      => RX_ERROR,
		-- L2P Direction Source Sync DDR related signals
		l2p_clk_p_o     => L2P_CLKp,
		l2p_clk_n_o     => L2P_CLKn,
		l2p_data_o      => L2P_DATA,
		l2p_dframe_o    => L2P_DFRAME,
		l2p_valid_o     => L2P_VALID,
		l2p_edb_o       => L2P_EDB,
		-- L2P Control
		l2p_rdy_i       => L2P_RDY,
		l_wr_rdy_i      => L_WR_RDY,
		p_rd_d_rdy_i    => P_RD_D_RDY,
		tx_error_i      => TX_ERROR,
		vc_rdy_i        => VC_RDY,
		-- Interrupt interface
		dma_irq_o       => dma_irq_s,
		irq_p_i         => irq_to_gn4124_s,
		irq_p_o         => GPIO(0),
		-- DMA registers wishbone interface (slave classic)
		dma_reg_clk_i   => sys_clk_100_s,
		dma_reg_adr_i   => dma_ctrl_wb_adr,
		dma_reg_dat_i   => cnx_master_out(c_SLAVE_DMA).dat,
		dma_reg_sel_i   => cnx_master_out(c_SLAVE_DMA).sel,
		dma_reg_stb_i   => cnx_master_out(c_SLAVE_DMA).stb,
		dma_reg_we_i    => cnx_master_out(c_SLAVE_DMA).we,
		dma_reg_cyc_i   => cnx_master_out(c_SLAVE_DMA).cyc,
		dma_reg_dat_o   => cnx_master_in(c_SLAVE_DMA).dat,
		dma_reg_ack_o   => cnx_master_in(c_SLAVE_DMA).ack,
		dma_reg_stall_o => cnx_master_in(c_SLAVE_DMA).stall,
		-- CSR wishbone interface (master pipelined)
		csr_clk_i       => sys_clk_100_s,
		csr_adr_o       => gn_wb_adr,
		csr_dat_o       => cnx_slave_in(c_MASTER_GENNUM).dat,
		csr_sel_o       => cnx_slave_in(c_MASTER_GENNUM).sel,
		csr_stb_o       => cnx_slave_in(c_MASTER_GENNUM).stb,
		csr_we_o        => cnx_slave_in(c_MASTER_GENNUM).we,
		csr_cyc_o       => cnx_slave_in(c_MASTER_GENNUM).cyc,
		csr_dat_i       => cnx_slave_out(c_MASTER_GENNUM).dat,
		csr_ack_i       => cnx_slave_out(c_MASTER_GENNUM).ack,
		csr_stall_i     => cnx_slave_out(c_MASTER_GENNUM).stall,
      csr_err_i       => cnx_slave_out(c_MASTER_GENNUM).err,
      csr_rty_i       => cnx_slave_out(c_MASTER_GENNUM).rty,
      csr_int_i       => cnx_slave_out(c_MASTER_GENNUM).int,
		-- DMA wishbone interface (pipelined)
		dma_clk_i       => sys_clk_100_s,
		dma_adr_o       => wb_dma_adr,
		dma_dat_o       => wb_dma_dat_o,
		dma_sel_o       => wb_dma_sel,
		dma_stb_o       => wb_dma_stb,
		dma_we_o        => wb_dma_we,
		dma_cyc_o       => wb_dma_cyc,
		dma_dat_i       => wb_dma_dat_i,
		dma_ack_i       => wb_dma_ack,
		dma_stall_i     => wb_dma_stall,
      dma_err_i       => wb_dma_err,
      dma_rty_i       => wb_dma_rty,
      dma_int_i       => wb_dma_int
	);

	p2l_pll_locked_s <= gn4124_status_s(0);
	
	-- Convert 32-bit word address into byte address for crossbar
	cnx_slave_in(c_MASTER_GENNUM).adr <= gn_wb_adr(29 downto 0) & "00";

	-- Convert 32-bit byte address into word address for DMA controller
	dma_ctrl_wb_adr <= "00" & cnx_master_out(c_SLAVE_DMA).adr(31 downto 2);

	-- Unused wishbone signals
	cnx_master_in(c_SLAVE_DMA).err <= '0';
	cnx_master_in(c_SLAVE_DMA).rty <= '0';
	cnx_master_in(c_SLAVE_DMA).int <= '0';

	------------------------------------------------------------------------------
	-- CSR wishbone crossbar
	------------------------------------------------------------------------------

	cmp_sdb_crossbar : xwb_sdb_crossbar
	 generic map (
		g_num_masters => c_NUM_WB_SLAVES,
		g_num_slaves  => c_NUM_WB_MASTERS,
		g_registered  => true,
		g_wraparound  => true,
		g_layout      => c_INTERCONNECT_LAYOUT,
		g_sdb_addr    => c_SDB_ADDRESS)

	 port map (
		clk_sys_i => sys_clk_100_s,
		rst_n_i   => reset_n_s,
		slave_i   => cnx_slave_in,
		slave_o   => cnx_slave_out,
		master_i  => cnx_master_in,
		master_o  => cnx_master_out);

	------------------------------------------------------------------------------
	-- Carrier 1-wire master
	--    DS18B20 (thermometer + unique ID)
	------------------------------------------------------------------------------
	cmp_carrier_onewire : xwb_onewire_master
	 generic map(
		g_interface_mode      => CLASSIC,
		g_address_granularity => BYTE,
		g_num_ports           => 1,
		g_ow_btp_normal       => "5.0",
		g_ow_btp_overdrive    => "1.0"
		)
	 port map(
		clk_sys_i => sys_clk_100_s,
		rst_n_i   => reset_n_s,

		slave_i => cnx_master_out(c_SLAVE_ONEWIRE),
		slave_o => cnx_master_in(c_SLAVE_ONEWIRE),
		desc_o  => open,

		owr_pwren_o => open,
		owr_en_o    => carrier_owr_en,
		owr_i       => carrier_owr_i
		);

	carrier_one_wire_b <= '0' when carrier_owr_en(0) = '1' else 'Z';
	carrier_owr_i(0)   <= carrier_one_wire_b;

	------------------------------------------------------------------------------
	-- Carrier CSR
	--    Carrier type and PCB version
	--    Bitstream (firmware) type and date
	--    Release tag
	--    VCXO DAC control (CLR_N)
	------------------------------------------------------------------------------
	cmp_carrier_csr : carrier_csr
	 port map(
		rst_n_i                          => reset_n_s,
		clk_sys_i                        => sys_clk_100_s,
      wb_adr_i                         => cnx_master_out(c_SLAVE_SPEC_CSR).adr(3 downto 2),  -- cnx_master_out.adr is byte address
      wb_dat_i                         => cnx_master_out(c_SLAVE_SPEC_CSR).dat,
      wb_dat_o                         => cnx_master_in(c_SLAVE_SPEC_CSR).dat,
      wb_cyc_i                         => cnx_master_out(c_SLAVE_SPEC_CSR).cyc,
      wb_sel_i                         => cnx_master_out(c_SLAVE_SPEC_CSR).sel,
      wb_stb_i                         => cnx_master_out(c_SLAVE_SPEC_CSR).stb,
      wb_we_i                          => cnx_master_out(c_SLAVE_SPEC_CSR).we,
      wb_ack_o                         => cnx_master_in(c_SLAVE_SPEC_CSR).ack,
      wb_stall_o                       => open,
		carrier_csr_carrier_pcb_rev_i    => pcb_ver_i,
		carrier_csr_carrier_reserved_o   => open,
		carrier_csr_carrier_type_i       => c_CARRIER_TYPE,
		carrier_csr_stat_fmc_pres_i      => fmc0_prsnt_m2c_n_i,
		carrier_csr_stat_p2l_pll_lck_i   => p2l_pll_locked_s,
		carrier_csr_stat_sys_pll_lck_i   => sys_clk_pll_locked_s,
		carrier_csr_stat_ddr3_cal_done_i => ddr3_calib_done_s,
		carrier_csr_stat_reserved_o      => open,
		carrier_csr_ctrl_led_o           => open, --aux_leds_o,
		carrier_csr_ctrl_reserved_o      => open,
      carrier_csr_rst_fmc0_n_o         => sw_rst_fmc0_n_s,
      carrier_csr_rst_reserved_o       => open
	);
			
	-- Unused wishbone signals
	cnx_master_in(c_SLAVE_SPEC_CSR).err   <= '0';
	cnx_master_in(c_SLAVE_SPEC_CSR).rty   <= '0';
	cnx_master_in(c_SLAVE_SPEC_CSR).stall <= '0';
	cnx_master_in(c_SLAVE_SPEC_CSR).int   <= '0';

	------------------------------------------------------------------------------
	-- Interrupt controller
	------------------------------------------------------------------------------
	cmp_irq_controller : irq_controller
	 port map(
		clk_i   => sys_clk_100_s,
		rst_n_i => reset_n_s,

		irq_src_p_i => irq_sources_s,

		irq_p_o => irq_to_gn4124_s,

      wb_adr_i => cnx_master_out(c_SLAVE_INT).adr(3 downto 2),  -- cnx_master_out.adr is byte address
      wb_dat_i => cnx_master_out(c_SLAVE_INT).dat,
      wb_dat_o => cnx_master_in(c_SLAVE_INT).dat,
      wb_cyc_i => cnx_master_out(c_SLAVE_INT).cyc,
      wb_sel_i => cnx_master_out(c_SLAVE_INT).sel,
      wb_stb_i => cnx_master_out(c_SLAVE_INT).stb,
      wb_we_i  => cnx_master_out(c_SLAVE_INT).we,
      wb_ack_o => cnx_master_in(c_SLAVE_INT).ack
	);

	-- Unused wishbone signals
	cnx_master_in(c_SLAVE_INT).err   <= '0';
	cnx_master_in(c_SLAVE_INT).rty   <= '0';
	cnx_master_in(c_SLAVE_INT).stall <= '0';
	cnx_master_in(c_SLAVE_INT).int   <= '0';

	-- IRQ sources
	--   0    -> End of DMA transfer
	--   1    -> DMA transfer error
	--   2    -> End of acquisition block 0
	--   3    -> End of acquisition block 1
	--   4-31 -> Unused
	irq_sources_s(1 downto 0)  <= dma_irq_s;
	irq_sources_s(3 downto 2)  <= acq_end_irq_p;
	irq_sources_s(31 downto 4) <= (others => '0');

	-- Detects end of adc core writing to ddr
	p_ddr_wr_fifo_empty : process (sys_clk_100_s)
	begin
	 if rising_edge(sys_clk_100_s) then
		ddr_wr_fifo_empty_d <= ddr_wr_fifo_empty;
	 end if;
	end process p_ddr_wr_fifo_empty;

	ddr_wr_fifo_empty_p <= ddr_wr_fifo_empty and not(ddr_wr_fifo_empty_d);

	-- End of acquisition interrupt generation
	p_acq_end : process (sys_clk_100_s)
	begin
	 if rising_edge(sys_clk_100_s) then
		if reset_s = '1' then
		  acq_end <= (others=>'0');
		elsif acqu_end_s /= "00" then
		  acq_end <= acqu_end_s;
		elsif acqu_end_force_s = '1' and last_acq_end = "01" then --to test
		  acq_end <= "10";
		  last_acq_end <= "10";
		elsif acqu_end_force_s = '1' and last_acq_end = "10" then --to test
		  acq_end <= "01";
		  last_acq_end <= "01";
		else                               --to test
		  acq_end <= (others=>'0');
		end if;
	 end if;
	end process p_acq_end;

	acq_end_irq_p <= acq_end;--to test

	-- IRQ leds
--	gen_irq_led : for I in 0 to 3 generate
--	 cmp_irq_led : gc_extend_pulse
--		generic map (
--		  g_width => 5000000)
--		port map (
--		  clk_i      => sys_clk_100_s,
--		  rst_n_i    => reset_s,
--		  pulse_i    => irq_sources_s(I),
--		  extended_o => led_s(I));
--	end generate gen_irq_led;	

	------------------------------------------------------------------------------
	-- DMA wishbone bus slaves
	--  -> DDR3 controller
	------------------------------------------------------------------------------
	cmp_ddr_ctrl : ddr3_ctrl
	 generic map(
		g_BANK_PORT_SELECT   => "SPEC_BANK3_64B_32B",
		g_MEMCLK_PERIOD      => 3000,
		g_SIMULATION         => "FALSE",--g_SIMULATION,
		g_CALIB_SOFT_IP      => "TRUE",--g_CALIB_SOFT_IP,
		g_P0_MASK_SIZE       => 8,
--		g_P0_DATA_PORT_SIZE  => 65,
		g_P0_DATA_PORT_SIZE  => 64,
		g_P0_BYTE_ADDR_WIDTH => 30,
		g_P1_MASK_SIZE       => 4,
		g_P1_DATA_PORT_SIZE  => 32,
		g_P1_BYTE_ADDR_WIDTH => 30)
	 port map (
		clk_i   => ddr_clk_s,
		rst_n_i => reset_n_s,

		status_o => ddr3_status_s,

		ddr3_dq_b     => DDR3_DQ,
		ddr3_a_o      => DDR3_A,
		ddr3_ba_o     => DDR3_BA,
		ddr3_ras_n_o  => DDR3_RAS_N,
		ddr3_cas_n_o  => DDR3_CAS_N,
		ddr3_we_n_o   => DDR3_WE_N,
		ddr3_odt_o    => DDR3_ODT,
		ddr3_rst_n_o  => DDR3_RESET_N,
		ddr3_cke_o    => DDR3_CKE,
		ddr3_dm_o     => DDR3_LDM,
		ddr3_udm_o    => DDR3_UDM,
		ddr3_dqs_p_b  => DDR3_LDQS_P,
		ddr3_dqs_n_b  => DDR3_LDQS_N,
		ddr3_udqs_p_b => DDR3_UDQS_P,
		ddr3_udqs_n_b => DDR3_UDQS_N,
		ddr3_clk_p_o  => DDR3_CK_P,
		ddr3_clk_n_o  => DDR3_CK_N,
		ddr3_rzq_b    => DDR3_RZQ,
		ddr3_zio_b    => DDR3_ZIO,

		wb0_clk_i   => sys_clk_100_s,
		wb0_sel_i   => wb_ddr_sel,
		wb0_cyc_i   => wb_ddr_cyc,
		wb0_stb_i   => wb_ddr_stb,
		wb0_we_i    => wb_ddr_we,
		wb0_addr_i  => wb_ddr_adr,
		wb0_data_i  => wb_ddr_dat_o,
		wb0_data_o  => open,
		wb0_ack_o   => wb_ddr_ack,
		wb0_stall_o => wb_ddr_stall,

		p0_cmd_empty_o   => open,
		p0_cmd_full_o    => open,
		p0_rd_full_o     => open,
		p0_rd_empty_o    => open,
		p0_rd_count_o    => open,
		p0_rd_overflow_o => open,
		p0_rd_error_o    => open,
		p0_wr_full_o     => open,
		p0_wr_empty_o    => ddr_wr_fifo_empty,
		p0_wr_count_o    => open,
		p0_wr_underrun_o => open,
		p0_wr_error_o    => open,

		wb1_clk_i   => sys_clk_100_s,
		wb1_sel_i   => wb_dma_sel,
		wb1_cyc_i   => wb_dma_cyc,
		wb1_stb_i   => wb_dma_stb,
		wb1_we_i    => wb_dma_we,
		wb1_addr_i  => wb_dma_adr,
		wb1_data_i  => wb_dma_dat_o,
		wb1_data_o  => wb_dma_dat_i,
		wb1_ack_o   => wb_dma_ack,
		wb1_stall_o => wb_dma_stall,

		p1_cmd_empty_o   => open,
		p1_cmd_full_o    => open,
		p1_rd_full_o     => open,
		p1_rd_empty_o    => open,
		p1_rd_count_o    => open,
		p1_rd_overflow_o => open,
		p1_rd_error_o    => open,
		p1_wr_full_o     => open,
		p1_wr_empty_o    => open,
		p1_wr_count_o    => open,
		p1_wr_underrun_o => open,
		p1_wr_error_o    => open

		);
	ddr3_calib_done_s <= ddr3_status_s(0);

	-- unused Wishbone signals
	wb_dma_err <= '0';
	wb_dma_rty <= '0';
	wb_dma_int <= '0';



	cmp_BTrainCore : fmc_adc2M18b2ch 
	generic map(g_simulation => g_simulation)
	port map(
		clk_i			=> sys_clk_100_s,
		clk_10_i	   => sys_clk_10_s,
		clk_adc_i	=> ADC_CLK_s,

		reset_i		  => reset_s, 
		reset_hw_i    => reset_hw_s,
		reset_n_hw_i  => reset_n_hw_i,
		reset_sw_o    => reset_sw_s,
		en_hw_reset_o => en_hw_reset_s,
		init_done_i   => init_done_s,

		--Input information signals
		F_low_marker_i => f_low_marker_s,
		F_high_marker_i=> f_high_marker_s,
		D_low_marker_i => d_low_marker_s,
		D_high_marker_i=> d_high_marker_s,
		C0_i				=> C0_pulse_in_s,
		zero_cycle_i	=> zero_cycle_s,

		--Switch
		switch_open1_o	=> switch_open1_s,
		switch_gain1_o	=> switch_gain1_s,
		switch_off1_o	=> switch_off1_s,
		
		switch_open2_o	=> switch_open2_s,
		switch_gain2_o	=> switch_gain2_s,
		switch_off2_o	=> switch_off2_s,
		
		error_status_o	=> status_error_code_s,
		calib_status_o	=> calib_status_reg_s,

		--ADC
		CNV_o		=> ADC_CNV_o,
		
		SDI_F_o		=> ADC1_SDI_o,
		SDO_F_i		=> ADC1_SDO_i,
		SCK_F_o		=> ADC1_SCK_o,

		SDI_D_o		=> ADC2_SDI_o,
		SDO_D_i		=> ADC2_SDO_i,
		SCK_D_o		=> ADC2_SCK_o,

      wb_csr_adr_i   => cnx_master_out(c_SLAVE_FMC).adr,
      wb_csr_dat_i   => cnx_master_out(c_SLAVE_FMC).dat,
      wb_csr_dat_o   => cnx_master_in(c_SLAVE_FMC).dat,
      wb_csr_cyc_i   => cnx_master_out(c_SLAVE_FMC).cyc,
      wb_csr_sel_i   => cnx_master_out(c_SLAVE_FMC).sel,
      wb_csr_stb_i   => cnx_master_out(c_SLAVE_FMC).stb,
      wb_csr_we_i    => cnx_master_out(c_SLAVE_FMC).we,
      wb_csr_ack_o   => cnx_master_in(c_SLAVE_FMC).ack,
      wb_csr_stall_o => cnx_master_in(c_SLAVE_FMC).stall,

		--Register	
		status_simeff_i      => status_simeff_s,
		reg_status_opsp_cmd_o    => opsp_cmd_o,
		reg_status_opsp_status_i => opsp_status_i,
		
		reg_dio_input_i   => reg_dio_input_s,
		reg_dio_dir_i     => reg_dio_dir_s,
--			reg_dio_dir_o   : out std_logic_vector(9 downto 0);
		reg_dio_cfg_o     => reg_dio_cfg_s,

		--White Rabbit values	
		wr_B_val_i     => wr_B_s,
		wr_Bdot_val_i  => wr_Bdot_s,
		wr_I_val_i     => I_s,
		wr_drdy_i      => wr_drdy_s,

		Bk_o         => Bk_s,
		B_data_rdy_o => B_data_rdy_s,
		Bdot_o			=> Bdot_s,
		Bdot_data_rdy_o=> Bdot_data_rdy_s,

		--WR-register
		wr_sample_period_o   => wr_sample_period_s,
		wr_ctrl_rx_latency_i => wr_ctrl_rx_latency_s,
		wr_ctrl_lost_frame_i => rx_lost,
		wr_losses_i          => wr_losses_s,
		wr_ctrl_tm_valid_i   => tm_time_valid,
		wr_lab_test_o        => wr_lab_test_s,
		wr_frame_type_o      => wr_frame_type_s,
		wr_send_ctrl_o       => wr_send_ctrl_s,
		
		--DAC
		SDI_i			=> ISO_DAC_SDO_i,
		SDO_o			=> ISO_DAC_SDIN_o,
		SCK_o			=> ISO_DAC_SCLK_o,

--		DMA wishbone
      wb_ddr_clk_i   => sys_clk_100_s,
      wb_ddr_adr_o   => wb_ddr_adr,
      wb_ddr_dat_o   => wb_ddr_dat_o,
      wb_ddr_sel_o   => wb_ddr_sel,
      wb_ddr_stb_o   => wb_ddr_stb,
      wb_ddr_we_o    => wb_ddr_we,
      wb_ddr_cyc_o   => wb_ddr_cyc,
      wb_ddr_ack_i   => wb_ddr_ack,
      wb_ddr_stall_i => wb_ddr_stall,

		mem_range_o		=> mem_range_s,
		acqu_end_o     => acqu_end_s,

		DAC_n_LDAC_o	=> ISO_DAC_LDAC_o,
		DAC_n_SYNC_o	=> ISO_DAC_SYNC_o,
		DAC_n_RESET_o	=> ISO_DAC_RESET_o,
		DAC_n_CLR_o		=> ISO_DAC_CLR_o,

		--LCD display I/O
		LCD_REG_SEL => LCD_REG_SEL_s, 
		LCD_ENABLE1 => LCD_ENABLE1_s,
		LCD_ENABLE2 => LCD_ENABLE2_s,
		LCD_SDOUT	=> LCD_SDOUT,
		LCD_NCS		=> LCD_NCS,
		LCD_CLK		=> LCD_CLK,

		NKEY1 => N_KEY1,
		NKEY2 => N_KEY2,
		NKEY3 => N_KEY3,
		NKEY4 => N_KEY4,
		
		dac_extern_clk_o => dac_extern_clk_o,
		dac_extern_cs_o  => dac_extern_cs_o,
		dac_extern_sdo_o => dac_extern_sdo_o
	);
	
-- to avoid 1 hour compilation
  -----------------------------------------------------------------------------
  -- The WR Core part. The simplest functional instantiation.
  -----------------------------------------------------------------------------

  U_The_WR_Core : xwr_core
    generic map (
      g_simulation => g_simulation
      )
    port map (
      -- Clocks & resets connections
      clk_sys_i  => clk_sys_wr,
      clk_ref_i  => clk_ref,
      clk_dmtd_i => clk_dmtd_s,
      rst_n_i    => reset_n_s,

      -- Fabric interface pins
      wrf_snk_i => wrcore_snk_in,
      wrf_snk_o => wrcore_snk_out,
      wrf_src_i => wrcore_src_in,
      wrf_src_o => wrcore_src_out,

      -- Timing interface pins
      tm_time_valid_o => tm_time_valid,
      tm_tai_o        => tm_tai,
      tm_cycles_o     => tm_cycles,

      -- PHY connections
      phy_ref_clk_i      => clk_ref,
      phy_tx_data_o      => phy_tx_data,
      phy_tx_k_o         => phy_tx_k,
      phy_tx_disparity_i => phy_tx_disparity,
      phy_tx_enc_err_i   => phy_tx_enc_err,
      phy_rx_data_i      => phy_rx_data,
      phy_rx_rbclk_i     => phy_rx_rbclk,
      phy_rx_k_i         => phy_rx_k,
      phy_rx_enc_err_i   => phy_rx_enc_err,
      phy_rx_bitslide_i  => phy_rx_bitslide,
      phy_rst_o          => phy_rst,
      phy_loopen_o       => phy_loopen,

      -- Oscillator control DACs connections
      dac_hpll_load_p1_o => dac_hpll_load_p1,
      dac_hpll_data_o    => dac_hpll_data,
      dac_dpll_load_p1_o => dac_dpll_load_p1,
      dac_dpll_data_o    => dac_dpll_data,

      -- Miscellanous pins
      uart_rxd_i => uart_rxd_i,
      uart_txd_o => uart_txd_o,

      scl_o => fmc_scl_out,
      scl_i => fmc_scl_b,
      sda_o => fmc_sda_out,
      sda_i => fmc_sda_b,

      sfp_scl_o => sfp_scl_out,
      sfp_scl_i => sfp_scl_b,
      sfp_sda_o => sfp_sda_out,
      sfp_sda_i => sfp_sda_b,

      sfp_det_i => sfp_det_i,

      led_link_o => led_sfp_green_o,
      led_act_o  => led_sfp_red_o,

      owr_en_o => owr_enable,
      owr_i    => owr_in,

      -- The PPS output, which we'll drive to the DIO mezzanine channel 1.
      pps_p_o => pps_p
      );

  -----------------------------------------------------------------------------
  -- Dual channel SPI DAC driver
  -----------------------------------------------------------------------------
  
  U_DAC_ARB : spec_serial_dac_arb
    generic map (
      g_invert_sclk    => false,        -- configured for 2xAD5662. Don't
                                        -- change the parameters.
      g_num_extra_bits => 8)

    port map (
      clk_i   => clk_sys_wr,
      rst_n_i => reset_n_s,

      -- DAC 1 controls the main (clk_ref) oscillator
      val1_i  => dac_dpll_data,
      load1_i => dac_dpll_load_p1,

      -- DAC 2 controls the helper (clk_ddmtd) oscillator
      val2_i  => dac_hpll_data,
      load2_i => dac_hpll_load_p1,

      dac_cs_n_o(0) => dac_cs1_n_o,
      dac_cs_n_o(1) => dac_cs2_n_o,
      dac_sclk_o    => dac_sclk_o,
      dac_din_o     => dac_din_o);

  -----------------------------------------------------------------------------
  -- Gigabit Ethernet PHY using Spartan-6 GTP transceviver.
  -----------------------------------------------------------------------------
  
  U_GTP : wr_gtp_phy_spartan6
    generic map (
      g_enable_ch0 => 0,
      -- each GTP has two channels, so does the PHY module.
      -- The SFP on the SPEC is connected to the 2nd channel. 
      g_enable_ch1 => 1,
      g_simulation => g_simulation)
    port map (
      gtp_clk_i => gtp_clk_s,

      ch1_ref_clk_i => clk_ref,

      -- TX code stream
      ch1_tx_data_i      => phy_tx_data,
      -- TX control/data select
      ch1_tx_k_i         => phy_tx_k,
      -- TX disparity of the previous symbol
      ch1_tx_disparity_o => phy_tx_disparity,
      -- TX encoding error
      ch1_tx_enc_err_o   => phy_tx_enc_err,

      -- RX recovered byte clock
      ch1_rx_rbclk_o    => phy_rx_rbclk,
      -- RX data stream
      ch1_rx_data_o     => phy_rx_data,
      -- RX control/data select
      ch1_rx_k_o        => phy_rx_k,
      -- RX encoding error detection
      ch1_rx_enc_err_o  => phy_rx_enc_err,
      -- RX path comma alignment bit slide delay (crucial for accuracy!)
      ch1_rx_bitslide_o => phy_rx_bitslide,

      -- Channel reset
      ch1_rst_i    => phy_rst,
      -- Loopback mode enable
      ch1_loopen_i => phy_loopen,

      pad_txn1_o => sfp_txn_o,
      pad_txp1_o => sfp_txp_o,
      pad_rxn1_i => sfp_rxn_i,
      pad_rxp1_i => sfp_rxp_i);

  -----------------------------------------------------------------------------
  -- Trigger distribution stuff - timestamping & packet transmission part
  -----------------------------------------------------------------------------
  
  -- Streamer instantiation. 
  U_TX_Streamer : xtx_streamer
    generic map (
      g_data_width => 16,--144,

      -- TX threshold = 10 data words.
      g_tx_threshold => 8,

      -- minimum timeout: sends packets asap to minimize latency (but it's not
      -- good for large amounts of data due to encapsulation overhead)
      g_tx_timeout => 1024)
    port map (
      clk_sys_i => clk_sys_wr,
      rst_n_i   => reset_n_s,
      -- Wire the packet source of the streamer to the packet sink of the WR Core
      src_i     => wrcore_snk_out,
      src_o     => wrcore_snk_in,

		-- to measure latency
		clk_ref_i       => clk_ref,
		tm_time_valid_i => tm_time_valid,
		tm_tai_i        => tm_tai,
		tm_cycles_i     => tm_cycles,

      tx_data_i  => tx_data,
      tx_valid_i => tx_valid,
      tx_dreq_o  => tx_dreq,
      -- every data word we send is the last one, as a single transfer in our
      -- case contains only one 144-bit data word.
      tx_last_i  => tx_last,

		-- Flush input. When asserted, the streamer will immediatly send out all
		-- the data that is stored in its TX buffer, ignoring g_tx_timeout.
		tx_flush_i => tx_flush,

		-- Reset sequence number. When asserted, the internal sequence number
		-- generator used to detect loss of frames is reset to 0. Advanced feature.
		tx_reset_seq_i => '0',

      -- send broadcast packets, so that many receivers can use triggers sent
      -- by us.
      cfg_mac_target_i => x"ffffffffffff",
      cfg_ethertype_i  => c_STREAMER_ETHERTYPE);

	U_Sync_Button : gc_sync_ffs 
	port map (
		clk_i    => clk_sys_wr,
		rst_n_i  => reset_n_s,
		data_i   => aux_buttons_i(1),
		synced_o => sync_force_send_s
	);

	process (clk_sys_wr)
	begin
		if (rising_edge(clk_sys_wr)) then
			if (reset_n_s='0') then
				force_send_s <= '0';
			else
				if ((sync_force_send_s='0')and(count_sync_s=0)) then
					force_send_s <= '1';
					count_sync_s <= count_sync_s+1;
				else
					force_send_s <=  '0';
					if ((count_sync_s<BUTTON_FREEZE)and(count_sync_s>0)and(c_G_SIMULATON=0)) then
						count_sync_s <= count_sync_s+1;
					elsif ((count_sync_s<BUTTON_FREEZE_SIM)and(count_sync_s>0)and(c_G_SIMULATON=1)) then
						count_sync_s <= count_sync_s+1;
					else
						count_sync_s <= 0;
					end if;
				end if;
			end if;
		end if;
	end process;

  -----------------------------------------------------------------------------
  -- Trigger distribution stuff - packet reception and pulse generation
  -----------------------------------------------------------------------------  
  
  -- Streamer instantiation
  U_RX_Streamer : xrx_streamer
    generic map (
      -- data width must be identical as in the TX streamer - otherwise, we'll be receiving
      -- rubbish
      g_data_width      => 16,--144,
		-- Number of data words in the payload. The xrx_streamer is going to transfer to the interface only those words.
		-- This generic able to distinguish between data and padding
		g_number_words		=> 9,
      -- we don't care where our triggers come from. Just blindly accept them all
      -- without checking source addresses.
      g_filter_remote_mac => false)    
    port map (
      clk_sys_i => clk_sys_wr,
      rst_n_i   => reset_n_s,

      -- Wire the packet sink of the streamer to the packet source of the WR Core
      snk_i => wrcore_src_out,
      snk_o => wrcore_src_in,

		-- to measure latency
		clk_ref_i       => clk_ref,
		tm_time_valid_i => tm_time_valid,
		tm_tai_i        => tm_tai,
		tm_cycles_i     => tm_cycles,

		-- 1 indicates the 1st word of the data block on rx_data_o.
		rx_first_o      => rx_first,
		-- 1 indicates the last word of the data block on rx_data_o.
		rx_last_o       => rx_last,

      rx_data_o    => rx_data,
      rx_valid_o   => rx_valid,
      rx_dreq_i    => '1',
		rd_seq_no_o  => rd_seq_no_s,

		rx_lost_o          => rx_lost,
		rx_latency_o       => wr_ctrl_rx_latency_s,
		
      cfg_ethertype_i         => c_STREAMER_ETHERTYPE,
      cfg_accept_broadcasts_i => '1'
	);
	
	--WR-LaboTest
	process (clk_sys_wr)
	begin
		if rising_edge(clk_sys_wr) then
			if (reset_n_s='0') then
				tx_B_frame_s.B    <= (others=>'0');
				tx_B_frame_s.Bdot <= (others=>'0');
				tx_B_frame_s.G    <= (others=>'0');
				tx_B_frame_s.S    <= (others=>'0');
				tx_I_frame_s.I    <= (others=>'0');
				ID_frame_s        <= ID_BkFrame;
				error_wr_bit_s    <= '0';
				latch_val_s       <= '0';
			else
				if (tx_sync_s='1') then
					if (wr_lab_test_s='1') then
						if (latch_val_s='0') then
							tx_B_frame_s.B    <= x"3fffffff";
							tx_B_frame_s.Bdot <= x"1fffffff";
							tx_B_frame_s.G    <= x"11223344";
							tx_B_frame_s.S    <= x"55667788";
							tx_I_frame_s.I    <= x"5fffffff";
							latch_val_s       <= '1';
						else
							tx_B_frame_s.B    <= x"c0000000";
							tx_B_frame_s.Bdot <= x"e0000000";
							tx_B_frame_s.G    <= x"55667788";
							tx_B_frame_s.S    <= x"11223344";
							tx_I_frame_s.I    <= x"a0000000";
							latch_val_s       <= '0';
						end if;
					else
						tx_B_frame_s.B    <= Bk_s;
						tx_B_frame_s.Bdot <= Bdot_s;
						tx_B_frame_s.G    <= x"12345678";
						tx_B_frame_s.S    <= x"87654321";
						tx_I_frame_s.I    <= x"11111111";
						latch_val_s       <= '0';
					end if;
				end if;
				-- Selection of frame type in function of ctrl register
				if (wr_frame_type_s='0') then
					ID_frame_s <= ID_BkFrame;
				else
					ID_frame_s <= ID_ImFrame;
				end if;
				--Error bit for WR communication
				if (status_error_code_s="0000") then
					error_wr_bit_s <= '0';	
				else
					error_wr_bit_s <= '1';	
				end if;
			end if;
		end if;
	end process;	
		
	cmp_WRBTrain : WRBTrain 
		port map(
			clk_i		=> clk_sys_wr,
			reset_i	=> reset_s,
			
			simeff_bit_i => status_simeff_s,
			error_bit_i  => error_wr_bit_s,

			reg_sample_period_i => wr_sample_period_s,

			rx_data_i    => rx_data,
			rx_valid_i   => rx_valid,
			rx_first_i   => rx_first,
			rx_lost_i    => rx_lost,
			rx_last_i    => rx_last,

			tx_B_frame_i => tx_B_frame_s,
			tx_I_frame_i => tx_I_frame_s,
			frame_ID_i	 => ID_frame_s,

			I_value_o    => I_s,
			new_I_rdy_o  => new_I_rdy_s,
			
			B_value_o    => wr_B_s,
			Bdot_value_o => wr_Bdot_s,
			G_value_o    => wr_G_s,
			S_value_o    => wr_S_s,
			new_B_rdy_o  => new_B_rdy_s,
	
			losses_o     => losses_s,
			losses_rdy_o => losses_rdy_s,
	
			force_send_i => force_send_s,
			send_cnt_i   => wr_send_ctrl_s,
			tx_sync_o    => tx_sync_s,
			
			tx_last_o    => tx_last,
			tx_flush_o   => tx_flush,
			tx_dreq_i    => tx_dreq,
			tx_data_o    => tx_data,
			tx_valid_o   => tx_valid
		);
	wr_drdy_s <= new_B_rdy_s or new_I_rdy_s;	
	
	--WR test
	B_tx_ttl_s <= tx_flush when ID_frame_s=ID_BkFrame else '0';
	I_tx_ttl_s <= tx_flush when ID_frame_s=ID_ImFrame else '0';
	
	B_rx_ttl_s <= new_B_rdy_s;
	I_rx_ttl_s <= new_I_rdy_s;
	
	cmp_losses_counter : losses_counter 
		generic map(
			g_max_time_counter => 62500000 --every second (unit : 16ns) 
		)
		port map(
			clk_i		=> clk_sys_wr,
			reset_i	=> reset_s,
			
			lost_i     => losses_s,
			lost_rdy_i => losses_rdy_s,
			
			losses_value_o => wr_losses_s
		);
	
	-----------------------------------------------------------------------------
	-- Combinatorial pins, tristate buffers, etc.
	-----------------------------------------------------------------------------

	-- The SFP is permanently enabled
	sfp_tx_disable_o  <= '0';
	sfp_rate_select_b <= '0';

	-- Open-drain driver for the Onewire bus
	carrier_one_wire_b <= '0' when owr_enable(0) = '1' else 'Z';
	owr_in(0)   <= carrier_one_wire_b;

	-- Open-drain drivers for the I2C busses
	fmc_scl_b <= '0' when fmc_scl_out = '0' else 'Z';
	fmc_sda_b <= '0' when fmc_sda_out = '0' else 'Z';

	sfp_scl_b <= '0' when sfp_scl_out = '0' else 'Z';
	sfp_sda_b <= '0' when sfp_sda_out = '0' else 'Z';

---------------------END WR-------------------------------------------------------------

	aux_leds_o(0)	<= led_s(0);
	aux_leds_o(1)	<= led_s(1);
	aux_leds_o(2)	<= led_s(2);
	aux_leds_o(3)	<= led_s(3);


	------------------------------------------------------------------------------
	-- Logical Input - Output configuration
	------------------------------------------------------------------------------

	dio_input : process (SYS_DIO_in_s,reg_dio_cfg_s)
	begin
		if (g_simulation = 0) then
			if (reg_dio_cfg_s(0)='0') then
				f_low_marker_s <= not SYS_DIO_in_s(0);  --trigger activ low (Default)
			else
				f_low_marker_s <= SYS_DIO_in_s(0);      --trigger activ high
			end if;
			if (reg_dio_cfg_s(1)='0') then
				f_high_marker_s <= not SYS_DIO_in_s(1); --trigger activ low (Default)
			else
				f_high_marker_s <= SYS_DIO_in_s(1);     --trigger activ high
			end if;
			if (reg_dio_cfg_s(2)='0') then
				d_low_marker_s <= not SYS_DIO_in_s(2); --trigger activ low (Default)
			else
				d_low_marker_s <= SYS_DIO_in_s(2);     --trigger activ high
			end if;
			if (reg_dio_cfg_s(3)='0') then
				d_high_marker_s <= not SYS_DIO_in_s(3); --trigger activ low (Default)
			else
				d_high_marker_s <= SYS_DIO_in_s(3);     --trigger activ high
			end if;
			if (reg_dio_cfg_s(4)='0') then
--				C0_pulse_in_s <= not SYS_DIO_in_s(4);       --activ low (Default)      
				C0_pulse_in_s <= SYS_DIO_in_s(4);       --activ low (Default)      
			else
--				C0_pulse_in_s <= SYS_DIO_in_s(4);   --activ high         
				C0_pulse_in_s <= not SYS_DIO_in_s(4);   --activ high         
			end if;
			if (reg_dio_cfg_s(5)='0') then
--				zero_cycle_s <= not SYS_DIO_in_s(5);        --activ low (Default)
				zero_cycle_s <= SYS_DIO_in_s(5);        --activ low (Default)
			else
--				zero_cycle_s <= SYS_DIO_in_s(5);    --activ high
				zero_cycle_s <= not SYS_DIO_in_s(5);    --activ high
			end if;
			if (reg_dio_cfg_s(6)='0') then
				status_simeff_s <= not SYS_DIO_in_s(6); --'1' => effective, '0' => simulated (Default)
			else
				status_simeff_s <= SYS_DIO_in_s(6);     --'0' => effective, '1' => simulated
			end if;
		else
			f_low_marker_s  <= '1';  
			f_high_marker_s <= '1'; 
			d_low_marker_s  <= '1'; 
			d_high_marker_s <= '1'; 
			C0_pulse_in_s   <= '1'; 
			zero_cycle_s    <= '1'; 
			status_simeff_s <= '0'; 
		end if;
	end process;

	dio_output : process (reg_dio_cfg_s,LCD_ENABLE1_s,LCD_ENABLE2_s,LCD_REG_SEL_s)
	begin
		-- 0 to 6 used as input
		if (reg_dio_cfg_s(7)='0') then
			SYS_DIO_out_s(7) <= LCD_ENABLE1_s;     --active low (Default)
		else
			SYS_DIO_out_s(7) <= not LCD_ENABLE1_s; --active high
		end if;
		if (reg_dio_cfg_s(8)='0') then
			SYS_DIO_out_s(8) <= LCD_ENABLE2_s;     --active low (Default)
		else
			SYS_DIO_out_s(8) <= not LCD_ENABLE2_s; --active high
		end if;
		if (reg_dio_cfg_s(9)='0') then
			SYS_DIO_out_s(9) <= LCD_REG_SEL_s;     --active low (Default)
		else
			SYS_DIO_out_s(9) <= not LCD_REG_SEL_s; --active high
		end if;
	end process;

	process (sys_clk_100_s,reset_s,SYS_DIO_in_s)
	begin
		if (sys_clk_100_s'event and sys_clk_100_s='1') then
			if (reset_s='1') then
				reg_dio_input_s <= (others=>'0');
			else
				reg_dio_input_s <= SYS_DIO_in_s;
			end if;
		end if;
	end process;

	--Direction of IO register
	--static I/O configuration 
	reg_dio_dir_s	<= "1110000000"; --hard coded to avoid short cut with our FMC
	DIR_DIO_o <= reg_dio_dir_s;

	-- Switch
	OFF1_BIT_o	<= switch_off1_s;
	GAIN1_BIT_o	<= switch_gain1_s;
	OPEN1_BIT_o	<= switch_open1_s;

	OFF2_BIT_o	<= switch_off2_s;
	GAIN2_BIT_o	<= switch_gain2_s;
	OPEN2_BIT_o	<= switch_open2_s;

	--LED front panel
	FM_TRIG_o	 <= f_low_marker_s or f_high_marker_s or d_low_marker_s or d_high_marker_s;
	OVER_RANGE_o <= '1';
	OPER_SPARE_o <= opsp_status_i;
	AUTO_CAL_o	 <= '0' when ((calib_status_reg_s=IDLE_st)or
									(calib_status_reg_s=NEXT_ZCYCLE_START_st)) else '1' ;

	SYS_DIO_gen : if (g_simulation = 0) generate
		SYS_DIO_LOOP_gen : for i in 0 to NB_SYS_IO-1 generate
			not_dio_dir_s(i) <= not reg_dio_dir_s(i);
			IOBUF_inst : IOBUF
				generic map (
					DRIVE => 12,
					IOSTANDARD => "DEFAULT",
					SLEW => "SLOW"
				)
				port map (
					O	=> SYS_DIO_in_s(i),		-- Extern to FPGA
					IO	=> SYS_DIO_io(i),			-- Buffer inout port (connect directly to top-level port)
					I	=> SYS_DIO_out_s(i),		-- FPGA to extern
					T	=> not_dio_dir_s(i)	-- 3-state enable input, high=extern to FPGA, low=FPGA to extern
				);
		end generate;
	end generate;
	
--	--Test register for LED
--	cmp_test_reg : test_reg 
--	  port map(
--		 rst_n_i   => reset_n_s,
--		 wb_clk_i  => sys_clk_100_s,
--		 
--		 wb_data_i => wb_dat_o_s,
--		 wb_data_o => wb_dat_i_s(c_CSR_WB_TEST_REG * 32 + 31 downto 32 * c_CSR_WB_TEST_REG),
--		 wb_cyc_i  => wb_cyc_s(c_CSR_WB_TEST_REG),
--		 wb_sel_i  => wb_sel_s,
--		 wb_stb_i  => wb_stb_s,
--		 wb_we_i   => wb_we_s,
--		 wb_ack_o  => wb_ack_s(c_CSR_WB_TEST_REG),
--
--		 -- Port for std_logic_vector field: 'LedReg' in reg: 'Test register SLV'
--		 test_reg_trslv_led_o      => test_reg_trslv_led_buff_s,
--		 test_reg_trslv_led_i      => trslv_led_s,
--		 test_reg_trslv_led_load_o => test_reg_trslv_led_load_s,
--		 -- Port for std_logic_vector field: '28-bit value' in reg: 'Test register SLV'
--		 test_reg_trslv_value_slv_o		=> test_reg_trslv_value_slv_buff_s,
--		 test_reg_trslv_value_slv_i		=> trslv_value_slv_s,
--		 test_reg_trslv_value_slv_load_o	=> test_reg_trslv_value_slv_load_s
--	  );
--
--	--extern register
--	ext_reg : process (sys_clk_100_s,test_reg_trslv_led_buff_s,test_reg_trslv_value_slv_buff_s)
--	begin
--		if (sys_clk_100_s='1')and(sys_clk_100_s'event) then
--			if (test_reg_trslv_led_load_s='1') then
--				trslv_led_s <= test_reg_trslv_led_buff_s;
--			end if;
--			if (test_reg_trslv_value_slv_load_s='1') then
--				trslv_value_slv_s <= test_reg_trslv_value_slv_buff_s;
--			end if;
--		end if;
--	end process;

--	-- Auxiliary pins
--	aux_leds_o(0)	<= trslv_led_s(0);
--	aux_leds_o(1)	<= trslv_led_s(1);
--	aux_leds_o(2)	<= trslv_led_s(2);
--	aux_leds_o(3)	<= trslv_led_s(3);

end Behavioral;

