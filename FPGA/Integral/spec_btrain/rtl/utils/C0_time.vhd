----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:51:00 04/09/2014 
-- Design Name: 
-- Module Name:    C0_time - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

library work;
use work.utils_pkg.all;

entity C0_time is
	port(
		clk_i 	: in std_logic;
		reset_i	: in std_logic;

		B_i   : in std_logic_vector(31 downto 0);
		Bdot_i: in std_logic_vector(31 downto 0);
		C0_i  : in std_logic;
		
		reg_c0_time_i        : in std_logic_vector(31 downto 0);
		reg_b_c0_t_o         : out std_logic_vector(31 downto 0);
		reg_max_b_c0_t_i     : in std_logic_vector(31 downto 0);
		reg_max_b_c0_t_load_i: in std_logic;
		reg_max_b_c0_t_o     : out std_logic_vector(31 downto 0);

		reg_bdot_c0_t_o         : out std_logic_vector(31 downto 0);
		reg_max_bdot_c0_t_i     : in std_logic_vector(31 downto 0);
		reg_max_bdot_c0_t_load_i: in std_logic;
		reg_max_bdot_c0_t_o     : out std_logic_vector(31 downto 0)
	);
end C0_time;

architecture Behavioral of C0_time is

	type C0_state is
	(
		IDLE,
		COUNT,
		LATCH
	);

	--Signal
	signal state_s : C0_state;
	signal C0_pulse_s : std_logic;
	
	signal reg_b_c0_t_s        : std_logic_vector(31 downto 0);
	signal reg_max_b_c0_t_s    : std_logic_vector(31 downto 0);
	
	signal reg_bdot_c0_t_s      : std_logic_vector(31 downto 0);
	signal reg_max_bdot_c0_t_s  : std_logic_vector(31 downto 0);
	
	signal cnt_delay_s : unsigned(31 downto 0);

begin

	--State Machine
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				reg_b_c0_t_s    <= (others=>'0');
				reg_max_b_c0_t_s<= (others=>'0');
				cnt_delay_s     <= (others=>'0');
				state_s <= IDLE;
			else
				reg_b_c0_t_o    <= reg_b_c0_t_s;
				reg_max_b_c0_t_o<= reg_max_b_c0_t_s;
				reg_bdot_c0_t_o    <= reg_bdot_c0_t_s;
				reg_max_bdot_c0_t_o<= reg_max_bdot_c0_t_s;

				if (reg_max_b_c0_t_load_i='1') then
					reg_max_b_c0_t_s <= reg_max_b_c0_t_i;
				end if;
				if (reg_max_bdot_c0_t_load_i='1') then
					reg_max_bdot_c0_t_s <= reg_max_bdot_c0_t_i;
				end if;
				
				case state_s is
					when IDLE =>
						if (C0_pulse_s='1') then
							state_s <= COUNT;
						end if;
						cnt_delay_s <= (others=>'0');
					when COUNT =>	
						if (signed(cnt_delay_s)>=signed(unsigned(reg_c0_time_i))) then
							state_s <= LATCH;
						end if;
						cnt_delay_s <= cnt_delay_s+1;
					when LATCH =>
						state_s <= IDLE;
						--B
						reg_b_c0_t_s <= B_i;
						if ((signed(B_i)>signed(reg_max_b_c0_t_s))and
								((reg_max_bdot_c0_t_load_i='0'))) then
							reg_max_b_c0_t_s <= B_i;
						end if;
						--Bdot
						reg_bdot_c0_t_s <= Bdot_i;
						if ((signed(Bdot_i)>signed(reg_max_bdot_c0_t_s))and
								((reg_max_bdot_c0_t_load_i='0'))) then
							reg_max_bdot_c0_t_s <= Bdot_i;
						end if;
				end case;
			end if;
		end if;
	end process;

	--Rising edge for C0
	cmp_start_calib : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> C0_i,
		s_o	=> C0_pulse_s
	);

end Behavioral;

