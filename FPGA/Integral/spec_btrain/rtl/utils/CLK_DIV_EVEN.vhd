----------------------------------------------------------------------------------
-- Company: CERN TE/MSC-MM
-- Engineer: David Giloteaux
-- 
-- Create Date:    10:53:17 07/24/2012 
-- Design Name: 
-- Module Name:    CLK_DIV_EVEN - Behavioral 
-- Project Name: New B-Train System
-- Target Devices: 
-- Tool versions: 
-- Description: 50% Duty Cycle Clock Divider with an EVEN number divisor rate
--              Create an add divider with a 50% duty cycle generate two clocks at half 
--              the desired output frequency with a quadrature-phase relationship.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
-- library UNISIM;
-- use UNISIM.VComponents.all;

entity CLK_DIV_EVEN is
    
	 Generic ( n: natural := 40 ); -- n : Divisor Rate must be an EVEN number
            
    Port ( CLK_IN : in std_logic; -- CLK_IN must have a 50% duty cycle
           RESET : in std_logic;  -- Active high asynchronous reset
			  DIV_CLK_OUT : out std_logic);
			  
end CLK_DIV_EVEN;

architecture Behavioral of CLK_DIV_EVEN is
  
        subtype divtype is natural range 0 to n-1;
		  signal counter: divtype;
		  -- This subtype will constrain counter width. 
        -- Tell to the synthesizer how many bits wide the generated counter should be.
        signal div1:    std_logic :='0';
        signal en_tff1: std_logic :='0';
		  
begin
                assert ((((n/2)*2) = n) and (n>2))
					 --Check if n is EVEN and greater than 2
					       report "Clock divisor must be an even and positive number > 2!"
							 severity failure; 
							 --Force the simulator or the synthesizer to halt if that is not the case
					 
					 process(CLK_IN, RESET, counter)
                begin
                        if (RESET = '1') then
                                counter <= 0;
                        elsif rising_edge(CLK_IN) then
                                if (counter = (n-1)) then counter <= 0;
										  -- Counter count to n-1 on the rising edge of clkin
                                                     else counter <= counter + 1;
                                end if;
                        end if;
                end process;

                en_tff1 <= '1' when ( (counter = 0) or (counter = (n/2)) ) else '0';
					 -- Enable the Toggle Flip-Flop Div1

                process(CLK_IN, RESET, en_tff1, div1)
                begin
                        if (RESET = '1') then div1 <= '0';
                        elsif rising_edge(CLK_IN) then
                                if (en_tff1 = '1') then div1 <= not(div1);
                                end if;
                        end if;
                end process;

                DIV_CLK_OUT <= div1;

end Behavioral;

