----------------------------------------------------------------------------------
-- Company: 		CERN/TE-MS-MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    15:46:51 01/18/2013 
-- Design Name: 
-- Module Name:    Mult32x32_2chan - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 	2 Multiplier 32bits x 32bits = 64 bits and additioner
--						The state machine need 8 clock to 
--						generate output result.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Mult32x32_2chan is
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;

		A_i	: in std_logic_vector(31 downto 0);
		B_i	: in std_logic_vector(31 downto 0);
		
		C_i	: in std_logic_vector(31 downto 0);
		D_i	: in std_logic_vector(31 downto 0);
		
		start_i	: in std_logic;

		result_o		: out std_logic_vector(63 downto 0);	
		result_rdy_o: out std_logic
	);
end Mult32x32_2chan;

architecture Behavioral of Mult32x32_2chan is

	--Type
	type FD_sum_states is 
	(	
		IDLE,
		LATCH_IN,
		MULT,
		ADD1,
		ADD2,
		SIGN,
		ADD3,
		LATCH_OUT
	);

	--Signal
	signal state_s	: FD_sum_states;
	
	signal A_in_s	: unsigned(31 downto 0);
	signal B_in_s	: unsigned(31 downto 0);
	signal C_in_s	: unsigned(31 downto 0);
	signal D_in_s	: unsigned(31 downto 0);

	signal A_sign_s,B_sign_s,C_sign_s,D_sign_s : std_logic;

	signal AB_11_s	: unsigned(31 downto 0);
	signal AB_12_s	: unsigned(31 downto 0);
	signal AB_21_s	: unsigned(31 downto 0);
	signal AB_22_s	: unsigned(31 downto 0);
	signal CD_11_s	: unsigned(31 downto 0);
	signal CD_12_s	: unsigned(31 downto 0);
	signal CD_21_s	: unsigned(31 downto 0);
	signal CD_22_s	: unsigned(31 downto 0);
	
	signal AB_1_s	: unsigned(63 downto 0);
	signal AB_2_s	: unsigned(63 downto 0);
	signal CD_1_s	: unsigned(63 downto 0);
	signal CD_2_s	: unsigned(63 downto 0);
	
	signal AB_s	: unsigned(63 downto 0);
	signal CD_s	: unsigned(63 downto 0);

	signal result_s : unsigned(63 downto 0);

--	signal latch_in_s		: std_logic;
	signal latch_out_s	: std_logic;
	
begin

	--State machine 
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				state_s <= IDLE;

				A_in_s	<= (others=>'0');
				B_in_s	<= (others=>'0');
				
				C_in_s	<= (others=>'0');
				D_in_s	<= (others=>'0');
				A_sign_s <= '0';
				B_sign_s <= '0';
				C_sign_s <= '0';
				D_sign_s <= '0';

				AB_11_s		<= (others=>'0');
				AB_12_s		<= (others=>'0');
				AB_21_s		<= (others=>'0');
				AB_22_s		<= (others=>'0');
				AB_1_s		<= (others=>'0');
				AB_2_s		<= (others=>'0');
				AB_s			<= (others=>'0');
				
				CD_11_s		<= (others=>'0');
				CD_12_s		<= (others=>'0');
				CD_21_s		<= (others=>'0');
				CD_22_s		<= (others=>'0');
				CD_1_s		<= (others=>'0');
				CD_2_s		<= (others=>'0');
				CD_s			<= (others=>'0');
				
				result_s		 <= (others=>'0');
				result_o     <= (others=>'0');
				result_rdy_o <= '0';
			else
				result_rdy_o <= '0';
				case state_s is
					when IDLE =>
						if (start_i = '1') then
							state_s <= LATCH_IN;
						end if;
					when LATCH_IN =>
						state_s <= MULT;
						if (A_i(A_i'length-1)='0') then
							A_in_s	<= unsigned(A_i);
							A_sign_s <= '0';
						else
							A_in_s	<= unsigned(not A_i) + unsigned(to_signed(1,A_i'length));
							A_sign_s <= '1';
						end if;
						if (B_i(B_i'length-1)='0') then
							B_in_s	<= unsigned(B_i);
							B_sign_s <= '0';
						else
							B_in_s	<= unsigned(not B_i) + unsigned(to_signed(1,B_i'length));
							B_sign_s <= '1';
						end if;
						
						if (C_i(C_i'length-1)='0') then
							C_in_s	<= unsigned(C_i);
							C_sign_s <= '0';
						else
							C_in_s	<= unsigned(not C_i) + unsigned(to_signed(1,C_i'length));
							C_sign_s <= '1';
						end if;
						if (D_i(D_i'length-1)='0') then
							D_in_s	<= unsigned(D_i);
							D_sign_s <= '0';
						else
							D_in_s	<= unsigned(not D_i) + unsigned(to_signed(1,D_i'length));
							D_sign_s <= '1';
						end if;
					when MULT =>
						state_s <= ADD1;
						latch_out_s	<= '0';
						AB_11_s		<= A_in_s(A_in_s'length/2-1 downto 0)					* B_in_s(B_in_s'length/2-1 downto 0);
						AB_12_s		<= A_in_s(A_in_s'length-1 downto A_in_s'length/2)	* B_in_s(B_in_s'length/2-1 downto 0);
						AB_21_s		<= A_in_s(A_in_s'length/2-1 downto 0)					* B_in_s(B_in_s'length-1 downto B_in_s'length/2);
						AB_22_s		<= A_in_s(A_in_s'length-1 downto A_in_s'length/2)	* B_in_s(B_in_s'length-1 downto B_in_s'length/2);

						CD_11_s		<= C_in_s(C_in_s'length/2-1 downto 0)					* D_in_s(D_in_s'length/2-1 downto 0);
						CD_12_s		<= C_in_s(C_in_s'length-1 downto C_in_s'length/2)	* D_in_s(D_in_s'length/2-1 downto 0);
						CD_21_s		<= C_in_s(C_in_s'length/2-1 downto 0)					* D_in_s(D_in_s'length-1 downto D_in_s'length/2);
						CD_22_s		<= C_in_s(C_in_s'length-1 downto C_in_s'length/2)	* D_in_s(D_in_s'length-1 downto D_in_s'length/2);
					when ADD1 =>
						state_s <= ADD2;
						AB_1_s		<= (x"00000000" & AB_11_s) + (x"0000" & AB_12_s & x"0000");
						AB_2_s		<= (x"0000" & AB_21_s & x"0000") + (AB_22_s & x"00000000");

						CD_1_s		<= (x"00000000" & CD_11_s) + (x"0000" & CD_12_s & x"0000");
						CD_2_s		<= (x"0000" & CD_21_s & x"0000") + (CD_22_s & x"00000000");
					when ADD2 =>
						state_s <= SIGN;
						AB_s			<= AB_1_s + AB_2_s;

						CD_s			<= CD_1_s + CD_2_s;
					when SIGN =>
						state_s <= ADD3;
						if ((A_sign_s='1') xor (B_sign_s='1')) then
							AB_s <= unsigned(not AB_s) + unsigned(to_signed(1,AB_s'length)); 						
						end if;
						if ((C_sign_s='1') xor (D_sign_s='1')) then
							CD_s <= unsigned(not CD_s) + unsigned(to_signed(1,CD_s'length)); 						
						end if;						
					when ADD3 =>
						state_s <= LATCH_OUT;
						result_s		<= AB_s + CD_s;
					when LATCH_OUT =>
						state_s <= IDLE;
						result_o	<= std_logic_vector(result_s);
						result_rdy_o <= '1';
				end case;
			end if;
		end if;
	end process;

end Behavioral;

