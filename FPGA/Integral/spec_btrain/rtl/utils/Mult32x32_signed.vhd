----------------------------------------------------------------------------------
-- Company: 		CERN/TE-MS-MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    15:26:13 02/25/2015 
-- Design Name: 
-- Module Name:    Mult32x32_signed - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 	Multiplier 32bits x 32bits signed
--						The state machine need clocks to 
--						generate output result.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.utils_pkg.all;

entity Mult32x32_signed is
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;

		A_i	: in std_logic_vector(31 downto 0);
		B_i	: in std_logic_vector(31 downto 0);
				
		start_i	: in std_logic;

		result_o		: out std_logic_vector(63 downto 0);	
		result_rdy_o: out std_logic
	);
end Mult32x32_signed;

architecture Behavioral of Mult32x32_signed is

	--Type
	type mult_states is 
	(	
		IDLE,
		LATCH_IN,
		MULT,
		ADD1,
		ADD2,
		SIGN,
		LATCH_OUT
	);

	--Signal
	signal state_s	: mult_states;
	
	signal start_s : std_logic;
	
	signal A_in_s	: unsigned(31 downto 0);
	signal B_in_s	: unsigned(31 downto 0);

	signal A_sign_s,B_sign_s,C_sign_s,D_sign_s : std_logic;

	signal AB_11_s	: unsigned(31 downto 0);
	signal AB_12_s	: unsigned(31 downto 0);
	signal AB_21_s	: unsigned(31 downto 0);
	signal AB_22_s	: unsigned(31 downto 0);
	
	signal AB_1_s	: unsigned(63 downto 0);
	signal AB_2_s	: unsigned(63 downto 0);
	
	signal AB_s	: unsigned(63 downto 0);

	signal result_s : unsigned(63 downto 0);

	signal latch_out_s : std_logic;
	
begin

	--State machine 
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				state_s <= IDLE;

				A_in_s	<= (others=>'0');
				B_in_s	<= (others=>'0');
				
				A_sign_s <= '0';
				B_sign_s <= '0';

				AB_11_s		<= (others=>'0');
				AB_12_s		<= (others=>'0');
				AB_21_s		<= (others=>'0');
				AB_22_s		<= (others=>'0');
				AB_1_s		<= (others=>'0');
				AB_2_s		<= (others=>'0');
				AB_s			<= (others=>'0');
				
				result_s		 <= (others=>'0');
				result_o     <= (others=>'0');
				result_rdy_o <= '0';
			else

				case state_s is
					when IDLE =>
						if (start_s = '1') then
							state_s <= LATCH_IN;
						end if;
						result_rdy_o <= '1';
						
					when LATCH_IN =>
						state_s <= MULT;
						result_rdy_o <= '0';
						if (A_i(A_i'length-1)='0') then
							A_in_s	<= unsigned(A_i);
							A_sign_s <= '0';
						else
							A_in_s	<= unsigned(not A_i) + unsigned(to_signed(1,A_i'length));
							A_sign_s <= '1';
						end if;
						if (B_i(B_i'length-1)='0') then
							B_in_s	<= unsigned(B_i);
							B_sign_s <= '0';
						else
							B_in_s	<= unsigned(not B_i) + unsigned(to_signed(1,B_i'length));
							B_sign_s <= '1';
						end if;
						
					when MULT =>
						state_s <= ADD1;
						latch_out_s	<= '0';
						result_rdy_o <= '0';
						AB_11_s		<= A_in_s(A_in_s'length/2-1 downto 0)					* B_in_s(B_in_s'length/2-1 downto 0);
						AB_12_s		<= A_in_s(A_in_s'length-1 downto A_in_s'length/2)	* B_in_s(B_in_s'length/2-1 downto 0);
						AB_21_s		<= A_in_s(A_in_s'length/2-1 downto 0)					* B_in_s(B_in_s'length-1 downto B_in_s'length/2);
						AB_22_s		<= A_in_s(A_in_s'length-1 downto A_in_s'length/2)	* B_in_s(B_in_s'length-1 downto B_in_s'length/2);

					when ADD1 =>
						state_s <= ADD2;
						result_rdy_o <= '0';
						AB_1_s		<= (x"00000000" & AB_11_s) + (x"0000" & AB_12_s & x"0000");
						AB_2_s		<= (x"0000" & AB_21_s & x"0000") + (AB_22_s & x"00000000");

					when ADD2 =>
						state_s <= SIGN;
						result_rdy_o <= '0';
						AB_s			<= AB_1_s + AB_2_s;

					when SIGN =>
						state_s <= LATCH_OUT;
						result_rdy_o <= '0';
						if ((A_sign_s='1') xor (B_sign_s='1')) then
							AB_s <= unsigned(not AB_s) + unsigned(to_signed(1,AB_s'length)); 						
						end if;

					when LATCH_OUT =>
						state_s <= IDLE;
						result_o	<= std_logic_vector(AB_s);
						result_rdy_o <= '1';
				end case;
			end if;
		end if;
	end process;

	--Rising edge for start
	cmp_start : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> start_i,
		s_o	=> start_s
	);

end Behavioral;

