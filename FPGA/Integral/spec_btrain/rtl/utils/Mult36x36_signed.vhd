----------------------------------------------------------------------------------
-- Company: 		CERN/TE-MS-MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    15:46:51 01/18/2013 
-- Design Name: 
-- Module Name:    Mult36x36_signed - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 	2 Multiplier 32bits x 32bits = 64 bits and additioner
--						The state machine need 8 clock to 
--						generate output result.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

library work;
use work.utils_pkg.all;

entity Mult36x36_signed is
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;

		A_i	: in std_logic_vector(35 downto 0);
		B_i	: in std_logic_vector(35 downto 0);
				
		start_i	: in std_logic;

		result_o		: out std_logic_vector(71 downto 0);	
		result_rdy_o: out std_logic
	);
end Mult36x36_signed;

architecture Behavioral of Mult36x36_signed is

	--Type
	type mult_states is 
	(	
		IDLE,
		LATCH_IN,
		MULT,
		ADD,
		LATCH_OUT
	);

	--Signal
	signal state_s	: mult_states;

	signal start_s	: std_logic;
	
	signal A_in_s	: unsigned(35 downto 0);
	signal B_in_s	: unsigned(35 downto 0);

	signal sign_A_s : std_logic;
	signal sign_B_s : std_logic;

	signal AB_11_s	: unsigned(35 downto 0);
	signal AB_12_s	: unsigned(35 downto 0);
	signal AB_21_s	: unsigned(35 downto 0);
	signal AB_22_s	: unsigned(35 downto 0);
	
	signal AB_1_s	: unsigned(71 downto 0);
	signal AB_2_s	: unsigned(71 downto 0);
	
begin

	--State Machine Output
	process (clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				AB_11_s		<= (others=>'0');
				AB_12_s		<= (others=>'0');
				AB_21_s		<= (others=>'0');
				AB_22_s		<= (others=>'0');
				AB_1_s		<= (others=>'0');
				AB_2_s		<= (others=>'0');
				A_in_s		<= (others=>'0');
				B_in_s		<= (others=>'0');
				result_o		<= (others=>'0');
				result_rdy_o<= '1';
				sign_A_s <= '0';
				sign_B_s <= '0';
			else
				case state_s is
					when IDLE =>
						if (start_s = '1') then
							state_s <= LATCH_IN;
						end if;
						result_rdy_o <= '1';
					when LATCH_IN =>
						state_s <= MULT;
						if (A_i(A_i'length-1)='0') then
							A_in_s	<= unsigned(A_i);
							sign_A_s	<= '0';
						else
							A_in_s	<= 1 + unsigned(not A_i);
							sign_A_s	<= '1';
						end if;
						if (B_i(A_i'length-1)='0') then
							B_in_s	<= unsigned(B_i);
							sign_B_s	<= '0';
						else
							B_in_s	<= 1 + unsigned(not B_i);
							sign_B_s	<= '1';
						end if;
						result_rdy_o <= '0';
					when MULT =>
						state_s <= ADD;
						AB_11_s	<= A_in_s(A_in_s'length/2-1 downto 0)					* B_in_s(B_in_s'length/2-1 downto 0);
						AB_12_s	<= A_in_s(A_in_s'length-1 downto A_in_s'length/2)	* B_in_s(B_in_s'length/2-1 downto 0);
						AB_21_s	<= A_in_s(A_in_s'length/2-1 downto 0)					* B_in_s(B_in_s'length-1 downto B_in_s'length/2);
						AB_22_s	<= A_in_s(A_in_s'length-1 downto A_in_s'length/2)	* B_in_s(B_in_s'length-1 downto B_in_s'length/2);
						result_rdy_o <= '0';
					when ADD =>
						state_s <= LATCH_OUT;
						AB_1_s	<= (x"000000000" & AB_11_s) + (x"0000" & "00" & AB_12_s & "00" & x"0000");
						AB_2_s	<= (x"0000" & "00" & AB_21_s & "00" & x"0000") + (AB_22_s & x"000000000");
						result_rdy_o <= '0';
					when LATCH_OUT =>
						state_s <= IDLE;
						if    (sign_A_s='0' and sign_B_s='0') or (sign_A_s='1' and sign_B_s='1') then
							result_o		<= AB_1_s + AB_2_s;
						elsif (sign_A_s='0' and sign_B_s='1') or (sign_A_s='1' and sign_B_s='0') then
							result_o		<= 1 + unsigned(not (AB_1_s + AB_2_s));
						end if;
						result_rdy_o <= '0';
				end case;
			end if;
		end if;
	end process;

	--Input rising edge
	cmp_start : RisingDetect
	port map(
		clk_i		=> clk_i,
		reset_i	=> reset_i,
		
		s_i	=> start_i,
		s_o	=> start_s
	);


end Behavioral;

