----------------------------------------------------------------------------------
-- Company: 		CERN/TE/MSC/MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    17:25:22 01/22/2013 
-- Design Name: 
-- Module Name:    RisingDetect - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RisingDetect is
	port (
		clk_i			: in std_logic;
		reset_i		: in std_logic;
		
		s_i	: in std_logic;
		s_o	: out std_logic
	);
end RisingDetect;

architecture Behavioral of RisingDetect is

	--Signal
	signal s_km1_s	: std_logic;
	
begin

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				s_km1_s 	<= '0';
				s_o		<= '0';
			else
				s_km1_s 	<= s_i;
				if (s_i='1' and s_km1_s='0') then
					s_o	<= '1';
				else
					s_o	<= '0';
				end if;
			end if;
		end if;
	end process;
	
end Behavioral;

