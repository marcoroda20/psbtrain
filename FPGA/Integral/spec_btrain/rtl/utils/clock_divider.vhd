----------------------------------------------------------------------------------
-- Company:        CERN TE/MSC
-- Engineer:       Daniel Oberson
-- 
-- Create Date:    12:27:02 07/08/2013 
-- Design Name: 
-- Module Name:    clock_divider - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

entity clock_divider is
generic (
    g_f_in : integer := 100000000;	-- Input frequency in [Hz]
    g_f_out : integer := 1				-- Output frequency in [Hz]
    );	
    port (
		clk_i			: in std_logic;
		reset_i		: in std_logic;

		clk_o	: out std_logic
	);
end clock_divider;

architecture Behavioral of clock_divider is

	constant COUNTER_MAX : integer := g_f_in/(g_f_out*2);
	--Signal
	signal counter_s	: integer;
	signal clk_s		: std_logic;
	
begin

	process (clk_i,reset_i,counter_s,clk_s)
	begin
		if (reset_i='1') then
			counter_s <= 0;
			clk_s	<= '0';
		elsif (clk_i'event and clk_i='1') then
			if (counter_s<=COUNTER_MAX) then
				counter_s <= counter_s + 1;
			else
				counter_s <= 0;
				clk_s	<= not clk_s;
			end if;
		end if;
	end process;
	clk_o <= clk_s;
	
end Behavioral;

