----------------------------------------------------------------------------------
-- Company: 		CERN TE/MSC
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    14:51:36 05/21/2013 
-- Design Name: 
-- Module Name:    divider - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.utils_pkg.all;

entity divider is
	generic (NB_BIT : integer := 32);
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		a_i	: in std_logic_vector(NB_BIT-1 downto 0);
		b_i	: in std_logic_vector(NB_BIT-1 downto 0);

		start_divider_i	: in std_logic;
		cmd_divider_i		: in std_logic;
		
		q_o	: out std_logic_vector(NB_BIT-1 downto 0);
		r_o	: out std_logic_vector(NB_BIT-1 downto 0);
		f_o	: out std_logic_vector(NB_BIT-1 downto 0);
		
		divide_by_0_o	: out std_logic;
		end_divider_o	: out std_logic
	);
end divider;

architecture Behavioral of divider is

  --Constant 
  constant ZERO_S		: std_logic_vector(NB_BIT-1 downto 0) := (others=>'0');
  constant F_ZERO_S	: std_logic_vector(2*NB_BIT-1 downto 0) := (others=>'0');

	--Type
	type mean_meas_state is
	(
		IDLE,
		LOAD_INPUT,
		DIVIDER_CALC,BY_ZERO_DIVIDER,FRACTION_CALC,
		LOAD_OUT_DIV,LOAD_OUT_FRAC
	);

	--Signal
	signal state_s 		: mean_meas_state;
	signal next_state_s	: mean_meas_state;
	
	signal start_divider_s	: std_logic;
	signal q_s 					: std_logic_vector(NB_BIT-1 downto 0);
	signal r_s 					: std_logic_vector(NB_BIT downto 0);
	signal f_q_s 				: std_logic_vector(2*NB_BIT-1 downto 0);
	signal f_r_s 				: std_logic_vector(2*NB_BIT downto 0);
	signal a_s					: std_logic_vector(NB_BIT-1 downto 0);
	signal b_s					: std_logic_vector(NB_BIT-1 downto 0);
	signal f_a_s				: std_logic_vector(2*NB_BIT-1 downto 0);
	signal f_b_s				: std_logic_vector(2*NB_BIT-1 downto 0);
	signal nb_bit_cnt_s 		: integer := 32;
	signal sign_s				: std_logic;	
	
begin

--Euclide algorithme 
--fonction [Q, R] = diviser(a, b)
--  si b == 0 alors
--    gnre l'exception "division par zro" ;
--  fin
--  Q := 0 ; 
--  R := 0 ;    // initialisation
--  pour i = n-1 ? 0
--    R = dcalage__gauche_de_bits(R, 1) ; // quivaut  rajouter un 0  droite
--    R(1) = a(i) ;      // le bit de poids faible de R est le i-me bit du numrateur
--    si R >= b alors
--      Q(i) = 1 ;       // i-me bit du quotient
--      R = R - b ;      // reste
--    fin
--  fin
--  retourne [Q, R] ;
--fin

	--State machine
	process (state_s,start_divider_s,nb_bit_cnt_s,b_i,cmd_divider_i)
	begin
		case state_s is
			when IDLE => 
				if (start_divider_s='1') then
					if (unsigned(b_i)=0) then
						next_state_s <= BY_ZERO_DIVIDER;
					else
						next_state_s <= LOAD_INPUT;
					end if;
				else
					next_state_s <= IDLE;
				end if;
				end_divider_o <= '1';
				
			when BY_ZERO_DIVIDER =>
				next_state_s <= LOAD_OUT_DIV;
				end_divider_o <= '0';
				
			when LOAD_INPUT =>
				if (cmd_divider_i='0') then
					next_state_s <= DIVIDER_CALC;
				else
					next_state_s <= FRACTION_CALC;
				end if;
				end_divider_o <= '0';
				
			when DIVIDER_CALC =>
				if (nb_bit_cnt_s/=0) then
					next_state_s <= DIVIDER_CALC;	
				else
					next_state_s <= LOAD_OUT_DIV;	
				end if;
				end_divider_o <= '0';
				
			when FRACTION_CALC =>
				if (nb_bit_cnt_s/=0) then
					next_state_s <= FRACTION_CALC;	
				else
					next_state_s <= LOAD_OUT_FRAC;	
				end if;
				end_divider_o <= '0';
				
			when LOAD_OUT_DIV => 
				next_state_s <= IDLE;	
				end_divider_o <= '0';
				
			when LOAD_OUT_FRAC => 
				next_state_s <= IDLE;	
				end_divider_o <= '0';
				
		end case;
	end process;
	
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				state_s <= IDLE;
			else
				state_s <= next_state_s;
			end if;
		end if;
	end process;

	--Euclide algorithme
	process (clk_i) 
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				nb_bit_cnt_s <= NB_BIT;
				divide_by_0_o <= '0';
				f_o <= (others=>'0');
				q_o <= (others=>'0');
				r_o <= (others=>'0');
				q_s <= (others=>'0');
				r_s <= (others=>'0');
				a_s <= (others=>'0');
				b_s <= (others=>'0');
				f_a_s <= (others=>'0');
				f_b_s <= (others=>'0');
				sign_s <= '0';
			else
				if (state_s=DIVIDER_CALC) then
					divide_by_0_o <= '0';
					if   (nb_bit_cnt_s=NB_BIT) then 
						q_s <= (others=>'0');
						r_s <= ZERO_S & a_s(nb_bit_cnt_s-1);
						nb_bit_cnt_s <= nb_bit_cnt_s - 1;
					elsif(nb_bit_cnt_s<NB_BIT) then 
						if (r_s(NB_BIT-1 downto 0)>=b_s) then
							q_s(nb_bit_cnt_s) <= '1';
							if (nb_bit_cnt_s/=0) then
								r_s <= std_logic_vector(unsigned(r_s(NB_BIT-1 downto 0)) - unsigned(b_s)) & a_s(nb_bit_cnt_s-1);
							else
								r_s <= '0' & std_logic_vector(unsigned(r_s(NB_BIT-1 downto 0)) - unsigned(b_s));
							end if ;
						else
							if (nb_bit_cnt_s/=0) then
								r_s <= r_s(NB_BIT-1 downto 0) & a_s(nb_bit_cnt_s-1);
							else
								r_s <= '0' & r_s(NB_BIT-1 downto 0);
							end if;
						end if;
						if (nb_bit_cnt_s/=0) then
							nb_bit_cnt_s <= nb_bit_cnt_s - 1;
						end if;
					end if;
				elsif (state_s=FRACTION_CALC) then
					divide_by_0_o <= '0';
					if   (nb_bit_cnt_s=2*NB_BIT) then 
						f_q_s <= (others=>'0');
						f_r_s <= F_ZERO_S & f_a_s(nb_bit_cnt_s-1);
						nb_bit_cnt_s <= nb_bit_cnt_s - 1;
					elsif(nb_bit_cnt_s<2*NB_BIT) then 
						if (f_r_s(2*NB_BIT-1 downto 0)>=f_b_s) then
							f_q_s(nb_bit_cnt_s) <= '1';
							if (nb_bit_cnt_s/=0) then
								f_r_s <= std_logic_vector(unsigned(f_r_s(2*NB_BIT-1 downto 0)) - unsigned(f_b_s)) & f_a_s(nb_bit_cnt_s-1);
							else
								f_r_s <= '0' & std_logic_vector(unsigned(f_r_s(2*NB_BIT-1 downto 0)) - unsigned(f_b_s));
							end if ;
						else
							if (nb_bit_cnt_s/=0) then
								f_r_s <= f_r_s(2*NB_BIT-1 downto 0) & f_a_s(nb_bit_cnt_s-1);
							else
								f_r_s <= '0' & f_r_s(2*NB_BIT-1 downto 0);
							end if;
						end if;
						if (nb_bit_cnt_s/=0) then
							nb_bit_cnt_s <= nb_bit_cnt_s - 1;
						end if;
					end if;
				elsif (state_s=BY_ZERO_DIVIDER) then
					divide_by_0_o <= '1';
					q_s <= (others=>'0');
					r_s <= (others=>'0');
					f_q_s <= (others=>'0');
					f_r_s <= (others=>'0');
				elsif (state_s=LOAD_INPUT) then
					if (cmd_divider_i='0') then
						sign_s <= a_i(NB_BIT-1) xor b_i(NB_BIT-1);
						if (a_i(NB_BIT-1)='0') then
							a_s <= a_i;
						else
							a_s <= std_logic_vector(1 + unsigned(not a_i));
						end if;
						if (b_i(NB_BIT-1)='0') then
							b_s <= b_i;
						else
							b_s <= std_logic_vector(1 + unsigned(not b_i));
						end if;
						nb_bit_cnt_s <= NB_BIT;
					else
						sign_s <= a_i(NB_BIT-1) xor b_i(NB_BIT-1);
						if (a_i(NB_BIT-1)='0') then
							f_a_s <= a_i & ZERO_S;
						else
							f_a_s <= std_logic_vector(1 + unsigned(not a_i)) & ZERO_S;
						end if;
						if (b_i(NB_BIT-1)='0') then
							f_b_s <= ZERO_S & b_i;
						else
							f_b_s <= (not ZERO_S) & std_logic_vector(1 + unsigned(not b_i));
						end if;
						nb_bit_cnt_s <= 2*NB_BIT;
					end if;
				elsif (state_s=LOAD_OUT_DIV) then
					if (sign_s='0') then
						q_o <= q_s;
						r_o <= r_s(NB_BIT-1 downto 0);
					else
						q_o <= std_logic_vector(1 + unsigned(not q_s));
						r_o <= std_logic_vector(1 + unsigned(not r_s(NB_BIT-1 downto 0)));
					end if;
					f_o <= (others=>'0');
				elsif (state_s=LOAD_OUT_FRAC) then
					if (sign_s='0') then
						q_o <= f_q_s(2*NB_BIT-1 downto NB_BIT);
						r_o <= f_r_s(NB_BIT-1 downto 0);
						f_o <= f_q_s(NB_BIT-1 downto 0);
					else
						q_o <= std_logic_vector(1 + unsigned(not f_q_s(2*NB_BIT-1 downto NB_BIT)));
						r_o <= std_logic_vector(1 + unsigned(not f_r_s(NB_BIT-1 downto 0)));
						f_o <= std_logic_vector(1 + unsigned(not f_q_s(NB_BIT-1 downto 0)));
					end if;
				end if;
			end if;
		end if;
	end process;

	--Input rising edge
	cmp_start_divider : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> start_divider_i,
			s_o	=> start_divider_s
		);
		
end Behavioral;

