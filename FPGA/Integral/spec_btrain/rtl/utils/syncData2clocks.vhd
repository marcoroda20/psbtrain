----------------------------------------------------------------------------------
-- Company:        CERN TE/MSC/MM
-- Engineer:       Daniel Oberson
-- 
-- Create Date:    15:26:43 12/11/2014 
-- Design Name: 
-- Module Name:    syncData2clocks - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity syncData2clocks is
	generic(
		g_DATA_LENGTH : integer := 32
	);
	port(
		clk_wr_i : in std_logic;
		clk_rd_i : in std_logic;
		reset_i  : in std_logic;
		
		wr_i   : in std_logic;
		data_i : in std_logic_vector(g_DATA_LENGTH-1 downto 0);
		rd_o   : out std_logic;
		data_o : out std_logic_vector(g_DATA_LENGTH-1 downto 0)
	);
end syncData2clocks;

architecture Behavioral of syncData2clocks is

	type fifo_t is array (0 to 1) of std_logic_vector(g_DATA_LENGTH-1 downto 0);  
	--Signals
	signal fifo_s : fifo_t :=(others => (others => '0')); 
	signal wr_s : std_logic;
	signal wr_km1_s : std_logic;
	signal new_data_in_s : std_logic;
	signal new_data_out_s : std_logic;
	signal rd_s : std_logic;

begin
	--Writing part of the little FIFO 1-deep
	process (clk_wr_i)
	begin
		if rising_edge(clk_wr_i) then
			if (reset_i='1') then
				fifo_s <= (others=>(others=>'0')); 
				wr_s <= '0';
				wr_km1_s <= '0';
			else
				wr_km1_s <= wr_i;
				if ((wr_i='1')and(wr_km1_s='0')) then
					wr_s <= not wr_s;
					if (wr_s='0') then
						fifo_s(0) <= data_i;
					else
						fifo_s(1) <= data_i;						
					end if;
					if (wr_s=rd_s) then
						new_data_in_s <='0';
					else
						new_data_in_s <='1';
					end if;
				end if;
			end if;
		end if;
	end process;

	--Reading part of the little FIFO 1-deep	
	process (clk_rd_i)
	begin
		if rising_edge(clk_rd_i) then
			if (reset_i='1') then
				rd_s <= '0';
				rd_o <= '0';
				data_o <= (others=>'0');
				new_data_out_s <= '0';
			else
				new_data_out_s <= new_data_in_s;
				if (new_data_out_s='1') then
					rd_o <= '1';
					rd_s <= not rd_s;
					if (rd_s='0') then
						data_o <= fifo_s(0);
					else
						data_o <= fifo_s(1);
					end if;
				else
					rd_o <= '0';
				end if;
			end if;
		end if;
	end process;
	

end Behavioral;

