----------------------------------------------------------------------------------
-- Company:       CERN TE/MSC/MM
-- Engineer:      Daniel Oberson
--  
-- Create Date:    12:01:42 09/27/2013 
-- Design Name: 
-- Module Name:    SynchroGen - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.utils_pkg.all;

entity SynchroGen is
	port(
		clk_i		: in std_logic; 
		reset_i	: in std_logic;

		reg_sample_period_i : in std_logic_vector(25 downto 0);
		send_tick_o       : out std_logic
	);
end SynchroGen;

architecture Behavioral of SynchroGen is

--Signal
signal sample_rate_counter_s : integer := 0;
signal zero_cnt_s     : std_logic;
signal tick_s         : std_logic;

begin

	--Sample rate control
	process (clk_i,reset_i,sample_rate_counter_s,reg_sample_period_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				sample_rate_counter_s <= 0;
				zero_cnt_s <= '0';
			else
				--No transmission if reg_sample_period = 0
				if (unsigned(reg_sample_period_i)/=0) then
					if (unsigned(to_signed(sample_rate_counter_s,reg_sample_period_i'length))
							<= (unsigned(reg_sample_period_i)-1)) then
						sample_rate_counter_s <= sample_rate_counter_s+1;
						zero_cnt_s <= '0';
					else
						sample_rate_counter_s <= 0;
						zero_cnt_s <= '1';
					end if;
				else
					sample_rate_counter_s <= 0;
					zero_cnt_s <= '0';
				end if;
			end if;
		end if;
	end process;
	send_tick_o <= tick_s;
	
	cmp : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> zero_cnt_s,
			s_o	=> tick_s
		);

end Behavioral;

