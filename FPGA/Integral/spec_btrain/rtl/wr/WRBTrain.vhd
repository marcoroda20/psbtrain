----------------------------------------------------------------------------------
-- Company:       CERN TE/MSC/MM
-- Engineer:      Daniel Oberson
-- 
-- Create Date:    08:47:56 09/30/2013 
-- Design Name: 
-- Module Name:    WRBTrain - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.wr_btrain_pkg.all;

entity WRBTrain is
	generic (
		g_data_width : integer:=16
	);
	port(
		clk_i		: in std_logic;
		reset_i	: in std_logic;
		
		simeff_bit_i   : in std_logic;
		error_bit_i    : in std_logic;
		d_low_marker_i : in std_logic;
		f_low_marker_i : in std_logic;
		C0_i           : in std_logic;
		zero_cycle_i   : in std_logic;
		
		reg_sample_period_i : in std_logic_vector(25 downto 0);

		rx_data_i    : in std_logic_vector(15 downto 0);
		rx_valid_i   : in std_logic;
		rx_first_i   : in std_logic;
		rx_lost_i    : in std_logic;
		rx_last_i    : in std_logic;

		tx_B_frame_i : in btrainFrameValue;
		tx_I_frame_i : in ImFrameValue;
		frame_ID_i	 : in std_logic_vector(7 downto 0);
		I_value_o    : out std_logic_vector(31 downto 0);
		new_I_rdy_o  : out std_logic;

		B_value_o      : out std_logic_vector(31 downto 0);
		Bdot_value_o   : out std_logic_vector(31 downto 0);
		G_value_o      : out std_logic_vector(31 downto 0);
		S_value_o      : out std_logic_vector(31 downto 0);
		d_low_marker_o : out std_logic;
		f_low_marker_o : out std_logic;
		C0_o           : out std_logic;
		zero_cycle_o   : out std_logic;
		eff_sim_o      : out std_logic;
		new_B_rdy_o    : out std_logic;
		
		losses_o     : out std_logic;
		losses_rdy_o : out std_logic;

		force_send_i : in std_logic;
		send_cnt_i   : in std_logic;
		tx_sync_o    : out std_logic;
		
		tx_last_o    : out std_logic;
		tx_flush_o   : out std_logic;
		tx_dreq_i    : in std_logic;
		tx_data_o    : out std_logic_vector(15 downto 0);
		tx_valid_o   : out std_logic
	);
end WRBTrain;

architecture Behavioral of WRBTrain is

	--Signal
	signal sync_s : std_logic;

	signal rxframe_ctrl_s   : btrainFrameCtrl;
	signal Irxframe_value_s : ImFrameValue;
	signal Brxframe_value_s : btrainFrameValue;
	signal rxframe_valid_s  : std_logic;

	signal factor_s : std_logic_vector(31 downto 0);
		
begin

	cmp_rxCtrl : rxCtrl 
		generic map(
			g_data_width => g_data_width
		)
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			rx_data_i  => rx_data_i,
			rx_valid_i => rx_valid_i,
			rx_first_i => rx_first_i,
			rx_last_i  => rx_last_i,
					
			rxframe_ctrl_o   => rxframe_ctrl_s,
			Irxframe_value_o => Irxframe_value_s,
			Brxframe_value_o => Brxframe_value_s,
			rxframe_valid_o  => rxframe_valid_s
		);

	process (clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				B_value_o    <= (others=>'0');
				Bdot_value_o <= (others=>'0');
				G_value_o    <= (others=>'0');
				S_value_o    <= (others=>'0');
				new_B_rdy_o  <= '0';
				I_value_o    <= (others=>'0');
				new_I_rdy_o  <= '0';
				losses_rdy_o <= '0';
				losses_o     <= '0';
			else
				losses_rdy_o <= rxframe_valid_s;
				losses_o     <= rx_lost_i;
				if (rxframe_valid_s='1') then
					if (rxframe_ctrl_s.frame_type=ID_BkFrame) then
						B_value_o     <= Brxframe_value_s.B;
						Bdot_value_o  <= Brxframe_value_s.Bdot;
						G_value_o     <= Brxframe_value_s.G;
						S_value_o     <= Brxframe_value_s.S;
						d_low_marker_o<= rxframe_ctrl_s.d_low_marker_bit;
						f_low_marker_o<= rxframe_ctrl_s.f_low_marker_bit;
						C0_o          <= rxframe_ctrl_s.C0_bit;
						zero_cycle_o  <= rxframe_ctrl_s.zero_cycle_bit;
						eff_sim_o     <= rxframe_ctrl_s.sim_eff_bit;
						new_B_rdy_o   <= '1';
					elsif (rxframe_ctrl_s.frame_type=ID_ImFrame) then
						I_value_o     <= Irxframe_value_s.I;
						new_I_rdy_o   <= '1';
					end if;
				else
					new_B_rdy_o <= '0';
					new_I_rdy_o <= '0';
				end if;
			end if;
		end if;
	end process;
	
	cmp_SynchroGen : SynchroGen
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			reg_sample_period_i => reg_sample_period_i,
			send_tick_o         => sync_s
		);
		
	cmp_txCtrl : txCtrl
		generic map(
			g_data_width => g_data_width
		)
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,

			-- Input frame for BtrainWR
			tx_B_frame_i   => tx_B_frame_i,
			tx_I_frame_i   => tx_I_frame_i,
			tx_sync_i      => sync_s,
			force_send_i   => force_send_i,
			tx_sent_o      => tx_sync_o,
			send_cnt_i     => send_cnt_i,
			simeff_bit_i   => simeff_bit_i,
			error_bit_i    => error_bit_i,
			d_low_marker_i => d_low_marker_i,
			f_low_marker_i => f_low_marker_i,
			C0_i           => C0_i,
			zero_cycle_i   => zero_cycle_i,
			frame_ID_i	   => frame_ID_i,
			
			-- Data word to be sent.
			tx_data_o  => tx_data_o,
			-- 1 indicates that the tx_data_o contains a valid data word.
			tx_valid_o => tx_valid_o,
			-- Synchronous data request: if active, the user may send a data word in
			-- the following clock cycle.
			tx_dreq_i  => tx_dreq_i,
			-- Used to indicate the last data word in a larger block of samples.
			tx_last_o  => tx_last_o,
			-- Flush command. When asserted, the streamer will immediatly send out all
			-- the data that is stored in its TX buffer, ignoring g_tx_timeout.
			tx_flush_o => tx_flush_o
		);
				
end Behavioral;

