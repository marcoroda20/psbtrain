----------------------------------------------------------------------------------
-- Company:       CERN TE/MSC/MM
-- Engineer:      Daniel Oberson
-- 
-- Create Date:    14:30:56 13/01/2014
-- Design Name: 
-- Module Name:    rxCtrl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.wr_btrain_pkg.all;

entity rxCtrl is
	generic (
		g_data_width : integer:=16
	);
	port(
		clk_i   : in std_logic; 
		reset_i : in std_logic;
		
		rx_data_i  : in std_logic_vector(g_data_width-1 downto 0);
		rx_valid_i : in std_logic;
		rx_first_i : in std_logic;
		rx_last_i  : in std_logic;
				
		rxframe_ctrl_o   : out btrainFrameCtrl;
		Irxframe_value_o : out ImFrameValue;
		Brxframe_value_o : out btrainFrameValue;
		rxframe_valid_o  : out std_logic
	);
end rxCtrl;

architecture Behavioral of rxCtrl is

	--Type
	type rx_btrain_state_t is (
			IDLE,
			FRAME_CTRL,
			I_FRAME,B_FRAME,
			END_FRAME
		);

	-- Signal
	signal state_s : rx_btrain_state_t;

	signal Iframe_value_s    : ImFrameValue;
	signal Bframe_value_s    : btrainFrameValue;
	signal Bframe_slv_s      : std_logic_vector(127 downto 0);
	signal frame_ctrl_s      : btrainFrameCtrl;
	signal frame_rdy_s       : std_logic;
	
	signal first_frame_s : std_logic;
	signal error_type_s  : std_logic;

	signal Bframe_counter_s : integer;

begin

	process(clk_i, reset_i)
	begin
		if rising_edge(clk_i) then 
			if (reset_i='1') then
				-- Frame for I value
				Iframe_value_s.I           <= (others => '0');
				-- Frame for B, Bdot, G, S values
				Bframe_value_s.B           <= (others => '0');
				Bframe_value_s.Bdot        <= (others => '0');
				Bframe_value_s.G           <= (others => '0');
				Bframe_value_s.S           <= (others => '0');
				Bframe_slv_s               <= (others => '0');
				-- Ctrl of frame
				frame_ctrl_s.bit_14to15    <= (others => '0');
				frame_ctrl_s.d_low_marker_bit <= '0';
				frame_ctrl_s.f_low_marker_bit <= '0';
				frame_ctrl_s.zero_cycle_bit<= '0';
				frame_ctrl_s.C0_bit        <= '0';
				frame_ctrl_s.error_bit     <= '0';
				frame_ctrl_s.sim_eff_bit   <= '0';
				frame_ctrl_s.frame_type    <= (others => '0');
				frame_rdy_s				      <= '0';
				-- after first frame reception 0
				first_frame_s              <= '1';
				-- error
				error_type_s               <= '0';	
				-- counter for B_frame
				Bframe_counter_s           <= 0;
			else
				case state_s is
				
					when IDLE => -- stand by state waiting for frame
						Bframe_counter_s <= 0;
						frame_rdy_s <= '0';
						if(rx_valid_i ='1') then   								
							-- first word 
							if (rx_first_i='1') then
								state_s <= FRAME_CTRL; 
								frame_ctrl_s.bit_14to15       <= rx_data_i(15 downto 14);
								frame_ctrl_s.d_low_marker_bit <= rx_data_i(13);
								frame_ctrl_s.f_low_marker_bit <= rx_data_i(12);
								frame_ctrl_s.zero_cycle_bit   <= rx_data_i(11);
								frame_ctrl_s.C0_bit           <= rx_data_i(10);
								frame_ctrl_s.error_bit        <= rx_data_i(9);
								frame_ctrl_s.sim_eff_bit      <= rx_data_i(8);
								frame_ctrl_s.frame_type       <= rx_data_i(7 downto 0);
							end if;
						else
							state_s 	<= IDLE;
						end if;
				
					when FRAME_CTRL => -- Ctrl of Frame to differentiate I and B frame
						if(rx_valid_i ='1') then                     
							-- Imagnet Frame
							if (frame_ctrl_s.frame_type=ID_ImFrame) then
								state_s <= I_FRAME; 
								Iframe_value_s.I(31 downto 16) <= rx_data_i;
							--  B Frame
							elsif (frame_ctrl_s.frame_type=ID_BkFrame) then
								state_s <= B_FRAME;
								Bframe_slv_s(Bframe_slv_s'length-1 downto Bframe_slv_s'length-g_data_width) <= rx_data_i;
							else
								state_s <= IDLE;              
								error_type_s <= '1';
							end if;
					end if;

					when I_FRAME =>                            -- I value
						if(rx_valid_i ='1') then                  
							state_s 	<= END_FRAME;
							Iframe_value_s.I(15 downto 0) <= rx_data_i;
						else
							state_s 	<= I_FRAME;                 -- waiting for data
						end if;
						
					when B_FRAME =>
						if(rx_valid_i ='1') then   									
							if (Bframe_counter_s<6) then
								Bframe_counter_s <= Bframe_counter_s + 1;
							else
								Bframe_counter_s <= 6;
							end if;
							Bframe_slv_s(Bframe_slv_s'length-(Bframe_counter_s+1)*g_data_width-1 downto Bframe_slv_s'length-(Bframe_counter_s+2)*g_data_width) <= rx_data_i;
							if(rx_last_i='1') then   			
								state_s 	<= END_FRAME;
							else
								state_s 	<= B_FRAME;
							end if;
						else
							state_s 	<= B_FRAME;			
						end if;
						
					when END_FRAME =>														
						frame_rdy_s <='1';
						Bframe_value_s.B    <= Bframe_slv_s(127 downto 96);
						Bframe_value_s.Bdot <= Bframe_slv_s( 95 downto 64);
						Bframe_value_s.G    <= Bframe_slv_s( 63 downto 32);
						Bframe_value_s.S    <= Bframe_slv_s( 31 downto  0);
						state_s <= IDLE;		
						if first_frame_s='1' then
							first_frame_s <= '0';
						end if;
						
				end case;  
			end if;
		end if;  
	end process;

	--Output
	rxframe_ctrl_o   <= frame_ctrl_s;
	Irxframe_value_o <= Iframe_value_s;
	Brxframe_value_o <= Bframe_value_s;
	rxframe_valid_o  <= frame_rdy_s;
	
end Behavioral;