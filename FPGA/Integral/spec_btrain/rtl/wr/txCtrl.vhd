----------------------------------------------------------------------------------
-- Company:       CERN TE/MSCC/MM
-- Engineer:      Daniel Oberson
-- 
-- Create Date:    14:05:26 13/01/2014
-- Design Name: 
-- Module Name:    txCtrl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.utils_pkg.all;
use work.wr_btrain_pkg.all;
use work.wrcore_pkg.all;
use work.wr_fabric_pkg.all;
use work.wr_xilinx_pkg.all;
use work.wishbone_pkg.all;
use work.streamers_pkg.all;

entity txCtrl is
	generic (
		g_data_width : integer:=16
	);
	port(
		clk_i    : in std_logic;
		reset_i  : in std_logic;

		-- Input frame for BtrainWR
		tx_B_frame_i   : in btrainFrameValue;
		tx_I_frame_i   : in ImFrameValue;
		tx_sync_i      : in std_logic;
		tx_sent_o      : out std_logic;
		force_send_i   : in std_logic;
		send_cnt_i     : in std_logic;
		simeff_bit_i   : in std_logic;
		error_bit_i    : in std_logic;
		d_low_marker_i : in std_logic;
		f_low_marker_i : in std_logic;
		C0_i           : in std_logic;
		zero_cycle_i   : in std_logic;
		frame_ID_i	   : in std_logic_vector(7 downto 0);
		
		-- Data word to be sent.
		tx_data_o  : out std_logic_vector(g_data_width-1 downto 0);
		-- 1 indicates that the tx_data_o contains a valid data word.
		tx_valid_o : out std_logic;
		-- Synchronous data request: if active, the user may send a data word in
		-- the following clock cycle.
		tx_dreq_i  : in std_logic;
		-- Used to indicate the last data word in a larger block of samples.
		tx_last_o  : out std_logic := '0';
		-- Flush command. When asserted, the streamer will immediatly send out all
		-- the data that is stored in its TX buffer, ignoring g_tx_timeout.
		tx_flush_o : out std_logic := '0'	
	);
end txCtrl;

architecture Behavioral of txCtrl is

	--Type
	type tx_btrain_state_t is (
			IDLE,WAIT_DREQ,
			START_SENDING,SENDING,FLUSH
		);

	--Signal
	signal state_s  : tx_btrain_state_t;
	signal n_word_s : integer;

	signal force_send_s : std_logic;
	signal B_frame_s      : std_logic_vector(127 downto 0);
	signal I_frame_s      : std_logic_vector(31 downto 0);
	
	signal tx_dreq_s : std_logic;
	
	signal tx_sync_cmd_s : std_logic;
	signal force_send_s_cmd_s : std_logic;
	
	-- counter to test WR-Link
	constant c_NUM_COUNTER_BITS : integer := 24;
	constant c_MAX_COUNTER : unsigned(c_NUM_COUNTER_BITS-1 downto 0) := x"ffffff";
	signal counter_s : unsigned(c_NUM_COUNTER_BITS-1 downto 0);
	
begin

	process(clk_i, reset_i)
	begin
		if (rising_edge(clk_i)) then
			if (reset_i='1') then  				--reset signals
				tx_sync_cmd_s <= '0';
				force_send_s_cmd_s <= '0';
				tx_data_o  <= (others => '0');
				tx_valid_o <= '0';
				tx_flush_o <= '0';
				n_word_s	  <= 0;
				tx_dreq_s  <= '0';
				state_s    <= IDLE;
				
			else
				tx_dreq_s <= tx_dreq_i;
				case state_s is
					when IDLE =>								
						tx_flush_o <= '0';
						if (tx_sync_i='1') then
							tx_sync_cmd_s <= '1';
						end if;
						if (force_send_s='1') then
							force_send_s_cmd_s <= '1';
						end if;
						if ((tx_sync_i='1') or (force_send_s='1') or 
								(tx_sync_cmd_s='1') or (force_send_s_cmd_s='1')) then           
							if (tx_dreq_s='1')then      -- if tx streamer ready to send data
								state_s	<= START_SENDING;       
							else                     
								state_s	<= WAIT_DREQ;     -- else waiting for tx streamer ready
							end if;	
						else    					
								state_s 	<= IDLE;
						end if;
				
					when WAIT_DREQ =>	            
						tx_valid_o <= '0';		
						if (tx_dreq_s='1')then				
							state_s 	<= START_SENDING;    -- send data word in next cycle
						else
							state_s 	<= WAIT_DREQ;        -- waiting streamer ready
						end if;

					when START_SENDING =>              -- send frame id    				
						tx_sync_cmd_s <= '0';
						force_send_s_cmd_s <= '0';
						tx_data_o  <= "00" & d_low_marker_i & f_low_marker_i & zero_cycle_i & C0_i & error_bit_i & not(simeff_bit_i) & frame_ID_i;   -- (see wr_btrain_pkg.vhd)) 
						tx_valid_o <= '1';          -- data valid
						n_word_s   <= 0;
						if (tx_dreq_s='1')then
							state_s 	<= SENDING; 			
						else
							state_s 	<= WAIT_DREQ;       -- waiting dreq
						end if;	

					when SENDING =>                    -- sending frame 		
						if (frame_ID_i=ID_BkFrame) then
							tx_data_o <= B_frame_s(B_frame_s'length-n_word_s*g_data_width-1 downto B_frame_s'length-(n_word_s+1)*g_data_width);
						else
							tx_data_o <= I_frame_s(I_frame_s'length-n_word_s*g_data_width-1 downto I_frame_s'length-(n_word_s+1)*g_data_width);
						end if;
						tx_valid_o <= '1';
						n_word_s   <= n_word_s + 1;
						if (tx_dreq_s='1') then
							if (frame_ID_i=ID_BkFrame) then
								if (n_word_s<(B_frame_s'length/g_data_width-1)) then
									state_s 	<= SENDING;      -- ready to send data
								else
									state_s 	<= FLUSH;
								end if;
							else
								if (n_word_s<(I_frame_s'length/g_data_width-1)) then
									state_s 	<= SENDING;      -- ready to send data
								else
									state_s 	<= FLUSH;
								end if;
							end if;
						else
							state_s 	<= WAIT_DREQ;       -- waiting dreq
						end if;									
					
					when FLUSH=>                       -- flush buffer
						tx_valid_o <='0';		
						tx_flush_o <='1';
						state_s    <= IDLE;  
						
				end case; 
			end if;				
		end if;  
	end process;

	-- Selection of sending value for check
	process (send_cnt_i,tx_B_frame_i,tx_I_frame_i,counter_s)
	begin
		if (send_cnt_i='0') then
			B_frame_s <= to_std_logic_vector(tx_B_frame_i);
			I_frame_s <= tx_I_frame_i.I;
		else
			B_frame_s <= x"10" & std_logic_vector(counter_s) & x"20" & std_logic_vector(counter_s) & x"30" & std_logic_vector(counter_s) & x"40" & std_logic_vector(counter_s);
			I_frame_s <= x"10" & std_logic_vector(counter_s);
		end if;
	end process;

	-- Counter to test WR-Link
	pr_cnt : process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				counter_s <= (others=>'0');
				tx_sent_o <= '0';
			else
				if ((tx_sync_i='1')or(force_send_s='1')) then
					if (counter_s < c_MAX_COUNTER) then
						counter_s <= counter_s + 1;
					else
						counter_s <= (others=>'0');
					end if;
					tx_sent_o <= '1';
				else
					tx_sent_o <= '0';
				end if;
			end if;
		end if;
	end process;

	cmp_ForceSend : RisingDetect 
		port map(
			clk_i		=> clk_i,
			reset_i	=> reset_i,
			
			s_i	=> force_send_i,
			s_o	=> force_send_s
		);
		
end Behavioral;

