target = "xilinx"
action = "synthesis"

syn_device = "xc6slx100t"
syn_grade = "-3"
syn_package = "fgg484"
syn_top = "spec_top_fmc_adc2M18b2ch"
#syn_tool = "ise"
syn_project = "spec_top_fmc_adc2M18b2ch.xise"

files = ["../spec_top_fmc_adc2M18b2ch.ucf"]

modules = { "local" : "../rtl" }
