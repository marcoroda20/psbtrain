----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:00:02 06/12/2013 
-- Design Name: 
-- Module Name:    TB_OverRange - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: To run the testbench with Modelsim type the command:
--                      do run_calc_overrange.do
--
--                      To generate reference file, use 
--                      ../../MatlabUtil/Generate_overr.m 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.std_logic_unsigned.all; 

use std.textio.all;

use work.Config_BTrain.all;

entity TB_OverRange is
end TB_OverRange;

architecture Behavioral of TB_OverRange is

	--Component
	component ADOverRange is
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			nb_overrange_i		: in std_logic_vector(15 downto 0);
			max_adc_overr_i	: in std_logic_vector(17 downto 0);
			clear_overrange_i	: in std_logic;
			
			adc_data_rdy_i	: in std_logic;
			adc_data_i		: in std_logic_vector(17 downto 0);
			
			overrange_o		: out std_logic
		);
	end component;

   --Inputs
   signal clk_sti 		: std_logic := '0';
   signal reset_sti 		: std_logic := '0';

   signal nb_overrange_sti		: std_logic_vector(15 downto 0);
   signal max_adc_overr_sti	: std_logic_vector(17 downto 0);
	signal clear_overrange_sti	: std_logic;
	
   signal adc_data_rdy_sti	: std_logic;
   signal adc_data_sti		: std_logic_vector(17 downto 0);
			
 	--Outputs
   signal overrange_obs		: std_logic;

   -- Clock period definitions
   constant clk_i_period : time := 10 ns;
	
begin

	uut_overrange : ADOverRange 
		port map(
			clk_i		=> clk_sti,
			reset_i	=> reset_sti,
			
			nb_overrange_i		=> nb_overrange_sti,
			max_adc_overr_i	=> max_adc_overr_sti,
			clear_overrange_i	=> clear_overrange_sti,
			
			adc_data_rdy_i	=> adc_data_rdy_sti,
			adc_data_i		=> adc_data_sti,
			
			overrange_o		=> overrange_obs
		);

   -- Clock process definitions
   clk_i_process : process
   begin
		clk_sti <= '0';
		wait for clk_i_period/2;
		clk_sti <= '1';
		wait for clk_i_period/2;
   end process;

   -- Stimulus process
   ADC_val_proc: process
	--To read values in file
	file coeff_f			: text open READ_MODE is "ADC_val.dat"; -- fichier des coefficients pour les stimuli
	variable coeff_line	: line;	
	variable coeff_v 		: integer;
	variable length_v		: integer;
	
	variable nb_adc_val_v	: integer;
	
   begin		
		--Initialisation de la simulation
		report ">>>>>> Start simulation";
		report ">>>>>>    Reset";
      reset_sti		<='1';
		
		adc_data_rdy_sti			<= '0';
		adc_data_sti				<= (others=>'0');
		clear_overrange_sti	<= '0';
		
		wait for 5*clk_i_period;

		--End reset
      reset_sti		<='0';
		wait for 5*clk_i_period;
		
		--ADC value
		readline(coeff_f,coeff_line);--first line header : 'Max Nb_val_mean Nb_val_signal'
		readline(coeff_f,coeff_line);--second line with value
		read(coeff_line,coeff_v);
		max_adc_overr_sti	<= conv_std_logic_vector(coeff_v,18);
		read(coeff_line,coeff_v);
		nb_overrange_sti	<= conv_std_logic_vector(coeff_v,16);
		read(coeff_line,coeff_v);
		nb_adc_val_v := coeff_v;
		wait for 5*clk_i_period;

		readline(coeff_f,coeff_line);--header : 'ADC value'
		for n in 0 to nb_adc_val_v-1 loop
			readline(coeff_f,coeff_line);--value
			read(coeff_line,coeff_v);
			adc_data_sti		<=conv_std_logic_vector(coeff_v,18);
			adc_data_rdy_sti	<= '1';
			wait for 3*clk_i_period;
			adc_data_rdy_sti	<= '0';
			wait for 10*clk_i_period;
		end loop;
		
		wait for 10*clk_i_period;
		clear_overrange_sti	<= '1';
		wait for clk_i_period;
		clear_overrange_sti	<= '0';

		wait;
		
	end process;
	
end Behavioral;

