--TB_divider testbench
--
-- Additional Comments: To run the testbench with Modelsim type the command:
--                      do run_divider.do
--

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;

  ENTITY TB_divider IS
  END TB_divider;

  ARCHITECTURE behavior OF TB_divider IS 

	-- Constant 
   constant clk_i_period : time := 10 ns;
	
	constant NB_BIT : integer := 24;
	
	-- Component Declaration
	component divider
		generic (NB_BIT : integer := 32);
		port(
			clk_i		: in std_logic;
			reset_i	: in std_logic;
			
			a_i	: in std_logic_vector(NB_BIT-1 downto 0);
			b_i	: in std_logic_vector(NB_BIT-1 downto 0);

			start_divider_i	: in std_logic;
			cmd_divider_i		: in std_logic;
			
			q_o	: out std_logic_vector(NB_BIT-1 downto 0);
			r_o	: out std_logic_vector(NB_BIT-1 downto 0);
			f_o	: out std_logic_vector(NB_BIT-1 downto 0);
			
			divide_by_0_o	: out std_logic;
			end_divider_o	: out std_logic
		);
	end component;

   --Inputs
	signal clk_sti 				: std_logic;
	signal reset_sti 				: std_logic;
	signal start_divider_sti	: std_logic;
	signal a_sti					: std_logic_vector(NB_BIT-1 downto 0);
	signal b_sti					: std_logic_vector(NB_BIT-1 downto 0);
	signal cmd_divider_sti		: std_logic := '0';
	
 	--Outputs
	signal q_obs				: std_logic_vector(NB_BIT-1 downto 0);
	signal r_obs				: std_logic_vector(NB_BIT-1 downto 0);
	signal f_obs				: std_logic_vector(NB_BIT-1 downto 0);
	signal divide_by_0_obs	: std_logic;
	signal end_divider_obs	: std_logic;

  BEGIN

	-- Component Instantiation
	uut : divider
		generic map(NB_BIT=>NB_BIT)
		port map(
			clk_i			=> clk_sti,
			reset_i		=> reset_sti,

			a_i	=> a_sti,
			b_i	=> b_sti,

			start_divider_i	=> start_divider_sti,
			cmd_divider_i		=> cmd_divider_sti,

			q_o	=> q_obs,
			r_o	=> r_obs,
			f_o	=> f_obs,

			divide_by_0_o	=> divide_by_0_obs,
			end_divider_o	=> end_divider_obs
		);

   -- Clock process definitions
   clk_i_process :process
   begin
		clk_sti <= '0';
		wait for clk_i_period/2;
		clk_sti <= '1';
		wait for clk_i_period/2;
   end process;

   -- Stimulus process
   stim_proc: process
	
   begin		
		--Initialisation de la simulation
		report ">>>>>> Start simulation";
		report ">>>>>>    Init stimulation and Reset";
      reset_sti			<= '1';
		start_divider_sti	<= '0';	
		a_sti					<= (others=>'0');
		b_sti					<= (others=>'0');
		
		wait for 8*clk_i_period;
		
		--End reset
      reset_sti	<='0';
		wait for 10*clk_i_period;
		report ">>>>>>    End init and reset";
		wait for 1*clk_i_period;
		wait for 1 ns;

		--Start division
		a_sti	<= x"800000";
		b_sti	<= x"300000";
		
		wait for 1*clk_i_period;
		start_divider_sti	<='1';
		wait for 1*clk_i_period;
		start_divider_sti	<='0';		

		wait for 1*clk_i_period;
		while (end_divider_obs='0') loop
			wait for 1 ns;
		end loop;
		
		--Start division
		a_sti	<= x"300000";
		b_sti	<= x"800000";
		
		wait for 1*clk_i_period;
		start_divider_sti	<='1';
		wait for 1*clk_i_period;
		start_divider_sti	<='0';		

		wait for 1*clk_i_period;
		while (end_divider_obs='0') loop
			wait for 1 ns;
		end loop;
		
		--Start division
		a_sti	<= x"300000";
		b_sti	<= x"300000";
		
		wait for 1*clk_i_period;
		start_divider_sti	<='1';
		wait for 1*clk_i_period;
		start_divider_sti	<='0';		

		wait for 1*clk_i_period;
		while (end_divider_obs='0') loop
			wait for 1 ns;
		end loop;
		
		--Start division
		a_sti	<= x"654321";
		b_sti	<= x"345678";
		
		wait for 1*clk_i_period;
		start_divider_sti	<='1';
		wait for 1*clk_i_period;
		start_divider_sti	<='0';		

		wait for 1*clk_i_period;
		while (end_divider_obs='0') loop
			wait for 1 ns;
		end loop;
		
		--Start division
		a_sti	<= x"654321";
		b_sti	<= x"000000";
		
		wait for 1*clk_i_period;
		start_divider_sti	<='1';
		wait for 1*clk_i_period;
		start_divider_sti	<='0';		

		wait for 1*clk_i_period;
		while (end_divider_obs='0') loop
			wait for 1 ns;
		end loop;
		
		--Start division
		a_sti	<= x"fffff9";
		b_sti	<= x"000002";
		
		wait for 1*clk_i_period;
		start_divider_sti	<='1';
		wait for 1*clk_i_period;
		start_divider_sti	<='0';		

		wait for 1*clk_i_period;
		while (end_divider_obs='0') loop
			wait for 1 ns;
		end loop;
		
		wait;
		
	end process;

  END;
