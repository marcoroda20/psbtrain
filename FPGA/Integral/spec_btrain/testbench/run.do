vlib work

#vcom -93 ../ip_cores/wr-cores/ip_cores/general-cores/modules/wishbone/wishbone_pkg.vhd
#vcom -93 ../ip_cores/wr-cores/modules/fabric/wr_fabric_pkg.vhd
#vcom -93 ../ip_cores/wr-cores/modules/wr_endpoint/endpoint_pkg.vhd
#vcom -93 ../ip_cores/wr-cores/modules/wr_softpll_ng/softpll_pkg.vhd

#vcom -93 ../ip_cores/general-cores/modules/genrams/genram_pkg.vhd
#vcom -93 ../ip_cores/general-cores/modules/common/gencores_pkg.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_crc_gen.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_moving_average.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_extend_pulse.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_delay_gen.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_dual_pi_controller.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_serial_dac.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_sync_ffs.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_arbitrated_mux.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_pulse_synchronizer.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_frequency_meter.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_dual_clock_ram.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_wfifo.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_rr_arbiter.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_prio_encoder.vhd
##vcom -93 ../ip_cores/general-cores/modules/common/gc_word_packer.vhd
#vcom -93 ../ip_cores/general-cores/modules/genrams/memory_loader_pkg.vhd
##vcom -93 ../ip_cores/general-cores/modules/genrams/generic_shiftreg_fifo.vhd
##vcom -93 ../ip_cores/general-cores/modules/genrams/inferred_sync_fifo.vhd
##vcom -93 ../ip_cores/general-cores/modules/genrams/inferred_async_fifo.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wishbone_pkg.vhd
##vcom -93 ../ip_cores/general-cores/modules/genrams/xilinx/generic_dpram.vhd
##vcom -93 ../ip_cores/general-cores/modules/genrams/xilinx/generic_dpram_sameclock.vhd
##vcom -93 ../ip_cores/general-cores/modules/genrams/xilinx/generic_dpram_dualclock.vhd
##vcom -93 ../ip_cores/general-cores/modules/genrams/xilinx/generic_spram.vhd
##vcom -93 ../ip_cores/general-cores/modules/genrams/xilinx/gc_shiftreg.vhd
##vcom -93 ../ip_cores/general-cores/modules/genrams/generic/generic_async_fifo.vhd
##vcom -93 ../ip_cores/general-cores/modules/genrams/generic/generic_sync_fifo.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_async_bridge/wb_async_bridge.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_async_bridge/xwb_async_bridge.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_onewire_master/wb_onewire_master.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_onewire_master/xwb_onewire_master.vhd
##vlog ../ip_cores/general-cores/modules/wishbone/wb_onewire_master/sockit_owm.v
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_i2c_master/i2c_master_bit_ctrl.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_i2c_master/i2c_master_byte_ctrl.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_i2c_master/i2c_master_top.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_i2c_master/wb_i2c_master.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_i2c_master/xwb_i2c_master.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_bus_fanout/xwb_bus_fanout.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_dpram/xwb_dpram.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_gpio_port/wb_gpio_port.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_gpio_port/xwb_gpio_port.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_simple_timer/wb_tics.vhd
##vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_simple_timer/xwb_tics.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_pkg.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/uart_async_rx.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/uart_async_tx.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/uart_baud_gen.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/simple_uart_pkg.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/simple_uart_wb.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/wb_simple_uart.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/xwb_simple_uart.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_vic/vic_prio_enc.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_vic/wb_slave_vic.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_vic/wb_vic.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_vic/xwb_vic.vhd
#vlog ../ip_cores/general-cores/modules/wishbone/wb_spi/spi_clgen.v +incdir+../ip_cores/general-cores/modules/wishbone/wb_spi/
#vlog ../ip_cores/general-cores/modules/wishbone/wb_spi/spi_shift.v +incdir+../ip_cores/general-cores/modules/wishbone/wb_spi/
#vlog ../ip_cores/general-cores/modules/wishbone/wb_spi/spi_top.v +incdir+../ip_cores/general-cores/modules/wishbone/wb_spi/
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_spi/wb_spi.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_spi/xwb_spi.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_crossbar/sdb_rom.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_crossbar/xwb_crossbar.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_crossbar/xwb_sdb_crossbar.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_lm32/generated/xwb_lm32.vhd
##vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/generated/lm32_allprofiles.v
#vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_mc_arithmetic.v +incdir+../ip_cores/general-cores/modules/wishbone/wb_lm32/src/
#vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/jtag_cores.v +incdir+../ip_cores/general-cores/modules/wishbone/wb_lm32/src/
#vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_adder.v +incdir+../ip_cores/general-cores/modules/wishbone/wb_lm32/src/
#vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_addsub.v +incdir+../ip_cores/general-cores/modules/wishbone/wb_lm32/src/
#vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_dp_ram.v +incdir+../ip_cores/general-cores/modules/wishbone/wb_lm32/src/
#vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_logic_op.v +incdir+../ip_cores/general-cores/modules/wishbone/wb_lm32/src/
#vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_ram.v +incdir+../ip_cores/general-cores/modules/wishbone/wb_lm32/src/
#vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_shifter.v +incdir+../ip_cores/general-cores/modules/wishbone/wb_lm32/src/
#vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/platform/spartan6/lm32_multiplier.v
#vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/platform/spartan6/jtag_tap.v
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_slave_adapter/wb_slave_adapter.vhd
#
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_xilinx_fpga_loader/xloader_registers_pkg.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_xilinx_fpga_loader/wb_xilinx_fpga_loader.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_xilinx_fpga_loader/xwb_xilinx_fpga_loader.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_xilinx_fpga_loader/xloader_wb.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_clock_crossing/xwb_clock_crossing.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_dma/xwb_dma.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_dpssram.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_eic.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_fifo_async.vhd
#vcom -93 ../ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_fifo_sync.vhd
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/rtl/ddr3_ctrl_pkg.vhd
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/rtl/ddr3_ctrl_wrapper_pkg.vhd
#vcom -93 ../ip_cores/ddr3-sp6-core/hdl/rtl/ddr3_ctrl.vhd
#vcom -93 ../ip_cores/ddr3-sp6-core/hdl/rtl/ddr3_ctrl_wb.vhd
#vcom -93 ../ip_cores/ddr3-sp6-core/hdl/rtl/ddr3_ctrl_wrapper.vhd
#vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/ddr3_ctrl_spec_bank3_64b_32b.vhd
#vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/memc3_infrastructure.vhd
#vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/memc3_wrapper.vhd
#vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/iodrp_controller.vhd
#vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/iodrp_mcb_controller.vhd
#vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/mcb_raw_wrapper.vhd
#vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/mcb_soft_calibration_top.vhd
#vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/mcb_soft_calibration.vhd
vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/spartan6/gn4124_core_pkg.vhd
vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/spartan6/gn4124_core.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/dma_controller.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/dma_controller_wb_slave.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/l2p_arbiter.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/l2p_dma_master.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/p2l_decode32.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/p2l_dma_master.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/wbmaster32.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/spartan6/l2p_ser.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/spartan6/p2l_des.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/spartan6/serdes_1_to_n_clk_pll_s2_diff.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/spartan6/serdes_1_to_n_data_s2_se.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/spartan6/serdes_n_to_1_s2_diff.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/spartan6/serdes_n_to_1_s2_se.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/spartan6/pulse_sync_rtl.vhd
#vcom -93 ../ip_cores/etherbone-core/hdl/eb_slave_core/vhdl_2008_workaround_pkg.vhd
#vcom -93 ../ip_cores/etherbone-core/hdl/eb_slave_core/piso_flag.vhd
#vcom -93 ../ip_cores/etherbone-core/hdl/eb_slave_core/sipo_flag.vhd
#vcom -93 ../ip_cores/etherbone-core/hdl/eb_slave_core/WB_bus_adapter_streaming_sg.vhd
#vcom -93 ../ip_cores/gn4124-core/hdl/common/rtl/wb_addr_decoder.vhd

#vcom -93 ../ip_cores/wr-cores/modules/wrc_core/wrcore_pkg.vhd


vcom -93 ../rtl/utils/utils_pkg.vhd
vcom -93 ../rtl/btrain_pkg.vhd
vcom -93 ../rtl/calib/calib_pkg.vhd
vcom -93 ../rtl/integral/integral_pkg.vhd
vcom -93 ../rtl/adc_dac/adc_dac_pkg.vhd
vcom -93 ../rtl/dma/dma_btrain_pkg.vhd
vcom -93 ../rtl/wr/wr_btrain_pkg.vhd

vlog ../rtl/streamers/ht-corelib-spartan6.v
vcom -93 ../rtl/streamers/ht-corelib-spartan6.vhd

vcom -93 ../rtl/dma/acqu_ddr_ctrl.vhd
vcom -93 ../rtl/dma/decimator.vhd

vcom -93 ../rtl/sdb/sdb_meta_pkg.vhd

#vcom -93 ../rtl/streamers/streamers_pkg.vhd
#vcom -93 ../rtl/streamers/xrx_streamer.vhd
#vcom -93 ../rtl/streamers/xtx_streamer.vhd

vcom -93 ../rtl/streamers16bit/streamers_pkg.vhd
vcom -93 ../rtl/streamers16bit/dropping_buffer.vhd
vcom -93 ../rtl/streamers16bit/gc_escape_detector.vhd
vcom -93 ../rtl/streamers16bit/gc_escape_inserter.vhd
vcom -93 ../rtl/streamers16bit/xrx_streamer.vhd
vcom -93 ../rtl/streamers16bit/xtx_streamer.vhd

vcom -93 ../rtl/wbgen/carrier_csr.vhd
vcom -93 ../rtl/wbgen/irq_controller_regs.vhd
vcom -93 ../rtl/wbgen/BTrainReg.vhd

vcom -93 ../rtl/timestamp_adder.vhd
vcom -93 ../rtl/irq_controller.vhd
vcom -93 ../rtl/testreg.vhd
vcom -93 ../rtl/BTrain.vhd

vcom -93 ../rtl/adc_dac/AD5791.vhd
vcom -93 ../rtl/adc_dac/AD7986.vhd
vcom -93 ../rtl/adc_dac/acquisition_core.vhd
vcom -93 ../rtl/adc_dac/ADOverRange.vhd
vcom -93 ../rtl/adc_dac/BDOT_DAC.vhd
vcom -93 ../rtl/adc_dac/mux_val.vhd
vcom -93 ../rtl/adc_dac/switch_dac.vhd
vcom -93 ../rtl/adc_dac/Acquisition_AD7986_corr.vhd
vcom -93 ../rtl/adc_dac/correction.vhd
vcom -93 ../rtl/adc_dac/mux_dac_val.vhd

vcom -93 ../rtl/calib/btrain_calib.vhd
vcom -93 ../rtl/calib/calib_cmd_dac.vhd
vcom -93 ../rtl/calib/calibration.vhd
vcom -93 ../rtl/calib/ctrl_calib.vhd
vcom -93 ../rtl/calib/gain_offset_calc.vhd
vcom -93 ../rtl/calib/offset_calc.vhd
vcom -93 ../rtl/calib/switch_ctrl.vhd
vcom -93 ../rtl/calib/mean_measurement.vhd

vcom -93 ../rtl/lcd/lcd_pkg.vhd
vcom -93 ../rtl/lcd/LCD4x40_Interface.vhd

vcom -93 ../rtl/integral/B_FD_Sum.vhd
vcom -93 ../rtl/integral/BCalcul3_8.vhd
vcom -93 ../rtl/integral/BTrain_integral.vhd

#vcom -93 ../rtl/wr/Config_btrainWR.vhd
#vcom -93 ../rtl/wr/ctrl_sender.vhd
vcom -93 ../rtl/wr/txCtrl.vhd
vcom -93 ../rtl/wr/rxCtrl.vhd
vcom -93 ../rtl/wr/SynchroGen.vhd
vcom -93 ../rtl/wr/WRBTrain.vhd

vcom -93 ../rtl/utils/BCD_counter.vhd
vcom -93 ../rtl/utils/bin2bcd.vhd
vcom -93 ../rtl/utils/clock_divider.vhd
vcom -93 ../rtl/utils/RisingDetect.vhd
vcom -93 ../rtl/utils/wb_addr_decoder.vhd
vcom -93 ../rtl/utils/CLK_DIV_EVEN.vhd
vcom -93 ../rtl/utils/Mult36x36_signed.vhd
vcom -93 ../rtl/utils/Mult32x32_2chan.vhd
vcom -93 ../rtl/utils/divider.vhd
vcom -93 ../rtl/utils/spec_reset_gen.vhd
vcom -93 ../rtl/utils/mean_pow2.vhd
vcom -93 ../rtl/utils/losses_counter.vhd

vcom -93 ../rtl/spec_btrain.vhd

vlog -sv main.sv 

vsim -L unisim -L secureip work.main -voptargs="+acc" -suppress 8684,8683
set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave_wr.do
run 300us
wave zoomfull
radix -hex
