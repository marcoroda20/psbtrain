vlib work

vcom -93 ../rtl/fmc_adc2M18b2ch_pkg.vhd 
vcom -93 ../rtl/calib/calib_pkg.vhd 
vcom -93 ../rtl/integral/integral_pkg.vhd 
vcom -93 ../rtl/adc_dac/adc_dac_pkg.vhd
vcom -93 ../rtl/streamers16bit/streamers_pkg.vhd 
vcom -93 ../rtl/wr/wr_btrain_pkg.vhd 
vcom -93 ../rtl/lcd/lcd_pkg.vhd 
vcom -93 ../rtl/dma/dma_pkg.vhd 
vcom -93 ../rtl/utils/utils_pkg.vhd 
vcom -93 ../rtl/sdb/sdb_meta_pkg.vhd 

vcom -93 ../rtl/utils/RisingDetect.vhd
vcom -93 ../rtl/utils/Mult32x32_2chan.vhd
vcom -93 ../rtl/utils/Mult36x36_signed.vhd
vcom -93 ../rtl/integral/B_FD_Sum.vhd

vcom -93 TB_B_FD_sum.vhd 

vsim -L unisim work.TB_B_FD_sum -voptargs="+acc"

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave_FD_sum.do

run 100us
wave zoomfull
radix -hex
