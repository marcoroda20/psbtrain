vlib work
      
vcom -93 ../ip_cores/general-cores/modules/genrams/genram_pkg.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gencores_pkg.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_crc_gen.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_moving_average.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_extend_pulse.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_delay_gen.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_dual_pi_controller.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_reset.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_serial_dac.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_sync_ffs.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_arbitrated_mux.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_pulse_synchronizer.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_frequency_meter.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_rr_arbiter.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_prio_encoder.vhd 
vcom -93 ../ip_cores/general-cores/modules/common/gc_word_packer.vhd 
vcom -93 ../ip_cores/general-cores/modules/genrams/memory_loader_pkg.vhd 
vcom -93 ../ip_cores/general-cores/modules/genrams/generic_shiftreg_fifo.vhd 
vcom -93 ../ip_cores/general-cores/modules/genrams/inferred_sync_fifo.vhd 
vcom -93 ../ip_cores/general-cores/modules/genrams/inferred_async_fifo.vhd 

vcom -93 ../ip_cores/general-cores/modules/wishbone/wishbone_pkg.vhd 
vcom -93 ../ip_cores/general-cores/modules/genrams/xilinx/generic_dpram.vhd 
vcom -93 ../ip_cores/general-cores/modules/genrams/xilinx/generic_dpram_sameclock.vhd 
vcom -93 ../ip_cores/general-cores/modules/genrams/xilinx/generic_dpram_dualclock.vhd 
vcom -93 ../ip_cores/general-cores/modules/genrams/xilinx/generic_simple_dpram.vhd 
vcom -93 ../ip_cores/general-cores/modules/genrams/xilinx/generic_spram.vhd 
vcom -93 ../ip_cores/general-cores/modules/genrams/xilinx/gc_shiftreg.vhd 
vcom -93 ../ip_cores/general-cores/modules/genrams/generic/generic_async_fifo.vhd 
vcom -93 ../ip_cores/general-cores/modules/genrams/generic/generic_sync_fifo.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_async_bridge/wb_async_bridge.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_async_bridge/xwb_async_bridge.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_onewire_master/wb_onewire_master.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_onewire_master/xwb_onewire_master.vhd 
vlog ../ip_cores/general-cores/modules/wishbone/wb_onewire_master/sockit_owm.v 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_i2c_master/i2c_master_bit_ctrl.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_i2c_master/i2c_master_byte_ctrl.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_i2c_master/i2c_master_top.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_i2c_master/wb_i2c_master.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_i2c_master/xwb_i2c_master.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_bus_fanout/xwb_bus_fanout.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_dpram/xwb_dpram.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_gpio_port/wb_gpio_port.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_gpio_port/xwb_gpio_port.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_simple_timer/wb_tics.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_simple_timer/xwb_tics.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/uart_async_rx.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/uart_async_tx.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/uart_baud_gen.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/simple_uart_wb.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/simple_uart_pkg.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/wb_simple_uart.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_uart/xwb_simple_uart.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_vic/vic_prio_enc.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_vic/wb_slave_vic.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_vic/wb_vic.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_vic/xwb_vic.vhd 
vlog ../ip_cores/general-cores/modules/wishbone/wb_spi/spi_defines.v 
vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_include.v 
vlog ../ip_cores/general-cores/modules/wishbone/wb_spi/timescale.v 
vlog ../ip_cores/general-cores/modules/wishbone/wb_spi/spi_clgen.v +incdir+/home/doberson/BTrain/psbtrain/FPGA/Integral/spec_btrain/ip_cores/general-cores/modules/wishbone/wb_spi
vlog ../ip_cores/general-cores/modules/wishbone/wb_spi/spi_shift.v +incdir+/home/doberson/BTrain/psbtrain/FPGA/Integral/spec_btrain/ip_cores/general-cores/modules/wishbone/wb_spi
vlog ../ip_cores/general-cores/modules/wishbone/wb_spi/spi_top.v +incdir+/home/doberson/BTrain/psbtrain/FPGA/Integral/spec_btrain/ip_cores/general-cores/modules/wishbone/wb_spi
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_spi/wb_spi.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_spi/xwb_spi.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_crossbar/sdb_rom.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_crossbar/xwb_crossbar.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_crossbar/xwb_sdb_crossbar.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_irq/wb_irq_pkg.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_irq/wb_irq_lm32.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_irq/wb_irq_slave.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_irq/wb_irq_master.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_lm32/generated/xwb_lm32.vhd 
vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/generated/lm32_allprofiles.v 
vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_mc_arithmetic.v +incdir+/home/doberson/BTrain/psbtrain/FPGA/Integral/spec_btrain/ip_cores/general-cores/modules/wishbone/wb_lm32/src
vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/jtag_cores.v 
vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_adder.v +incdir+/home/doberson/BTrain/psbtrain/FPGA/Integral/spec_btrain/ip_cores/general-cores/modules/wishbone/wb_lm32/src
vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_addsub.v +incdir+/home/doberson/BTrain/psbtrain/FPGA/Integral/spec_btrain/ip_cores/general-cores/modules/wishbone/wb_lm32/src
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_dp_ram.vhd 
vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_logic_op.v +incdir+/home/doberson/BTrain/psbtrain/FPGA/Integral/spec_btrain/ip_cores/general-cores/modules/wishbone/wb_lm32/src
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_ram.vhd 
vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/src/lm32_shifter.v +incdir+/home/doberson/BTrain/psbtrain/FPGA/Integral/spec_btrain/ip_cores/general-cores/modules/wishbone/wb_lm32/src
vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/platform/spartan6/lm32_multiplier.v 
vlog ../ip_cores/general-cores/modules/wishbone/wb_lm32/platform/spartan6/jtag_tap.v 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_slave_adapter/wb_slave_adapter.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_clock_crossing/xwb_clock_crossing.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_dma/xwb_dma.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_dma/xwb_streamer.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_serial_lcd/wb_serial_lcd.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_spi_flash/wb_spi_flash.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_pkg.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_simple_pwm/simple_pwm_wbgen2_pkg.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_simple_pwm/simple_pwm_wb.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_simple_pwm/wb_simple_pwm.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_simple_pwm/xwb_simple_pwm.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_dpssram.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_eic.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_fifo_async.vhd 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_fifo_sync.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/rtl/ddr3_ctrl.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/rtl/ddr3_ctrl_wb.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/rtl/ddr3_ctrl_wrapper.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/rtl/ddr3_ctrl_wrapper_pkg.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/rtl/ddr3_ctrl_pkg.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/ddr3_ctrl_spec_bank3_64b_32b.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/memc3_infrastructure.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/memc3_wrapper.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/iodrp_controller.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/iodrp_mcb_controller.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/mcb_raw_wrapper.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/mcb_soft_calibration_top.vhd 
vcom -93 ../ip_cores/ddr3-sp6-core/hdl/spec/ip_cores/ddr3_ctrl_spec_bank3_64b_32b/user_design/rtl/mcb_soft_calibration.vhd 
vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/dma_controller.vhd 
vcom -93 ../ip_cores/gn4124-core/hdl/gn4124core/rtl/dma_controller_wb_slave.vhd 
 
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_crossbar/sdb_rom.vhd
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_crossbar/xwb_crossbar.vhd
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_crossbar/xwb_sdb_crossbar.vhd
vcom -93 ../ip_cores/general-cores/modules/wishbone/wb_crossbar/xwb_register_link.vhd
 
   
vcom -93 ../rtl/sdb/sdb_meta_pkg.vhd   
vcom -93 ../rtl/fmc_adc2M18b2ch_pkg.vhd
vcom -93 ../rtl/lcd/lcd_pkg.vhd
vcom -93 ../rtl/lcd/LCD4x40_Interface.vhd

#Integral
vcom -93 ../rtl/sdb/sdb_meta_pkg.vhd
vcom -93 ../rtl/integral/integral_pkg.vhd
vcom -93 ../rtl/integral/Bdot.vhd
vcom -93 ../rtl/integral/BCalcul3_8.vhd
vcom -93 ../rtl/integral/BCalculRect.vhd
vcom -93 ../rtl/integral/BTrain_integral.vhd
vcom -93 ../rtl/integral/B_FD_Sum.vhd
vcom -93 ../rtl/integral/mux_integral.vhd
#ADC (AD7986) and DAC (AD5791)
vcom -93 ../rtl/adc_dac/adc_dac_pkg.vhd
vcom -93 ../rtl/adc_dac/AD5791.vhd
vcom -93 ../rtl/adc_dac/AD7986.vhd
vcom -93 ../rtl/adc_dac/Acquisition_AD7986_corr.vhd
vcom -93 ../rtl/adc_dac/correction.vhd
vcom -93 ../rtl/adc_dac/acquisition_core.vhd
vcom -93 ../rtl/adc_dac/switch_dac.vhd
vcom -93 ../rtl/adc_dac/ADOverRange.vhd
vcom -93 ../rtl/adc_dac/mux_dac_val.vhd
vcom -93 ../rtl/adc_dac/BDOT_DAC.vhd
#Wishbone register
vcom -93 ../rtl/wbgen/fmc_adc2M18b2ch_reg.vhd
vcom -93 ../rtl/wbgen/carrier_csr.vhd
vcom -93 ../rtl/wbgen/irq_controller_regs.vhd
#DMA
vcom -93 ../rtl/dma/dma_pkg.vhd
vcom -93 ../rtl/dma/acqu_ddr_ctrl.vhd
vcom -93 ../rtl/dma/decimator.vhd
#Utils
vcom -93 ../rtl/utils/utils_pkg.vhd
vcom -93 ../rtl/utils/RisingDetect.vhd
vcom -93 ../rtl/utils/CLK_DIV_EVEN.vhd
vcom -93 ../rtl/utils/Mult36x36_signed.vhd
vcom -93 ../rtl/utils/Mult32x32_2chan.vhd
vcom -93 ../rtl/utils/divider.vhd
vcom -93 ../rtl/utils/BCD_counter.vhd
vcom -93 ../rtl/utils/clock_divider.vhd
vcom -93 ../rtl/utils/C0_time.vhd
vcom -93 ../rtl/utils/mean_pow2.vhd
#Calib
vcom -93 ../rtl/calib/calib_pkg.vhd
vcom -93 ../rtl/calib/calib.vhd
vcom -93 ../rtl/calib/calib_cmd_dac.vhd
vcom -93 ../rtl/calib/calibration.vhd
vcom -93 ../rtl/calib/ctrl_calib.vhd
vcom -93 ../rtl/calib/gain_offset_calc.vhd
vcom -93 ../rtl/calib/offset_calc.vhd
vcom -93 ../rtl/calib/switch_ctrl.vhd
vcom -93 ../rtl/calib/mean_measurement.vhd

vcom -93 ../rtl/fmc_adc2M18b2ch.vhd

vcom -93 TB_BTrain.vhd 

vsim work.TB_BTrain 

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave_btrain.do

run 150us
wave zoomfull
radix -hex
