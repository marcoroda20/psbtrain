vlib work

#Integral
vcom -93 ../rtl/integral/integral_pkg.vhd
vcom -93 ../rtl/integral/Bdot.vhd
vcom -93 ../rtl/integral/BCalcul3_8.vhd
vcom -93 ../rtl/integral/BCalculRect.vhd
vcom -93 ../rtl/integral/BTrain_integral.vhd
vcom -93 ../rtl/integral/B_FD_Sum.vhd
vcom -93 ../rtl/integral/mux_integral.vhd


vcom -93 TB_BTrain_integral.vhd 

vsim work.TB_BTrain_integral 

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave_integral.do

run 1ms
wave zoomfull
radix -hex
