vlib work

vcom -93 ../rtl/Config_BTrain.vhd

vcom -93 ../rtl/utils/RisingDetect.vhd
vcom -93 ../rtl/utils/Mult36x36_signed.vhd
vcom -93 ../rtl/integral/BCalcul3_8.vhd

vcom -93 TB_BCalcul3_8.vhd 

vsim -L unisim work.TB_BCalcul3_8 -voptargs="+acc"

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave_calc_3_8.do

run 200us
wave zoomfull
radix -hex
