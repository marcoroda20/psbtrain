vlib work

vcom -93 ../rtl/fmc_adc2M18b2ch_pkg.vhd 
vcom -93 ../rtl/calib/calib_pkg.vhd 
vcom -93 ../rtl/integral/integral_pkg.vhd 
vcom -93 ../rtl/adc_dac/adc_dac_pkg.vhd
vcom -93 ../rtl/streamers16bit/streamers_pkg.vhd 
vcom -93 ../rtl/wr/wr_btrain_pkg.vhd 
vcom -93 ../rtl/lcd/lcd_pkg.vhd 
vcom -93 ../rtl/dma/dma_pkg.vhd 
vcom -93 ../rtl/utils/utils_pkg.vhd 
vcom -93 ../rtl/sdb/sdb_meta_pkg.vhd 
#vcom -93 ../rtl/spec_top_fmc_adc2M18b2ch.vhd 
#vcom -93 ../rtl/irq_controller.vhd 
vcom -93 ../rtl/fmc_adc2M18b2ch.vhd 
#vcom -93 ../rtl/testreg.vhd 
#vcom -93 ../rtl/timestamp_adder.vhd
#vcom -93 ../rtl/calib/calib.vhd 
#vcom -93 ../rtl/calib/calib_cmd_dac.vhd 
#vcom -93 ../rtl/calib/calibration.vhd 
#vcom -93 ../rtl/calib/ctrl_calib.vhd 
#vcom -93 ../rtl/calib/gain_offset_calc.vhd 
#vcom -93 ../rtl/calib/offset_calc.vhd 
#vcom -93 ../rtl/calib/switch_ctrl.vhd 
#vcom -93 ../rtl/calib/mean_measurement.vhd 
vcom -93 ../rtl/integral/BCalcul3_8.vhd 
vcom -93 ../rtl/integral/BCalculRect.vhd 
vcom -93 ../rtl/integral/BTrain_integral.vhd 
vcom -93 ../rtl/integral/B_FD_Sum.vhd 
vcom -93 ../rtl/integral/mux_integral.vhd 
vcom -93 ../rtl/integral/Bdot.vhd 
#vcom -93 ../rtl/adc_dac/AD5791.vhd 
#vcom -93 ../rtl/adc_dac/AD7986.vhd 
#vcom -93 ../rtl/adc_dac/acquisition_core.vhd 
#vcom -93 ../rtl/adc_dac/ADOverRange.vhd 
#vcom -93 ../rtl/adc_dac/BDOT_DAC.vhd 
#vcom -93 ../rtl/adc_dac/switch_dac.vhd 
#vcom -93 ../rtl/adc_dac/Acquisition_AD7986_corr.vhd 
#vcom -93 ../rtl/adc_dac/correction.vhd 
#vcom -93 ../rtl/adc_dac/mux_dac_val.vhd 
#vcom -93 ../rtl/streamers16bit/dropping_buffer.vhd 
#vcom -93 ../rtl/streamers16bit/gc_escape_detector.vhd 
#vcom -93 ../rtl/streamers16bit/gc_escape_inserter.vhd 
#vcom -93 ../rtl/streamers16bit/xrx_streamer.vhd 
#vcom -93 ../rtl/streamers16bit/xtx_streamer.vhd 
#vcom -93 ../rtl/wr/txCtrl.vhd 
#vcom -93 ../rtl/wr/rxCtrl.vhd 
#vcom -93 ../rtl/wr/SynchroGen.vhd 
#vcom -93 ../rtl/wr/WRBTrain.vhd 
#vcom -93 ../rtl/lcd/LCD4x40_Interface.vhd 
#vcom -93 ../rtl/wbgen/fmc_adc2M18b2ch_reg.vhd 
#vcom -93 ../rtl/wbgen/carrier_csr.vhd 
#vcom -93 ../rtl/wbgen/irq_controller_regs.vhd 
#vcom -93 ../rtl/dma/acqu_ddr_ctrl.vhd 
#vcom -93 ../rtl/dma/decimator.vhd 
#vcom -93 ../rtl/utils/delayLine.vhd 
#vcom -93 ../rtl/utils/BCD_counter.vhd 
#vcom -93 ../rtl/utils/bin2bcd.vhd 
#vcom -93 ../rtl/utils/clock_divider.vhd 
vcom -93 ../rtl/utils/RisingDetect.vhd 
vcom -93 ../rtl/utils/monostable.vhd 
#vcom -93 ../rtl/utils/wb_addr_decoder.vhd 
#vcom -93 ../rtl/utils/CLK_DIV_EVEN.vhd 
vcom -93 ../rtl/utils/Mult36x36_signed.vhd 
#vcom -93 ../rtl/utils/Mult32x32_2chan.vhd 
#vcom -93 ../rtl/utils/divider.vhd 
#vcom -93 ../rtl/utils/spec_reset_gen.vhd 
#vcom -93 ../rtl/utils/mean_pow2.vhd 
#vcom -93 ../rtl/utils/losses_counter.vhd 
#vcom -93 ../rtl/utils/C0_time.vhd 

vcom -93 TB_BCalculWithoutTiming.vhd 

vsim work.TB_BCalculWithoutTiming 

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave_calc_wot.do

run 100us
wave zoomfull
radix -hex
