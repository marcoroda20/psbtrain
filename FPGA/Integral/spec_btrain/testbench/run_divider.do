vlib work

vcom -93 ../rtl/utils/RisingDetect.vhd
vcom -93 ../rtl/utils/divider.vhd

vcom -93 TB_divider.vhd 

vsim -L unisim work.TB_divider -voptargs="+acc"

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave_divider.do

run 4us
wave zoomfull
radix -hex

