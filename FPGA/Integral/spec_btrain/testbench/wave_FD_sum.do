onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_b_fd_sum/uut/clk_i
add wave -noupdate /tb_b_fd_sum/uut/reset_i
add wave -noupdate -divider Input
add wave -noupdate -radix hexadecimal /tb_b_fd_sum/uut/reg_f_b_coeff_i
add wave -noupdate -radix hexadecimal /tb_b_fd_sum/uut/reg_d_b_coeff_i
add wave -noupdate -radix hexadecimal /tb_b_fd_sum/uut/reg_kf_bdot_coeff_i
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/Bk_f_i
add wave -noupdate /tb_b_fd_sum/uut/B_f_data_rdy_i
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/Bk_d_i
add wave -noupdate /tb_b_fd_sum/uut/B_d_data_rdy_i
add wave -noupdate -divider {B_FD and Bdot calc}
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/reg_f_b_coeff_i
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/reg_d_b_coeff_i
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/Bk_f_in_s
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/Bk_d_in_s
add wave -noupdate /tb_b_fd_sum/uut/state_s
add wave -noupdate /tb_b_fd_sum/uut/start_calc_bdot_s
add wave -noupdate /tb_b_fd_sum/uut/start_calc_b_s
add wave -noupdate /tb_b_fd_sum/uut/end_s
add wave -noupdate /tb_b_fd_sum/uut/end_calc_b_s
add wave -noupdate -radix hexadecimal /tb_b_fd_sum/uut/cmp_B_F_D/D_i
add wave -noupdate -radix hexadecimal /tb_b_fd_sum/uut/cmp_B_F_D/C_i
add wave -noupdate -radix hexadecimal /tb_b_fd_sum/uut/cmp_B_F_D/B_i
add wave -noupdate -radix hexadecimal /tb_b_fd_sum/uut/cmp_B_F_D/A_i
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/cmp_B_F_D/D_in_s
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/cmp_B_F_D/C_in_s
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/cmp_B_F_D/B_in_s
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/cmp_B_F_D/A_in_s
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/cmp_B_F_D/result_o
add wave -noupdate /tb_b_fd_sum/uut/cmp_B_F_D/result_rdy_o
add wave -noupdate /tb_b_fd_sum/uut/cmp_B_F_D/start_i
add wave -noupdate /tb_b_fd_sum/uut/fact_s
add wave -noupdate /tb_b_fd_sum/uut/mult_Bkm1_s
add wave -noupdate /tb_b_fd_sum/uut/mult_Bk_s
add wave -noupdate /tb_b_fd_sum/uut/Bkm1_s
add wave -noupdate /tb_b_fd_sum/uut/Bkm1_36_s
add wave -noupdate /tb_b_fd_sum/uut/Bk_s
add wave -noupdate /tb_b_fd_sum/uut/Bk_36_s
add wave -noupdate /tb_b_fd_sum/uut/result_mult_Bkm1_s
add wave -noupdate /tb_b_fd_sum/uut/result_mult_Bk_s
add wave -noupdate /tb_b_fd_sum/uut/cmp_B_F_D/state_s
add wave -noupdate -divider Output
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/Bk_o
add wave -noupdate /tb_b_fd_sum/uut/B_data_rdy_o
add wave -noupdate -radix decimal /tb_b_fd_sum/uut/Bdot_o
add wave -noupdate /tb_b_fd_sum/Bdot_data_rdy_obs
add wave -noupdate /tb_b_fd_sum/B_data_rdy_obs
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {208 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 336
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {528 ns} {1024 ns}
