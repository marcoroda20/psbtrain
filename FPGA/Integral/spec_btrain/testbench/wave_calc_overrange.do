onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_overrange/uut_overrange/clk_i
add wave -noupdate /tb_overrange/uut_overrange/reset_i
add wave -noupdate -divider Input
add wave -noupdate /tb_overrange/uut_overrange/nb_overrange_i
add wave -noupdate /tb_overrange/uut_overrange/max_adc_overr_i
add wave -noupdate /tb_overrange/uut_overrange/clear_overrange_i
add wave -noupdate /tb_overrange/uut_overrange/adc_data_rdy_i
add wave -noupdate /tb_overrange/uut_overrange/adc_data_i
add wave -noupdate -divider Calc
add wave -noupdate -radix decimal /tb_overrange/uut_overrange/count_nb_val_s
add wave -noupdate -radix decimal /tb_overrange/uut_overrange/sum_value_s
add wave -noupdate -radix decimal /tb_overrange/uut_overrange/acc_value_s
add wave -noupdate -radix decimal /tb_overrange/uut_overrange/divisor_s
add wave -noupdate -radix decimal /tb_overrange/uut_overrange/divider_output_s
add wave -noupdate /tb_overrange/uut_overrange/start_div_s
add wave -noupdate /tb_overrange/uut_overrange/end_divider_s
add wave -noupdate -radix decimal /tb_overrange/uut_overrange/result_div_s
add wave -noupdate -divider Output
add wave -noupdate /tb_overrange/uut_overrange/overrange_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1335 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 286
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ns} {105 us}
