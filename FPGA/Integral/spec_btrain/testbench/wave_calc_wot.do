onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_bcalculwithouttiming/reset_sti
add wave -noupdate -divider {State Machine}
add wave -noupdate /tb_bcalculwithouttiming/C0_sti
add wave -noupdate /tb_bcalculwithouttiming/uut/C0_pulse_s
add wave -noupdate /tb_bcalculwithouttiming/uut/B_rst_s
add wave -noupdate /tb_bcalculwithouttiming/low_marker_sti
add wave -noupdate /tb_bcalculwithouttiming/uut/low_marker_pulse_s
add wave -noupdate /tb_bcalculwithouttiming/uut/low_marker_s
add wave -noupdate /tb_bcalculwithouttiming/uut/start_s
add wave -noupdate /tb_bcalculwithouttiming/uut/state_s
add wave -noupdate /tb_bcalculwithouttiming/uut/start_mult_s
add wave -noupdate /tb_bcalculwithouttiming/uut/end_mult_s
add wave -noupdate /tb_bcalculwithouttiming/uut/end_mult_in_s
add wave -noupdate -divider Input
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/Bk_i
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/Bsmooth_i
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/corr_fact_i
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/Vk_i
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/Vo_i
add wave -noupdate /tb_bcalculwithouttiming/C0_sti
add wave -noupdate /tb_bcalculwithouttiming/uut/C0_pulse_s
add wave -noupdate -divider Calc
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/low_marker_val_i
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/high_marker_val_i
add wave -noupdate -radix hexadecimal /tb_bcalculwithouttiming/uut/vk_sub_vo_32_s
add wave -noupdate -radix hexadecimal /tb_bcalculwithouttiming/uut/vk_sub_vo_36_s
add wave -noupdate -radix hexadecimal /tb_bcalculwithouttiming/uut/corr_fact_s
add wave -noupdate -radix hexadecimal /tb_bcalculwithouttiming/uut/Bk_s
add wave -noupdate -radix hexadecimal -childformat {{/tb_bcalculwithouttiming/uut/result_mult_s(71) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(70) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(69) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(68) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(67) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(66) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(65) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(64) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(63) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(62) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(61) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(60) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(59) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(58) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(57) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(56) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(55) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(54) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(53) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(52) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(51) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(50) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(49) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(48) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(47) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(46) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(45) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(44) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(43) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(42) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(41) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(40) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(39) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(38) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(37) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(36) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(35) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(34) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(33) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(32) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(31) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(30) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(29) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(28) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(27) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(26) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(25) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(24) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(23) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(22) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(21) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(20) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(19) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(18) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(17) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(16) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(15) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(14) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(13) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(12) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(11) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(10) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(9) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(8) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(7) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(6) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(5) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(4) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(3) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(2) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(1) -radix decimal} {/tb_bcalculwithouttiming/uut/result_mult_s(0) -radix decimal}} -subitemconfig {/tb_bcalculwithouttiming/uut/result_mult_s(71) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(70) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(69) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(68) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(67) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(66) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(65) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(64) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(63) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(62) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(61) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(60) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(59) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(58) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(57) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(56) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(55) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(54) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(53) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(52) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(51) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(50) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(49) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(48) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(47) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(46) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(45) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(44) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(43) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(42) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(41) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(40) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(39) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(38) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(37) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(36) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(35) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(34) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(33) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(32) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(31) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(30) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(29) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(28) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(27) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(26) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(25) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(24) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(23) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(22) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(21) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(20) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(19) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(18) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(17) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(16) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(15) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(14) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(13) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(12) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(11) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(10) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(9) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(8) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(7) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(6) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(5) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(4) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(3) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(2) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(1) {-height 15 -radix decimal} /tb_bcalculwithouttiming/uut/result_mult_s(0) {-height 15 -radix decimal}} /tb_bcalculwithouttiming/uut/result_mult_s
add wave -noupdate -radix hexadecimal /tb_bcalculwithouttiming/uut/Bk_corr_fact_s
add wave -noupdate -divider Multiplier
add wave -noupdate /tb_bcalculwithouttiming/uut/cmp_muliplier/start_i
add wave -noupdate /tb_bcalculwithouttiming/uut/cmp_muliplier/Present_State_s
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/cmp_muliplier/A_i
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/cmp_muliplier/B_i
add wave -noupdate /tb_bcalculwithouttiming/uut/cmp_muliplier/sign_A_s
add wave -noupdate /tb_bcalculwithouttiming/uut/cmp_muliplier/sign_B_s
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/cmp_muliplier/B_in_s
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/cmp_muliplier/A_in_s
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/cmp_muliplier/AB_11_s
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/cmp_muliplier/AB_12_s
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/cmp_muliplier/AB_21_s
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/cmp_muliplier/AB_22_s
add wave -noupdate -radix decimal /tb_bcalculwithouttiming/uut/cmp_muliplier/AB_1_s
add wave -noupdate -radix hexadecimal /tb_bcalculwithouttiming/uut/cmp_muliplier/AB_2_s
add wave -noupdate -radix hexadecimal /tb_bcalculwithouttiming/uut/cmp_muliplier/result_s
add wave -noupdate /tb_bcalculwithouttiming/uut/cmp_muliplier/result_rdy_o
add wave -noupdate -radix hexadecimal /tb_bcalculwithouttiming/uut/cmp_muliplier/result_o
add wave -noupdate -divider Output
add wave -noupdate -radix hexadecimal /tb_bcalculwithouttiming/uut/B_out_s
add wave -noupdate -radix hexadecimal /tb_bcalculwithouttiming/uut/BKp1_o
add wave -noupdate /tb_bcalculwithouttiming/data_rdy_obs
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {4036 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 331
configure wave -valuecolwidth 116
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {3828 ns} {4524 ns}
