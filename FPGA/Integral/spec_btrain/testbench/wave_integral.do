onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_btrain_integral/uut/clk_i
add wave -noupdate /tb_btrain_integral/uut/reset_i
add wave -noupdate -divider {Configuration Integral}
add wave -noupdate /tb_btrain_integral/uut/Bk_3_8_s
add wave -noupdate /tb_btrain_integral/uut/Bk_rect_s
add wave -noupdate /tb_btrain_integral/uut/Bsmooth_i
add wave -noupdate /tb_btrain_integral/uut/C0_i
add wave -noupdate /tb_btrain_integral/uut/Vo_i
add wave -noupdate /tb_btrain_integral/uut/adc_data_i
add wave -noupdate /tb_btrain_integral/uut/adc_data_rdy_i
add wave -noupdate /tb_btrain_integral/uut/corr_fact_i
add wave -noupdate /tb_btrain_integral/uut/data_rdy_3_8_s
add wave -noupdate /tb_btrain_integral/uut/data_rdy_rect_s
add wave -noupdate /tb_btrain_integral/uut/high_marker_i
add wave -noupdate /tb_btrain_integral/uut/high_marker_val_i
add wave -noupdate /tb_btrain_integral/uut/init_Bk_i
add wave -noupdate /tb_btrain_integral/uut/int_c0_used_i
add wave -noupdate /tb_btrain_integral/uut/int_range_sel_i
add wave -noupdate /tb_btrain_integral/uut/low_marker_i
add wave -noupdate /tb_btrain_integral/uut/low_marker_val_i
add wave -noupdate /tb_btrain_integral/uut/reg_val_cfg_sel_integral_i
add wave -noupdate -divider Output
add wave -noupdate /tb_btrain_integral/uut/BKp1_o
add wave -noupdate /tb_btrain_integral/uut/B_data_rdy_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 381
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {810563 ps}
