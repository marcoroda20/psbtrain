onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_maintain/uut/clk_i
add wave -noupdate /tb_maintain/uut/reset_i
add wave -noupdate -divider Signals
add wave -noupdate /tb_maintain/uut/s_intern_s
add wave -noupdate /tb_maintain/uut/s_i
add wave -noupdate /tb_maintain/uut/s_keep_s
add wave -noupdate -radix decimal /tb_maintain/uut/g_factor
add wave -noupdate /tb_maintain/uut/count_ctrl_s
add wave -noupdate /tb_maintain/uut/s_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
quietly wave cursor active 0
configure wave -namecolwidth 252
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ns} {95916 ns}
