onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /main/spec_btrainA/reset_n_hw_i
add wave -noupdate /main/spec_btrainA/reg_status_reset_hw_s
add wave -noupdate /main/spec_btrainA/clk_ref
add wave -noupdate -radix hexadecimal /main/spec_btrainA/reset_s
add wave -noupdate /main/L_RST_N
add wave -noupdate -radix hexadecimal /main/spec_btrainA/reset_n_s
add wave -noupdate /main/button
add wave -noupdate -radix hexadecimal /main/spec_btrainA/start_timer_reset_s
add wave -noupdate -radix hexadecimal /main/spec_btrainA/start_timer_counter_s
add wave -noupdate /main/spec_btrainA/reg_status_simeff_s
add wave -noupdate -radix hexadecimal /main/spec_btrainA/aux_buttons_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/sys_clk_pll_locked_s
add wave -noupdate /main/spec_btrainA/led_sfp_green_o
add wave -noupdate /main/spec_btrainA/sync_force_send_s
add wave -noupdate /main/spec_btrainA/force_send_s
add wave -noupdate -radix unsigned /main/spec_btrainA/count_sync_s
add wave -noupdate /main/spec_btrainA/cmp_BTrainReg/rst_n_i
add wave -noupdate /main/spec_btrainA/cmp_BTrainReg/reg_wr_sample_period_o
add wave -noupdate /main/spec_btrainA/tx_sync_s
add wave -noupdate -divider txCtrl
add wave -noupdate -height 16 /main/spec_btrainA/cmp_WRBTrain/cmp_txCtrl/state_s
add wave -noupdate /main/spec_btrainA/cmp_WRBTrain/cmp_txCtrl/tx_sync_i
add wave -noupdate /main/spec_btrainA/cmp_WRBTrain/cmp_txCtrl/tx_sync_cmd_s
add wave -noupdate /main/spec_btrainA/cmp_WRBTrain/cmp_txCtrl/tx_last_o
add wave -noupdate /main/spec_btrainA/cmp_WRBTrain/cmp_txCtrl/tx_flush_o
add wave -noupdate /main/spec_btrainA/cmp_WRBTrain/cmp_txCtrl/tx_dreq_s
add wave -noupdate /main/spec_btrainA/cmp_WRBTrain/cmp_txCtrl/tx_dreq_i
add wave -noupdate /main/spec_btrainA/cmp_WRBTrain/cmp_txCtrl/tx_valid_o
add wave -noupdate /main/spec_btrainA/cmp_WRBTrain/cmp_txCtrl/tx_data_o
add wave -noupdate -divider {SPEC RESET GEN}
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/clk_sys_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/rst_pcie_n_a_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/rst_button_n_a_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/rst_n_o
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/powerup_cnt
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/button_synced_n
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/pcie_synced_n
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/powerup_n
add wave -noupdate -divider {SPEC A - TX Streamer}
add wave -noupdate /main/spec_btrainA/cmp_WRBTrain/tx_flush_o
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/timeout_counter
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/tx_timeout_hit
add wave -noupdate -height 16 /main/spec_btrainA/U_TX_Streamer/state
add wave -noupdate -expand /main/spec_btrainA/U_TX_Streamer/fsm_out
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/tx_flush_latched
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/tx_flush_i
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/tx_threshold_hit
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/ser_count
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/total_words
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/seq_no
add wave -noupdate -radix hexadecimal /main/spec_btrainA/tx_data
add wave -noupdate -radix hexadecimal /main/spec_btrainA/tx_dreq
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_TX_Streamer/tx_data_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_TX_Streamer/tx_valid_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_TX_Streamer/tx_dreq_o
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/tx_almost_full
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/reset_dly
add wave -noupdate -radix hexadecimal -childformat {{/main/spec_btrainA/U_TX_Streamer/src_i.ack -radix hexadecimal} {/main/spec_btrainA/U_TX_Streamer/src_i.stall -radix hexadecimal} {/main/spec_btrainA/U_TX_Streamer/src_i.err -radix hexadecimal} {/main/spec_btrainA/U_TX_Streamer/src_i.rty -radix hexadecimal}} -subitemconfig {/main/spec_btrainA/U_TX_Streamer/src_i.ack {-height 13 -radix hexadecimal} /main/spec_btrainA/U_TX_Streamer/src_i.stall {-height 13 -radix hexadecimal} /main/spec_btrainA/U_TX_Streamer/src_i.err {-height 13 -radix hexadecimal} /main/spec_btrainA/U_TX_Streamer/src_i.rty {-height 13 -radix hexadecimal}} /main/spec_btrainA/U_TX_Streamer/src_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_TX_Streamer/src_o
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/U_TX_Buffer/d_i
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/U_TX_Buffer/count_o
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/tx_fifo_d
add wave -noupdate /main/spec_btrainA/U_TX_Streamer/tx_fifo_q
add wave -noupdate -divider {SPEC A - PHY}
add wave -noupdate -radix hexadecimal /main/spec_btrainA/phy_tx_data
add wave -noupdate -radix hexadecimal /main/spec_btrainA/phy_tx_k
add wave -noupdate -radix hexadecimal /main/spec_btrainA/phy_tx_disparity
add wave -noupdate -radix hexadecimal /main/spec_btrainA/phy_tx_enc_err
add wave -noupdate -divider {SPEC B - PHY}
add wave -noupdate -radix hexadecimal /main/spec_btrainB/phy_rx_data
add wave -noupdate -radix hexadecimal /main/spec_btrainB/phy_rx_rbclk
add wave -noupdate -radix hexadecimal /main/spec_btrainB/phy_rx_k
add wave -noupdate -radix hexadecimal /main/spec_btrainB/phy_rx_enc_err
add wave -noupdate -divider RXFabric
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/we
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/sof_o
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/snk_out
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/snk_o
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/snk_i
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/rst_n_i
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/rd_d0
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/rd
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/q_valid
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/pre_sof
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/pre_eof
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/pre_dvalid
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/pre_bytesel
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/post_sof
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/post_dvalid
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/post_data
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/post_addr
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/full
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/fout_reg
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/fout
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/fin
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/error_o
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/eof_o
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/dvalid_o
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/dreq_i
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/data_o
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/cyc_d0
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/clk_i
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/c_fifo_width
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/bytesel_o
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_Fabric_Sink/addr_o
add wave -noupdate -divider CRC
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/fsm_in
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/crc_restart
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/crc_match
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/crc_en_masked
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/crc_en
add wave -noupdate -divider {SPEC B - RX streamer}
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/rst_n_i
add wave -noupdate -height 16 /main/spec_btrainB/U_RX_Streamer/state
add wave -noupdate -expand /main/spec_btrainB/U_RX_Streamer/fab
add wave -noupdate -expand /main/spec_btrainB/U_RX_Streamer/fsm_in
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/seq_no
add wave -noupdate -radix hexadecimal -childformat {{/main/spec_btrainB/U_RX_Streamer/snk_i.adr -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/snk_i.dat -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/snk_i.cyc -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/snk_i.stb -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/snk_i.we -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/snk_i.sel -radix hexadecimal}} -expand -subitemconfig {/main/spec_btrainB/U_RX_Streamer/snk_i.adr {-radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/snk_i.dat {-radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/snk_i.cyc {-radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/snk_i.stb {-radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/snk_i.we {-radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/snk_i.sel {-radix hexadecimal}} /main/spec_btrainB/U_RX_Streamer/snk_i
add wave -noupdate -radix hexadecimal /main/spec_btrainB/U_RX_Streamer/snk_o
add wave -noupdate -radix hexadecimal -childformat {{/main/spec_btrainB/U_RX_Streamer/rx_data_o(15) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(14) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(13) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(12) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(11) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(10) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(9) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(8) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(7) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(6) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(5) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(4) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(3) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(2) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(1) -radix hexadecimal} {/main/spec_btrainB/U_RX_Streamer/rx_data_o(0) -radix hexadecimal}} -subitemconfig {/main/spec_btrainB/U_RX_Streamer/rx_data_o(15) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(14) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(13) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(12) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(11) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(10) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(9) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(8) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(7) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(6) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(5) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(4) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(3) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(2) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(1) {-height 13 -radix hexadecimal} /main/spec_btrainB/U_RX_Streamer/rx_data_o(0) {-height 13 -radix hexadecimal}} /main/spec_btrainB/U_RX_Streamer/rx_data_o
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/rx_lost_o
add wave -noupdate -radix hexadecimal /main/spec_btrainB/U_RX_Streamer/rx_valid_o
add wave -noupdate -radix hexadecimal /main/spec_btrainB/U_RX_Streamer/rx_dreq_i
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/rx_latency_valid_o
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/rx_latency_o
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/rx_lost_o
add wave -noupdate -radix hexadecimal /main/spec_btrainA/led_s
add wave -noupdate -divider xrx-streamer
add wave -noupdate -height 16 /main/spec_btrainB/U_RX_Streamer/state
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/ser_count
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/tx_tag_cycles
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_rx_crc_generator/data_i
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_rx_crc_generator/data_i2
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_rx_crc_generator/crca
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_rx_crc_generator/crc_next
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/U_rx_crc_generator/crc_cur
add wave -noupdate -expand /main/spec_btrainB/U_RX_Streamer/fsm_in
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/fifo_last
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/fifo_drop
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/fifo_dout
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/fifo_din
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/fifo_lost
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/fifo_accept
add wave -noupdate /main/spec_btrainB/U_RX_Streamer/fab_dvalid_pre
add wave -noupdate /main/spec_btrainA/clk_ref
add wave -noupdate -radix hexadecimal /main/spec_btrainA/reset_s
add wave -noupdate /main/L_RST_N
add wave -noupdate -radix hexadecimal /main/spec_btrainA/reset_n_s
add wave -noupdate /main/button
add wave -noupdate -radix hexadecimal /main/spec_btrainA/start_timer_reset_s
add wave -noupdate -radix hexadecimal /main/spec_btrainA/start_timer_counter_s
add wave -noupdate -radix hexadecimal /main/spec_btrainA/aux_buttons_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/sys_clk_pll_locked_s
add wave -noupdate -radix hexadecimal /main/spec_btrainB/led_s
add wave -noupdate /main/spec_btrainA/led_sfp_green_o
add wave -noupdate -radix unsigned /main/spec_btrainB/U_RX_Streamer/count_data
add wave -noupdate -divider rxCtrl
add wave -noupdate -height 16 /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/state_s
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/rxframe_valid_o
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/rxframe_ctrl_o
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/rx_valid_i
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/rx_last_i
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/rx_first_i
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/rx_data_i
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/reset_i
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/g_data_width
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/frame_rdy_s
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/frame_ctrl_s
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/first_frame_s
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/error_type_s
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/clk_i
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/Irxframe_value_o
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/Iframe_value_s
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/Brxframe_value_o
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/Bframe_value_s
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/Bframe_slv_s
add wave -noupdate /main/spec_btrainB/cmp_WRBTrain/cmp_rxCtrl/Bframe_counter_s
add wave -noupdate -divider {Counter Losses}
add wave -noupdate /main/spec_btrainB/cmp_losses_counter/g_max_time_counter
add wave -noupdate /main/spec_btrainB/cmp_losses_counter/losses_value_o
add wave -noupdate /main/spec_btrainB/cmp_losses_counter/lost_i
add wave -noupdate /main/spec_btrainB/cmp_losses_counter/lost_rdy_i
add wave -noupdate /main/spec_btrainB/cmp_losses_counter/lost_rdy_s
add wave -noupdate -height 16 /main/spec_btrainB/cmp_losses_counter/state_s
add wave -noupdate /main/spec_btrainB/cmp_losses_counter/time_counter_s
add wave -noupdate /main/spec_btrainB/cmp_losses_counter/counter_s
add wave -noupdate -divider {WR value}
add wave -noupdate /main/spec_btrainB/new_I_rdy_s
add wave -noupdate /main/spec_btrainB/I_s
add wave -noupdate /main/spec_btrainB/new_B_rdy_s
add wave -noupdate /main/spec_btrainB/wr_S_s
add wave -noupdate /main/spec_btrainB/wr_G_s
add wave -noupdate /main/spec_btrainB/wr_Bdot_s
add wave -noupdate /main/spec_btrainB/wr_B_s
add wave -noupdate -divider {Register WR}
add wave -noupdate /main/spec_btrainB/reg_wr_ctrl_rx_latency_s
add wave -noupdate /main/spec_btrainB/reg_wr_ctrl_lost_frame_s
add wave -noupdate /main/spec_btrainB/reg_wr_frame_type_s
add wave -noupdate -divider {SPEC RESET GEN}
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/clk_sys_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/rst_pcie_n_a_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/rst_button_n_a_i
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/rst_n_o
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/powerup_cnt
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/button_synced_n
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/pcie_synced_n
add wave -noupdate -radix hexadecimal /main/spec_btrainA/U_Reset_Gen/powerup_n
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {166307623790 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 216
configure wave -valuecolwidth 79
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 fs} {315 us}
