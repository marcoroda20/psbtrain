--------------------------------------------------------------------------------
--
-- CERN BE-CO-HT         Reference design for PCIe FMC carrier
--                       http://www.ohwr.org/projects/fmc-pci-carrier
--------------------------------------------------------------------------------
--
-- unit name: pfc_ddr_ref_design_top (pfc_ref_design_top.vhd)
--
-- author: Matthieu Cattin (matthieu.cattin@cern.ch)
--
-- date: 21-02-2011
--
-- version: 1.0
--
-- description: Top entity for PFC board.
--
--------------------------------------------------------------------------------
-- last changes: see svn log.
--------------------------------------------------------------------------------
-- TODO: - 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use work.gn4124_core_pkg.all;

library UNISIM;
use UNISIM.vcomponents.all;


entity pfc_top is
  generic(
    g_SIMULATION    : string := "FALSE";
    g_CALIB_SOFT_IP : string := "TRUE"
    );
  port
    (

      -- Global ports
      SYS_CLK_P : in std_logic;         -- 25MHz system clock
      SYS_CLK_N : in std_logic;         -- 25MHz system clock

      -- From GN4124 Local bus
      L_CLKp : in std_logic;            -- Local bus clock (frequency set in GN4124 config registers)
      L_CLKn : in std_logic;            -- Local bus clock (frequency set in GN4124 config registers)

      L_RST_N : in std_logic;           -- Reset from GN4124 (RSTOUT18_N)

      -- General Purpose Interface
      GPIO : inout std_logic_vector(1 downto 0);  -- GPIO[0] -> GN4124 GPIO8
                                                  -- GPIO[1] -> GN4124 GPIO9

      -- PCIe to Local [Inbound Data] - RX
      P2L_RDY    : out std_logic;                      -- Rx Buffer Full Flag
      P2L_CLKn   : in  std_logic;                      -- Receiver Source Synchronous Clock-
      P2L_CLKp   : in  std_logic;                      -- Receiver Source Synchronous Clock+
      P2L_DATA   : in  std_logic_vector(15 downto 0);  -- Parallel receive data
      P2L_DFRAME : in  std_logic;                      -- Receive Frame
      P2L_VALID  : in  std_logic;                      -- Receive Data Valid

      -- Inbound Buffer Request/Status
      P_WR_REQ : in  std_logic_vector(1 downto 0);  -- PCIe Write Request
      P_WR_RDY : out std_logic_vector(1 downto 0);  -- PCIe Write Ready
      RX_ERROR : out std_logic;                     -- Receive Error

      -- Local to Parallel [Outbound Data] - TX
      L2P_DATA   : out std_logic_vector(15 downto 0);  -- Parallel transmit data
      L2P_DFRAME : out std_logic;                      -- Transmit Data Frame
      L2P_VALID  : out std_logic;                      -- Transmit Data Valid
      L2P_CLKn   : out std_logic;                      -- Transmitter Source Synchronous Clock-
      L2P_CLKp   : out std_logic;                      -- Transmitter Source Synchronous Clock+
      L2P_EDB    : out std_logic;                      -- Packet termination and discard

      -- Outbound Buffer Status
      L2P_RDY    : in std_logic;                     -- Tx Buffer Full Flag
      L_WR_RDY   : in std_logic_vector(1 downto 0);  -- Local-to-PCIe Write
      P_RD_D_RDY : in std_logic_vector(1 downto 0);  -- PCIe-to-Local Read Response Data Ready
      TX_ERROR   : in std_logic;                     -- Transmit Error
      VC_RDY     : in std_logic_vector(1 downto 0);  -- Channel ready

      -- Font panel LEDs
      LED_RED   : out std_logic;
      LED_GREEN : out std_logic;

      -- User IO (eSATA connector)
      USER_IO_0_P : out std_logic;
      USER_IO_0_N : out std_logic;
      USER_IO_1_P : out std_logic;
      USER_IO_1_N : out std_logic;

      -- FMC connector management signals
      PRSNT_M2C_L       : in    std_logic;
      PG_C2M            : out   std_logic;
      M2C_DIR           : in    std_logic;  -- HPC only
      FPGA_SDA          : inout std_logic;
      FPGA_SCL          : out   std_logic;
      TMS_TO_FMC_P1V8   : out   std_logic;
      TDO_FROM_FMC_P1V8 : in    std_logic;
      TDI_TO_FMC_P1V8   : out   std_logic;
      TCK_TO_FMC_P1V8   : out   std_logic;
      -- Note: TRST_TO_FMC is accessed through the SPI I/O extender

      -- FMC connector Gigabit links
      --DP4_M2C_P : in  std_logic;        -- HPC only
      --DP4_M2C_N : in  std_logic;        -- HPC only
      --DP4_C2M_P : out std_logic;        -- HPC only
      --DP4_C2M_N : out std_logic;        -- HPC only
      --DP3_M2C_P : in  std_logic;        -- HPC only
      --DP3_M2C_N : in  std_logic;        -- HPC only
      --DP3_C2M_P : out std_logic;        -- HPC only
      --DP3_C2M_N : out std_logic;        -- HPC only
      --DP2_M2C_P : in  std_logic;        -- HPC only
      --DP2_M2C_N : in  std_logic;        -- HPC only
      --DP2_C2M_P : out std_logic;        -- HPC only
      --DP2_C2M_N : out std_logic;        -- HPC only
      --DP1_M2C_P : in  std_logic;        -- HPC only
      --DP1_M2C_N : in  std_logic;        -- HPC only
      --DP1_C2M_P : out std_logic;        -- HPC only
      --DP1_C2M_N : out std_logic;        -- HPC only
      --DP0_M2C_P : in  std_logic;
      --DP0_M2C_N : in  std_logic;
      --DP0_C2M_P : out std_logic;
      --DP0_C2M_N : out std_logic;

      -- FMC connector Gigabit links clocks
      --GBTCLK1_M2C_P : in std_logic;     -- HPC only
      --GBTCLK1_M2C_N : in std_logic;     -- HPC only
      --GBTCLK0_M2C_P : in std_logic;
      --GBTCLK0_M2C_N : in std_logic;

      -- FMC connector clock inputs
      CLK1_M2C_P : in std_logic;
      CLK1_M2C_N : in std_logic;
      CLK0_M2C_P : in std_logic;
      CLK0_M2C_N : in std_logic;

      -- FMC connector user defined signals
      LA33_P : out std_logic;
      LA33_N : out std_logic;
      LA32_P : out std_logic;
      LA32_N : out std_logic;
      LA31_P : out std_logic;
      LA31_N : out std_logic;
      LA30_P : out std_logic;
      LA30_N : out std_logic;
      LA29_P : out std_logic;
      LA29_N : out std_logic;
      LA28_P : out std_logic;
      LA28_N : out std_logic;
      LA27_P : out std_logic;
      LA27_N : out std_logic;
      LA26_P : out std_logic;
      LA26_N : out std_logic;
      LA25_P : out std_logic;
      LA25_N : out std_logic;
      LA24_P : out std_logic;
      LA24_N : out std_logic;
      LA23_P : out std_logic;
      LA23_N : out std_logic;
      LA22_P : out std_logic;
      LA22_N : out std_logic;
      LA21_P : out std_logic;
      LA21_N : out std_logic;
      LA20_P : out std_logic;
      LA20_N : out std_logic;
      LA19_P : out std_logic;
      LA19_N : out std_logic;
      LA18_P : out std_logic;
      LA18_N : out std_logic;
      LA17_P : out std_logic;
      LA17_N : out std_logic;
      LA16_P : out std_logic;
      LA16_N : out std_logic;
      LA15_P : out std_logic;
      LA15_N : out std_logic;
      LA14_P : out std_logic;
      LA14_N : out std_logic;
      LA13_P : out std_logic;
      LA13_N : out std_logic;
      LA12_P : out std_logic;
      LA12_N : out std_logic;
      LA11_P : out std_logic;
      LA11_N : out std_logic;
      LA10_P : in  std_logic;
      LA10_N : out std_logic;
      LA09_P : out std_logic;
      LA09_N : out std_logic;
      LA08_P : out std_logic;
      LA08_N : out std_logic;
      LA07_P : out std_logic;
      LA07_N : out std_logic;
      LA06_P : out std_logic;
      LA06_N : out std_logic;
      LA05_P : out std_logic;
      LA05_N : out std_logic;
      LA04_P : out std_logic;
      LA04_N : out std_logic;
      LA03_P : out std_logic;
      LA03_N : out std_logic;
      LA02_P : out std_logic;
      LA02_N : out std_logic;
      LA01_P : in  std_logic;
      LA01_N : out std_logic;
      LA00_P : out std_logic;
      LA00_N : out std_logic;

      -- SPI interface
      SCLK_V_MON_P1V8 : out std_logic;
      DIN_V_MON       : out std_logic;
      CSVR_V_MON_P1V8 : out std_logic;  -- Digital potentiometer for VADJ

      -- Power supplies control
      ENABLE_VADJ : out std_logic;

      -- DDR3 interface
      ddr3_a_o      : out   std_logic_vector(13 downto 0);
      ddr3_ba_o     : out   std_logic_vector(2 downto 0);
      ddr3_cas_n_o  : out   std_logic;
      ddr3_clk_p_o  : out   std_logic;
      ddr3_clk_n_o  : out   std_logic;
      ddr3_cke_o    : out   std_logic;
      ddr3_dm_o     : out   std_logic;
      ddr3_dq_b     : inout std_logic_vector(15 downto 0);
      ddr3_dqs_p_b  : inout std_logic;
      ddr3_dqs_n_b  : inout std_logic;
      ddr3_odt_o    : out   std_logic;
      ddr3_ras_n_o  : out   std_logic;
      ddr3_rst_n_o  : out   std_logic;
      ddr3_udm_o    : out   std_logic;
      ddr3_udqs_p_b : inout std_logic;
      ddr3_udqs_n_b : inout std_logic;
      ddr3_we_n_o   : out   std_logic;
      ddr3_rzq_b    : inout std_logic;
      ddr3_zio_b    : inout std_logic
      );
end pfc_top;

architecture rtl of pfc_top is

  ------------------------------------------------------------------------------
  -- Components declaration
  ------------------------------------------------------------------------------

  component gn4124_core
    generic(
      g_BAR0_APERTURE     : integer := 20;     -- BAR0 aperture, defined in GN4124 PCI_BAR_CONFIG register (0x80C)
                                               -- => number of bits to address periph on the board
      g_CSR_WB_SLAVES_NB  : integer := 1;      -- Number of CSR wishbone slaves
      g_DMA_WB_SLAVES_NB  : integer := 1;      -- Number of DMA wishbone slaves
      g_DMA_WB_ADDR_WIDTH : integer := 26      -- DMA wishbone address bus width
      );
    port
      (
        ---------------------------------------------------------
        -- Control and status
        --
        -- Asynchronous reset from GN4124
        rst_n_a_i      : in  std_logic;
        -- P2L clock PLL locked
        p2l_pll_locked : out std_logic;
        -- Debug ouputs
        debug_o        : out std_logic_vector(7 downto 0);

        ---------------------------------------------------------
        -- P2L Direction
        --
        -- Source Sync DDR related signals
        p2l_clk_p_i  : in  std_logic;                      -- Receiver Source Synchronous Clock+
        p2l_clk_n_i  : in  std_logic;                      -- Receiver Source Synchronous Clock-
        p2l_data_i   : in  std_logic_vector(15 downto 0);  -- Parallel receive data
        p2l_dframe_i : in  std_logic;                      -- Receive Frame
        p2l_valid_i  : in  std_logic;                      -- Receive Data Valid
        -- P2L Control
        p2l_rdy_o    : out std_logic;                      -- Rx Buffer Full Flag
        p_wr_req_i   : in  std_logic_vector(1 downto 0);   -- PCIe Write Request
        p_wr_rdy_o   : out std_logic_vector(1 downto 0);   -- PCIe Write Ready
        rx_error_o   : out std_logic;                      -- Receive Error

        ---------------------------------------------------------
        -- L2P Direction
        --
        -- Source Sync DDR related signals
        l2p_clk_p_o  : out std_logic;                      -- Transmitter Source Synchronous Clock+
        l2p_clk_n_o  : out std_logic;                      -- Transmitter Source Synchronous Clock-
        l2p_data_o   : out std_logic_vector(15 downto 0);  -- Parallel transmit data
        l2p_dframe_o : out std_logic;                      -- Transmit Data Frame
        l2p_valid_o  : out std_logic;                      -- Transmit Data Valid
        l2p_edb_o    : out std_logic;                      -- Packet termination and discard
        -- L2P Control
        l2p_rdy_i    : in  std_logic;                      -- Tx Buffer Full Flag
        l_wr_rdy_i   : in  std_logic_vector(1 downto 0);   -- Local-to-PCIe Write
        p_rd_d_rdy_i : in  std_logic_vector(1 downto 0);   -- PCIe-to-Local Read Response Data Ready
        tx_error_i   : in  std_logic;                      -- Transmit Error
        vc_rdy_i     : in  std_logic_vector(1 downto 0);   -- Channel ready

        ---------------------------------------------------------
        -- Interrupt interface
        dma_irq_o : out std_logic_vector(1 downto 0);  -- Interrupts sources to IRQ manager
        irq_p_i   : in  std_logic;                     -- Interrupt request pulse from IRQ manager
        irq_p_o   : out std_logic;                     -- Interrupt request pulse to GN4124 GPIO

        ---------------------------------------------------------
        -- Target interface (CSR wishbone master)
        wb_clk_i : in  std_logic;
        wb_adr_o : out std_logic_vector(g_BAR0_APERTURE-log2_ceil(g_CSR_WB_SLAVES_NB+1)-1 downto 0);
        wb_dat_o : out std_logic_vector(31 downto 0);                         -- Data out
        wb_sel_o : out std_logic_vector(3 downto 0);                          -- Byte select
        wb_stb_o : out std_logic;
        wb_we_o  : out std_logic;
        wb_cyc_o : out std_logic_vector(g_CSR_WB_SLAVES_NB-1 downto 0);
        wb_dat_i : in  std_logic_vector((32*g_CSR_WB_SLAVES_NB)-1 downto 0);  -- Data in
        wb_ack_i : in  std_logic_vector(g_CSR_WB_SLAVES_NB-1 downto 0);

        ---------------------------------------------------------
        -- DMA interface (Pipelined wishbone master)
        dma_clk_i   : in  std_logic;
        dma_adr_o   : out std_logic_vector(31 downto 0);
        dma_dat_o   : out std_logic_vector(31 downto 0);                         -- Data out
        dma_sel_o   : out std_logic_vector(3 downto 0);                          -- Byte select
        dma_stb_o   : out std_logic;
        dma_we_o    : out std_logic;
        dma_cyc_o   : out std_logic;                                             --_vector(g_DMA_WB_SLAVES_NB-1 downto 0);
        dma_dat_i   : in  std_logic_vector((32*g_DMA_WB_SLAVES_NB)-1 downto 0);  -- Data in
        dma_ack_i   : in  std_logic;                                             --_vector(g_DMA_WB_SLAVES_NB-1 downto 0);
        dma_stall_i : in  std_logic--_vector(g_DMA_WB_SLAVES_NB-1 downto 0)        -- for pipelined Wishbone
        );
  end component;  --  gn4124_core

  component fmc_io_regs
    port (
      rst_n_i           : in  std_logic;
      wb_clk_i          : in  std_logic;
      wb_addr_i         : in  std_logic_vector(2 downto 0);
      wb_data_i         : in  std_logic_vector(31 downto 0);
      wb_data_o         : out std_logic_vector(31 downto 0);
      wb_cyc_i          : in  std_logic;
      wb_sel_i          : in  std_logic_vector(3 downto 0);
      wb_stb_i          : in  std_logic;
      wb_we_i           : in  std_logic;
      wb_ack_o          : out std_logic;
      fmc_io_stat_i     : in  std_logic_vector(31 downto 0);
      fmc_io_ctrl_1_o   : out std_logic_vector(31 downto 0);
      fmc_io_ctrl_2_o   : out std_logic_vector(31 downto 0);
      fmc_io_ctrl_3_o   : out std_logic_vector(31 downto 0);
      fmc_io_led_ctrl_o : out std_logic_vector(31 downto 0)
      );
  end component fmc_io_regs;

  component ram_2048x32
    port (
      clka  : in  std_logic;
      wea   : in  std_logic_vector(0 downto 0);
      addra : in  std_logic_vector(10 downto 0);
      dina  : in  std_logic_vector(31 downto 0);
      douta : out std_logic_vector(31 downto 0)
      );
  end component;

  component wb_spi_master
    port(
      wb_clk_i   : in  std_logic;
      wb_rst_i   : in  std_logic;
      wb_adr_i   : in  std_logic_vector(4 downto 0);
      wb_dat_i   : in  std_logic_vector(31 downto 0);
      wb_dat_o   : out std_logic_vector(31 downto 0);
      wb_sel_i   : in  std_logic_vector(3 downto 0);
      wb_stb_i   : in  std_logic;
      wb_cyc_i   : in  std_logic;
      wb_we_i    : in  std_logic;
      wb_ack_o   : out std_logic;
      wb_err_o   : out std_logic;
      wb_int_o   : out std_logic;
      ss_pad_o   : out std_logic_vector(7 downto 0);
      sclk_pad_o : out std_logic;
      mosi_pad_o : out std_logic;
      miso_pad_i : in  std_logic);
  end component;

  component ddr3_ctrl
    generic(
      g_MEMCLK_PERIOD      : integer := 3200;               -- in ps
      g_RST_ACT_LOW        : integer := 1;                  -- 1=active low
      g_INPUT_CLK_TYPE     : string  := "SINGLE_ENDED";
      g_SIMULATION         : string  := "FALSE";
      g_CALIB_SOFT_IP      : string  := "TRUE";
      g_MEM_ADDR_ORDER     : string  := "ROW_BANK_COLUMN";  -- BANK_ROW_COLUMN or ROW_BANK_COLUMN
      g_NUM_DQ_PINS        : integer := 16;
      g_MEM_ADDR_WIDTH     : integer := 14;
      g_MEM_BANKADDR_WIDTH : integer := 3;
      g_P0_MASK_SIZE       : integer := 4;
      g_P0_DATA_PORT_SIZE  : integer := 32;
      g_P1_MASK_SIZE       : integer := 4;
      g_P1_DATA_PORT_SIZE  : integer := 32
      );

    port(
      clk_i         : in    std_logic;
      rst_n_i       : in    std_logic;
      calib_done    : out   std_logic;
      ddr3_dq_b     : inout std_logic_vector(g_NUM_DQ_PINS-1 downto 0);
      ddr3_a_o      : out   std_logic_vector(g_MEM_ADDR_WIDTH-1 downto 0);
      ddr3_ba_o     : out   std_logic_vector(g_MEM_BANKADDR_WIDTH-1 downto 0);
      ddr3_ras_n_o  : out   std_logic;
      ddr3_cas_n_o  : out   std_logic;
      ddr3_we_n_o   : out   std_logic;
      ddr3_odt_o    : out   std_logic;
      ddr3_rst_n_o  : out   std_logic;
      ddr3_cke_o    : out   std_logic;
      ddr3_dm_o     : out   std_logic;
      ddr3_udm_o    : out   std_logic;
      ddr3_dqs_p_b  : inout std_logic;
      ddr3_dqs_n_b  : inout std_logic;
      ddr3_udqs_p_b : inout std_logic;
      ddr3_udqs_n_b : inout std_logic;
      ddr3_clk_p_o  : out   std_logic;
      ddr3_clk_n_o  : out   std_logic;
      ddr3_rzq_b    : inout std_logic;
      ddr3_zio_b    : inout std_logic;

      wb0_clk_i   : in  std_logic;
      wb0_sel_i   : in  std_logic_vector(g_P0_MASK_SIZE - 1 downto 0);
      wb0_cyc_i   : in  std_logic;
      wb0_stb_i   : in  std_logic;
      wb0_we_i    : in  std_logic;
      wb0_addr_i  : in  std_logic_vector(29 downto 0);
      wb0_data_i  : in  std_logic_vector(g_P0_DATA_PORT_SIZE - 1 downto 0);
      wb0_data_o  : out std_logic_vector(g_P0_DATA_PORT_SIZE - 1 downto 0);
      wb0_ack_o   : out std_logic;
      wb0_stall_o : out std_logic;

      wb1_clk_i   : in  std_logic;
      wb1_sel_i   : in  std_logic_vector(g_P0_MASK_SIZE - 1 downto 0);
      wb1_cyc_i   : in  std_logic;
      wb1_stb_i   : in  std_logic;
      wb1_we_i    : in  std_logic;
      wb1_addr_i  : in  std_logic_vector(29 downto 0);
      wb1_data_i  : in  std_logic_vector(g_P0_DATA_PORT_SIZE - 1 downto 0);
      wb1_data_o  : out std_logic_vector(g_P0_DATA_PORT_SIZE - 1 downto 0);
      wb1_ack_o   : out std_logic;
      wb1_stall_o : out std_logic
      );
  end component ddr3_ctrl;

  component pipeline_vec is
    generic (
      g_WIDTH : integer := 8;
      g_DEPTH : integer := 1
      );
    port
      (
        clk_i   : in std_logic;
        rst_n_i : in std_logic;

        data_i : in  std_logic_vector(g_WIDTH-1 downto 0);
        data_o : out std_logic_vector(g_WIDTH-1 downto 0)
        );
  end component pipeline_vec;

  ------------------------------------------------------------------------------
  -- Constants declaration
  ------------------------------------------------------------------------------
  constant c_BAR0_APERTURE         : integer := 20;
  constant c_CSR_WB_SLAVES_NB      : integer := 3;
  constant c_DMA_WB_SLAVES_NB      : integer := 1;
  constant c_DMA_WB_ADDR_WIDTH     : integer := 26;
  constant c_DMA_WB_PIPELINE_DEPTH : integer := 5;

  ------------------------------------------------------------------------------
  -- Signals declaration
  ------------------------------------------------------------------------------

  -- System clock
  signal sys_clk_in         : std_logic;
  signal sys_clk_50_buf     : std_logic;
  signal sys_clk_200_buf    : std_logic;
  signal sys_clk_300_buf    : std_logic;
  signal sys_clk_50         : std_logic;
  signal sys_clk_200        : std_logic;
  signal sys_clk_300        : std_logic;
  signal sys_clk_fb         : std_logic;
  signal sys_clk_pll_locked : std_logic;

  -- DDR3 clock
  signal ddr_clk            : std_logic;
  signal ddr_clk_buf        : std_logic;
  signal ddr_clk_fb         : std_logic;
  signal ddr_clk_pll_locked : std_logic;

  -- LCLK from GN4124 used as system clock
  signal l_clk : std_logic;

  -- P2L colck PLL status
  signal p2l_pll_locked : std_logic;

  -- Reset
  signal rst_a : std_logic;
  signal rst   : std_logic;

  -- CSR wishbone bus
  signal wb_adr     : std_logic_vector(c_BAR0_APERTURE-log2_ceil(c_CSR_WB_SLAVES_NB+1)-1 downto 0);
  signal wb_dat_i   : std_logic_vector((32*c_CSR_WB_SLAVES_NB)-1 downto 0);
  signal wb_dat_o   : std_logic_vector(31 downto 0);
  signal wb_sel     : std_logic_vector(3 downto 0);
  signal wb_cyc     : std_logic_vector(c_CSR_WB_SLAVES_NB-1 downto 0);
  signal wb_stb     : std_logic;
  signal wb_we      : std_logic;
  signal wb_ack     : std_logic_vector(c_CSR_WB_SLAVES_NB-1 downto 0);
  signal spi_wb_adr : std_logic_vector(4 downto 0);
  signal ddr_wb_adr : std_logic_vector(29 downto 0);

  -- DMA wishbone bus
  signal dma_adr            : std_logic_vector(31 downto 0);
  signal dma_dat_i          : std_logic_vector((32*c_DMA_WB_SLAVES_NB)-1 downto 0);
  signal dma_dat_o          : std_logic_vector(31 downto 0);
  signal dma_sel            : std_logic_vector(3 downto 0);
  signal dma_cyc            : std_logic;  --_vector(c_DMA_WB_SLAVES_NB-1 downto 0);
  signal dma_stb            : std_logic;
  signal dma_we             : std_logic;
  signal dma_ack            : std_logic;  --_vector(c_DMA_WB_SLAVES_NB-1 downto 0);
  signal dma_stall          : std_logic;  --_vector(c_DMA_WB_SLAVES_NB-1 downto 0);
  signal dma_adr_ddr        : std_logic_vector(31 downto 0);
  signal dma_dat_i_ddr      : std_logic_vector((32*c_DMA_WB_SLAVES_NB)-1 downto 0);
  signal dma_dat_o_ddr      : std_logic_vector(31 downto 0);
  signal dma_sel_ddr        : std_logic_vector(3 downto 0);
  signal dma_cyc_ddr        : std_logic;  --_vector(c_DMA_WB_SLAVES_NB-1 downto 0);
  signal dma_stb_ddr        : std_logic;
  signal dma_we_ddr         : std_logic;
  signal dma_ack_ddr        : std_logic;  --_vector(c_DMA_WB_SLAVES_NB-1 downto 0);
  signal dma_stall_ddr      : std_logic;  --_vector(c_DMA_WB_SLAVES_NB-1 downto 0);
  signal dma_wb_gn_to_pipe  : std_logic_vector(70 downto 0);
  signal dma_wb_pipe_to_ddr : std_logic_vector(70 downto 0);
  signal dma_wb_ddr_to_pipe : std_logic_vector(33 downto 0);
  signal dma_wb_pipe_to_gn  : std_logic_vector(33 downto 0);
  signal ram_we             : std_logic_vector(0 downto 0);
  signal ddr_dma_adr        : std_logic_vector(29 downto 0);

  -- Interrupts stuff
  signal irq_sources   : std_logic_vector(1 downto 0);
  signal irq_to_gn4124 : std_logic;

  -- CSR whisbone slaves for test
  signal fmc_io_stat     : std_logic_vector(31 downto 0);
  signal fmc_io_ctrl_1   : std_logic_vector(31 downto 0);
  signal fmc_io_ctrl_2   : std_logic_vector(31 downto 0);
  signal fmc_io_ctrl_3   : std_logic_vector(31 downto 0);
  signal fmc_io_led_ctrl : std_logic_vector(31 downto 0);

  -- SPI
  signal spi_slave_select : std_logic_vector(7 downto 0);

  -- DDR3
  signal ddr3_calib_done   : std_logic;
  signal ddr3_calib_done_t : std_logic;

  -- FOR TESTS
  signal debug       : std_logic_vector(7 downto 0);
  signal clk_div_cnt : unsigned(3 downto 0);
  signal clk_div     : std_logic;


begin

  ------------------------------------------------------------------------------
  -- System clock distribution from 25MHz TCXO
  --  50.000 MHz system clock
  -- 200.000 MHz fast system clock
  -- 300.000 MHz clock
  ------------------------------------------------------------------------------
  cmp_sysclk_inbuf : IBUFDS
    generic map (
      DIFF_TERM    => false,            -- Differential Termination
      IBUF_LOW_PWR => true,             -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "DEFAULT")
    port map (
      O  => sys_clk_in,                 -- Buffer output
      I  => sys_clk_p,                  -- Diff_p buffer input (connect directly to top-level port)
      IB => sys_clk_n                   -- Diff_n buffer input (connect directly to top-level port)
      );

  cmp_sys_clk_pll : PLL_BASE
    generic map (
      BANDWIDTH          => "OPTIMIZED",
      CLK_FEEDBACK       => "CLKFBOUT",
      COMPENSATION       => "INTERNAL",
      DIVCLK_DIVIDE      => 1,
      CLKFBOUT_MULT      => 24,
      CLKFBOUT_PHASE     => 0.000,
      CLKOUT0_DIVIDE     => 12,
      CLKOUT0_PHASE      => 0.000,
      CLKOUT0_DUTY_CYCLE => 0.500,
      CLKOUT1_DIVIDE     => 3,
      CLKOUT1_PHASE      => 0.000,
      CLKOUT1_DUTY_CYCLE => 0.500,
      CLKOUT2_DIVIDE     => 2,
      CLKOUT2_PHASE      => 0.000,
      CLKOUT2_DUTY_CYCLE => 0.500,
      CLKIN_PERIOD       => 40.0,
      REF_JITTER         => 0.016)
    port map (
      CLKFBOUT => sys_clk_fb,
      CLKOUT0  => sys_clk_50_buf,
      CLKOUT1  => sys_clk_200_buf,
      CLKOUT2  => open,                 --sys_clk_300_buf,
      CLKOUT3  => open,
      CLKOUT4  => open,
      CLKOUT5  => open,
      LOCKED   => sys_clk_pll_locked,
      RST      => '0',
      CLKFBIN  => sys_clk_fb,
      CLKIN    => sys_clk_in);

  cmp_clk_50_buf : BUFG
    port map (
      O => sys_clk_50,
      I => sys_clk_50_buf);

  cmp_clk_200_buf : BUFG
    port map (
      O => sys_clk_200,
      I => sys_clk_200_buf);

  --cmp_clk_300_buf : BUFG
  --  port map (
  --    O => sys_clk_300,
  --    I => sys_clk_300_buf);

  ------------------------------------------------------------------------------
  -- System clock distribution from 25MHz TCXO
  -- 312.500 MHz DDR3 clock
  ------------------------------------------------------------------------------
  cmp_ddr_clk_pll : PLL_BASE
    generic map (
      BANDWIDTH          => "OPTIMIZED",
      CLK_FEEDBACK       => "CLKFBOUT",
      COMPENSATION       => "INTERNAL",
      DIVCLK_DIVIDE      => 1,
      CLKFBOUT_MULT      => 25,
      CLKFBOUT_PHASE     => 0.000,
      CLKOUT0_DIVIDE     => 2,
      CLKOUT0_PHASE      => 0.000,
      CLKOUT0_DUTY_CYCLE => 0.500,
      CLKIN_PERIOD       => 40.0,
      REF_JITTER         => 0.016)
    port map (
      CLKFBOUT => ddr_clk_fb,
      CLKOUT0  => ddr_clk_buf,
      CLKOUT1  => open,
      CLKOUT2  => open,
      CLKOUT3  => open,
      CLKOUT4  => open,
      CLKOUT5  => open,
      LOCKED   => ddr_clk_pll_locked,
      RST      => '0',
      CLKFBIN  => ddr_clk_fb,
      CLKIN    => sys_clk_in);

  cmp_ddr_clk_buf : BUFG
    port map (
      O => ddr_clk,
      I => ddr_clk_buf);

  ------------------------------------------------------------------------------
  -- Local clock from gennum LCLK
  ------------------------------------------------------------------------------
  cmp_l_clk_buf : IBUFDS
    generic map (
      DIFF_TERM    => false,            -- Differential Termination
      IBUF_LOW_PWR => true,             -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "DEFAULT")
    port map (
      O  => l_clk,                      -- Buffer output
      I  => L_CLKp,                     -- Diff_p buffer input (connect directly to top-level port)
      IB => L_CLKn                      -- Diff_n buffer input (connect directly to top-level port)
      );

  ------------------------------------------------------------------------------
  -- Active high reset
  ------------------------------------------------------------------------------
  rst <= not(L_RST_N);

  ------------------------------------------------------------------------------
  -- GN4124 interface
  ------------------------------------------------------------------------------
  cmp_gn4124_core : gn4124_core
    generic map (
      g_BAR0_APERTURE     => c_BAR0_APERTURE,
      g_CSR_WB_SLAVES_NB  => c_CSR_WB_SLAVES_NB,
      g_DMA_WB_SLAVES_NB  => c_DMA_WB_SLAVES_NB,
      g_DMA_WB_ADDR_WIDTH => c_DMA_WB_ADDR_WIDTH
      )
    port map
    (
      ---------------------------------------------------------
      -- Control and status
      --
      -- Asynchronous reset from GN4124
      rst_n_a_i      => L_RST_N,
      -- P2L clock PLL locked
      p2l_pll_locked => p2l_pll_locked,
      -- Debug outputs
      debug_o        => debug,

      ---------------------------------------------------------
      -- P2L Direction
      --
      -- Source Sync DDR related signals
      p2l_clk_p_i  => P2L_CLKp,
      p2l_clk_n_i  => P2L_CLKn,
      p2l_data_i   => P2L_DATA,
      p2l_dframe_i => P2L_DFRAME,
      p2l_valid_i  => P2L_VALID,

      -- P2L Control
      p2l_rdy_o  => P2L_RDY,
      p_wr_req_i => P_WR_REQ,
      p_wr_rdy_o => P_WR_RDY,
      rx_error_o => RX_ERROR,

      ---------------------------------------------------------
      -- L2P Direction
      --
      -- Source Sync DDR related signals
      l2p_clk_p_o  => L2P_CLKp,
      l2p_clk_n_o  => L2P_CLKn,
      l2p_data_o   => L2P_DATA,
      l2p_dframe_o => L2P_DFRAME,
      l2p_valid_o  => L2P_VALID,
      l2p_edb_o    => L2P_EDB,

      -- L2P Control
      l2p_rdy_i    => L2P_RDY,
      l_wr_rdy_i   => L_WR_RDY,
      p_rd_d_rdy_i => P_RD_D_RDY,
      tx_error_i   => TX_ERROR,
      vc_rdy_i     => VC_RDY,

      ---------------------------------------------------------
      -- Interrupt interface
      dma_irq_o => irq_sources,
      irq_p_i   => irq_to_gn4124,
      irq_p_o   => GPIO(0),

      ---------------------------------------------------------
      -- Target Interface (Wishbone master)
      wb_clk_i => sys_clk_50,
      wb_adr_o => wb_adr,
      wb_dat_o => wb_dat_o,
      wb_sel_o => wb_sel,
      wb_stb_o => wb_stb,
      wb_we_o  => wb_we,
      wb_cyc_o => wb_cyc,
      wb_dat_i => wb_dat_i,
      wb_ack_i => wb_ack,

      ---------------------------------------------------------
      -- L2P DMA Interface (Pipelined Wishbone master)
      dma_clk_i   => sys_clk_50,
      dma_adr_o   => dma_adr,
      dma_dat_o   => dma_dat_o,
      dma_sel_o   => dma_sel,
      dma_stb_o   => dma_stb,
      dma_we_o    => dma_we,
      dma_cyc_o   => dma_cyc,
      dma_dat_i   => dma_dat_i,
      dma_ack_i   => dma_ack,
      dma_stall_i => dma_stall
      );

  ------------------------------------------------------------------------------
  -- CSR wishbone bus slaves
  ------------------------------------------------------------------------------

  -- SPI master
  cmp_spi_master : wb_spi_master
    port map (
      wb_clk_i   => sys_clk_50,
      wb_rst_i   => rst,
      wb_adr_i   => spi_wb_adr,
      wb_dat_i   => wb_dat_o,
      wb_dat_o   => wb_dat_i(31 downto 0),
      wb_sel_i   => wb_sel,
      wb_stb_i   => wb_stb,
      wb_cyc_i   => wb_cyc(0),
      wb_we_i    => wb_we,
      wb_ack_o   => wb_ack(0),
      wb_err_o   => open,
      wb_int_o   => open,
      ss_pad_o   => spi_slave_select,
      sclk_pad_o => SCLK_V_MON_P1V8,
      mosi_pad_o => DIN_V_MON,
      miso_pad_i => '0');

  CSVR_V_MON_P1V8 <= spi_slave_select(0);

  -- 32-bit word to byte address
  spi_wb_adr <= wb_adr(2 downto 0) & "00";

  -- Control and status registers
  cmp_fmc_io_regs : fmc_io_regs
    port map(
      rst_n_i           => L_RST_N,
      wb_clk_i          => sys_clk_50,
      wb_addr_i         => wb_adr(2 downto 0),
      wb_data_i         => wb_dat_o,
      wb_data_o         => wb_dat_i(63 downto 32),
      wb_cyc_i          => wb_cyc(1),
      wb_sel_i          => wb_sel,
      wb_stb_i          => wb_stb,
      wb_we_i           => wb_we,
      wb_ack_o          => wb_ack(1),
      fmc_io_stat_i     => fmc_io_stat,
      fmc_io_ctrl_1_o   => fmc_io_ctrl_1,
      fmc_io_ctrl_2_o   => fmc_io_ctrl_2,
      fmc_io_ctrl_3_o   => fmc_io_ctrl_3,
      fmc_io_led_ctrl_o => fmc_io_led_ctrl
      );

  fmc_io_stat <= X"DEAD0"
                 & ddr3_calib_done
                 & sys_clk_pll_locked
                 & LA01_P
                 & LA10_P
                 & CLK1_M2C_P
                 & CLK1_M2C_N
                 & CLK0_M2C_P
                 & CLK0_M2C_N
                 & TDO_FROM_FMC_P1V8
                 & M2C_DIR
                 & PRSNT_M2C_L
                 & p2l_pll_locked;

  LED_RED   <= fmc_io_led_ctrl(0);
  LED_GREEN <= fmc_io_led_ctrl(1);
  PG_C2M    <= fmc_io_led_ctrl(2);
  LA10_N    <= fmc_io_led_ctrl(3);

  FPGA_SDA        <= 'Z';
  FPGA_SCL        <= '0';
  TMS_TO_FMC_P1V8 <= '1';
  TDI_TO_FMC_P1V8 <= '1';
  TCK_TO_FMC_P1V8 <= '1';

  ENABLE_VADJ <= fmc_io_ctrl_3(0);

  LA33_P <= fmc_io_ctrl_2(30);
  LA33_N <= fmc_io_ctrl_2(29);
  LA32_P <= fmc_io_ctrl_2(28);
  LA32_N <= fmc_io_ctrl_2(27);
  LA31_P <= fmc_io_ctrl_2(26);
  LA31_N <= fmc_io_ctrl_2(25);
  LA30_P <= fmc_io_ctrl_2(24);
  LA30_N <= fmc_io_ctrl_2(23);
  LA29_P <= fmc_io_ctrl_2(22);
  LA29_N <= fmc_io_ctrl_2(21);
  LA28_P <= fmc_io_ctrl_2(20);
  LA28_N <= fmc_io_ctrl_2(19);
  LA27_P <= fmc_io_ctrl_2(18);
  LA27_N <= fmc_io_ctrl_2(17);
  LA26_P <= fmc_io_ctrl_2(16);
  LA26_N <= fmc_io_ctrl_2(15);
  LA25_P <= fmc_io_ctrl_2(14);
  LA25_N <= fmc_io_ctrl_2(13);
  LA24_P <= fmc_io_ctrl_2(12);
  LA24_N <= fmc_io_ctrl_2(11);
  LA23_P <= fmc_io_ctrl_2(10);
  LA23_N <= fmc_io_ctrl_2(9);
  LA22_P <= fmc_io_ctrl_2(8);
  LA22_N <= fmc_io_ctrl_2(7);
  LA21_P <= fmc_io_ctrl_2(6);
  LA21_N <= fmc_io_ctrl_2(5);
  LA20_P <= fmc_io_ctrl_2(4);
  LA20_N <= fmc_io_ctrl_2(3);
  LA19_P <= fmc_io_ctrl_2(2);
  LA19_N <= fmc_io_ctrl_2(1);
  LA18_P <= fmc_io_ctrl_2(0);
  LA18_N <= fmc_io_ctrl_2(31);
  LA17_P <= fmc_io_ctrl_2(30);
  LA17_N <= fmc_io_ctrl_2(29);
  LA16_P <= fmc_io_ctrl_2(28);
  LA16_N <= fmc_io_ctrl_2(27);
  LA15_P <= fmc_io_ctrl_1(26);
  LA15_N <= fmc_io_ctrl_1(25);
  LA14_P <= fmc_io_ctrl_1(24);
  LA14_N <= fmc_io_ctrl_1(23);
  LA13_P <= fmc_io_ctrl_1(22);
  LA13_N <= fmc_io_ctrl_1(21);
  LA12_P <= fmc_io_ctrl_1(20);
  LA12_N <= fmc_io_ctrl_1(19);
  LA11_P <= fmc_io_ctrl_1(18);
  LA11_N <= fmc_io_ctrl_1(17);
  --Note: LA10_P is an input on FMC digital IO V1
  --Note: LA10_N is controlled by the fmc_io_led_ctrl register
  LA09_P <= fmc_io_ctrl_1(16);
  LA09_N <= fmc_io_ctrl_1(15);
  LA08_P <= fmc_io_ctrl_1(14);
  LA08_N <= fmc_io_ctrl_1(13);
  LA07_P <= fmc_io_ctrl_1(12);
  LA07_N <= fmc_io_ctrl_1(11);
  LA06_P <= fmc_io_ctrl_1(10);
  LA06_N <= fmc_io_ctrl_1(9);
  LA05_P <= fmc_io_ctrl_1(8);
  LA05_N <= fmc_io_ctrl_1(7);
  LA04_P <= fmc_io_ctrl_1(6);
  LA04_N <= fmc_io_ctrl_1(5);
  LA03_P <= '0';                        --Note: IO buffer DIR control, must be set as output to avoid conflict with FPGA IO
  LA03_N <= '0';                        --Note: IO buffer DIR control, must be set as output to avoid conflict with FPGA IO
  LA02_P <= fmc_io_ctrl_1(4);
  LA02_N <= fmc_io_ctrl_1(3);
  --Note: LA01_P is an input on FMC digital IO V1
  LA01_N <= fmc_io_ctrl_1(2);
  LA00_P <= fmc_io_ctrl_1(1);
  LA00_N <= fmc_io_ctrl_1(0);

  ------------------------------------------------------------------------------
  -- DMA wishbone bus connected to a DDR3 controller
  ------------------------------------------------------------------------------
  cmp_ddr_ctrl : ddr3_ctrl
    generic map(
      g_MEMCLK_PERIOD => 3200,
      g_SIMULATION    => g_SIMULATION,
      g_CALIB_SOFT_IP => g_CALIB_SOFT_IP
      )
    port map (
      clk_i   => ddr_clk,
      rst_n_i => L_RST_N,

      calib_done => ddr3_calib_done_t,

      ddr3_dq_b     => ddr3_dq_b,
      ddr3_a_o      => ddr3_a_o,
      ddr3_ba_o     => ddr3_ba_o,
      ddr3_ras_n_o  => ddr3_ras_n_o,
      ddr3_cas_n_o  => ddr3_cas_n_o,
      ddr3_we_n_o   => ddr3_we_n_o,
      ddr3_odt_o    => ddr3_odt_o,
      ddr3_rst_n_o  => ddr3_rst_n_o,
      ddr3_cke_o    => ddr3_cke_o,
      ddr3_dm_o     => ddr3_dm_o,
      ddr3_udm_o    => ddr3_udm_o,
      ddr3_dqs_p_b  => ddr3_dqs_p_b,
      ddr3_dqs_n_b  => ddr3_dqs_n_b,
      ddr3_udqs_p_b => ddr3_udqs_p_b,
      ddr3_udqs_n_b => ddr3_udqs_n_b,
      ddr3_clk_p_o  => ddr3_clk_p_o,
      ddr3_clk_n_o  => ddr3_clk_n_o,
      ddr3_rzq_b    => ddr3_rzq_b,
      ddr3_zio_b    => ddr3_zio_b,

      wb0_clk_i   => sys_clk_50,              --'0',
      wb0_sel_i   => "1111",
      wb0_cyc_i   => wb_cyc(2),               --'0',
      wb0_stb_i   => wb_stb,                  --'0',
      wb0_we_i    => wb_we,                   --'0',
      wb0_addr_i  => ddr_wb_adr,              --X"0000000" & "00",
      wb0_data_i  => wb_dat_o,                --X"00000000",
      wb0_data_o  => wb_dat_i(95 downto 64),  --open,
      wb0_ack_o   => wb_ack(2),               --open,
      wb0_stall_o => open,

      wb1_clk_i   => sys_clk_50,
      wb1_sel_i   => "1111",
      wb1_cyc_i   => dma_cyc_ddr,
      wb1_stb_i   => dma_stb_ddr,
      wb1_we_i    => dma_we_ddr,
      wb1_addr_i  => ddr_dma_adr,
      wb1_data_i  => dma_dat_o_ddr,
      wb1_data_o  => dma_dat_i_ddr,
      wb1_ack_o   => dma_ack_ddr,
      wb1_stall_o => dma_stall_ddr);

  -- 32-bit word to byte address
  ddr_wb_adr <= "0000000000" & wb_adr & "00";

  -- 32-bit word to byte address
  ddr_dma_adr <= dma_adr_ddr(27 downto 0) & "00";

  process (sys_clk_50)
  begin
    if rising_edge(sys_clk_50) then
      ddr3_calib_done <= ddr3_calib_done_t;
    end if;
  end process;

  -- Intermediate signals foreseen to pipeline the wishbone bus
  dma_wb_gn_to_pipe <= dma_sel & dma_cyc & dma_stb & dma_we & dma_adr & dma_dat_o;
  dma_dat_o_ddr     <= dma_wb_pipe_to_ddr(31 downto 0);
  dma_adr_ddr       <= dma_wb_pipe_to_ddr(63 downto 32);
  dma_we_ddr        <= dma_wb_pipe_to_ddr(64);
  dma_stb_ddr       <= dma_wb_pipe_to_ddr(65);
  dma_cyc_ddr       <= dma_wb_pipe_to_ddr(66);
  dma_sel_ddr       <= dma_wb_pipe_to_ddr(70 downto 67);

  dma_wb_pipe_to_ddr <= dma_wb_gn_to_pipe;

  dma_wb_ddr_to_pipe <= dma_dat_i_ddr & dma_ack_ddr & dma_stall_ddr;
  dma_stall          <= dma_wb_pipe_to_gn(0);
  dma_ack            <= dma_wb_pipe_to_gn(1);
  dma_dat_i          <= dma_wb_pipe_to_gn(33 downto 2);

  dma_wb_pipe_to_gn <= dma_wb_ddr_to_pipe;


------------------------------------------------------------------------------
-- Interrupt stuff
------------------------------------------------------------------------------
-- just forward irq pulses for test
  irq_to_gn4124 <= irq_sources(1) or irq_sources(0);


------------------------------------------------------------------------------
-- FOR TEST
------------------------------------------------------------------------------
  p_div_clk : process (l_clk, L_RST_N)
  begin
    if L_RST_N = '0' then
      clk_div     <= '0';
      clk_div_cnt <= (others => '0');
    elsif rising_edge(l_clk) then
      if clk_div_cnt = 4 then
        clk_div     <= not (clk_div);
        clk_div_cnt <= (others => '0');
      else
        clk_div_cnt <= clk_div_cnt + 1;
      end if;
    end if;
  end process p_div_clk;


  USER_IO_0_P <= clk_div;
  USER_IO_0_N <= '0';
  USER_IO_1_P <= '0';
  USER_IO_1_N <= '0';

------------------------------------------------------------------------------
-- Assign unused outputs
------------------------------------------------------------------------------
  GPIO(1) <= '0';


end rtl;


