#!   /usr/bin/env	python
#    coding: utf8

import sys
import rr
import random
import time

if __name__ == '__main__':

    #======================================================================
    # Functions definition
    #======================================================================

    def check_host_addr(gennum, host_l_wr, nhost_l_wr):
        host_l_rd = gennum.iread(0, 0xC, 4)
        host_h_rd = gennum.iread(0, 0x10, 4)
        nhost_l_rd = gennum.iread(0, 0x18, 4)
        nhost_h_rd = gennum.iread(0, 0x1C, 4)
        error = 0
        if host_l_rd != host_l_wr:
            print 'host addr low = %.8X should be %.8X' % (host_l_rd, host_l_wr)
            error += 1
        if host_h_rd != 0:
            print 'host addr high = %.8X should be 0x00000000' % host_h_rd
            error += 1
        if nhost_l_rd != nhost_l_wr:
            print 'next item host addr low = %.8X should be %.8X' % (nhost_l_rd, nhost_l_wr)
            error += 1
        if nhost_h_rd != 0:
            print 'next item host addr high = %.8X should be 0x00000000' % nhost_h_rd
            error += 1
        if error != 0:
            print 'BAD HOST ADDRESS'
            return error
        else :
            print 'Host addresses in DMA controller OK'
            return error

    def set_local_bus_freq(gennum, freq):
        # freq in MHz
        # LCLK = (25MHz*(DIVFB+1))/(DIVOT+1)
        # DIVFB = 31
        # DIVOT = (800/LCLK)-1
        divot = int(round((800/freq)-1,0))
        #print '%d' % divot
        data = 0xe001f00c + (divot << 4)
        #print '%.8X' % data
        print 'Set local bus freq to %dMHz' % int(round(800/(divot+1),0))
        return gennum.iwrite(4, 0x808, 4, data)

    def gpio_interrupt_config(gennum):
        print 'Configure Gennum to generate interrupts (MSI) from GPIO 8'
        gennum.iwrite(4, 0x0A04, 4, 0x00000100) # set GPIO8 as input
        gennum.iwrite(4, 0x0A1C, 4, 0x0000FEFF) # set GPIO interrupt mask
        gennum.iwrite(4, 0x0A18, 4, 0x00000100) # set GPIO interrupt mask
        gennum.iwrite(4, 0x0A28, 4, 0x00000100) # set GPIO8 polarity to rising edge
        gennum.iwrite(4, 0x0820, 4, 0x00008000) # enable GPIO interrupt on INT0 line
        return 0

    def first_dma_item(gennum, dma_carrier_addr, dma_host_addr,
                     dma_length, dma_next_item_addr, dma_dir, dma_last):
        # write the first DMA item in the carrier DMA config registers
        # Carrier address and dma length are in 32-bit word
        # Host addresses are in byte
        # dma_dir  = 1 -> PCIe to carrier
        # dma_dir  = 0 -> carrier to PCIe
        # dma_last = 1 -> last item in the transfer
        # dma_last = 0 -> more item in the transfer
        gennum.iwrite(0, 0x8, 4, (dma_carrier_addr << 2))    # Start address in the carrier
        gennum.iwrite(0, 0xc, 4, dma_host_addr)	             # Start address (low) in the host
        gennum.iwrite(0, 0x10, 4, 0x0)	                     # Start address (high) in the host
        gennum.iwrite(0, 0x14, 4, (dma_length << 2))	     # Length
        gennum.iwrite(0, 0x18, 4, dma_next_item_addr)	     # Address (low) of the next item in the host
        gennum.iwrite(0, 0x1C, 4, 0x0)	                     # Address (high) of the next item in the host
        gennum.iwrite(0, 0x20, 4, (dma_dir << 1) + dma_last) # Control of the DMA chain
        return 0

    def new_dma_item(gennum, item_host_addr, dma_carrier_addr, dma_host_addr,
                     dma_length, dma_next_item_addr, dma_dir, dma_last):
        # write a new DMA item in host memory
        # Carrier address and dma length are in 32-bit word
        # Host addresses are in byte
        # dma_dir  = 1 -> PCIe to carrier
        # dma_dir  = 0 -> carrier to PCIe
        # dma_last = 1 -> last item in the transfer
        # dma_last = 0 -> more item in the transfer
        gennum.iwrite(0xc, item_host_addr, 4, (dma_carrier_addr << 2))	# Start address in the carrier
        gennum.iwrite(0xc, item_host_addr + 0x4, 4, dma_host_addr)	# Start address (low) in the host
        gennum.iwrite(0xc, item_host_addr + 0x8, 4, 0x0)	# Start address (high) in the host
        gennum.iwrite(0xc, item_host_addr + 0xc, 4, (dma_length << 2))	# Length
        gennum.iwrite(0xc, item_host_addr + 0x10, 4, dma_next_item_addr)	# Address low of the next item in the host
        gennum.iwrite(0xc, item_host_addr + 0x14, 4, 0x0)	# Address high of the next item in the host
        gennum.iwrite(0xc, item_host_addr + 0x18, 4, (dma_dir << 1) + dma_last) # Control of the DMA chain
        return 0

    def print_host_page(gennum, page, size):
        # size in 32-bit words
        print 'Page %d:' % page
        for num in xrange(0, size*4, 4):
            print '%.8X' % gennum.iread(0Xc, (int(page) << 12) + num, 4)
        return 0

    def print_dma_ctrl_config(gennum):
        print 'DMA controller status and config:'
        for num in xrange(0, 9*4, 4):
            print '%.8X' % gennum.iread(0, num, 4)
        return 0

    def dma_ctrl_status(gennum):
        status = gennum.iread(0, 0x4, 4)
        if status == 0 :
            print 'DMA controller status is IDLE'
        elif status == 1 :
            print 'DMA controller status is DONE'
        elif status == 2 :
            print 'DMA controller status is BUSY'
        elif status == 3 :
            print 'DMA controller status is ERROR'
        elif status == 4 :
            print 'DMA controller status is ABORTED'
        else :
            print 'DMA controller status is !! UNDEFINED !!'
        return 0

    def read_interrupt_status(gennum):
        int_stat = gennum.iread(4, 0x814, 4)
        gpio_stat = gennum.iread(4, 0xA20, 4)
        #print 'INT status  : %.8X' %  int_stat
        #print 'GPIO status : %.8X' %  gpio_stat
        return 0

    def wait_end_dma(gennum):
        #print 'Wait for end DMA interrupt'
        gennum.irqwait()
        #print 'INTERRUPT RECEIVED'
        read_interrupt_status(card)
        read_interrupt_status(card)

    def data_compare(gennum, length):
        errors = 0
        for num in xrange(0, length, 1):
            wr = gennum.iread(0xC, 0x1000 + (num << 2), 4)
            rd = gennum.iread(0xC, 0x2000 + (num << 2), 4)
            if wr != rd:
                print 'wr:%.8X rd:%.8X' %(wr,rd)
                errors += 1
        return errors

    def test_main(gennum, test_nb):
        carrier_addr = random.randint(0, 0x3FFFFFFF)
        dma_length = random.randint(0, 0x10)
        for addr in xrange(0,dma_length,1):
            wdata = random.randint(0, 0xFFFFFFFF)
            gennum.iwrite(0xc, 0x1000 + (addr << 2), 4, wdata)
        for addr in xrange(0,dma_length,1):
            gennum.iwrite(0xc, 0x2000 + (addr << 2), 4, 0x0)
        first_dma_item(gennum, carrier_addr, address1, dma_length, address0, 1, 1)
        new_dma_item(gennum, 0x0, carrier_addr, address2, dma_length, 0, 0, 0)
        card.iwrite(0, 0x0, 4, 1)
        #dma_ctrl_status(gennum)
        wait_end_dma(gennum)
        err = data_compare(gennum, dma_length)
        if err == 0 :
            print 'Test No: %5d Carrier start address (32-bit words): %.8X DMA length (32-bit words): 0x%.4X Result: OK' \
                  %(test_nb, carrier_addr, dma_length)
        else :
            print 'Test No: %5d Carrier start address (32-bit words): %.8X DMA length (32-bit words): 0x%.4X (%4d) Result: %d ERROR(S)' \
                  %(test_nb, carrier_addr, dma_length, dma_length, err)
            sys.exit(0)


    # bind to the Gennum kit
    card = rr.Gennum()

    # Find the physical addresses of the three first pages of the buffer
    pages = card.getplist()         # get page list
    pages = pages[:3]               # list slicing: get only 0, 1, 2 page
    pages = [ addr << 12 for addr in pages ]    # shift by 12 bit

    print hex(pages[0])
    print hex(pages[1])
    print hex(pages[2])

    address0 = pages[0]
    address1 = pages[1]
    address2 = pages[2]

    print '### Configuration ###'

    # Change local bus frequency to 100MHz
    set_local_bus_freq(card, 100)

    print 'Configure VADJ at 2.5V.'
    card.iwrite(0, 0x00040010, 4, 0x2418) # Configure SPI transfer
    print 'CTRL = %.8X' % card.iread(0, 0x00040010, 4)
    card.iwrite(0, 0x00040014, 4, 0x00000013) # Set SPI clock to 5MHz
    print 'DIVIDER = %.8X' % card.iread(0, 0x00040014, 4)
    card.iwrite(0, 0x00040000, 4, 0x001940) # Set digital potentiometer to 1.056 kohm (VADJ = 2.5V)
    print 'Tx0 = %.8X' % card.iread(0, 0x00040000, 4)
    card.iwrite(0, 0x00040018, 4, 0x00000001) # Select digital potentiometer
    print 'SS = %.8X' % card.iread(0, 0x00040018, 4)
    card.iwrite(0, 0x00040010, 4, 0x2518) # Send config to digital potentiometer
    while 1:
        time.sleep(.1)
        ctrl = card.iread(0, 0x00040010, 4)
        print 'CTRL = %.8X' % ctrl
        if (ctrl & 0x100) == 0:
            break

    print 'Enables VADJ'
    card.iwrite(0, 0x0008000C, 4, 0x1) # Enable VADJ switching regulator

    # Registers
    print 'Status register 1 = %.8X' % card.iread(0, 0x00080000, 4)


    for i in range(1,3):
        card.iwrite(0, 0x00080010, 4, 0x00000003)
        time.sleep(.5)
        card.iwrite(0, 0x00080010, 4, 0x00000000)
        time.sleep(.5)
        print 'LED blink %d' %i

    # Gennum config for interrupt generation from GPIO
    gpio_interrupt_config(card)

    card.irqena()

    print'Start test loop'
    test_nb = 1
    for i in range(1, 100):
    #while 1:
        test_main(card,test_nb)
        test_nb += 1


    """
    # Assign random carrier address and dma length
    carrier_addr = random.randint(0, 0x3FFFFFFF)
    dma_length = random.randint(1, 0x10)
    #print 'Carrier start address (32-bit): %.8X' % carrier_addr
    #print 'DMA length (in 32-bit words)  : %8d' % dma_length

    # Write random data in host memory to be catched by DMA engine
    for addr in xrange(0,dma_length,1):
        wdata = random.randint(0, 0xFFFFFFFF)
        card.iwrite(0xc, 0x1000 + (addr << 2), 4, wdata)

    # Clear page 3 (page that will receive data from the board)
    #print 'Clear page 3'
    for addr in xrange(0,dma_length,1):
        card.iwrite(0xc, 0x2000 + (addr << 2), 4, 0x0)

    #dma_ctrl_status(card)

    # Configure the first transfer in the DMA controler
    first_dma_item(card, carrier_addr, address1, dma_length, address0, 1, 1)
    #print_dma_ctrl_config(card)
    # Write a new item of the DMA chain in the first page of host memory
    new_dma_item(card, 0x0, carrier_addr, address2, dma_length, 0, 0, 0)
    #print_host_page(card, 0, 7)

    # Clear error interrupt sources
    #card.iwrite(4, 0x814, 4, 0x6000)

    read_interrupt_status(card)

    #print 'Enable interrupts'
    card.irqena()

    #check_host_addr(card, address1, address0)

    #print 'Starting transfer'
    card.iwrite(0, 0x0, 4, 1)

    wait_end_dma(card)

    #dma_ctrl_status(card)
    #check_host_addr(card, address2, 0)

    err = data_compare(card, dma_length)
    if err == 0 :
        print 'Test No: %8d Carrier start address (32-bit words): %.8X DMA length (32-bit words): %.4X Result: OK' \
        %(test_nb, carrier_addr, dma_length)
    else :
        print 'Test No: %8d Carrier start address (32-bit words): %.8X DMA length (32-bit words): %.4X Result: %d ERROR(S)' \
        %(test_nb, carrier_addr, dma_length, err)
        sys.exit(0)

    #print_host_page(card, 0, 7)
    #print_host_page(card, 1, dma_length)
    #print_host_page(card, 2, dma_length)
    """
