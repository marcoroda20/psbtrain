#!   /usr/bin/env	python
#    coding: utf8

import sys
import rr
import time
import random

if __name__ == '__main__':

    # bind to the Gennum kit
    card = rr.Gennum()

    # Change local bus frequency to 100MHz
    print 'Set local bus freq to 100MHz'
    card.iwrite(4, 0x0808, 4, 0xE001F07C)

    print 'Configure VADJ at 2.5V.'
    card.iwrite(0, 0x00040010, 4, 0x2418) # Configure SPI transfer
    print 'CTRL = %.8X' % card.iread(0, 0x00040010, 4)
    card.iwrite(0, 0x00040014, 4, 0x00000013) # Set SPI clock to 5MHz
    print 'DIVIDER = %.8X' % card.iread(0, 0x00040014, 4)
    card.iwrite(0, 0x00040000, 4, 0x001940) # Set digital potentiometer to 1.056 kohm (VADJ = 2.5V)
    print 'Tx0 = %.8X' % card.iread(0, 0x00040000, 4)
    card.iwrite(0, 0x00040018, 4, 0x00000001) # Select digital potentiometer
    print 'SS = %.8X' % card.iread(0, 0x00040018, 4)
    card.iwrite(0, 0x00040010, 4, 0x2518) # Send config to digital potentiometer
    while 1:
        time.sleep(.1)
        ctrl = card.iread(0, 0x00040010, 4)
        print 'CTRL = %.8X' % ctrl
        if (ctrl & 0x100) == 0:
            break

    print 'Enable VADJ'
    card.iwrite(0, 0x0008000C, 4, 0x1) # Enable VADJ switching regulator

    """
    # GN4124 BAR0 config
    print 'PCI_BAR0_LOW   = %.8X' % card.iread(4, 0x10, 4)
    print 'PCI_BAR0_HIGH  = %.8X' % card.iread(4, 0x14, 4)
    print 'PCI_BAR_CONFIG = %.8X' % card.iread(4, 0x80C, 4)
    """

    # Registers
    print 'Status register 1 = %.8X' % card.iread(0, 0x00080000, 4)


    for i in range(1,3):
        card.iwrite(0, 0x00080010, 4, 0x00000003)
        time.sleep(.5)
        card.iwrite(0, 0x00080010, 4, 0x00000000)
        time.sleep(.5)
        print 'LED blink %d' %i


    ddr_data_wr = random.randint(0, 0xFFFFFFFF)#0xDEADC0DE
    ddr_base = 0x000C0000
    ddr_offset = 0x0000FFFC & random.randint(0, 0x0000FFFF)
    ddr_addr = ddr_base + ddr_offset

    card.iwrite(0, ddr_addr, 4, ddr_data_wr)
    print 'WRITE %.8X at %.8X' %(ddr_data_wr, ddr_addr)
    time.sleep(.1)
    ddr_data_rd = card.iread(0, ddr_addr, 4)
    print 'READ %.8X from %.8X' %(ddr_data_rd, ddr_addr)
    if ddr_data_wr == ddr_data_rd:
        print 'OK'
    else:
        print 'ERROR'

    sys.exit()
