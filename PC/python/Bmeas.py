#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
ZetCode PyQt4 tutorial 

In this example, we determine the event sender
object.

author: Jan Bodnar
website: zetcode.com 
last edited: October 2011
"""

import sys
#from PyQt4 import QtGui, QtCore
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
from math import sin, pi
import fcntl
import struct
import ctypes
import operator
import os.path
import time

class Example(QtGui.QMainWindow):
    
    def __init__(self):
        super(Example, self).__init__()
        
        self.initUI()
        
    def __del__(self):
        self.bmeas.stop_dma(self.bus) # stop DMA

    def initUI(self):      

        win = pg.GraphicsWindow(title="Value of DMA from Bmeas driver")
        win.resize(1000,600)
        win.setWindowTitle('Bmeas GUI')
        win.setWindowIcon(QtGui.QIcon('python_icon.png')) 

        # Enable antialiasing for prettier plots
        pg.setConfigOptions(antialias=True)
        
        self.offset = 2
        self.DMAType = 1
        # Create some widgets to be placed inside
        btnChannel = QtGui.QPushButton('Channel')
        btnFileWrite = QtGui.QPushButton('Write File')
        btnShowC0 = QtGui.QPushButton('Show/Hide C0')
        self.textOffset = QtGui.QLineEdit('Channel [' + str(self.offset) + ']')
        self.textOffset.setReadOnly(True)
        btnDMA = QtGui.QPushButton('DMA type')
        self.textDMA = QtGui.QLineEdit('DMA type : B WR values')
        self.textDMA.setReadOnly(True)
        self.filename = QtGui.QLineEdit('Signals')
        self.nMeas = QtGui.QLineEdit('1')
        self.comboTime = QtGui.QComboBox()
	self.comboTime.addItems (('30','15'))
	
	self.comboTime.currentIndexChanged.connect(self.currentIndexChanged)

        self.plot = pg.PlotWidget()
        btnChannel.clicked.connect(self.buttonChannelClicked)               
        btnDMA.clicked.connect(self.buttonDMAClicked)               
        btnFileWrite.clicked.connect(self.buttonFileWriteClicked)
        btnShowC0.clicked.connect(self.buttonShowC0Clicked)        

        ## Create a grid layout to manage the widgets size and position
        layout = QtGui.QGridLayout()
        win.setLayout(layout)

        ## Add widgets to the layout in their proper positions
        layout.addWidget(btnChannel, 0, 0)     # button for channel
        layout.addWidget(self.textOffset, 1, 0)# text edit for channel 
        layout.addWidget(btnDMA, 2, 0)         # button for DMA type choice
        layout.addWidget(self.textDMA, 3, 0)   # text edit for DMA type
        layout.addWidget(self.filename, 4, 0)  # text edit for filename
        layout.addWidget(btnFileWrite, 5, 0)   # button goes in upper-left
        layout.addWidget(self.nMeas, 6, 0)     # number of measurement written when we click btnFileWrite
        layout.addWidget(btnShowC0, 7, 0)      # button to show or hide the C0
        layout.addWidget(self.comboTime, 8, 0) # tab selection for time
        layout.addWidget(self.plot, 0, 1, 10, 1)     # plot goes on right side, spanning 3 rows
        layout.setColumnStretch(1, 2)         
        
        self.plot.resize(1000,600)
        mypen=pg.mkPen('y',width=1)
        self.curve = self.plot.plot(pen=mypen)
        self.curve2 = self.plot.plot(pen='r')
        self.plot.setLabel('left', "B", units='T')
        self.plot.setLabel('bottom', "t", units='s')
        self.plot.showGrid(x=1, y=1, alpha=None)

        self.setCentralWidget(win)
        self.nFile = 1
        self.nMeasCounter = 0
        
        # Configure Bmeas to be able to read the DMA
        self.bmeas = ctypes.CDLL("/home/mroda/psbtrain/PC/spec-bmeas-sw/tools/bmeas_python_wrapper.so")
	self.bus=5 # fmc-0500 => integral
        self.dma_tag = self.bmeas.dma_tag(self.bus)
        data_0300 = self.bmeas.read_reg(self.bus,0x5000);
        self.bmeas.start_dma(self.bus) # start DMA
        dma_range = self.bmeas.write_reg(self.bus,0x500c,0x6ddd0);
	self.bmeas.write_reg(self.bus,0x5000,data_0300 | 0x100000);
        n_bytes = (900000*4*4)
        self.last_cycle_pointer = np.ones(n_bytes, dtype=np.int32)
        self.last_sample_address = np.ones(1, dtype=np.int32)

        # Plot cycle
        self.t_plot_max=30
        self.fe = 10e3
        self.t = np.arange(-1*self.t_plot_max,0,1.0/self.fe)
        #self.bmeas.write_reg(self.bus,0x5008,20);
        self.len_signal = len(self.t)
        self.signal = np.zeros(self.len_signal, dtype=np.double)
        self.C0Show=None

        self.B = np.zeros(self.len_signal, dtype=np.double)
        self.scalling_B = 10e-9
        self.offset_B = 2
        self.Bdot = np.zeros(self.len_signal, dtype=np.double)
        self.scalling_Bdot = 1.0e-6
        self.offset_Bdot = 3
        self.ctrl_1 = np.zeros(self.len_signal, dtype=np.uint32)
        self.offset_ctrl_1 = 0
        self.ctrl_2 = np.zeros(self.len_signal, dtype=np.uint32)
        self.scalling_ctrl = 1
        self.offset_ctrl_2 = 1
 
        self.C0 = np.zeros(self.len_signal, dtype=np.double)
        self.ZeroCycle = np.zeros(self.len_signal, dtype=np.double)

        self.statusBar()
        
        self.setGeometry(300, 300, 1600, 800)
        self.setWindowTitle('Bmeas GUI')
        self.show()
    
    def currentIndexChanged(self):
        self.t_plot_max=int(self.comboTime.currentText())
        self.t = np.arange(-1*self.t_plot_max,0,1.0/self.fe)
        #self.bmeas.write_reg(self.bus,0x5008,20);
        self.len_signal = len(self.t)
        self.signal = np.zeros(self.len_signal, dtype=np.double)

        self.B = np.zeros(self.len_signal, dtype=np.double)
        self.scalling_B = 10e-9
        self.offset_B = 2
        self.Bdot = np.zeros(self.len_signal, dtype=np.double)
        self.scalling_Bdot = 1.0e-6
        self.offset_Bdot = 3
        self.ctrl_1 = np.zeros(self.len_signal, dtype=np.uint32)
        self.offset_ctrl_1 = 0
        self.ctrl_2 = np.zeros(self.len_signal, dtype=np.uint32)
        self.scalling_ctrl = 1
        self.offset_ctrl_2 = 1
 
        self.C0 = np.zeros(self.len_signal, dtype=np.double)
        self.ZeroCycle = np.zeros(self.len_signal, dtype=np.double)
            
    def buttonChannelClicked(self):
        self.signal = np.zeros(self.len_signal, dtype=np.double)

        self.B = np.zeros(self.len_signal, dtype=np.double)
        self.Bdot = np.zeros(self.len_signal, dtype=np.double)
        self.ctrl_1 = np.zeros(self.len_signal, dtype=np.uint32)
        self.ctrl_2 = np.zeros(self.len_signal, dtype=np.uint32)
        if self.offset < 3:
            self.offset = self.offset + 1
        else:
            self.offset = 0
        self.textOffset.setText('Channel [' + str(self.offset) + ']')

    def buttonDMAClicked(self):
        #self.signal = np.zeros(self.len_signal, dtype=np.double)

        if self.DMAType < 3:
          self.DMAType = self.DMAType + 1
        else:
          self.DMAType = 0
	
        data = self.bmeas.read_reg(self.bus,0x5000) & 0xffcfffff;
        if self.DMAType == 0:
	  self.textDMA.setText('DMA type : B WR values')
          data = data | 0x00100000;
        elif self.DMAType == 1:
	  self.textDMA.setText('DMA type : B local values')
          data = data | 0x00000000;
        elif self.DMAType == 2:
	  self.textDMA.setText('DMA type : B side by side values')
          data = data | 0x00300000;
        else:
	  self.textDMA.setText('DMA type : ADC values')
          data = data | 0x00200000;
        self.bmeas.write_reg(self.bus,0x5000,data);
    
    def buttonShowC0Clicked(self):
	if self.C0Show==None:
          self.C0Show='r'
        else:
          self.C0Show=None

    def buttonFileWriteClicked(self):
	filename = self.filename.text()
        if self.nMeasCounter==0:
          self.nMeasCounter = int(self.nMeas.text())
	self.timeStamp = time.time();
	self.nFile=1
	while os.path.isfile(os.getcwd()+'/'+filename+'_'+str(self.nFile).zfill(4)+'.txt'):
	  self.nFile=self.nFile+1
	with file(os.getcwd()+'/'+filename+'_'+str(self.nFile).zfill(4)+'.txt', 'w') as outfile:
	  reg_5000 = self.bmeas.read_reg(self.bus,0x5000);
	  outfile.write('#Register 0x5000 : '+hex(reg_5000)+'\n')
	  outfile.write('# B.int32[10nT] Bdot.int32[1uT/s] ctrl1 ctrl2 \n')
	  write_data=np.array([self.B,self.Bdot,self.ctrl_1,self.ctrl_2])
	  np.savetxt(outfile, write_data.transpose())
	  print os.getcwd()+'/'+filename+'_'+str(self.nFile).zfill(4)+'.txt was written'
	  self.nMeasCounter = self.nMeasCounter-1
	  #while :
	  #  time.sleep(1)

    def update(self):
        self.statusBar().showMessage('Update timer event')
        step = 4

	if self.DMAType == 0:  #'DMA type : B WR values'
	  self.plot.setLabel('left', "B", units='T')
	  if self.offset==2:
	    scalling = 10e-9
	  elif self.offset==3:
	    scalling = 1e-6
	  else:
	    scalling = 1
	elif self.DMAType == 1:#'DMA type : B local values'
	  self.plot.setLabel('left', "B", units='T')
	  if self.offset==2:
	    scalling = 10e-9
	  elif self.offset==3:
	    scalling = 1e-6
	  else:
	    scalling = 1
	elif self.DMAType == 2:#'DMA type : B side by side values'
	  self.plot.setLabel('left', "B", units='T')
	  if self.offset==2:
	    scalling = 10e-9
	  elif self.offset==3:
	    scalling = 1e-6
	  else:
	    scalling = 1
	elif self.DMAType == 3:#'DMA type : ADC values'
	  self.plot.setLabel('left', "ADC", units='V')
	  scalling = 0.000076294

        #if self.offset==2:
        #    scalling = 10e-9
        #elif self.offset==3:
        #    scalling = 1e-6
        #else:
        #    scalling = 1
        if self.dma_tag!=self.bmeas.dma_tag(self.bus):
            self.dma_tag=self.bmeas.dma_tag(self.bus)
            self.bmeas.last_cycle(self.bus,self.last_cycle_pointer.ctypes.data,self.last_sample_address.ctypes.data)
            self.memsize = (self.last_sample_address[0]+1)*2
            last = self.memsize/step
            len_keep_signal = self.len_signal-last

            self.signal[0:len_keep_signal] = self.signal[last:self.len_signal]
            self.signal[len_keep_signal:self.len_signal] = scalling*np.double(self.last_cycle_pointer[self.offset:self.memsize:step])

            self.B[0:len_keep_signal] = self.B[last:self.len_signal]
            self.B[len_keep_signal:self.len_signal] = self.scalling_B*np.double(self.last_cycle_pointer[self.offset_B:self.memsize:step])

            self.Bdot[0:len_keep_signal] = self.Bdot[last:self.len_signal]
            self.Bdot[len_keep_signal:self.len_signal] = self.scalling_Bdot*np.double(self.last_cycle_pointer[self.offset_Bdot:self.memsize:step])

            self.ctrl_1[0:len_keep_signal] = self.ctrl_1[last:self.len_signal]
            self.ctrl_1[len_keep_signal:self.len_signal] = self.scalling_ctrl*np.uint32(self.last_cycle_pointer[self.offset_ctrl_1:self.memsize:step])

            self.ctrl_2[0:len_keep_signal] = self.ctrl_2[last:self.len_signal]
            self.ctrl_2[len_keep_signal:self.len_signal] = self.scalling_ctrl*np.uint32(self.last_cycle_pointer[self.offset_ctrl_2:self.memsize:step])

            self.C0[0:len_keep_signal] = self.C0[last:self.len_signal]
            self.C0[len_keep_signal:self.len_signal] = np.double(0.1*np.double(np.uint32(self.last_cycle_pointer[self.offset_ctrl_1:self.memsize:step])&np.uint32(1)))

            self.curve.setData(self.t,self.signal)
            self.curve2.setData(self.t,self.C0,pen=self.C0Show)
		
        if self.nMeasCounter!=0:
            if ((self.timeStamp+(42*1.2)+0.7)<time.time()):
              self.buttonFileWriteClicked()

	#if self.DMAType == 0:  #'DMA type : B WR values'
	#elif self.DMAType == 1:#'DMA type : B local values'
	#elif self.DMAType == 2:#'DMA type : B side by side values'
	#if self.DMAType == 3:#'DMA type : ADC values'
        #  print "Mean value %s" % hex(sum(self.signal)/(0.000076294*len(self.signal)))
	#  for x in self.signal:
	#    if x>5:
        #      hexvalue = hex(x/0.000076294)
	#      print hexvalue
         

    def closeEvent(self, event):
        print "Stop the DMA"
        bmeas.stop_dma(self.bus) # stop DMA
    
def main():
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    timer = QtCore.QTimer()
    timer.timeout.connect(ex.update)
    timer.start(200) 
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
