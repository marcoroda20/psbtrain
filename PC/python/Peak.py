#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
ZetCode PyQt4 tutorial 

In this example, we determine the event sender
object.

author: Jan Bodnar
website: zetcode.com 
last edited: October 2011
"""

import sys
#from PyQt4 import QtGui, QtCore
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
from math import sin, pi
import fcntl
import struct
import ctypes
import operator
import os.path

class Example(QtGui.QMainWindow):
    
    def __init__(self):
        super(Example, self).__init__()
        
        self.initUI()
        
    def initUI(self):      

        win = pg.GraphicsWindow(title="Value of DMA from Bmeas driver")
        win.resize(1000,600)
        win.setWindowTitle('Bmeas GUI')

        # Enable antialiasing for prettier plots
        #pg.setConfigOptions(antialias=False)
        pg.setConfigOptions(antialias=True)
        
        self.offset = 2
        # Create some widgets to be placed inside
        btnChannel = QtGui.QPushButton('Channel')
        btnFileWrite = QtGui.QPushButton('Write File')
        self.text = QtGui.QLineEdit('Channel [' + str(self.offset) + ']')
        self.text.setReadOnly(True)
        self.filename = QtGui.QLineEdit('Signals')
        self.comboTime = QtGui.QComboBox()
	self.comboTime.addItems (('15','30'))
	
	self.comboTime.currentIndexChanged.connect(self.currentIndexChanged)

        plot = pg.PlotWidget()
        btnChannel.clicked.connect(self.buttonChannelClicked)               
        btnFileWrite.clicked.connect(self.buttonFileWriteClicked)               
        
        ## Create a grid layout to manage the widgets size and position
        layout = QtGui.QGridLayout()
        win.setLayout(layout)

        ## Add widgets to the layout in their proper positions
        layout.addWidget(btnChannel, 0, 0)   # button goes in upper-left
        layout.addWidget(self.text, 1, 0)   # text edit goes in middle-left
        layout.addWidget(self.filename, 2, 0)   # text edit goes in middle-left
        layout.addWidget(btnFileWrite, 3, 0)   # button goes in upper-left
        layout.addWidget(self.comboTime, 4, 0)   # tab selection for time
        layout.addWidget(plot, 0, 1, 6, 1)  # plot goes on right side, spanning 3 rows
        layout.setColumnStretch(1, 2)         
        
        plot.resize(1000,600)
        mypen=pg.mkPen('y',width=1)
        self.curve = plot.plot(pen=mypen)
        self.curve2 = plot.plot(pen='r')
        plot.setLabel('left', "B", units='T')
        plot.setLabel('bottom', "t", units='s')
        
        self.setCentralWidget(win)
        self.nFile=1
        
        # Configure Bmeas to be able to read the DMA
        self.bmeas = ctypes.CDLL("/home/doberson/psbtrain/PC/spec-bmeas-sw/tools/bmeas_python_wrapper.so")
        self.bus=4 # fmc-0300 => integral
        self.dma_tag = self.bmeas.dma_tag(self.bus)
        data_0300 = self.bmeas.read_reg(self.bus,0x5000);
        self.bmeas.start_dma(self.bus) # start DMA
        n_bytes = (900000*4*4)
        self.last_cycle_pointer = np.ones(n_bytes, dtype=np.int32)
        self.last_sample_address = np.ones(1, dtype=np.int32)
 
        # Plot cycle
        self.t_plot_max=15
        self.fe = 50e3
        self.t = np.arange(-1*self.t_plot_max,0,1.0/self.fe)
        self.len_signal = len(self.t)
        self.signal = np.zeros(self.len_signal, dtype=np.double)

        self.offset_ctrl_1 = 0
        self.offset_ctrl_2 = 1
        self.C0 = np.zeros(self.len_signal, dtype=np.double)
        self.ZeroCycle = np.zeros(self.len_signal, dtype=np.double)

        self.statusBar()
        
        self.setGeometry(300, 300, 1600, 800)
        self.setWindowTitle('Bmeas GUI')
        self.show()
                
    def currentIndexChanged(self):
        self.t_plot_max=int(self.comboTime.currentText())
	print self.t_plot_max
        self.fe = 50e3
        self.t = np.arange(-1*self.t_plot_max,0,1.0/self.fe)
        self.len_signal = len(self.t)
        self.signal = np.zeros(self.len_signal, dtype=np.double)

        self.B = np.zeros(self.len_signal, dtype=np.double)
        self.scalling_B = 10e-9
        self.offset_B = 2
        self.Bdot = np.zeros(self.len_signal, dtype=np.double)
        self.scalling_Bdot = 1.0e-6
        self.offset_Bdot = 3
        self.ctrl_1 = np.zeros(self.len_signal, dtype=np.uint32)
        self.offset_ctrl_1 = 0
        self.ctrl_2 = np.zeros(self.len_signal, dtype=np.uint32)
        self.scalling_ctrl = 1
        self.offset_ctrl_2 = 1
 
        self.C0 = np.zeros(self.len_signal, dtype=np.double)
        self.ZeroCycle = np.zeros(self.len_signal, dtype=np.double)
            
    def buttonChannelClicked(self):
        self.signal = np.zeros(self.len_signal, dtype=np.double)

        self.B = np.zeros(self.len_signal, dtype=np.double)
        self.Bdot = np.zeros(self.len_signal, dtype=np.double)
        self.ctrl_1 = np.zeros(self.len_signal, dtype=np.uint32)
        self.ctrl_2 = np.zeros(self.len_signal, dtype=np.uint32)
        if self.offset < 3:
            self.offset = self.offset + 1
        else:
            self.offset = 0
        self.text.setText('Channel [' + str(self.offset) + ']')

    def buttonFileWriteClicked(self):
	filename = self.filename.text()
	while os.path.isfile(os.getcwd()+'/'+filename+'_'+str(self.nFile).zfill(4)+'.txt'):
            self.nFile=self.nFile+1
        with file(os.getcwd()+'/'+filename+'_'+str(self.nFile).zfill(4)+'.txt', 'w') as outfile:
            reg_5000 = self.bmeas.read_reg(self.bus,0x5000);
            outfile.write('#Register 0x5000 : '+hex(reg_5000)+'\n')
            outfile.write('# B.int32[10nT] Bdot.int32[1uT/s] ctrl1 ctrl2 \n')
            write_data=np.array([self.B,self.Bdot,self.ctrl_1,self.ctrl_2])
            np.savetxt(outfile, write_data.transpose())

    def update(self):
        self.statusBar().showMessage('Update timer event')
        step = 2
        if self.offset==2:
            scalling = 10e-9
        elif self.offset==3:
            scalling = 1e-6
        else:
            scalling = 1
        if self.dma_tag!=self.bmeas.dma_tag(self.bus):
            self.dma_tag=self.bmeas.dma_tag(self.bus)
            self.bmeas.last_cycle(self.bus,self.last_cycle_pointer.ctypes.data,self.last_sample_address.ctypes.data)
            self.memsize = (self.last_sample_address[0]+1)*2
            last = self.memsize/step
            len_keep_signal = self.len_signal-last
	    print "Last address = % d" % self.bmeas.read_reg(self.bus,0x5010)
	    #print "step = %d" % step
	    print self.last_sample_address[0]
	    print "Signal length to add = %d" % len(self.last_cycle_pointer[self.offset:self.memsize:step])
	    #print "Index %d " % np.argmax(self.last_cycle_pointer[self.offset:self.memsize:step])
	    #print "Max B int32_t = %d" % max(self.last_cycle_pointer[self.offset:self.memsize:step])
	    print self.offset

            if self.offset<2:
              self.signal[0:len_keep_signal] = self.signal[last:self.len_signal]
              self.signal[len_keep_signal:self.len_signal] = scalling*np.double(self.last_cycle_pointer[self.offset:self.memsize:step])
            else:
              self.signal[0:len_keep_signal+1] = self.signal[last-1:self.len_signal]
              self.signal[len_keep_signal+1:self.len_signal] = scalling*np.double(self.last_cycle_pointer[self.offset:self.memsize:step])


            #self.C0[0:len_keep_signal] = self.C0[last:self.len_signal]
            #self.C0[len_keep_signal:self.len_signal] = np.double(0.1*np.double(np.uint32(self.last_cycle_pointer[self.offset_ctrl_1:self.memsize:step])&np.uint32(1)))

            self.curve.setData(self.t,self.signal)
            #self.curve2.setData(self.t,self.C0)
         
    def closeEvent(self, event):
        print "Stop the DMA"
        bmeas.stop_dma(seld.bus) # stop DMA
        
def main():
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    timer = QtCore.QTimer()
    timer.timeout.connect(ex.update)
    timer.start(200) 
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
