#from ctypes import *
import numpy
#import scipy
import ctypes
#from ctypes.util import find_library

bmeas = ctypes.CDLL("/home/doberson/psbtrain/PC/spec-bmeas-sw/tools/bmeas_python_wrapper.so")

bus=3
# fmc-0300 => integral
data_0300 = bmeas.read_reg(bus,0x5000);

# start DMA
bmeas.start_dma(bus)

# print last_cycle
n_bytes = (720000*4*4)
last_cycle_pointer = numpy.ones(n_bytes, dtype=numpy.uint32)
last_sample_address = numpy.ones(1, dtype=numpy.uint32)
bmeas.last_cycle(bus,last_cycle_pointer.ctypes.data,last_sample_address.ctypes.data)
print last_sample_address

# stop DMA
bmeas.stop_dma(bus)

# fmc-0400 => peakdetector
data_0400 = bmeas.bm_read(4,0x5000);

