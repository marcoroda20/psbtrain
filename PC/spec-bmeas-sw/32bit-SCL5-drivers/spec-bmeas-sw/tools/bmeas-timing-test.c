#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <time.h>
#include "bmeas-lib.h"

int main (int argc, char * argv []) {
	int fd,i;
	uint32_t last_dma_tag=0;
	struct bmeasd_reg     read_reg;
	//struct bmeasd_dma     dma;
	struct bmeasd_dma_tag dma_tag;
	struct bmeasd_cycle   test_cycle;
	//struct firmware       frmw_name;
	//uint32_t *last_cycle_data;
	char filename[13] = "/dev/fmc-0400";
	int arg_index = 0;
	int m;
	double diff;
	clock_t launch,done;

	test_cycle.pdata = (unsigned int *)malloc(4*DMA_DATA_LENGTH*SIZE_DMA_DATA);
	test_cycle.size = DMA_DATA_LENGTH;

	if ( (argc-arg_index)>=3) 
	{
		if (strcmp(argv[1], cmd_mBUS) == 0)  
		{
			arg_index += 2;
			filename[10]=*argv[2];
		}
	}

	fd = open(filename, O_RDWR);

	if (fd == -1)
	{
		printf("Error in opening file \n");
		exit(-1);
	}
	printf("File opened \n");

	if ( (argc-arg_index)>=2) 
	{

		if (strcmp(argv[1+arg_index], cmd_READ) == 0)  
		{
			printf("Argument : %s\n",cmd_READ);
			//READ example
			if ( (argc-arg_index)>=3) {
				sscanf(argv[2+arg_index], "%x", &read_reg.addr);
			}else{
				read_reg.addr = 0x5000;
			}
			printf("Address = %08x\n",read_reg.addr);
			ioctl(fd,BMEAS_IOC_READ_REG,&read_reg);   
			printf("BMEAS_IOC_READ_REG : %08x\n",BMEAS_IOC_READ_REG);
			printf("READ register %08x => %08x\n",read_reg.addr,read_reg.data);
		}
		if (strcmp(argv[1+arg_index], cmd_WRITE) == 0)  
		{
			//WRITE example
			if ( (argc-arg_index)>=3) {
				sscanf(argv[2+arg_index], "%x", &read_reg.addr);
			}else{
				read_reg.addr = 0x5000;
			}
			printf("Addresse = %08x\n",read_reg.addr);
			if ( (argc-arg_index)>=4) {
				sscanf(argv[3+arg_index], "%x", &read_reg.data);
			}
			else{
				ioctl(fd,BMEAS_IOC_READ_REG,&read_reg);   
				read_reg.data = read_reg.data | 0x80;
			}
			printf("Data     = %08x\n",read_reg.data);
			printf("WRITE register %08x => %08x\n",read_reg.addr,read_reg.data);
			ioctl(fd,BMEAS_IOC_WRITE_REG,&read_reg);  
			printf("after write reg. %08x => %08x\n",read_reg.addr,read_reg.data);
		}
		if (strcmp(argv[1+arg_index], cmd_START_DMA) == 0)  
		{
			printf("Argument : %s\n",cmd_START_DMA);
			//dma.size=DMA_DATA_LENGTH;
			//printf("dma.size : %d\n",dma.size);
			//ioctl(fd,BMEAS_IOC_START_DMA,&dma);  //ioctl call
			ioctl(fd,BMEAS_IOC_START_DMA);  //ioctl call
		}
		if (strcmp(argv[1+arg_index], cmd_STOP_DMA) == 0)  
		{
			printf("Argument : %s\n",cmd_STOP_DMA);
			ioctl(fd,BMEAS_IOC_STOP_DMA);  //ioctl call
		}
		if (strcmp(argv[1+arg_index], cmd_DMA_TAG) == 0)
		{
			printf("Argument : %s\n",cmd_DMA_TAG);
			ioctl(fd,BMEAS_IOC_DMA_TAG,&dma_tag);  //ioctl call
			printf("DMA tag : %d\n",dma_tag.dma_tag);
		}
		if (strcmp(argv[1+arg_index], cmd_TIMING) == 0)
		{
			ioctl(fd,BMEAS_IOC_START_DMA);  //ioctl call
			launch = clock();
			for (m=0;m<100;m++)
			{
				ioctl(fd,BMEAS_IOC_DMA_TAG,&dma_tag);  //ioctl call
				if (dma_tag.dma_tag!=last_dma_tag)
				{
					done = clock();
					diff = (done - launch) / CLOCKS_PER_SEC;
					printf("DMA_tag : %d   Timing : %f \n",dma_tag.dma_tag,diff);
					last_dma_tag=dma_tag.dma_tag;
				}
				sleep(10);
			}
			ioctl(fd,BMEAS_IOC_STOP_DMA);  //ioctl call
		}
		if (strcmp(argv[1+arg_index], cmd_LAST_CYCLE) == 0)
		{
			printf("Argument : %s\n",cmd_LAST_CYCLE);
			ioctl(fd,BMEAS_IOC_LAST_CYCLE,&test_cycle);  //ioctl call
			for (i=0;i<DMA_DATA_LENGTH;i++)
			{
				if ((i>=0)&(i<16)){
					//printf("Buffer[%d] : 0x%04x \n",i,*(((unsigned short *)test_cycle.pdata)+i));
					printf("Buffer[%d] : 0x%08x \n",i,*((test_cycle.pdata)+i));
				}
				if (i>DMA_DATA_LENGTH-17){
					//printf("Buffer[%d] : 0x%04x \n",i,*(((unsigned short *)test_cycle.pdata)+i));
					printf("Buffer[%d] : 0x%08x \n",i,*((test_cycle.pdata)+i));
				}
			}

		}
	}
	close(fd);
	printf("File closed \n");
	return 0;
}
