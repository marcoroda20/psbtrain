#!/bin/bash
echo

DRIVER_MOUNTED=`lsmod | grep fmc | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver fmc.ko already inserted
else
 echo Insert driver fmc.ko...
 insmod /lib/modules/`uname -r`/extra/fmc.ko
 echo OK
fi
sleep 3

DRIVER_MOUNTED=`lsmod | grep spec | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver spec.ko already inserted
else
 echo Insert driver spec.ko...
 insmod /lib/modules/`uname -r`/extra/spec.ko
 echo OK
fi
sleep 3

echo Load firmware spec_top_fmc_adc2M18b2ch.bin... 
 ./spec-fwloader -b 3 ../../../FPGA/Integral/spec_btrain/syn/spec_top_fmc_adc2M18b2ch.bin
# ./spec-fwloader -b 3 ../../firmware/spec_top_fmc_adc2M18b2ch.bin
sleep 1
echo Load firmware spec_top_fmc_adc_16b_10Ms.bin ...
 ./spec-fwloader -b 4 ../../../FPGA/FMR/spec_peak_detector/syn/spec_top_fmc_adc_16b_10Ms.bin
# ./spec-fwloader -b 4 ../../firmware/spec_top_fmc_adc_16b_10Ms.bin
sleep 1

#./specmem 502c 03630323
#echo Setup register 0x502c = 0x`./specmem 502c`


DRIVER_MOUNTED=`lsmod | grep bmeas | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver bmeas.ko already inserted
else
 echo Insert driver bmeas.ko...
 insmod /lib/modules/`uname -r`/extra/bmeas.ko
 echo OK
fi
sleep 2

echo " rm -f /dev/fmc-0300"
rm -f /dev/fmc-0300
echo " rm -f /dev/fmc-0400"
rm -f /dev/fmc-0400

minor_3=`awk -F " " 'BEGIN {} $2 = /fmc-0300/ { print $1 }' /proc/misc`
echo "Create mknod with correct minor number : mknod /dev/fmc-0300 c 10 $minor_3"
#awk -F " " 'BEGIN {} $2 = /fmc-0300/ { mknod /dev/fmc-0300 c 10 $1 }' /proc/misc
mknod /dev/fmc-0300 c 10 $minor_3
minor_4=`awk -F " " 'BEGIN {} $2 = /fmc-0400/ { print $1 }' /proc/misc`
echo "Create mknod with correct minor number : mknod /dev/fmc-0400 c 10 $minor_4"
#awk -F " " 'BEGIN {} $2 = /fmc-0400/ { mknod /dev/fmc-0400 c 10 $1 }' /proc/misc
mknod /dev/fmc-0400 c 10 $minor_4

echo "Give rights to user : chmod 666 /dev/fmc-0300"
chmod 666 /dev/fmc-0300
echo "Give rights to user : chmod 666 /dev/fmc-0400"
chmod 666 /dev/fmc-0400

