#!/bin/bash
echo

echo " rm -f /dev/fmc-0300"
rm -f /dev/fmc-0300
echo " rm -f /dev/fmc-0400"
rm -f /dev/fmc-0400

minor_3=`awk -F " " 'BEGIN {} $2 = /fmc-0300/ { print $1 }' /proc/misc`
echo "Create mknod with correct minor number : mknod /dev/fmc-0300 c 10 $minor_3"
#awk -F " " 'BEGIN {} $2 = /fmc-0300/ { mknod /dev/fmc-0300 c 10 $1 }' /proc/misc
mknod /dev/fmc-0300 c 10 $minor_3
minor_4=`awk -F " " 'BEGIN {} $2 = /fmc-0400/ { print $1 }' /proc/misc`
echo "Create mknod with correct minor number : mknod /dev/fmc-0400 c 10 $minor_4"
#awk -F " " 'BEGIN {} $2 = /fmc-0400/ { mknod /dev/fmc-0400 c 10 $1 }' /proc/misc
mknod /dev/fmc-0400 c 10 $minor_4

echo "Give rights to user : chmod 666 /dev/fmc-0300"
chmod 666 /dev/fmc-0300
echo "Give rights to user : chmod 666 /dev/fmc-0400"
chmod 666 /dev/fmc-0400

