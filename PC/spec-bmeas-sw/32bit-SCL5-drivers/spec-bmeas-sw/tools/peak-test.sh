#!/bin/bash
echo

DRIVER_MOUNTED=`lsmod | grep fmc | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver fmc.ko already inserted
else
 echo Insert driver fmc.ko...
 insmod /lib/modules/`uname -r`/extra/fmc.ko
 echo OK
fi
sleep 1

DRIVER_MOUNTED=`lsmod | grep spec | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver spec.ko already inserted
else
 echo Insert driver spec.ko...
 insmod /lib/modules/`uname -r`/extra/spec.ko
 echo OK
fi
sleep 1

echo 
echo Load firmware spec_top_fmc_adc_16b_10Ms.bin ...
echo "  path => bus 4 ../../../FPGA/FMR/spec_peak_detector/syn/spec_top_fmc_adc_16b_10Ms.bin"
 ./spec-fwloader -b 4 ../../../FPGA/FMR/spec_peak_detector/syn/spec_top_fmc_adc_16b_10Ms.bin
sleep 1
echo "  path => bus 5 ../../../FPGA/Integral/spec_btrain/syn/spec_top_fmc_adc2M18b2ch.bin"
 ./spec-fwloader -b 5 ../../../FPGA/Integral/spec_btrain/syn/spec_top_fmc_adc2M18b2ch.bin
sleep 1

echo 
echo Script to check the peakdetector FMC card with a TG5011 frequency generator. The generator is setuped as 1KHz, amplitude max 480mVpp and the C0 pulse replaced by the generator Sync output.
echo The system MUST BE SET TO EFFECTIVE mode.
echo 

# Setup registers
./specmem -b 4 502c 0x00410555
echo Setup Config register 0x502c = 0x`./specmem -b 4 502c` "(DAC1=>Threshold 5024, DAC2=>ADC high 1"

./specmem -b 4 5024 e000
echo Setup Threshold 1 register 0x5024 = 0x`./specmem -b 4 5024` "(-2.5V)"

./specmem -b 4 5028 e000
echo Setup Threshold 2 register 0x5028 = 0x`./specmem -b 4 5028` "(-2.5V)"

./specmem -b 4 5030 1388
echo Setup Windelay 1 register 0x5030 = 0x`./specmem -b 4 5030` "(100us)"

./specmem -b 4 5034 1388
echo Setup Windelay 2 register 0x5034 = 0x`./specmem -b 4 5034` "(100us)"

./specmem -b 4 5038 000061a8
echo Setup WinWidth 1 register 0x5038 = 0x`./specmem -b 4 5038` "(500us)"

./specmem -b 4 503c 000061a8
echo Setup WinWidth 2 register 0x503c = 0x`./specmem -b 4 503c` "(500us)"

./specmem -b 4 5040 64
echo Setup TrigWidth 1 register 0x5040 = 0x`./specmem -b 4 5040` "(20us)"

./specmem -b 4 5044 64
echo Setup TrigWidth 2 register 0x5044 = 0x`./specmem -b 4 5044` "(20us)"

./specmem -b 4 5048 0
echo Setup TrigDelayForced 1 register 0x5048 = 0x`./specmem -b 4 5048` "(0us)"

./specmem -b 4 504c 0
echo Setup TrigDelayForced 2 register 0x504c = 0x`./specmem -b 4 504c` "(0us)"

#DRIVER_MOUNTED=`lsmod | grep bmeas | wc -l`
#if [ $DRIVER_MOUNTED -ne 0 ]
#then
# echo Driver bmeas.ko already inserted
#else
# echo Insert driver bmeas.ko...
# insmod /lib/modules/`uname -r`/extra/bmeas.ko
# echo OK
#fi
#sleep 1

