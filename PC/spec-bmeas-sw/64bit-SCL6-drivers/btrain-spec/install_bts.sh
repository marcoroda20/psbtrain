#/bin/sh
# Build environment and run installation script
# BINARY_NAME without the prefix spec/svec
#DEVICE_NAME=SPECBTRAIN \
#DRIVER_NAME=SPECBTRAIN \
#CDEVNAME="fmc-0600" \
#DEPENDS="fmc" \
#BINARY_NAME="spec_top_fmc_adc2M18b2ch.bin" \
#sh /acc/local/share/drv/scripts/driver_install/install_fmc_mezzanine.sh $@
DRIVER_MOUNTED=`/sbin/lsmod | grep fmc | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver fmc.ko already inserted
else
 echo Insert driver fmc.ko...
 /sbin/insmod ../fmc-bus/fmc.ko
fi

DRIVER_MOUNTED=`/sbin/lsmod | grep spec | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver spec.ko already inserted
else
 echo Insert driver spec.ko...
 /sbin/insmod ../btrain-spec/spec.ko
 sleep 1
fi

echo "Load firmware"
./spec-fwloader -b 5 /usr/local/fmc/spec_top_fmc_adc2M18b2ch.bin

sleep 2

DRIVER_MOUNTED=`/sbin/lsmod | grep bmeas | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver bmeas.ko already inserted
else
 echo Insert driver bmeas.ko...
 /sbin/insmod ../btrain-spec/bmeas.ko
 sleep 1
fi

sleep 2

rm -f /dev/fmc-0500

minor_int=`awk -F " " 'BEGIN {} $2 = /fmc-0500/ { print $1 }' /proc/misc`
mknod /dev/fmc-0500 c 10 $minor_int

chmod 666 /dev/fmc-0500

