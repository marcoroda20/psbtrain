#/bin/sh
DRIVER_MOUNTED=`/sbin/lsmod | grep fmc | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver fmc.ko already inserted
else
 /sbin/insmod ../fmc-bus/fmc.ko
fi

DRIVER_MOUNTED=`/sbin/lsmod | grep spec | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver spec.ko already inserted
else
 /sbin/insmod ../peakdetector-spec/spec.ko
 sleep 1
fi

echo "Load firmware"
./spec-fwloader -b 4 /usr/local/fmc/spec_top_fmc_adc_16b_10Ms.bin

sleep 2

DRIVER_MOUNTED=`/sbin/lsmod | grep peak | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver peak.ko already inserted
else
  echo 'No driver needed for peak detector yet!'
  sleep 1
  # /sbin/insmod ../peakdetector-spec/peak.ko
fi

sleep 1 

rm -f /dev/fmc-0400

minor_peak=`awk -F " " 'BEGIN {} $2 = /fmc-0400/ { print $1 }' /proc/misc`
mknod /dev/fmc-0400 c 10 $minor_peak

chmod 666 /dev/fmc-0400
echo 'peak detector chmod done'
